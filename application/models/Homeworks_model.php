<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Homeworks_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'homeworks';
    var $column_order = array(); //set column field database for datatable orderable
    var $column_search = array(); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('homeworks.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'homeworks_m');
        $this->homeworks_m->set_table('homeworks');
    }

    public function get_homeworks($room_id, $teacher_id) {
        $where = array(
            "room_id" => $room_id,
            "teacher_id" => $teacher_id
        );
        $this->db->join('rooms_teachers', 'rooms_teachers.id=homeworks.room_teacher_id');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->where($where);
        $this->db->order_by('deadline', 'desc');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_homeworks_for_student($student_id) {
        $where = array(
            "student_id" => $student_id,
            'is_active' => 2,
        );
        $this->db->select('first_name,last_name,subject_name,deadline,title,text,attach,rooms_teachers.id as room_teacher_id');
        $this->db->join('rooms_teachers', 'rooms_teachers.id=homeworks.room_teacher_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id=teachers.user_id');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->where($where);
        $this->db->order_by('deadline', 'desc');
        $result = $this->get_all_crud();

        return $result;
    }

    public function count_all_homeworks($room_id, $teacher_id) {
        $where = array(
            "room_id" => $room_id,
            "teacher_id" => $teacher_id
        );
        $this->db->join('rooms_teachers', 'rooms_teachers.id=homeworks.room_teacher_id');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->order_by('deadline', 'desc');
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_homeworks($room_id, $teacher_id) {
        $where = array(
            "room_id" => $room_id,
            "teacher_id" => $teacher_id
        );
        $this->db->join('rooms_teachers', 'rooms_teachers.id=homeworks.room_teacher_id');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->where($where);
        $this->db->order_by('deadline', 'desc');
        $result = $this->count_filtered_crud();
        return $result;
    }

    public function count_all_homeworks_from_student($student_id) {
        $where = array(
            "student_id" => $student_id,
            'is_active' => 2,
        );
        $this->db->select('first_name,last_name,subject_name,deadline,title,text,attach,rooms_teachers.id as room_teacher_id');
        $this->db->join('rooms_teachers', 'rooms_teachers.id=homeworks.room_teacher_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id=teachers.user_id');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_homeworks_from_student($student_id) {
        $where = array(
            "student_id" => $student_id,
            'is_active' => 2,
        );
        $this->db->select('first_name,last_name,subject_name,deadline,title,text,attach,rooms_teachers.id as room_teacher_id');
        $this->db->join('rooms_teachers', 'rooms_teachers.id=homeworks.room_teacher_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id=teachers.user_id');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
