<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_cards_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'report_cards';
    var $column_order = array('id',); //set column field database for datatable orderable
    var $column_search = array('student_record_id',); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'report_cards_m');
        $this->report_cards_m->set_table('report_cards');
    }

}
