<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Messages_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'messages';
    var $column_order = array('msg_subject', 'msg_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('msg_subject', 'msg_date'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('messages.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'messages_m');
        $this->messages_m->set_table('messages');
    }

    public function get_messages() {
        
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_to ');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_message($id) {
        $where = array('id' => $id);
        $result = $this->get_by($where);
        return $result;
    }

    public function get_message_by_msg_id($msg_id) {
        $where = array('messages.id' => $msg_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*,users.id as user_id, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_to ');
        $result = $this->get_by($where);
        return $result;
    }

    public function get_sent_message_by_msg_id($msg_id) {
        $where = array('messages.id' => $msg_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*,users.id as user_id, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_to ');
        $result = $this->get_by($where);
        return $result;
    }

    public function get_recieve_message_by_msg_id($msg_id) {
        $where = array('messages.id' => $msg_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*,users.id as user_id, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from');
        $result = $this->get_by($where);

        return $result;
    }

    public function get_recieve_message_by_msg_id_from_parent($msg_id) {
        $where = array('messages.id' => $msg_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*,users.id as user_id, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from');
        $this->db->join('parents', 'parents.user_id=users.id', 'left');
        $result = $this->get_by($where);
        return $result;
    }

    public function get_sender_messages_by_user($user_id) {
        $where = array('messages.msg_from' => $user_id,
//            "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_to ');
        $this->db->where($where);
        $this->db->order_by('messages.msg_date', 'desc');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_recieve_messages_by_user($user_id) {
        $where = array('messages.msg_to' => $user_id,
//            "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from ');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_recieve_messages_by_user_from_parent($user_id) {
        $where = array('messages.msg_to' => $user_id,
//                "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from');
        $this->db->join('parents', 'parents.user_id=users.id', 'left');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }

    public function count_received_msg_for_user_id($user_id) {
        $where = array(
            'messages.msg_to' => $user_id,
            'seen' => 0,
//              "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('seen,messages.id as id');
        $this->db->join('users', 'users.id=messages.msg_from');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return count($result);
    }

    public function count_all_sender_messages_by_user($user_id) {
        
        $where = array('messages.msg_from' => $user_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_to ');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_sender_messages_by_user($user_id) {
        $where = array('messages.msg_from' => $user_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_to ');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    public function count_all_recieve_messages_by_user($user_id) {
        $where = array('messages.msg_to' => $user_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from ');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_recieve_messages_by_user($user_id) {
        $where = array('messages.msg_to' => $user_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from ');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    public function count_all_recieve_messages_by_user_from_parent($user_id) {
        $where = array('messages.msg_to' => $user_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from');
        $this->db->join('parents', 'parents.user_id=users.id', 'left');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_recieve_messages_by_user_from_parent($user_id) {
        $where = array('messages.msg_to' => $user_id,
//                  "YEAR(messages.msg_date)" => $this->_archive_year,
        );
        $this->db->select('*, messages.id as id,first_name,last_name');
        $this->db->join('users', 'users.id=messages.msg_from');
        $this->db->join('parents', 'parents.user_id=users.id', 'left');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
