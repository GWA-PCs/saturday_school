<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Meetings_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    public $table = 'meetings';
    public $column_order = array('meeting_title', 'meeting_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    public $column_search = array('meeting_title', 'meeting_date', 'meeting_file'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    public $order = array('meetings.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'meetings_m');
        $this->meetings_m->set_table('meetings');
    }

    public function get_meetings() {
        $this->db->select('*, meetings.id as id');
        $this->db->order_by('meeting_date', 'desc');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_meeting($id) {
        $where = array('id' => $id);
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_meetings() {
        $this->db->select('*, meetings.id as id');
        $result = $this->count_all();
        return $result;
    }

    public function count_filtered_crud_meetings() {
        $this->db->select('*, meetings.id as id');
        $result = $this->count_filtered_crud();
        return $result;
    }

    public function get_meeting_to_from_rooms($rooms_ids) {
        $res = [];

        $where = array('rooms_teachers.year' => $this->_archive_year,);
        $this->db->select('user_id');
        $this->db->where_in('room_id', $rooms_ids);
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $room_teacher = $this->rooms_teachers_model->get_many_by($where);

        foreach ($room_teacher as $item) {
            if (!in_array($item->user_id, $res)) {
                $res[] = $item->user_id;
            }
        }
        return $res;
    }

    public function get_user_meeting($user_id) {
        $where = array('user_id' => $user_id);
        $this->db->select('*');
        $this->db->where($where);
        $this->db->join('users_meetings', 'users_meetings.meet_id=meetings.id');
        $users_meetings = $this->get_all_crud();

        return $users_meetings;
    }

}
