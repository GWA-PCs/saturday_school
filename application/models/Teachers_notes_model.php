<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers_notes_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'teachers_notes';
    var $column_order = array('note_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('note_date'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'teachers_notes_m');
        $this->teachers_notes_m->set_table('teachers_notes');

        $this->load->model("teachers_model");
    }

    function get_teacher_notes($teacher_id) {
        $where = array('teacher_id' => $teacher_id,);
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }
}
