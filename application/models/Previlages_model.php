<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Previlages_model extends MY_Model
{
    public $before_create = array( 'created_at', 'updated_at' );
    public $before_update = array( 'updated_at' );

    var $table = 'previlages';
    var $column_order = array('previlage_name_en', 'previlage_name_ar', 'function_name', 'previlage_type', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('previlage_name_en', 'previlage_name_ar', 'function_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order


    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model', 'previlage_m');
        $this->previlage_m->set_table('previlages');
    }


    function get_previlage_by_id($id) {
        $where = array(
            'id' => $id
        );
        $result = $this->previlage_m->get_by($where);
        return $result;
    }


    function get_permissions($num_rec_per_page, $page) {
        $data = array();
        $start_from = ($page-1) * $num_rec_per_page;
        $total = $this->previlage_m->get_all();
        $sql = "SELECT * FROM previlage Order By id asc LIMIT $start_from, $num_rec_per_page";
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result() as $row){
            $result[] = $row;
        }
        $data['data'] = $result;
        $data['total'] = count($total);
        return $data;
    }

    function get_previlages_by_groups_id($groups) {

        $groups_id = array();
        foreach ($groups as $group) {
            $groups_id[] = $group->group_id;
        }

        $this->db->where('group_id', $groups_id[0]);
        for($i = 1 ; $i < count($groups_id) ; $i++) {
            $this->db->group_start();
            $this->db->or_where('group_id', $groups_id[$i]);
            if($i == count($groups_id) -1){
                $this->db->group_end();
            }
        }

        $this->db->join('groups_previlages', 'groups_previlages.previlage_id = previlages.id');
        $resutls = $this->previlage_m->get_all();

        return $resutls;
    }

}
