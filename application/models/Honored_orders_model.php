<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Honored_orders_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'honored_orders';
    var $column_order = array('order_title', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('first_name', 'last_name', 'order_title', 'stage_name', 'honored_orders.created_at', 'honored_date', 'order_status', 'honored_done',); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('honored_orders.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'honored_orders_m');
        $this->honored_orders_m->set_table('honored_orders');
    }

    public function get_honored_orders($teacher_id) {
        $where = array('teacher_id' => $teacher_id,
        );

        $this->db->select('*, honored_orders.id as id');
        $this->db->join('stages', 'stages.id = honored_orders.stage_id');
        $this->db->join('teachers', 'teachers.id = honored_orders.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));
        $this->db->order_by("honored_orders.created_at DESC");
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_other_honored_orders($teacher_id, $honored_date) {
        $where = array(
            'teacher_id<>' => $teacher_id,
            'order_status' => 'reservation_done',
            'honored_date' => $honored_date,
        );

        $this->db->select('*, honored_orders.id as id');

        $this->db->where($where);
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));

        $result = $this->get_all();
        return $result;
    }

    public function get_honored_orders_for_admin() {


        $this->db->select('*, honored_orders.id as id,honored_orders.created_at as created_at,honored_date');
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));
        $this->db->join('stages', 'stages.id = honored_orders.stage_id');
        $this->db->join('teachers', 'teachers.id = honored_orders.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');

//        $this->db->order_by('honored_orders.created_at', 'asc');
//        $this->db->order_by("honored_orders.created_at ASC");
        $this->db->order_by("honored_orders.created_at DESC,order_status DESC");
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_any_reserve_honored_order() {
        $where = array(
            'order_status' => 'reservation_done',
        );
        $result = $this->get_by($where);
        return $result;
    }

    public function get_pending_honored_orders() {
        $where = array(
            'order_status' => 'pending',
        );
        $this->db->select('*');
        $this->db->where($where);
        $this->db->order_by("created_at ASC");
        $result = $this->get_all();
        return $result;
    }

    public function get_first_pending_honored_order() {
        $where = array(
            'order_status' => 'pending',
        );
        $this->db->order_by("created_at ASC");
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_honored_orders($teacher_id) {
        $where = array('teacher_id' => $teacher_id,
        );

        $this->db->select('*, honored_orders.id as id');
        $this->db->join('stages', 'stages.id = honored_orders.stage_id');
        $this->db->join('teachers', 'teachers.id = honored_orders.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));

        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_honored_orders($teacher_id) {
        $where = array('teacher_id' => $teacher_id,
        );

        $this->db->select('*, honored_orders.id as id');
        $this->db->join('stages', 'stages.id = honored_orders.stage_id');
        $this->db->join('teachers', 'teachers.id = honored_orders.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));

        $result = $this->count_filtered_crud();
        return $result;
    }

    public function count_all_honored_orders_for_admin() {
        $order = array(
//            'order_status' => 'reservation_done',
            'honored_orders.created_at' => 'asc',
        );
        $where = array(
//            "YEAR(honored_orders.honored_date)" => $this->_archive_year,
//            "YEAR(honored_orders.honored_date)" => $this->_archive_year+1,
        );
        $this->db->select('*, honored_orders.id as id,honored_orders.created_at as created_at,honored_date');
        $this->db->join('stages', 'stages.id = honored_orders.stage_id');
        $this->db->join('teachers', 'teachers.id = honored_orders.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
//        $this->db->order_by('honored_orders.created_at', 'asc');
//        $this->db->order_by("order_status DESC,honored_orders.created_at ASC");
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_honored_orders_for_admin() {
        $order = array(
//            'order_status' => 'reservation_done',
            'honored_orders.created_at' => 'asc',
        );
        $where = array(
//            "YEAR(honored_orders.honored_date)" => $this->_archive_year,
//               "YEAR(honored_orders.honored_date)" => $this->_archive_year+1,
        );
        $this->db->select('*, honored_orders.id as id,honored_orders.created_at as created_at,honored_date');
        $this->db->join('stages', 'stages.id = honored_orders.stage_id');
        $this->db->join('teachers', 'teachers.id = honored_orders.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
//        $this->db->order_by('honored_orders.created_at', 'asc');
        $this->db->where_in('YEAR(honored_orders.honored_date)', array($this->_archive_year, $this->_archive_year + 1));
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
