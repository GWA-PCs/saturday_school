<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Petty_cashes_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'petty_cashes';
    var $column_order = array('petty_cashes_date', 'petty_cash_type', 'petty_cash_account', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('petty_cash_date', 'petty_cash_type', 'petty_cash_account', 'petty_cash_memo', 'petty_cash_payment', 'petty_cash_deposit'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('petty_cashes.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'petty_cash_m');
        $this->petty_cash_m->set_table('petty_cash');
    }

    public function get_all_petty_cash() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('*, petty_cashes.id as id');
        $this->db->where($where);
        $this->db->order_by('id','desc');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_sum_petty_cash() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('SUM(petty_cash_payment) as payment,SUM(petty_cash_deposit) as deposit');
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_petty_cash() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('*, petty_cashes.id as id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_petty_cash() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('*, petty_cashes.id as id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
