<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_subjects_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'parent_subject';
    var $column_order = array('parent_subject_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('parent_subject_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('parent_subjects.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'parent_subjects_m');
        $this->subjects_m->set_table('parent_subjects');
    }

    public function get_parent_subjects() {
        $this->db->order_by('id', 'asc');
        $result = $this->get_all_crud();
        return $result;
    }

}
