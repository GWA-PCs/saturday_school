<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_payment_months_model extends MY_Model
{

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'monthes';
    var $column_order = array('month_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('month_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('monthes.id' => 'asc'); // default order

    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model', 'monthes_m');
        $this->stages_m->set_table('monthes');
    }

    public function get_student_payment_months()
    {
        $result = $this->get_all_crud();
        return $result;
    }

//    public function get_student_payment_months_for_specific_student($student_payment_ids) {
//        $this->db->select('month_name');
//        foreach ($student_payment_ids as $value) {
////            $this->db->where('student_payment_id', $value);
//            $where = array('student_payment_id' => $value);
//        }
//        $result = $this->get_many_by($where);
//
//        $months=array();
//        if (isset($result) && $result)
//            foreach ($result as $value) {
//                $months[$value->month_name]=$value->month_name;
//            }
//        return $months;
//    }

    public function get_student_payment_months_for_specific_student($student_payment_id)
    {
        $this->db->select('month_name');
//        foreach ($student_payment_ids as $value) {
//            $this->db->where('student_payment_id', $value);
        $where = array('student_payment_id' => $student_payment_id);
//        }
        $result = $this->get_many_by($where);

        $months = array();
        if (isset($result) && $result)
            foreach ($result as $value) {
                $months[$value->month_name] = $value->month_name;
            }
        return $months;
    }

    function get_students_not_paid($months)
    {
        // get students records paid this monthes
        $where2 = array(
            'is_active' => 2,
            'payment_item_type' => 'tuition',
            'year' => $this->_archive_year,
        );
        $this->db->join('students_payments', ' students_payments.id=student_payment_months.student_payment_id');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');

        $monthes_in_db = array();
        if (isset($months) && $months) {
            foreach ($months as $key => $value) {
                if ($months[$key] != null) {
                    $monthes_in_db[$key] = $key;
                }
            }
            $this->db->where_in('month_name', $monthes_in_db);
        }
        $this->db->order_by('student_record_id');
        $this->db->where($where2);
        $result_biomonthy_monthy = $this->get_all();

        // get students for full year payments
        $where3 = array(
            'is_active' => 2,
            'payment_item_type' => 'tuition',
            'year' => $this->_archive_year,
        );
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->order_by('student_record_id');
        $this->db->where('tuition_type', 'full_year');
        $this->db->where($where3);
        $result_full_year = $this->students_payments_model->get_all();

        $result=array_merge($result_biomonthy_monthy,$result_full_year);

        // get students records who not paid this monthes
        $result2 = array();
        foreach ($result as $value) {
            $result2[] = $value->student_record_id;
        }

        $where = array(
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('is_active,'
            . 'students_records.id as student_record_id,students_records.student_id as student_id,grade_name,grade_id,room_id,room_name'
            . ',student_name,father_name ,family_name');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        if (isset($result2) && $result2) {
            $this->db->where_not_in('students_records.id', $result2);
        }
        $students_records = $this->students_records_model->get_many_by($where);

        return $students_records;
    }

}
