<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'payments';
    var $column_order = array('id', 'payment_value', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('payment_value , student_index'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('payments.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'payments_m');
        $this->payments_m->set_table('payments');
    }

    function get_payments() {
        $result = $this->get_all_crud();
        return $result;
    }

    function delete_payment($id) {
        $result = $this->payments_m->delete_by('id', $id);
        return $result;
    }

    function get_payment_by_id($id) {
        $where = array('id' => $id);
        $result = $this->payments_m->get_by($where);
        return $result;
    }

    function update_payment_by_id($item_id, $data) {
        $where = array('id' => $item_id);
        $result = $this->payments_m->update_by($where, $data);

        if (isset($result) && $result)
            return TRUE;
        else
            return FALSE;
    }

    public function insert_payment($data) {
        $result = $this->payments_m->insert($data);
        return $result;
    }

}
