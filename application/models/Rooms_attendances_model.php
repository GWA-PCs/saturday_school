<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms_attendances_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'rooms_attendances_m');
        $this->rooms_attendances_m->set_table('rooms_attendances');
        $this->load->model('students_model');
        $this->load->model('rooms_model');
        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');
        $this->load->model('general_model', 'students_attendances_m');
        $this->students_attendances_m->set_table('students_attendances');
    }

    function get_all_students_by_attendance_date($room_id, $date) {
        $where = array(
            'room_id' => $room_id,
            'attendance_date' => $date,
            'is_active' => 2,
            'year' => $this->_archive_year,
//            'YEAR(attendance_date)' => $this->_archive_year,
        );
        $this->db->select("students_records.id as id ,room_id, student_name, family_name, father_name, mother_name, attendance_date, attendance_type, semester, year,student_id");
        $this->db->join("parents", "parents.user_id = users.id");
        $this->db->join("students", "students.parent_id = parents.id");
        $this->db->join("students_records", "students_records.student_id = students.id");
        $this->db->join("students_attendances", "students_records.id = students_attendances.student_record_id");
        $this->db->order_by("student_name", 'ASC');
        $students_info = $this->users_m->get_many_by($where);
        return $students_info;
    }

    function get_room_attendance_by_date($room_id, $date) {
        $where = array(
            'attendance_date' => $date,
            'room_id' => $room_id,
//            'YEAR(attendance_date)' => $this->_archive_year,
        );
        $room_attendance_info = $this->rooms_attendances_m->get_by($where);
        return $room_attendance_info;
    }

    function get_all_students_by_room_id($room_id) {
        $where = array(
            'room_id' => $room_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select("students_records.created_at as created_at,students_records.id as id, room_id, student_name, family_name, father_name, mother_name, year,student_id");
        $this->db->join("parents", "parents.user_id = users.id");
        $this->db->join("students", "students.parent_id = parents.id");
        $this->db->join("students_records", "students_records.student_id = students.id");
        $this->db->order_by("student_name");
        $students_info = $this->users_m->get_many_by($where);

        return $students_info;
    }

    function insert_attendance($data) {
        $inserted = $this->rooms_attendances_m->insert($data);
        return $inserted;
    }

    function insert_list_students_attendaces($data) {
//        $inserted = False;
//        foreach ($data as $value) {
//            if ($value['attendance_type'] !=NULL )
//                $inserted = $this->students_attendances_m->insert($value);
//        }
        $inserted = $this->db->insert_batch('students_attendances', $data);
        return $inserted;
    }

    function delete_student_attendance($id) {
        $delete_item = $this->students_attendances_m->delete_by('student_record_id', $id);
        return $delete_item;
    }

    function update_student_attendance($id, $data) {
        $update_item = $this->students_attendances_m->update_by(array('student_record_id' => $id), $data);
        return $update_item;
    }

    function insert_student_attendance($data) {
        $insert = $this->students_attendances_m->insert($data);
        return $insert;
    }

        function get_number_of_absent_dayes($student_record_id, $semester_name) {
        $absent_days = 0;

        $where = array(
            'student_record_id' => $student_record_id,
            'attendance_type' => 'absence',
            'semester' => $semester_name
        );
        $this->db->select("COUNT(students_attendances.id) as count ,students_records.student_id as student_id, students_records.id as students_record_id,grade_name, attendance_date, attendance_type");
        $this->db->join("students_records", "students_records.id = students_attendances.student_record_id");
        $this->db->join("rooms", "rooms.id = students_records.room_id");
        $this->db->join("grades", "grades.id = rooms.grade_id");
        $attendance_info = $this->students_attendances_m->get_by($where);

        if (isset($attendance_info) && $attendance_info)
            $absent_days = $attendance_info->count;
        return $absent_days;
    }

}
