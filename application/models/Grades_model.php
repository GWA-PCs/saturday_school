<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Grades_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'grades';
    var $column_order = array('', '', '', null); //set column field database for datatable orderable
    var $column_search = array('grade_name', 'stage_name','next_grade_name',); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('grades.id' => 'ASC'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'grades_m');
        $this->grades_m->set_table('grades');

        $this->load->model('general_model', 'stages_m');
        $this->stages_m->set_table('stages');
    }

    public function get_grades() {
        $this->db->select('*, grades.id as id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->order_by('grade_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_grade($id) {
        $where = array(
            'next_grade_name' => $id,
        );
        $result = $this->get_by($where);
        return $result;
    }

    public function get_any_student_grade($id) {
        $where = array(
            'grades.id' => $id,
        );
        $this->db->join('rooms', 'rooms.grade_id=grades.id');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');

        $result = $this->get_by($where);
        return $result;
    }

    function get_grades_array() {
        $res = $this->grades_m->get_all();

        $array_result = array();
        foreach ($res as $item) {
            $array_result[$item->id] = $item->grade_name;
        }

        return $array_result;
    }

    public function get_grade_name($id) {
        $where = array(
            'id' => $id,
        );
        $result = $this->get_by($where);
        return $result;
    }

    public function get_stage_of_grade($grade_id) {
        $where = array('grades.id' => $grade_id);
        $this->db->select('grades.id as id,grade_name,stage_name,stage_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_grades() {
        $this->db->select('*, grades.id as id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->order_by('grade_name', 'ASC');
        $result = $this->count_all();
        return $result;
    }
    
    public function count_filtered_crud_grades() {
        $this->db->select('*, grades.id as id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->order_by('grade_name', 'ASC');
        $result = $this->count_filtered_crud();
        return $result;
    }

}
