<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Halls_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'halls';
    var $column_order = array('hall_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('hall_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('halls.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'halls_m');
        $this->halls_m->set_table('halls');
    }

    public function get_halls() {
        $this->db->select('*, halls.id as id');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_hall_status() {
        $where = array();
        $this->db->select('hall_status');
        $result = $this->get_by($where);
        return $result->hall_status;
    }

}
