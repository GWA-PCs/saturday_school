<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class users_meetings_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'users_meetings';
    var $column_order = array(null); //set column field database for datatable orderable
    var $column_search = array(); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('users_meetings.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'users_meetings_m');
        $this->users_meetings_m->set_table('users_meetings');
    }


    public function count_all_teacher_meeting($user_id) {
        $where = array(
            'users_meetings.user_id' => $user_id,
        );
        $this->db->select("*");
        $this->db->where($where);
        
        $teacher_meetings = $this->count_by($where);


        return $teacher_meetings;
    }

    public function count_filtered_crud_teacher_meeting($user_id) {
        $where = array(
            'users_meetings.user_id' => $user_id,
        );

        $this->db->select('*');
        $this->db->where($where);
        $teacher_meetings = $this->count_filtered_crud();

        // var_dump();
        return $teacher_meetings;
    }



}
