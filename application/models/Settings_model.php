<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'settings_m');
        $this->settings_m->set_table('settings');
    }

    function get_current_year() {
        $where = array(
            'setting_key' => 'year',
        );
        $this->db->select('setting_value');
        $res = $this->settings_m->get_by($where);
        return $res->setting_value;
    }

    function update_year($new_year) {
        $where = array(
            'setting_key' => 'year'
        );
        $res = $this->settings_m->update_by($where, array('setting_value' => $new_year));
        return $res;
    }

    function get_school_contact_email() {
        $where = array(
            'setting_key' => 'school_contact_email',
        );
        $this->db->select('setting_value');
        $res = $this->settings_m->get_by($where);
        return $res->setting_value;
    }

    function update_school_contact_email($school_contact_email) {
        $where = array(
            'setting_key' => 'school_contact_email'
        );
        $res = $this->settings_m->update_by($where, array('setting_value' => $school_contact_email));
        return $res;
    }

    

}
