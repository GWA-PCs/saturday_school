<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log_messages_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'log_messages';
    var $column_order = array('sender_email', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('sender_email', 'reciever_email'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('log_messages.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'log_messages_m');
        $this->log_messages_m->set_table('log_messages');
    }

    public function get_log_messages() {
        $this->db->select('*, log_messages.id as id');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_log_message_status() {
        $where = array();
        $this->db->select('status');
        $result = $this->get_by($where);
        return $result->status;
    }

}
