<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Parents_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'parents';
    var $column_order = array('', '', '', '', null); //set column field database for datatable orderable
    var $column_search = array('family_name', 'father_name', 'mother_name', 'mother_phone', 'father_phone', 'email',); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('parents.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();


        $this->load->model('general_model', 'parents_m');
        $this->parents_m->set_table('parents');

        $this->load->model('general_model', 'students_m');
        $this->students_m->set_table('students');

        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');
    }

    function get_parents() {
        $where = array('type' => 'parent');
        $this->db->select('*, parents.id as id ');
        $this->db->join('users', 'users.id = parents.user_id');
        $this->db->where($where);
        $this->db->order_by('family_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    function update_parent_by_id($item_id, $user_data, $parent_data) {
        $where = array('id' => $item_id);
        $result = $this->parents_m->update_by($where, $parent_data);

        $where = array('id' => $item_id);
        $parent_info = $this->parents_m->get_by($where);
//        var_dump($parent_info);

        $where = array('id' => $parent_info->user_id);
        $user_result = $this->users_m->update_by($where, $user_data);

        if (isset($user_result) && $user_result && isset($result) && $result)
            return TRUE;
        else
            return FALSE;
    }

    function get_parent_by_id($id) {
        $where = array('parents.id' => $id);
        $this->db->select('*,parents.id as id');
        $this->db->join('users', 'users.id = parents.user_id');
        $result = $this->parents_model->get_by($where);
        return $result;
    }

    function delete_parent($id) {
        $user_id = $this->get_user_id_by_parent_id($id);

        $sql = 'DELETE   `parents`,`users_groups`,`users`
            FROM     `parents`,`users_groups`,`users`
            WHERE   `users`        .`id`       = ' . $user_id . '
            AND     `parents`   .`id` = ' . $id . ' 
            AND     `users_groups`.`user_id`  = ' . $user_id;
        $result = $this->db->query($sql);
        return $result;
    }

    function get_parents_for_auto_complete($query) {
        $where = array('type' => 'parent',);
        $this->db->select('*,users.id as user_id,parents.id as parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->where($where);
        $this->db->group_start();
        $this->db->like('first_name', $query);
        $this->db->or_like('last_name', $query);
        $this->db->or_like('father_name', $query);
        $this->db->group_end();
        $this->db->limit(20);
        $result = $this->get_all();
        return $result;
    }

    function get_parent_by_parent_id($parent_id) {
        $where = array('id' => $parent_id);
        $result = $this->parents_m->get_by($where);
        return $result;
    }

    function get_user_id_by_parent_id($item_id) {
        $where = array('id' => $item_id);
        $parent_info = $this->parents_m->get_by($where);

        $where = array('id' => $parent_info->user_id);
        $user_result = $this->users_m->get_by($where);

        if (isset($user_result) && $user_result)
            return $user_result->id;
        else
            return FALSE;
    }

    function get_student_parent($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->order_by('student_name', 'ASC');
        $result = $this->students_m->get_by($where);
        return $result;
    }

    function get_parent_children($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->select('student_name,family_name,parent_id,students.id as student_id');
        $this->db->join('students', 'students.parent_id=parents.id');
        $this->db->where($where);
        $this->db->order_by('student_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    function get_parent_by_user_id($user_id) {
        $where = array('users.id' => $user_id);
        $this->db->select('family_name,first_name,last_name,mother_name,father_name,balance,user_id,parents.id as id,address,email,home_phone,father_phone,mother_phone,non_encrept_pass,email2,'
                . 'emergency_name_1 , emergency_name_2 , emergency_name_3 ,  '
                . 'emergency_phone_1, emergency_phone_2,  emergency_phone_3, '
                . 'emergency_relationship_1, emergency_relationship_2,  emergency_relationship_3, '
                . 'custody_info_specify   , custody_info_other_info');
        $this->db->join('users', 'users.id = parents.user_id');
        $result = $this->get_by($where);

        return $result;
    }

    function get_class_parents_for_specific_class($grade_id) {
        $where = array(
            'is_active' => 2,
            'grade_id' => $grade_id,
        );
        $this->db->select('is_active,grade_id,grade_name,family_name,parents.user_id as user_id,father_name,mother_name,email,email2');
        $this->db->join('students', 'students.parent_id=parents.id');
        $this->db->join('students_records', 'students_records.student_id=students.id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $result = $this->get_all_crud();

        if (isset($result) && $result)
            return $result;
        else
            return false;
    }

    function get_parents_for_active_students() {
        $where = array(
            'students_records.year' => $this->_archive_year,
            'is_active' => 2,
        );
        $this->db->select('students_records.year,is_active,grade_id,grade_name,family_name,parents.user_id as user_id,first_name,last_name,father_name,mother_name');
        $this->db->join('students', 'students.parent_id=parents.id');
        $this->db->join('students_records', 'students_records.student_id=students.id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $result = $this->get_all_crud();

        return $result;
    }

    function count_all_parent_children($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->select('student_name,family_name,parent_id,students.id as student_id');
        $this->db->join('students', 'students.parent_id=parents.id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_parent_children($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->select('student_name,family_name,parent_id,students.id as student_id');
        $this->db->join('students', 'students.parent_id=parents.id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    function count_all_parents() {
        $where = array('type' => 'parent');
        $this->db->select('*, parents.id as id ');
        $this->db->join('users', 'users.id = parents.user_id');
        $this->db->where($where);
        $result = $this->count_all();
        return $result;
    }

    function count_filtered_crud_parents() {
        $where = array('type' => 'parent');
        $this->db->select('*, parents.id as id ');
        $this->db->join('users', 'users.id = parents.user_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
