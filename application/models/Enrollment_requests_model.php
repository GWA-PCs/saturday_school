<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Enrollment_requests_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'enrollment_requests';
    var $column_order = array('student_1',); //set column field database for datatable orderable
    var $column_search = array('student_1',); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'enrollment_requests_m');
        $this->enrollment_requests_m->set_table('enrollment_requests');
    }

    function get_enrollment_requests() {
        $result = $this->get_all_crud();
        return $result;
    }

    function count_all_enrollment_requests() {
        $where = array();
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_enrollment_requests() {
        $result = $this->count_filtered_crud();
        return $result;
    }

}
