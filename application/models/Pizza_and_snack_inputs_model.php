<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pizza_and_snack_inputs_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'pizza_and_snack_inputs';
    var $column_order = array('input_date', 'input_title', 'cost', 'withdrawal', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('input_date', 'input_title', 'cost', 'deposit', 'withdrawal'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('pizza_and_snack_inputs.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'pizza_and_snack_inputs_m');
        $this->pizza_and_snack_inputs_m->set_table('pizza_and_snack_inputs');
    }

    public function get_all_output_pizza_and_snack_inputs() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('*, pizza_and_snack_inputs.id as id');
        $this->db->where($where);
        $this->db->order_by('id', 'desc');
        $result = $this->get_all_crud();
        return $result;
    }

    public function count_all_output_pizza_and_snack_inputs() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('*, pizza_and_snack_inputs.id as id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_output_pizza_and_snack_inputs() {
        $where = array('year' => $this->_archive_year,);
        $this->db->select('*, pizza_and_snack_inputs.id as id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
