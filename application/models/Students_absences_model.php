<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_absences_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'students_absences';
    var $column_order = array('absence_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('absence_date'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'students_absences_m');
        $this->students_absences_m->set_table('students_absences');
    }

    function get_student_absences($student_record_id) {
        $where = array('student_record_id' => $student_record_id,);
        $this->db->where($where);
        $result = $this->get_all_crud();

        return $result;
    }

}
