<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Groups_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'groups';
    var $column_order = array('name', 'description', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('name', 'description'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'groups_m');
        $this->groups_m->set_table('groups');

    }


    function get_groups() {
        $result = $this->get_all_crud();
        return $result;
    }

    function get_group_by_id($id) {
        $where = array('id' => $id);
        $result = $this->groups_m->get_by($where);
        return $result;
    }

    function get_groups_by_user_id($user_id) {
        $where = array(
            'user_id' => $user_id,
        );
        $this->db->join('users_groups', 'users_groups.group_id = groups.id');
        $resutls = $this->groups_m->get_many_by($where);
        return $resutls;
    }

}
