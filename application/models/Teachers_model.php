<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'teachers';
    var $column_order = array('', '', '', '', null); //set column field database for datatable orderable
    var $column_search = array('first_name', 'last_name', 'mobile', 'assistant_teacher'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('teachers.id' => 'desc'); // default order

//    public $tables = 'users as users_info';

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'teachers_m');
        $this->teachers_m->set_table('teachers');
        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');

        $this->load->model('general_model', 'rooms_teachers_m');
        $this->rooms_teachers_m->set_table('rooms_teachers');
    }

    function get_teachers() {
        $where = array('users.type' => 'teacher');
        $this->db->select('*, users.first_name, users.last_name, users.father_name, users.mother_name, users.username, teachers.id as id,user_id');
        $this->db->where($where);
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->order_by('users.first_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    function update_teacher_by_id($item_id, $user_data, $teacher_data) {
        $where = array('id' => $item_id);
        $result = $this->teachers_m->update_by($where, $teacher_data);

        $row_teacher = $this->teachers_m->get_by(array('id' => $item_id));
        $where = array('id' => $row_teacher->user_id);
        $user_result = $this->users_m->update_by($where, $user_data);

        $update = $this->ion_auth->change_password($user_data['email'], $user_data['password'], $this->input->post('new'));

        if (isset($result) && $result && (isset($user_result) && $user_result))
            return TRUE;
        else
            return FALSE;
    }

    function get_teacher_by_id($id) {
        $where = array('teachers.id' => $id);
        $this->db->select('*,teachers.id as id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $result = $this->teachers_model->get_by($where);
        return $result;
    }

    function delete_teacher($id) {
        $where = array('id' => $id);
        $teacher_info = $this->teachers_m->get_by($where);
        $user_id = $teacher_info->user_id;

        $sql = 'DELETE   `teachers`,`users_groups`,`users`
            FROM     `teachers`,`users_groups`,`users`
            WHERE   `users`        .`id`       = ' . $user_id . '
            AND     `teachers`   .`id` = ' . $id . ' 
            AND     `users_groups`.`user_id`  = ' . $user_id;

        $result = $this->db->query($sql);
        return $result;
    }

    function get_teachers_for_auto_complete($query) {
        $this->db->select('*,teachers.id as teacher_id');
        $this->db->join('users', 'users.id=teachers.user_id');
        $this->db->group_start();
        $this->db->like('first_name', $query);
        $this->db->or_like('last_name', $query);
        $this->db->group_end();
        $this->db->limit(20);
        $result = $this->get_all();
        return $result;
    }

    function get_teacher_room($teacher_id, $stage_id = '') {
        if ($stage_id == '') {
            $where = array(
                'teachers.id' => $teacher_id,
            );
        } else {
            $where = array(
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        }

        $this->db->select('*,rooms.id as id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->where($where);
        $this->db->group_by('grade_id');
        $results = $this->get_all_crud();

        return $results;
    }

    function get_teacher_id_by_user_id($user_id) {
        $where = array('users.id' => $user_id);
        $this->db->select('teachers.id as teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $result = $this->get_by($where);
        return $result->teacher_id;
    }

    function get_students_teacher($teacher_id, $stage_id = '') {

        if ($stage_id != '') {
            $where = array(
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        } else {
            $where = array(
                'teachers.id' => $teacher_id,
            );
        }
        $this->db->select('student_name,family_name,students_records.id as student_record_id,students_records.room_id as room_id,grade_id,teacher_id,parent_id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('students', 'students.id=students_records.student_id');

        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->where($where);
        $results = $this->get_all_crud();

        return $results;
    }

    function get_teacher_by_user_id($user_id) {
        $where = array('users.id' => $user_id);
        $this->db->join('users', 'users.id = teachers.user_id');
        $result = $this->get_by($where);
        return $result;
    }

    function get_teacher_room_by_user_id($user_id) {
        $where = array(
            'teachers.user_id' => $user_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('rooms.grade_id as id,grade_name,first_name,last_name');
        $this->db->join('users', ' users.id = teachers.user_id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->group_by('rooms.grade_id');
        $results = $this->get_many_by($where);
        return $results;
    }

    function get_teachers_for_parent_children($parent_id) {
        $where = array(
            'students.parent_id' => $parent_id,
            'is_active' => 2,
        );
        $this->db->select('teachers.user_id as user_id,first_name,last_name,student_name,students_records.id as student_record_id,students_records.room_id as room_id,grade_id,teacher_id,parent_id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('users', ' users.id = teachers.user_id');
        $this->db->group_by('student_record_id');
//        $this->db->join('parents', 'parents.id=students.parent_id');
        $results = $this->get_many_by($where);
        return $results;
    }

    function count_all_teacher_room($teacher_id, $stage_id = '') {
        if ($stage_id == '') {
            $where = array(
                'teachers.id' => $teacher_id,
            );
        } else {
            $where = array(
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        }

        $this->db->select('*,rooms.id as id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->where($where);
        $this->db->group_by('grade_id');
        $results = $this->count_by($where);

        return $results;
    }

    function count_filtered_crud_teacher_room($teacher_id, $stage_id = '') {
        if ($stage_id == '') {
            $where = array(
                'teachers.id' => $teacher_id,
            );
        } else {
            $where = array(
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        }

        $this->db->select('*,rooms.id as id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->where($where);
        $this->db->group_by('grade_id');
        $results = $this->count_filtered_crud();

        return $results;
    }

    function count_all_teachers() {
        $where = array('users.type' => 'teacher');
        $this->db->select('*, users.first_name, users.last_name, users.father_name, users.mother_name, users.username, teachers.id as id,user_id');
//        $this->db->where($where);
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->order_by('users.first_name', 'ASC');
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_teachers() {
        $where = array('users.type' => 'teacher');
        $this->db->select('*, users.first_name, users.last_name, users.father_name, users.mother_name, users.username, teachers.id as id,user_id');
        $this->db->where($where);
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->order_by('users.first_name', 'ASC');
        $result = $this->count_filtered_crud();
        return $result;
    }

    function get_students_teacher_for_auto_complete($teacher_id, $query) {

        $where = array(
            'teachers.id' => $teacher_id,
        );
        $this->db->select('students.id as id,student_name,family_name');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->group_start();
        $this->db->like('LOWER(student_name)', strtolower($query));
        $this->db->or_like('LOWER(family_name)', strtolower($query));
        $this->db->or_like('student_name', $query);
        $this->db->or_like('family_name', $query);
        $this->db->group_end();
        $this->db->distinct('student_id');
        $this->db->limit(5);
        $this->db->where($where);
        $results = $this->get_all();

        return $results;
    }

    function get_teacher_room_for_auto_complete($teacher_id, $query) {
        $where = array(
            'teachers.id' => $teacher_id,
        );

        $this->db->select('grade_name,grades.id as id');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->group_start();
        $this->db->or_like('LOWER(grade_name)', strtolower($query));
        $this->db->or_like('grade_name', $query);
        $this->db->group_end();
        $this->db->limit(5);
        $this->db->where($where);
        $this->db->distinct('grades.id');
//        $this->db->group_by('grade_id');
        $results = $this->get_all();

        return $results;
    }

    function get_parents_for_teacher_room_by_user_id($user_id) {
        $where = array('users.id' => $user_id);
        $this->db->select('*,teachers.id as id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $teachers = $this->teachers_model->get_by($where);

        $where = array(
            'teachers.id' => $teachers->id,
            'is_active' => 2,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('parents.user_id as user_id,rooms.grade_id as id,grade_name,father_name,mother_name,family_name,student_name');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', ' parents.user_id=users.id');
        $this->db->order_by('parents.family_name');
        $this->db->group_by('parents.user_id');
        $results = $this->get_many_by($where);

        return $results;
    }

    function get_students_for_teacher_room_by_user_id($user_id) {
        $where = array('users.id' => $user_id);
        $this->db->select('*,teachers.id as id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $teachers = $this->teachers_model->get_by($where);


        $where = array(
            'teachers.id' => $teachers->id,
            'is_active' => 2,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('students.user_id as user_id,rooms.grade_id as id,grade_name,father_name,mother_name,family_name,student_name');
        $this->db->join('rooms_teachers', ' rooms_teachers.teacher_id = teachers.id');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('students_records', 'students_records.room_id=rooms.id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', ' students.user_id=users.id', 'right'); //$this->db->group_by('grade_id');
        $this->db->order_by('students.student_name');
        $this->db->order_by('parents.family_name');
        $this->db->group_by('students.user_id');
        $results = $this->get_many_by($where);

        return $results;
    }

}
