<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Honored_students_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'honored_students';
    var $column_order = array('honored_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('grade_name', 'student_name', 'family_name', 'room_name', 'family_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('honored_students.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'honored_students_m');
        $this->honored_students_m->set_table('honored_students');
    }

    public function get_honored_students($honored_order_id) {
        $where = array(
            'honored_order_id' => $honored_order_id,
            "year" => $this->_archive_year,
        );
        $this->db->where($where);
        $this->db->select('honored_students.id as id ,grade_id,grade_name,student_name,family_name,room_name,honored_date,honored_reason,students_records.room_id ,student_record_id,student_id,year');
        $this->db->join('students_records', 'students_records.id=honored_students.student_record_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('student_name');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_honored_request($id) {
        $where = array(
            'honored_students.id' => $id,
            "year" => $this->_archive_year,
        );
        $this->db->select('honored_students.id as id ,grade_id,grade_name,student_name,family_name,room_name,honored_date,honored_reason,students_records.room_id ,student_record_id,year');
        $this->db->join('students_records', 'students_records.id=honored_students.student_record_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_honored_students($honored_order_id) {
        $where = array(
            'honored_order_id' => $honored_order_id,
            "year" => $this->_archive_year,
        );
        $this->db->select('honored_students.id as id ,grade_id,grade_name,student_name,family_name,room_name,honored_date,honored_reason,students_records.room_id ,student_record_id,student_id,year');
        $this->db->join('students_records', 'students_records.id=honored_students.student_record_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('student_name');
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_honored_students($honored_order_id) {
        $where = array(
            'honored_order_id' => $honored_order_id,
            "year" => $this->_archive_year,
        );
        $this->db->where($where);
        $this->db->select('honored_students.id as id ,grade_id,grade_name,student_name,family_name,room_name,honored_date,honored_reason,students_records.room_id ,student_record_id,student_id,year');
        $this->db->join('students_records', 'students_records.id=honored_students.student_record_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('student_name');
        $result = $this->count_filtered_crud();
        return $result;
    }

}
