<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_records_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'students_records';
    var $column_order = array('student_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('room_name', 'grade_name', 'is_active', 'teacher_child', 'student_name', 'family_name', 'father_name', 'mother_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('students_records.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'students_records_m');
        $this->students_records_m->set_table('students_records');

        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');

        $this->load->model('grades_model');

        $this->load->model('general_model', 'students_m');
        $this->students_m->set_table('students');
//        $this->load->model('students_model');
    }

    function get_students_records($room_id) {
        $where = array('room_id' => $room_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('grade_type,room_name,grade_name,is_active,teacher_child,room_id,student_name,family_name,students_records.id AS id,father_name,mother_name,students_records.student_id,photo_permission_slips,islamic_book_return,status_islamic_book_return,stage_id,grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('student_name', 'ASC');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }

    function get_general_social_development($student_record_id) {
        $where = array('students_records.id' => $student_record_id,);
        $general_social_development = get_general_social_development();
        foreach ($general_social_development as $value) {
            $this->db->select("$value");
        }
        $this->db->select('student_name,family_name,students_records.id as id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $result = $this->get_by($where);
        return $result;
    }

    function get_non_active_manually_students_records($grade_id) {
        $where = array('grade_id' => $grade_id,
            'is_active' => 3,
            'year' => $this->_archive_year,
        );
        $this->db->select('year,is_active,room_id,room_name,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
//        $this->db->group_by('student_id');
        $result = $this->get_all_crud();

        return $result;
    }

    function get_students_who_did_not_give_permission($grade_id) {
        $where = array(
            'grade_id' => $grade_id,
            'photo_permission_slips' => 1,
            'year' => $this->_archive_year,
            'is_active' => 2,
        );
        $this->db->select('year,is_active,room_id,room_name,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }

    function get_students_who_did_not_return_the_books($grade_id) {
        $where = array(
            'grade_id' => $grade_id,
            'islamic_book_return' => 1,
            'year' => $this->_archive_year,
            'is_active' => 2,
        );
        $this->db->select('year,is_active,room_id,room_name,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }

    function get_number_of_students($grade_id) {
        $grade_name = $this->grades_model->get_grade_name($grade_id);

        $students_number_s1 = 0;
        $students_number_s2 = 0;
        $students_number_end_year = 0;

        $number_of_students = array();
        $where = array(
            'grade_id' => $grade_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );

        $this->db->select('year,MONTH(students_records.created_at) as created_at,is_active,room_id,room_name,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,grade_id,grade_name');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->get_many_by($where);

        if (isset($grade_id) && $grade_id) {
            $where = array(
                'grade_id' => $grade_id,
                'is_active' => 2,
                'year' => $this->_archive_year,
            );

            $this->db->select('COUNT(students_records.id) as count,student_id');
            $this->db->join('students', 'students.id=students_records.student_id');
            $this->db->join('parents', 'parents.id=students.parent_id');
            $this->db->join('users', 'users.id=parents.user_id');
            $this->db->join('rooms', 'rooms.id=students_records.room_id');
            $this->db->join('grades', 'grades.id=rooms.grade_id');
//            $this->db->group_by('student_id');
            $result_count = $this->get_by($where);


            foreach ($result as $value) {
                $created_at = $value->created_at;
                if (in_array($created_at, get_months_of_first_semester())) {
                    $students_number_s1++;
//                    var_dump('s1 : '.$students_number_s1);
                } elseif (in_array($created_at, get_months_of_second_semester())) {
                    $students_number_s2++;
//                          var_dump('s2 : '.$students_number_s2);
                }
            }
            $number_of_students = array(
                'students_number_s1' => $students_number_s1,
                'students_number_s2' => $students_number_s2,
                'students_number_end_year' => $result_count->count,
            );

            if (isset($grade_name) && $grade_name) {
                $number_of_students['grade_name'] = $grade_name->grade_name;
            } else {
                $number_of_students['grade_name'] = ' - ';
            }
        }
        return $number_of_students;
    }

    function get_number_of_students_in_all_grades() {
        $grades = $this->grades_model->get_grades();

        $number_of_students_for_all_grades = array();
        foreach ($grades as $grade) {
            $grade_id = $grade->id;
            $students_number_s1 = 0;
            $students_number_s2 = 0;
            $students_number_end_year = 0;

            $number_of_students = array();
            $where = array(
                'grade_id' => $grade_id,
                'is_active' => 2,
                'year' => $this->_archive_year,
            );

            $this->db->select('year,MONTH(students_records.created_at) as created_at,is_active,room_id,room_name,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,grade_id,grade_name');
            $this->db->join('students', 'students.id=students_records.student_id');
            $this->db->join('parents', 'parents.id=students.parent_id');
            $this->db->join('users', 'users.id=parents.user_id');
            $this->db->join('rooms', 'rooms.id=students_records.room_id');
            $this->db->join('grades', 'grades.id=rooms.grade_id');
            $result = $this->get_many_by($where);

            if (isset($grade_id) && $grade_id) {
                $where = array(
                    'grade_id' => $grade_id,
                    'is_active' => 2,
                    'year' => $this->_archive_year,
                );

                $this->db->select('COUNT(students_records.id) as count,student_id');
                $this->db->join('students', 'students.id=students_records.student_id');
                $this->db->join('parents', 'parents.id=students.parent_id');
                $this->db->join('users', 'users.id=parents.user_id');
                $this->db->join('rooms', 'rooms.id=students_records.room_id');
                $this->db->join('grades', 'grades.id=rooms.grade_id');
//            $this->db->group_by('student_id');
                $result_count = $this->get_by($where);

                foreach ($result as $value) {
                    $created_at = $value->created_at;
                    if (in_array($created_at, get_months_of_first_semester())) {
                        $students_number_s1++;
//                    var_dump('s1 : '.$students_number_s1);
                    } elseif (in_array($created_at, get_months_of_second_semester())) {
                        $students_number_s2++;
//                          var_dump('s2 : '.$students_number_s2);
                    }
                }
                $number_of_students = array(
                    'students_number_s1' => $students_number_s1,
                    'students_number_s2' => $students_number_s2,
                    'students_number_end_year' => $result_count->count,
                );
                $number_of_students['grade_name'] = $grade->grade_name;

                $number_of_students_for_all_grades[] = $number_of_students;
            }
        }
        return $number_of_students_for_all_grades;
    }

    function get_all_active_student_record($student_id) {
        $where = array(
            'is_active' => 2,
            'student_id' => $student_id,
            'year' => $this->_archive_year,
        );
        $this->db->select('grade_type,is_active,teacher_child,students_records.id as id,students_records.student_id,grade_name,grade_id,room_id,room_name,report_card_name');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
        $result = $this->get_many_by($where);
        return $result;
    }

    function get_student_record($student_id, $room_id) {
        $where = array(
            'is_active' => 2,
            'student_id' => $student_id,
            'room_id' => $room_id,
        );
        $this->db->select('students_records.id,is_active,student_id ,grade_name,grade_id,room_id,room_name');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->get_by($where);
        return $result;
    }

    function get_active_student_record($student_id) {
        $where = array(
            'is_active' => 2,
            'student_id' => $student_id,
        );
        $this->db->select('is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->get_by($where);
        return $result;
    }

    function get_student_order($student_id) {
        $where = array(
            'id' => $student_id,
        );
        $result = $this->students_m->get_by($where);

        if (isset($result) && $result) {
            $where = array(
                'parent_id' => $result->parent_id,
                'is_active' => 2,
                'year' => $this->_archive_year,
                'student_id <>' => $student_id,
            );
            $this->db->join('students_records', 'students_records.student_id=students.id');
            $this->db->group_by('student_id');
            $same_parent = count($this->students_m->get_many_by($where));

            return $same_parent;
        }
    }

    function get_student_record_by_student_record_id($student_record_id) {
        $where = array(
            'students_records.id' => $student_record_id,
            'is_active' => 2,
        );
        $this->db->select('is_active,students_records.id as student_record_id,students_records.student_id,parent_id,student_order');
        $this->db->join('students', 'students.id=students_records.student_id');
        $result = $this->get_by($where);

        return $result;
    }

    function get_all_brothers_and_sisters($student_record_id, $parent_id, $student_order) {
        $where = array(
            'is_active' => 2,
            'parent_id' => $parent_id,
            'student_order >' => $student_order,
            'students_records.id <>' => $student_record_id,
        );
        $this->db->select('is_active,students_records.id as id,student_order,parent_id,student_id');
        $this->db->join('students', 'students.id =students_records.student_id');
        $result = $this->get_many_by($where);

        return $result;
    }

    function get_all_students_records() {
        $where = array(
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('grade_type,stage_id,is_active,teacher_child,room_id,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,photo_permission_slips,islamic_book_return,status_islamic_book_return,room_name,grade_name,grade_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->where($where);
        $this->db->order_by('student_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    function count_all_students_records($room_id) {
        $where = array('room_id' => $room_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('is_active,teacher_child,room_id,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,photo_permission_slips,islamic_book_return,status_islamic_book_return,stage_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_students_records($room_id) {
        $where = array('room_id' => $room_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('is_active,teacher_child,room_id,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,photo_permission_slips,islamic_book_return,status_islamic_book_return,stage_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    function count_all_students_records_without_room() {
        $where = array(
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('stage_id,is_active,teacher_child,room_id,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,photo_permission_slips,islamic_book_return,status_islamic_book_return,room_name,grade_name');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_students_records_without_room() {
        $where = array(
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('stage_id,is_active,teacher_child,room_id,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,photo_permission_slips,islamic_book_return,status_islamic_book_return,room_name,grade_name');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    function get_student_record_info($student_record_id) {
        $where = array(
            'students_records.id' => $student_record_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,'
                . 'room_name,grade_name,room_id,grade_id,report_card_name,participates_in_class,shows_cooperative_attitude_with_peers,'
                . 'shows_cooperative_attitude_with_teachers,listens_when_others_are_speaking,respects_equipment_and_material');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $result = $this->get_by($where);

        return $result;
    }

    function get_report_name($student_record_id) {
        $where = array(
            'students_records.id' => $student_record_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );
        $this->db->select('report_card_name');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');

        $result = $this->get_by($where);

        return $result;
    }

    function get_all_students_in_year() {
        $where = array(
            'year' => $this->_archive_year,
        );

        $this->db->select('year,students_records.student_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->get_many_by($where);

        return $result;
    }

    function get_attendances_for_specific_attendance_type_date_and_grade($grade_id, $attendance_type, $attendance_date) {
        $result_array = array();

        if ($attendance_type == '0' || $attendance_date == '') {
            return $result_array;
        }
//get all students 
        $where = array(
            'year' => $this->_archive_year,
        );
        if ($this->_current_year == $this->_archive_year) {
            $where['is_active'] = 2;
        }
        if (isset($grade_id) && $grade_id != 'all') {
            $where['grade_id'] = $grade_id;
        }
        $this->db->select('students_records.created_at as created_at,year,is_active,room_id,room_name,student_name,family_name,students_records.id as id,father_name,mother_name,students_records.student_id,grade_id,grade_name');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('student_name');
        $this->db->order_by('family_name');
        $students_records = $this->get_many_by($where);

        $students_records_array = array();
        $rooms_array = array();

        foreach ($students_records as $value) {
            $students_records_array[$value->id] = $value->id;
            $rooms_array[$value->room_id] = $value->room_id;
        }

        if (isset($students_records) && $students_records) {
            //get all rooms attendances records for the rooms
            $this->db->select('*,rooms.id as id');
            $this->db->where('attendance_date', $attendance_date);
            $this->db->where_in('room_id', $rooms_array);
            $this->db->join("rooms", "rooms_attendances.room_id = rooms.id");
            $this->db->join("grades", "rooms.grade_id = grades.id");
            $attendance_rooms_records = $this->rooms_attendances_model->get_all();

            $attendance_rooms_records_array = array();
            foreach ($attendance_rooms_records as $value) {
                $attendance_rooms_records_array[] = $value->id;
            }
            $resutls = array();

            if (isset($attendance_rooms_records_array) && $attendance_rooms_records_array) {
                if ($attendance_type == 'attendance') {
                    $where = array(
                        'students_attendances.attendance_date' => $attendance_date,
                    );

                    $where1 = array(
                        'students_attendances.attendance_type' => 'delay',
                    );
                    $where2 = array(
                        'students_attendances.attendance_type' => 'absence',
                    );
                    //get all students attendances records
                    $this->db->select("students_records.created_at as created_at ,room_name,"
                            . "rooms_attendances.room_id as room_id,students_records.student_id as student_id,"
                            . " students_records.id as student_record_id,student_name,family_name");
                    $this->db->select("grade_name,students_attendances.attendance_date as attendance_date,"
                            . " students_attendances.attendance_type");
                    $this->db->join("students_attendances", "students_attendances.student_record_id=students_records.id");
                    $this->db->join("rooms_attendances", "rooms_attendances.room_id=students_records.room_id");
                    $this->db->join('students', 'students.id=students_records.student_id');
                    $this->db->join('parents', 'parents.id=students.parent_id');
                    $this->db->join("rooms", "rooms.id = students_records.room_id");
                    $this->db->join("grades", "grades.id = rooms.grade_id");
                    $this->db->group_start();
                    $this->db->where($where1);
                    $this->db->or_where($where2);
                    $this->db->group_end();
                    $this->db->where($where);
                    $this->db->where_in('students_records.id', $students_records_array);
                    $this->db->where_in('rooms_attendances.room_id', $attendance_rooms_records_array);
                    $this->db->order_by('student_name');
                    $this->db->order_by('family_name');
                    $this->db->group_by('student_record_id');
                    $resutls = $this->students_records_m->get_all();
                } else {
                    $where = array(
                        'students_attendances.attendance_date' => $attendance_date,
                        'students_attendances.attendance_type' => $attendance_type,
                    );
                    //get all students attendances records
                    $this->db->select("students_records.created_at as created_at ,room_name,"
                            . "rooms_attendances.room_id as room_id,students_records.student_id as student_id,"
                            . " students_records.id as student_record_id,student_name,family_name");
                    $this->db->select("grade_name,students_attendances.attendance_date as attendance_date,"
                            . " students_attendances.attendance_type");
                    $this->db->join("students_attendances", "students_attendances.student_record_id=students_records.id");
                    $this->db->join("rooms_attendances", "rooms_attendances.room_id=students_records.room_id");
                    $this->db->join('students', 'students.id=students_records.student_id');
                    $this->db->join('parents', 'parents.id=students.parent_id');
                    $this->db->join("rooms", "rooms.id = students_records.room_id");
                    $this->db->join("grades", "grades.id = rooms.grade_id");
                    $this->db->where($where);
                    $this->db->where_in('students_records.id', $students_records_array);
                    $this->db->where_in('rooms_attendances.room_id', $attendance_rooms_records_array);
                    $this->db->order_by('student_name');
                    $this->db->order_by('family_name');
                    $this->db->group_by('student_record_id');
                    $resutls = $this->students_records_m->get_all();
                }
            }

            $results_array_for_attendence = array();
            if (isset($resutls) && $resutls) {
                foreach ($resutls as $value) {
                    $results_array_for_attendence[] = $value->student_record_id;
                }
            }

            if ($attendance_type == 'attendance') {
                if (isset($students_records) && $students_records) {
                    foreach ($students_records as $value) {
                        $registration_date = new DateTime($value->created_at);
                        $registration_date = $registration_date->format('Y-m-d');

                        if (!in_array($value->id, $results_array_for_attendence) && ($registration_date <= $attendance_date)) {
                            $res = array();
                            $res['attendance_date'] = $attendance_date;
                            $res['room_name'] = $value->room_name;
                            $res['attendance_type'] = $attendance_type;
                            $res['student_name'] = $value->student_name .' '. $value->family_name;
                            $res['grade_name'] = $value->grade_name;
                            $result_array[][] = $res;
                        }
                    }
                }
            } else {
                if (isset($resutls) && $resutls) {
                    foreach ($resutls as $value) {
                        $res = array();
                        $res['attendance_date'] = $value->attendance_date;
                        $res['room_name'] = $value->room_name;
                        $res['attendance_type'] = $value->attendance_type;
                        $res['student_name'] = $value->student_name  .' '.$value->family_name;
                        $res['grade_name'] = $value->grade_name;
                        $result_array[][] = $res;
                    }
                }
            }
        }

        return $result_array;
    }

}
