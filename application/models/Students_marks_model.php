<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_marks_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'students_marks';
    var $column_order = array('mark_key', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('mark_key','student_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'students_marks_m');
        $this->students_marks_m->set_table('students_marks');

        $this->load->model('students_records_model');
    }

    function get_students_marks($semester, $room_id, $subject_id) {
        $students = $this->students_records_model->get_students_records($room_id);

        $where = array(
            'year' => $this->_archive_year,
            'room_id' => $room_id,
            'subject_id' => $subject_id,
            'semester' => $semester,
        );
        $this->db->select('*,students_marks.id as students_marks_id, students_records.id as student_record_id');
        $this->db->join('students_marks', ' students_marks.student_record_id =  students_records.id');
        $this->db->where($where);
        $marks = $this->students_records_m->get_all();

        foreach ($students as $stu) {
            foreach ($marks as $mark) {
                if ($stu->id == $mark->student_record_id) {
                    $stu->mark_key = $mark->mark_key;
                    $stu->mark_note = $mark->mark_note;
                    $stu->students_marks_id = $mark->students_marks_id;
                }
            }
        }

        $result = $students;

        return $result;
    }

    function get_all_student_marks($semester, $room_id, $student_record_id) {

        $where = array(
            'room_id' => $room_id,
            'semester' => $semester,
            'student_record_id' => $student_record_id,
        );
        $this->db->select('*,students_marks.id as students_marks_id, students_records.id as student_record_id');
        $this->db->join('students_records', ' students_records.id = students_marks.student_record_id');
        $marks = $this->get_many_by($where);

        return $marks;
    }

//    function count_all_students_marks($semester, $room_id, $subject_id) {
//        $students = $this->students_records_model->get_students_records($room_id);
//
//        $where = array(
//            'year' => $this->_archive_year,
//            'room_id' => $room_id,
//            'subject_id' => $subject_id,
//            'semester' => $semester,
//        );
//        $this->db->select('*,students_marks.id as students_marks_id, students_records.id as student_record_id');
//        $this->db->join('students_marks', ' students_marks.student_record_id =  students_records.id');
//        $this->db->where($where);
//        $marks = $this->students_records_m->get_all();
//
//        foreach ($students as $stu) {
//            foreach ($marks as $mark) {
//                if ($stu->id == $mark->student_record_id) {
//                    $stu->mark_key = $mark->mark_key;
//                    $stu->mark_note = $mark->mark_note;
//                    $stu->students_marks_id = $mark->students_marks_id;
//                }
//            }
//        }
//
//        $result = $students;
//
//        return $result;
//    }

}
