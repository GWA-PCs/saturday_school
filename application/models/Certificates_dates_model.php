<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Certificates_dates_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'certificate_dates';
    var $column_order = array('', '', '', '', null); //set column field database for datatable orderable
    var $column_search = array('grade_name', 'date_of_expired'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('certificate_dates.id' => 'desc'); // default order

//    public $tables = 'users as users_info';

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'certificates_dates_m');
        $this->certificates_dates_m->set_table('certificates_dates');

        $this->load->model('general_model', 'grades_m');
        $this->grades_m->set_table('grades');
    }

    function get_certificates_dates() {
        $where = array(
            'year' => $this->_archive_year,
        );
        $this->db->select('grade_id,year,date_of_expired,id');
        $this->db->where($where);
        $result = $this->get_all();
        return $result;
    }

}
