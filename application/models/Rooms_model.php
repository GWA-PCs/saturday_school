<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'rooms';
    var $column_order = array('room_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('room_name',); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('rooms.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'rooms_m');
        $this->rooms_m->set_table('rooms');

        $this->load->model('rooms_teachers_model');
        $this->load->model('students_records_model');
    }

    public function get_rooms($grade_id = '') {
        if ($grade_id != '') {
            $where = array('grade_id' => $grade_id);
            $this->db->where($where);
        }
        $this->db->select('*,rooms.id as id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('room_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_any_teachers($room_id) {
        $where = array(
            'room_id' => $room_id,
        );
        $results = $this->rooms_teachers_model->get_by($where);
        return $results;
    }

    public function get_any_students_records($room_id) {
        $where = array(
            'room_id' => $room_id,
            'is_active'=> 2,
        );
        $this->db->join('students','students.id= students_records.student_id');
        $results = $this->students_records_model->get_by($where);
//        var_dump($results);
//        die;
        return $results;
    }

    public function delete_rooms($id) {
        $result = $this->rooms_m->delete_by('id', $id);
        return $result;
    }

    public function insert_rooms($data) {
        $result = $this->rooms_m->insert($data);
        return $result;
    }

    public function update_by_id($item_id, $data) {
        $result = $this->rooms_m->update_by(array('id' => $item_id), $data);
        return $result;
    }

    public function get_room($room_id) {
        $where = array('rooms.id' => $room_id);
        $this->db->select('*,rooms.id as id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_rooms($grade_id) {
        $where = array('grade_id' => $grade_id);
        $this->db->select('*,rooms.id as id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_rooms($grade_id) {
        $where = array('grade_id' => $grade_id);

        $this->db->select('*,rooms.id as id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud($where);
        return $result;
    }

    public function get_rooms_for_grade_type($grade_type) {
        $where = array('grade_type' => $grade_type);
        $this->db->where($where);
        $this->db->select('*,rooms.id as id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->order_by('room_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

}
