<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_items_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'payment_items_m');
        $this->payment_items_m->set_table('payment_items');
    }

    function get_tuition() {
        $result = array();
        $payment_items = $this->payment_items_m->get_all();

        foreach ($payment_items as $pay_key => $pay_value) {
            $result[$pay_value->payment_key] = $pay_value->payment_value;
        }
        return $result;
    }

    function update_tuition($data) {
        foreach ($data as $key => $value) {
            $where = array(
                'payment_key' => $key
            );
            $res = $this->payment_items_m->update_by($where, array('payment_value' => $value));
        }
        return $res;
    }
    
    
    function get_tuition_by_key($key) {
        $result = $this->payment_items_m->get_by('payment_key',$key);
        return $result->payment_value;
    }

}
