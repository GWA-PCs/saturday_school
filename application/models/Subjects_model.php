<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = array('subjects');
    var $column_order = array('subject_name', 'parent_subject_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('subject_name', 'parent_subject_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('subjects.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'subjects_m');
        $this->subjects_m->set_table('subjects');
    }

    public function get_subjects($grade_id) {
        $where = array('grade_id' => $grade_id);
        $this->db->select('*,subjects.id as id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $this->db->order_by('subject_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_subjects_room($room_id) {
        $where = array('rooms.id' => $room_id);
        $this->db->select('*,subjects.id as id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('rooms', 'rooms.grade_id=grades.id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $result = $this->get_many_by($where);
        return $result;
    }

    function get_teacher_subjects($teacher_id, $room_id) {
        $where = array(
            'rooms_teachers.teacher_id' => $teacher_id,
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->join('rooms_teachers', ' rooms_teachers.subject_id = subjects.id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('rooms', 'rooms.grade_id=grades.id ');
//        $this->db->join('subjects', 'subjects.grade_id=grades.id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $this->db->group_by('subjects.id');
        $results = $this->get_all_crud();

        return $results;
    }

    public function get_subject($subject_id) {
        $where = array('id' => $subject_id);
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_subjects($grade_id) {
        $where = array('grade_id' => $grade_id);
        $this->db->select('*,subjects.id as id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_subjects($grade_id) {
        $where = array('grade_id' => $grade_id);
        $this->db->select('*,subjects.id as id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    public function get_subjects_except_arabic_and_its_children($grade_id) {
        $where = array(
            'grade_id' => $grade_id,
            'parent_subject_id' => '1');
        $this->db->select('*,subjects.id as id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $this->db->order_by('subject_name', 'ASC');
        $result = $this->get_all();
        return $result;
    }

    public function get_children_subjects_of_arabic($grade_id) {
        $where = array(
            'grade_id' => $grade_id,
            'parent_subject_id' => '2');
        $this->db->select('*,subjects.id as id,');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $result = $this->get_all();
        return $result;
    }
    
    public function count_get_children_subjects_of_arabic($grade_id) {
        $where = array(
            'grade_id' => $grade_id,
            'parent_subject_id' => '2');
        $this->db->select('subject_name,COUNT(subjects.id) as count,');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $result = $this->get_by($where);
        return $result;
    }

}
