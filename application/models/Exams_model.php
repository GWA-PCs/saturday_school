<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Exams_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'exams';
    var $column_order = array('exam_type', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('exam_type'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'exams_m');
        $this->exams_m->set_table('exams');
    }

    function get_exams($course_id) {
        $where = array('course_id' => $course_id,);
        $this->db->where($where);
        $result = $this->get_all_crud();

        return $result;
    }

}
