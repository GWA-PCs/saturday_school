<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Types_payments_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'types_payments';
    var $column_order = array('name_payment', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('name_payment'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('types_payments.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'types_payments_m');
        $this->types_payments_m->set_table('types_payments');
    }

    function get_type_payment() {
        $result = $this->get_all_crud();
        return $result;
    }

    function get_type_payment_array() {
        $types_payments = $this->types_payments_model->get_all_crud();
        $types_payments_array = array();
        foreach ($types_payments as $key => $value) {
            $types_payments_array[$value->id] = $value->name_payment;
        }
        $result = $types_payments_array;
        return $result;
    }

    function delete_type_payment($id) {
        $result = $this->types_payments_m->delete_by('id', $id);
        return $result;
    }

    function get_type_payment_by_id($id) {
        $where = array('id' => $id);
        $result = $this->types_payments_m->get_by($where);
        return $result;
    }

    function update_type_payment_by_id($item_id, $data) {
        $where = array('id' => $item_id);
        $result = $this->types_payments_m->update_by($where, $data);

        if (isset($result) && $result)
            return TRUE;
        else
            return FALSE;
    }

    public function insert_type_payment($data) {
        $result = $this->types_payments_m->insert($data);
        return $result;
    }

}
