<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_allergies_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'students_allergies';
    var $column_order = array('allergy_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('allergy_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'students_allergies_m');
        $this->students_allergies_m->set_table('students_allergies');
    }

    function get_student_allergies($student_record_id) {
        $where = array('student_record_id' => $student_record_id,);
        $this->db->where($where);
        $result = $this->get_all_crud();

        return $result;
    }

    function get_all_allergies($student_id) {
        $where = array(
            'students.id' => $student_id,
            'is_active' => 2,
            'year' => $this->_archive_year,
        );

        $this->db->join("students_records", "students_records.student_id = students.id");
        $this->db->join("students_allergies", "students_records.id = students_allergies.student_record_id");
        $this->db->join("rooms", "students_records.room_id = rooms.id");
        $this->db->join("grades", "rooms.grade_id = grades.id");
        $this->db->group_by("student_record_id");
        $results = $this->students_model->get_many_by($where);


        $res_array = array();
        foreach ($results as $item) {
            $where = array(
                'student_record_id' => $item->student_record_id,
            );
            $this->db->limit(4);
            $result = $this->students_allergies_m->get_many_by($where);
            $result[0]->grade_name = $item->grade_name;
            $result[0]->room = $item->room_name;
            $res_array[$item->student_record_id] = $result;
        }

        return $res_array;
    }

    function count_all_students_allergies($student_record_id) {
        $where = array('student_record_id' => $student_record_id,);
        $result = $this->count_by($where);

        return $result;
    }

    function count_filtered_crud_students_allergies($student_record_id) {
        $where = array('student_record_id' => $student_record_id,);
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

}
