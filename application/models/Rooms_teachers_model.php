<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms_teachers_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'rooms_teachers';
    var $column_order = array('room_id', 'teacher_id', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('first_name', 'father_name', 'last_name', 'grade_name', 'room_name', 'subject_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('room_id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'rooms_teachers_m');
        $this->rooms_teachers_m->set_table('rooms_teachers');
        $this->load->model('teachers_model');
    }

    function get_rooms_teachers($room_id) {
        $where = array(
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');

        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $this->db->order_by('first_name', 'ASC');
        $room_teachers = $this->get_all_crud();

        return $room_teachers;
    }

    function get_all_room_teachers($room_id) {
        $where = array(
            'room_id' => $room_id,
        );
        $resutls = $this->rooms_teachers_m->get_many_by($where);
        return $resutls;
    }

    function get_all_rooms_teachers_for_all_room() {
        $where = array(
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id ,grade_name,room_name, rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $this->db->order_by('first_name', 'ASC');
        $room_teachers = $this->get_all_crud();

        return $room_teachers;
    }

    function get_all_rooms_teachers_for_all_room_grouped_by_room_id() {
        $where = array(
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id ,grade_name,room_name, rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $this->db->order_by('first_name', 'ASC');
        $this->db->order_by('last_name', 'ASC');
        $this->db->group_by("room_id");

        $room_teachers = $this->get_all_crud();

        return $room_teachers;
    }

    function count_all_rooms_teachers($room_id) {
        $where = array(
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');

        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $room_teachers = $this->count_by($where);

        return $room_teachers;
    }

    function count_filtered_crud_rooms_teachers($room_id) {
        $where = array(
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');

        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $room_teachers = $this->count_filtered_crud();

        return $room_teachers;
    }

    function count_all_rooms_teachers_for_all_room() {
        $where = array(
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id ,grade_name,room_name, rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $room_teachers = $this->count_by($where);

        return $room_teachers;
    }

    function count_filtered_crud_rooms_teachers_for_all_room() {
        $where = array(
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id ,grade_name,room_name, rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $this->db->join('teachers', 'teachers.id = rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->where($where);
        $room_teachers = $this->count_filtered_crud();

        return $room_teachers;
    }

    function get_teacher_room($teacher_id, $stage_id = '') {
        if ($stage_id == '') {
            $where = array(
                'rooms_teachers.year' => $this->_archive_year,
                'teachers.id' => $teacher_id,
            );
        } else {
            $where = array(
                'rooms_teachers.year' => $this->_archive_year,
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        }

        $this->db->select('year,rooms.id as id  , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->where($where);
        $this->db->group_by('rooms.grade_id');
        $results = $this->get_all_crud();

        return $results;
    }

    function count_all_teacher_room($teacher_id, $stage_id = '') {
        if ($stage_id == '') {
            $where = array(
                'rooms_teachers.year' => $this->_archive_year,
                'teachers.id' => $teacher_id,
            );
        } else {
            $where = array(
                'rooms_teachers.year' => $this->_archive_year,
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        }

        $this->db->select('year,rooms.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->where($where);
        $this->db->group_by('rooms.grade_id');
        $results = $this->count_by($where);

//        var_dump($results);
//        die;
        return $results;
    }

    function count_filtered_crud_teacher_room($teacher_id, $stage_id = '') {
        if ($stage_id == '') {
            $where = array(
                'rooms_teachers.year' => $this->_archive_year,
                'teachers.id' => $teacher_id,
            );
        } else {
            $where = array(
                'rooms_teachers.year' => $this->_archive_year,
                'teachers.id' => $teacher_id,
                'stage_id' => $stage_id,
            );
        }

        $this->db->select('year,rooms.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $this->db->where($where);
        $this->db->group_by('rooms.grade_id');
        $results = $this->count_filtered_crud();

        return $results;
    }

    function get_teacher_subjects($teacher_id, $room_id) {
        $where = array(
            'rooms_teachers.teacher_id' => $teacher_id,
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id ');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('rooms', 'rooms.grade_id=grades.id ');
//        $this->db->join('subjects', 'subjects.grade_id=grades.id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $this->db->group_by('subjects.id');
        $results = $this->get_all_crud();

        return $results;
    }

    function count_all_teacher_subjects($teacher_id, $room_id) {
        $where = array(
            'rooms_teachers.teacher_id' => $teacher_id,
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id ');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('rooms', 'rooms.grade_id=grades.id ');
//        $this->db->join('subjects', 'subjects.grade_id=grades.id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->group_by('subjects.id');
        $results = $this->count_by($where);

        return $results;
    }

    function count_filtered_crud_teacher_subjects($teacher_id, $room_id) {
        $where = array(
            'rooms_teachers.teacher_id' => $teacher_id,
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('subjects', 'subjects.id=rooms_teachers.subject_id');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id ');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('grades', 'grades.id=subjects.grade_id');
        $this->db->join('rooms', 'rooms.grade_id=grades.id ');
//        $this->db->join('subjects', 'subjects.grade_id=grades.id');
        $this->db->join('parent_subjects', 'parent_subjects.id=subjects.parent_subject_id');
        $this->db->where($where);
        $this->db->group_by('subjects.id');
        $results = $this->count_filtered_crud();

        return $results;
    }

    function get_if_teacher_subject($subject_id) {
        $where = array(
            'subject_id' => $subject_id,
            'rooms_teachers.year' => $this->_archive_year,
        );
        $this->db->select('year,rooms.id as id  , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('stages', 'stages.id=grades.stage_id');
        $results = $this->get_by($where);
        return $results;
    }

    function get_teacher_of_arabic_subject($room_id) {
        $where = array('rooms_teachers.year' => $this->_archive_year,
            'room_id' => $room_id,
            'assistant_teacher' => '1',
        );
        $this->db->select('teachers.user_id as user_id,assistant_teacher,parent_subject_name,year,rooms.id as id  , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,parent_subject_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $this->db->where('lower(parent_subject_name)', 'arabic');

        $results = $this->get_all();
        return $results;
    }

    function get_teacher_of_quran_subject($room_id) {
        $where = array('rooms_teachers.year' => $this->_archive_year,
            'room_id' => $room_id,
            'assistant_teacher' => '1'
        );
        $this->db->select('teachers.user_id as user_id,parent_subject_name,year,rooms.id as id  , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,parent_subject_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $this->db->where('lower(subject_name)', 'quran');
        $results = $this->get_all();
        return $results;
    }

    function get_teacher_of_islamic_studies_subject($room_id) {
        $where = array('rooms_teachers.year' => $this->_archive_year,
            'room_id' => $room_id,
            'assistant_teacher' => '1',
        );
        $this->db->select('teachers.user_id as user_id,parent_subject_name,year,rooms.id as id  , rooms_teachers.teacher_id as teacher_id , first_name, father_name, last_name,subject_id,subject_name,parent_subject_id,room_id,parent_subject_name');
        $this->db->join('teachers', 'teachers.id=rooms_teachers.teacher_id');
        $this->db->join('users', 'users.id = teachers.user_id');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $this->db->where_in('lower(subject_name)', array('islamic studies', 'islamic study'));
        $results = $this->get_all();
        return $results;
    }

    function get_room_teacher_info_by_room_id_and_teacher_id($room_id, $teacher_id) {
        $where = array(
            'room_id' => $room_id,
            'rooms_teachers.year' => $this->_archive_year,
            'teacher_id' => $teacher_id
        );
        $this->db->select('year,rooms_teachers.id as id , rooms_teachers.teacher_id as teacher_id , subject_id,subject_name,parent_subject_name,parent_subject_id,room_id,grade_name,room_name');
        $this->db->join('subjects', 'subjects.id = rooms_teachers.subject_id ');
        $this->db->join('parent_subjects', 'parent_subjects.id = subjects.parent_subject_id ');
        $this->db->join('rooms', 'rooms.id=rooms_teachers.room_id ');
        $this->db->join('grades', 'grades.id=rooms.grade_id ');
        $room_teacher = $this->get_by($where);

        return $room_teacher;
    }

}
