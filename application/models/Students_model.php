<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'students';
    var $column_order = array('student_name', 'age', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('student_name', 'email', 'student_notes', 'family_name', 'age', 'gender'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('students.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();

        $this->load->model('general_model', 'students_m');
        $this->students_m->set_table('students');

        $this->load->model('general_model', 'parents_m');
        $this->parents_m->set_table('parents');

        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');

        $this->load->model('general_model', 'students_attendances_m');
        $this->students_attendances_m->set_table('students_attendances');

        $this->load->model("students_records_model");
        $this->load->model("rooms_attendances_model");
    }

    function get_students() {
        $this->db->select('*,family_name,students.id as id,users.email as email');
        $this->db->join('users', 'users.id = students.user_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->order_by('student_name', 'ASC');
        $result = $this->get_all_crud();

        return $result;
    }

    function delete_student($id) {
        $where = array('id' => $id);
        $student_info = $this->students_m->get_by($where);

        $sql = 'DELETE   `students`,`users_groups`,`users`
            FROM     `students`,`users_groups`,`users`
            WHERE   `users`        .`id`       = ' . $student_info->user_id . '
            AND     `students`   .`user_id` = ' . $student_info->user_id . ' 
            AND     `users_groups`.`user_id`  = ' . $student_info->user_id;
        $result = $this->db->query($sql);
        return $result;
    }

    function get_student_by_id($id) {
        $where = array('students.id' => $id);
        $this->db->select('*,students.user_id as user_id,students.id as id,parents.user_id as user_parent_id');
        $this->db->join('users', 'users.id = students.user_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $result = $this->students_model->get_by($where);
        return $result;
    }

    function update_student_by_id($item_id, $user_data, $student_data) {
        $where = array('id' => $item_id);
        $result = $this->students_m->update_by($where, $student_data);

        $where = array('id' => $item_id);
        $student_info = $this->students_m->get_by($where);

        $where = array('id' => $student_info->user_id);
        $user_result = $this->users_m->update_by($where, $user_data);

        if (isset($result) && $result && (isset($user_result) && $user_result))
            return TRUE;
        else
            return FALSE;
    }

    function get_student_by_user_id($user_id) {
        $where = array('users.id' => $user_id);
        $this->db->select('*,users.id as id');
        $this->db->join('users', 'users.id = students.user_id');
        $result = $this->students_model->get_by($where);
        return $result;
    }

    function get_students_for_auto_complete($query) {
        $this->db->select('*,students.id as student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->group_start();
        $this->db->like('student_name', $query);
        $this->db->or_like('family_name', $query);
        $this->db->group_end();
        $this->db->limit(20);
        $result = $this->get_all();
        return $result;
    }

    function get_student_by_student_id($student_id) {
        $where = array('id' => $student_id);
        $result = $this->students_m->get_by($where);
        return $result;
    }

    function get_student_info($student_id) {
        $where = array('students.id' => $student_id);
        $this->db->select('students.id as id,parent_id, student_name, grade, age,gender, email, family_name, mother_phone, father_phone, home_phone, father_name, mother_name,parents.user_id as user_parent_id ');
        $this->db->join('parents', 'parents.id = students.parent_id');
        $this->db->join('users', 'parents.user_id = users.id');
        $result = $this->students_model->get_by($where);
        return $result;
    }

    function get_student_records($student_id) {
        $where = array(
            'students.id' => $student_id,
            'year' => $this->_archive_year,
//            'is_active' => 2,
        );
        $this->db->select("student_name, age, grade_name , room_name, is_active, photo_permission_slips, islamic_book_return,status_islamic_book_return,stage_id,non_active_registration_date,	teacher_child");
        $this->db->join("students_records", "students_records.student_id = students.id");
        $this->db->join("rooms", "students_records.room_id = rooms.id");
        $this->db->join("grades", "rooms.grade_id = grades.id");
        $res = $this->students_m->get_many_by($where);

        return $res;
    }

    function get_attendances($student_id) {
        $result_array = array();
        //get all students rooms ids if there is more than one
        $where = array(
            'student_id' => $student_id,
            'year' => $this->_archive_year,
            'is_active' => 2,
        );
        $this->db->select("student_id, room_id,id,created_at");
        $rooms_ids = $this->students_records_model->get_many_by($where);


        $student_record_id = array();
        foreach ($rooms_ids as $value) {
            $student_record_id[$value->id] = $value->id;

            $date = new DateTime($value->created_at);
            $date = $date->format('Y-m-d');
            $student_record_id[$value->room_id] = $date;
        }


        if (isset($rooms_ids) && $rooms_ids) {
            //get all rooms attendances records for the rooms
            $this->db->group_start();
            $this->db->where('room_id', $rooms_ids[0]->room_id);
            for ($i = 1; $i < count($rooms_ids); $i++) {
                $this->db->or_where('room_id', $rooms_ids[$i]->room_id);
            }
            $this->db->group_end();

            $this->db->join("rooms", "rooms_attendances.room_id = rooms.id");
            $this->db->join("grades", "rooms.grade_id = grades.id");
            $this->db->order_by('attendance_date');
            $attendance_rooms_records = $this->rooms_attendances_model->get_all();

            //get all students attendances records
            $this->db->select("students_records.student_id as student_id, students_records.id as students_record_id");
            $this->db->select("grade_name, attendance_date, attendance_type");
            $this->db->join("students_records", "students_records.id = students_attendances.student_record_id");
            $this->db->join("rooms", "rooms.id = students_records.room_id");
            $this->db->join("grades", "grades.id = rooms.grade_id");
            $this->db->where_in('student_record_id', $student_record_id);
            $resutls = $this->students_attendances_m->get_all();

            $attendance_student_array = array();
            foreach ($resutls as $record) {
                $attendance_student_array[$record->attendance_date] = $record;
                $attendance_student_array[$record->students_record_id] = $record;
            }

            //compare and get the final attendances record
            foreach ($attendance_rooms_records as $item) {

                $res = array();
                $res['data'] = date("m-d-Y", strtotime($item->attendance_date));
                $res['type'] = lang("attend");
                $res['grade'] = $item->grade_name;
                $res['room'] = $item->room_name;


                if (isset($attendance_student_array[$item->attendance_date])) {
                    if ($attendance_student_array[$item->attendance_date]->attendance_type == '')
                        $res['type'] = lang('non_status_saved');
                    else
                        $res['type'] = lang($attendance_student_array[$item->attendance_date]->attendance_type);
                }
                if ($student_record_id[$item->room_id] > $item->attendance_date) {
                    $res['type'] = lang("non_active");
                }
                if (isset($result_array[$item->grade_name])) {
                    $result_array[$item->grade_name][] = $res;
                } else {
                    $result_array[$item->grade_name][] = $res;
                }
            }
        }

        return $result_array;
    }

    function get_student_records_with_limit($student_id) {
        $where = array(
            'students.id' => $student_id,
            'year' => $this->_archive_year,
        );
        $this->db->select("student_name, age, grade_name , room_name, is_active, photo_permission_slips,teacher_child, islamic_book_return,non_active_registration_date");
        $this->db->join("students_records", "students_records.student_id = students.id");
        $this->db->join("rooms", "students_records.room_id = rooms.id");
        $this->db->join("grades", "rooms.grade_id = grades.id");
        $this->db->limit('4');
        $res = $this->students_m->get_many_by($where);

        return $res;
    }

    function get_students_by_letter($letter) {
//        $letter='Z';
        $this->db->select('*,students.id as id');
        $this->db->join('parents', 'parents.id=students.parent_id');
//        $this->db->like('family_name',"$letter%");
//         $this->db->or_like('LOWER(family_name)', strtolower($letter.'%'));
        $this->db->where('family_name LIKE', "$letter%");
        $result = $this->get_all_crud();

//        var_dump($result);
//        die;
        return $result;
    }

    function count_all_students() {
        $where = array();
        $this->db->select('*,family_name,students.id as id');
        $this->db->join('users', 'users.id = students.user_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->order_by('student_name', 'ASC');
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_students() {
        $this->db->select('*,family_name,students.id as id');
        $this->db->join('users', 'users.id = students.user_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->order_by('student_name', 'ASC');
        $result = $this->count_filtered_crud();
        return $result;
    }

    function get_parent_children($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->select('student_name,family_name,parent_id,students.id as student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->where($where);
        $this->db->order_by('student_name', 'ASC');
        $result = $this->get_all_crud();
        return $result;
    }

    function count_all_parent_children($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->select('student_name,family_name,parent_id,students.id as student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->where($where);
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_parent_children($parent_id) {
        $where = array('parent_id' => $parent_id);
        $this->db->select('student_name,family_name,parent_id,students.id as student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    function get_student_records_for_profile($student_id) {
        $where = array(
            'students.id' => $student_id,
            'year' => $this->_archive_year,
            'is_active' => 2,
        );
        $this->db->select("student_name, age, grade_name , room_name, is_active, photo_permission_slips, islamic_book_return,status_islamic_book_return,stage_id,non_active_registration_date,	teacher_child");
        $this->db->join("students_records", "students_records.student_id = students.id");
        $this->db->join("rooms", "students_records.room_id = rooms.id");
        $this->db->join("grades", "rooms.grade_id = grades.id");
        $res = $this->students_m->get_many_by($where);

        return $res;
    }

    function get_class_students_for_specific_class($grade_id) {
        $where = array(
            'is_active' => 2,
            'grade_id' => $grade_id,
        );
        $this->db->select('is_active,grade_id,grade_name,family_name,students.user_id as user_id,father_name,mother_name,student_name,email');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('students_records', 'students_records.student_id=students.id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('users', 'users.id = students.user_id');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $result = $this->get_all_crud();

        if (isset($result) && $result)
            return $result;
        else
            return false;
    }

    function get_student_by_user_id_use_in_msg($user_id) {
        $where = array('users.id' => $user_id,
            'is_active' => 2,);
        $this->db->select('*,users.id as id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('students_records', 'students_records.student_id=students.id');
        $this->db->join('users', 'students.user_id=users.id');
        $result = $this->students_model->get_by($where);
        return $result;
    }

}
