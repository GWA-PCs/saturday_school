<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_payments_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'students_payments';
    var $column_order = array('payment_date', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('grade_name', 'room_name', 'payment_item_type', 'student_payment_date', 'invoice', 'check_or_cash'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('students_payments.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'students_payments_m');
        $this->students_payments_m->set_table('students_payments');

//        $this->load->model('general_model', 'rooms_courses_m');
//        $this->rooms_courses_m->set_table('rooms_courses');
//
        $this->load->model('general_model', 'students_m');
        $this->students_m->set_table('students');
    }

    function get_student_payments($student_id) {
        $where = array(
//            'is_active' => 2,
            'student_id' => $student_id,
            'year' => $this->_archive_year,
        );

        $this->db->select('month_name,tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,invoice,payment,check_or_cash,student_payment_description,student_payment_date,student_payment_notes,teacher_child_discount,full_year_discount,tuition_type');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $this->db->where($where);
//        $this->db->order_by('student_payment_date', 'desc');
        $this->db->order_by('students_payments.id', 'desc');
        $result = $this->get_all_crud();
        return $result;
    }

    function get_student_cost_room_course($student_id) {
        $where = array('id' => $student_id,);
        $result = $this->students_m->get_by($where);

        return $result;
    }

    function get_sum_of_student_payments_value($student_id) {
        $where = array(
            'student_id' => $student_id,
        );
        $this->db->select('student_payment_value,SUM(student_payment_value) as sum');
        $result = $this->get_by($where);

        return $result;
    }

    function get_students_not_paid($months) {

        $result = array();
        $where2 = array(
            'is_active' => 2,
            'tuition_type' => 'monthly',
            'tuition_type' => 'bimonthly',
            'payment_item_type' => 'tuition',
            'year' => $this->_archive_year,
        );

        $this->db->select('year,month_name,tuition_type,students_payments.id as id ,is_active,'
                . 'students_records.id as student_record_id,students_records.student_id as student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,invoice,check_or_cash,student_payment_description,'
                . 'student_payment_date,student_payment_notes,student_name,father_name ,family_name');

        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('students', 'students.id=students_records.student_id');
        $this->db->join('parents', 'parents.id=students.parent_id');
        $this->db->join('users', 'users.id=parents.user_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.id');

        if (isset($months) && $months) {
            foreach ($months as $key => $value) {
                if ($months[$key] != null) {
                    $this->db->where('month_name', $key);
                }
            }

            $result = $this->get_many_by($where2);
        }

        return $result;
    }

    function count_all_students_payments($student_id) {
        $where = array(
            'is_active' => 2,
            'student_id' => $student_id,
            'year' => $this->_archive_year,
        );

        $this->db->select('year,month_name,tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,invoice,check_or_cash,student_payment_description,student_payment_date,student_payment_notes,teacher_child_discount,full_year_discount,tuition_type');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $result = $this->count_by($where);
        return $result;
    }

    function count_filtered_crud_students_payments($student_id) {
        $where = array(
            'is_active' => 2,
            'student_id' => $student_id,
            'year' => $this->_archive_year,
        );

        $this->db->select('year,month_name,tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,invoice,check_or_cash,student_payment_description,student_payment_date,student_payment_notes,teacher_child_discount,full_year_discount,tuition_type');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    function get_children_payments_for_parent($parent_children_array) {
        $where = array(
            'is_active' => 2,
            'year' => $this->_archive_year,
        );

        $this->db->select('year,month_name,tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,invoice,payment,check_or_cash,student_payment_description,student_payment_date,student_payment_notes,teacher_child_discount,full_year_discount,tuition_type');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $this->db->where($where);
        $this->db->where_in('student_id', $parent_children_array);
        $result = $this->get_all();
        return $result;
    }

    function get_students_payments_for_all_students_in_year($students_id_array) {
        if (!empty($students_id_array)) {
            $this->db->select('month_name,tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,'
                    . 'students_records.student_id,grade_name,grade_id,room_id,room_name'
                    . ',payment_item_id,payment_item_type,invoice,payment,check_or_cash,student_payment_description,'
                    . 'student_payment_date,student_payment_notes,teacher_child_discount,full_year_discount,tuition_type,student_name,'
                    . 'family_name,'
                    . 'SUM(invoice) as invoice_sum,SUM(payment) as payment_sum');
            $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
            $this->db->join('students', 'students.id=students_records.student_id');
            $this->db->join('parents', 'parents.id=students.parent_id');
            $this->db->join('rooms', 'rooms.id=students_records.room_id');
            $this->db->join('grades', 'grades.id=rooms.grade_id');
            $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
            $this->db->where_in('student_id', $students_id_array);
            $this->db->group_by('student_id');
            $result = $this->get_all();

            return $result;
        } else {
            return false;
        }
    }

}
