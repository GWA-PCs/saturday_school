<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stages_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'stages';
    var $column_order = array('stage_name', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('stage_name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('stages.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'stages_m');
        $this->stages_m->set_table('stages');
    }

    public function get_stages() {
        $this->db->select('*, stages.id as id');
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_stage($id) {
        $where = array('id'=> $id);
        $result = $this->get_by($where);
        return $result;
    }

    

}
