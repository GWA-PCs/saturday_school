<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pizza_and_snack_items_model extends MY_Model {

    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');
    var $table = 'pizza_and_snack_items';
    var $column_order = array('bills', 'quantity', 'cost', 'created_at', 'updated_at', null); //set column field database for datatable orderable
    var $column_search = array('bills', 'quantity', 'cost'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('pizza_and_snack_items.id' => 'desc'); // default order

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'pizza_and_snack_items_m');
        $this->pizza_and_snack_items_m->set_table('pizza_and_snack_items');
    }

    public function get_all_output_pizza_and_snack_items($pizza_and_snack_input_id) {
        $where = array(
            'pizza_and_snack_input_id' => $pizza_and_snack_input_id,
            'year' => $this->_archive_year,
        );
        $this->db->select('*, pizza_and_snack_items.id as id');
        $this->db->join('pizza_and_snack_inputs', 'pizza_and_snack_inputs.id=pizza_and_snack_items.pizza_and_snack_input_id');
        $this->db->where($where);
        $result = $this->get_all_crud();
        return $result;
    }

    public function get_sum_pizza_and_snack_items($pizza_and_snack_input_id = '') {
        $where = array(
            'pizza_and_snack_input_id' => $pizza_and_snack_input_id,
        );
        $this->db->select('SUM(total) as total_sum');
        $result = $this->get_by($where);
        return $result;
    }

    public function get_output_pizza_and_snack_items_by_id($pizza_and_snack_input_id) {
        $where = array('pizza_and_snack_input_id' => $pizza_and_snack_input_id,
            'year' => $this->_archive_year,);
        $this->db->select('*, pizza_and_snack_items.id as id');
        $this->db->join('pizza_and_snack_inputs', 'pizza_and_snack_inputs.id=pizza_and_snack_items.pizza_and_snack_input_id');
        $result = $this->get_by($where);
        return $result;
    }

    public function count_all_output_pizza_and_snack_items($pizza_and_snack_input_id) {
        $where = array('pizza_and_snack_input_id' => $pizza_and_snack_input_id,
            'year' => $this->_archive_year,);
        $this->db->select('*, pizza_and_snack_items.id as id');
        $this->db->join('pizza_and_snack_inputs', 'pizza_and_snack_inputs.id=pizza_and_snack_items.pizza_and_snack_input_id');
        $result = $this->count_by($where);
        return $result;
    }

    public function count_filtered_crud_output_pizza_and_snack_items($pizza_and_snack_input_id) {
        $where = array('pizza_and_snack_input_id' => $pizza_and_snack_input_id, 'year' => $this->_archive_year,);
        $this->db->select('*, pizza_and_snack_items.id as id');
        $this->db->join('pizza_and_snack_inputs', 'pizza_and_snack_inputs.id=pizza_and_snack_items.pizza_and_snack_input_id');
        $this->db->where($where);
        $result = $this->count_filtered_crud();
        return $result;
    }

    public function get_sum_pizza_and_snack_all_items() {
        $where = array(
            'year' => $this->_archive_year,
        );
        $this->db->select('SUM(total) as total_sum');
        $this->db->join('pizza_and_snack_inputs', 'pizza_and_snack_inputs.id=pizza_and_snack_items.pizza_and_snack_input_id');
        $this->db->group_by('pizza_and_snack_input_id');
        $result = $this->get_many_by($where);
        return $result;
    }

    public function get_output_pizza_and_snack_all_items() {
        $where = array(
            'year' => $this->_archive_year,
        );
        $this->db->select('*, pizza_and_snack_items.id as id');
        $this->db->join('pizza_and_snack_inputs', 'pizza_and_snack_inputs.id=pizza_and_snack_items.pizza_and_snack_input_id');
        $result = $this->get_many_by($where);
        return $result;
    }

}
