-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2018 at 01:28 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saturday_school`
--

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_requests`
--

CREATE TABLE `enrollment_requests` (
  `id` int(11) NOT NULL,
  `family_last_name` varchar(255) NOT NULL,
  `student_1` varchar(255) NOT NULL,
  `student_2` varchar(255) DEFAULT NULL,
  `student_3` varchar(255) DEFAULT NULL,
  `student_4` varchar(255) DEFAULT NULL,
  `student_age_1` varchar(255) DEFAULT NULL,
  `student_age_2` varchar(255) DEFAULT NULL,
  `student_age_3` varchar(255) DEFAULT NULL,
  `student_age_4` varchar(255) DEFAULT NULL,
  `student_grade_1` varchar(255) DEFAULT NULL,
  `student_grade_2` varchar(255) DEFAULT NULL,
  `student_grade_3` varchar(255) DEFAULT NULL,
  `student_grade_4` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home_address` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `mother_phone` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `father_phone` varchar(255) DEFAULT NULL,
  `attended` varchar(255) DEFAULT NULL,
  `their_grade_level` varchar(255) DEFAULT NULL,
  `other_information_1` varchar(255) CHARACTER SET utf16 DEFAULT NULL,
  `other_information_2` varchar(255) CHARACTER SET utf16 DEFAULT NULL,
  `other_information_3` varchar(255) CHARACTER SET utf16 DEFAULT NULL,
  `other_information_4` varchar(255) CHARACTER SET utf16 DEFAULT NULL,
  `agree` varchar(255) DEFAULT NULL,
  `contract` tinyint(4) NOT NULL DEFAULT '0',
  `permission` tinyint(4) NOT NULL DEFAULT '0',
  `book_fees` tinyint(4) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `form_name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enrollment_requests`
--

INSERT INTO `enrollment_requests` (`id`, `family_last_name`, `student_1`, `student_2`, `student_3`, `student_4`, `student_age_1`, `student_age_2`, `student_age_3`, `student_age_4`, `student_grade_1`, `student_grade_2`, `student_grade_3`, `student_grade_4`, `created_at`, `updated_at`, `email`, `home_address`, `home_phone`, `mother_name`, `mother_phone`, `father_name`, `father_phone`, `attended`, `their_grade_level`, `other_information_1`, `other_information_2`, `other_information_3`, `other_information_4`, `agree`, `contract`, `permission`, `book_fees`, `date`, `type`, `is_deleted`, `form_name`) VALUES
(23, '85', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '2018-10-10 12:18:18', '2018-10-10 12:18:18', 'e@w.com', '9', '9', '85', '85', '8585', '85', 'yes', '885', '85', '8', '8', '88', '1', 1, 1, 1, '2018-10-12', '15', 0, NULL),
(25, '985623', '9562', '89', '956', '965', '96', '56', '956', '', '96', '956', '956', '562', '2018-10-11 05:49:52', '2018-10-11 05:49:52', 'w@w.com', '562', '56', '2', '56', '562', '62', 'yes', '56', '', '65', '62', '62', '1', 1, 1, 1, '2018-10-25', '14', 0, ''),
(46, 'fppppppppppppp', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-11 13:28:56', '2018-10-11 13:28:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL),
(47, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-11 13:42:46', '2018-10-11 13:42:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL),
(48, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-11 14:02:57', '2018-10-11 14:02:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL),
(55, 'هخهخه', 'عخه', NULL, NULL, NULL, 'هخ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-12 20:54:52', '2018-10-12 20:54:52', 'email@email.com', 'هخ', 'خ', 'هخ', 'لتال', 'هخخ', 'اال', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-10-19', NULL, 0, 'youth_group_registration_form'),
(51, 'klklkl', 'koko', '9', '9', '9', '99', '99', '99', '9', '99', '9', '9', '99', '2018-10-12 16:45:53', '2018-10-12 16:45:53', 'email@email.com', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, 'arabic_reading_club'),
(52, 'klklkl', 'koko', '9', '9', '9', '99', '99', '99', '9', '99', '9', '9', '99', '2018-10-12 16:46:08', '2018-10-12 16:46:08', 'email@email.com', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, 'arabic_reading_club'),
(56, 'IYUIYUIYU', 'عخه', NULL, NULL, NULL, 'هخ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-12 20:55:06', '2018-10-12 20:55:06', 'email@email.com', 'هخ', 'خ', 'هخ', 'لتال', 'هخخ', 'اال', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-10-19', NULL, 0, 'youth_group_registration_form'),
(57, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-12 21:20:54', '2018-10-12 21:20:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL),
(58, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-12 21:45:33', '2018-10-12 21:45:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL),
(59, 'okioi', 'ioo', 'io', 'io', 'o', 'iio', 'io', 'io', 'ioio', 'io', 'io', 'ioi', 'io', '2018-10-12 21:47:31', '2018-10-12 22:11:20', 'email@edail.com', NULL, NULL, NULL, '434', NULL, '8675443', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 1, 'arabic_reading_club_new'),
(60, 'wwwwwwwwwwww', 'ffff', 'io', 'io', 'o', 'iio', 'io', 'io', 'ioio', 'io', 'io', 'ioi', 'io', '2018-10-12 22:10:48', '2018-10-12 22:10:48', 'email@edail.com', NULL, NULL, NULL, '434', NULL, '8675443', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, 'arabic_reading_club_new'),
(61, 'kooooooooo', 'ii', 'io', 'io', 'io', 'io', 'io', 'io', 'ioi', 'io', 'ioio', 'io', 'oio', '2018-10-13 00:03:49', '2018-10-13 00:03:49', 'io@h.com', 'io', 'i', 'i', '435', 'io', '543', 'yes', '534', '534', '53', '53', '53', '', 1, 1, 0, NULL, NULL, 0, 'summer_camp_form'),
(62, '98562389556', '98', '9', '89', '998', '89', '989', '98', '89', '8', '98', '898', '89', '2018-10-13 00:19:52', '2018-10-13 00:19:52', 't@l.com', '89', '898', '989', '89', '89', '9898', 'yes', '89', '89', '989', '898', '98', '1', 1, 1, 1, '2018-10-25', '5', 0, 'registration_form');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `grade_name` varchar(255) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `next_grade_name` varchar(255) NOT NULL,
  `report_card_name` enum('p1_report_card','kg_pre2_report_card','elem_report_card','no_report_card') NOT NULL DEFAULT 'no_report_card',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `grade_name`, `stage_id`, `next_grade_name`, `report_card_name`, `created_at`, `updated_at`) VALUES
(23, 'Kindergarten ', 1, '40', 'kg_pre2_report_card', '2018-06-03 08:20:10', '2018-09-16 12:01:25'),
(24, 'Preschool 2 ', 1, '23', 'kg_pre2_report_card', '2018-06-03 08:21:11', '2018-09-16 11:53:15'),
(25, 'Preschool 1', 1, '24', 'p1_report_card', '2018-06-03 08:22:36', '2018-09-16 11:53:06'),
(26, 'Sixth', 2, '36', 'elem_report_card', '2018-06-03 08:23:15', '2018-09-16 12:00:44'),
(27, 'Fifth ', 2, '26', 'elem_report_card', '2018-06-03 08:23:50', '2018-09-16 12:01:08'),
(28, 'Fourth ', 2, '27', 'elem_report_card', '2018-06-03 08:24:13', '2018-09-16 12:01:21'),
(29, 'Third ', 2, '28', 'elem_report_card', '2018-06-03 08:39:55', '2018-09-16 12:00:40'),
(36, 'Youth Group', 3, '0', 'no_report_card', '2018-06-06 07:18:25', '2018-06-18 11:49:17'),
(35, 'Book Club', 3, '0', 'no_report_card', '2018-06-06 07:17:38', '2018-09-16 12:01:02'),
(40, 'First', 2, '41', 'elem_report_card', '2018-08-26 18:14:48', '2018-09-16 12:01:16'),
(41, 'Second', 2, '29', 'elem_report_card', '2018-08-26 19:15:13', '2018-09-15 08:36:44'),
(42, 'Beginners 1', 2, '0', 'elem_report_card', '2018-08-26 21:07:59', '2018-09-16 12:00:50'),
(43, 'Beginners 2', 2, '0', 'elem_report_card', '2018-08-26 21:08:12', '2018-09-16 12:00:58');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `description`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'test', 'for test', '2018-05-02 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'admin', 'Administrator', '2018-05-02 00:00:00', '2018-05-17 00:00:00', 0),
(7, 'members', 'General User', '2018-05-02 00:00:00', '2018-05-11 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups_previlages`
--

CREATE TABLE `groups_previlages` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `previlage_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--

CREATE TABLE `halls` (
  `id` int(11) NOT NULL,
  `hall_name` varchar(255) NOT NULL,
  `hall_status` enum('empty','reserved') NOT NULL DEFAULT 'empty',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `halls`
--

INSERT INTO `halls` (`id`, `hall_name`, `hall_status`, `created_at`, `updated_at`) VALUES
(1, 'الأولى', 'reserved', '2018-05-27 08:28:55', '2018-09-20 05:40:06'),
(4, 'الثانية', 'empty', '2018-07-05 08:47:21', '2018-07-05 08:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `honored_orders`
--

CREATE TABLE `honored_orders` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `order_title` varchar(255) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `order_status` enum('pending','reservation_done','honored_done') NOT NULL,
  `honored_done` tinyint(1) NOT NULL DEFAULT '0',
  `honored_date` date DEFAULT NULL,
  `honored_reason` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `honored_orders`
--

INSERT INTO `honored_orders` (`id`, `teacher_id`, `order_title`, `stage_id`, `order_status`, `honored_done`, `honored_date`, `honored_reason`, `created_at`, `updated_at`) VALUES
(19, 11, 'Honored request ', 1, 'reservation_done', 0, NULL, 'Complete surah Fatiha', '2018-07-19 13:30:10', '2018-07-19 13:30:10'),
(20, 11, 'Quran Stars', 2, 'reservation_done', 0, NULL, 'Surah Alkjfnrjk', '2018-08-26 18:37:23', '2018-08-26 18:37:23'),
(23, 29, 'Quran Stars', 2, 'honored_done', 2, '2018-09-12', 'Surah Falaq', '2018-09-01 05:52:26', '2018-09-19 06:35:54'),
(24, 19, 'تكريم 1', 2, 'reservation_done', 0, NULL, 'ttt', '2018-09-10 09:01:18', '2018-09-10 09:01:18'),
(25, 19, 'Cer for beg 1 ', 1, 'honored_done', 2, '2018-08-27', 'Cer for beg 1 ', '2018-09-20 11:58:45', '2018-09-20 12:03:37'),
(26, 19, 'dfbdfhdfgf', 1, 'reservation_done', 0, '2018-07-31', ',mnbv', '2018-09-20 12:07:38', '2018-09-20 12:07:38'),
(27, 19, 'xz', 1, 'reservation_done', 0, '2018-05-08', 'xz', '2018-09-20 12:07:46', '2018-09-20 12:07:46'),
(28, 19, 'dsdsdsd', 1, 'reservation_done', 0, '2018-09-20', 'xz', '2018-09-20 12:07:54', '2018-09-20 12:07:54'),
(29, 19, 'xz', 2, 'reservation_done', 0, '2018-09-03', 'xz', '2018-09-20 12:08:02', '2018-09-20 12:08:02'),
(30, 19, 'xz', 2, 'reservation_done', 0, '2018-09-12', 'xz', '2018-09-20 12:08:12', '2018-09-20 12:08:12'),
(31, 19, 'xz', 1, 'reservation_done', 0, '2018-09-04', 'xz', '2018-09-20 12:08:19', '2018-09-20 12:08:19'),
(32, 19, 'xz', 1, 'reservation_done', 0, '2018-08-29', 'xz', '2018-09-20 12:08:27', '2018-09-20 12:08:27'),
(33, 19, 'xz', 1, 'reservation_done', 0, '2018-09-08', 'xz', '2018-09-20 12:08:35', '2018-09-20 12:08:35'),
(34, 19, 'xz', 2, 'reservation_done', 0, '2018-09-14', 'xz', '2018-09-20 12:08:43', '2018-09-20 12:08:43'),
(35, 20, 'dsda', 1, 'reservation_done', 0, '2018-09-20', 'scsdsd', '2018-09-20 12:23:09', '2018-09-20 12:23:09'),
(36, 20, 'ertyu', 2, 'pending', 0, '2018-09-20', 'sdsd', '2018-09-20 12:24:32', '2018-09-20 12:24:32'),
(37, 20, 'dad', 2, 'reservation_done', 0, '2018-09-21', 'sa', '2018-09-20 12:25:07', '2018-09-20 12:25:07'),
(38, 20, 'lkjhg', 1, 'reservation_done', 0, '2018-09-04', 'lkjnhgb', '2018-09-20 12:36:06', '2018-09-20 12:36:06'),
(39, 19, 'lkjhg', 1, 'reservation_done', 0, '2018-09-26', 'sfd', '2018-09-20 05:37:14', '2018-09-20 05:37:14'),
(40, 19, ',mnb v', 2, 'reservation_done', 0, '2018-09-27', '.,mnb', '2018-09-20 05:40:06', '2018-09-20 05:40:06');

-- --------------------------------------------------------

--
-- Table structure for table `honored_students`
--

CREATE TABLE `honored_students` (
  `id` int(11) NOT NULL,
  `honored_order_id` int(11) NOT NULL,
  `student_record_id` int(11) NOT NULL,
  `honored_date` date NOT NULL,
  `honored_reason` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `honored_students`
--

INSERT INTO `honored_students` (`id`, `honored_order_id`, `student_record_id`, `honored_date`, `honored_reason`, `created_at`, `updated_at`) VALUES
(9, 18, 56, '0000-00-00', '', '2018-06-21 11:25:46', '2018-06-21 11:25:46'),
(10, 19, 71, '0000-00-00', '', '2018-07-19 13:30:21', '2018-07-19 13:30:21'),
(11, 19, 83, '0000-00-00', '', '2018-08-19 13:35:21', '2018-08-19 13:35:21'),
(12, 20, 56, '0000-00-00', '', '2018-08-26 18:37:51', '2018-08-26 18:37:51'),
(13, 20, 64, '0000-00-00', '', '2018-08-26 18:37:58', '2018-08-26 18:37:58'),
(14, 23, 94, '0000-00-00', '', '2018-09-01 05:53:21', '2018-09-01 05:53:21'),
(15, 24, 170, '0000-00-00', '', '2018-09-10 09:01:31', '2018-09-10 09:01:31'),
(16, 24, 169, '0000-00-00', '', '2018-09-10 09:01:44', '2018-09-10 09:01:44');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `meeting_title` varchar(255) NOT NULL,
  `meeting_date` date NOT NULL,
  `meeting_file` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `meeting_title`, `meeting_date`, `meeting_file`, `created_at`, `updated_at`) VALUES
(39, 'First meeting ', '2018-08-04', 'Meeting_11_11_17.png', '2018-08-30 08:52:55', '2018-08-30 08:52:55'),
(40, 'Second', '2018-09-13', NULL, '2018-09-19 06:55:07', '2018-09-19 06:55:07');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `msg_from` varchar(255) NOT NULL,
  `msg_to` varchar(255) NOT NULL,
  `msg_content` text NOT NULL,
  `msg_subject` varchar(255) NOT NULL,
  `msg_date` datetime NOT NULL,
  `for_parent` varchar(500) DEFAULT NULL,
  `msg_file` varchar(255) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `msg_from`, `msg_to`, `msg_content`, `msg_subject`, `msg_date`, `for_parent`, `msg_file`, `seen`, `created_at`, `updated_at`) VALUES
(36, '1', '58', 'Content', 'Subject', '2018-07-19 14:58:09', NULL, NULL, 0, NULL, NULL),
(35, '1', '49', 'send to parent ', 'send to parent ', '2018-07-19 13:59:07', NULL, 'test24.docx', 0, NULL, NULL),
(37, '1', '57', 'Content', 'Subject', '2018-07-19 14:58:09', NULL, NULL, 0, NULL, NULL),
(38, '1', '56', 'Content', 'Subject', '2018-07-19 14:58:09', NULL, NULL, 0, NULL, NULL),
(39, '1', '55', 'Content', 'Subject', '2018-07-19 14:58:09', NULL, NULL, 1, NULL, '2018-08-26 12:23:04'),
(41, '48', '1', 'msg to admin from parent ', 'msg to admin', '2018-07-19 16:05:02', NULL, NULL, 1, NULL, '2018-07-23 11:28:13'),
(43, '59', '1', 'msg to admin from parent', 'msg to admin', '2018-07-19 16:13:19', NULL, NULL, 1, NULL, '2018-08-02 12:07:00'),
(45, '59', '55', 'msg to teacher from parent', 'msg to teacher', '2018-07-21 08:43:20', NULL, NULL, 1, NULL, '2018-08-02 06:10:56'),
(68, '55', '48', 'msg from parent ', 'msg from parent ', '2018-08-02 17:15:26', 'Saleh - Eman / Zain', NULL, 1, NULL, '2018-08-19 13:35:38'),
(69, '55', '48', 'msg to parent ', 'msg to parent ', '2018-08-19 16:37:27', 'Saleh - Eman / Zain', 'test.txt', 1, NULL, '2018-08-19 13:37:31'),
(70, '59', '1', 'msg from parent ', 'msg from parent ', '2018-08-19 16:44:34', NULL, 'test1.txt', 1, NULL, '2018-08-19 13:45:01'),
(84, '1', '95', '--', 'test', '2018-09-20 13:37:14', 'Hesham - Hanin Jaber / Alashqar', NULL, 1, NULL, '2018-10-11 06:30:42'),
(85, '1', '97', '--', 'test', '2018-09-20 13:37:14', 'Abdirashid - Fadumo Ali / Dahir', NULL, 0, NULL, NULL),
(86, '1', '98', '--', 'test', '2018-09-20 13:37:14', 'Haroun - Ghada Haroun / Qawasmeh', NULL, 0, NULL, NULL),
(87, '1', '66', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(88, '1', '64', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(89, '1', '67', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(90, '1', '65', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(91, '1', '68', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(92, '1', '85', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(93, '1', '145', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(94, '1', '144', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(95, '1', '88', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(96, '1', '89', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL),
(97, '1', '82', 'test', 'test', '2018-09-20 13:38:03', NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `monthes`
--

CREATE TABLE `monthes` (
  `id` int(11) NOT NULL,
  `month_name` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `monthes`
--

INSERT INTO `monthes` (`id`, `month_name`, `created_at`, `updated_at`) VALUES
(1, 'September ', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(2, 'October', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(3, 'November', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(4, 'December', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(5, 'January', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(6, 'February', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(7, 'March', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(8, 'April', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(9, 'May', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(10, 'September-October', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(11, 'October-November', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(12, 'November-December', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(13, 'December-January', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(14, 'January-February', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(15, 'February-March', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(16, 'March-April', '2018-06-24 00:00:00', '2018-06-24 06:13:34'),
(17, 'April-May', '2018-06-24 00:00:00', '2018-06-24 00:00:00'),
(18, 'full_year', '2018-06-25 00:00:00', '2018-06-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `family_name` varchar(255) NOT NULL,
  `mother_phone` varchar(45) NOT NULL,
  `father_phone` varchar(45) NOT NULL,
  `home_phone` varchar(45) DEFAULT NULL,
  `balance` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `user_id`, `family_name`, `mother_phone`, `father_phone`, `home_phone`, `balance`, `created_at`, `updated_at`, `deleted`) VALUES
(24, 95, 'Alashqar', '714-723-1091', '714-726-2004', '', 0, '2018-08-31 04:59:23', '2018-08-31 05:00:24', 0),
(25, 96, 'Obeid', '562-607-4400', '951-281-9206', '', 0, '2018-08-31 05:11:43', '2018-08-31 05:11:43', 0),
(26, 97, 'Dahir', '714-461-6764', '714-461-6764', '', 0, '2018-08-31 05:19:51', '2018-08-31 05:19:51', 0),
(27, 98, 'Qawasmeh', '714-209-3694', '714-209-2624', '', 0, '2018-08-31 05:30:30', '2018-08-31 05:30:30', 0),
(28, 99, 'Alqaisi', '714-248-2873', '818-714-5181', '', 0, '2018-08-31 05:37:13', '2018-08-31 05:37:13', 0),
(29, 100, 'Bouhairi', '949-300-0439', '949-300-4956', '', 0, '2018-08-31 05:45:40', '2018-08-31 05:45:40', 0),
(30, 101, 'Keshk', '217-721-7065', '217-721-7321', '', 0, '2018-08-31 05:49:06', '2018-08-31 05:49:06', 0),
(32, 103, 'Nasif', '714-306-2673', '714-306-3133', '', 0, '2018-08-31 19:06:33', '2018-08-31 19:06:33', 0),
(34, 105, 'Setiawan', '714-924-2394', '714-924-2356', '', 0, '2018-08-31 20:37:45', '2018-08-31 20:37:45', 0),
(35, 106, 'Itani', '310-895-8768', '310-895-5019', '', 0, '2018-08-31 20:45:39', '2018-08-31 20:45:39', 0),
(36, 107, 'Elkamash', '714-325-4607', '714-325-4566', '', 0, '2018-08-31 22:58:43', '2018-08-31 22:58:43', 0),
(37, 108, 'Abdelbaky', '714-423-8977', '714-869-4320', '', 0, '2018-08-31 23:04:47', '2018-08-31 23:04:47', 0),
(38, 109, 'Alshekhle', '714-865-2025', '714-723-4887', '', 0, '2018-08-31 23:10:27', '2018-08-31 23:10:27', 0),
(39, 110, 'Awad', '714-329-9868', '', '', 0, '2018-08-31 23:16:37', '2018-08-31 23:16:37', 0),
(40, 111, 'Abdelkarim', '951-500-3082', '714-883-2510', '', 0, '2018-08-31 23:21:03', '2018-08-31 23:21:03', 0),
(41, 112, 'Tahiri', '714-321-9353', '714-321-9346', '', 0, '2018-08-31 23:43:35', '2018-08-31 23:43:35', 0),
(42, 113, 'Aldajani', '408-833-0726', '707-738-1365', '', 0, '2018-08-31 23:48:44', '2018-08-31 23:48:44', 0),
(43, 114, 'Habibeh', '714-381-0362', '714-624-9553', '', 0, '2018-08-31 23:51:31', '2018-08-31 23:51:31', 0),
(44, 115, 'Abu Seer', '714-388-7979', '714-602-0002', '', 0, '2018-09-01 00:10:26', '2018-09-01 00:10:26', 0),
(45, 116, 'Sheikh', '714-457-0253', '714-728-5931', '', 0, '2018-09-01 00:21:18', '2018-09-01 00:21:18', 0),
(46, 117, 'Abdulrahman', '714-487-9846', '714-209-3931', '', 0, '2018-09-01 00:26:12', '2018-09-01 00:26:12', 0),
(47, 118, 'Alrikabi', '714-924-2480', '714-924-2480', '', 0, '2018-09-01 00:39:40', '2018-09-01 00:39:40', 0),
(48, 119, 'Affani', '949-419-7144', '949-500-9322', '', 0, '2018-09-01 00:44:56', '2018-09-01 00:44:56', 0),
(49, 120, 'Ismail', '714-600-5124', '714-274-5993', '', 0, '2018-09-01 00:54:55', '2018-09-01 00:54:55', 0),
(50, 121, 'Kloub', '714-360-6866', '714-719-6866', '', 0, '2018-09-01 01:02:27', '2018-09-01 01:02:27', 0),
(51, 122, 'Mamsa', '714-815-7397', '714-815-7399', '714-280-1411', 0, '2018-09-01 01:11:01', '2018-09-01 01:11:01', 0),
(52, 123, 'Zidan', '714-926-2295', '714-728-6063', '', 0, '2018-09-01 01:16:19', '2018-09-01 01:16:19', 0),
(53, 124, 'Bahrawy', '714-468-7770', '714-855-6555', '', 0, '2018-09-01 01:35:55', '2018-09-01 01:35:55', 0),
(54, 125, 'Abuhajjaj', '408-420-8469', '408-420-8469', '', 0, '2018-09-01 01:39:27', '2018-09-01 01:39:27', 0),
(55, 126, 'Abdallat', '714-408-8864', '714-390-3206', '', 0, '2018-09-01 01:48:38', '2018-09-19 10:54:07', 0),
(56, 127, 'Azez', '714-600-8144', '619-792-2620', '714-6000-8144', 0, '2018-09-01 01:56:21', '2018-09-01 01:56:21', 0),
(57, 128, 'Abusharar', '909-346-5496', '909-749-2812', '', 0, '2018-09-01 02:15:14', '2018-09-01 02:15:14', 0),
(58, 129, 'Osman', '562-228-6942', '562-650-3091', '562-462-1183', 0, '2018-09-01 02:35:15', '2018-09-01 02:35:15', 0),
(59, 130, 'Khalafalla', '562-714-2845', '562-714-4533', '', 0, '2018-09-01 02:37:30', '2018-09-01 02:37:30', 0),
(60, 131, 'Ibrahim', '(714) 351-6750', '714-851-3895', '', 0, '2018-09-01 02:52:55', '2018-09-01 02:52:55', 0),
(61, 132, 'Azizadah', '714-292-5160', '714-745-0888', '', 0, '2018-09-01 02:58:28', '2018-09-01 02:58:28', 0),
(62, 133, 'Issa', '714-251-7163', '714-234-7852', '', 0, '2018-09-01 03:03:05', '2018-09-01 03:03:05', 0),
(63, 134, 'Sabeh', '714-475-4006', '714-743-3749', '', 0, '2018-09-01 03:36:59', '2018-09-01 03:36:59', 0),
(64, 135, 'Hassan', '323-497-4806', '323-497-4804', '714-752-6331', 0, '2018-09-01 04:06:43', '2018-09-01 04:06:43', 0),
(65, 136, 'Arar', '714-614-4898', '714-812-7900', '', 0, '2018-09-01 04:16:30', '2018-09-01 04:16:30', 0),
(69, 140, 'Abu Jableh', '714-349-0794', '714-900-1065', '', 0, '2018-09-01 18:14:05', '2018-09-01 18:14:05', 0),
(70, 141, 'Masri', '714-396-0039', '714-709-7903', '', 0, '2018-09-01 18:41:52', '2018-09-01 18:41:52', 0),
(71, 142, 'Noufal', '714-603-9309', '714-696-2754', '', 0, '2018-09-01 18:45:00', '2018-09-01 18:45:00', 0),
(72, 143, 'McDonald', '951-264-8512', '626-483-6142', '', 0, '2018-09-01 18:48:32', '2018-09-01 18:48:32', 0),
(73, 146, 'Mohamed', '714-334-1616', '714-334-2015', '', 0, '2018-09-02 00:56:52', '2018-09-02 00:56:52', 0),
(74, 147, 'Farshoukh', '310-408-7090', '310-259-9112', '310-542-1930', 0, '2018-09-02 01:02:03', '2018-09-02 01:02:03', 0),
(75, 148, 'Elgendy', '714-495-5072', '714-495-7297', '', 0, '2018-09-02 01:09:36', '2018-09-02 01:09:36', 0),
(76, 149, 'Itani', '714-829-5867', '714-733-8155', '', 0, '2018-09-02 01:11:56', '2018-09-02 01:11:56', 0),
(77, 150, 'Almasri', '626-609-9199', '626-922-2416', '', 0, '2018-09-02 01:16:32', '2018-09-02 01:16:32', 0),
(78, 151, 'Aljabi', '909-556-1844', '626-241-810', '', 0, '2018-09-02 01:19:40', '2018-09-02 01:19:40', 0),
(79, 152, 'Kabbara', '951-454-7499', '951-454-7068', '', 0, '2018-09-02 01:24:08', '2018-09-02 01:24:08', 0),
(80, 153, 'Alramahi', '714-269-9274', '714-269-9274', '', 0, '2018-09-02 01:28:03', '2018-09-02 01:28:03', 0),
(81, 154, 'Zaid', '714-348-0749', '714-628-6435', '', 0, '2018-09-02 01:31:29', '2018-09-02 01:31:29', 0),
(82, 155, 'Jubran', '714-401-0396', '714-936-9379', '', 0, '2018-09-02 01:34:25', '2018-09-02 01:34:25', 0),
(83, 156, 'Ghazal', '714-642-4006', '714-421-8186', '', 0, '2018-09-02 01:39:37', '2018-09-02 01:39:37', 0),
(84, 157, 'Saleh', '714-715-6198', '714-715-6225', '', 0, '2018-09-02 01:41:52', '2018-09-02 01:41:52', 0),
(85, 158, 'Almaoui', '702-513-3676', '702-513-9643', '', 0, '2018-09-02 01:44:37', '2018-09-02 01:44:37', 0),
(86, 159, 'Albawab', '310-349-9613', '310-349-9394', '', 0, '2018-09-02 01:47:46', '2018-09-02 01:47:46', 0),
(87, 160, 'Abdelrazek', '714-247-9236', '714-343-1250', '714-447-8812', 0, '2018-09-02 01:50:25', '2018-09-10 07:39:22', 0),
(88, 161, 'Alhenn', '714-414-2852', '714-818-0377', '', 0, '2018-09-02 01:56:36', '2018-09-10 07:39:08', 0),
(94, 165, 'yooooth', '878', '87', '878778', 0, '2018-10-11 10:49:06', '2018-10-11 10:49:06', 0),
(95, 166, 'edrfgthjk', '63', '63', '9 6', 0, '2018-10-11 11:41:00', '2018-10-11 11:41:00', 0),
(96, 163, 'هخهخه', 'لتال', 'اال', 'خ', 0, '2018-10-12 20:58:20', '2018-10-12 20:58:20', 0),
(97, 0, 'klklkl', '', '', NULL, 0, '2018-10-12 21:03:55', '2018-10-12 21:03:55', 0),
(98, 0, 'klklkl', '', '', NULL, 0, '2018-10-12 22:06:08', '2018-10-12 22:06:08', 0),
(99, 0, 'klklkl', '', '', NULL, 0, '2018-10-12 22:08:41', '2018-10-12 22:08:41', 0),
(100, 164, 'kooooooooo', '435', '543', 'i', 0, '2018-10-13 00:06:09', '2018-10-13 00:06:09', 0),
(101, 165, '98562389556', '89', '9898', '898', 0, '2018-10-13 00:20:46', '2018-10-13 00:20:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `parent_subjects`
--

CREATE TABLE `parent_subjects` (
  `id` int(11) NOT NULL,
  `parent_subject_name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parent_subjects`
--

INSERT INTO `parent_subjects` (`id`, `parent_subject_name`, `created_at`, `updated_at`) VALUES
(1, 'no_parent_subject', '2018-05-09 00:00:00', '2018-05-29 00:00:00'),
(2, 'arabic', '2018-05-09 00:00:00', '2018-05-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_items`
--

CREATE TABLE `payment_items` (
  `id` int(11) NOT NULL,
  `payment_key` varchar(255) NOT NULL,
  `payment_value` float NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_items`
--

INSERT INTO `payment_items` (`id`, `payment_key`, `payment_value`, `created_at`, `updated_at`) VALUES
(1, 'saturday_school_registration_fee', 30, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(34, 'saturday_school_pre_kinder_books_fee', 55, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(3, 'saturday_school_tuition_full_year_first_child', 720, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(4, 'saturday_school_tuition_full_year_second_child', 675, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(5, 'saturday_school_tuition_full_year_third_child', 630, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(6, 'saturday_school_tuition_full_year_discount', 20, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(7, 'saturday_school_tuition_bimonthly_first_child', 160, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(8, 'saturday_school_tuition_bimonthly_second_child', 150, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(9, 'saturday_school_tuition_bimonthly_third_child', 140, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(10, 'saturday_school_tuition_monthly_first_child', 80, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(11, 'saturday_school_tuition_monthly_second_child', 75, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(12, 'saturday_school_tuition_monthly_third_child', 70, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(13, 'saturday_school_late_payment_fee', 15, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(14, 'saturday_school_late_pickup_fee', 10, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(15, 'saturday_school_teacher_child_discount', 50, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(16, 'arabic_book_club_per_month', 10, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(17, 'youth_group_registration_fee', 40, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(18, 'youth_group_tuition_full_year_first_child', 720, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(19, 'youth_group_tuition_full_year_second_child', 675, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(20, 'youth_group_tuition_full_year_third_child', 630, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(21, 'youth_group_tuition_full_year_discount', 20, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(22, 'youth_group_tuition_bimonthly_first_child', 160, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(23, 'youth_group_tuition_bimonthly_second_child', 150, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(24, 'youth_group_tuition_bimonthly_third_child', 140, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(25, 'youth_group_tuition_monthly_first_child', 80, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(26, 'youth_group_tuition_monthly_second_child', 75, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(27, 'youth_group_tuition_monthly_third_child', 70, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(28, 'youth_group_late_payment_fee', 15, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(35, 'saturday_school_elementary_books_fee', 40, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(31, 'youth_group_late_payment_fee', 15, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(32, 'youth_group_late_pickup_fee', 15, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(33, 'youth_group_teacher_child_discount', 50, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(36, 'saturday_school_islamic_textbook', 30, '2018-06-23 00:00:00', '2018-10-01 07:58:53'),
(37, 'saturday_school_islamic_workbook', 8, '2018-06-23 00:00:00', '2018-10-01 07:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `petty_cashes`
--

CREATE TABLE `petty_cashes` (
  `id` int(11) NOT NULL,
  `petty_cash_date` date NOT NULL,
  `petty_cash_type` enum('dep','csh') NOT NULL DEFAULT 'csh',
  `petty_cash_account` varchar(255) NOT NULL,
  `petty_cash_memo` text NOT NULL,
  `petty_cash_payment` double NOT NULL,
  `petty_cash_deposit` double NOT NULL,
  `petty_cash_balance` double NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `petty_cashes`
--

INSERT INTO `petty_cashes` (`id`, `petty_cash_date`, `petty_cash_type`, `petty_cash_account`, `petty_cash_memo`, `petty_cash_payment`, `petty_cash_deposit`, `petty_cash_balance`, `year`, `created_at`, `updated_at`) VALUES
(4, '2018-07-11', 'csh', 'Marwan Sakkal', 'paying ice cream', 12, 0, 0, 2018, '2018-07-30 06:28:18', '2018-08-26 20:55:30'),
(6, '2018-09-22', 'dep', 'Dania Sakkal', 'Dania Sakkal', 600, 100, 0, 2018, '2018-09-19 08:51:51', '2018-09-19 08:51:51'),
(7, '2018-06-16', 'csh', 'fdsfsdfdf', 'fdsfsdf', 12000, 232, 0, 2018, '2018-09-30 11:12:30', '2018-09-30 11:12:30'),
(9, '2018-07-04', 'dep', 'Marwan Sakkal', 'rdf', 0, 0, 0, 2018, '2018-10-01 07:49:01', '2018-10-01 07:49:01'),
(10, '2018-06-24', 'dep', 'cv', 'xcv', 12000, 100, 0, 2018, '2018-10-02 11:57:31', '2018-10-02 11:57:31');

-- --------------------------------------------------------

--
-- Table structure for table `pizza_and_snack_inputs`
--

CREATE TABLE `pizza_and_snack_inputs` (
  `id` int(11) NOT NULL,
  `input_date` date NOT NULL,
  `input_title` varchar(255) NOT NULL,
  `cost` double NOT NULL,
  `deposit` double NOT NULL,
  `withdrawal` double NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pizza_and_snack_items`
--

CREATE TABLE `pizza_and_snack_items` (
  `id` int(11) NOT NULL,
  `pizza_and_snack_input_id` int(11) NOT NULL,
  `bills` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `previlages`
--

CREATE TABLE `previlages` (
  `id` int(11) NOT NULL,
  `function_name` varchar(255) NOT NULL,
  `previlage_name_ar` varchar(255) NOT NULL,
  `previlage_name_en` varchar(225) NOT NULL,
  `previlage_type` enum('admins','teachers','students') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `registration_form`
--

CREATE TABLE `registration_form` (
  `id` int(11) NOT NULL,
  `student_1` varchar(255) DEFAULT NULL,
  `student_2` varchar(255) DEFAULT NULL,
  `student_3` varchar(255) DEFAULT NULL,
  `student_4` varchar(255) DEFAULT NULL,
  `student_age_1` varchar(255) DEFAULT NULL,
  `student_age_2` varchar(255) DEFAULT NULL,
  `student_age_3` varchar(255) DEFAULT NULL,
  `student_age_4` varchar(255) DEFAULT NULL,
  `student_grade_1` varchar(255) DEFAULT NULL,
  `student_grade_2` varchar(255) DEFAULT NULL,
  `student_grade_3` varchar(255) DEFAULT NULL,
  `student_grade_4` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home_address` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `mother_phone` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `father_phone` varchar(255) DEFAULT NULL,
  `attended` varchar(255) DEFAULT NULL,
  `their_grade_level` varchar(255) DEFAULT NULL,
  `other_information` varchar(255) DEFAULT NULL,
  `agree` varchar(255) DEFAULT NULL,
  `contract` tinyint(4) NOT NULL DEFAULT '0',
  `permission` tinyint(4) NOT NULL DEFAULT '0',
  `book_fees` tinyint(4) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Dumping data for table `registration_form`
--

INSERT INTO `registration_form` (`id`, `student_1`, `student_2`, `student_3`, `student_4`, `student_age_1`, `student_age_2`, `student_age_3`, `student_age_4`, `student_grade_1`, `student_grade_2`, `student_grade_3`, `student_grade_4`, `email`, `home_address`, `home_phone`, `mother_name`, `mother_phone`, `father_name`, `father_phone`, `attended`, `their_grade_level`, `other_information`, `agree`, `contract`, `permission`, `book_fees`, `date`, `type`, `created_at`, `updated_at`) VALUES
(2, 'Reina Almaoui, 10, 1st grade ', 'Diane Almaoui, 9, 1st grade ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abirzantout@hotmail.com', '53 Diamond, Irvine CA 92620', '702-513-3676', 'Abir ', '702-513-3676', 'Mazen', '702-513-9643', 'yes', 'Beginners ', '', '1', 1, 1, 1, '2018-08-19', '10', '2018-08-19 19:32:00', '2018-08-19 19:32:00'),
(3, 'Yaseen Saleh, 6, Male', 'Farida Saleh, 4, Female', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'm.bakr@yahoo.com', '124 Stratford Cir., Placentia, CA 92870', '', 'Yasmin Noureldin ', '7147156198', 'Mohamed Saleh', '7147156225', 'yes', 'Pre-K and kindergarten', '', '1', 1, 1, 1, '2018-08-19', '1', '2018-08-19 20:01:02', '2018-08-19 20:01:02'),
(4, 'Salma Ghazal, Age 4.5 years', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bassamghazal@gmail.com', '6600 Warner Ave Unit195', '', 'Tamara Ghazal', '7146424006', 'Bassam Ghazal', '7144218186', 'yes', 'Preschool', '', '1', 1, 1, 1, '2018-08-19', '1', '2018-08-19 20:10:35', '2018-08-19 20:10:35'),
(90, 'Jana Alhenn, 9, Beginners', 'Bayan Alhenn, 7, Beginners', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'khuloudhassan80@gmail.com', '9672 Colony Streeet Anaheim, CA 92804', '', 'Khuloud hassan', '714-414-2852', 'Omar Alhenn', '714-818-0377', 'no', '', 'Emergency Contact: 714-728-5476, Hamzeh Alhenn, Uncle', '1', 1, 1, 1, '2018-09-01', '2', '2018-09-01 21:13:44', '2018-09-01 21:13:44'),
(7, 'Mona jubran, 12, 6', 'Hannah jubran, 10, 4th ', 'Omar jubran, 7, kk', 'Ali jubran, 4, preschool ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rasha_jubran@hotmail.com', '2254 E. Vermont Ave Anaheim Ca 92806', '714-401-0396', 'Rasha Jubran ', '714-401-0396', 'Jubran jubran ', '714-936-9379', 'yes', '6', 'None ', '1', 1, 0, 1, '2018-08-19', '2', '2018-08-19 23:25:25', '2018-08-19 23:25:25'),
(89, 'Hassan Abdelrazek, 6, 1st grade', 'Ali Abdelrazek, 9, 5th grade', 'Kareem. 14, 9th grade', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'lailasobhy@gmail.com', '1416 Sunnycrest Drive Fullerton, CA 92835', '714-447-8812', 'Laila Sobhy', '714-247-9236', 'Ahmed Abdelrazek', '714-343-1250', 'no', 'N/a', 'None', '1', 1, 1, 1, '2018-09-01', '2', '2018-09-01 20:14:53', '2018-09-01 20:14:53'),
(88, 'Jad Albawab 10 year 6th', 'Lea Albawab 8 3rd ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fatom1979@yahoo.com', '930 S McCloud St Anaheim ca 92805', '3103499613', 'Fatima ', '3103499613', 'Amer', '3103499394', 'yes', '5th 2nd ', 'No', '1', 1, 1, 1, '2018-09-01', '0', '2018-09-01 18:39:03', '2018-09-01 18:39:03'),
(10, 'Zayed Zaid, 6, preschool', 'Mohammed Zaid, 4,presxhool', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shirinelwir@gmail.com', '2132 derek dr apt a', '7143480749', 'Shirin Elwir', '7143480749', 'Eid Zaid', '7146286435', 'yes', 'Preschool 1', 'None', '1', 1, 1, 1, '2018-08-19', '2', '2018-08-20 03:50:26', '2018-08-20 03:50:26'),
(11, 'Farah Alramahi age 12 6th grade', 'Ahmad Alramahi age 4 (almost 5) grade pre KG (was with mrs nada last year)', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mnrabah@hotmail.com', '5310 Fairview Avenue', '7142699274', 'Tharwa Ahmad ', '7142699274', 'Mohd Alramahi ', '7142699274', 'yes', '5 and pre kg', '', '1', 1, 1, 1, '2018-08-20', '2', '2018-08-20 20:16:14', '2018-08-20 20:16:14'),
(93, 'Hala Zayat, 8 Beginner 2', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bayannaamani@gmail.com', '7147321830', '', 'Bayan Naamani', '7147321830', 'Ghassan Zayat', '7147100751', 'yes', 'beginner', '', '1', 1, 1, 1, '2018-09-08', '4', '2018-09-02 02:49:50', '2018-09-02 02:49:50'),
(94, 'Yousef Jazaeri, 7 1st grade', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'emanshehadeh@gmail.com', '2957 Brea Blvd. Fullerton CA, 92835', '209-256-1020', 'Eman Shehadeh', '209-256-1020', 'Ahmad Jazaeri', '360-888-8647', 'no', '', 'none', '1', 1, 0, 1, '2018-09-01', '4', '2018-09-02 04:03:33', '2018-09-02 04:03:33'),
(13, 'Robbie Kabbara, 5 ', 'Katie Kabbara, 11', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rkabbara@msn.com', '3659 Garretson Avenue', '9514547499', 'Rana Kabbara', '9514547499', 'Mazen Kabbara', '9514547068', 'yes', '', '', '1', 1, 1, 1, '2018-08-24', '2', '2018-08-24 17:11:14', '2018-08-24 17:11:14'),
(95, 'Maham rehman 2nd grade 8 years', 'Izhan rehman 2nd grade 7 years', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shumailakamran29@gmail.com', '313 North East Street C Anaheim CA92805', '6572626245', 'Shumaila kamran', '65726245', 'Kamran rehman', '7148017593', 'no', '2nd and 3rd', '', '1', 1, 1, 1, '2018-09-02', '0', '2018-09-03 01:32:34', '2018-09-03 01:32:34'),
(15, 'Zoher Aljabi, 8, 4th', 'Leen Aljabi, 6, 1st', 'Lulia Aljabi, 4, Pre 2', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'duaa.kassar@gmail.com', '20615 Fuero Dr', '9095561844', 'Duoa alkassar', '9095561844', 'Reda aljabi', '6262418101', 'yes', '', 'Lulia has food allergies to sesame seeds/pistachio /cashew ', '1', 1, 1, 1, '2018-08-24', '2', '2018-08-24 19:15:06', '2018-08-24 19:15:06'),
(16, 'Omar almasri ,6years ,1grade', 'Adam almasri,4years,p2', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'h.doukmak@yahoo.com', '425E arrow hwy ,Glendora ', '6266099199', 'Hazaar', '6266099199', 'Kenana', '6269222416', 'yes', '', 'Omar has tree nuts allergies', '1', 1, 1, 1, '2018-08-24', '3', '2018-08-24 19:35:42', '2018-08-24 19:35:42'),
(96, 'Mohammad Elwir 6 kindergarten', 'Heba Abdelhady', 'Heba Abdelhady', 'Heba Abdelhady', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hebahani_90@hotmail.com', '1549 cherry street unit D placentia Ca', '7142493554', 'Heba Abdelhady', '7142493554', 'Eid Elwir', '7144239171', 'yes', 'Kindergarten ', 'No', '1', 1, 1, 1, '2018-09-04', '2', '2018-09-04 16:34:56', '2018-09-04 16:34:56'),
(19, 'Samer Itani ,10,4th ', 'Mustapha Itani ,8,3rd grade', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rorotani2014@hotmail.com', '5573 Saint Ann ave cypress ca 90630', '7147338155', 'Rania', '7148295867', 'Mohammad', '7147338155', 'yes', '2 and 3 ', '', '1', 1, 1, 1, '2018-08-24', '1', '2018-08-25 03:07:19', '2018-08-25 03:07:19'),
(20, 'Ghazal Elgendy,9,4', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nayraahmed42@gmail.com', '315 n associated re apt 507 brea ca 92821', '', 'Naira Elawady ', '7144955072', 'Mohamed Elgendy ', '7144957297', 'no', '', '', '1', 1, 1, 1, '2018-08-24', '2', '2018-08-25 03:19:32', '2018-08-25 03:19:32'),
(21, 'Jad Farshoukh 13 Beginners 2', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'emailrazan@gmail.com', '2111 Warfield Ave #B Redondo Beach CA 90278', '310-542-1930', 'Razanne', '310-408-7090', 'Nabil', '310-259-9112', 'yes', 'Beginners one', 'Mild asthma, uses hand held inhaler when needed. ', '1', 1, 1, 1, '2018-07-24', '2', '2018-08-25 04:53:59', '2018-08-25 04:53:59'),
(22, 'Jodi Mohamed', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ykamalan@gmail.com', '9722 PASEO DE ORO', '7143342015', 'Youssra Nasr', '7143341616', 'Islam Abdelaziz', '7143342015', 'yes', '', 'Na', '1', 1, 1, 1, '2018-08-24', '1', '2018-08-25 05:18:16', '2018-08-25 05:18:16'),
(23, 'Ayah Arar, 10, 4', 'Saja Arar, 7, 2', 'Hamza Arar, 11, 6', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'amonaabood@yahoo.com', '439 S. Westridge Cir Anaheim 92807', '714-614-4898', 'Amani ', '714-614-4898', 'Abdallah ', '714-812-7900', 'yes', '3, 1,7 ', '', '0', 0, 0, 0, '2018-08-24', '0', '2018-08-25 05:35:00', '2018-08-25 05:35:00'),
(85, 'Lily Nofal, 5, kindergarten', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'noura.tarek.saad@gmail.com', '1540 w ball rd, anaheim, ca, 92802, apt 20A', '', 'Nouran saad', '(714) 603-9309', 'Moustafa nofal', '‭(714) 696-2754‬', 'no', '', 'None', '1', 1, 1, 1, '2018-08-31', '2', '2018-09-01 06:05:15', '2018-09-01 06:05:15'),
(86, 'Talin Abu Jableh  4 years', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hebaabudebei09@hotmai.com', '2900e Lincoln ave Anaheim 92806', '', 'Heba ', '7143490794', 'Baker', '7149001065', 'no', '', '', '1', 1, 1, 1, '2018-09-01', '0', '2018-09-01 17:16:42', '2018-09-01 17:16:42'),
(87, 'Layal Masri 5 year old pres-school', 'Toufic Masri', 'Toufic Masri', 'Toufic Masri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tmasri@gmail.com', '140 N. Hidden Cyn, Orange ca 92869', '7147097903', 'Rania Masri', '7143960039', 'Toufic Masri', '7147097903', 'no', 'None', '', '1', 1, 1, 1, '2017-09-01', '0', '2018-09-01 18:29:12', '2018-09-01 18:29:12'),
(26, 'Hassan Hassan , 9 , Grade 3 ', 'Niveen Hassan, 9, Grade 3', 'Zayd Hassan, 6, Grade KG ', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mayaya7@aol.com', '8582 emerywood dr', '(714) 752-6331', 'May Ahmad', '(323)497-4806', 'Morad Hassan', '(323)497-4804', 'yes', 'Hassan and Niveen were Grade 2 and Zayd was Pre-K 2', '', '1', 1, 1, 1, '2018-08-24', '2', '2018-08-25 05:49:43', '2018-08-25 05:49:43'),
(27, 'Safiya sabeh 8 1/2 3rd', 'Hisham sabeh  3 1/2 pre k', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'susu.sabeh@gmail.com', '1054 S Mountvale Ct', '7144754006', 'sawsan sabeh', '7144754006', 'Gino sabeh', '7147433749', 'yes', '2nd', 'N/a', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 08:49:34', '2018-08-25 08:49:34'),
(28, 'Yaseen Issa, 10 years, 5th grade', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ammari976@att.net', '2834 E. Frontera St., Unit D Anaheim, CA 92806', '', 'Manal Issa', '714-251-7163', 'Ammar Issa', '714-234-7852', 'yes', '4th', 'None', '1', 1, 1, 1, '2018-08-25', '0', '2018-08-25 13:30:14', '2018-08-25 13:30:14'),
(84, 'Liam McDonald, 6 Kindergarten', 'Najanine McDonald, 11 7th grade', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jasminemcdonald762@yahoo.com', '13005 Cambridge Ct. Chino CA 91710', '951.264.8512', 'Jasmine McDonald', '9512648512', 'James McDonald', '6264836142', 'yes', 'Najanine was in 6th and Liam was in PK2', '', '1', 1, 1, 1, '2018-08-31', '0', '2018-09-01 03:35:52', '2018-09-01 03:35:52'),
(30, 'Sannah Azizadah, 5, Kindergarten', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tamim@twinmoons.com', '17006 New Pine Dr, Hacienda Heights, 91745', '7147450888', 'Husna Azizadah', '7142925160', 'Tamim Azizadah', '7147450888', 'yes', 'Pre-K', '', '1', 1, 1, 1, '2018-08-25', '1', '2018-08-25 17:16:11', '2018-08-25 17:16:11'),
(32, 'Lara Ibrahim 5 K', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'talab89@yahoo.com', '714 W Lamark Dr Anaheim, CA 92802', '7148513895', 'Olvat Tabel', '‭+1 (714) 351-6750‬', 'Talab Ibrahim', '714-851-3895', 'yes', 'Blue with  Ms Nada ', '', '1', 1, 1, 1, '2018-08-25', '0', '2018-08-25 17:30:14', '2018-08-25 17:30:14'),
(105, 'Aisha Rahmatee 5 kindergarten', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'manejahaidary@gmail.com', '100 north citrus ranch road 205', '', 'Maneja Haidary', '7147274827', 'Rafatullah Rahmatee', '7142605372', 'no', '', 'None', '1', 1, 1, 1, '2018-09-08', '2', '2018-09-08 16:44:16', '2018-09-08 16:44:16'),
(34, 'Bayan Khalafalla, 10, 5', 'Bilal Khalafalla, 7, 1', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'newhendali@gmail.com', '11645 Old River School Road, Downey 90242', '562-714-2845', 'Hend Ali', '562-714-2845', 'Ahmed Khalafalla', '562-714-4533', 'yes', '4 and K', 'Bayan is allergic to peppermint candy. Both children have mild asthma', '1', 1, 1, 1, '2018-08-25', '3', '2018-08-25 17:40:07', '2018-08-25 17:40:07'),
(83, 'Haroun Tahiri, 7years old, kindergarden Level for Minaret', 'Imraan Tahiri, 4 years old, preschool', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Htahiri@ymail.com', '928 south flint ridge way', '7143219353', 'Hediya Tahiri ', '7143219353', 'Yousuf Tahiri ', '7143219346', 'yes', 'Haroun Tahiri  preschool 2', '', '1', 1, 1, 1, '2018-08-31', '0', '2018-08-31 17:25:46', '2018-08-31 17:25:46'),
(36, 'Laila Osman, 10, 2nd', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ghadafaysal1973@hotmail.com', '11251 Pioneer blvd., apt # H6, Norwalk, CA, 90650', '562-462-1183', 'Ghada Mahmoud', '562-228-6942', 'Mustafa Osman', '562-650-3091', 'yes', 'first', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 18:02:18', '2018-08-25 18:02:18'),
(37, 'Ahmed albisani', 'Honada albisani', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'quietangel66@hotmail.com', '804 s Webster ave Anaheim CA 92804 Apt 11', '', 'Hend kanaan', '7143764058', 'Bilal kanaan', '3235570147', 'yes', '', '', '1', 1, 1, 1, '2018-08-25', '3', '2018-08-25 18:10:10', '2018-08-25 18:10:10'),
(91, 'Sarah Akil, 7, 1st', 'Kareem Akil, 6, 1st', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'baroutaji70@hotmail.com', '5541 Saint Ann Avenue Cypres, CA 90630', '', 'Hazar Baroutaji', '714-510-4771', 'Mohammed Akil', '714-864-7583', 'yes', 'Kg, Pre 2 (but before was with Kg)', 'Emergency Contact: Lina Akil, Aunt', '1', 1, 1, 1, '2018-09-01', '2', '2018-09-01 21:28:49', '2018-09-01 21:28:49'),
(92, 'Alya Mraish, 8, 2nd grade', 'Ghalya Mraish, 4, Pre 1', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ranamtow@yahoo.com', '222 North Muller Street Apt. 21 Anaheim, CA 92801', '', 'Rana AlMughrabi', '714-737-5148', 'Saer Mraish', '714-300-4835', 'yes', '1st, Pre 1', 'Emergency Contact:  Hazar Akil, Friend', '1', 1, 1, 1, '2018-09-01', '2', '2018-09-01 21:34:29', '2018-09-01 21:34:29'),
(39, 'Daniel Abusharar, 5 yrs, kindergarten ', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'info@abushararlaw.com', '747 S Crown Pointe drive. Anaheim, CA 92807', '9093465496', 'Lubna Albajjar', '9093465496', 'Akram Abusharar', '9097492812', 'yes', 'Pre-K', 'Egges', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 18:38:41', '2018-08-25 18:38:41'),
(40, 'Hamza Azez, 7, Male', 'Maria Azez, 7 Femal', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sarahalmagsoosi@gmail.com', '2740 w ball rd.  Atp#L4', '71460008144', 'Sarah Almagsoosi', '7146008144', 'Firas Azez', '6197922620', 'yes', 'K', 'penicillen', '1', 1, 1, 1, '2018-09-08', '4', '2018-08-25 18:40:11', '2018-08-25 18:40:11'),
(41, 'Shakir Abdallat 7 kindergarten ', 'Zayne Abdallat 5 p2', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dydoria@yahoo.com', '3228 e longridge dr orange ca92867', '', 'Doria yuan', '7144088864', 'Sal Abdallat ', '7143903206', 'yes', 'P2 P1', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 18:59:31', '2018-08-25 18:59:31'),
(42, 'Omar Abuhajjaj 4 years old transitional kindergarten', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abuhajjaj@yahoo.com', '5845 brushwood court', '4084208469', 'Karen', '4084208469', 'Iyad', '4084208469', 'yes', '', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 19:08:16', '2018-08-25 19:08:16'),
(44, 'Talia Bahrawy 3 ', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sbahrawy@hotmail.com', '2531 thistlewood lane, corona, ca 92882', '', 'dina kandil', '7144687770', 'mostafa bahrawy', '7148556555', 'no', '', '', '1', 1, 1, 1, '2018-08-25', '1', '2018-08-25 19:36:03', '2018-08-25 19:36:03'),
(46, 'Dalia Zidan, 9, 3rd', 'Ali Zidan, 6, KG', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zidansfun@gmail.com', '852 South Endicott Court Anaheim Hills 92808', '', 'Zaena Sabeh', '714-926-2295', 'Walid Zidan', '714-728-6063', 'yes', 'third, kg', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 19:54:29', '2018-08-25 19:54:29'),
(47, 'Faizaan Mamsa, 9, 4th', 'another email: abdur.mamsa@gmail.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uzma_munnee@yahoo.com', '8850 East Garden View Drive, Anaheim, CA 92808', '714-280-1411', 'Uzma Mamsa', '714-815-7397', 'Abdurrehman Mamsa', '714-815-7399', 'yes', '3rd', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 20:04:48', '2018-08-25 20:04:48'),
(48, 'Hamzeh kloub 8 , 3rd grade', 'Ameer kloub 6, 1st grade', 'Sondos karaki', 'Sondos karaki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'karaki99@yahoo.com', '409 S Windmill Ln , anaheim, ca 92807', '', 'Sondos kloub', '7143606866', 'Emad kloub', '7147196866', 'yes', '2nd grade, KG', '', '1', 1, 1, 1, '2018-08-25', '1', '2018-08-25 20:08:22', '2018-08-25 20:08:22'),
(49, 'Ayah Ismail', 'Jumana Ismail', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dgismail@aol.com', '9096 Fallbrook Canyon Drive, Corona, CA 92883', '', 'Donia Ismail', '714-600-5124', 'Samer Ismail', '714-274-5993', 'yes', 'second, kindergarten', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 20:21:19', '2018-08-25 20:21:19'),
(97, 'Mohammad mahamda , 4 , pre school', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ibrahemahamda@hotmail.com', '2633 e la Palma ave apt 122', '724-786-8038', 'Shereen imran', '7247868038', 'Ibrahim mahamda', '7247868036', 'yes', '', '', '1', 1, 1, 1, '2018-09-06', '0', '2018-09-06 18:58:13', '2018-09-06 18:58:13'),
(98, 'Sanna Nassr', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'drlinaa@yahoo.com', '18736 Turfway Park Yorba Linda ', '', 'Lina Abou Ayache ', '6268088507', 'Hussein Nassr ', '9149126578', 'yes', '1', 'None', '1', 1, 1, 1, '2018-09-06', '0', '2018-09-06 19:55:50', '2018-09-06 19:55:50'),
(52, 'Lilium Affani, 11, Beg 2', 'Talia Affani, 8, 1st grade', 'Sama Affani, 4, pre 2 ', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'neso_77@hotmail.com', '4 Wilking Drive, Irvine, CA 92620', '', 'Nisreen AlKhateeb', '949-419-7144', 'Feras Affani', '949-500-9322', 'yes', ' Beg 1, Kg, Pre 1', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-25 20:29:19', '2018-08-25 20:29:19'),
(101, 'Zara Khan, 7, 2nd grade', 'Taha Khan,4,kindergarten', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sabaejaz@live.com', '1219 s athena way apt #2, Anaheim CA 92806', '7148182229', 'Saba Ejaz', '7148182229', 'Umer Khan', '8189030846', 'yes', '1st grade', '', '1', 1, 1, 1, '2018-09-07', '4', '2018-09-08 04:52:46', '2018-09-08 04:52:46'),
(100, 'Maryam Maasarani, 6, 1st', 'Aminah Maasarani, 5, Kg', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zeina@zsqured.me', '17060 San Bruno St. Apt. F11', '7142615545', 'Zeina El-Kassem', '7142615545', 'Zeyad Maasarani', '13109268091', 'yes', 'Pre-k 2 and Kg', '', '1', 1, 1, 1, '2018-09-07', '2', '2018-09-07 13:38:59', '2018-09-07 13:38:59'),
(54, 'Yousif al rikabi', 'Yasir Al Rikabi', 'Yasir Al Rikabi', 'Yasir Al Rikabi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yasiralrikabi@gmail.com', '2828, W.Lincoln Ave', '17149242480', 'Sarah nafea', '7149242480', 'Yasir Al Rikabi', '7149242480', 'yes', 'Kindergarten ', '', '1', 1, 1, 1, '2018-08-25', '0', '2018-08-25 20:47:00', '2018-08-25 20:47:00'),
(56, 'Sereen', 'Mohammad', 'Sarah', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'firasabdulrahman@yahoo.com', '13032casa Linda ln apt b garden grove ca 92844', '7142093931', 'Rasha', '7144879846', 'Firas', '7142093931', 'yes', 'Dina (two of them), Mohammed is beginners 1, sereen first grade  ', '', '1', 1, 1, 1, '2018-08-25', '1', '2018-08-25 20:50:30', '2018-08-25 20:50:30'),
(57, 'Taaha Sheikh 10 Years 4th', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bushranoor_10@hotmail.com', '215 N. Magnolia Ave. Anaheim # D Anaheim CA 92801', 'N/A', 'Bushra Sheikh', '714-457-0253', 'Abdul Ghaffar Sheikh', '714-728-5931', 'yes', '', '', '1', 1, 1, 1, '2018-08-25', '1', '2018-08-25 20:56:41', '2018-08-25 20:56:41'),
(58, 'Jenna Abu Seer, 7, 1st', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Shantash_99@yahoo.com', '125 n. Maude ln, Anaheim 92807', '', 'Samah abu hantash', '7143887979', 'Ehab abu sair', '7146020002', 'yes', 'Kg', '', '1', 1, 1, 1, '2018-08-25', '0', '2018-08-25 20:59:07', '2018-08-25 20:59:07'),
(82, 'Tala Nasif , 10 years old , ', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hananalziq@gmail.com', '1700 west Cerritos ave unit 139 Anaheim ca 92804', '7143062673', 'Hanan zak', '7143062673', 'Ahmed nasif', '7143063133', 'yes', 'First grade', '', '1', 1, 1, 1, '2018-08-31', '3', '2018-08-31 15:55:41', '2018-08-31 15:55:41'),
(102, 'nancy fardoun, 5 kd', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zfardon@yahoo.com', '5225 edgemont cir', '7147467132', 'zahra fardoun', '7147477132', 'mohamad fardoun', '7147467143', 'yes', 'Pre 1', '', '1', 1, 0, 1, '2018-07-08', '2', '2018-09-08 16:05:26', '2018-09-08 16:05:26'),
(103, 'Dana shatat 10 second', 'Salim shatat 9 first', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hadoool_85@hotmal.com', '1421 S Knott Ave 31 anaheim ca 92804', '7148296149', 'Hadeel', '7148296149', 'Mohd', '9495021315', 'yes', 'Kinder first', '', '1', 1, 1, 1, '2018-09-08', '2', '2018-09-08 16:07:10', '2018-09-08 16:07:10'),
(63, 'Malek Habibeh, 5, Kg', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rama.sabeh@gmail.com', '8215 E. White Oak Ridge Unit 95, Orange, CA 92869', '', 'Rama Sabeh', '714-381-0362', 'Abdulkareem Habibeh', '714-624-9553', 'yes', 'Pre 2', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-26 00:06:31', '2018-08-26 00:06:31'),
(104, 'Laith Alrushiedat 4 years old, preschool', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nalrushiedat@fullerton.edu', '720 Cienaga Drive Fullerton CA 92835', '7148651629', 'Wafaa Alrushiedat', '7148644665', 'Nimer Alrushiedat', '7148651629', 'yes', '', '', '1', 1, 1, 1, '2018-09-08', '1', '2018-09-08 16:43:29', '2018-09-08 16:43:29'),
(65, 'Laura Aldajani, 7, first', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'lanalanon@gmail.com', '6407 E. Calle Del Norte Anaheim, CA 92807', '', 'Lana Aldajani', '408-833-0726', 'Tiem Aldajani', '707-738-1365', 'yes', 'Kg', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-26 00:09:09', '2018-08-26 00:09:09'),
(66, 'Tariq Abdelkarim, 11, 5th', 'Hatim AbdelKarim, 9, 4th', 'Ziyad Abdelkarim, 5, Kg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dkarim803@gmail.com', '85 Legacy Way Irvine, CA 92602', '', 'Donia Abdelkarim', '951-500-3082', 'Arafat Abdelkarim', '714-883-2510', 'yes', '4th, 3rd, Pre 2', '', '1', 1, 1, 1, '2018-08-25', '2', '2018-08-26 00:13:09', '2018-08-26 00:13:09'),
(67, 'Reem Awad, 12, 6th', 'Mohammed Awad, 8, 2nd', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ghadiaw@yahoo.com', '1779 Majestic Dr. Corona, CA 92880', '', 'Ghadeer Awad', '714-329-9868', 'N/A', 'N/A', 'yes', '5th, 1st', '', '1', 1, 1, 1, '2018-08-18', '2', '2018-08-26 00:15:16', '2018-08-26 00:15:16'),
(68, 'Hajar Alshekhle, 8, 2nd', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mahmood1_200y@yahoo.com', '712 E South St 203, Anaheim, CA 92805', '', 'Qutadah Mohammed', '714-865-2025', 'Mahmood Alshekhle', '714-723-4887', 'no', '', '', '1', 1, 1, 1, '2018-08-18', '1', '2018-08-26 00:17:49', '2018-08-26 00:17:49'),
(69, 'Rina Abdelbaky, 5, Pre 2', 'other email, elarabyy@hotmail.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dinasaied14@gmail.com', '310 S Jefferson St. Placentia, CA 92870', '', 'Dina Saied', '714-423-8977', 'Mahmoud Abdelbaky', '714-869-4320', 'yes', 'Pre 1', '', '1', 1, 1, 1, '2018-08-18', '1', '2018-08-26 00:20:10', '2018-08-26 00:20:10'),
(70, 'Loaay Elkamash, 6, 1st', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a.abouelazm@gmail.com', '180 N Muller St. #205 Anaheim, CA 92801', '', 'Sherin Abouelazm', '714-325-4607', 'Mohamed elkamash', '714-325-4566', 'yes', 'Kg', '', '1', 1, 1, 1, '2018-08-18', '1', '2018-08-26 00:21:57', '2018-08-26 00:21:57'),
(71, 'Noah Itani, 4, kg', 'other email cestrada2527@yahoo.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'khalil.itani1@gmail.com', '14740 San Esteban Dr. La Mirada, CA 90638', '', 'Celia Estrada', '310-895-8768', 'Khalil Itani', '310-895-5019', 'yes', 'Pre 2', '', '1', 1, 1, 1, '2018-08-18', '2', '2018-08-26 00:24:11', '2018-08-26 00:24:11'),
(72, 'Everilda Setiawan, 9, 2nd', 'other email wsetiawan3@yahoo.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rizana.huda@gmail.com', '18946 Vickie Ave #03 Cerritos, CA 90703', '', 'Rizana Huda', '714-924-2394', 'Willy Setiawan', '714-924-2356', 'yes', '1st', '', '1', 1, 1, 1, '2018-08-18', '2', '2018-08-26 00:26:28', '2018-08-26 00:26:28'),
(73, 'Adam Keshk, 8, 3rd', 'other email aiatraafat@yahoo.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'wkeshk@yahoo.com', '387 Draft Way Placentia, CA 92870', '', 'Aiat Keshk', '217-721-7065', 'Walied Keshk', '217-721-7321', 'yes', '2nd', '', '1', 1, 1, 1, '2018-08-18', '1', '2018-08-26 00:28:29', '2018-08-26 00:28:29'),
(74, 'Luna Bouhairi, 10, 3rd', 'other email karbou74@hotmail.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ramroumeh@hotmail.com', '124 Pendant Irvine CA 92620', '', 'Lamiaa Khayat', '949-300-0439', 'Karim Bouhairi', '949-300-4956', 'yes', '2nd', '', '1', 1, 1, 1, '2018-08-25', '1', '2018-08-26 00:36:49', '2018-08-26 00:36:49'),
(75, 'FATIMAH ALQAISI, 10 , 2nd Grade', 'ABDULLAH ALQAISI, 8 , 2nd Grade', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rafidalkaisi@gmail.com', '8113 Cerritos Ave Apt# 79 Stanton, CA 90680', '8187145181', 'Noor Alnayyar', '7142482873', 'Rafid Alkaisi', '8187145181', 'yes', '1 st grafe', 'Abdulla has a Sulfa Allergic', '1', 1, 1, 1, '2018-08-25', '0', '2018-08-26 09:20:11', '2018-08-26 09:20:11'),
(77, 'Leesa Qawasmeh, 11 5 th', 'Lelass Qawasmeh, 9 4th', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'leesaharoun@yahoo.com', '201 E Chapman Ave 41N Placentia CA 92870', '7142093694', 'Ghada Haroun', '7142093694', 'Haroun Qawasmeh', '7142092624', 'yes', '3rd 4th', '', '1', 1, 1, 1, '2018-08-28', '0', '2018-08-28 23:10:36', '2018-08-28 23:10:36'),
(78, 'Remi Obeid. 10 years,  4th grade ', 'Zain Obeid, 7 years, 1st grade', 'Ahmad Obeid', 'Ahmad Obeid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hadi411@hotmail.com', '5 Mesquite, Trabuco Canyon, CA 92679', '9512819206', 'Sali Dalloul', '5626074400', 'Ahmad Obeid', '9512819206', 'yes', 'Kindergarten and 3rd grade ', '', '1', 1, 1, 1, '2018-08-29', '0', '2018-08-30 06:21:09', '2018-08-30 06:21:09'),
(79, 'Zahra Dahir, 9yr, Grade 3', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alifadumo673@gmail.com', '111 W Elm Street #409', '', 'Fadumo Ali', '7144616764', 'Abdirashid Dahir ', '7144616764', 'yes', '2 ', '', '1', 1, 1, 1, '2018-08-30', '0', '2018-08-30 16:21:24', '2018-08-30 16:21:24'),
(81, 'Malak Alashqar 11 6rd', 'Lamah Alashqar 9 4rd', 'Lyan alashqar', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'haninjaber2006@hotmail.com', '3146 W Ball Rd # 13', '17147231091', 'Hanin jaber', '7147231091', 'Hesham alashqar', '7147262004', 'yes', '5rd , 3rd ', 'None ', '1', 1, 1, 1, '2018-08-30', '0', '2018-08-30 23:14:38', '2018-08-30 23:14:38'),
(118, 'Rashes almawajdeh 5 kg', 'Randa almawajdeh r preschool', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'moe_832000@yahoo.com', '1791 e warrenton ave Anaheim ca 92805', '7143627818', 'Dina alzubi', '7143868231', 'Mohammed almawajdeh', '7143627818', 'no', '', '', '1', 1, 1, 1, '2018-09-15', '1', '2018-09-15 15:22:28', '2018-09-15 15:22:28'),
(107, 'Abdelhady zayed', 'Heyam zayed', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jamlaza@yahoo.com', '1218 eckenroad placentia', '714 906-6415', 'Jamla', '714 906-6414', 'Mohamad', '714 496-3152', 'yes', '5 and 6', 'Jamla zaid', '1', 1, 1, 1, '2018-09-08', '0', '2018-09-08 16:57:03', '2018-09-08 16:57:03'),
(108, 'Hend', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tonyz1510@iclould.com', '4661 plumosa 15', '7146959234', 'Rehab', '7144690572', 'Toafek', '7145250866', 'yes', 'Gk', 'None', '0', 0, 0, 0, '2018-09-08', '0', '2018-09-08 17:01:30', '2018-09-08 17:01:30'),
(109, 'Amani abdo 12.5 first grade', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alanabdo@gmail.com', '11774 tulip ct fountain valley', '714-915-6666', 'Bana abdo', '714-679-2400', 'Ahmad abdo', '714-915-6666', 'yes', 'Beginners', '', '1', 1, 1, 1, '2018-09-08', '0', '2018-09-08 17:21:07', '2018-09-08 17:21:07'),
(117, 'sireen taha 8 second grade', 'amjad taha 6 first grade', 'illiyeen taha 5 kindergarten ', 'dean taha 3 preschool ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mawwa001@gmail.com', '26930 red maple st', '9514913272', 'manal taha', '9514913272', 'abed taha', '9518130604', 'no', '', '', '1', 1, 1, 1, '2019-09-14', '2', '2018-09-14 23:07:58', '2018-09-14 23:07:58'),
(111, 'Lina  Abdel Kader , 5, preschool ', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sarahanafy1984@hotmail.com', '4291 green avenue , Los Alamitos ', '', 'Sara ', '714 406 7163', 'Mohamed ', '213 259 6207', 'yes', 'Preschool 1', '', '1', 1, 1, 1, '2018-09-08', '2', '2018-09-08 17:24:13', '2018-09-08 17:24:13'),
(112, 'Kareem Mouataz Mansour , 6, 1st grade. ', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hooha2009@yahoo.com', '714 South Webster Ave, Anaheim CA 92804', '', 'Maha Adel El Serafy ', '7143316380', 'moutaz', '7145979670', 'yes', 'kinder garden', 'no', '1', 1, 1, 1, '2018-09-08', '2', '2018-09-08 17:31:23', '2018-09-08 17:31:23'),
(113, 'Zayna palmer 5 pre2', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'neveen.palmer@gmail.com', '3020 Yorba Linda Blvd apt R12', '', 'Neveen Palmer', '7145616033', 'Muhammad Palmer', '7142026161', 'no', '', '', '1', 1, 1, 1, '2018-09-08', '3', '2018-09-08 17:33:04', '2018-09-08 17:33:04'),
(114, 'Omar  Alhammouri  12  first grade', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'manalbarakat@yahoo.com', '2856 W lincoln Ave Apt k2', '714-737-6747', 'manal barakat', '714-737-6747', 'nabih alhammouri', '714-499-8706', 'yes', 'beginner', 'none', '1', 1, 1, 1, '2018-09-08', '9', '2018-09-08 17:37:19', '2018-09-08 17:37:19'),
(115, 'Leanne Jubara, 8, 2', 'Sarah Jubara, 4, preschool', 'Maram Maithalouni', 'Maram Maithalouni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'maithalouni@gmail.comt', '17308 De Groot Pl, Cerrits CA 90703', '9493255663', 'Maram Maithalouni', '9493255663', 'Aam Jubara', '9493255664', 'no', '', '', '1', 1, 1, 1, '2018-09-08', '2', '2018-09-08 17:43:10', '2018-09-08 17:43:10'),
(116, 'Nawal Hassan, 3, Preshool 1', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'denahassan5@icloud.com', '13061 Lampson Ave Garden Grove, CA 92840', '', 'Dena Hassan', '714-858-3252', 'Aleyyan Hassan', '714-595-0565', 'no', '', '', '1', 1, 1, 1, '2018-09-08', '2', '2018-09-08 18:08:35', '2018-09-08 18:08:35'),
(119, 'DONIA BASSIOUNI, 11 YEARS, BEGINER1', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hebbssc@aol.com', '706 San Diego Lane', '5626502441', 'HEBA HAMOUDA', '5626502441', 'IHAB SHEHATA', '5626502441', 'yes', 'BEGINER1', 'DIABITAIS', '1', 1, 1, 1, '2018-09-15', '1', '2018-09-15 16:06:34', '2018-09-15 16:06:34'),
(120, 'Bassam third grade 8 years', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'daliaeid21@yahoo.com', '1667 s diamond bar blvd', '9099937606', 'Dalia', '9099937606', 'Mahmoud', '9092039513', 'yes', '3', 'No', '1', 1, 0, 1, '2018-09-15', '3', '2018-09-15 16:21:10', '2018-09-15 16:21:10'),
(125, 'Mahmoud Morra, 6', 'Zaina Morra, 6', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hamayel_sanad@yahoo.com', '2180 West Hiawatha Avenue Anaheim, CA 92804', '', 'Reem Morra', '323-916-1618', 'Iyad Morra', '323-373-6468', 'no', '', '', '1', 1, 1, 1, '2018-09-15', '1', '2018-09-15 19:40:42', '2018-09-15 19:40:42'),
(122, 'Hana Mohammad, 11, 5th grade', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'basma.arar@yahoo.com', '520 East Ruverdale Orange, CA 92865', '', 'Basma Arar', '714-287-3139', 'Asmar Mohammad', '714-281-4683', 'yes', '4th', '', '1', 1, 1, 1, '2018-09-15', '2', '2018-09-15 16:33:43', '2018-09-15 16:33:43'),
(123, 'Khalid Zayid, 7, Second Grade', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'walaydie@gmail.com', '825 Tamarack Ave Apt. #135 Brea, CA ', '', 'Wisal Alaydie', '703-677-7789', 'Nadar Zayid', '703-655-8283', 'no', '', '', '1', 1, 1, 1, '2018-09-15', '2', '2018-09-15 17:14:57', '2018-09-15 17:14:57'),
(124, 'Salma Ayesh, 3, Preschool 1', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'z.a_joy@yahoo.com', '2633 East La Palma Ave Apt #156 Anaheim, CA 92806', '', 'Zain Abu Farha', '562-713-2415', 'Abdelrahman ', '562-242-6641', 'no', '', '', '1', 1, 1, 1, '2018-09-15', '2', '2018-09-15 17:28:46', '2018-09-15 17:28:46'),
(126, 'ddd', 'ewewqeqw', 'dssfsdf', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sanfora200@hotmail.com', '6+', '6+', '6+6+', '6+', '+6', '+6', 'yes', '6+6+', '+6', '1', 0, 1, 1, '2018-10-13', '1', '2018-10-04 05:41:33', '2018-10-04 05:41:33'),
(127, 'ddd', 'ewewqeqw', 'dssfsdf', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sanfora200@hotmail.com', '6+', '6+', '6+6+', '6+', '+6', '+6', 'yes', '6+6+', '+6', '1', 0, 1, 1, '2018-10-13', '1', '2018-10-04 05:41:56', '2018-10-04 05:41:56'),
(128, 'ddd', 'ewewqeqw', 'dssfsdf', '6', '8956', '96', '66+', '6+', '965', '36', '6', '6', 'sanfora200@hotmail.com', '6+', '6+', '6+6+', '6+', '+6', '+6', 'yes', '6+6+', '+6', '1', 0, 1, 1, '2018-10-13', '1', '2018-10-04 06:03:28', '2018-10-04 06:03:28'),
(129, 'ddd', 'ewewqeqw', 'dssfsdf', '6', '8956', '96', '66+', '6+', '965', '36', '6', '6', 'sanfora200@hotmail.com', '6+', '6+', '6+6+', '6+', '+6', '+6', 'yes', '6+6+', '+6', '1', 0, 1, 1, '2018-10-13', '1', '2018-10-04 06:03:53', '2018-10-04 06:03:53'),
(130, 'ddd', 'ewewqeqw', 'dssfsdf', '6', '8956', '96', '66+', '6+', '965', '36', '6', '6', 'sanfora200@hotmail.com', '6+', '6+', '6+6+', '6+', '+6', '+6', 'yes', '6+6+', '+6', '1', 0, 1, 1, '2018-10-13', '1', '2018-10-04 06:04:20', '2018-10-04 06:04:20'),
(131, 'ddd', 'ewewqeqw', 'dssfsdf', '6', '8956', '96', '66+', '6+', '965', '36', '6', '6', 'sanfora200@hotmail.com', '6+', '6+', '6+6+', '6+', '+6', '+6', 'yes', '6+6+', '+6', '1', 0, 1, 1, '2018-10-13', '1', '2018-10-04 06:05:39', '2018-10-04 06:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `report_cards`
--

CREATE TABLE `report_cards` (
  `id` int(11) NOT NULL,
  `student_record_id` int(11) NOT NULL,
  `semester` tinyint(4) NOT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `quran_teacher` varchar(255) DEFAULT NULL,
  `arabic_teacher` varchar(255) DEFAULT NULL,
  `islamic_studies_teacher` varchar(255) DEFAULT NULL,
  `teacher_comments` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `report_cards`
--

INSERT INTO `report_cards` (`id`, `student_record_id`, `semester`, `teacher`, `quran_teacher`, `arabic_teacher`, `islamic_studies_teacher`, `teacher_comments`, `created_at`, `updated_at`) VALUES
(1, 94, 1, NULL, 'gggggggxxxggggggggg', 'oooooooooooooo', 'dsdsds', NULL, '2018-09-16 07:14:17', '2018-09-16 11:22:48'),
(2, 164, 1, 'ffsdfdsfsdfsd', NULL, NULL, NULL, 'dsssssssssssssssssssss', '2018-09-17 09:16:56', '2018-09-17 09:37:27'),
(3, 177, 1, 'first semester', NULL, NULL, NULL, 'first semester', '2018-09-17 11:34:36', '2018-09-17 11:34:36'),
(4, 177, 2, 'second semester', NULL, NULL, NULL, 'second semester', '2018-09-17 11:34:53', '2018-09-17 11:34:53'),
(5, 94, 2, NULL, 'mmmmmmmmmmmm', 'fghjkl;', 'JK', NULL, '2018-09-18 08:47:59', '2018-09-18 08:47:59'),
(6, 190, 1, NULL, 'cxcxcx', '', '', NULL, '2018-09-18 09:09:18', '2018-09-18 09:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_name`, `grade_id`, `created_at`, `updated_at`, `deleted`) VALUES
(20, 'THIRD: 209', 29, '2018-06-21 08:31:11', '2018-08-26 21:04:20', 0),
(21, 'BOOK CLUB', 35, '2018-06-25 06:14:09', '2018-08-26 21:04:51', 0),
(22, 'GIRLS YOUTH', 36, '2018-07-02 07:02:40', '2018-08-26 21:05:15', 0),
(23, 'FOURTH: 208', 28, '2018-07-08 11:40:37', '2018-08-26 21:03:48', 0),
(24, 'Yellow', 23, '2018-07-16 11:12:03', '2018-08-26 20:28:40', 0),
(25, 'Red', 23, '2018-07-16 11:12:06', '2018-08-26 20:28:28', 0),
(27, 'FIFTH: 205', 27, '2018-07-19 12:30:22', '2018-08-26 21:03:18', 0),
(29, 'SIXTH: 210', 26, '2018-07-19 12:30:50', '2018-08-26 21:02:45', 0),
(30, 'Green', 24, '2018-08-26 20:33:02', '2018-08-26 20:33:02', 0),
(31, 'Blue', 25, '2018-08-26 20:38:54', '2018-08-26 20:38:54', 0),
(32, 'FIRST: 204', 40, '2018-08-26 21:05:42', '2018-08-26 21:05:42', 0),
(33, 'SECOND: 203 (112)', 41, '2018-08-26 21:07:24', '2018-08-26 21:07:24', 0),
(34, 'BEGINNERS 1: 206 (103)', 42, '2018-08-26 21:09:54', '2018-08-26 21:09:54', 0),
(35, 'UNKNOWN', 43, '2018-09-01 00:50:24', '2018-09-01 00:50:24', 0),
(36, '2', 25, '2018-09-16 11:55:42', '2018-09-16 11:55:42', 0),
(37, '3', 25, '2018-09-16 11:55:45', '2018-09-16 11:55:45', 0),
(38, '4', 25, '2018-09-16 11:55:51', '2018-09-16 11:55:51', 0),
(39, '5', 25, '2018-09-16 11:55:56', '2018-09-16 11:55:56', 0),
(40, '6', 25, '2018-09-16 11:56:00', '2018-09-16 11:56:00', 0),
(41, 'A', 25, '2018-09-16 11:56:05', '2018-09-16 11:56:05', 0),
(42, 'c', 25, '2018-09-16 11:56:14', '2018-09-16 11:56:14', 0),
(43, 'D', 25, '2018-09-16 11:56:25', '2018-09-16 11:56:25', 0),
(44, 'g', 25, '2018-09-16 11:56:39', '2018-09-16 11:56:39', 0),
(45, 'h', 25, '2018-09-16 11:56:48', '2018-09-16 11:56:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rooms_attendances`
--

CREATE TABLE `rooms_attendances` (
  `id` int(11) NOT NULL,
  `attendance_date` date NOT NULL,
  `room_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms_attendances`
--

INSERT INTO `rooms_attendances` (`id`, `attendance_date`, `room_id`, `created_at`, `updated_at`) VALUES
(40, '2018-08-18', 20, '2018-08-25 11:32:32', '2018-08-25 11:32:32'),
(41, '2018-08-11', 21, '2018-08-25 11:33:25', '2018-08-25 11:33:25'),
(42, '2018-08-26', 25, '2018-08-26 18:35:04', '2018-08-26 18:35:04'),
(43, '2018-09-01', 24, '2018-09-02 05:32:15', '2018-09-02 05:32:15'),
(44, '2018-09-16', 33, '2018-09-16 05:47:51', '2018-09-16 05:47:51');

-- --------------------------------------------------------

--
-- Table structure for table `rooms_teachers`
--

CREATE TABLE `rooms_teachers` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms_teachers`
--

INSERT INTO `rooms_teachers` (`id`, `room_id`, `teacher_id`, `subject_id`, `year`, `created_at`, `updated_at`) VALUES
(5, 32, 31, 308, 2018, '2018-09-01 23:58:31', '2018-09-01 23:58:31'),
(6, 32, 31, 309, 2018, '2018-09-01 23:58:43', '2018-09-01 23:58:43'),
(7, 32, 42, 309, 2018, '2018-09-01 23:59:22', '2018-09-01 23:59:22'),
(8, 33, 23, 313, 2018, '2018-09-02 00:00:05', '2018-09-02 00:00:05'),
(13, 30, 26, 287, 2018, '2018-09-02 00:04:13', '2018-09-02 00:04:13'),
(14, 30, 26, 286, 2018, '2018-09-02 00:04:26', '2018-09-02 00:04:26'),
(17, 34, 19, 317, 2018, '2018-09-02 00:07:43', '2018-09-02 00:07:43'),
(18, 34, 19, 316, 2018, '2018-09-02 00:07:54', '2018-09-02 00:07:54'),
(19, 35, 19, 321, 2018, '2018-09-02 00:08:09', '2018-09-02 00:08:09'),
(20, 35, 19, 320, 2018, '2018-09-02 00:08:21', '2018-09-02 00:08:21'),
(21, 29, 19, 295, 2018, '2018-09-02 00:08:33', '2018-09-02 00:08:33'),
(22, 29, 19, 294, 2018, '2018-09-02 00:08:43', '2018-09-02 00:08:43'),
(24, 32, 31, 334, 2018, '2018-09-02 00:18:03', '2018-09-02 00:18:03'),
(25, 32, 25, 307, 2018, '2018-09-02 00:19:50', '2018-09-02 00:19:50'),
(26, 34, 25, 315, 2018, '2018-09-02 00:20:01', '2018-09-02 00:20:01'),
(27, 31, 20, 288, 2018, '2018-09-02 00:21:23', '2018-09-02 00:21:23'),
(28, 32, 20, 306, 2018, '2018-09-02 00:21:33', '2018-09-02 00:21:33'),
(29, 33, 23, 348, 2018, '2018-09-02 00:24:35', '2018-09-02 00:24:35'),
(30, 32, 42, 347, 2018, '2018-09-02 00:25:11', '2018-09-02 00:25:11'),
(31, 30, 26, 326, 2018, '2018-09-02 00:25:43', '2018-09-02 00:25:43'),
(32, 30, 26, 285, 2018, '2018-09-02 00:25:55', '2018-09-02 00:25:55'),
(33, 34, 19, 336, 2018, '2018-09-02 00:26:32', '2018-09-02 00:26:32'),
(34, 35, 19, 337, 2018, '2018-09-02 00:26:41', '2018-09-02 00:26:41'),
(35, 29, 19, 328, 2018, '2018-09-02 00:26:52', '2018-09-02 00:26:52'),
(36, 29, 22, 341, 2018, '2018-09-02 00:27:07', '2018-09-02 00:27:07'),
(37, 23, 35, 343, 2018, '2018-09-02 00:27:21', '2018-09-02 00:27:21'),
(38, 33, 33, 310, 2018, '2018-09-02 00:28:23', '2018-09-02 00:28:23'),
(39, 20, 33, 300, 2018, '2018-09-02 00:28:31', '2018-09-02 00:28:31'),
(40, 33, 18, 335, 2018, '2018-09-02 00:28:58', '2018-09-02 00:28:58'),
(41, 25, 38, 283, 2018, '2018-09-02 00:30:05', '2018-09-02 00:30:05'),
(42, 23, 16, 322, 2018, '2018-09-02 00:30:54', '2018-09-02 00:30:54'),
(43, 22, 37, 346, 2018, '2018-09-02 00:31:14', '2018-09-02 00:31:14'),
(44, 31, 28, 289, 2018, '2018-09-02 00:31:33', '2018-09-02 00:31:33'),
(45, 31, 28, 327, 2018, '2018-09-02 00:31:42', '2018-09-02 00:31:42'),
(46, 25, 32, 325, 2018, '2018-09-02 00:32:07', '2018-09-02 00:32:07'),
(47, 25, 32, 282, 2018, '2018-09-02 00:32:18', '2018-09-02 00:32:18'),
(48, 27, 39, 342, 2018, '2018-09-02 00:32:49', '2018-09-02 00:32:49'),
(49, 24, 27, 325, 2018, '2018-09-02 00:33:12', '2018-09-02 00:33:12'),
(50, 24, 27, 283, 2018, '2018-09-02 00:33:25', '2018-09-02 00:33:25'),
(51, 27, 34, 297, 2018, '2018-09-02 00:34:46', '2018-09-02 00:34:46'),
(52, 29, 34, 293, 2018, '2018-09-02 00:34:58', '2018-09-02 00:34:58'),
(53, 22, 34, 305, 2018, '2018-09-02 00:35:08', '2018-09-02 00:35:08'),
(54, 20, 36, 330, 2018, '2018-09-02 00:35:49', '2018-09-02 00:35:49'),
(55, 27, 36, 329, 2018, '2018-09-02 00:36:02', '2018-09-02 00:36:02'),
(56, 31, 40, 340, 2018, '2018-09-02 00:36:20', '2018-09-02 00:36:20'),
(57, 24, 44, 282, 2018, '2018-09-02 00:36:54', '2018-09-02 00:36:54'),
(58, 21, 41, 333, 2018, '2018-09-02 00:37:21', '2018-09-02 00:37:21'),
(59, 30, 45, 339, 2018, '2018-09-02 00:37:48', '2018-09-02 00:37:48'),
(60, 30, 45, 284, 2018, '2018-09-02 00:37:58', '2018-09-02 00:37:58'),
(61, 33, 20, 353, 2018, '2018-09-16 11:11:38', '2018-09-16 11:11:38'),
(62, 33, 20, 354, 2018, '2018-09-16 11:11:53', '2018-09-16 11:11:53'),
(63, 27, 19, 297, 2018, '2018-09-19 12:47:39', '2018-09-19 12:47:39'),
(64, 35, 19, 350, 2018, '2018-09-19 12:48:01', '2018-09-19 12:48:01'),
(65, 35, 16, 350, 2018, '2018-09-20 05:54:55', '2018-09-20 05:54:55'),
(66, 30, 19, 339, 2018, '2018-09-20 05:57:06', '2018-09-20 05:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `setting_key` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting_key`, `setting_value`, `created_at`, `updated_at`) VALUES
(1, 'year', '2018', '2018-06-04 00:00:00', '2018-07-24 13:15:33'),
(2, 'school_contact_email', 'saturday@minaretacademy.net', '2018-07-29 00:00:00', '2018-09-01 10:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `id` int(11) NOT NULL,
  `stage_name` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stages`
--

INSERT INTO `stages` (`id`, `stage_name`, `created_at`, `updated_at`) VALUES
(1, 'downstairs', '2018-06-02 12:53:34', '2018-06-02 12:55:40'),
(2, 'upstairs', '2018-06-02 12:55:49', '2018-06-02 12:55:49'),
(3, 'no_stage', '2018-06-02 12:55:49', '2018-06-02 12:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_notes` text NOT NULL,
  `personal_image` text,
  `grade` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` enum('male','female') NOT NULL DEFAULT 'female',
  `parent_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_name`, `student_notes`, `personal_image`, `grade`, `age`, `gender`, `parent_id`, `created_at`, `updated_at`, `deleted`) VALUES
(34, 'Malak ', 'No notes', NULL, '', 11, 'female', 24, '2018-08-31 05:01:14', '2018-09-01 18:21:22', 0),
(35, 'Lamah ', 'No notes', NULL, '', 9, 'female', 24, '2018-08-31 05:02:00', '2018-09-01 18:21:17', 0),
(40, 'Zain', 'No notes', NULL, '', 7, 'male', 25, '2018-08-31 05:13:32', '2018-09-01 18:21:10', 0),
(41, 'Remi', 'No notes', NULL, '', 10, 'male', 25, '2018-08-31 05:21:22', '2018-09-01 18:21:04', 0),
(42, 'Zahra', 'No notes', NULL, '', 9, 'female', 26, '2018-08-31 05:22:15', '2018-09-01 18:20:54', 0),
(43, 'Leesa', 'No notes', NULL, '', 11, 'female', 27, '2018-08-31 05:31:16', '2018-09-01 18:20:48', 0),
(44, 'Lelass', 'No notes', NULL, '', 9, 'female', 27, '2018-08-31 05:31:52', '2018-09-01 18:20:42', 0),
(45, 'Abdallah', 'No notes', NULL, '', 8, 'male', 28, '2018-08-31 05:37:37', '2018-09-01 18:20:35', 0),
(46, 'Fatimah', 'No notes', NULL, '', 10, 'female', 28, '2018-08-31 05:38:03', '2018-09-01 18:20:20', 0),
(47, 'Luna', 'No notes', NULL, '', 10, 'female', 29, '2018-08-31 05:46:47', '2018-09-01 18:20:14', 0),
(48, 'Adam', 'No notes', NULL, '', 8, 'male', 30, '2018-08-31 05:50:20', '2018-09-01 18:20:08', 0),
(51, 'Tala', 'No notes', NULL, '', 10, 'female', 32, '2018-08-31 19:06:56', '2018-09-01 18:20:02', 0),
(54, 'Everilda', 'No notes', NULL, '', 9, 'female', 34, '2018-08-31 20:41:01', '2018-09-01 18:19:56', 0),
(55, 'Noah', 'No notes', NULL, '', 4, 'male', 35, '2018-08-31 20:46:05', '2018-09-01 18:19:51', 0),
(56, 'Loaay', 'No notes', NULL, '', 7, 'male', 36, '2018-08-31 23:00:44', '2018-09-01 18:19:41', 0),
(57, 'Rina', 'No notes', NULL, '', 5, 'female', 37, '2018-08-31 23:05:30', '2018-09-01 18:19:36', 0),
(58, 'Hajar', 'No notes', NULL, '', 8, 'female', 38, '2018-08-31 23:10:56', '2018-09-01 18:19:31', 0),
(59, 'Reem', 'No notes', NULL, '', 12, 'female', 39, '2018-08-31 23:17:07', '2018-09-01 18:19:25', 0),
(60, 'Mohammed', 'No notes', NULL, '', 8, 'male', 39, '2018-08-31 23:17:31', '2018-09-01 18:19:19', 0),
(61, 'Tariq', 'No notes', NULL, '', 11, 'male', 40, '2018-08-31 23:22:07', '2018-09-01 18:19:10', 0),
(62, 'Hatim', 'No notes', NULL, '', 9, 'male', 40, '2018-08-31 23:28:49', '2018-09-01 18:19:04', 0),
(63, 'Ziyad', 'No notes', NULL, '', 5, 'male', 40, '2018-08-31 23:29:06', '2018-09-01 18:18:59', 0),
(64, 'Haroun', 'No notes', NULL, '', 7, 'male', 41, '2018-08-31 23:44:10', '2018-09-01 18:18:49', 0),
(65, 'Imraan', 'No notes', NULL, '', 4, 'male', 41, '2018-08-31 23:44:37', '2018-09-01 18:18:43', 0),
(66, 'Laura', 'No notes', NULL, '', 7, 'female', 42, '2018-08-31 23:49:17', '2018-09-01 18:18:33', 0),
(67, 'Malek', 'No notes', NULL, '', 5, 'male', 43, '2018-08-31 23:52:26', '2018-09-01 18:18:21', 0),
(68, 'Jenna', 'No notes', NULL, '', 7, 'female', 44, '2018-09-01 00:10:49', '2018-09-01 00:10:49', 0),
(69, 'Taaha', 'No notes', NULL, '', 10, 'male', 45, '2018-09-01 00:21:46', '2018-09-01 00:21:46', 0),
(70, 'Sarah', 'No notes', NULL, '', 0, 'female', 46, '2018-09-01 00:31:00', '2018-09-01 00:31:00', 0),
(71, 'Sereen', 'No notes', NULL, '', 0, 'female', 46, '2018-09-01 00:31:48', '2018-09-01 00:31:48', 0),
(72, 'Mohammad', 'No notes', NULL, '', 0, 'male', 46, '2018-09-01 00:33:35', '2018-09-01 00:33:35', 0),
(73, 'Yousif', 'No notes', NULL, '', 0, 'male', 47, '2018-09-01 00:41:13', '2018-09-01 00:41:13', 0),
(74, 'Lilium', 'No notes', NULL, '', 11, 'female', 48, '2018-09-01 00:45:28', '2018-09-01 00:45:28', 0),
(75, 'Talia', 'No notes', NULL, '', 8, 'female', 48, '2018-09-01 00:45:44', '2018-09-01 00:45:44', 0),
(76, 'Sama', 'No notes', NULL, '', 4, 'female', 48, '2018-09-01 00:45:58', '2018-09-01 00:45:58', 0),
(77, 'Ayah', 'No notes', NULL, '', 0, 'female', 49, '2018-09-01 00:55:19', '2018-09-01 00:55:19', 0),
(78, 'Jumana', 'No notes', NULL, '', 0, 'female', 49, '2018-09-01 00:55:57', '2018-09-01 00:55:57', 0),
(79, 'Hamzeh', 'No notes', NULL, '', 8, 'male', 50, '2018-09-01 01:02:52', '2018-09-01 01:02:52', 0),
(80, 'Ameer', 'No notes', NULL, '', 6, 'male', 50, '2018-09-01 01:07:39', '2018-09-01 01:07:39', 0),
(81, 'Faizaan', 'No notes', NULL, '', 9, 'male', 51, '2018-09-01 01:11:25', '2018-09-01 01:11:25', 0),
(82, 'Dalia', 'No notes', NULL, '', 9, 'female', 52, '2018-09-01 01:19:39', '2018-09-01 01:19:39', 0),
(83, 'Ali', 'No notes', NULL, '', 6, 'male', 52, '2018-09-01 01:20:22', '2018-09-01 01:20:22', 0),
(84, 'Talia', 'No notes', NULL, '', 3, 'female', 53, '2018-09-01 01:36:28', '2018-09-01 01:36:28', 0),
(85, 'Omar', 'No notes', NULL, '', 4, 'male', 54, '2018-09-01 01:39:44', '2018-09-01 01:39:44', 0),
(86, 'Shakir', 'No notes', NULL, '', 7, 'male', 55, '2018-09-01 01:49:03', '2018-09-01 01:49:03', 0),
(87, 'Zayne', 'No notes', NULL, '', 5, 'male', 55, '2018-09-01 01:49:16', '2018-09-01 01:49:16', 0),
(88, 'Hamza', 'No notes', NULL, '', 7, 'male', 56, '2018-09-01 01:56:43', '2018-09-01 01:56:43', 0),
(89, 'Maria', 'No notes', NULL, '', 7, 'female', 56, '2018-09-01 01:57:28', '2018-09-01 01:57:51', 0),
(90, 'Daniel', 'No notes', NULL, '', 5, 'male', 57, '2018-09-01 02:15:39', '2018-09-01 02:15:39', 0),
(91, 'Laila', 'No notes', NULL, '', 10, 'female', 58, '2018-09-01 02:35:32', '2018-09-01 02:35:32', 0),
(92, 'Bayan', 'No notes', NULL, '', 10, 'female', 59, '2018-09-01 02:37:56', '2018-09-01 02:37:56', 0),
(93, 'Bilal', 'No notes', NULL, '', 7, 'male', 59, '2018-09-01 02:38:07', '2018-09-01 02:38:07', 0),
(94, 'Lara', 'No notes', NULL, '', 5, 'female', 60, '2018-09-01 02:53:25', '2018-09-01 02:53:25', 0),
(95, 'Sannah', 'No notes', NULL, '', 5, 'female', 61, '2018-09-01 02:59:23', '2018-09-01 02:59:23', 0),
(96, 'Yaseen', 'No notes', NULL, '', 10, 'male', 62, '2018-09-01 03:03:35', '2018-09-01 03:03:35', 0),
(97, 'Safiya', 'No notes', NULL, '', 8, 'female', 63, '2018-09-01 03:37:33', '2018-09-01 03:37:33', 0),
(98, 'Hisham', 'No notes', NULL, '', 3, 'male', 63, '2018-09-01 03:37:53', '2018-09-01 03:37:53', 0),
(99, 'Hassan', 'No notes', NULL, '', 9, 'male', 64, '2018-09-01 04:07:22', '2018-09-01 04:07:22', 0),
(101, 'Niveen', 'No notes', NULL, '', 9, 'female', 64, '2018-09-01 04:08:24', '2018-09-01 04:08:46', 0),
(102, 'Zayd', 'No notes', NULL, '', 6, 'male', 64, '2018-09-01 04:09:05', '2018-09-01 04:09:05', 0),
(103, 'Ayah', 'No notes', NULL, '', 10, 'female', 65, '2018-09-01 04:17:08', '2018-09-01 04:17:08', 0),
(104, 'Saja', 'No notes', NULL, '', 7, 'female', 65, '2018-09-01 04:17:25', '2018-09-01 04:17:25', 0),
(105, 'Hamza', 'No notes', NULL, '', 11, 'male', 65, '2018-09-01 04:20:02', '2018-09-01 04:20:02', 0),
(108, 'std_test', '-', NULL, '', 5, 'female', 67, '2018-09-01 10:27:23', '2018-09-01 10:27:23', 0),
(110, 'Talin', 'No notes', NULL, '', 4, 'female', 69, '2018-09-01 18:14:21', '2018-09-01 18:14:21', 0),
(111, 'Layal', 'No notes', NULL, '', 5, 'female', 70, '2018-09-01 18:42:09', '2018-09-01 18:42:09', 0),
(112, 'Lily', 'No notes', NULL, '', 5, 'female', 71, '2018-09-01 18:45:28', '2018-09-01 18:45:28', 0),
(113, 'Liam', 'No notes', NULL, '', 6, 'male', 72, '2018-09-01 18:48:48', '2018-09-01 18:48:48', 0),
(115, 'Jodi', 'No notes', NULL, '', 0, 'female', 73, '2018-09-02 01:00:18', '2018-09-02 01:00:18', 0),
(116, 'Jad', 'No notes', NULL, '', 13, 'male', 74, '2018-09-02 01:02:21', '2018-09-02 01:02:21', 0),
(117, 'Ghazal', 'No notes', NULL, '', 9, 'female', 75, '2018-09-02 01:09:59', '2018-09-02 01:09:59', 0),
(118, 'Samer', 'No notes', NULL, '', 10, 'male', 76, '2018-09-02 01:12:12', '2018-09-02 01:12:12', 0),
(119, 'Mustapha', 'No notes', NULL, '', 8, 'male', 76, '2018-09-02 01:12:25', '2018-09-02 01:12:25', 0),
(120, 'Omar', 'No notes', NULL, '', 6, 'male', 77, '2018-09-02 01:16:47', '2018-09-02 01:16:47', 0),
(121, 'Adam', 'No notes', NULL, '', 4, 'male', 77, '2018-09-02 01:16:54', '2018-09-02 01:16:54', 0),
(122, 'Zoher', 'No notes', NULL, '', 8, 'male', 78, '2018-09-02 01:20:03', '2018-09-02 01:20:03', 0),
(123, 'Leen', 'No notes', NULL, '', 6, 'female', 78, '2018-09-02 01:20:13', '2018-09-02 01:20:13', 0),
(124, 'Lulia', 'No notes', NULL, '', 4, 'female', 78, '2018-09-02 01:20:26', '2018-09-02 01:20:26', 0),
(125, 'Robbie', 'No notes', NULL, '', 5, 'male', 79, '2018-09-02 01:24:28', '2018-09-02 01:24:28', 0),
(126, 'Katie', 'No notes', NULL, '', 11, 'female', 79, '2018-09-02 01:24:36', '2018-09-02 01:24:36', 0),
(127, 'Farah', 'No notes', NULL, '', 12, 'female', 80, '2018-09-02 01:28:24', '2018-09-02 01:28:24', 0),
(128, 'Ahmad', 'No notes', NULL, '', 4, 'male', 80, '2018-09-02 01:28:31', '2018-09-02 01:28:31', 0),
(129, 'Zayed', 'No notes', NULL, '', 6, 'male', 81, '2018-09-02 01:31:47', '2018-09-02 01:31:47', 0),
(130, 'Mohammed', 'No notes', NULL, '', 4, 'male', 81, '2018-09-02 01:32:01', '2018-09-02 01:32:01', 0),
(131, 'Mona', 'No notes', NULL, '', 12, 'female', 82, '2018-09-02 01:35:04', '2018-09-02 01:35:04', 0),
(132, 'Hannah', 'No notes', NULL, '', 10, 'female', 82, '2018-09-02 01:35:17', '2018-09-02 01:35:17', 0),
(133, 'Omar', 'No notes', NULL, '', 7, 'male', 82, '2018-09-02 01:35:29', '2018-09-02 01:35:29', 0),
(134, 'Ali', 'No notes', NULL, '', 4, 'male', 82, '2018-09-02 01:35:45', '2018-09-02 01:35:45', 0),
(135, 'Salma', 'No notes', NULL, '', 4, 'female', 83, '2018-09-02 01:40:09', '2018-09-02 01:40:09', 0),
(136, 'Yaseen', 'No notes', NULL, '', 6, 'male', 84, '2018-09-02 01:42:12', '2018-09-02 01:42:12', 0),
(137, 'Farida', 'No notes', NULL, '', 4, 'female', 84, '2018-09-02 01:42:22', '2018-09-02 01:42:22', 0),
(138, 'Reina', 'No notes', NULL, '', 10, 'female', 85, '2018-09-02 01:44:52', '2018-09-02 01:44:52', 0),
(139, 'Diane', 'No notes', NULL, '', 9, 'female', 85, '2018-09-02 01:45:06', '2018-09-02 01:45:06', 0),
(140, 'Jad', 'No notes', NULL, '', 10, 'male', 86, '2018-09-02 01:48:01', '2018-09-02 01:48:01', 0),
(141, 'Lea', 'No notes', NULL, '', 8, 'female', 86, '2018-09-02 01:48:10', '2018-09-02 01:48:10', 0),
(142, 'Hassan', 'No notes', NULL, '', 6, 'male', 87, '2018-09-02 01:50:40', '2018-09-02 01:50:40', 0),
(143, 'Ali', 'No notes', NULL, '', 9, 'male', 87, '2018-09-02 01:50:51', '2018-09-02 01:50:51', 0),
(144, 'Jana', 'No notes', NULL, '', 9, 'female', 88, '2018-09-02 01:58:27', '2018-09-02 01:58:27', 0),
(145, 'Bayan', 'No notes', NULL, '', 7, 'female', 88, '2018-09-02 01:58:36', '2018-09-02 01:58:36', 0),
(146, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 11:25:04', '2018-10-09 11:25:04', 0),
(147, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 11:50:58', '2018-10-09 11:50:58', 0),
(148, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 11:50:59', '2018-10-09 11:50:59', 0),
(149, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 11:50:59', '2018-10-09 11:50:59', 0),
(150, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 11:50:59', '2018-10-09 11:50:59', 0),
(151, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 11:56:18', '2018-10-09 11:56:18', 0),
(152, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 11:58:32', '2018-10-09 11:58:32', 0),
(153, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 11:58:32', '2018-10-09 11:58:32', 0),
(154, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 11:58:32', '2018-10-09 11:58:32', 0),
(155, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 11:58:32', '2018-10-09 11:58:32', 0),
(156, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:01:24', '2018-10-09 12:01:24', 0),
(157, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:01:24', '2018-10-09 12:01:24', 0),
(158, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:01:24', '2018-10-09 12:01:24', 0),
(159, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:01:25', '2018-10-09 12:01:25', 0),
(160, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:04:03', '2018-10-09 12:04:03', 0),
(161, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:04:03', '2018-10-09 12:04:03', 0),
(162, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:04:03', '2018-10-09 12:04:03', 0),
(163, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:04:03', '2018-10-09 12:04:03', 0),
(164, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:13:55', '2018-10-09 12:13:55', 0),
(165, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:13:55', '2018-10-09 12:13:55', 0),
(166, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:13:55', '2018-10-09 12:13:55', 0),
(167, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:13:55', '2018-10-09 12:13:55', 0),
(168, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:14:37', '2018-10-09 12:14:37', 0),
(169, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:14:38', '2018-10-09 12:14:38', 0),
(170, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:14:38', '2018-10-09 12:14:38', 0),
(171, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:14:38', '2018-10-09 12:14:38', 0),
(172, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:15:45', '2018-10-09 12:15:45', 0),
(173, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:15:45', '2018-10-09 12:15:45', 0),
(174, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:15:45', '2018-10-09 12:15:45', 0),
(175, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:15:45', '2018-10-09 12:15:45', 0),
(176, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:24:57', '2018-10-09 12:24:57', 0),
(177, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:24:57', '2018-10-09 12:24:57', 0),
(178, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:24:57', '2018-10-09 12:24:57', 0),
(179, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:24:57', '2018-10-09 12:24:57', 0),
(180, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:27:34', '2018-10-09 12:27:34', 0),
(181, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:27:34', '2018-10-09 12:27:34', 0),
(182, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:27:35', '2018-10-09 12:27:35', 0),
(183, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:27:35', '2018-10-09 12:27:35', 0),
(184, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:33:40', '2018-10-09 12:33:40', 0),
(185, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:33:40', '2018-10-09 12:33:40', 0),
(186, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:33:40', '2018-10-09 12:33:40', 0),
(187, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:33:40', '2018-10-09 12:33:40', 0),
(188, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:35:28', '2018-10-09 12:35:28', 0),
(189, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:35:28', '2018-10-09 12:35:28', 0),
(190, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:35:28', '2018-10-09 12:35:28', 0),
(191, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:35:28', '2018-10-09 12:35:28', 0),
(192, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:41:28', '2018-10-09 12:41:28', 0),
(193, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:41:28', '2018-10-09 12:41:28', 0),
(194, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:41:28', '2018-10-09 12:41:28', 0),
(195, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:41:28', '2018-10-09 12:41:28', 0),
(196, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 12:58:25', '2018-10-09 12:58:25', 0),
(197, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 12:58:25', '2018-10-09 12:58:25', 0),
(198, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 12:58:25', '2018-10-09 12:58:25', 0),
(199, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 12:58:25', '2018-10-09 12:58:25', 0),
(200, '9856', '', NULL, '985', 895623, 'female', 162, '2018-10-09 13:00:18', '2018-10-09 13:00:18', 0),
(201, '6956', '', NULL, '956', 956, 'female', 162, '2018-10-09 13:00:18', '2018-10-09 13:00:18', 0),
(202, '956', '', NULL, '8956', 956, 'female', 162, '2018-10-09 13:00:18', '2018-10-09 13:00:18', 0),
(203, '9', '', NULL, '956', 56, 'female', 162, '2018-10-09 13:00:18', '2018-10-09 13:00:18', 0),
(204, 'عخه', '', NULL, '', 0, 'female', 96, '2018-10-12 20:58:20', '2018-10-12 20:58:20', 0),
(205, 'koko', '', NULL, '99', 99, 'female', 97, '2018-10-12 21:03:55', '2018-10-12 21:03:55', 0),
(206, '9', '', NULL, '9', 99, 'female', 97, '2018-10-12 21:03:55', '2018-10-12 21:03:55', 0),
(207, '9', '', NULL, '9', 99, 'female', 97, '2018-10-12 21:03:55', '2018-10-12 21:03:55', 0),
(208, '9', '', NULL, '99', 9, 'female', 97, '2018-10-12 21:03:55', '2018-10-12 21:03:55', 0),
(209, 'koko', '', NULL, '99', 99, 'female', 98, '2018-10-12 22:06:08', '2018-10-12 22:06:08', 0),
(210, '9', '', NULL, '9', 99, 'female', 98, '2018-10-12 22:06:08', '2018-10-12 22:06:08', 0),
(211, '9', '', NULL, '9', 99, 'female', 98, '2018-10-12 22:06:08', '2018-10-12 22:06:08', 0),
(212, '9', '', NULL, '99', 9, 'female', 98, '2018-10-12 22:06:08', '2018-10-12 22:06:08', 0),
(213, 'koko', '', NULL, '99', 99, 'female', 99, '2018-10-12 22:08:41', '2018-10-12 22:08:41', 0),
(214, '9', '', NULL, '9', 99, 'female', 99, '2018-10-12 22:08:41', '2018-10-12 22:08:41', 0),
(215, '9', '', NULL, '9', 99, 'female', 99, '2018-10-12 22:08:41', '2018-10-12 22:08:41', 0),
(216, '9', '', NULL, '99', 9, 'female', 99, '2018-10-12 22:08:41', '2018-10-12 22:08:41', 0),
(217, 'ii', '', NULL, 'io', 0, 'female', 100, '2018-10-13 00:06:09', '2018-10-13 00:06:09', 0),
(218, 'io', '', NULL, 'ioio', 0, 'female', 100, '2018-10-13 00:06:09', '2018-10-13 00:06:09', 0),
(219, 'io', '', NULL, 'io', 0, 'female', 100, '2018-10-13 00:06:09', '2018-10-13 00:06:09', 0),
(220, 'io', '', NULL, 'oio', 0, 'female', 100, '2018-10-13 00:06:09', '2018-10-13 00:06:09', 0),
(221, '98', '89', NULL, '8', 89, 'female', 101, '2018-10-13 00:20:46', '2018-10-13 00:20:46', 0),
(222, '9', '989', NULL, '98', 989, 'female', 101, '2018-10-13 00:20:46', '2018-10-13 00:20:46', 0),
(223, '89', '898', NULL, '898', 98, 'female', 101, '2018-10-13 00:20:46', '2018-10-13 00:20:46', 0),
(224, '998', '98', NULL, '89', 89, 'female', 101, '2018-10-13 00:20:47', '2018-10-13 00:20:47', 0),
(225, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:34:58', '2018-10-13 00:34:58', 0),
(226, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:34:59', '2018-10-13 00:34:59', 0),
(227, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:34:59', '2018-10-13 00:34:59', 0),
(228, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:34:59', '2018-10-13 00:34:59', 0),
(229, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:35:54', '2018-10-13 00:35:54', 0),
(230, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:35:54', '2018-10-13 00:35:54', 0),
(231, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:35:54', '2018-10-13 00:35:54', 0),
(232, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:35:54', '2018-10-13 00:35:54', 0),
(233, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:39:32', '2018-10-13 00:39:32', 0),
(234, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:39:32', '2018-10-13 00:39:32', 0),
(235, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:39:32', '2018-10-13 00:39:32', 0),
(236, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:39:33', '2018-10-13 00:39:33', 0),
(237, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:50:31', '2018-10-13 00:50:31', 0),
(238, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:50:31', '2018-10-13 00:50:31', 0),
(239, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:50:31', '2018-10-13 00:50:31', 0),
(240, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:50:32', '2018-10-13 00:50:32', 0),
(241, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:50:41', '2018-10-13 00:50:41', 0),
(242, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:50:41', '2018-10-13 00:50:41', 0),
(243, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:50:41', '2018-10-13 00:50:41', 0),
(244, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:50:42', '2018-10-13 00:50:42', 0),
(245, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:52:40', '2018-10-13 00:52:40', 0),
(246, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:52:40', '2018-10-13 00:52:40', 0),
(247, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:52:40', '2018-10-13 00:52:40', 0),
(248, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:52:40', '2018-10-13 00:52:40', 0),
(249, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:53:03', '2018-10-13 00:53:03', 0),
(250, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:53:03', '2018-10-13 00:53:03', 0),
(251, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:53:03', '2018-10-13 00:53:03', 0),
(252, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:53:03', '2018-10-13 00:53:03', 0),
(253, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:53:18', '2018-10-13 00:53:18', 0),
(254, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:53:18', '2018-10-13 00:53:18', 0),
(255, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:53:18', '2018-10-13 00:53:18', 0),
(256, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:53:18', '2018-10-13 00:53:18', 0),
(257, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 00:56:36', '2018-10-13 00:56:36', 0),
(258, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 00:56:36', '2018-10-13 00:56:36', 0),
(259, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 00:56:36', '2018-10-13 00:56:36', 0),
(260, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 00:56:36', '2018-10-13 00:56:36', 0),
(261, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 01:02:30', '2018-10-13 01:02:30', 0),
(262, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 01:02:30', '2018-10-13 01:02:30', 0),
(263, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 01:02:30', '2018-10-13 01:02:30', 0),
(264, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 01:02:30', '2018-10-13 01:02:30', 0),
(265, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 01:03:20', '2018-10-13 01:03:20', 0),
(266, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 01:03:20', '2018-10-13 01:03:20', 0),
(267, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 01:03:20', '2018-10-13 01:03:20', 0),
(268, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 01:03:20', '2018-10-13 01:03:20', 0),
(269, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 01:03:25', '2018-10-13 01:03:25', 0),
(270, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 01:03:25', '2018-10-13 01:03:25', 0),
(271, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 01:03:25', '2018-10-13 01:03:25', 0),
(272, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 01:03:26', '2018-10-13 01:03:26', 0),
(273, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 01:05:57', '2018-10-13 01:05:57', 0),
(274, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 01:05:57', '2018-10-13 01:05:57', 0),
(275, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 01:05:57', '2018-10-13 01:05:57', 0),
(276, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 01:05:57', '2018-10-13 01:05:57', 0),
(277, '98', '89', NULL, '8', 89, 'female', 165, '2018-10-13 01:06:16', '2018-10-13 01:06:16', 0),
(278, '9', '989', NULL, '98', 989, 'female', 165, '2018-10-13 01:06:17', '2018-10-13 01:06:17', 0),
(279, '89', '898', NULL, '898', 98, 'female', 165, '2018-10-13 01:06:17', '2018-10-13 01:06:17', 0),
(280, '998', '98', NULL, '89', 89, 'female', 165, '2018-10-13 01:06:17', '2018-10-13 01:06:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_allergies`
--

CREATE TABLE `students_allergies` (
  `id` int(11) NOT NULL,
  `student_record_id` int(11) NOT NULL,
  `allergy_name` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_allergies`
--

INSERT INTO `students_allergies` (`id`, `student_record_id`, `allergy_name`, `created_at`, `updated_at`) VALUES
(9, 72, 'nuts', '2018-07-19 12:53:15', '2018-07-19 12:53:15'),
(10, 77, 'nuts', '2018-07-24 11:33:51', '2018-07-24 11:33:51'),
(11, 76, 'nuts', '2018-07-30 06:20:56', '2018-07-30 06:20:56'),
(13, 78, 'Nuts', '2018-08-25 11:31:36', '2018-08-25 11:31:36'),
(14, 85, 'peanuts example', '2018-08-31 01:39:16', '2018-08-31 01:39:16'),
(15, 94, 'Allergic to Sulfa', '2018-08-31 05:43:16', '2018-08-31 05:43:16'),
(16, 135, 'Allergic to Penicillen', '2018-09-01 01:59:59', '2018-09-01 01:59:59'),
(17, 136, 'Allergic to Penicillen', '2018-09-01 02:02:32', '2018-09-01 02:02:32'),
(18, 140, 'Has Mild Asthma', '2018-09-01 02:39:41', '2018-09-01 02:39:41'),
(19, 139, 'Has mild asthma', '2018-09-01 02:40:17', '2018-09-01 02:46:46'),
(20, 139, 'Allergic to Peppermint Candy', '2018-09-01 02:43:32', '2018-09-01 02:43:32'),
(22, 137, 'Allergic to Eggs', '2018-09-02 00:47:23', '2018-09-02 00:47:23'),
(23, 159, 'Mild asthma; uses hand held inhaler when needed.', '2018-09-02 01:07:26', '2018-09-02 01:07:26'),
(24, 163, 'Has tree nuts allergies', '2018-09-02 01:17:46', '2018-09-02 01:17:46'),
(25, 165, 'Allergic to sesame seeds/pistachio /cashew', '2018-09-02 01:21:15', '2018-09-02 01:21:15'),
(26, 190, 'nuts', '2018-09-19 05:47:52', '2018-09-19 05:47:52'),
(27, 190, 'sulfa', '2018-09-19 05:47:58', '2018-09-19 05:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `students_attendances`
--

CREATE TABLE `students_attendances` (
  `id` int(11) NOT NULL,
  `attendance_date` date NOT NULL,
  `attendance_type` enum('absence','delay') NOT NULL,
  `semester` varchar(45) DEFAULT NULL,
  `student_record_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_attendances`
--

INSERT INTO `students_attendances` (`id`, `attendance_date`, `attendance_type`, `semester`, `student_record_id`, `created_at`, `updated_at`, `deleted`) VALUES
(50, '2018-08-18', '', 'first', 56, '2018-08-25 13:46:17', '2018-08-25 13:46:17', 0),
(51, '2018-09-01', 'absence', 'first', 130, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(52, '2018-09-01', 'absence', 'first', 127, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(53, '2018-09-01', 'absence', 'first', 137, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(54, '2018-09-01', 'absence', 'first', 111, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(55, '2018-09-01', 'absence', 'first', 158, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(56, '2018-09-01', 'absence', 'first', 156, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(57, '2018-09-01', 'absence', 'first', 114, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(58, '2018-09-01', 'absence', 'first', 176, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(59, '2018-09-01', 'absence', 'first', 132, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(60, '2018-09-16', 'absence', 'first', 94, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(61, '2018-09-16', 'absence', 'first', 101, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(62, '2018-09-16', 'absence', 'first', 95, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(63, '2018-09-16', 'absence', 'first', 103, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_marks`
--

CREATE TABLE `students_marks` (
  `id` int(11) NOT NULL,
  `student_record_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `mark_key` enum('O','S','N') DEFAULT 'O',
  `mark_note` text,
  `semester` enum('first','second') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_marks`
--

INSERT INTO `students_marks` (`id`, `student_record_id`, `subject_id`, `mark_key`, `mark_note`, `semester`, `created_at`, `updated_at`) VALUES
(11, 72, 281, 'S', '_', 'first', '2018-07-19 13:29:05', '2018-07-19 13:29:05'),
(12, 83, 281, 'S', '-', 'first', '2018-08-19 13:28:51', '2018-08-19 13:28:51'),
(13, 78, 281, 'S', '-', 'first', '2018-08-19 13:28:51', '2018-08-19 13:28:51'),
(14, 74, 281, 'S', '-', 'first', '2018-08-19 13:28:51', '2018-08-19 13:28:51'),
(15, 83, 281, 'O', '-', 'second', '2018-08-19 13:30:05', '2018-08-19 13:30:05'),
(16, 78, 281, 'O', '-', 'second', '2018-08-19 13:30:05', '2018-08-19 13:30:05'),
(17, 74, 281, 'O', '-', 'second', '2018-08-19 13:30:05', '2018-08-19 13:30:05'),
(18, 94, 335, 'S', 'baaad ', 'first', '2018-09-15 11:02:58', '2018-09-16 10:11:53'),
(19, 101, 335, 'N', '', 'first', '2018-09-15 11:02:59', '2018-09-16 10:11:53'),
(20, 95, 335, 'O', '', 'first', '2018-09-15 11:02:59', '2018-09-16 10:11:53'),
(21, 103, 335, 'O', '', 'first', '2018-09-15 11:02:59', '2018-09-16 10:11:53'),
(22, 138, 335, 'O', '', 'first', '2018-09-15 11:02:59', '2018-09-16 10:11:53'),
(23, 105, 335, 'O', '', 'first', '2018-09-15 11:02:59', '2018-09-16 10:11:53'),
(24, 100, 335, 'O', '', 'first', '2018-09-15 11:02:59', '2018-09-16 10:11:53'),
(25, 94, 335, 'O', 'gfgfdggdf', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(26, 101, 335, 'O', '', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(27, 95, 335, 'O', '', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(28, 103, 335, 'O', '', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(29, 138, 335, 'O', '', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(30, 105, 335, 'O', '', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(31, 100, 335, 'O', '', 'second', '2018-09-15 11:05:25', '2018-09-15 11:05:25'),
(32, 94, 354, 'S', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(33, 101, 354, 'O', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(34, 95, 354, 'O', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(35, 103, 354, 'O', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(36, 138, 354, 'O', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(37, 105, 354, 'O', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(38, 100, 354, 'O', '', 'first', '2018-09-16 11:13:01', '2018-09-16 11:17:15'),
(39, 94, 353, 'S', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(40, 101, 353, 'O', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(41, 95, 353, 'O', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(42, 103, 353, 'O', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(43, 138, 353, 'O', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(44, 105, 353, 'O', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(45, 100, 353, 'O', '', 'first', '2018-09-16 11:13:12', '2018-09-16 11:17:01'),
(46, 130, 325, 'S', 'sdfghjk', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(47, 127, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(48, 137, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(49, 111, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(50, 158, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(51, 156, 325, 'O', 'dsdsa', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(52, 114, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(53, 176, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(54, 132, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(55, 168, 325, 'O', '', 'first', '2018-09-16 12:12:14', '2018-09-16 12:36:05'),
(56, 119, 325, NULL, NULL, 'first', '2018-09-16 12:15:03', '2018-09-16 12:15:03'),
(57, 133, 325, NULL, NULL, 'first', '2018-09-16 12:15:03', '2018-09-16 12:15:03'),
(58, 148, 325, NULL, NULL, 'first', '2018-09-16 12:15:03', '2018-09-16 12:15:03'),
(59, 108, 325, NULL, NULL, 'first', '2018-09-16 12:15:03', '2018-09-16 12:15:03'),
(60, 119, 325, NULL, NULL, 'first', '2018-09-16 12:15:12', '2018-09-16 12:15:12'),
(61, 133, 325, NULL, NULL, 'first', '2018-09-16 12:15:12', '2018-09-16 12:15:12'),
(62, 148, 325, NULL, NULL, 'first', '2018-09-16 12:15:12', '2018-09-16 12:15:12'),
(63, 108, 325, NULL, NULL, 'first', '2018-09-16 12:15:12', '2018-09-16 12:15:12'),
(64, 119, 325, NULL, NULL, 'first', '2018-09-16 12:36:05', '2018-09-16 12:36:05'),
(65, 133, 325, NULL, NULL, 'first', '2018-09-16 12:36:05', '2018-09-16 12:36:05'),
(66, 148, 325, NULL, NULL, 'first', '2018-09-16 12:36:05', '2018-09-16 12:36:05'),
(67, 108, 325, NULL, NULL, 'first', '2018-09-16 12:36:05', '2018-09-16 12:36:05'),
(68, 97, 330, 'S', 'greaaaaaate', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(69, 124, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(70, 129, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(71, 126, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(72, 146, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(73, 184, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(74, 96, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(75, 162, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(76, 147, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(77, 145, 330, 'O', '', 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(78, 91, 330, NULL, NULL, 'first', '2018-09-17 05:27:16', '2018-09-17 05:27:16'),
(79, 97, 330, 'N', 'ggggggggggggggggggg', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(80, 124, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(81, 129, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(82, 126, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(83, 146, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(84, 184, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(85, 96, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(86, 162, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(87, 147, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(88, 145, 330, 'O', '', 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(89, 91, 330, NULL, NULL, 'second', '2018-09-17 05:27:26', '2018-09-17 05:27:26'),
(90, 164, 326, 'O', 'very good ', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(91, 171, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(92, 180, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(93, 141, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(94, 154, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(95, 155, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(96, 165, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(97, 173, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(98, 109, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(99, 178, 326, 'O', '', 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(100, 123, 326, NULL, NULL, 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(101, 142, 326, NULL, NULL, 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(102, 172, 326, NULL, NULL, 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(103, 134, 326, NULL, NULL, 'first', '2018-09-17 07:44:18', '2018-09-17 07:44:18'),
(104, 164, 285, 'S', 'need more ', 'first', '2018-09-17 07:44:30', '2018-09-17 07:44:30'),
(105, 171, 285, 'O', '', 'first', '2018-09-17 07:44:30', '2018-09-17 07:44:30'),
(106, 180, 285, 'O', '', 'first', '2018-09-17 07:44:30', '2018-09-17 07:44:30'),
(107, 141, 285, 'O', '', 'first', '2018-09-17 07:44:30', '2018-09-17 07:44:30'),
(108, 154, 285, 'O', '', 'first', '2018-09-17 07:44:30', '2018-09-17 07:44:30'),
(109, 155, 285, 'O', '', 'first', '2018-09-17 07:44:30', '2018-09-17 07:44:30'),
(110, 165, 285, 'O', '', 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(111, 173, 285, 'O', '', 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(112, 109, 285, 'O', '', 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(113, 178, 285, 'O', '', 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(114, 123, 285, NULL, NULL, 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(115, 142, 285, NULL, NULL, 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(116, 172, 285, NULL, NULL, 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(117, 134, 285, NULL, NULL, 'first', '2018-09-17 07:44:31', '2018-09-17 07:44:31'),
(118, 177, 327, 'S', 'werdtfygujk', 'first', '2018-09-17 11:33:30', '2018-09-17 11:33:30'),
(119, 144, 327, 'S', '', 'first', '2018-09-17 11:33:30', '2018-09-17 11:33:30'),
(120, 112, 327, 'O', '', 'first', '2018-09-17 11:33:30', '2018-09-17 11:33:30'),
(121, 131, 327, 'S', '', 'first', '2018-09-17 11:33:30', '2018-09-17 11:33:30'),
(122, 153, 327, 'N', '', 'first', '2018-09-17 11:33:30', '2018-09-17 11:33:30'),
(123, 177, 327, 'N', 'gd', 'second', '2018-09-17 11:33:43', '2018-09-17 11:33:43'),
(124, 144, 327, 'N', 'gdf', 'second', '2018-09-17 11:33:43', '2018-09-17 11:33:43'),
(125, 112, 327, 'S', 'gdf', 'second', '2018-09-17 11:33:43', '2018-09-17 11:33:43'),
(126, 131, 327, 'S', 'gd', 'second', '2018-09-17 11:33:43', '2018-09-17 11:33:43'),
(127, 153, 327, 'N', 'gd', 'second', '2018-09-17 11:33:43', '2018-09-17 11:33:43'),
(128, 159, 337, 'S', 'Jad Farshoukh', 'first', '2018-09-20 05:55:41', '2018-09-20 05:55:41'),
(129, 121, 337, 'N', 'Lilium Affani', 'first', '2018-09-20 05:55:41', '2018-09-20 05:55:41'),
(130, 157, 337, 'N', 'Lilium Affani', 'first', '2018-09-20 05:55:41', '2018-09-20 05:55:41'),
(131, 164, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(132, 171, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(133, 180, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(134, 141, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(135, 154, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(136, 155, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(137, 165, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(138, 173, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(139, 109, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(140, 178, 339, NULL, NULL, 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:01'),
(141, 123, 339, 'N', 'ns', 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:22'),
(142, 142, 339, 'N', 'ns', 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:22'),
(143, 172, 339, 'N', 'ns', 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:22'),
(144, 134, 339, 'N', 'ns', 'first', '2018-09-20 06:09:01', '2018-09-20 06:09:22'),
(145, 164, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(146, 171, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(147, 180, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(148, 141, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(149, 154, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(150, 155, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(151, 165, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(152, 173, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(153, 109, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22'),
(154, 178, 339, NULL, NULL, 'first', '2018-09-20 06:09:22', '2018-09-20 06:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `students_notes`
--

CREATE TABLE `students_notes` (
  `id` int(11) NOT NULL,
  `student_record_id` int(11) NOT NULL,
  `note_month` varchar(255) NOT NULL,
  `note_text` text NOT NULL,
  `note_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_notes`
--

INSERT INTO `students_notes` (`id`, `student_record_id`, `note_month`, `note_text`, `note_date`, `created_at`, `updated_at`) VALUES
(9, 72, 'january', 'very hard working', '2018-01-01', '2018-07-22 07:41:27', '2018-07-22 07:41:35'),
(10, 76, 'June', 'very hard working', '2018-06-28', '2018-07-30 06:21:47', '2018-07-30 06:21:47'),
(11, 78, 'February', 'hard working ', '2018-07-31', '2018-08-02 14:16:39', '2018-08-02 14:16:39'),
(12, 83, 'February', 'Hard Working', '2018-08-19', '2018-08-19 13:13:06', '2018-08-19 13:31:03'),
(13, 190, 'March', 'wdfg', '2018-10-25', '2018-09-19 05:52:34', '2018-09-19 05:52:34'),
(14, 190, 'January', 'sdfghj', '2018-11-08', '2018-09-19 05:52:44', '2018-09-19 05:52:44'),
(15, 190, 'April', 'iujhygfdc', '2018-11-15', '2018-09-19 05:53:14', '2018-09-19 05:53:14');

-- --------------------------------------------------------

--
-- Table structure for table `students_payments`
--

CREATE TABLE `students_payments` (
  `id` int(11) NOT NULL,
  `student_record_id` int(11) NOT NULL,
  `payment_item_id` int(11) NOT NULL,
  `payment_item_type` varchar(255) NOT NULL,
  `tuition_type` varchar(255) NOT NULL,
  `late_fee_type` varchar(255) NOT NULL,
  `books_payment_type` varchar(255) NOT NULL,
  `full_year_discount` float NOT NULL DEFAULT '0',
  `teacher_child_discount` float NOT NULL,
  `invoice` float NOT NULL,
  `payment` float NOT NULL DEFAULT '0',
  `check_or_cash` enum('cash','check') NOT NULL,
  `student_payment_description` varchar(45) NOT NULL,
  `student_payment_date` date NOT NULL,
  `student_payment_notes` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_payments`
--

INSERT INTO `students_payments` (`id`, `student_record_id`, `payment_item_id`, `payment_item_type`, `tuition_type`, `late_fee_type`, `books_payment_type`, `full_year_discount`, `teacher_child_discount`, `invoice`, `payment`, `check_or_cash`, `student_payment_description`, `student_payment_date`, `student_payment_notes`, `created_at`, `updated_at`, `deleted`) VALUES
(33, 56, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 0, 'cash', 'رسوم التسجيل', '2018-05-25', 'رسوم التسجيل', '2018-07-04 08:33:05', '2018-07-04 08:33:05', 0),
(34, 56, 0, 'books', '000', '000', 'pre_kinder_books_fee', 0, 0, 40, 0, 'cash', 'كتب ما قبل مرحلة رياض الأطفال', '2018-05-25', 'كتب ما قبل مرحلة رياض الأطفال', '2018-07-04 08:33:30', '2018-07-04 08:33:30', 0),
(35, 56, 0, 'tuition', 'bimonthly', '000', '000', 0, 0, 40, 0, 'cash', '-', '2018-05-11', '-', '2018-07-04 08:37:12', '2018-08-02 06:17:53', 0),
(37, 68, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 0, 'cash', 'رسوم تسجيل', '2018-07-06', 'رسوم تسجيل', '2018-07-05 07:26:00', '2018-07-05 07:26:00', 0),
(38, 68, 0, 'tuition', 'bimonthly', '000', '000', 0, 0, 140, 0, 'cash', 'رسوم دراسية لشهرين ', '2018-07-19', 'رسوم دراسية لشهرين ', '2018-07-05 08:06:05', '2018-07-05 08:06:05', 0),
(39, 74, 0, 'registration_fee', '000', '000', '000', 0, 0, 300, 0, 'cash', '-', '2018-08-02', '-', '2018-08-02 07:26:06', '2018-08-02 07:26:06', 0),
(40, 74, 0, 'books', '000', '000', 'pre_kinder_books_fee', 0, 0, 40, 0, 'check', 'Description', '2018-08-13', '', '2018-08-02 07:28:08', '2018-08-02 07:28:08', 0),
(42, 74, 0, 'tuition', 'bimonthly', '000', '000', 0, 0, 300, 0, 'cash', '-', '2018-08-27', '', '2018-08-02 07:31:46', '2018-08-02 07:31:46', 0),
(43, 74, 0, 'registration_fee', '000', '000', '000', 0, 0, 50, 0, 'cash', '-', '2018-08-28', '', '2018-08-02 07:37:20', '2018-08-02 07:37:20', 0),
(44, 78, 0, 'tuition', 'full_year', '000', '000', 20, 0, 315, 0, 'check', '-', '2018-12-26', '', '2018-08-02 10:26:35', '2018-08-02 10:28:04', 0),
(48, 134, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 40, 'cash', 'oo', '2018-10-11', 'oo', '2018-10-02 07:33:20', '2018-10-02 07:46:42', 0),
(49, 107, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 20, 'cash', '-', '2018-10-24', '-', '2018-10-02 09:26:01', '2018-10-02 09:26:01', 0),
(50, 106, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 30, 'cash', '-', '2018-10-02', '-', '2018-10-02 09:26:15', '2018-10-02 09:26:15', 0),
(51, 108, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 50, 'cash', '-', '2018-10-13', '--', '2018-10-02 09:26:44', '2018-10-02 09:26:44', 0),
(71, 133, 0, 'registration_fee', '000', '000', '000', 0, 0, 30, 80, 'check', 'kmjnhbg', '2018-05-18', 'vf', '2018-10-02 12:50:46', '2018-10-02 12:50:46', 0),
(72, 133, 0, 'books', '000', '000', 'elementary_books_fee', 0, 0, 40, 400, 'cash', 'cx', '2018-05-16', 'vxc', '2018-10-02 12:51:01', '2018-10-02 12:51:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_records`
--

CREATE TABLE `students_records` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `total_payment` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '2',
  `non_active_registration_date` date DEFAULT NULL,
  `teacher_child` tinyint(2) NOT NULL DEFAULT '1',
  `student_order` int(11) NOT NULL DEFAULT '1',
  `photo_permission_slips` tinyint(1) NOT NULL DEFAULT '0',
  `islamic_book_return` tinyint(1) NOT NULL DEFAULT '0',
  `status_islamic_book_return` enum('excellent','very_good','good','bad','no_status') NOT NULL DEFAULT 'no_status',
  `year` int(11) NOT NULL,
  `participates_in_class` varchar(255) NOT NULL,
  `shows_cooperative_attitude_with_peers` varchar(255) NOT NULL,
  `shows_cooperative_attitude_with_teachers` varchar(255) NOT NULL,
  `listens_when_others_are_speaking` varchar(255) NOT NULL,
  `respects_equipment_and_material` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_records`
--

INSERT INTO `students_records` (`id`, `student_id`, `room_id`, `total_payment`, `is_active`, `non_active_registration_date`, `teacher_child`, `student_order`, `photo_permission_slips`, `islamic_book_return`, `status_islamic_book_return`, `year`, `participates_in_class`, `shows_cooperative_attitude_with_peers`, `shows_cooperative_attitude_with_teachers`, `listens_when_others_are_speaking`, `respects_equipment_and_material`, `created_at`, `updated_at`) VALUES
(57, 25, 21, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-06-25 06:16:21', '2018-06-25 06:16:21'),
(56, 25, 20, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, 'yes', 'yes', 'yes', 'yes', 'yes', '2018-06-21 08:31:24', '2018-07-01 06:13:06'),
(67, 26, 22, 0, 2, NULL, 2, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-07-02 07:02:52', '2018-07-02 07:02:52'),
(66, 26, 21, 0, 2, NULL, 2, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-07-01 08:32:14', '2018-07-02 07:49:08'),
(64, 26, 20, 0, 1, NULL, 1, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-07-01 08:20:09', '2018-07-01 08:29:35'),
(68, 27, 20, 0, 1, NULL, 1, 3, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-07-05 07:24:49', '2018-07-22 12:48:31'),
(69, 28, 23, 0, 1, NULL, 1, 4, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-07-08 11:40:48', '2018-07-22 13:07:07'),
(71, 28, 25, 0, 1, NULL, 1, 4, 1, 1, 'excellent', 2018, '', '', '', '', '', '2018-07-17 12:21:26', '2018-07-19 12:54:45'),
(85, 33, 30, 0, 2, NULL, 2, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-26 23:00:34', '2018-08-30 14:40:10'),
(74, 27, 25, 0, 2, NULL, 1, 3, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-07-22 12:48:31', '2018-07-22 12:48:31'),
(78, 28, 25, 0, 2, NULL, 2, 4, 2, 2, 'very_good', 2018, '', '', '', '', '', '2018-08-02 10:22:42', '2018-08-02 10:22:42'),
(84, 32, 20, 0, 2, NULL, 2, 1, 2, 1, 'no_status', 2018, '1', '1', '1', '1', '1', '2018-08-26 21:43:19', '2018-08-31 01:39:43'),
(83, 23, 25, 0, 2, NULL, 1, 3, 1, 1, 'no_status', 2018, 'yes', 'yes', 'yes', 'yes', 'yes', '2018-08-19 13:10:40', '2018-08-19 13:14:49'),
(86, 34, 29, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:06:27', '2018-08-31 05:06:27'),
(87, 35, 23, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:08:28', '2018-08-31 05:08:28'),
(88, 36, 23, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:14:19', '2018-08-31 05:14:19'),
(89, 40, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:14:50', '2018-08-31 05:14:50'),
(90, 41, 23, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:21:51', '2018-08-31 05:21:51'),
(91, 42, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:27:58', '2018-08-31 05:27:58'),
(92, 43, 27, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:33:35', '2018-08-31 05:33:35'),
(93, 44, 23, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:34:08', '2018-08-31 05:34:08'),
(94, 45, 33, 0, 3, NULL, 1, 1, 2, 1, 'no_status', 2018, 'S', 'S', 'S', 'N', 'S', '2018-08-31 05:40:54', '2018-09-18 12:10:04'),
(95, 46, 33, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:42:11', '2018-08-31 05:42:11'),
(96, 47, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:47:30', '2018-08-31 05:47:30'),
(97, 48, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 05:51:01', '2018-08-31 05:51:01'),
(100, 51, 33, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 19:07:48', '2018-08-31 19:07:48'),
(101, 54, 33, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 20:41:26', '2018-08-31 20:41:26'),
(102, 56, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:01:14', '2018-08-31 23:01:14'),
(103, 58, 33, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:11:52', '2018-08-31 23:11:52'),
(104, 59, 29, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:18:03', '2018-08-31 23:18:03'),
(105, 60, 33, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:18:35', '2018-08-31 23:18:35'),
(106, 61, 27, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:29:45', '2018-08-31 23:29:45'),
(107, 62, 23, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:30:13', '2018-08-31 23:30:13'),
(108, 63, 24, 0, 2, NULL, 1, 3, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:34:14', '2018-09-02 00:46:21'),
(109, 57, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:34:41', '2018-09-02 00:46:00'),
(110, 55, 25, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:35:08', '2018-09-02 00:50:31'),
(111, 64, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:45:11', '2018-09-02 00:52:50'),
(112, 65, 31, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:45:27', '2018-09-02 00:52:44'),
(113, 66, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-08-31 23:49:58', '2018-08-31 23:49:58'),
(114, 67, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:01:50', '2018-09-01 00:01:50'),
(115, 68, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:11:27', '2018-09-01 00:11:27'),
(116, 69, 23, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:22:23', '2018-09-01 00:22:23'),
(117, 70, 34, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:34:18', '2018-09-01 00:34:18'),
(118, 72, 34, 0, 1, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:34:37', '2018-09-01 21:57:57'),
(119, 71, 24, 0, 2, NULL, 1, 3, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:35:39', '2018-09-02 00:46:45'),
(120, 73, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:41:47', '2018-09-01 00:41:47'),
(121, 74, 35, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:51:08', '2018-09-01 00:51:08'),
(122, 75, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:51:30', '2018-09-01 00:51:30'),
(123, 76, 30, 0, 2, NULL, 1, 3, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:51:54', '2018-09-01 00:51:54'),
(124, 77, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:58:21', '2018-09-01 00:58:21'),
(125, 78, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 00:59:07', '2018-09-01 00:59:07'),
(126, 79, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:08:07', '2018-09-01 01:08:07'),
(127, 80, 24, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:08:24', '2018-09-02 00:51:06'),
(128, 81, 23, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:12:03', '2018-09-01 01:12:03'),
(129, 82, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:21:16', '2018-09-01 01:21:16'),
(130, 83, 24, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, 'S', 'N', 'N', 'S', 'S', '2018-09-01 01:21:37', '2018-09-16 12:11:17'),
(131, 84, 31, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:37:29', '2018-09-02 00:48:38'),
(132, 85, 24, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:40:55', '2018-09-01 01:40:55'),
(133, 86, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:49:46', '2018-09-02 00:44:58'),
(134, 87, 30, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:51:48', '2018-09-02 00:45:24'),
(135, 89, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 01:59:33', '2018-09-01 01:59:33'),
(136, 88, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 02:02:03', '2018-09-01 02:02:03'),
(137, 90, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 02:16:00', '2018-09-02 00:47:10'),
(138, 91, 33, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 02:36:01', '2018-09-01 02:36:01'),
(139, 92, 27, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 02:38:57', '2018-09-01 02:38:57'),
(140, 93, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 02:39:16', '2018-09-01 02:39:16'),
(141, 94, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 02:53:46', '2018-09-02 00:50:02'),
(142, 95, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 03:00:00', '2018-09-02 00:51:57'),
(143, 96, 27, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 03:06:40', '2018-09-01 03:06:40'),
(144, 98, 31, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 03:38:39', '2018-09-02 00:51:24'),
(145, 97, 20, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 03:39:24', '2018-09-01 03:39:24'),
(146, 99, 20, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 04:10:36', '2018-09-01 04:10:36'),
(147, 101, 20, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 04:12:26', '2018-09-01 04:12:26'),
(148, 102, 24, 0, 2, NULL, 1, 3, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 04:12:48', '2018-09-02 00:49:35'),
(153, 110, 31, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 18:14:48', '2018-09-01 18:14:48'),
(154, 111, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 18:42:27', '2018-09-01 18:42:27'),
(155, 112, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 18:45:42', '2018-09-01 18:45:42'),
(156, 113, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 18:52:01', '2018-09-01 18:52:01'),
(157, 72, 35, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-01 21:57:57', '2018-09-01 21:57:57'),
(158, 115, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:00:45', '2018-09-02 01:00:45'),
(159, 116, 35, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:07:03', '2018-09-02 01:07:03'),
(160, 117, 23, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:10:24', '2018-09-02 01:10:24'),
(161, 118, 23, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:12:55', '2018-09-02 01:12:55'),
(162, 119, 20, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:13:11', '2018-09-02 01:13:11'),
(163, 120, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:17:32', '2018-09-02 01:17:32'),
(164, 121, 30, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, 'S', 'N', 'N', 'S', 'S', '2018-09-02 01:18:01', '2018-09-17 06:46:19'),
(165, 124, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:20:51', '2018-09-02 01:20:51'),
(166, 123, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:21:33', '2018-09-02 01:21:33'),
(167, 122, 23, 0, 2, NULL, 1, 3, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:21:49', '2018-09-02 01:21:49'),
(168, 125, 24, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:25:21', '2018-09-02 01:25:21'),
(169, 126, 29, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:25:47', '2018-09-02 01:25:47'),
(170, 127, 29, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:29:03', '2018-09-02 01:29:03'),
(171, 128, 30, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:29:15', '2018-09-02 01:29:15'),
(172, 129, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:32:32', '2018-09-02 01:32:32'),
(173, 130, 30, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:32:47', '2018-09-02 01:32:47'),
(191, 131, 22, 0, 2, NULL, 1, 4, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 02:07:51', '2018-09-02 02:07:51'),
(175, 132, 23, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:37:05', '2018-09-02 02:07:19'),
(176, 133, 24, 0, 2, NULL, 1, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:37:21', '2018-09-02 02:07:19'),
(177, 134, 31, 0, 2, NULL, 1, 3, 1, 1, 'no_status', 2018, 'S', 'N', 'N', 'S', 'S', '2018-09-02 01:37:43', '2018-09-17 11:23:44'),
(178, 135, 30, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:40:31', '2018-09-02 01:40:31'),
(179, 136, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:42:55', '2018-09-02 01:42:55'),
(180, 137, 30, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:43:08', '2018-09-02 01:43:08'),
(181, 138, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:45:43', '2018-09-02 01:45:43'),
(182, 139, 32, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:45:57', '2018-09-02 01:45:57'),
(183, 140, 29, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:48:42', '2018-09-02 01:48:42'),
(184, 141, 20, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:48:56', '2018-09-02 01:48:56'),
(188, 143, 27, 0, 2, NULL, 1, 2, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:57:58', '2018-09-02 01:57:58'),
(187, 142, 32, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:57:38', '2018-09-02 01:57:38'),
(189, 144, 34, 0, 2, NULL, 1, 1, 2, 1, 'no_status', 2018, '', '', '', '', '', '2018-09-02 01:58:51', '2018-09-02 01:58:51'),
(216, 168, 24, 0, 2, NULL, 1, 5, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:14:38', '2018-10-09 12:14:38'),
(217, 169, 32, 0, 2, NULL, 1, 6, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:14:38', '2018-10-09 12:14:38'),
(224, 176, 24, 0, 2, NULL, 1, 13, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:24:57', '2018-10-09 12:24:57'),
(223, 175, 23, 0, 2, NULL, 1, 12, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:15:45', '2018-10-09 12:15:45'),
(222, 174, 23, 0, 2, NULL, 1, 11, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:15:45', '2018-10-09 12:15:45'),
(221, 173, 32, 0, 2, NULL, 1, 10, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:15:45', '2018-10-09 12:15:45'),
(220, 172, 24, 0, 2, NULL, 1, 9, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:15:45', '2018-10-09 12:15:45'),
(219, 171, 23, 0, 2, NULL, 1, 8, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:14:38', '2018-10-09 12:14:38'),
(218, 170, 23, 0, 2, NULL, 1, 7, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:14:38', '2018-10-09 12:14:38'),
(215, 167, 23, 0, 2, NULL, 1, 4, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:13:55', '2018-10-09 12:13:55'),
(214, 166, 23, 0, 2, NULL, 1, 3, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:13:55', '2018-10-09 12:13:55'),
(213, 165, 32, 0, 2, NULL, 1, 2, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:13:55', '2018-10-09 12:13:55'),
(212, 164, 24, 0, 2, NULL, 1, 1, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:13:55', '2018-10-09 12:13:55'),
(230, 182, 32, 0, 2, NULL, 1, 16, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:27:35', '2018-10-09 12:27:35'),
(229, 181, 21, 0, 2, NULL, 1, 15, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:27:35', '2018-10-09 12:27:35'),
(228, 180, 24, 0, 2, NULL, 1, 14, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:27:34', '2018-10-09 12:27:34'),
(231, 183, 29, 0, 2, NULL, 1, 17, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:27:35', '2018-10-09 12:27:35'),
(232, 184, 0, 0, 2, NULL, 1, 18, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:33:40', '2018-10-09 12:33:40'),
(233, 185, 0, 0, 2, NULL, 1, 19, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:33:40', '2018-10-09 12:33:40'),
(234, 186, 0, 0, 2, NULL, 1, 20, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:33:40', '2018-10-09 12:33:40'),
(235, 187, 0, 0, 2, NULL, 1, 21, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:33:40', '2018-10-09 12:33:40'),
(236, 188, 0, 0, 2, NULL, 1, 22, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:35:28', '2018-10-09 12:35:28'),
(237, 189, 0, 0, 2, NULL, 1, 23, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:35:28', '2018-10-09 12:35:28'),
(238, 190, 0, 0, 2, NULL, 1, 24, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:35:28', '2018-10-09 12:35:28'),
(239, 191, 0, 0, 2, NULL, 1, 25, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:35:28', '2018-10-09 12:35:28'),
(240, 192, 0, 0, 2, NULL, 1, 26, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:41:28', '2018-10-09 12:41:28'),
(241, 193, 0, 0, 2, NULL, 1, 27, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:41:28', '2018-10-09 12:41:28'),
(242, 194, 0, 0, 2, NULL, 1, 28, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:41:28', '2018-10-09 12:41:28'),
(243, 195, 0, 0, 2, NULL, 1, 29, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:41:28', '2018-10-09 12:41:28'),
(244, 196, 0, 0, 2, NULL, 1, 30, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:58:25', '2018-10-09 12:58:25'),
(245, 197, 0, 0, 2, NULL, 1, 31, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:58:25', '2018-10-09 12:58:25'),
(246, 198, 0, 0, 2, NULL, 1, 32, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:58:25', '2018-10-09 12:58:25'),
(247, 199, 29, 0, 2, NULL, 1, 33, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 12:58:25', '2018-10-09 12:58:25'),
(248, 200, 0, 0, 2, NULL, 1, 34, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 13:00:18', '2018-10-09 13:00:18'),
(249, 201, 0, 0, 2, NULL, 1, 35, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 13:00:18', '2018-10-09 13:00:18'),
(250, 202, 0, 0, 2, NULL, 1, 36, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 13:00:18', '2018-10-09 13:00:18'),
(251, 203, 29, 0, 2, NULL, 1, 37, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-09 13:00:18', '2018-10-09 13:00:18'),
(252, 204, 22, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 20:58:20', '2018-10-12 20:58:20'),
(253, 205, 21, 0, 2, NULL, 1, 1, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 21:03:55', '2018-10-12 21:03:55'),
(254, 206, 0, 0, 2, NULL, 1, 2, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 21:03:55', '2018-10-12 21:03:55'),
(255, 207, 0, 0, 2, NULL, 1, 3, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 21:03:55', '2018-10-12 21:03:55'),
(256, 208, 0, 0, 2, NULL, 1, 4, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 21:03:55', '2018-10-12 21:03:55'),
(257, 209, 21, 0, 2, NULL, 1, 1, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:06:08', '2018-10-12 22:06:08'),
(258, 210, 21, 0, 2, NULL, 1, 2, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:06:08', '2018-10-12 22:06:08'),
(259, 211, 21, 0, 2, NULL, 1, 3, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:06:08', '2018-10-12 22:06:08'),
(260, 212, 21, 0, 2, NULL, 1, 4, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:06:09', '2018-10-12 22:06:09'),
(261, 213, 21, 0, 2, NULL, 1, 1, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:08:41', '2018-10-12 22:08:41'),
(262, 214, 0, 0, 2, NULL, 1, 2, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:08:41', '2018-10-12 22:08:41'),
(263, 215, 0, 0, 2, NULL, 1, 3, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:08:41', '2018-10-12 22:08:41'),
(264, 216, 0, 0, 2, NULL, 1, 4, 0, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-12 22:08:41', '2018-10-12 22:08:41'),
(265, 217, 29, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:06:09', '2018-10-13 00:06:09'),
(266, 218, 32, 0, 2, NULL, 1, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:06:09', '2018-10-13 00:06:09'),
(267, 220, 0, 0, 2, NULL, 1, 3, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:06:09', '2018-10-13 00:06:09'),
(268, 221, 30, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:20:46', '2018-10-13 00:20:46'),
(269, 222, 0, 0, 2, NULL, 1, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:20:46', '2018-10-13 00:20:46'),
(270, 223, 23, 0, 2, NULL, 1, 3, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:20:47', '2018-10-13 00:20:47'),
(271, 224, 0, 0, 2, NULL, 1, 4, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:20:47', '2018-10-13 00:20:47'),
(272, 225, 31, 0, 2, NULL, 1, 1, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:34:59', '2018-10-13 00:34:59'),
(273, 226, 0, 0, 2, NULL, 1, 2, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:34:59', '2018-10-13 00:34:59'),
(274, 227, 0, 0, 2, NULL, 1, 3, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:34:59', '2018-10-13 00:34:59'),
(275, 228, 0, 0, 2, NULL, 1, 4, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:34:59', '2018-10-13 00:34:59'),
(276, 229, 31, 0, 2, NULL, 1, 5, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:35:54', '2018-10-13 00:35:54'),
(277, 230, 0, 0, 2, NULL, 1, 6, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:35:54', '2018-10-13 00:35:54'),
(278, 231, 0, 0, 2, NULL, 1, 7, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:35:54', '2018-10-13 00:35:54'),
(279, 232, 21, 0, 2, NULL, 1, 8, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:35:54', '2018-10-13 00:35:54'),
(280, 233, 0, 0, 2, NULL, 1, 9, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:39:32', '2018-10-13 00:39:32'),
(281, 234, 23, 0, 2, NULL, 1, 10, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:39:32', '2018-10-13 00:39:32'),
(282, 235, 0, 0, 2, NULL, 1, 11, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:39:33', '2018-10-13 00:39:33'),
(283, 236, 0, 0, 2, NULL, 1, 12, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:39:33', '2018-10-13 00:39:33'),
(284, 237, 0, 0, 2, NULL, 1, 13, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:31', '2018-10-13 00:50:31'),
(285, 238, 23, 0, 2, NULL, 1, 14, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:31', '2018-10-13 00:50:31'),
(286, 239, 21, 0, 2, NULL, 1, 15, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:32', '2018-10-13 00:50:32'),
(287, 240, 0, 0, 2, NULL, 1, 16, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:32', '2018-10-13 00:50:32'),
(288, 241, 23, 0, 2, NULL, 1, 17, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:41', '2018-10-13 00:50:41'),
(289, 242, 0, 0, 2, NULL, 1, 18, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:41', '2018-10-13 00:50:41'),
(290, 243, 0, 0, 2, NULL, 1, 19, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:42', '2018-10-13 00:50:42'),
(291, 244, 0, 0, 2, NULL, 1, 20, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:50:42', '2018-10-13 00:50:42'),
(292, 245, 0, 0, 2, NULL, 1, 21, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:52:40', '2018-10-13 00:52:40'),
(293, 246, 0, 0, 2, NULL, 1, 22, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:52:40', '2018-10-13 00:52:40'),
(294, 247, 31, 0, 2, NULL, 1, 23, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:52:40', '2018-10-13 00:52:40'),
(295, 248, 0, 0, 2, NULL, 1, 24, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:52:40', '2018-10-13 00:52:40'),
(296, 249, 20, 0, 2, NULL, 1, 25, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:03', '2018-10-13 00:53:03'),
(297, 250, 0, 0, 2, NULL, 1, 26, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:03', '2018-10-13 00:53:03'),
(298, 251, 0, 0, 2, NULL, 1, 27, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:03', '2018-10-13 00:53:03'),
(299, 252, 0, 0, 2, NULL, 1, 28, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:04', '2018-10-13 00:53:04'),
(300, 253, 20, 0, 2, NULL, 1, 29, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:18', '2018-10-13 00:53:18'),
(301, 254, 0, 0, 2, NULL, 1, 30, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:18', '2018-10-13 00:53:18'),
(302, 255, 0, 0, 2, NULL, 1, 31, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:18', '2018-10-13 00:53:18'),
(303, 256, 0, 0, 2, NULL, 1, 32, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:53:18', '2018-10-13 00:53:18'),
(304, 257, 31, 0, 2, NULL, 1, 33, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:56:36', '2018-10-13 00:56:36'),
(305, 258, 0, 0, 2, NULL, 1, 34, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:56:36', '2018-10-13 00:56:36'),
(306, 259, 0, 0, 2, NULL, 1, 35, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:56:36', '2018-10-13 00:56:36'),
(307, 260, 0, 0, 2, NULL, 1, 36, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 00:56:36', '2018-10-13 00:56:36'),
(308, 261, 29, 0, 2, NULL, 1, 37, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:02:30', '2018-10-13 01:02:30'),
(309, 262, 0, 0, 2, NULL, 1, 38, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:02:30', '2018-10-13 01:02:30'),
(310, 263, 0, 0, 2, NULL, 1, 39, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:02:30', '2018-10-13 01:02:30'),
(311, 264, 0, 0, 2, NULL, 1, 40, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:02:31', '2018-10-13 01:02:31'),
(312, 265, 21, 0, 2, NULL, 1, 41, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:20', '2018-10-13 01:03:20'),
(313, 266, 0, 0, 2, NULL, 1, 42, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:20', '2018-10-13 01:03:20'),
(314, 267, 0, 0, 2, NULL, 1, 43, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:20', '2018-10-13 01:03:20'),
(315, 268, 0, 0, 2, NULL, 1, 44, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:20', '2018-10-13 01:03:20'),
(316, 269, 21, 0, 2, NULL, 1, 45, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:25', '2018-10-13 01:03:25'),
(317, 270, 0, 0, 2, NULL, 1, 46, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:25', '2018-10-13 01:03:25'),
(318, 271, 0, 0, 2, NULL, 1, 47, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:26', '2018-10-13 01:03:26'),
(319, 272, 0, 0, 2, NULL, 1, 48, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:03:26', '2018-10-13 01:03:26'),
(320, 273, 21, 0, 2, NULL, 1, 49, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:05:57', '2018-10-13 01:05:57'),
(321, 274, 0, 0, 2, NULL, 1, 50, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:05:57', '2018-10-13 01:05:57'),
(322, 275, 0, 0, 2, NULL, 1, 51, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:05:57', '2018-10-13 01:05:57'),
(323, 276, 0, 0, 2, NULL, 1, 52, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:05:58', '2018-10-13 01:05:58'),
(324, 277, 0, 0, 2, NULL, 1, 53, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:06:17', '2018-10-13 01:06:17'),
(325, 278, 0, 0, 2, NULL, 1, 54, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:06:17', '2018-10-13 01:06:17'),
(326, 279, 27, 0, 2, NULL, 1, 55, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:06:17', '2018-10-13 01:06:17'),
(327, 280, 0, 0, 2, NULL, 1, 56, 1, 1, 'no_status', 2018, '', '', '', '', '', '2018-10-13 01:06:17', '2018-10-13 01:06:17');

-- --------------------------------------------------------

--
-- Table structure for table `student_payment_months`
--

CREATE TABLE `student_payment_months` (
  `id` int(11) NOT NULL,
  `student_payment_id` int(11) NOT NULL,
  `month_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_payment_months`
--

INSERT INTO `student_payment_months` (`id`, `student_payment_id`, `month_name`, `created_at`, `updated_at`) VALUES
(61, 38, 'April', '2018-07-05 08:06:06', '2018-07-05 08:06:06'),
(60, 38, 'January', '2018-07-05 08:06:06', '2018-07-05 08:06:06'),
(63, 35, 'April', '2018-08-02 06:17:54', '2018-08-02 06:17:54'),
(62, 35, 'January', '2018-08-02 06:17:54', '2018-08-02 06:17:54'),
(64, 42, 'January', '2018-08-02 07:31:46', '2018-08-02 07:31:46'),
(65, 42, 'April', '2018-08-02 07:31:46', '2018-08-02 07:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(100) NOT NULL,
  `min_mark` int(11) NOT NULL,
  `max_mark` int(11) NOT NULL,
  `grade_id` varchar(45) NOT NULL,
  `parent_subject_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '10000',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `min_mark`, `max_mark`, `grade_id`, `parent_subject_id`, `priority`, `created_at`, `updated_at`) VALUES
(282, 'Quran ', 0, 0, '23', 1, 10000, '2018-08-19 13:23:03', '2018-08-26 20:27:40'),
(283, 'Islamic Studies', 0, 0, '23', 1, 10000, '2018-08-26 20:26:17', '2018-08-26 20:26:17'),
(284, 'Islamic Studies', 0, 0, '24', 1, 10000, '2018-08-26 20:29:52', '2018-08-26 20:29:52'),
(285, 'Quran ', 0, 0, '24', 1, 10000, '2018-08-26 20:30:00', '2018-08-26 20:30:00'),
(288, 'Islamic Studies', 0, 0, '25', 1, 10000, '2018-08-26 20:33:27', '2018-08-26 20:33:27'),
(289, 'Quran ', 0, 0, '25', 1, 10000, '2018-08-26 20:33:32', '2018-08-26 20:33:32'),
(292, 'Islamic Studies', 0, 0, '26', 1, 10000, '2018-08-26 20:36:39', '2018-08-26 20:36:39'),
(293, 'Quran ', 0, 0, '26', 1, 10000, '2018-08-26 20:36:46', '2018-08-26 20:36:46'),
(296, 'Islamic Studies', 0, 0, '27', 1, 10000, '2018-08-26 20:40:07', '2018-08-26 20:40:07'),
(297, 'Quran ', 0, 0, '27', 1, 10000, '2018-08-26 20:40:13', '2018-08-26 20:40:13'),
(300, 'Islamic Studies', 0, 0, '29', 1, 10000, '2018-08-26 20:42:54', '2018-08-26 20:42:54'),
(301, 'Quran ', 0, 0, '29', 1, 10000, '2018-08-26 20:43:01', '2018-08-26 20:43:01'),
(304, 'Islamic Studies', 0, 0, '36', 1, 10000, '2018-08-26 20:45:24', '2018-08-26 20:45:24'),
(305, 'Quran ', 0, 0, '36', 1, 10000, '2018-08-26 20:45:33', '2018-08-26 20:45:33'),
(306, 'Islamic Studies', 0, 0, '40', 1, 10000, '2018-08-26 20:45:57', '2018-08-26 20:45:57'),
(307, 'Quran ', 0, 0, '40', 1, 10000, '2018-08-26 20:46:03', '2018-08-26 20:46:03'),
(310, 'Islamic Studies', 0, 0, '41', 1, 10000, '2018-08-26 20:46:49', '2018-08-26 20:46:49'),
(311, 'Quran ', 0, 0, '41', 1, 10000, '2018-08-26 20:46:55', '2018-08-26 20:46:55'),
(314, 'Islamic Studies', 0, 0, '42', 1, 10000, '2018-08-26 21:08:34', '2018-08-26 21:08:34'),
(315, 'Quran ', 0, 0, '42', 1, 10000, '2018-08-26 21:08:46', '2018-08-26 21:08:46'),
(318, 'Islamic Studies', 0, 0, '43', 1, 10000, '2018-08-26 21:10:19', '2018-08-26 21:10:19'),
(319, 'Quran ', 0, 0, '43', 1, 10000, '2018-08-26 21:10:28', '2018-08-26 21:10:28'),
(322, 'Arabic', 0, 0, '28', 2, 10000, '2018-09-02 00:11:38', '2018-09-02 00:11:38'),
(323, 'Quran ', 0, 0, '28', 1, 10000, '2018-09-02 00:11:44', '2018-09-02 00:11:44'),
(324, 'Islamic Studies', 0, 0, '28', 1, 10000, '2018-09-02 00:11:48', '2018-09-02 00:11:48'),
(325, 'Arabic', 0, 0, '23', 2, 10000, '2018-09-02 00:12:17', '2018-09-02 00:12:17'),
(327, 'Arabic', 0, 0, '25', 2, 10000, '2018-09-02 00:13:07', '2018-09-02 00:13:07'),
(328, 'Arabic', 0, 0, '26', 2, 10000, '2018-09-02 00:13:27', '2018-09-02 00:13:27'),
(329, 'Arabic', 0, 0, '27', 2, 10000, '2018-09-02 00:13:55', '2018-09-02 00:13:55'),
(330, 'Arabic', 0, 0, '29', 2, 10000, '2018-09-02 00:14:32', '2018-09-02 00:14:32'),
(333, 'Arabic', 0, 0, '35', 2, 10000, '2018-09-02 00:14:58', '2018-09-02 00:14:58'),
(334, 'Arabic', 0, 0, '40', 2, 10000, '2018-09-02 00:15:39', '2018-09-02 00:15:39'),
(336, 'Arabic', 0, 0, '42', 2, 10000, '2018-09-02 00:16:23', '2018-09-02 00:16:23'),
(337, 'Arabic', 0, 0, '43', 2, 10000, '2018-09-02 00:16:51', '2018-09-02 00:16:51'),
(338, 'ASSISTANT', 0, 0, '23', 1, 10000, '2018-09-02 00:22:02', '2018-09-02 00:22:02'),
(339, 'ASSISTANT', 0, 0, '24', 1, 10000, '2018-09-02 00:22:13', '2018-09-02 00:22:13'),
(340, 'ASSISTANT', 0, 0, '25', 1, 10000, '2018-09-02 00:22:23', '2018-09-02 00:22:23'),
(341, 'ASSISTANT', 0, 0, '26', 1, 10000, '2018-09-02 00:22:33', '2018-09-02 00:22:33'),
(342, 'ASSISTANT', 0, 0, '27', 1, 10000, '2018-09-02 00:22:42', '2018-09-02 00:22:42'),
(343, 'ASSISTANT', 0, 0, '28', 1, 10000, '2018-09-02 00:22:51', '2018-09-02 00:22:51'),
(344, 'ASSISTANT', 0, 0, '29', 1, 10000, '2018-09-02 00:23:03', '2018-09-02 00:23:03'),
(345, 'ASSISTANT', 0, 0, '35', 1, 10000, '2018-09-02 00:23:16', '2018-09-02 00:23:16'),
(346, 'ASSISTANT', 0, 0, '36', 1, 10000, '2018-09-02 00:23:28', '2018-09-02 00:23:28'),
(347, 'ASSISTANT', 0, 0, '40', 1, 10000, '2018-09-02 00:23:37', '2018-09-02 00:23:37'),
(348, 'ASSISTANT', 0, 0, '41', 1, 10000, '2018-09-02 00:23:48', '2018-09-02 00:23:48'),
(349, 'ASSISTANT', 0, 0, '42', 1, 10000, '2018-09-02 00:24:01', '2018-09-02 00:24:01'),
(350, 'ASSISTANT', 0, 0, '43', 1, 10000, '2018-09-02 00:24:12', '2018-09-02 00:24:12'),
(353, 'Writing', 0, 0, '41', 2, 10000, '2018-09-16 10:17:02', '2018-09-16 10:17:02'),
(354, 'Reading', 0, 0, '41', 2, 10000, '2018-09-16 10:17:09', '2018-09-16 10:17:09'),
(355, 'Writing', 0, 0, '24', 2, 10000, '2018-09-17 07:45:32', '2018-09-17 07:45:32'),
(356, 'Reading', 0, 0, '24', 2, 10000, '2018-09-17 07:46:05', '2018-09-17 07:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `salary` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `assistant_teacher` tinyint(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `salary`, `user_id`, `assistant_teacher`, `created_at`, `updated_at`, `deleted`) VALUES
(8, 96523, 51, 1, '2018-05-27 10:53:35', '2018-05-27 10:53:35', 0),
(16, NULL, 63, 1, '2018-08-26 19:27:05', '2018-09-01 23:48:31', 0),
(17, NULL, 64, 1, '2018-08-26 19:30:31', '2018-09-01 21:49:49', 0),
(18, NULL, 65, 1, '2018-08-26 19:34:16', '2018-09-01 23:46:55', 0),
(19, NULL, 66, 1, '2018-08-26 19:35:24', '2018-09-18 09:03:16', 0),
(20, NULL, 67, 1, '2018-08-26 19:37:43', '2018-09-02 00:01:39', 0),
(21, NULL, 68, 1, '2018-08-26 19:39:03', '2018-09-01 21:42:51', 0),
(22, NULL, 69, 2, '2018-08-26 19:40:18', '2018-09-01 23:44:31', 0),
(23, NULL, 70, 2, '2018-08-26 19:42:03', '2018-09-01 20:37:27', 0),
(24, NULL, 71, 1, '2018-08-26 19:43:09', '2018-08-26 19:43:09', 0),
(25, NULL, 72, 1, '2018-08-26 19:44:17', '2018-09-01 20:33:08', 0),
(26, NULL, 73, 1, '2018-08-26 19:45:50', '2018-09-02 00:03:57', 0),
(27, NULL, 74, 1, '2018-08-26 19:47:01', '2018-09-01 23:52:06', 0),
(28, NULL, 75, 1, '2018-08-26 19:48:38', '2018-09-01 23:49:52', 0),
(29, NULL, 76, 1, '2018-08-26 19:49:46', '2018-09-01 05:47:03', 0),
(30, NULL, 77, 1, '2018-08-26 19:50:42', '2018-08-26 19:50:42', 0),
(31, NULL, 78, 1, '2018-08-26 19:51:40', '2018-09-01 20:19:10', 0),
(32, NULL, 79, 1, '2018-08-26 19:52:55', '2018-09-01 23:50:29', 0),
(33, NULL, 80, 1, '2018-08-26 19:54:13', '2018-09-01 23:46:08', 0),
(34, NULL, 81, 1, '2018-08-26 19:55:31', '2018-09-01 23:52:28', 0),
(35, NULL, 82, 2, '2018-08-26 19:56:51', '2018-09-01 23:45:01', 0),
(36, NULL, 83, 1, '2018-08-26 19:57:59', '2018-09-01 23:52:51', 0),
(37, NULL, 84, 2, '2018-08-26 19:59:07', '2018-09-01 23:49:10', 0),
(38, NULL, 85, 2, '2018-08-26 20:00:23', '2018-09-01 23:47:49', 0),
(39, NULL, 86, 2, '2018-08-26 20:01:43', '2018-09-01 23:51:31', 0),
(40, NULL, 87, 2, '2018-08-26 20:02:54', '2018-09-01 23:53:23', 0),
(41, NULL, 88, 2, '2018-08-26 20:05:08', '2018-09-01 23:55:20', 0),
(42, NULL, 89, 2, '2018-08-26 20:06:09', '2018-09-01 20:45:23', 0),
(43, NULL, 90, 1, '2018-08-26 20:08:09', '2018-09-01 22:11:41', 0),
(44, NULL, 144, 2, '2018-09-01 23:54:52', '2018-09-01 23:54:52', 0),
(45, NULL, 145, 2, '2018-09-01 23:56:09', '2018-09-01 23:56:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teachers_notes`
--

CREATE TABLE `teachers_notes` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `note_text` text NOT NULL,
  `note_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teachers_notes`
--

INSERT INTO `teachers_notes` (`id`, `teacher_id`, `note_text`, `note_date`, `created_at`, `updated_at`) VALUES
(12, 20, 'Assistnat for Pre-1. \r\nTeacher for 1st grade. ', '2018-09-01', '2018-09-02 00:03:03', '2018-09-02 00:03:11'),
(13, 25, 'Assistant for Beginners. \r\nQuran Teacher for 1st. ', '2018-09-01', '2018-09-02 00:20:32', '2018-09-02 00:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `non_encrept_pass` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `father_name` varchar(45) NOT NULL,
  `mother_name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `type` enum('admin','employee','student','teacher','parent') NOT NULL,
  `birthdate` date NOT NULL DEFAULT '2000-01-01'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `non_encrept_pass`, `salt`, `email`, `email2`, `activation_code`, `first_name`, `last_name`, `father_name`, `mother_name`, `address`, `mobile`, `phone`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `created_at`, `updated_at`, `deleted`, `type`, `birthdate`) VALUES
(1, '97.73.244.53', 'admin@admin.com', '$2y$08$PfyMghUU2uoUIV3t4PFxuu/rDKFWcESKjdvVnL7MKEqf7NfMhhp9G', '', NULL, 'admin@admin.com', NULL, '', 'admin', '', '', '', '23', '23', '2', NULL, NULL, NULL, 1525932546, 1539364011, 1, NULL, '2018-05-15 00:00:00', '2018-05-22 00:00:00', 0, 'admin', '2000-01-01'),
(63, '97.73.244.53', 'jalmajzoub@gmail.com', '$2y$08$2upcDhVxNO1T0Hx3Jp9fv.DA8MAV62bL0fbJbSKDNc0tN4GUyqzZ6', 'jida1234', NULL, 'jalmajzoub@gmail.com', NULL, '', 'Jida', 'Al-Majzoub', 'N/A', 'N/A', '', '626-428-7374', '', NULL, NULL, NULL, 1535311625, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:48:31', 0, 'teacher', '1976-05-10'),
(64, '97.73.244.53', 'bkana2000@gmail.com', '$2y$08$0KfQfpvopeozbXkJpB4YSeQ0Gd/LHvwiuTw.wGAFp5bs5U0kTnFja', 'bkana456', NULL, 'bkana2000@gmail.com', NULL, '', 'Bushra ', 'Kanawati', 'N/A', 'N/A', '', '', '', NULL, NULL, NULL, 1535311831, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 21:49:49', 0, 'teacher', '2018-01-01'),
(65, '97.73.244.53', 'nabdina@hotmail.com', '$2y$08$cqcBhAJLZGFalnlrUvFfIupUhvijQuQnJwjsumztMgOlkmEiesU..', 'dina89000', NULL, 'nabdina@hotmail.com', NULL, '', 'Dina', 'Assil', 'N/A', 'N/A', '', '951-751-9073', '', NULL, NULL, NULL, 1535312056, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:46:55', 0, 'teacher', '1968-07-05'),
(66, '97.73.244.53', 'teacher@teacher.com', '$2y$08$TBmh.iqx4WlTB2QOrj1GPOwG012NlY2zae9msG8crw61Ql9IDoZDm', '123456789', NULL, 'teacher@teacher.com', NULL, '', 'Bayan', 'Naamani', 'N/A', 'N/A', '', '714-732-1830', '', NULL, NULL, NULL, 1535312124, 1538477905, 1, NULL, '0000-00-00 00:00:00', '2018-09-18 09:03:16', 0, 'teacher', '1986-02-01'),
(67, '97.73.244.53', 'estwani94@gmail.com', '$2y$08$ygYj0Lu4xymeURv8eIN36.sQe0GEScVX3Go0DEU/ekIWXaGtm5yMu', 'dima9494', NULL, 'estwani94@gmail.com', NULL, '', 'Dima', 'Estwani', 'N/A', 'N/A', '', '714-883-5360', '', NULL, NULL, NULL, 1535312263, 1537446109, 1, NULL, '0000-00-00 00:00:00', '2018-09-02 00:01:39', 0, 'teacher', '1994-05-22'),
(68, '97.73.244.53', 'estwani11@gmail.com', '$2y$08$XmsMSquBoActpozwiQ3i5OJV0yvzwpbF9R4gDjz1hlXdzr7A3wE1e', 'hania1101', NULL, 'estwani11@gmail.com', NULL, '', 'Hania', 'Estwani', 'N/A', 'N/A', '', '', '', NULL, NULL, NULL, 1535312343, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 21:42:51', 0, 'teacher', '2018-01-01'),
(69, '97.73.244.53', 'itafmurrah@yahoo.com', '$2y$08$yzC9KXVgauh.5ZWZLuwNw.ttyCQaq4EVpqS6PUBz80EXdZEfxw4ui', 'itafmurrah123', NULL, 'itafmurrah@yahoo.com', NULL, '', 'Itaf', 'Zoubiedi', 'N/A', 'N/A', '', '562-291-9585', '', NULL, NULL, NULL, 1535312418, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:44:31', 0, 'teacher', '1970-01-15'),
(70, '97.73.244.53', 'joumanaalsawaf@hotmail.com', '$2y$08$ukJPOrWGvLsQZHdE5COvROWxt2x6bYwIsq/1Nzs21r2FELc4a3sEy', 'joumana456', NULL, 'joumanaalsawaf@hotmail.com', NULL, '', 'Joumana', 'Alsawaf', 'N/A', 'N/A', '', '951-220-1828', '', NULL, NULL, NULL, 1535312523, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 20:37:27', 0, 'teacher', '1963-05-28'),
(71, '97.73.244.53', 'layla0720@gmail.com', '$2y$08$34jSVZW8BD9KUWtDAHrETOos3laLWEN/3c2tfHts05NRT3B0/J82u', 'layla0720', NULL, 'layla0720@gmail.com', NULL, '', 'Layla ', 'Bahar Aloom', 'N/A', 'N/A', '', '', '', NULL, NULL, NULL, 1535312589, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'teacher', '2018-01-01'),
(72, '97.73.244.53', 'linadoughani@gmail.com', '$2y$08$hYC00q0kddMF3lvIJtwpmu7x/2GsolGTp15sT72FDjP5mNmy.hrZq', 'lina1020', NULL, 'linadoughani@gmail.com', NULL, '', 'Lina', 'Doughani', 'N/A', 'N/A', '', '714-310-2683', '', NULL, NULL, NULL, 1535312657, 1535679723, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 20:33:08', 0, 'teacher', '1975-04-26'),
(73, '97.73.244.53', 's.maysan84@gmail.com', '$2y$08$xu5lhXzOongyAxoGKHvxq.LvJ0FciGfTdWXNvTb.as5qaDg7Sqhoa', 'maysan84', NULL, 's.maysan84@gmail.com', NULL, '', 'Maysan', 'Saad Alden', 'N/A', 'N/A', '', '714-728-7118', '', NULL, NULL, NULL, 1535312750, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-02 00:03:57', 0, 'teacher', '1984-02-28'),
(74, '97.73.244.53', 'maysounkattan@hotmail.com', '$2y$08$VTn0CM8qyDnzZewoiRW6p.rnVLrkkEGwhyri4wNEXrTx8tQHbpHHu', 'maysoun11', NULL, 'maysounkattan@hotmail.com', NULL, '', 'Maysoun', 'Kattan', 'N/A', 'N/A', '', '714-209-3289', '', NULL, NULL, NULL, 1535312821, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:52:06', 0, 'teacher', '1963-01-20'),
(75, '97.73.244.53', 'nada84us@yahoo.com', '$2y$08$t//1tq2MUkN3YDBXPxScGegkiOktBu65dlgy9iGCR1snrgCoWpGjO', 'nada8411', NULL, 'nada84us@yahoo.com', NULL, '', 'Nada', 'Najjar', 'N/A', 'N/A', '', '909-614-3326', '', NULL, NULL, NULL, 1535312918, 1535868252, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:49:52', 0, 'teacher', '1965-05-12'),
(76, '97.73.244.53', '22noor22noor@gmail.com', '$2y$08$BC1JAJAQFN91rTGxtb66p.L2k3we.4XRkx6zEN4IvTJO8XtJZll8K', 'noor2222', NULL, '22noor22noor@gmail.com', NULL, '', 'Noor', 'Naji', 'N/A', 'N/A', '', '', '', NULL, NULL, NULL, 1535312986, 1535780850, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 05:47:03', 0, 'teacher', '2018-01-01'),
(77, '97.73.244.53', 'malam@minaretacademy.net', '$2y$08$jS6Yx880DSba9FM/PbWFtuA3PDGGG6em0SYV2xxPiu6hAOQ4o7.I6', 'malam', NULL, 'malam@minaretacademy.net', NULL, '', 'Mayada', 'Alam', 'N/A', 'N/A', '', '', '', NULL, NULL, NULL, 1535313042, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'teacher', '2018-01-01'),
(78, '97.73.244.53', 'raniaalzabibi@hotmail.com', '$2y$08$i2AzqiW9ff7I8Sp2dJQRAuAwtFFXrCbHsjGJfAiMAWP0ceyT7lX4G', 'rania1257', NULL, 'raniaalzabibi@hotmail.com', NULL, '', 'Rania', 'Alzabibi', 'N/A', 'N/A', '', '714-403-0793', '', NULL, NULL, NULL, 1535313100, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 20:19:10', 0, 'teacher', '1979-07-05'),
(79, '97.73.244.53', 'rjaboulie@hotmail.com', '$2y$08$Pim/jYoaT6najpu.zr6DZeixuqrT0onnEW8tRZ9N2BbEzhLjvGV7m', 'rjaboulie33', NULL, 'rjaboulie@hotmail.com', NULL, '', 'Roua ', 'Jaboulie', 'N/A', 'N/A', '', '714-725-2197', '', NULL, NULL, NULL, 1535313175, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:50:29', 0, 'teacher', '1972-10-01'),
(80, '97.73.244.53', 'samahmisri@gmail.com', '$2y$08$5PUXs6PPGZNB1Mbr3f1EAe/LzZBmyQ29onQLCW.lTgm4XLJGHidUu', 'samahmisri555', NULL, 'samahmisri@gmail.com', NULL, '', 'Samah', 'Misri', 'N/A', 'N/A', '', '714-873-4456', '', NULL, NULL, NULL, 1535313253, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:46:08', 0, 'teacher', '2018-09-01'),
(81, '97.73.244.53', 'sarahest64@gmail.com', '$2y$08$WpWOQaBjZlFDLvy25JzZWONlca2memIPH1Xc1vJBysE8DntsrdwUu', 'sarahest64', NULL, 'sarahest64@gmail.com', NULL, '', 'Sarah', 'Estwani', 'N/A', 'N/A', '', '714-873-4458', '', NULL, NULL, NULL, 1535313331, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:52:28', 0, 'teacher', '2018-04-06'),
(82, '97.73.244.53', 'shaimaahaithem@gmail.com', '$2y$08$4Uxy8ocM.3NtBszwhjyLhOhe8zZ2RxbX41WIRuCinmZHiPwcuAXkC', 'shaimaa345', NULL, 'shaimaahaithem@gmail.com', NULL, '', 'Shaimaa', 'Ahmed', 'N/A', 'N/A', '', '949-690-4913', '', NULL, NULL, NULL, 1535313411, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:45:02', 0, 'teacher', '1970-01-15'),
(83, '97.73.244.53', 'suha.attar@gmail.com', '$2y$08$pSUs005/0mwtDHCG/6bdhuhqh979zPkXMy5G8UptWXn8Rz87sS6RG', 'suhaa789', NULL, 'suha.attar@gmail.com', NULL, '', 'Suha', 'Attar', 'N/A', 'N/A', '', '562-330-3947', '', NULL, NULL, NULL, 1535313479, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:52:51', 0, 'teacher', '1964-06-09'),
(84, '97.73.244.53', 'zeina@zsquared.me', '$2y$08$EeCv8TbBFg6EBFBf4vUGq.xCdSmJb3XXQVtb88hSadSfWfJ7HIYOS', 'zsquared22', NULL, 'zeina@zsquared.me', NULL, '', 'Zeina', 'El-Kassem', 'N/A', 'N/A', '', '714-261-5545', '', NULL, NULL, NULL, 1535313547, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:49:10', 0, 'teacher', '2018-01-27'),
(85, '97.73.244.53', 'baroutaji70@hotmail.com', '$2y$08$59q95cWAk2vol4Ya0r3eWOIG41zwr8K.MEOpI2KuIeXIACvMuuasO', 'hazarbar70', NULL, 'baroutaji70@hotmail.com', NULL, '', 'Hazar', 'Baroutaji', 'N/A', 'N/A', '', '714-510-4771', '', NULL, NULL, NULL, 1535313623, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:47:49', 0, 'teacher', '1970-09-14'),
(86, '97.73.244.53', 'joudy47@gmail.com', '$2y$08$IXhefj/3cFGzFHNWBB23/.QFwvnfArfyAxctRIyWDrMTfs4mSXldW', 'joudy477', NULL, 'joudy47@gmail.com', NULL, '', 'Joudy', 'Abdulal', 'N/A', 'N/A', '', '901-642-3878', '', NULL, NULL, NULL, 1535313703, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:51:31', 0, 'teacher', '1998-05-10'),
(87, '97.73.244.53', 'riskandarani25@gmail.com', '$2y$08$yyw4iODBZI96PeAU9w0cZ.QUsjPUgfto9P/ZcjtSXrK67gns3JC/K', 'ramaisk25', NULL, 'riskandarani25@gmail.com', NULL, '', 'Rama', 'Iskandarani', 'N/A', 'N/A', '', '714-399-8902', '', NULL, NULL, NULL, 1535313774, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:53:23', 0, 'teacher', '1997-05-17'),
(88, '97.73.244.53', 'ranamtow@yahoo.com', '$2y$08$pdYU6989vaVOCEutHJ939.nHjOnALGCfEAsJwpObx5TZvv6VToPOW', 'ranamtow', NULL, 'ranamtow@yahoo.com', NULL, '', 'Rana', 'Almughrabi', 'N/A', 'N/A', '', '714-737-5148', '', NULL, NULL, NULL, 1535313908, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 23:55:20', 0, 'teacher', '1973-01-01'),
(89, '97.73.244.53', 'saloum_83@yahoo.com', '$2y$08$1ILUP7AJd4g6lZ.nEEwCreO06vCQo0DAg2KHaNoyZgRzvhEkvKu2i', 'salmasaleh83', NULL, 'saloum_83@yahoo.com', NULL, '', 'Selma', 'Saleh', 'N/A', 'N/A', '', '714-829-0298', '', NULL, NULL, NULL, 1535313969, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 20:45:23', 0, 'teacher', '1983-10-08'),
(90, '97.73.244.53', 'samarmoheddin@yahoo.com', '$2y$08$zBH5N0tgsx2PzqWfFPjode3DkLznzCimL6dvOMGe3vh4Se8cwymIm', 'samar678', NULL, 'samarmoheddin@yahoo.com', NULL, '', 'Samar', 'Hajjar', 'N/A', 'N/A', '', '714-767-1826', '', NULL, NULL, NULL, 1535314089, 1535640354, 1, NULL, '0000-00-00 00:00:00', '2018-09-01 22:11:41', 0, 'teacher', '2018-01-01'),
(95, '97.73.244.53', 'haninjaber2006@hotmail.com', '$2y$08$nCTSME.WV079J//Uiu8KNurRcno1z1g7wxP5YGiF.LXfeNKfFsIxe', 'Alashqar2006', NULL, 'haninjaber2006@hotmail.com', '', '', '', '', 'Hesham', 'Hanin Jaber', '3146 W Ball Rd # 13 Anaheim, CA 92804', '', NULL, NULL, NULL, NULL, 1535691562, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-08-31 05:00:24', 0, 'parent', '2000-01-01'),
(96, '97.73.244.53', 'hadi411@hotmail.com', '$2y$08$mUAQKOgUGrrYJSlACMBSUOxm5/Pael.39JIMqseMU.dTTdIZUVvd2', 'obeid411', NULL, 'hadi411@hotmail.com', '', '', '', '', 'Ahmad', 'Sali Dalloul', '5 Mesquite, Trabuco Canyon, CA 92679	', '', NULL, NULL, NULL, NULL, 1535692303, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(97, '97.73.244.53', 'Alifadumo673@gmail.com', '$2y$08$z/1Pe59x83i/AirIVr0qt./kjuee1gMY1eNbjH/FCUNfqGPMoS92u', 'zdahir673', NULL, 'Alifadumo673@gmail.com', '', '', '', '', 'Abdirashid', 'Fadumo Ali', '111 W Elm Street #409 Anaheim, CA 92805', '', NULL, NULL, NULL, NULL, 1535692791, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(98, '97.73.244.53', 'leesaharoun@yahoo.com', '$2y$08$NQNyUzXJGaV1KgHp2IJlAeAqpm4Qy5kTPuyimEC5kj6dJ8o48Y.B2', 'leesalelass123', NULL, 'leesaharoun@yahoo.com', '', '', '', '', 'Haroun', 'Ghada Haroun', '201 E Chapman Ave 41N Placentia, CA 92870', '', NULL, NULL, NULL, NULL, 1535693430, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(99, '97.73.244.53', 'rafidalkaisi@gmail.com', '$2y$08$lTzjYdw6gBFfzggH5Yb5Q.aO.dYRmCd29CPdAiXGXyqmItyw1IHP6', 'afalqaisi44', NULL, 'rafidalkaisi@gmail.com', '', '', '', '', 'Rafid Alkaisi', 'Noor Alnayyar', '8113 Cerritos Ave Apt# 79 Stanton, CA 90680', '', NULL, NULL, NULL, NULL, 1535693833, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(100, '97.73.244.53', 'ramroumeh@hotmail.com', '$2y$08$PUFdlTRup7Ya2sSmrIKiEuB6gWXBiRMue8fRMKAap6jAwqIGF13OK', 'bouhairi123', NULL, 'ramroumeh@hotmail.com', '', '', '', '', 'Karim', 'Lamiaa Khayat', '124 Pendant Irvine, CA 92620', '', NULL, NULL, NULL, NULL, 1535694340, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(101, '97.73.244.53', 'wkeshk@yahoo.com', '$2y$08$iM2TN5Z33FalEch6XAnAk.xo3CpktG1gNdhG5cRY12Luf.hk0zQWa', 'keshk387', NULL, 'wkeshk@yahoo.com', 'aiatraafat@yahoo.com', '', '', '', 'Walied', 'Aiat Keshk', '387 Draft Way Placentia, CA 92870', '', NULL, NULL, NULL, NULL, 1535694546, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(103, '97.73.244.53', 'hananalziq@gmail.com', '$2y$08$ShIcVm3KzElKZCK8fQi5XOYmcOGBgqITMcTmQG1ylZhF2HHXMqsdi', 'nasif1700', NULL, 'hananalziq@gmail.com', '', '', '', '', 'Ahmed', 'Hanan Zak', '1700 West Cerritos Ave unit 139 Anaheim, CA 9', '', NULL, NULL, NULL, NULL, 1535742393, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(105, '134.71.249.23', 'rizana.huda@gmail.com', '$2y$08$.3456IL.MLBQIpxFFU9/TOXwgNJtXTigCQu0YPid8KXBqarixMV4i', 'setiawan456', NULL, 'rizana.huda@gmail.com', 'wsetiawan3@yahoo.com', '', '', '', 'Willy', 'Rizana Huda', '18946 Vickie Ave #03 Cerritos, CA 90703', '', NULL, NULL, NULL, NULL, 1535747865, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(106, '134.71.249.23', 'khalil.itani1@gmail.com', '$2y$08$86z6JSyGjUBahqIsp0ZVKuHyXRtpmiJeIhu7GsFhUeD8AS3pF0jN2', 'itani2527', NULL, 'khalil.itani1@gmail.com', 'cestrada2527@yahoo.com', '', '', '', 'Khalil', 'Celia Estrada', '14740 San Esteban Dr. La Mirada, CA 90638', '', NULL, NULL, NULL, NULL, 1535748339, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(107, '97.73.244.53', 'a.abouelazm@gmail.com', '$2y$08$GdZd5xUm5XHGigLfBQ4dv.eyP/Bb2Am/r6b6wau66oLX90q5u3Tjq', 'elkamash180', NULL, 'a.abouelazm@gmail.com', '', '', '', '', 'Mohamed', 'Sherin Abouelazm', '180 N Muller St. #205 Anaheim, CA 92801', '', NULL, NULL, NULL, NULL, 1535756323, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(108, '97.73.244.53', 'dinasaied14@gmail.com', '$2y$08$3H9lxjhThsAied0v84.Ox.IbCddY/3Kca.WdtaCSvzoQBDrSRueZ6', 'abdelbaky14', NULL, 'dinasaied14@gmail.com', 'elarabyy@hotmail.com', '', '', '', 'Mahmoud', 'Dina Saied', '310 S Jefferson St. Placentia, CA 92870', '', NULL, NULL, NULL, NULL, 1535756687, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(109, '97.73.244.53', 'mahmood1_200y@yahoo.com', '$2y$08$T8ExrwQNxzm77f6uuu/cZesBysc9u2WtgxsyjgwIwRG/It7T/P3ZW', 'alshekhle1200', NULL, 'mahmood1_200y@yahoo.com', '', '', '', '', 'Mahmood', 'Qutadah Mohammed', '712 E South St 203, Anaheim, CA 92805', '', NULL, NULL, NULL, NULL, 1535757027, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(110, '97.73.244.53', 'ghadiaw@yahoo.com', '$2y$08$NsrYvyZ.9w1n5hGKURWMJ.C3Wa7fGWXnCTCinPo41PXEkcL940MZy', 'awad1779', NULL, 'ghadiaw@yahoo.com', '', '', '', '', 'N/A', 'Ghadeer Awad', '1779 Majestic Dr. Corona, CA 92880', '', NULL, NULL, NULL, NULL, 1535757397, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(111, '97.73.244.53', 'dkarim803@gmail.com', '$2y$08$j3DWwp0iMN0VrGVRpeOv2OvJTrCHWRsa5ylsfESf2RZ4WobT0oa6S', 'abdelkarim803', NULL, 'dkarim803@gmail.com', '', '', '', '', 'Arafat', 'Donia Abdelkarim', '85 Legacy Way Irvine, CA 92602', '', NULL, NULL, NULL, NULL, 1535757663, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(112, '97.73.244.53', 'Htahiri@ymail.com', '$2y$08$hkyIkanpgD5.iESl1aWLKuiWor0lBR6r/MooGee13lT2kVUcQ14rW', 'tahiri9280', NULL, 'Htahiri@ymail.com', '', '', '', '', 'Yousuf', 'Hediya Tahiri', '928 South Flint Ridge Way, Anaheim, CA 92808', '', NULL, NULL, NULL, NULL, 1535759015, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(113, '97.73.244.53', 'lanalanon@gmail.com', '$2y$08$K630lulcGoqv.drn556UfOGw9O6zkqzvesPMe5srwX7G6UkDcoCwy', 'aldajani6407', NULL, 'lanalanon@gmail.com', '', '', '', '', 'Tiem', 'Lana Aldajani', '6407 E. Calle Del Norte Anaheim, CA 92807', '', NULL, NULL, NULL, NULL, 1535759324, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(114, '97.73.244.53', 'rama.sabeh@gmail.com', '$2y$08$QHJ0nApfPsOGfRNBmv6EKO963Q.kD0CRgzK8zBUa.GnFYVR3EWb/O', 'habibeh8215', NULL, 'rama.sabeh@gmail.com', '', '', '', '', 'Abdulkareem', 'Rama Sabeh', '8215 E. White Oak Ridge Unit 95, Orange, CA 9', '', NULL, NULL, NULL, NULL, 1535759491, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(115, '97.73.244.53', 'Shantash_99@yahoo.com', '$2y$08$Ov/JV692iGbbHzabr/pnJ.fEwNWj66H8ynck1FVImNQOuojN5vCsC', 'abuseer99', NULL, 'Shantash_99@yahoo.com', '', '', '', '', 'Ehab Abu Sair', 'Samah Abu Hantash', '125 N. Maude ln, Anaheim, CA 92807	', '', NULL, NULL, NULL, NULL, 1535760626, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(116, '97.73.244.53', 'bushranoor_10@hotmail.com', '$2y$08$BzcHhyd6anDHuqtA302ff.nXHkXLd8igSTDMXcgjuL09ZtKFSOARy', 'sheikh21510', NULL, 'bushranoor_10@hotmail.com', '', '', '', '', 'Abdul Ghaffar', 'Bushra Sheikh', '215 N. Magnolia Ave. Anaheim # D Anaheim, CA ', '', NULL, NULL, NULL, NULL, 1535761278, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(117, '97.73.244.53', 'firasabdulrahman@yahoo.com', '$2y$08$cqdDTnjLVeqbFIb4ooVOU.hahUstJqoTa7Cpsehrw9un6FIHbxcwi', 'abdulrahman13032', NULL, 'firasabdulrahman@yahoo.com', '', '', '', '', 'Firas', 'Rasha', '13032 Casa Linda ln apt B Garden Grove, CA 92', '', NULL, NULL, NULL, NULL, 1535761572, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(118, '97.73.244.53', 'yasiralrikabi@gmail.com', '$2y$08$3k4y1PlSH1t3IH6lh63xXORq0wzhG/at.4UaKWTDnp4WAAxOd6DU2', 'alrikabi2828', NULL, 'yasiralrikabi@gmail.com', '', '', '', '', 'Yasir', 'Sarah Nafea', '2828 W.Lincoln Ave Anaheim, CA 92801', '', NULL, NULL, NULL, NULL, 1535762380, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(119, '97.73.244.53', 'neso_77@hotmail.com', '$2y$08$H8BEshqieLYpMDL8sVtxpOVV3.dkKPPl2zYSZFSqc2SEI0RmBl3ai', 'neso7740', NULL, 'neso_77@hotmail.com', '', '', '', '', 'Feras', 'Nisreen AlKhateeb', '4 Wilking Drive, Irvine, CA 92620', '', NULL, NULL, NULL, NULL, 1535762696, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(120, '97.73.244.53', 'dgismail@aol.com', '$2y$08$gl2zZRrHLRlXHYziJmjFU.8G9dAVcmtN1oMQbN5g14Xl0leoOkpuO', 'ismail9096', NULL, 'dgismail@aol.com', '', '', '', '', 'Samer', 'Donia Ismail', '9096 Fallbrook Canyon Drive, Corona, CA 92883', '', NULL, NULL, NULL, NULL, 1535763295, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(121, '97.73.244.53', 'karaki99@yahoo.com', '$2y$08$8vtxnyiH1UigZ4VRcxoc5OYZl/LIompe2wzgQqw06fTdvKEMaFW9W', '99kloubkaraki', NULL, 'karaki99@yahoo.com', '', '', '', '', 'Emad', 'Sondos Kloub', '409 S. Windmill Ln Anaheim, CA, 92807', '', NULL, NULL, NULL, NULL, 1535763747, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(122, '97.73.244.53', 'uzma_munnee@yahoo.com', '$2y$08$yi4jphx.M7Mb28AcqoOzXu3CWLnOQPRuMItxvJhUl8VzcDugNcWGa', 'mamsa8850', NULL, 'uzma_munnee@yahoo.com', 'abdur.mamsa@gmail.com', '', '', '', 'Abdurrehman ', 'Uzma Mamsa', '8850 East Garden View Drive Anaheim, CA 92808', '', NULL, NULL, NULL, NULL, 1535764261, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(123, '97.73.244.53', 'zidansfun@gmail.com', '$2y$08$gt1b9S8Na.SBacpzlw1otOJQGtSb0Dg9bwDU9qlXw28mPD1dKY9Pu', 'zidan852', NULL, 'zidansfun@gmail.com', '', '', '', '', 'Walid', 'Zaena Sabeh', '852 South Endicott Court Anaheim Hills 92808', '', NULL, NULL, NULL, NULL, 1535764579, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(124, '97.73.244.53', 'sbahrawy@hotmail.com', '$2y$08$N/WZwbG0MeLoMD6K/GCQu.SnwYNyieL0SqXhiTNV7jNXYArRW2Opi', 'bahrawy2531', NULL, 'sbahrawy@hotmail.com', '', '', '', '', 'Mostafa', 'Dina Kandil', '2531 Thistlewood lane, Corona, CA 92882', '', NULL, NULL, NULL, NULL, 1535765755, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(125, '97.73.244.53', 'abuhajjaj@yahoo.com', '$2y$08$Diu6WZKKzbaMitJ9aGBYHOmcUqhZNgHSW5NI7cJh2Oes37uKoeuj6', 'hajjaj5845', NULL, 'abuhajjaj@yahoo.com', '', '', '', '', 'Iyad', 'Karen Abuhajjaj', '5845 Brushwood Court Chino Hills, CA 91709', '', NULL, NULL, NULL, NULL, 1535765967, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(126, '97.73.244.53', 'parent@parent.com', '$2y$08$gjLA.AxbxHsmKyNee7GvvOBmNNk5apNGJrkiMqpfkYcwX0eSK/7NG', '12345678', NULL, 'parent@parent.com', '', '', '', '', 'Sal', 'Doria Yuan', '3228 E Longridge Dr, Orange CA 92867', '', NULL, NULL, NULL, NULL, 1535766518, 1538475481, 1, NULL, '0000-00-00 00:00:00', '2018-09-19 10:54:07', 0, 'parent', '2000-01-01'),
(127, '97.73.244.53', 'sarahalmagsoosi@gmail.com', '$2y$08$X2v4iMQAWQy.afuOsvTfduxq2yuqg.hQD76Pc7IE7DV0PjLCZLp.C', 'azez2740', NULL, 'sarahalmagsoosi@gmail.com', '', '', '', '', 'Firas', 'Sarah Almagsoosi', '2740 W Ball Rd. Apy #L4 Anaheim, CA 92804', '', NULL, NULL, NULL, NULL, 1535766981, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(128, '97.73.244.53', 'info@abushararlaw.com', '$2y$08$AHYvItCy8oX4lBPziujLBOw3bC53Y9xqFj4p.1tJtBpd/JkuR2piW', 'abusharar747', NULL, 'info@abushararlaw.com', '', '', '', '', 'Akram', 'Lubna Albajjar', '747 S Crown Pointe drive. Anaheim, CA 92807', '', NULL, NULL, NULL, NULL, 1535768114, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(129, '97.73.244.53', 'ghadafaysal1973@hotmail.com', '$2y$08$llYr92ULlSaV02AL8LQH5u3yKsVTC68y5ECMZt6yh4iuh.QTKLQcO', 'osman11251', NULL, 'ghadafaysal1973@hotmail.com', '', '', '', '', 'Mustafa', 'Ghada Mahmoud', '11251 Pioneer blvd., apt # H6, Norwalk, CA, 9', '', NULL, NULL, NULL, NULL, 1535769315, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(130, '97.73.244.53', 'newhendali@gmail.com', '$2y$08$H/8csv/zxcO.ELLYkzlUvu.tJADPRgViFDu1yrWzUq5pC8zsCQVTm', 'khalafalla11645', NULL, 'newhendali@gmail.com', '', '', '', '', 'Ahmed', 'Hend Ali', '11645 Old River School Road, Downey 90242', '', NULL, NULL, NULL, NULL, 1535769450, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(131, '97.73.244.53', 'talab89@yahoo.com', '$2y$08$5SARpsfyin9WLyxx1zaXke54uCfHto9JsBTtmJMpiBLdDNAylXKdq', 'ibrahim7140', NULL, 'talab89@yahoo.com', '', '', '', '', 'Talab', 'Olvat Tabel', '714 W Lamark Dr Anaheim, CA 92802', '', NULL, NULL, NULL, NULL, 1535770375, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(132, '97.73.244.53', 'tamim@twinmoons.com', '$2y$08$Qyq7vzjFZ3wRZwLeNHiG/OYhA/flZBmJTO4GG0vLURMmprQtyOhiu', 'azizadah170', NULL, 'tamim@twinmoons.com', '', '', '', '', 'Tamim', 'Husna Azizadah', '17006 New Pine Dr, Hacienda Heights, 91745', '', NULL, NULL, NULL, NULL, 1535770708, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(133, '97.73.244.53', 'ammari976@att.net', '$2y$08$u9Vn0KPr/68PY7iLPHnX4Oyo/WrDzT5YiRnKZ.fB6UZp5mkc6AEVC', 'issa2834', NULL, 'ammari976@att.net', '', '', '', '', 'Ammar', 'Manal', '2834 E. Frontera St., Unit D Anaheim, CA 9280', '', NULL, NULL, NULL, NULL, 1535770985, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(134, '97.73.244.53', 'susu.sabeh@gmail.com', '$2y$08$WbnhPGGFi5fEOLEE7ACLJOVBNt6eVjOqHeJnSDBfYtT2Ui5d0nmBW', 'sabeh1054', NULL, 'susu.sabeh@gmail.com', '', '', '', '', 'Gino', 'Sawsan Sabeh', '1054 S Mountvale Ct Anaheim, CA 92808', '', NULL, NULL, NULL, NULL, 1535773019, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(135, '97.73.244.53', 'mayaya7@aol.com', '$2y$08$OaqCg2NYtCnOHX9KvzCioef451jf72Wvaa3w99go0sTRPSeDKhUDq', 'hassan777', NULL, 'mayaya7@aol.com', '', '', '', '', 'Morad', 'May Hassan', '8582 Emerywood Dr Beuna Park, CA 90621', '', NULL, NULL, NULL, NULL, 1535774803, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(136, '97.73.244.53', 'amonaabood@yahoo.com', '$2y$08$HswhvpDaWnMpM/dgHbWe9O0679ch2ldIWtZLSivFo3R9rv7.UiFta', 'arar4390', NULL, 'amonaabood@yahoo.com', '', '', '', '', 'Abdallah', 'Amani Arar', '439 S. Westridge Cir Anaheim, CA 92807', '', NULL, NULL, NULL, NULL, 1535775390, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(140, '12.162.220.106', 'hebaabudebei09@hotmai.com', '$2y$08$uZGF8DeL38qO8fY5KIntfugFs/EdgUIWFsJYWNQF9atw/rbvWlNa.', 'abujableh2900', NULL, 'hebaabudebei09@hotmai.com', '', '', '', '', 'Baker', 'Heba', '2900 E Lincoln Ave Anaheim, CA 92806	', '', NULL, NULL, NULL, NULL, 1535825645, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(141, '12.162.220.106', 'tmasri@gmail.com', '$2y$08$4oPXsDyBzhGjg0FFwBBoReFVZLaUaJKp4nLfy98I8BXImxtYvFyZG', 'masri140', NULL, 'tmasri@gmail.com', '', '', '', '', 'Toufic', 'Rania Masri', '140 N. Hidden Cyn, Orange, CA 92869', '', NULL, NULL, NULL, NULL, 1535827312, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(142, '12.162.220.106', 'noura.tarek.saad@gmail.com', '$2y$08$73bk0ckjTHjgKX5zQnlrxO5WaNta6sDzGxHi2lQz9g3ZKL.FnwQ8q', 'noufal1540', NULL, 'noura.tarek.saad@gmail.com', '', '', '', '', 'Moustafa', 'Nouran', '1540 W Ball Rd, Anaheim, CA, 92802, apt 20A', '', NULL, NULL, NULL, NULL, 1535827500, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(143, '12.162.220.106', 'jasminemcdonald762@yahoo.com', '$2y$08$KZ7iXIPNlNHr.k3/dhFTGe9nRWyYn2LfA75lYJfTkl6vysjzvYlG2', 'mcdonald13005', NULL, 'jasminemcdonald762@yahoo.com', '', '', '', '', 'James', 'Jasmine McDonald', '13005 Cambridge Ct. Chino CA 91710', '', NULL, NULL, NULL, NULL, 1535827712, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(144, '4.34.87.243', 'manarkreiker@gmail.com', '$2y$08$WmP4vG5Fr7ByaPLGrBoXxOmDudRcxz/DV.fHHI/MUj9GjioD4ryFq', 'manar8771', NULL, 'manarkreiker@gmail.com', NULL, '', 'Manar', 'Kreiker', '', '', '', '714-727-8771', NULL, NULL, NULL, NULL, 1535846092, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'teacher', '2002-03-30'),
(145, '4.34.87.243', 'lanmachta@hotmail.com', '$2y$08$19xDGaahjk50YyrtdNl23OBe2iUKSVd5CT.YtM7YZKSVMN13moU0C', 'lana7605', NULL, 'lanmachta@hotmail.com', NULL, '', 'Lana', 'Machta', '', '', '', '714-487-7605', NULL, NULL, NULL, NULL, 1535846169, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'teacher', '2001-12-26'),
(146, '4.34.87.243', 'ykamalan@gmail.com', '$2y$08$P9igJtXKncu0L5o9skhEn.aahJzShXwdH/ATr8rOOkzX4GReRbFzm', 'mohamed9722', NULL, 'ykamalan@gmail.com', '', '', '', '', 'Islam Abdelaziz', 'Youssra Nasr', '9722 Paseo De Oro Cypress, CA 90630', '', NULL, NULL, NULL, NULL, 1535849812, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(147, '4.34.87.243', 'emailrazan@gmail.com', '$2y$08$qUze0OtwxnJhGxZAKn0.KOmSflS/nduWNqtb21GFIZgvOEh8p0JJW', 'farshoukh2111', NULL, 'emailrazan@gmail.com', '', '', '', '', 'Nabil', 'Razanne', '2111 Warfield Ave #B Redondo Beach CA 90278', '', NULL, NULL, NULL, NULL, 1535850123, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(148, '4.34.87.243', 'nayraahmed42@gmail.com', '$2y$08$x9iiRIT3KEt/0j2xXv.2a.FwjnKfaZFBKh75.H.25wxrPL/z2EXai', 'elgendy315', NULL, 'nayraahmed42@gmail.com', '', '', '', '', 'Mohamed', 'Naira Elawady', '315 N Associated Re Apt 507 Brea, CA 92821', '', NULL, NULL, NULL, NULL, 1535850576, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(149, '4.34.87.243', 'rorotani2014@hotmail.com', '$2y$08$LNHZ7vcSjQQSUDUNdQ2lM.2/srC4OBaD109erG.gUlX6J6so4he32', 'itani5573', NULL, 'rorotani2014@hotmail.com', '', '', '', '', 'Mohammad', 'Rania Itani', '5573 Saint Ann Ave Cypress, CA 90630', '', NULL, NULL, NULL, NULL, 1535850716, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(150, '4.34.87.243', 'h.doukmak@yahoo.com', '$2y$08$jhXLBno/QibtD9j.Y7PUEeCWzu.eSMS58svJZGoBQgFsrGzQypiBO', 'almasri425', NULL, 'h.doukmak@yahoo.com', '', '', '', '', 'Kinan', 'Hazar Almasri', '425 E Arrow Hwy Glendora, CA 91740', '', NULL, NULL, NULL, NULL, 1535850992, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(151, '4.34.87.243', 'duaa.kassar@gmail.com', '$2y$08$NrQMw.6Me5.u83JCAnUQF.7z2vl90GwhuyiOpR019tUutasz0zESC', 'aljabi20615', NULL, 'duaa.kassar@gmail.com', '', '', '', '', 'Reda', 'Duoa Alkassar', '20615 Fuero Dr Walnut, CA 91789', '', NULL, NULL, NULL, NULL, 1535851180, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(152, '4.34.87.243', 'rkabbara@msn.com', '$2y$08$0OOxVlfL9o0az732Evr2DuWv93HeZYFh5FM0jM/vxl8Z4QP1NRSTa', 'kabbara3659', NULL, 'rkabbara@msn.com', '', '', '', '', 'Mazen', 'Rana', '3659 Garretson Avenue Corona, CA 92881', '', NULL, NULL, NULL, NULL, 1535851448, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(153, '4.34.87.243', 'mnrabah@hotmail.com', '$2y$08$lHD5rs3pJ3PMKJTPZoDTQum6ejZ/72jnHBFxO245l/ORJKY9KAV1i', 'alramahi5310', NULL, 'mnrabah@hotmail.com', '', '', '', '', 'Mohd', 'Tharwa Ahmad', '5310 Fairview Avenue Beuna Park, CA 90621', '', NULL, NULL, NULL, NULL, 1535851683, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(154, '4.34.87.243', 'shirinelwir@gmail.com', '$2y$08$ZDF6g0ReHe9L2LR1sau0P.XVqHEGThJnLnE94oya5pxUOhWIYoKge', 'zaid2132', NULL, 'shirinelwir@gmail.com', '', '', '', '', 'Eid', 'Shirin Elwir', '2132 Derek Dr Apt A Fullerton, CA 92831', '', NULL, NULL, NULL, NULL, 1535851889, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(155, '4.34.87.243', 'rasha_jubran@hotmail.com', '$2y$08$lWRpSr479DGeI2je1tGuveizZdfXWfcoxL/5JbrLnpDf0Ek116zTy', '2254jubran', NULL, 'rasha_jubran@hotmail.com', '', '', '', '', 'Jubran', 'Rasha Jubran', '2254 E. Vermont Ave Anaheim, CA 92806	', '', NULL, NULL, NULL, NULL, 1535852065, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(156, '4.34.87.243', 'bassamghazal@gmail.com', '$2y$08$kX9MHR/Dnj.fvSbT1JKaiOnbgnVWzFlMf9Fm5QRQdXOznTEsX39qC', '6600ghazal', NULL, 'bassamghazal@gmail.com', '', '', '', '', 'Bassam', 'Tamara', '6600 Warner Ave UNIT 195 Huntington Beach, CA', '', NULL, NULL, NULL, NULL, 1535852377, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(157, '4.34.87.243', 'm.bakr@yahoo.com', '$2y$08$91OScOkVUZDEQ52oxmQ0fO9cEXKvoh9GGbIKDJX.oDHBUONHORkHa', 'saleh124', NULL, 'm.bakr@yahoo.com', '', '', '', '', 'Mohamed', 'Yasmin Noureldin', '124 Stratford Cir., Placentia, CA 92870	', '', NULL, NULL, NULL, NULL, 1535852512, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(158, '4.34.87.243', 'abirzantout@hotmail.com', '$2y$08$cMWsJ1GryTwi9p63qaGMLewEIUEodef5WH6dSCA.jRF2xagdwU3LK', 'reinadiane53', NULL, 'abirzantout@hotmail.com', '', '', '', '', 'Mazen', 'Abir Almaoui', '53 Diamond, Irvine CA 92620', '', NULL, NULL, NULL, NULL, 1535852677, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(159, '4.34.87.243', 'fatom1979@yahoo.com', '$2y$08$F3wfxUFV4QLzekrYGBbIW.t9uGx/HPS7rOs.NnD4Nb/tuCjw9hnoa', 'albawab039', NULL, 'fatom1979@yahoo.com', '', '', '', '', 'Amer', 'Fatima', '930 S McCloud St Anaheim, CA 92805', '', NULL, NULL, NULL, NULL, 1535852866, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(160, '4.34.87.243', 'lailasobhy@gmail.com', '$2y$08$ouNJ5kOjuih31rkCb7pviu2xMQswa25psB.yR5hsbuqXhpN.CNm0G', 'abdelrazek6141', NULL, 'lailasobhy@gmail.com', '', '', '', '', 'Ahmed', 'Laila Sobhy', '1416 Sunnycrest Drive Fullerton, CA 92835', '', NULL, NULL, NULL, NULL, 1535853025, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-10 07:39:22', 0, 'parent', '2000-01-01'),
(161, '4.34.87.243', 'khuloudhassan80@gmail.com', '$2y$08$7u2BPajrnHxAlNor0jruNuis9PXLdGpbQBV96OCekF87RuscE9bo2', 'alhenn2769', NULL, 'khuloudhassan80@gmail.com', 'khulodudhassan80@gmail.com', '', '', '', 'Omar', 'Khuloud hassan', '9672 Colony Streeet Anaheim, CA 92804	', '', NULL, NULL, NULL, NULL, 1535853396, NULL, 1, NULL, '0000-00-00 00:00:00', '2018-09-10 07:39:08', 0, 'parent', '2000-01-01'),
(162, '::1', 'sanfora200@hmotail.com', '$2y$08$W/1mrXs97fAVaQniJxmwUeV4l4VHEYpHqmIWmGRQSPIDN2AhkrRAu', '', NULL, 'sanfora200@hmotail.com', NULL, '', '', '', 'des', '65', '6565', '', NULL, NULL, NULL, NULL, 1539000987, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(163, '::1', 'email@email.com', '$2y$08$ZWlKtfa2Ig28Hsx28g9LJefHbETTbDRTNmlQU78BfMK0FiQe4oV1W', 'S9jmYjCZ', NULL, 'email@email.com', NULL, '', '', '', 'هخخ', 'هخ', 'هخ', '', NULL, NULL, NULL, NULL, 1539370700, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(164, '::1', 'io@h.com', '$2y$08$PShzqfv/YfH6guWHdiip5.7sjuuva.ZPHaHOf3EAPNhCA5hmQhMdW', 'NNltM1SD', NULL, 'io@h.com', NULL, '', '', '', 'io', 'i', 'io', '', NULL, NULL, NULL, NULL, 1539381969, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01'),
(165, '::1', 't@l.com', '$2y$08$e8yDH8O9nTJC7bjD4HEDzOFHQ5t/huOyiuF6lJaGEAcFCMBw069Ce', 'w8M39kwP', NULL, 't@l.com', NULL, '', '', '', '89', '989', '89', '', NULL, NULL, NULL, NULL, 1539382846, NULL, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'parent', '2000-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `users_authentication`
--

CREATE TABLE `users_authentication` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `login_email` varchar(255) DEFAULT NULL,
  `expired_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_authentication`
--

INSERT INTO `users_authentication` (`id`, `users_id`, `token`, `login_email`, `expired_at`, `created_at`, `updated_at`) VALUES
(0, 51, '50vHLIGnj6agI', NULL, '2018-05-27 22:58:16', '2018-05-27 13:58:16', '2018-05-27 13:58:16'),
(0, 76, '551UYCLUv9G5U', NULL, '2018-08-27 10:12:29', '2018-08-27 01:12:29', '2018-08-27 01:12:29'),
(0, 76, '161Saxg0uCYS6', '22noor22noor@gmail.com', '2018-09-01 17:47:30', '2018-09-01 08:47:30', '2018-09-01 08:47:30'),
(0, 137, '10A2v3hfPWXWI', 'daniawahhab93@gmail.com', '2018-09-01 20:22:58', '2018-09-01 11:22:58', '2018-09-01 11:22:58'),
(0, 138, '205T20bUwWS3c', 'daniawahhab93@gmail.com', '2018-09-01 21:39:24', '2018-09-01 12:39:24', '2018-09-01 12:39:24'),
(0, 75, '201ok0.cNGrFc', 'nada84us@yahoo.com', '2018-09-02 18:03:22', '2018-09-02 09:03:22', '2018-09-02 09:03:22'),
(0, 75, '21.Ri97ZzZmR2', 'nada84us@yahoo.com', '2018-09-02 18:04:12', '2018-09-02 09:04:12', '2018-09-02 09:04:12'),
(0, 67, '16G5qWMq1WiyA', 'estwani94@gmail.com', '2018-09-21 00:21:49', '2018-09-20 15:21:49', '2018-09-20 15:21:49'),
(0, 1, '81CFX4yhqbvSc', 'admin@admin.com', '2018-10-01 19:30:15', '2018-10-01 10:30:15', '2018-10-01 10:30:15'),
(0, 1, '60jVIQI2wD5T.', 'admin@admin.com', '2018-10-02 17:29:19', '2018-10-02 08:29:19', '2018-10-02 08:29:19'),
(0, 66, '13vlbCXm7.2mw', 'teacher@teacher.com', '2018-10-02 22:58:26', '2018-10-02 13:58:26', '2018-10-02 13:58:26'),
(0, 1, '16.VfBanMEoWE', 'admin@admin.com', '2018-10-03 20:34:42', '2018-10-03 11:34:42', '2018-10-03 11:34:42'),
(0, 1, '19rjO5N9XDF8k', 'admin@admin.com', '2018-10-04 19:36:08', '2018-10-04 10:36:08', '2018-10-04 10:36:08'),
(0, 1, '19XFF37JosZt6', 'admin@admin.com', '2018-10-06 17:59:27', '2018-10-06 08:59:27', '2018-10-06 08:59:27'),
(0, 1, '946uxVGAhpe4I', 'admin@admin.com', '2018-10-07 18:50:10', '2018-10-07 09:50:10', '2018-10-07 09:50:10'),
(0, 1, '30gkzD3cyW5tw', 'admin@admin.com', '2018-10-08 17:29:55', '2018-10-08 08:29:55', '2018-10-08 08:29:55'),
(0, 1, '73muiavbVsHNA', 'admin@admin.com', '2018-10-08 23:36:49', '2018-10-08 14:36:49', '2018-10-08 14:36:49'),
(0, 1, '148VLtT02RM9E', 'admin@admin.com', '2018-10-09 20:15:07', '2018-10-09 11:15:07', '2018-10-09 11:15:07'),
(0, 1, '219rjqpJQIXBA', 'admin@admin.com', '2018-10-10 08:23:13', '2018-10-09 21:23:13', '2018-10-09 21:23:13'),
(0, 1, '10FWseXzT2EAU', 'admin@admin.com', '2018-10-12 23:13:24', '2018-10-12 12:13:24', '2018-10-12 12:13:24'),
(0, 1, '186FOYYFWgXh2', 'admin@admin.com', '2018-10-13 01:28:28', '2018-10-12 14:28:28', '2018-10-12 14:28:28'),
(0, 1, '6003uDr1syrdA', 'admin@admin.com', '2018-10-13 04:33:12', '2018-10-12 17:33:12', '2018-10-12 17:33:12'),
(0, 1, '29ekfyEnbcFxY', 'admin@admin.com', '2018-10-13 07:06:51', '2018-10-12 20:06:51', '2018-10-12 20:06:51');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(37, 36, 7),
(38, 37, 7),
(39, 38, 7),
(40, 39, 7),
(42, 41, 7),
(43, 42, 7),
(44, 43, 7),
(45, 44, 7),
(52, 51, 7),
(64, 63, 7),
(65, 64, 7),
(66, 65, 7),
(67, 66, 7),
(68, 67, 7),
(69, 68, 7),
(70, 69, 7),
(71, 70, 7),
(72, 71, 7),
(73, 72, 7),
(74, 73, 7),
(75, 74, 7),
(76, 75, 7),
(77, 76, 7),
(78, 77, 7),
(79, 78, 7),
(80, 79, 7),
(81, 80, 7),
(82, 81, 7),
(83, 82, 7),
(84, 83, 7),
(85, 84, 7),
(86, 85, 7),
(87, 86, 7),
(88, 87, 7),
(89, 88, 7),
(90, 89, 7),
(91, 90, 7),
(96, 95, 7),
(97, 96, 7),
(98, 97, 7),
(99, 98, 7),
(100, 99, 7),
(101, 100, 7),
(102, 101, 7),
(104, 103, 7),
(106, 105, 7),
(107, 106, 7),
(108, 107, 7),
(109, 108, 7),
(110, 109, 7),
(111, 110, 7),
(112, 111, 7),
(113, 112, 7),
(114, 113, 7),
(115, 114, 7),
(116, 115, 7),
(117, 116, 7),
(118, 117, 7),
(119, 118, 7),
(120, 119, 7),
(121, 120, 7),
(122, 121, 7),
(123, 122, 7),
(124, 123, 7),
(125, 124, 7),
(126, 125, 7),
(127, 126, 7),
(128, 127, 7),
(129, 128, 7),
(130, 129, 7),
(131, 130, 7),
(132, 131, 7),
(133, 132, 7),
(134, 133, 7),
(135, 134, 7),
(136, 135, 7),
(137, 136, 7),
(141, 140, 7),
(142, 141, 7),
(143, 142, 7),
(144, 143, 7),
(145, 144, 7),
(146, 145, 7),
(147, 146, 7),
(148, 147, 7),
(149, 148, 7),
(150, 149, 7),
(151, 150, 7),
(152, 151, 7),
(153, 152, 7),
(154, 153, 7),
(155, 154, 7),
(156, 155, 7),
(157, 156, 7),
(158, 157, 7),
(159, 158, 7),
(160, 159, 7),
(161, 160, 7),
(162, 161, 7),
(163, 162, 7),
(164, 163, 7),
(165, 164, 7),
(166, 165, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enrollment_requests`
--
ALTER TABLE `enrollment_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `groups_previlages`
--
ALTER TABLE `groups_previlages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_group_previlage_previlage1_idx` (`previlage_id`),
  ADD KEY `fk_group_previlage_group1_idx` (`group_id`);

--
-- Indexes for table `halls`
--
ALTER TABLE `halls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `honored_orders`
--
ALTER TABLE `honored_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `honored_students`
--
ALTER TABLE `honored_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthes`
--
ALTER TABLE `monthes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_parent_user1_idx` (`user_id`);

--
-- Indexes for table `parent_subjects`
--
ALTER TABLE `parent_subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `payment_items`
--
ALTER TABLE `payment_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petty_cashes`
--
ALTER TABLE `petty_cashes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pizza_and_snack_inputs`
--
ALTER TABLE `pizza_and_snack_inputs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pizza_and_snack_items`
--
ALTER TABLE `pizza_and_snack_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `previlages`
--
ALTER TABLE `previlages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `registration_form`
--
ALTER TABLE `registration_form`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `report_cards`
--
ALTER TABLE `report_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms_attendances`
--
ALTER TABLE `rooms_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`);

--
-- Indexes for table `rooms_teachers`
--
ALTER TABLE `rooms_teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_allergies`
--
ALTER TABLE `students_allergies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_attendances`
--
ALTER TABLE `students_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_absence_student_course1_idx` (`student_record_id`);

--
-- Indexes for table `students_marks`
--
ALTER TABLE `students_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_notes`
--
ALTER TABLE `students_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_payments`
--
ALTER TABLE `students_payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `students_records`
--
ALTER TABLE `students_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_payment_months`
--
ALTER TABLE `student_payment_months`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_employee_user1_idx` (`user_id`);

--
-- Indexes for table `teachers_notes`
--
ALTER TABLE `teachers_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_groups_user1_idx` (`user_id`),
  ADD KEY `fk_users_groups_group1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enrollment_requests`
--
ALTER TABLE `enrollment_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `groups_previlages`
--
ALTER TABLE `groups_previlages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halls`
--
ALTER TABLE `halls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `honored_orders`
--
ALTER TABLE `honored_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `honored_students`
--
ALTER TABLE `honored_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `monthes`
--
ALTER TABLE `monthes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `parent_subjects`
--
ALTER TABLE `parent_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_items`
--
ALTER TABLE `payment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `petty_cashes`
--
ALTER TABLE `petty_cashes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pizza_and_snack_inputs`
--
ALTER TABLE `pizza_and_snack_inputs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pizza_and_snack_items`
--
ALTER TABLE `pizza_and_snack_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `previlages`
--
ALTER TABLE `previlages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `registration_form`
--
ALTER TABLE `registration_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `report_cards`
--
ALTER TABLE `report_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `rooms_attendances`
--
ALTER TABLE `rooms_attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `rooms_teachers`
--
ALTER TABLE `rooms_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT for table `students_allergies`
--
ALTER TABLE `students_allergies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `students_attendances`
--
ALTER TABLE `students_attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `students_marks`
--
ALTER TABLE `students_marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `students_notes`
--
ALTER TABLE `students_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `students_payments`
--
ALTER TABLE `students_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `students_records`
--
ALTER TABLE `students_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;

--
-- AUTO_INCREMENT for table `student_payment_months`
--
ALTER TABLE `student_payment_months`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=357;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `teachers_notes`
--
ALTER TABLE `teachers_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rooms_attendances`
--
ALTER TABLE `rooms_attendances`
  ADD CONSTRAINT `rooms_rooms_attendance_FK` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rooms_teachers`
--
ALTER TABLE `rooms_teachers`
  ADD CONSTRAINT `rooms_teachers_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rooms_teachers_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
