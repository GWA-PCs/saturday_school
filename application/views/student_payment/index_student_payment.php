<!--<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item fix-lang">
                <a  href="<?php echo base_url('home/home_page') ?>" ><?php echo lang('home_page') ?></a>
            </li>
            <li class="breadcrumb-item fix-lang">
                <a  href="<?php echo base_url('parents/index_parent') ?>" > <?php echo lang('index_parent') ?></a>
            </li>
            <li class="breadcrumb-item fix-lang">
                <a  href="<?php echo base_url('parents/index_parent_children') . '/' . $parent_id ?>" > <?php echo lang('index_parent_children') ?></a>
            </li>
            <li class="breadcrumb-item active fix-lang"><?php echo $page_title ?></li>
        </ol>
    </div>
</div>-->


<?php
if (isset($student_name))
    bs3_card($page_title, $student_name);
else
    bs3_card($page_title);
?>
<?php if (isset($add_student_payment) && $add_student_payment) { ?>
    <?php if ($_current_year == $_archive_year) { ?>
        <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_student_payment") ?>
        <?php bs3_hidden('id') ?>

        <?php
        bs3_dropdown('grade_id', $students_grades_options, isset($students_payments->grade_id) ? $students_payments->grade_id : FALSE, FALSE, FALSE);
        ?>

        <?php
        $options = array('000' => lang('select_room_name'));
        $options_extra = array();
        foreach ($active_student_record as $item) {
            $options[$item->room_id] = $item->room_name;
            $options_extra[$item->grade_id]['key'] = 'grade_id1';
            $options_extra[$item->grade_id]['value'] = $item->grade_id;
        }
        ?>
        <div id="room_id">
            <?php bs3_dropdown('room_id', $options, isset($students_payments->grade_id) ? $students_payments->grade_id : FALSE, false, $options_extra); ?>
        </div>

        <div id="payment_item_type">
            <?php bs3_dropdown('payment_item_type', $payment_item_type) ?>
        </div>

        <div id="books_payment_type">
            <?php bs3_dropdown('books_payment_type', $books_payment_type) ?>
        </div>

        <div id="late_fee_type">
            <?php bs3_dropdown('late_fee_type', $late_fee_type) ?>
        </div>

        <div id="tuition_type">
            <?php bs3_dropdown('tuition_type', $tuition_type) ?>
        </div>

        <div id="teacher_child_discount">
            <?php bs3_number('teacher_child_discount') ?>
        </div>


        <?php $m = 0; ?>
        <div class="form-group row" id="months" >
            <?php foreach ($monthes_options as $key => $value) { ?>
                <div class="col-md-4">
                    <label class="control-label col-md-6"   ><?php echo lang($key); ?></label>
                    <div class="checkbox checkbox-success">
                        <input  name="<?php echo $key; ?>" id="checkbox<?php echo $key ?>" 
                                value="1"    
                                type="checkbox" >                 
                        <label for="checkbox<?php echo $key ?>"></label>
                    </div>
                </div>

            <?php } ?>
        </div>

        <div id="invoice">
            <?php bs3_number_with_dollar('invoice') ?>
        </div>

        <div id="payment">
            <?php bs3_number_with_dollar('payment') ?>
        </div>

        <div id="full_year_discount">
            <?php bs3_number('full_year_discount') ?>
        </div>

        <?php bs3_dropdown('check_or_cash', $check_or_cash_options) ?>
        <?php bs3_textarea('student_payment_description') ?>
        <?php bs3_date('student_payment_date') ?>
        <?php bs3_textarea('student_payment_notes') ?>

        <?php bs3_modal_footer('create'); ?>
    <?php } ?>
<?php } ?>
<br><br>
<br>
<div class="" style="margin-left: 4px;">
    <h5 style="color: red"> <b> Note: </b> If you want to see total balance of parent go to 
        <u><a style="color: red" href="<?php echo base_url("profile/page_parent_profile/") . $user_id ?>">parent profile</a></u>
    </h5>
</div>

<div class="card">
    <div class="card-body">
        <hr>
        <div class="row text-center">
            <div class="col-md-4 align-self-center">
                <h5 class="text-center" ><b><?php echo lang('invoice'); ?></b> </h5>
                <hr>
                <h5 class="text-center" id="sum_invoice"> </h5>
            </div>
            <div class="col-md-4 align-self-center">
                <h5 class="text-center" ><b><?php echo lang('payment'); ?></b> </h5>
                <hr>
                <h5 class="text-center" id="sum_payment"> </h5>
            </div>
            <div class="col-md-4 align-self-center">
                <h5 class="text-center" style="color: red"><b><?php echo lang('balance_student'); ?></b> </h5>
                <hr>
                <h5 class="text-center" style="color: red" id="balance_student"> </h5>
            </div>
        </div>
        <hr>
    </div>
</div>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>



<script>
    $(document).ready(function () {
    $.ajax({
    url: "<?php echo base_url("student_payment/total_student_payment_and_balance/") . $student_id ?>",
            type: "POST",
            dataType: "JSON",
            success: function (data) {
            $.each(data['data'], function (key, value) {
            $("#" + key).text(value);
            });
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
    });
    });</script>



<script>
    $(document).ready(function () {
    //**************************** Hide fields related in **********************
    $('#room_id').hide();
    $('#payment_item_type').hide();
    $('#books_payment_type').hide();
    $('#months').hide();
    $('#invoice').hide();
    $('#payment').hide();
    $('#late_fee_type').hide();
    $('#teacher_child_discount').hide();
    $('#tuition_type').hide();
    $('#full_year_discount').hide();
    //**************************************************************************
    //************************* if Payment item type change ********************
    //**************************************************************************
    $("#inputPayment_item_type").change(function () {
    if ($(this).val() === '000') {
    $('#late_fee_type').fadeOut();
    $('#books_payment_type').fadeOut();
    $('#months').fadeOut();
    $('#invoice').fadeOut();
    $('#payment').fadeOut();
    //=============================== if val = registration_fee ================
    } else if ($(this).val() === 'registration_fee') {
    var type = $(this).val();
    var grade_id = $("#inputGrade_id").val();
    var room_id = $("#inputRoom_id").val();
    $.ajax({
    url: "<?php echo base_url("student_payment/get_registration_fee_invoice/") ?>" + grade_id + "/" + room_id + "/" + type + "/" + <?php echo $student_id ?>,
            type: 'post',
            data: {depart: type},
            dataType: 'json',
            success: function (data) {
//            if (data.data.teacher_child_check === '2'){
//            $('#teacher_child_discount').fadeIn();
//            $("#inputTeacher_child_discount").val(data.data.teacher_child_discount);
//            }

            $("#inputInvoice").val(data.data.invoice);
            $("#inputInvoice").prop('readonly', true);
            $("#inputPayment").val(data.data.payment);
            }
    });
//    $("#inputInvoice").val('0');
    $('#invoice').fadeIn();
//    $("#inputPayment").val('0');
    $('#payment').fadeIn();
    $('#late_fee_type').fadeOut();
    $('#books_payment_type').fadeOut();
    $('#months').fadeOut();
    $('#tuition_type').fadeOut();
    $('#full_year_discount').fadeOut();
    //=============================== if val = books ===========================
    } else if ($(this).val() === 'books') {
    $("#inputInvoice").val('0');
    $("#inputPayment").val('0');
    $('#books_payment_type').fadeIn();
    $('#late_fee_type').fadeOut();
    $('#months').fadeOut();
    $('#tuition_type').fadeOut();
    $('#full_year_discount').fadeOut();
    //=============================== if val = late_fee ========================
    } else if ($(this).val() === 'late_fee') {
    $("#inputInvoice").val('0');
    $("#inputPayment").val('0');
    $('#late_fee_type').fadeIn();
    $('#books_payment_type').fadeOut();
    $('#invoice').fadeOut();
    $('#payment').fadeOut();
    $('#months').fadeOut();
    $('#tuition_type').fadeOut();
    $('#full_year_discount').fadeOut();
    //=============================== if val = custom_payment =========================
    } else if ($(this).val() === 'custom_payment'){
    $("#inputInvoice").prop('readonly', false);
    $("#inputInvoice").val('0');
    $("#inputPayment").val('0');
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    $('#months').fadeOut();
    $('#books_payment_type').fadeOut();
    $('#late_fee_type').fadeOut();
    $('#full_year_discount').fadeOut();
    } else{
//    $("#inputInvoice").val('0');
//    $("#inputPayment").val('0');
    $('#tuition_type').fadeIn();
    $('#months').fadeOut();
    $('#books_payment_type').fadeOut();
    $('#late_fee_type').fadeOut();
    $('#full_year_discount').fadeOut();
    }
    });
    //**************************************************************************
    //**************************** Tuition type is change **********************
    //**************************************************************************
    $("#inputTuition_type").change(function () {
    var type = $(this).val();
    var grade_id = $("#inputGrade_id").val();
    var room_id = $("#inputRoom_id").val();
    $.ajax({
    url: "<?php echo base_url("student_payment/get_tuition_invoice/") ?>" + grade_id + "/" + room_id + "/" + type + "/" + <?php echo $student_id ?>,
            type: 'post',
            data: {depart: type},
            dataType: 'json',
            success: function (data) {

            $("#inputInvoice").prop('readonly', true);
            if (data.data.teacher_child_check === '2') {

            $("#inputTeacher_child_discount").val(data.data.teacher_child_discount);
            $("#inputFull_year_discount").val('0');
            $('#full_year_discount').fadeOut();
            var invoice_with_discount_child = data.data.invoice - (data.data.invoice * data.data.teacher_child_discount / 100);
            $("#inputInvoice").val(invoice_with_discount_child);
            }
            else if (type === 'full_year' && data.data.teacher_child_check === '1') {
            $("#inputFull_year_discount").val(data.data.full_year_discount);
            var invoice_with_discount = data.data.invoice - data.data.full_year_discount;
            $("#inputInvoice").val(invoice_with_discount);
            } else{
            $("#inputInvoice").val(data.data.invoice);
            }
            $("#inputPayment").val(data.data.payment);
            }
    });
    $("#inputFull_year_discount").change(function () {
    var invoice_with_discount = $("#inputInvoice").val() - $("#inputFull_year_discount").val();
    $("#inputInvoice").val(invoice_with_discount);
    });
    $("#inputTeacher_child_discount").change(function () {
    var invoice_with_discount_child = $("#inputInvoice").val() - ($("#inputInvoice").val() * $("#inputTeacher_child_discount").val() / 100);
    $("#inputInvoice").val(invoice_with_discount_child);
    });
    if ($(this).val() === '000') {
    $("#inputInvoice").val('0');
    $('#invoice').fadeOut();
    $("#inputPayment").val('0');
    $('#payment').fadeOut();
    } else if ($(this).val() === 'full_year') {
    $('#months').fadeOut();
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
//    $('#full_year_discount').fadeIn();
    } else if ($(this).val() === 'bimonthly') {
    $('#months').fadeIn();
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    $('#full_year_discount').fadeOut();
    } else if ($(this).val() === 'monthly') {
    $('#months').fadeIn();
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    $('#full_year_discount').fadeOut();
    }
    });
    //**************************************************************************
    //************************* Books payment type is change *******************
    //**************************************************************************
    $("#inputBooks_payment_type").change(function () {
    var type = $(this).val();
    var grade_id = $("#inputGrade_id").val();
    $.ajax({
    url: "<?php echo base_url("student_payment/get_book_invoice/") ?>" + grade_id + "/" + type,
            type: 'post',
            data: {depart: type},
            dataType: 'json',
            success: function (data) {
            $("#inputInvoice").prop('readonly', true);
            $("#inputInvoice").val(data.data.invoice);
            $("#inputPayment").val(data.data.payment);
            }
    });
    if ($(this).val() === '000') {
    $("#inputInvoice").val('0');
    $('#invoice').fadeOut();
    $('#payment').fadeOut();
    } else if ($(this).val() === 'pre_kinder_books_fee') {
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    } else if ($(this).val() === 'elementary_books_fee') {
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    } else if ($(this).val() === 'islamic_textbook') {
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    } else if ($(this).val() === 'islamic_workbook') {
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    }
    });
    //**************************************************************************
    //************************* Late fee type is change ************************
    //**************************************************************************
    $("#inputLate_fee_type").change(function () {
    var type = $(this).val();
    var grade_id = $("#inputGrade_id").val();
    $.ajax({
    url: "<?php echo base_url("student_payment/get_late_payment_fee_invoice/") ?>" + grade_id + "/" + type,
            type: 'post',
            data: {depart: type},
            dataType: 'json',
            success: function (data) {
            $("#inputInvoice").prop('readonly', true);
            $("#inputInvoice").val(data.data.invoice);
            $("#inputPayment").val(data.data.payment);
            }
    });
    if ($(this).val() === '000') {
    $("#inputInvoice").val('0');
    $('#invoice').fadeOut();
    $('#payment').fadeOut();
    } else if ($(this).val() === 'late_payment_fee') {
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    } else if ($(this).val() === 'late_pickup_fee') {
    $('#invoice').fadeIn();
    $('#payment').fadeIn();
    }
    });
    //**************************************************************************
    //************************* if Grade is change *****************************
    //**************************************************************************
    $("#inputGrade_id").change(function () {
    if ($(this).val() === '000') {
    $('#room_id').fadeOut();
    } else {
    $('#room_id').fadeIn();
    $('#payment_item_type').fadeIn();
    }
    });
    });</script>

<script>
    $(document).ready(function () {

    // ==================== for filter the rooms which student in it ===========
    $("#inputGrade_id").change(function () {
    var deptid = $(this).val();
    $.ajax({
    url: "<?php echo base_url("student_payment/get_rooms_for_filter/") . $student_id ?>",
            type: 'post',
            data: {depart: deptid},
            dataType: 'json',
            success: function (response) {
            var len = response.length;
            $("#inputRoom_id").empty();
            for (var i = 0; i < len; i++) {
            var grade_id = response[i]['grade_id'];
            var id = response[i]['room_id'];
            var name = response[i]['room_name'];
            if (grade_id === deptid)
                    $("#inputRoom_id").append("<option value='" + id + "' grade_id1='" + id + "'>" + name + "</option>");
            }
            }
    });
    });
    });</script>




<script>

    var save_method; //for save method string
    var table;
    var base_url = '<?php echo base_url(); ?>';
    function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
    }
<?php if (isset($specific_show_object_link)) { ?>
        $(document).ready(function () {

        //datatables
        table = $('.object_table').DataTable({


        dom: 'Bfrtip',
                buttons: [
                {
                text: '<i class="mdi mdi-file-excel "> </i> Excel',
                        extend: 'excel',
                        action: newExportAction,
                        exportOptions: {
                        columns: '.printable'
                        }
                },
                {
                text:'<i class="mdi mdi-printer "> </i> Print',
                        extend: 'print',
                        exportOptions: {
                        columns: '.printable'
                        }
                },
                ],
    <?php if ($lang == "ar") { ?>
            "language": {
            "sProcessing": "جاري التحميل...",
                    "sLengthMenu": "إظهار _MENU_ عناصر",
                    "sZeroRecords": "لم يُعثر على أية سجلات",
                    "sInfo": "العناصر الظاهرة من _START_ إلى _END_ من أصل _TOTAL_ عنصر",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجلّ",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث:",
                    "sUrl": "",
                    "oPaginate": {
                    "sFirst": "الأول",
                            "sPrevious": "السابق",
                            "sNext": "التالي",
                            "sLast": "الأخير"
                    }
            },
    <?php } ?>
        "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering": false,
                // Load data for the table's content from an Ajax source
                "ajax": {
                "url": "<?php echo base_url($specific_show_object_link) ?>",
                        "type": "POST"
                },
                //Set column definition initialisation properties.
                "displayLength": 25,
                "columnDefs": [
                {
                "targets": [ - 1], //last column
                        "orderable": false, //set not orderable
                },
                {
                "targets": [ - 2], //2 last column (photo)
                        "orderable": false, //set not orderable
                },
                ],
                'createdRow': function (row, data, dataIndex) {
                $(row).attr('id', 'someID');
                //                console.log(row);
                if (data['2'] === 'No' || data['2'] === 'no')
                        $(row).attr('style', 'background:#ff00001c;');
                },
        });
        });
<?php } ?>
<?php if (isset($modal_name)) { ?>
        function add_object(modal_id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
        var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
        if (hidden_in == "add") {
        $('[name=' + pair[0] + ']').parent().parent().hide();
        } else {
        $('[name=' + pair[0] + ']').parent().parent().show();
        }
        }
        save_method = 'add';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $(show_modal_id).modal('show'); // show bootstrap modal
        var add_object_title = "<?php echo $add_object_title; ?>";
        $('.modal-title').text(add_object_title); // Set Title to Bootstrap modal title

        $('#photo-preview').hide(); // hide photo preview modal

        $('#label-photo').text('Upload Photo'); // label photo upload
        }
<?php } ?>
<?php if (isset($specific_get_object_link)) { ?>
        function edit_object(id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        save_method = 'update';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
        var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
        if (hidden_in == "update") {
        $('[name=' + pair[0] + ']').parent().parent().hide();
        } else {
        $('[name=' + pair[0] + ']').parent().parent().show();
        }
        }



        $(show_modal_id).modal('show'); // show bootstrap modal
        //Ajax Load data from ajax
        $.ajax({
        url: "<?php echo base_url($specific_get_object_link) ?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                for (var pair of formData.entries()) {
                $('[name=' + pair[0] + ']').val(data[pair[0]]);
                }
                var update_object_title = "<?php echo $update_object_title; ?>";
                $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error get data from ajax');
                }
        });
        }
<?php } ?>
    function specific_get_response(data) {

    var show_modal_id = "<?php echo "#$modal_name" ?>";
    if (data.status == "200") {
    // message success data.message
    swal({
    title: "<?php echo lang('success') ?>",
            text: data.message,
            type: "success",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    $(show_modal_id).modal('hide');
    reload_table();
    var total_sum_url = "<?php echo base_url("student_payment/total_student_payment_and_balance/") . $student_id ?>";
//    console.log(total_sum_url);
    $.ajax({
    url: total_sum_url,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
//            console.log(data['data']);
            $.each(data['data'], function (key, value) {
            $("#" + key).html(value);
            });
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
    });
    //$("#remain_cost_after_discount").html(data['data']);
    } else if (data.status == "201") {
    // valedation error data.data
    messages_error("<?php echo lang('error'); ?>", data.data);
    } else if (data.status == "202") {

    swal({
    title: "<?php echo lang('error_existing') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    } else if (data.status == "203") {
    // permission error 
    var redirct_url = "<?php echo base_url() ?>/home/home_page";
    window.location.replace(redirct_url);
    } else if (data.status == "204") {
    //permission success
    } else if (data.status == "205") {
    // Date error 
    swal({
    title: "<?php echo lang('date_error') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    } else if (data.status == "206") {
    // Date error 
    swal({
    title: "<?php echo lang('Time_error') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    } else if (data.status == "400") {
    swal({
    title: "<?php echo lang('error') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    $(show_modal_id).modal('hide');
    // message error in controller data.message
    } else if (data.status == "401") {
    // message error in information data.message and status
    }
    }
<?php if (isset($specific_add_object_link) || isset($specific_update_object_link)) { ?>
        function save() {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable
        var url;
        if (save_method == 'add') {
        url = "<?php echo site_url($specific_add_object_link) ?>";
        } else {
        url = "<?php echo site_url($specific_update_object_link) ?>";
        }
        specific_add_update(url);
        }
<?php } ?>

<?php if (isset($specific_delete_object_link)) { ?>

        function delete_object(id) {
        swal({
        title: "<?php echo lang('delete_confirmation_text') ?>",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "<?php echo lang('close') ?>",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('delete') ?>",
                closeOnConfirm: false
        }, function () {
        $.ajax({
        url: "<?php echo site_url($specific_delete_object_link) ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                //if success reload ajax table
                // $(show_modal_id).modal('hide');
                swal("<?php echo lang('delete_success') ?>", "", "success");
                reload_table();
                var total_sum_url = "<?php echo base_url("student_payment/total_student_payment_and_balance/") . $student_id ?>";
                $.ajax({
                url: total_sum_url,
                        type: "POST",
                        dataType: "JSON",
                        success: function (data) {
                        //            console.log(data['data']);
                        $.each(data['data'], function (key, value) {
                        $("#" + key).html(value);
                        });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                });
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error deleting data');
                }
        });
        });
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        }

<?php } ?>
    function specific_add_update(url) {
    // ajax adding data to database
    var formData = new FormData($('#form_crud')[0]);
    $.ajax({
    url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
            if (data.status) //if success close modal and reload ajax table
            {
            specific_get_response(data);
            }
            else {
            for (var i = 0; i < data.inputerror.length; i++) {
            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
            }
            }
            var save = '<?php echo lang('save') ?>';
            $('.btnSave').text(save); //change button text
            $('.btnSave').attr('disabled', false); //set button enable
            },
            error: function (jqXHR, textStatus, errorThrown) {
//            alert('Error adding / update data');
            var save = '<?php echo lang('save') ?>';
            $('.btnSave').text(save); //change button text
            $('.btnSave').attr('disabled', false); //set button enable
            }
    });
    }
//      $("#inputInvoice").prop('disabled', false);
</script>

