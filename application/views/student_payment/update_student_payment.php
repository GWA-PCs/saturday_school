<?php bs3_card($page_title); ?>

<?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php bs3_hidden('id') ?>

<?php
bs3_dropdown('grade_id', $students_grades_options, isset($students_payments->grade_id) ? $students_payments->grade_id : FALSE, 'disabled', FALSE);
?>

<?php
$options = array('000' => lang('select_room_name'));
$options_extra = array();
foreach ($active_student_record as $item) {
    $options[$item->room_id] = $item->room_name;
    $options_extra[$item->grade_id]['key'] = 'grade_id1';
    $options_extra[$item->grade_id]['value'] = $item->grade_id;
}
?>
<div id="room_id">
    <?php bs3_dropdown('room_id', $options, isset($students_payments->room_id) ? $students_payments->room_id : FALSE, 'disabled', $options_extra); ?>
</div>

<div id="payment_item_type">
    <?php bs3_dropdown('payment_item_type', $payment_item_type, isset($students_payments->payment_item_type) ? $students_payments->payment_item_type : FALSE, 'disabled') ?>
</div>

<?php if (isset($students_payments->books_payment_type) && $students_payments->books_payment_type != '000') { ?>
    <div id="books_payment_type">
        <?php bs3_dropdown('books_payment_type', $books_payment_type, isset($students_payments->books_payment_type) ? $students_payments->books_payment_type : FALSE, 'disabled') ?>
    </div>
<?php } ?>

<?php if (isset($students_payments->late_fee_type) && $students_payments->late_fee_type != '000') { ?>
    <div id="late_fee_type">
        <?php bs3_dropdown('late_fee_type', $late_fee_type, isset($students_payments->late_fee_type) ? $students_payments->late_fee_type : FALSE, 'disabled') ?>
    </div>
<?php } ?>

<?php if (isset($students_payments->tuition_type) && $students_payments->tuition_type != '000') { ?>
    <div id="tuition_type">
        <?php bs3_dropdown('tuition_type', $tuition_type, isset($students_payments->tuition_type) ? $students_payments->tuition_type : FALSE, 'disabled') ?>
    </div>
<?php } ?>
<?php if (isset($students_payments->teacher_child_discount) && $students_payments->teacher_child_discount != 0) { ?>
    <!--<div id="teacher_child_discount">-->
    <?php // bs3_number_with_dollar('teacher_child_discount', isset($students_payments->teacher_child_discount) ? $students_payments->teacher_child_discount : FALSE) ?>
    <!--</div>-->
<?php } ?>


<?php if (isset($student_payment_months_for_specific_student) && $student_payment_months_for_specific_student) { ?>
    <?php $m = 0; ?>
    <div class="form-group row" id="months">
        <?php foreach ($monthes_options as $key => $value) {
            ?>
            <div class="col-md-4">
                <label class="control-label col-md-6"><?php echo lang($key); ?></label>
                <div class="checkbox checkbox-success" style="margin: 12px!important;">
                    <input name="<?php echo $key; ?>" id="checkbox<?php echo $key ?>"
                           value="1"<?php if (isset($student_payment_months_for_specific_student[$key]) && $student_payment_months_for_specific_student[$key]) echo 'checked'; ?>
                           type="checkbox">
                    <label for="checkbox<?php echo $key ?>"></label>
                </div>
            </div>

        <?php } ?>
    </div>
<?php } ?>

<?php if ($students_payments->payment_item_type == 'custom_payment') { ?>
    <div id="invoice">
        <?php bs3_number_with_dollar('invoice', isset($students_payments->invoice) ? $students_payments->invoice : FALSE) ?>
    </div>
<?php } else { ?>
    <div id="invoice">
        <?php bs3_number_with_dollar('invoice', isset($students_payments->invoice) ? $students_payments->invoice : FALSE) ?>
    </div>
<?php } ?>

<div id="payment">
    <?php bs3_number_with_dollar('payment', isset($students_payments->payment) ? $students_payments->payment : FALSE) ?>
</div>

<?php if (isset($students_payments->full_year_discount) && $students_payments->full_year_discount != 0) { ?>
    <!--<div id="full_year_discount">-->
    <?php // bs3_number('full_year_discount', isset($students_payments->full_year_discount) ? $students_payments->full_year_discount : FALSE) ?>
    <!--</div>-->
<?php } ?>

<?php bs3_dropdown('check_or_cash', $check_or_cash_options, isset($students_payments->check_or_cash) ? $students_payments->check_or_cash : FALSE) ?>
<?php bs3_textarea('student_payment_description', isset($students_payments->student_payment_description) ? $students_payments->student_payment_description : FALSE) ?>
<?php bs3_date_for_details('student_payment_date', isset($students_payments->student_payment_date) ? $students_payments->student_payment_date : FALSE) ?>
<?php bs3_textarea('student_payment_notes', isset($students_payments->student_payment_notes) ? $students_payments->student_payment_notes : FALSE) ?>

<button type="button" onclick="edit_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave"
        value="true" name="create"><?php echo lang('save'); ?></button>
<?php echo form_close(); ?>
<?php bs3_card_f(); ?>
<style>
    @media (min-width: 576px) {
        .col-sm-8 {
            flex: 0 0 60% !important;
            max-width: 60% !important;
        }

        .col-sm-4 {
            flex: 0 0 20% !important;
            max-width: 20% !important;
        }
    }

    @media (min-width: 576px) {
        .col-md-8 {
            flex: 0 0 60% !important;
            max-width: 60% !important;
        }

        .col-md-4 {
            flex: 0 0 20% !important;
            max-width: 20% !important;
        }
    }
</style>


<script>
    function edit_info() {
        var data = $("#form_crud1").serialize();

        <?php
        $CI = get_instance();
        $student_id = $CI->uri->segment(3);
        $student_payment_id = $CI->uri->segment(4);
        ?>
        var url = "<?php echo base_url('student_payment/ajax_student_payment/') . $student_id . '/' . $student_payment_id; ?>";

        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {

                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
//            }
            }
        });

    }
</script>


<script>
    $(document).ready(function () {


//        $("#inputFull_year_discount").change(function () {
//            var invoice_with_discount = $("#inputInvoice").val() - ($("#inputInvoice").val() * $("#inputFull_year_discount").val() / 100);
//            $("#inputInvoice").val(invoice_with_discount);
//        });
//        $("#inputTeacher_child_discount").change(function () {
//            var invoice_with_discount = $("#inputInvoice").val() - ($("#inputInvoice").val() * $("#inputTeacher_child_discount").val() / 100);
//            $("#inputInvoice").val(invoice_with_discount);
//        });

//        });
        //**************************************************************************
        //************************* Books payment type is change *******************
        //**************************************************************************
//        $("#inputBooks_payment_type").change(function () {


        // ==================== for filter the rooms which student in it ===========
        $("#inputGrade_id").change(function () {
            var deptid = $(this).val();
            $.ajax({
                url: "<?php echo base_url("student_payment/get_rooms_for_filter/") . $student_id ?>",
                type: 'post',
                data: {depart: deptid},
                dataType: 'json',
                success: function (response) {
                    var len = response.length;
                    $("#inputRoom_id").empty();
                    for (var i = 0; i < len; i++) {
                        var grade_id = response[i]['grade_id'];
                        var id = response[i]['room_id'];
                        var name = response[i]['room_name'];
                        if (grade_id === deptid)
                            $("#inputRoom_id").append("<option value='" + id + "' grade_id1='" + id + "'>" + name + "</option>");
                    }
                }
            });
        });
    });

</script>


