<?php bs3_card($page_title,false,false); ?>

<?php
bs3_dropdown('grade_id', $students_grades_options, isset($students_payments->grade_id) ? $students_payments->grade_id : FALSE, 'disabled', FALSE);
?>

<?php
$options = array('000' => lang('select_room_name'));
$options_extra = array();
foreach ($active_student_record as $item) {
    $options[$item->room_id] = $item->room_name;
    $options_extra[$item->grade_id]['key'] = 'grade_id1';
    $options_extra[$item->grade_id]['value'] = $item->grade_id;
}
?>
<div id="room_id">
    <?php bs3_dropdown('room_id', $options, isset($students_payments->room_id) ? $students_payments->room_id : FALSE, 'disabled', $options_extra); ?>
</div>

<div id="payment_item_type">
    <?php bs3_dropdown('payment_item_type', $payment_item_type, isset($students_payments->payment_item_type) ? $students_payments->payment_item_type : FALSE, 'disabled') ?>
</div>

<?php if (isset($students_payments->books_payment_type) && $students_payments->books_payment_type != '000') { ?>
    <div id="books_payment_type">
        <?php bs3_dropdown('books_payment_type', $books_payment_type, isset($students_payments->books_payment_type) ? $students_payments->books_payment_type : FALSE, 'disabled') ?>
    </div>
<?php } ?>

<?php if (isset($students_payments->late_fee_type) && $students_payments->late_fee_type != '000') { ?>
    <div id="late_fee_type">
        <?php bs3_dropdown('late_fee_type', $late_fee_type, isset($students_payments->late_fee_type) ? $students_payments->late_fee_type : FALSE, 'disabled') ?>
    </div>
<?php } ?>

<?php if (isset($students_payments->tuition_type) && $students_payments->tuition_type != '000') {?>
    <div id="tuition_type">
        <?php bs3_dropdown('tuition_type', $tuition_type, isset($students_payments->tuition_type) ? $students_payments->tuition_type : FALSE, 'disabled') ?>
    </div>
<?php } ?>

<?php if (isset($students_payments->teacher_child_discount) && $students_payments->teacher_child_discount != 0) { ?>
    <div id="teacher_child_discount">
        <?php bs3_number('teacher_child_discount', isset($students_payments->teacher_child_discount) ? $students_payments->teacher_child_discount : FALSE, 'disabled') ?>
    </div>
<?php } ?>


<?php if (isset($student_payment_months_for_specific_student) && $student_payment_months_for_specific_student) { ?>
    <?php $m = 0; ?>
    <div class="form-group row" id="months" >
        <?php foreach ($monthes_options as $key => $value) {
            ?>
            <div class="col-md-4">
                <label class="control-label col-md-6"   ><?php echo lang($key); ?></label>
                <div class="checkbox checkbox-success">
                    <input disabled name="<?php echo $key; ?>" id="checkbox<?php echo $key ?>" 
                            value="1"<?php if (isset($student_payment_months_for_specific_student[$key]) && $student_payment_months_for_specific_student[$key]) echo 'checked'; ?>
                            type="checkbox"   >                 
                    <label for="checkbox<?php echo $key ?>"></label>
                </div>
            </div>

        <?php } ?>
    </div>
<?php } ?>

<div id="invoice">
    <?php bs3_number_with_dollar('invoice', isset($students_payments->invoice) ? $students_payments->invoice : FALSE, 'disabled') ?>
</div>

<?php if (isset($students_payments->full_year_discount) && $students_payments->full_year_discount != 0) { ?>
    <div id="full_year_discount">
        <?php bs3_number('full_year_discount', isset($students_payments->full_year_discount) ? $students_payments->full_year_discount : FALSE, 'disabled') ?>
    </div>
<?php } ?>

<?php bs3_dropdown('check_or_cash', $check_or_cash_options, isset($students_payments->check_or_cash) ? $students_payments->check_or_cash : FALSE, 'disabled') ?>
<?php bs3_textarea('student_payment_description', isset($students_payments->student_payment_description) ? $students_payments->student_payment_description : FALSE, 'disabled') ?>
<?php bs3_date_for_details('student_payment_date', isset($students_payments->student_payment_date) ? $students_payments->student_payment_date : FALSE, '','disabled') ?>
<?php bs3_textarea('student_payment_notes', isset($students_payments->student_payment_notes) ? $students_payments->student_payment_notes : FALSE, 'disabled') ?>

<?php bs3_card_f(); ?>
<style>
    .m-l-10 {
        margin-right: 48px !important;
    }
</style>
<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }
    
     @media (min-width: 576px){
        .col-sm-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-sm-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>


<script>
    function edit_info() {
        var data = $("#form_crud1").serialize();

<?php
$CI = get_instance();
$student_id = $CI->uri->segment(3);
$student_payment_id = $CI->uri->segment(4);
?>
        var url = "<?php echo base_url('student_payment/ajax_student_payment/') . $student_id . '/' . $student_payment_id; ?>";

        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {

                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });

    }
</script>


<script>
    $(document).ready(function () {
        //**************************** Hide fields related in **********************
//        $('#room_id').hide();
//        $('#payment_item_type').hide();
//        $('#books_payment_type').hide();
//        $('#months').hide();
//        $('#invoice').hide();
//        $('#late_fee_type').hide();
//        $('#teacher_child_discount').hide();
//        $('#tuition_type').hide();
//        $('#full_year_discount').hide();
        //**************************************************************************
        //************************* if Payment item type change ********************
        //**************************************************************************
        $("#inputPayment_item_type").change(function () {
            if ($(this).val() === '000') {
                $('#late_fee_type').fadeOut();
                $('#books_payment_type').fadeOut();
                $('#months').fadeOut();
                $('#invoice').fadeOut();
                //=============================== if val = registration_fee ================
            } else if ($(this).val() === 'registration_fee') {
                var type = $(this).val();
                var grade_id = $("#inputGrade_id").val();
                var room_id = $("#inputRoom_id").val();
                $.ajax({
                    url: "<?php echo base_url("student_payment/get_registration_fee_invoice/") ?>" + grade_id + "/" + room_id + "/" + type + "/" + <?php echo $student_id ?>,
                    type: 'post',
                    data: {depart: type},
                    dataType: 'json',
                    success: function (data) {
                        $("#inputInvoice").val(data.data.invoice);
                    }
                });
                $("#inputInvoice").val('0');
                $('#invoice').fadeIn();
                $('#late_fee_type').fadeOut();
                $('#books_payment_type').fadeOut();
                $('#months').fadeOut();
                $('#tuition_type').fadeOut();
                $('#full_year_discount').fadeOut();
                //=============================== if val = books ===========================
            } else if ($(this).val() === 'books') {
                $("#inputInvoice").val('0');
                $('#books_payment_type').fadeIn();
                $('#late_fee_type').fadeOut();
                $('#months').fadeOut();
                $('#tuition_type').fadeOut();
                $('#full_year_discount').fadeOut();
                //=============================== if val = late_fee ========================
            } else if ($(this).val() === 'late_fee') {
                $("#inputInvoice").val('0');
                $('#late_fee_type').fadeIn();
                $('#books_payment_type').fadeOut();
                $('#invoice').fadeOut();
                $('#months').fadeOut();
                $('#tuition_type').fadeOut();
                $('#full_year_discount').fadeOut();
                //=============================== if val = tuition =========================
            } else {
                $("#inputInvoice").val('0');
                $('#tuition_type').fadeIn();
                $('#months').fadeOut();
                $('#books_payment_type').fadeOut();
                $('#late_fee_type').fadeOut();
                $('#full_year_discount').fadeOut();
            }
        });
        //**************************************************************************
        //**************************** Tuition type is change **********************
        //**************************************************************************
        $("#inputTuition_type").change(function () {
            var type = $(this).val();
            var grade_id = $("#inputGrade_id").val();
            var room_id = $("#inputRoom_id").val();
            $.ajax({
                url: "<?php echo base_url("student_payment/get_tuition_invoice/") ?>" + grade_id + "/" + room_id + "/" + type + "/" + <?php echo $student_id ?>,
                type: 'post',
                data: {depart: type},
                dataType: 'json',
                success: function (data) {
                    if (data.data.teacher_child_check === '2') {
                        $('#teacher_child_discount').fadeIn();
                        $("#inputTeacher_child_discount").val(data.data.teacher_child_discount);
                    }
                    $("#inputInvoice").val(data.data.invoice);
                    if (type === 'full_year') {
                        $("#inputFull_year_discount").val(data.data.full_year_discount);
                    }
                }
            });
            if ($(this).val() === '000') {
                $("#inputInvoice").val('0');
                $('#invoice').fadeOut();
            } else if ($(this).val() === 'full_year') {
                $('#months').fadeOut();
                $('#invoice').fadeIn();
                $('#full_year_discount').fadeIn();
            } else if ($(this).val() === 'bimonthly') {
                $('#months').fadeIn();
                $('#full_year_discount').fadeOut();
            } else if ($(this).val() === 'monthly') {
                $('#months').fadeIn();
                $('#full_year_discount').fadeOut();
            }
        });
        //**************************************************************************
        //************************* Books payment type is change *******************
        //**************************************************************************
        $("#inputBooks_payment_type").change(function () {
            var type = $(this).val();
            var grade_id = $("#inputGrade_id").val();
            $.ajax({
                url: "<?php echo base_url("student_payment/get_book_invoice/") ?>" + grade_id + "/" + type,
                type: 'post',
                data: {depart: type},
                dataType: 'json',
                success: function (data) {
                    $("#inputInvoice").val(data.data.invoice);
                }
            });
            if ($(this).val() === '000') {
                $("#inputInvoice").val('0');
                $('#invoice').fadeOut();
            } else if ($(this).val() === 'pre_kinder_books_fee') {
                $('#invoice').fadeIn();
            } else if ($(this).val() === 'elementary_books_fee') {
                $('#invoice').fadeIn();
            } else if ($(this).val() === 'islamic_textbook') {
                $('#invoice').fadeIn();
            } else if ($(this).val() === 'islamic_workbook') {
                $('#invoice').fadeIn();
            }
        });
        //**************************************************************************
        //************************* Late fee type is change ************************
        //**************************************************************************
        $("#inputLate_fee_type").change(function () {
            var type = $(this).val();
            var grade_id = $("#inputGrade_id").val();
            $.ajax({
                url: "<?php echo base_url("student_payment/get_late_payment_fee_invoice/") ?>" + grade_id + "/" + type,
                type: 'post',
                data: {depart: type},
                dataType: 'json',
                success: function (data) {
                    $("#inputInvoice").val(data.data.invoice);
                }
            });
            if ($(this).val() === '000') {
                $("#inputInvoice").val('0');
                $('#invoice').fadeOut();
            } else if ($(this).val() === 'late_payment_fee') {
                $('#invoice').fadeIn();
            } else if ($(this).val() === 'late_pickup_fee') {
                $('#invoice').fadeIn();
            }
        });
        //**************************************************************************
        //************************* if Grade is change *****************************
        //**************************************************************************
        $("#inputGrade_id").change(function () {
            if ($(this).val() === '000') {
//                    $('#room_id').fadeOut();
            } else {
//                    $('#room_id').fadeIn();
                $('#payment_item_type').fadeIn();
            }
        });
    });


    $(document).ready(function () {

        // ==================== for filter the rooms which student in it ===========
        $("#inputGrade_id").change(function () {
            var deptid = $(this).val();
            $.ajax({
                url: "<?php echo base_url("student_payment/get_rooms_for_filter/") . $student_id ?>",
                type: 'post',
                data: {depart: deptid},
                dataType: 'json',
                success: function (response) {
                    var len = response.length;
                    $("#inputRoom_id").empty();
                    for (var i = 0; i < len; i++) {
                        var grade_id = response[i]['grade_id'];
                        var id = response[i]['room_id'];
                        var name = response[i]['room_name'];
                        if (grade_id === deptid)
                            $("#inputRoom_id").append("<option value='" + id + "' grade_id1='" + id + "'>" + name + "</option>");
                    }
                }
            });
        });
    });

</script>


