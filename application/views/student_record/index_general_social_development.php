<?php bs3_card($page_title, '', false); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php } ?>
<?php $general_social_development = get_general_social_development(); ?>
<?php foreach ($general_social_development as $value) { ?>
    <?php if (isset($data[$value]) && $data[$value]) { ?>
        <?php bs3_dropdown($value, $mark_key_options, $data[$value]) ?>
    <?php } else { ?>
        <?php bs3_dropdown($value, $mark_key_options) ?>
    <?php } ?>
<?php } ?>
<?php if ($_current_year == $_archive_year) { ?>
    <button type="button" onclick="add_to_db()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>
<?php } ?>
<?php bs3_card_f(); ?>


<script>
    function add_to_db() {
        // ajax adding data to database
        var formData = new FormData($('#form_crud1')[0]);
        $.ajax({
            url: "<?php echo base_url("student_record/add_general_social_development/") . $student_record_id ?>",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (dataR) {
                if (dataR.status == "200") //if success close modal and reload ajax table
                {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: dataR.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (dataR.status == "201") {
                    // valedation error data.data
                    messages_error("<?php echo lang('error'); ?>", dataR.data);

                } else if (dataR.status == "202") {

                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: dataR.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",

                    });
                } else if (dataR.status == "400") {
                    // message error in controller data.message
                } else if (dataR.status == "401") {
                    // message error in information data.message and status
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error adding / update data');
                $('.btnSave').text('save'); //change button text
                $('.btnSave').attr('disabled', false); //set button enable
            }
        });
    }

</script>