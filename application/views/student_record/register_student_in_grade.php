<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_student_record") ?>
    <?php bs3_hidden('id') ?>
    <a href="<?php echo base_url('student/index_student'); ?>"><p style=" color: red;"><strong> <?php echo lang('note'); ?>: </strong> <?php echo lang('if_you_dont_find_student_please_add_it_by_click_on_this_note'); ?></p></a>    
    <?php bs3_input('student_id', false, true, 'update') ?>
    <?php bs3_checkbox('is_active') ?>
    <?php
    $options1 = array('000' => lang('select_grade_name'));
    $options_extra1 = array();
    foreach ($grades_options as $item) {
        $options1[$item->id] = $item->grade_name;
        $options_extra1[$item->id]['key'] = 'grade_id1';
        $options_extra1[$item->id]['value'] = $item->id;
    }
    ?>
    <?php
    bs3_dropdown('grade_id', $options1, FALSE, FALSE, $options_extra1, 'update');
    ?>

    <?php
    $options = array('000' => lang('select_room_name'));
    $options_extra = array();
    foreach ($rooms_options as $item) {
        $options[$item->id] = $item->room_name;
        $options_extra[$item->id]['key'] = 'grade_id2';
        $options_extra[$item->id]['value'] = $item->grade_id;
    }
    ?>
    <div id="room_id">
        <?php bs3_dropdown('room_id', $options, FALSE, FALSE, $options_extra, 'update'); ?>
    </div>

    <?php bs3_checkbox('teacher_child') ?>


    <?php bs3_dropdown('photo_permission_slips', $photo_permission_slips_options) ?>
    <div class="custom_field">
        <?php bs3_checkbox('islamic_book_return') ?>
        <?php bs3_dropdown('status_islamic_book_return', $status_islamic_book_return_array) ?>
    </div>

    <?php bs3_hidden('student_key') ?>
    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>
<?php bs3_table_f() ?>

<?php bs3_card_f(); ?>
<style>
    ul#ui-id-1 {z-index:10000!important}
</style>
<script>
    $(function () {
        $("#inputStudent_id").autocomplete({
            minLength: 1,
            source: "<?php echo base_url() ?>student_record/search_student_auto",
            focus: function (event, ui) {
                $("#inputStudent_id").val(ui.item.value);
                return false;
            },
            select: function (event, ui) {
                $("#inputStudent_id").val(ui.item.value);
                $("#inputStudent_key").val(ui.item.key);
                return false;
            }
        });
    });

    $(document).ready(function () {
//        $('.custom_field').hide();
    });


    $("#inputGrade_id").change(function () {
        if ($(this).data('options') == undefined) {
            $(this).data('options', $('#inputRoom_id option').clone());
        }
        var id = $(this).find(":selected").attr('grade_id1');
        var options = $(this).data('options').filter('[grade_id2=' + id + ']');
        $('#inputRoom_id').html(options);

        var grade_id = $("#inputGrade_id").val();
        var URL = "<?php echo base_url('room_attendance/get_stage_of_grade/') ?>" + grade_id;

        $.ajax({
            url: URL,
            type: "post",
            data: {date: grade_id},
            dataType: "json",
            success: function (data) {
//                if (data.data.stage_id === '2')
                    $('.custom_field').fadeIn();
//                else
//                    $('.custom_field').fadeOut();
            },
            error: function () {
//                alert("errors");
            }
        });
    });

</script>
