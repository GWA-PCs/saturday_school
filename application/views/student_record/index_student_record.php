<?php $room = (isset($room_name)) ? lang('room_id') . ': ' . $room_name->room_name . '<br><br>' . $room_name->grade_name : ""; ?>
<?php bs3_card($page_title, $room); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_student_record") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_input('student_id', false, true, 'update') ?>
    <?php bs3_checkbox('is_active') ?>
    <?php bs3_checkbox('teacher_child') ?>
    <?php bs3_dropdown('photo_permission_slips', $photo_permission_slips_options) ?>
    <?php // if ($room_name->stage_id == '2') { ?>
        <?php // bs3_radio ('photo_permission_slips');   ?>

        <?php bs3_checkbox('islamic_book_return') ?>
        <?php bs3_dropdown('status_islamic_book_return', $status_islamic_book_return_array) ?>
    <?php // } ?>
    <?php bs3_hidden('student_key') ?>
    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>
<?php bs3_table_f() ?>

<?php bs3_card_f(); ?>
<style>
    ul#ui-id-1 {z-index:10000!important}
</style>
<script>
    $(function () {
        $("#inputStudent_id").autocomplete({
            minLength: 1,
            source: "<?php echo base_url() ?>student_record/search_student_auto",
            focus: function (event, ui) {
                $("#inputStudent_id").val(ui.item.value);
                return false;
            },
            select: function (event, ui) {
                $("#inputStudent_id").val(ui.item.value);
                $("#inputStudent_key").val(ui.item.key);
                return false;
            }
        });
    });
</script>
