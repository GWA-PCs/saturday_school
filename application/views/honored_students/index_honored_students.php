<?php bs3_card($page_title); ?>

<?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_honored_students") ?>
<?php bs3_hidden('id') ?>

<?php
$options_extra = array();
bs3_dropdown('grade_id', $teacher_grades_options, false, false, $options_extra, 'update')
?>

<?php
$options = array('000' => lang('select_room_name'));
$options_extra = array();
foreach ($teacher_rooms as $item) {
    $options[$item->grade_id] = $item->room_name;
    $options_extra[$item->grade_id]['key'] = 'grade_id1';
    $options_extra[$item->grade_id]['value'] = $item->grade_id;
}
?>
<?php bs3_dropdown('room_id', $options, false, false, $options_extra, 'update') ?>

<?php
$options = array('000' => lang('select_student_name'));
$options_extra = array();
foreach ($students_teacher as $item) {
    $options[$item->student_record_id] = $item->student_name . ' ' . $item->family_name;
    $options_extra[$item->student_record_id]['key'] = 'grade_id2';
    $options_extra[$item->student_record_id]['value'] = $item->grade_id;
}
?>
<?php bs3_dropdown('student_record_id', $options, false, false, $options_extra, 'update') ?>

<?php // bs3_date('honored_date'); ?>
<?php // bs3_textarea('honored_reason'); ?>
<?php bs3_modal_footer('create'); ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>

<!--<script>
    $(document).ready(function () {

        $("#inputGrade_id").change(function () {
            var deptid = $(this).val();
            $.ajax({
                url: "<?php echo base_url("honored_students/get_honored_students_for_filter/") . $honored_order_id ?>",
                type: 'post',
                data: {depart: deptid},
                dataType: 'json',
                success: function (response) {

                    var len = response.length;

                    $("#inputRoom_id").empty();
                    for (var i = 0; i < len; i++) {
                        var id = response[i]['grade_id'];
                        var name = response[i]['room_name'];

                        $("#inputRoom_id").append("<option grade_id1='" + id + "'>" + name + "</option>");

                    }
                }
            });
        });

        $("#inputRoom_id").change(function () {
            var deptid = $(this).val();
            $.ajax({
                url: "<?php echo base_url("honored_students/get_honored_students_for_filter/") . $honored_order_id ?>",
                type: 'post',
                data: {depart: deptid},
                dataType: 'json',
                success: function (response) {

                    var len = response.length;

                    $("#inputStudent_record_id").empty();
                    for (var i = 0; i < len; i++) {
                        var id = response[i]['room_id'];
                        var name = response[i]['student_name'];

                        $("#inputStudent_record_id").append("<option grade_id2='" + id + "'>" + name + "</option>");

                    }
                }
            });
        });
    });
</script>-->

<script>


    $("#inputGrade_id").change(function () {

        if ($(this).data('options') == undefined) {
            $(this).data('options', $('#inputRoom_id option').clone());
        }
        var id = $(this).val();
        console.log();
        var options = $(this).data('options').filter('[grade_id1=' + id + ']');
        $('#inputRoom_id').html(options);
    });

</script>