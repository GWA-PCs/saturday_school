<?php $room = (isset($room_name)) ? lang('room_id') . ': ' . $room_name->room_name . '<br><br>' . $room_name->grade_name : ""; ?>
<?php bs3_card($page_title, $room); ?>


<?php // bs3_table($thead, 'object_table') ?>
<?php // bs3_table_f() ?>


<div class="table-responsive m-t-40">

    <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
        <thead>
            <tr>
                <?php if ($_current_year == $_archive_year) { ?>
                    <?php if (isset($students) && $students) { ?>
                        <th id="head-checkbox">
                            <input name="" type="checkbox" class="checkboxfull_perms"   />
                        </th> 
                    <?php } ?>
                <?php } ?>
                <th>  <?php echo lang('student_name'); ?> </th>
                <th>  <?php echo lang('father_name'); ?> </th>
                <th>  <?php echo lang('mother_name'); ?> </th>
            </tr>
        </thead>
        <tbody id="transferring">
            <?php if (isset($students) && $students) { ?>
                <?php foreach ($students as $value) { ?>
                    <tr data-id="<?php echo $value->id ?>"  >
                        <?php if ($_current_year == $_archive_year) { ?>
                            <td>
                                <input name="" type="checkbox" class="i-check"  />
                            </td>
                        <?php } ?>
                        <td style="display:none"><?php echo $value->id ?> </td>
                        <td ><a href="<?php echo base_url("profile/page_student_profile/" . $value->student_id) ?>"><?php echo $value->student_name . ' ' . $value->family_name ?></a></td>
                        <td><?php echo $value->father_name ?></td>
                        <td><?php echo $value->mother_name ?></td>  
                    </tr>
                <?php } ?> 
            <?php } else { ?> 
                <tr class="odd"><td valign="top" colspan="4" class="dataTables_empty"><?php echo lang('no_matching_records_found') ?></td></tr>
            <?php } ?>
        </tbody> 
    </table>

</div>
<?php if ($_current_year == $_archive_year) { ?>
    <?php if (isset($students) && $students) {
        ?>
        <?php if (isset($rooms) && $rooms) { ?>
            <div class="add-new-item">
                <?php echo form_open_multipart('', 'class="form-horizontal" role="form" id="create_form"'); ?>

                <?php bs3_hidden('create', true) ?>

                <?php bs3_dropdown('move_to_room', $rooms_options) ?>
                <button type="button" onclick="edit_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>

                <?php echo form_close() ?>
            </div>	

        <?php } ?>
    <?php } ?>
<?php } ?>
<?php bs3_card_f(); ?>

<style>
    [type="checkbox"]:not(:checked).checkboxfull_perms, [type="checkbox"]:checked.checkboxfull_perms {
        position: inherit!important;
    }
    [type="checkbox"]:not(:checked).i-check, [type="checkbox"]:checked.i-check {
        position: inherit!important;
    }

</style>  

<style>
    @media (min-width: 576px){
        .col-sm-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-sm-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<script>
    var arr;
    function edit_info() {
        arr = $('#table-style').find('[type="checkbox"]:checked').map(function () {
            return $(this).closest('tr').find('td:nth-child(2)').text();
        }).get();

        var Rooms_id = $('#inputMove_to_room').val();

        dataString = arr; // array?
        var jsonString = JSON.stringify(dataString);
        $.ajax({
            type: 'post',
            url: "<?php echo base_url('transferring_students/movements/') . $room_name->id ?>",
            data: {data: jsonString, room: Rooms_id},
            dataType: "json",
            success: function (data) {
                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                    reloadTable();
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });
    }

    function reloadTable() {
        var room_id = <?php echo $room_id ?>;
        var URL = "<?php echo base_url('transferring_students/get_all_data/') ?>" + room_id;

        $.getJSON(URL, function (data) {
            var output = html_output(data.data);
            $("#transferring").html(output);
        });
    }

    function html_output(data) {
        var output = "";
        if (data != null && data != "") {
            $.each(data, function (i, item) {
                output += '<tr data-id="' + item.id + '" >';
                output += '<td>';
                output += '<input name="" type="checkbox" class="i-check" />';
                output += '</td>';
                output += '<td style="display:none" >';
                output += item.id;
                output += '</td>';
                output += '<td>';
                output += item.student_name + " " + item.family_name;
                output += '</td>';
                output += '<td>';
                output += item.father_name;
                output += '</td>';
                output += '<td>';
                output += item.mother_name;
                output += '</td>';
                output += "</tr>";
            });
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="4" class="dataTables_empty">';
            output += "<?php echo lang('no_matching_records_found'); ?>"
            output += '</td>';
            output += '</tr>';
            $(".add-new-item").hide();
            $("#head-checkbox").hide();
        }
        return output;
    }

    //for check all
    $(document).ready(function () {
        $('.checkboxfull_perms').change(function () {
            if ($('.checkboxfull_perms').is(':checked')) {
                $(".i-check").prop('checked', true);
            }
        });
        $('.i-check').change(function () {
            ////////////////////////////////////////////////goooooooooooooooooooood//////////////////////
            var all = false;
            $(".i-check").each(function (i) {
                if ($(this).is(':checked')) {
                    all = true;
                } else {
                    all = false;
                    return all;
                }
            });
            if (all) {
                $(".checkboxfull_perms").prop('checked', true);
            } else {
                $(".checkboxfull_perms").prop('checked', false);
            }
        });

    });
</script>

