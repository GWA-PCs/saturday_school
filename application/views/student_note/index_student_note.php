<?php bs3_card($page_title); ?>
<?php if (isset($add_student_note) && $add_student_note) { ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_student_note") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_dropdown('note_month', $months, false, false, false, 'update') ?>
    <?php bs3_date('note_date') ?>
    <?php bs3_textarea('note_text') ?>

    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php } ?>
<?php bs3_table($thead, 'object_table','','','',$non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>