<?php bs3_card($page_title, FALSE, FALSE); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php } ?>
<?php $tuition = get_tuition_array(); ?>
<?php
$saturday_school = array();
$arabic_book_club = array();
$youth_group = array();
?>
<?php foreach ($tuition as $value) { ?>
    <?php
    if (strpos($value, 'saturday_school') !== false) {
        $saturday_school[$value] = $value;
    } elseif (strpos($value, 'arabic_book_club') !== false) {
        $arabic_book_club[$value] = $value;
    } elseif (strpos($value, 'youth_group') !== false) {
        $youth_group[$value] = $value;
    }
    ?>
    <?php
}
?>

<p>
    <button class="btn btn-block" type="button" data-toggle="collapse" data-target="#saturday_school" aria-expanded="false" aria-controls="collapseExample">
        <?php echo lang('saturday_school'); ?>
    </button>
</p>

<div class="collapse" id="saturday_school">
    <div class="card card-body">
        <?php
        foreach ($saturday_school as $key => $value) {
            if (isset($payment_items_tuition[$value]) && $payment_items_tuition[$value]) {
                if ($_current_year == $_archive_year) {
                    if ( $key == 'saturday_school_teacher_child_discount') {
                        bs3_number($value, $payment_items_tuition[$value]);
                    } else {
                        bs3_number_with_dollar($value, $payment_items_tuition[$value]);
                    }
                } else {
                    if ( $key == 'saturday_school_teacher_child_discount') {
                        bs3_number($value, $payment_items_tuition[$value], 'disabled="true"');
                    } else {
                        bs3_number_with_dollar($value, $payment_items_tuition[$value], 'disabled="true"');
                    }
                }
            } else {
                if ($_current_year == $_archive_year) {
                    if ( $key == 'saturday_school_teacher_child_discount') {
                        bs3_number($value);
                    } else {
                        bs3_number_with_dollar($value);
                    }
                } else {
                    if ($key == 'saturday_school_teacher_child_discount') {
                        bs3_number($value, '', 'disabled="true"');
                    } else {
                        bs3_number_with_dollar($value, '', 'disabled="true"');
                    }
                }
            }
        }
        ?>
    </div>
</div>

<p>
    <button class="btn btn-block" type="button" data-toggle="collapse" data-target="#arabic_book_club" aria-expanded="false" aria-controls="collapseExample">
        <?php echo lang('arabic_book_club'); ?>
    </button>
</p>

<div class="collapse" id="arabic_book_club">
    <div class="card card-body">
        <?php foreach ($arabic_book_club as $value) { ?>
            <?php if ($_current_year == $_archive_year) { ?>
                <?php if (isset($payment_items_tuition[$value]) && $payment_items_tuition[$value]) { ?>
                    <?php bs3_number_with_dollar($value, $payment_items_tuition[$value]) ?>
                <?php } else { ?>
                    <?php bs3_number_with_dollar($value) ?>
                <?php } ?>
            <?php } else { ?>
                <?php if (isset($payment_items_tuition[$value]) && $payment_items_tuition[$value]) { ?>
                    <?php bs3_number_with_dollar($value, $payment_items_tuition[$value], 'disabled="true"') ?>
                <?php } else { ?>
                    <?php bs3_number_with_dollar($value, '', 'disabled="true"') ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<p>
    <button class="btn btn-block" type="button" data-toggle="collapse" data-target="#youth_group" aria-expanded="false" aria-controls="collapseExample">
        <?php echo lang('youth_group'); ?>
    </button>
</p>

<div class="collapse" id="youth_group">
    <div class="card card-body">
        <?php
        foreach ($youth_group as $key => $value) {
            if (isset($payment_items_tuition[$value]) && $payment_items_tuition[$value]) {
                if ($_current_year == $_archive_year) {
                    if ( $key == 'youth_group_teacher_child_discount') {
                        bs3_number($value, $payment_items_tuition[$value]);
                    } else {
                        bs3_number_with_dollar($value, $payment_items_tuition[$value]);
                    }
                } else {
                    if ( $key == 'youth_group_teacher_child_discount') {
                        bs3_number($value, $payment_items_tuition[$value], 'disabled="true"');
                    } else {
                        bs3_number_with_dollar($value, $payment_items_tuition[$value], 'disabled="true"');
                    }
                }
            } else {
                if ($_current_year == $_archive_year) {
                    if ( $key == 'youth_group_teacher_child_discount') {
                        bs3_number($value);
                    } else {
                        bs3_number_with_dollar($value);
                    }
                } else {
                    if ( $key == 'youth_group_teacher_child_discount') {
                        bs3_number($value, '', 'disabled="true"');
                    } else {
                        bs3_number_with_dollar($value, '', 'disabled="true"');
                    }
                }
            }
        }
        ?>
    </div>
</div>

<?php if ($_current_year == $_archive_year) { ?>
    <button type="button" onclick="add_to_db()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>
<?php } ?>
<?php bs3_card_f(); ?>
<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<script>
    function add_to_db() {
        // ajax adding data to database
        var formData = new FormData($('#form_crud1')[0]);
        $.ajax({
            url: "<?php echo base_url("financing/add_tuition") ?>",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (dataR) {
                if (dataR.status == "200") //if success close modal and reload ajax table
                {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: dataR.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (dataR.status == "201") {
                    // valedation error data.data
                    messages_error("<?php echo lang('error'); ?>", dataR.data);

                } else if (dataR.status == "202") {

                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: dataR.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",

                    });
                } else if (dataR.status == "400") {
                    // message error in controller data.message
                } else if (dataR.status == "401") {
                    // message error in information data.message and status
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error adding / update data');
                $('.btnSave').text('save'); //change button text
                $('.btnSave').attr('disabled', false); //set button enable
            }
        });
    }

</script>
<style>
    .btn-block {
        margin-bottom: 30px;
        padding: 12px;
    }    
</style>