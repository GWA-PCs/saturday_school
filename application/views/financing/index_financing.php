<div class="row">
    <?php $i = $color_index = 5 ?>
    <?php bs3_card_wid_colored(base_url('financing/index_tuition'), "mdi mdi-book-open-page-variant", lang("index_tuition"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('financing/index_petty_cash'), "mdi mdi-cash-multiple", lang("index_petty_cash"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('financing/index_pizza_and_snack_inputs'), "mdi mdi-silverware-variant", lang("index_pizza_and_snack_inputs"), false, $color_wid[$color_index++], 100, $text_align); ?>
</div>