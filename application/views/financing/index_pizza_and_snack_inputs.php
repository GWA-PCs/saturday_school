<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_input") ?>
    <?php bs3_hidden('id') ?>

    <?php bs3_date('input_date'); ?>  
    <?php bs3_input('input_title'); ?> 
    <?php bs3_number_with_dollar('cost'); ?>
    <?php // bs3_number_with_dollar('deposit'); ?>
    <?php // bs3_number_with_dollar('withdrawal'); ?>

    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<br><br>
<br>
<div class="" style="margin-left: 4px;">
    <h5 style="color: red"> <b> Note: </b> This values will change when you enter the inputs details for each food sales</h5>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <!--<hr>-->
            <div class="d-flex flex-row">

                <div class="m-l-10 align-self-center">
                    <button class="btn" style="cursor: auto; color:white; background-color:#20AEE3;" ><?php echo lang('total_sum'); ?> </button>
                    <hr>
                    <h5 class="text-center" id="total_sum"> </h5>
                </div>
                <div class="m-l-10 align-self-center">
                    <button class="btn" style="cursor: auto; color:white; background-color:#1D7BC2;" ><?php echo lang('cost'); ?> </button>
                    <hr>
                    <h5 class="text-center" id="cost"> </h5>
                </div>
                <div class="m-l-10 align-self-center">
                    <button class="btn" style="cursor: auto; color:white; background-color:#FF5C6C;" ><?php echo lang('profit'); ?> </button>
                    <hr>
                    <h5 class="text-center" id="profit"> </h5>
                </div>
                <!--                <div class="m-l-10 align-self-center">
                                    <h5 class="text-center" style="color: red" ><?php echo lang('deposit'); ?> </h5>
                                    <hr>
                                    <h5 class="text-center " style="color: red" id="deposit"> </h5>
                                    <hr>
                                </div>-->

            </div>
        </div>
    </div>
</div>

<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>



<style>
    hr{
        margin-top: 8px!important;
    }
    .m-l-10 {
        margin-right: 150px !important;
    }
</style>

<script>
    $(document).ready(function () {
    $.ajax({
    url: "<?php echo base_url("financing/how_much_made_overall_pizza_and_snack") ?>",
            type: "POST",
            dataType: "JSON",
            success: function (data) {
            $.each(data['data'], function (key, value) {
            $("#" + key).text(value);
            });
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
    });
    });</script>



<script>

    var save_method; //for save method string
    var table;
    var base_url = '<?php echo base_url(); ?>';
    function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
    }
<?php if (isset($specific_show_object_link)) { ?>
        $(document).ready(function () {

        //datatables
        table = $('.object_table').DataTable({

        dom: 'Bfrtip',
                buttons: [
                {
                text: '<i class="mdi mdi-file-excel "> </i> Excel',
                        extend: 'excel',
                        action: newExportAction,
                        exportOptions: {
                        columns: '.printable'
                        }
                },
                {
                text:'<i class="mdi mdi-printer "> </i> Print',
                        extend: 'print',
                        exportOptions: {
                        columns: '.printable'
                        }
                },
                ],
    <?php if ($lang == "ar") { ?>
            "language": {
            "sProcessing": "جاري التحميل...",
                    "sLengthMenu": "إظهار _MENU_ عناصر",
                    "sZeroRecords": "لم يُعثر على أية سجلات",
                    "sInfo": "العناصر الظاهرة من _START_ إلى _END_ من أصل _TOTAL_ عنصر",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجلّ",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث:",
                    "sUrl": "",
                    "oPaginate": {
                    "sFirst": "الأول",
                            "sPrevious": "السابق",
                            "sNext": "التالي",
                            "sLast": "الأخير"
                    }
            },
    <?php } ?>
        "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering": false,
                // Load data for the table's content from an Ajax source
                "ajax": {
                "url": "<?php echo base_url($specific_show_object_link) ?>",
                        "type": "POST"
                },
                //Set column definition initialisation properties.

                "displayLength":10,
                "columnDefs": [
                {
                "targets": [ - 1], //last column
                        "orderable": false, //set not orderable
                },
                {
                "targets": [ - 2], //2 last column (photo)
                        "orderable": false, //set not orderable
                },
                ],
                'createdRow': function (row, data, dataIndex) {
                $(row).attr('id', 'someID');
                },
        });
        });
<?php } ?>
<?php if (isset($modal_name)) { ?>
        function add_object(modal_id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
        var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
        if (hidden_in == "add") {
        $('[name=' + pair[0] + ']').parent().parent().hide();
        } else {
        $('[name=' + pair[0] + ']').parent().parent().show();
        }
        }
        save_method = 'add';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $(show_modal_id).modal('show'); // show bootstrap modal
        var add_object_title = "<?php echo $add_object_title; ?>";
        $('.modal-title').text(add_object_title); // Set Title to Bootstrap modal title

        $('#photo-preview').hide(); // hide photo preview modal

        $('#label-photo').text('Upload Photo'); // label photo upload
        }
<?php } ?>
<?php if (isset($specific_get_object_link)) { ?>
        function edit_object(id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        save_method = 'update';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
        var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
        if (hidden_in == "update") {
        $('[name=' + pair[0] + ']').parent().parent().hide();
        } else {
        $('[name=' + pair[0] + ']').parent().parent().show();
        }
        }



        $(show_modal_id).modal('show'); // show bootstrap modal
        //Ajax Load data from ajax
        $.ajax({
        url: "<?php echo base_url($specific_get_object_link) ?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                for (var pair of formData.entries()) {
                $('[name=' + pair[0] + ']').val(data[pair[0]]);
                }
                var update_object_title = "<?php echo $update_object_title; ?>";
                $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error get data from ajax');
                }
        });
        }
<?php } ?>
    function specific_get_response(data) {

    var show_modal_id = "<?php echo "#$modal_name" ?>";
    if (data.status == "200") {
    // message success data.message
    swal({
    title: "<?php echo lang('success') ?>",
            text: data.message,
            type: "success",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    $(show_modal_id).modal('hide');
    reload_table();
    var total_sum_url = "<?php echo base_url("financing/how_much_made_overall_pizza_and_snack") ?>";
    $.ajax({
    url: total_sum_url + data['data'],
            type: "POST",
            dataType: "JSON",
            success: function (data) {
//            console.log(data['data']);
            $.each(data['data'], function (key, value) {
            $("#" + key).html(value);
            });
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
    });
    //$("#remain_cost_after_discount").html(data['data']);
    } else if (data.status == "201") {
    // valedation error data.data
    messages_error("<?php echo lang('error'); ?>", data.data);
    } else if (data.status == "202") {

    swal({
    title: "<?php echo lang('error_existing') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    } else if (data.status == "203") {
    // permission error 
    var redirct_url = "<?php echo base_url() ?>/home/home_page";
    window.location.replace(redirct_url);
    } else if (data.status == "204") {
    //permission success
    } else if (data.status == "205") {
    // Date error 
    swal({
    title: "<?php echo lang('date_error') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    } else if (data.status == "206") {
    // Date error 
    swal({
    title: "<?php echo lang('Time_error') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    } else if (data.status == "400") {
    swal({
    title: "<?php echo lang('error') ?>",
            text: data.message,
            type: "error",
            confirmButtonText: "<?php echo lang('close') ?>",
    });
    $(show_modal_id).modal('hide');
    // message error in controller data.message
    } else if (data.status == "401") {
    // message error in information data.message and status
    }
    }
<?php if (isset($specific_add_object_link) || isset($specific_update_object_link)) { ?>
        function save() {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable
        var url;
        if (save_method == 'add') {
        url = "<?php echo site_url($specific_add_object_link) ?>";
        } else {
        url = "<?php echo site_url($specific_update_object_link) ?>";
        }
        specific_add_update(url);
        }
<?php } ?>

<?php if (isset($specific_delete_object_link)) { ?>

        function delete_object(id) {
        swal({
        title: "<?php echo lang('delete_confirmation_text') ?>",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "<?php echo lang('close') ?>",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('delete') ?>",
                closeOnConfirm: false
        }, function () {
        $.ajax({
        url: "<?php echo site_url($specific_delete_object_link) ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                //if success reload ajax table
                // $(show_modal_id).modal('hide');
                swal("<?php echo lang('delete_success') ?>", "", "success");
                reload_table();
                var total_sum_url = "<?php echo base_url("financing/how_much_made_overall_pizza_and_snack") ?>";
                $.ajax({
                url: total_sum_url + data['data'],
                        type: "POST",
                        dataType: "JSON",
                        success: function (data) {
                        //            console.log(data['data']);
                        $.each(data['data'], function (key, value) {
                        $("#" + key).html(value);
                        });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                });
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error deleting data');
                }
        });
        });
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        }

<?php } ?>
    function specific_add_update(url) {
    // ajax adding data to database
    var formData = new FormData($('#form_crud')[0]);
    $.ajax({
    url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
            if (data.status) //if success close modal and reload ajax table
            {
            specific_get_response(data);
            }
            else {
            for (var i = 0; i < data.inputerror.length; i++) {
            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
            }
            }
            var save = '<?php echo lang('save') ?>';
            $('.btnSave').text(save); //change button text
            $('.btnSave').attr('disabled', false); //set button enable
            },
            error: function (jqXHR, textStatus, errorThrown) {
//            alert('Error adding / update data');
            var save = '<?php echo lang('save') ?>';
            $('.btnSave').text(save); //change button text
            $('.btnSave').attr('disabled', false); //set button enable
            }
    });
    }
</script>