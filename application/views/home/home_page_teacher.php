<div class="row">
    <?php $i = $color_index = 0 ?>
    <?php if (isset($teacher_rooms) && $teacher_rooms) { ?>
        <?php
        foreach ($teacher_rooms as $item) {
            $color_index = color_index($color_index, $color);
            bs3_card_wid_colored("teacher_room/$item->room_id", 'mdi mdi-arrange-bring-forward', ucwords(strtolower($item->room_name)), false, $color_wid[$color_index++], 100, $text_align);
        }
        ?>
    <?php } ?>
    <?php if ($home_page_links) { ?>
        <?php
        foreach ($home_page_links as $item) {
            $color_index = color_index($color_index, $color);
            bs3_card_wid_colored($item['link'], $item['icon'], lang($item['page']), false, $color_wid[$color_index++], 100, $text_align);
        }
        ?>
    <?php } ?>

</div>
