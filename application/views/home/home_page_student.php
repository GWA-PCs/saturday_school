<div class="row">
    <?php if ($home_page_links) { ?>
        <?php $i = $color_index = 6 ?>
        <?php foreach ($home_page_links as $item) {
            $color_index = color_index($color_index, $color);
            bs3_card_wid_colored($item['link'], $item['icon'], lang($item['page']), false, $color_wid[$color_index++], 100, $text_align);
        } ?>
    <?php } ?>

</div>
