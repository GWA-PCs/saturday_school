<!--<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item fix-lang">
<?php if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                        <a  href="<?php echo base_url('home/home_page_teacher') ?>" ><?php echo lang('home_page') ?></a>
<?php } elseif (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                        <a href="<?php echo base_url('home/home_page_parent') ?>"  ><?php echo lang('home_page') ?></a>
<?php } else { ?>
                        <a  href="<?php echo base_url('home/home_page') ?>" ><?php echo lang('home_page') ?></a>
<?php } ?>

            </li>
            <li class="breadcrumb-item active  fix-lang"><?php echo $page_title ?></li>
        </ol>
    </div>
</div>-->
<div class="row">
    <?php if ($home_page_links) { ?>
        <?php $i = $color_index = 0 ?>
        <?php
        foreach ($home_page_links as $item) {
            $color_index = color_index($color_index, $color_wid);
            bs3_card_wid_home_page($item['link'], $item['icon'], lang($item['page']), false, $color_wid[$color_index++], 100, $text_align);
        }
        ?>
    <?php } ?>
</div>
