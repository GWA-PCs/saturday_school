<div class="row">
    <?php $i = $color_index = 3 ?>
    <?php if (isset($rooms_info) && $rooms_info) { ?>
        <?php
        $color_index = color_index($color_index, $color);
        bs3_card_wid_colored(base_url("teacher/index_teacher_subjects/$rooms_info->room_id"), 'mdi mdi-book-open-page-variant fa-2x', lang('index_teacher_subjects'), false, $color_wid[$color_index++], 100, $text_align);
        $color_index = color_index($color_index, $color);
        bs3_card_wid_colored(base_url("teacher/index_teacher_student_record/$rooms_info->room_id"), 'mdi  mdi-account-multiple-plus fa-2x', lang('index_teacher_student_record'), false, $color_wid[$color_index++], 100, $text_align);
       $color_index = color_index($color_index, $color);
        bs3_card_wid_colored(base_url("room_attendance/index_room_attendance/$rooms_info->room_id"), 'mdi mdi-account-multiple-minus fa-2x', lang('index_room_attendance'), false, $color_wid[$color_index++], 100, $text_align);
        $color_index = color_index($color_index, $color);
        bs3_card_wid_colored(base_url("teacher/homeworks/$rooms_info->room_id"), 'mdi mdi-book-multiple-variant fa-2x ', lang('homeworks'), false, $color_wid[$color_index++], 100, $text_align);
        ?>
    <?php } ?>
</div>
