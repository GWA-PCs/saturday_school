<?php bs3_card($page_title, FALSE, FALSE); ?>
<br>
<?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php bs3_dropdown('grade', $grades) ?>
<button type="button" onclick="search_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('search'); ?></button>
<div class="table-responsive m-t-40">
    <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>  <?php echo lang('grade_name'); ?> </th>
                <th>  <?php echo lang('students_number_s1'); ?> </th>
                <th>  <?php echo lang('students_number_s2'); ?> </th>
                <th>  <?php echo lang('students_number_end_year'); ?> </th>
            </tr>
        </thead>
        <tbody id="no_active_records">
        </tbody> 
    </table>
</div>


<style>
    @media (min-width: 576px){
        .col-sm-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-sm-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<?php form_close(); ?>
<?php bs3_card_f(); ?>

<script type="text/javascript">
    $(function () {
        search_info();
    });

    function search_info() {
//        var data = $("#form_crud1").serialize();
        var grade_id = $('#inputGrade').val();

        var url = "<?php echo base_url('report/search_number_of_students/'); ?>" + grade_id;

        $.ajax({
            url: url,
            dataType: "json",
//            data: data,
            type: "post",
            success: function (data) {
//                console.log(data);
                reload_table(data);
            },
            error: function () {
//                alert("Error");
            }
        });

    }
    function reload_table(data) {
        var output = "";
        if (data.data != null && data.data != "") {
            $.each(data.data, function (i, item) {
                output += "<tr>";
                output += "<td>";
                output += item.grade_name;
                output += "</td>";

                output += "<td>";
                output += item.students_number_s1;
                output += "</td>";

                output += "<td>";
                output += item.students_number_s2;
                output += "</td>";

                output += "<td>";
                output += item.students_number_end_year;
                output += "</td>";

                output += "</tr>";
            });
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="4" class="dataTables_empty">';
            output += "<?php echo lang('no_matching_records_found'); ?>"
            output += '</td>';
            output += '</tr>';
        }

        $("#no_active_records").html(output);
    }

</script>
