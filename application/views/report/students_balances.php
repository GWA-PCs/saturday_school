<?php bs3_card($page_title, FALSE, FALSE); ?>
<br>
<?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php bs3_dropdown('balance', $balance_options) ?>
<button type="button" onclick="search_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('search'); ?></button>
<div class="table-responsive m-t-40">
    <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>  <?php echo lang('student_name'); ?> </th>
                <th>  <?php echo lang('balance'); ?> </th>
            </tr>
        </thead>
        <tbody id="students_balances">
        </tbody> 
    </table>
</div>

<?php form_close(); ?>
<?php bs3_card_f(); ?>

<style>
    @media (min-width: 576px){
        .col-sm-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-sm-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }
</style>
<script type="text/javascript">
    $(function () {
        search_info();
    });

    function search_info() {
//        var data = $("#form_crud1").serialize();
        var balance = $('#inputBalance').val();

        var url = "<?php echo base_url('report/search_students_balances/'); ?>" + balance;

        $.ajax({
            url: url,
            dataType: "json",
//            data: data,
            type: "post",
            success: function (data) {
//                console.log(data);
                reload_table(data);
            },
            error: function () {
//                alert("Error");
            }
        });

    }
    function reload_table(data) {
        var output = "";
        if (data.data != null && data.data != "") {
            $.each(data.data, function (i, item) {
                output += "<tr>";
                output += "<td>";
                output += item.student_name + " " + item.family_name;
                output += "</td>";

                output += "<td>";
                output += item.student_balance;
                output += "</td>";

                output += "</tr>";
            });
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="2" class="dataTables_empty">';
            output += "<?php echo lang('no_matching_records_found'); ?>"
            output += '</td>';
            output += '</tr>';
        }

        $("#students_balances").html(output);
    }

</script>
