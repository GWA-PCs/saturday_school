<div class="row">
    <?php $i = $color_index = 2 ?>
    <?php bs3_card_wid_colored(base_url('report/student_non_active_manually'), "mdi mdi-account-remove ", lang("student_non_active_manually"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('report/students_who_did_not_give_permission'), "mdi mdi-camera-off", lang("students_who_did_not_give_permission"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('report/students_who_did_not_return_the_books'), "mdi mdi-book-open-page-variant ", lang("students_who_did_not_return_the_books"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('report/number_of_students'), "mdi mdi-account-multiple", lang("number_of_students"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('report/students_payments'), "mdi mdi-square-inc-cash ", lang("students_payments"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('report/students_balances'), "fa fa-money", lang("students_balances"), false, $color_wid[$color_index++], 100, $text_align, 'padding: 16px;!important'); ?>
    <?php bs3_card_wid_colored(base_url('report/students_attendances'), "mdi mdi-pencil-box-outline", lang("students_attendances"), false, $color_wid[$color_index++], 100, $text_align); ?>


</div>