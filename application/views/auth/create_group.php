<?php bs3_card($page_title); ?>

<?php bs3_modal_header('group', 'create', "fa fa-plus-circle  hvr-icon", "add_new_group", false , 'create', 'auth/create_group') ?>
<?php bs3_input('group_name', $group_name, true, true) ?>
<?php bs3_input('description', $description, true, true) ?>
<?php foreach ($permissions as $key => $perm) { ?>
    <?php echo bs3_checkbox("perm_".$key, ${"perm_".$key}, false, false, false, $perm ); ?>
<?php }?>
<?php bs3_modal_footer('create'); ?>


<?php bs3_card_f(); ?>
