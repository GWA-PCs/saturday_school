<link href="<?php echo base_url("assets/$dir/css/pages/login-register-lock.css") ?>" rel="stylesheet">
<style>
    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .25);
    }
    .control-label{
        line-height: normal!important;
    }
    
    
    @media (max-width: 767px){
        .login-sidebar .login-box {
            position: relative;
            zoom: 72%!important;
        }
    }
    </style>

</style>


<script>


    function log() {
        var formData = new FormData($('#loginForm')[0]);

        var loginUrl = "<?php echo base_url("auth/login_function") ?>";
        var homePageUrl = "<?php echo base_url('auth/index') ?>";

        $.ajax({
            url: loginUrl,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                if (data.status == 200) {
                    localStorage.setItem("user_id", data.data.user_id);
                    localStorage.setItem("token", data.data.token);
                    redirectTo(homePageUrl);
                    //return to home page
                } else if (data.status == 201) {
                    //validation error
                    messages_error("<?php echo lang('error'); ?>", data.data);
                } else if (data.status == 406) {
                    //login failed
                    messages_error("<?php echo lang('error'); ?>", data.data);
                }
            }
        });
    }

    function redirectTo(page) {
        window.location.replace(page);
    }

</script>

<div class="login-box card">
    <div class="card-body">
        <div class="form-horizontal form-material" style="margin-top:80px">
            <?php echo form_open("auth/login", array('id' => 'loginForm')); ?>
            <!--<form class="form-horizontal form-material" id="loginform" action="auth/login">-->
            <a href="javascript:void(0)" class="text-center db">
                <img  src="<?php echo base_url("assets/assets/images/stratus-logo.png") ?>"/>
            </a>
            <div class="form-group m-t-30">
                <div class="col-xs-12 m-l-10 m-r-10">
                    <?php echo form_input('identity', '', 'class="form-control" placeholder="Username"'); ?>
                </div>
            </div>
            <div class="form-group m-t-40">
                <div class="col-xs-12 m-l-10 m-r-10">
                    <?php echo form_password('password', '', 'class="form-control" placeholder="Password"'); ?>
                </div>
            </div>
            <div class="form-group text-center">
                <div class="col-xs-12 m-b-5 m-t-30  m-l-10 m-r-10">
                    <button type="button" class="btn btn-block btn-lg btn-info btn-rounded" onclick="log()"><?php echo lang('login_submit_btn'); ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        </form>
        <form class="form-horizontal" id="recoverform" action="index.html">
            <div class="form-group ">
                <div class="col-xs-12">
                    <h3>Recover Password</h3>
                    <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Email"></div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light"
                            type="submit">Reset
                    </button>
                </div>
            </div>
        </form>
    </div>

    <footer class="footer text-center">
        <a href="http://gwa-us.com/">POWER BY © GWA GROUP 2010 - <?php echo date("Y") ?></a>
    </footer>
</div>


