<!--<h1><?php echo lang('change_password_heading'); ?></h1>-->
<?php bs3_card($page_title, FALSE, FALSE); ?>
<div style="color: red" d="infoMessage"><?php echo $message; ?></div>
<?php if ($lang == 'ar') { ?>
    <style>
        form label {
            float: right;
        }
        .btn {
            float: right;
        }

    </style>
<?php } ?> 
<?php echo form_open("auth/change_password"); ?>
<p>
    <label for="old_password"> <?php echo lang('change_password_old_password_label', 'old_password'); ?></label> 

    <?php echo form_input($old_password); ?>
</p>
<br>
<p>
    <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length); ?></label> 
    <?php echo form_input($new_password); ?>
</p>
<br>
<p>
    <label for="old_password"> <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm'); ?> </label>
    <?php echo form_input($new_password_confirm); ?>
</p>

<?php echo form_input($user_id); ?>
<p><?php echo form_submit('submit', lang('change_password_submit_btn')); ?></p>

<?php echo form_close(); ?>
<?php bs3_card_f(); ?>


<script>


//    function log() {
//        var formData = new FormData($('#loginForm')[0]);
//
//        var logoutUrl = '<?php echo base_url('auth/logout') ?>';
//            var loginUrl = '<?php echo base_url('auth/change_password') ?>';
//
//        $.ajax({
//            url: loginUrl,
//            type: "POST",
//            data: formData,
//            contentType: false,
//            processData: false,
//            dataType: "JSON",
//            success: function (data) {
//                if (data.status == 200) {
////                    localStorage.setItem("user_id", data.data.user_id);
////                    localStorage.setItem("token", data.data.token);
//                   redirectTo(loginUrl);
//                    //return to home page
//                } else if (data.status == 201) {
//                    //validation error
//                    messages_error("<?php echo lang('error'); ?>", data.data);
//                } else if (data.status == 406) {
//                    //login failed
//                    messages_error("<?php echo lang('error'); ?>", data.data);
//                }
//            }
//        });
//    }
//
//    function redirectTo(page) {
//        window.location.replace(page);
//    }

</script>