<?php bs3_card($page_title); ?>
<?php
$CI = get_instance();
$url_seg = $CI->uri->segment(3);
?>
<style>
    .form-group{
        padding: 18px!important;
        margin-bottom: 0px!important;
    } 
</style>
<div class="my_div" style="padding:13px;">
    <?php if ($url_seg == 'recieve_msg') { ?>

        <div class="form-group row " style="background-color: #F4F4F4;">
            <label class="col-md-3"><?php echo lang("msg_from") . ": "; ?> </label>
            <div class="col-md-8">
                <h6 id="msg_from"></h6>
            </div>
        </div>
    <?php } else { ?>
        <div class="form-group row " style="background-color: #F4F4F4;">
            <label class="col-md-3"><?php echo lang("msg_to") . ": "; ?> </label>
            <div class="col-md-8">
                <h6 id="msg_to"></h6>
            </div>
        </div>
    <?php } ?>
    <div class="form-group row">
        <label class="col-md-3"><?php echo lang("msg_subject") . ": "; ?> </label>
        <div class="col-md-8">
            <h6 id="msg_subject"></h6>
        </div>
    </div>
    <div class="form-group row" style="background-color: #F4F4F4;">
        <label class="col-md-3"><?php echo lang("msg_date") . ": "; ?> </label>
        <div class="col-md-9">
            <h6 id="msg_date"></h6>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-3"><?php echo lang("msg_content") . ": "; ?> </label>
        <div class="col-md-9">
            <h6 id="msg_content"></h6>
        </div>
    </div>
    <div class="form-group row" style="background-color: #F4F4F4;">
        <label class="col-md-3"><?php echo lang("msg_file") . ": "; ?> </label>
        <div class="col-md-9">
            <a id="msg_file" href="<?php echo base_url("assets/uploads/msg_file/") . $msg_file ?>"><?php lang('no_file') ?></a>
        </div>
    </div>

</div>
<?php bs3_card_f(); ?>



<script>
    $(document).ready(function () {
        get_msg_details();
        update_status_details();
    });

    function get_msg_details() {
        var msg_id = <?php echo $msg_id; ?>;

        var url_seg = "<?php
$CI = get_instance();
echo $CI->uri->segment(3);
?>";
        if (url_seg === 'send_msg') {
            var URL = "<?php echo base_url("message/get_sent_msg_details/") ?>" + msg_id;
        } else if (url_seg === 'recieve_msg') {
            var URL = "<?php echo base_url("message/get_recieve_msg_details/") ?>" + msg_id;
        }

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var msg_from = data.data.msg_from;
                var msg_to = data.data.msg_to;
                var msg_subject = data.data.msg_subject;
                var msg_content = data.data.msg_content;
                var msg_file = data.data.msg_file;
                var msg_date = data.data.msg_date;

                $("#msg_from").text(msg_from);
                $("#msg_to").text(msg_to);
                $("#msg_subject").text(msg_subject);
                $("#msg_content").text(msg_content);
                $("#msg_file").text(msg_file);
                $("#msg_date").text(msg_date);

            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function update_status_details() {
        $.ajax({
            url: "<?php echo base_url("message/update_status_details/") . $msg_id.'/'.$user_id ?>",
            type: "POST",
            dataType: "JSON",
            success: function (data) {
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    }
</script>
