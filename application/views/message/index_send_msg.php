<!-- =================== Script for multiple select =================== -->
<script src="<?php echo base_url("assets/assets/select2.min.js") ?>" type="text/javascript"></script>
<link href="<?php echo base_url("assets/assets/select2.min.css") ?>" rel="stylesheet" type="text/css"/>

<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "compose_message") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_dropdown('msg_to_type', $msg_to_options, FALSE, FALSE, FALSE, 'update') ?>

    <!-- ======================= Start Multiple select ======================== -->
    <div id="msg_to">
        <div class="form-group row " >
            <label for="inputMsg_to" class="col-sm-4 control-label"><?php echo lang('msg_to[]'); ?>:</label>
            <div class="col-sm-8">
                <select class="js-example-basic-multiple-limit1 form-control" id="inputMsg_to" hidden-in="update" name="msg_to[]" required="true" multiple="multiple">
                </select>
            </div>
        </div>
    </div>    
    <!-- ======================= End Multiple select ======================== -->

    <?php bs3_input('msg_subject') ?>
    <?php bs3_textarea('msg_content') ?>
    <?php bs3_checkbox('upload_file_check'); ?>
    <div id="msg_file">
        <?php bs3_file_or_image('msg_file', '', false, '100', 'false', false) ?>    
    </div>
    <script>
        $(document).ready(function () {

            $('#msg_file').hide();
            $('#checkboxupload_file_check').change(function () {

                if (this.checked) {
                    $('#msg_file').fadeIn();
                    var element = "";
                    element += '<span class="btn btn-default btn-file">';
                    element += '  <span class="fileinput-new">';
                    element += '<?php echo lang("choose_file") ?>';
                    element += '  </span>';
                    element += '  <span class="fileinput-exists">';
                    element += '<?php echo lang("change") ?>';
                    element += '  </span>';
                    element += '<input required="true"  type="file" accept=".pdf,.docx,.doc,.xls,.xlsx,.txt,.csv,.ppt,.pptx,.zip,.rar,.psd,.txt,.pps,image/*" id="file" name="msg_file">';
                    element += '  </span>';
                    element += '<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">';
                    element += '<?php echo lang("remove") ?>';
                    element += '</a>';
                    $("#div_for_file").html(element);
                } else {
                    $("#msg_file").remove();
                }

            });
        });
    </script>




    <div id="send_by_email">
        <?php bs3_checkbox('send_by_email'); ?>
    </div>

    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>

<?php bs3_card_f(); ?>
<script type="text/javascript">
    function edit_object(id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        save_method = 'update';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        //$('.help-block').empty(); // clear error string

        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
            var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
            if (hidden_in == "update") {
                $('[name=' + pair[0] + ']').parent().parent().hide();
            } else {
                $('[name=' + pair[0] + ']').parent().parent().show();
            }
        }

        $(show_modal_id).modal('show'); // show bootstrap modal
        //Ajax Load data from ajax
        $form = $(event.target);
        var URL = "<?php echo base_url($get_object_link_message) ?>/" + id;
        $.ajax({
            url: URL,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $("form#form_crud :input").each(function (index, item) {
                    var input_name = $(this).attr('name'); // This is the jquery object of the input, do what you will
                    var input_type = $(this).attr('type');
                    if (input_type === 'file') {
                        $(".fileinput").removeClass("fileinput-new");
                        $(".fileinput").addClass("fileinput-exists");
                        $(".fileinput-preview").text(data.msg_file);
                    }
                    if (input_type != 'checkbox' && input_type != 'file') {
                        $(this).val(data[input_name]);
                    } else if (input_type == 'checkbox') {
                        if (data[input_name] == 2) {
                            $(this).prop("checked", true);
                        } else {
                            $(this).prop("checked", false);
                        }
                    }
                });
                var update_object_title = "<?php echo $update_object_title; ?>";
                $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error get data from ajax');
            }
        });
    }

</script>

<script>
    $(document).ready(function () {
        $('#msg_to').hide();
// ==================== For filter the rooms which student in it ===========
        $("#inputMsg_to_type").change(function () {
            var msg_to_users = $(this).val();
            $.ajax({
                url: "<?php echo base_url("message/get_msg_to_users/") ?>" + msg_to_users,
                type: 'post',
                data: {depart: msg_to_users},
                dataType: 'json',
                success: function (response) {
                    var len = response.length;
                    $("#inputMsg_to").empty();
                    for (var i = 0; i < len; i++) {
                        var user_id = response[i]['user_id'];
                        var first_name = response[i]['first_name'];
                        var last_name = response[i]['last_name'];
                        $("#inputMsg_to").append("<option  id='" + user_id + "' class='my_form'  value='" + user_id + "'>" + first_name + ' ' + last_name + "</option>");
                    }

                }
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#inputMsg_to').val("");
//**************************** Hide fields related in ******************* * **
        $('#msg_to').hide();
        $('#msg_subject').hide();
        $('#msg_content').hide();
//**************************************************************************
//************************* if message to change ********************
//**************************************************************************
        $("#inputMsg_to_type").change(function () {
            if ($(this).val() === '000') {
                $('#msg_to').fadeOut();
                $('#msg_subject').fadeOut();
                $('#msg_content').fadeOut();
//=============================== if val = teachers ================
            } else if ($(this).val() === 'teachers') {
                $('#msg_to').fadeIn();
                $('#msg_subject').fadeIn();
                $('#msg_content').fadeIn();
                $('#send_by_email').fadeIn();
//=============================== if val = parents ===========================
            } else if ($(this).val() === 'parents') {
                $('#msg_to').fadeIn();
                $('#msg_subject').fadeIn();
                $('#msg_content').fadeIn();
                $('#send_by_email').fadeIn();
//=============================== if val = class_parents ===========================
            } else if ($(this).val() === 'class_parents') {
                $('#msg_to').fadeIn();
                $('#msg_subject').fadeIn();
                $('#msg_content').fadeIn();
                $('#se                nd_by_email').fadeIn();
            }    //=============================== if val = students =========================== 
            else if ($(this).val() === 'students') {
                $('#msg_to').fadeIn();
                $('#msg_subject').fadeIn();
                $('#msg_content').fadeIn();
                $('#send_by_email').fadeOut();
//=============================== if val = class_students ===========================
            } else if ($(this).val() === 'class_students') {
                $('#msg_to').fadeIn();
                $('#msg_subject').fadeIn();
                $('#msg_content').fadeIn();
                $('#send_by_email').fadeOut();
            }
//=============================== if val = admin ===========================
            else if ($(this).val() === 'admin') {
                $('#msg_to').fadeOut();
                $('#msg_subject').fadeIn();
                $('#msg_content').fadeIn();
                $('#send_by_email').fadeIn();
            }
        });
    });
</script>

<script                     type="text/javascript">
    $(".js-example-basic-multiple-limit").select2({
        maximumSelectionLength: 100,
    });
    $(".js-example-basic-multiple-limit1").select2({
        maximumSelectionLength: 100,
    });

</script>

<style>
    .select2-container--default .select2-results>.select2-results__options{
        margin-top: -20px !important;
    }
    .select2-selection__rendered {
        resize: auto!important;
        width: 400px;
        min-height: 100px;
        padding: 5px;
        overflow-y: scroll!important;
        box-sizing: border-box;
    }

    .select2-container--open{
        z-index:9999999         
    }
    .select2-container {
        width: 100% !important;
    }
    .select2-results__options li{
        list-style-type: none;
    }

    .select2-selection--multiple{
        margin-bottom: -2px!important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__rendered{
        border: 1px solid #dddddd;
        border-radius: .25rem;
        height: calc(2.25rem + 20px);
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        /* border: solid black 1px; */
        outline: 0;
        /*height: 45px;*/
        border-radius: 0px;
        border: 1px solid #e7e7e7;
        /*margin-top: 10px;*/
        width: 100%;
        display: block;
    }
    .select2-container--default .select2-selection--multiple {
        background-color: white;
        /*height: 45px  !important;*/
        border: 1px solid #fff !important;
        border-radius: 0px !important;
        cursor: text;
        /*margin-top: 10px;*/
        width: 100% !important;
    }
    .tg-search-form .select2-container--default.select2-container--focus .select2-selection--multiple {
        /* border: solid black 1px; */
        outline: 0;
        /*height: 38px!important;*/
        border-radius: 0px;
        border: 1px solid #e7e7e7;
        /*margin-top: 0;*/
        width: 84%!important;
        display: block;
    }
    .tg-search-form .select2-container--default .select2-selection--multiple {
        background-color: white;
        /*height: 38px!important;*/
        border: 1px solid #fff !important;
        border-radius: 0px !important;
        cursor: text;
        /*margin-top: 0px;*/
        width: 84%!important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        color: #657176!important;
    }


    /*    .form {
            border: 1px solid black;
            text-align: center;
            outline: none;
            min-width:4px;
        }*/

</style>

