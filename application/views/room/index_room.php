<?php bs3_card($page_title, lang('grade_id') . ': ' . $grade_name); ?>

<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_room") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_input('room_name') ?>
    <?php bs3_modal_footer('create'); ?> 
<?php } ?>

<?php bs3_table($thead, 'object_table','','','',$non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>
