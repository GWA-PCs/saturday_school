
<?php bs3_card($page_title); ?>

<?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_student_parent") ?>
<?php bs3_hidden('id') ?>
<?php bs3_dropdown('parent_id',$parents) ?>
<?php bs3_hidden('parent_key') ?>
<?php bs3_modal_footer('create'); ?>

<?php bs3_table($thead, 'object_table','','','',$non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>
<script>
    $(function () {
        $("#inputParent_id").autocomplete({
            minLength: 1,
            source: "<?php echo base_url() ?>student_parent/search_parent_auto",
            focus: function (event, ui) {
                $("#inputParent_id").val(ui.item.value);
                return false;
            },
            select: function (event, ui) {
                $("#inputParent_id").val(ui.item.value);
                $("#inputParent_key").val(ui.item.key);
                return false;
            }
        });
    });
</script>


