<?php bs3_card($page_title); ?>
<?php
$CI = get_instance();
$url_seg = $CI->uri->segment(3);
?>

<div class="form-group row">
    <label class="col-md-3"><?php echo lang('family_last_name'); ?>: </label>
    <div class="col-md-9">
        <h6 id="family_last_name"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('student_1'); ?>: </label>
    <div class="col-md-9">
        <h6 id="student_1"></h6>

    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('student_2'); ?>:</label>
    <div class="col-md-9">
        <h6 id="student_2"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('student_3'); ?>:</label>
    <div class="col-md-9">
        <h6 id="student_3"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('student_4'); ?>:</label>
    <div class="col-md-9">
        <h6 id="student_4"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('email'); ?>: </label>
    <div class="col-md-9">
        <h6 id="email"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('home_address'); ?>: </label>
    <div class="col-md-9">
        <h6 id="home_address"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('home_phone'); ?>: </label>
    <div class="col-md-9">
        <h6 id="home_phone"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('mother_name'); ?>: </label>
    <div class="col-md-9">
        <h6 id="mother_name"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('father_name'); ?>: </label>
    <div class="col-md-9">
        <h6 id="father_name"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('mother_phone'); ?>: </label>
    <div class="col-md-9">
        <h6 id="mother_phone"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('father_phone'); ?>: </label>
    <div class="col-md-9">
        <h6 id="father_phone"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('attended'); ?>: </label>
    <div class="col-md-9">
        <h6 id="attended"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('their_grade_level'); ?>: </label>
    <div class="col-md-9">
        <h6 id="their_grade_level"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('other_information_1'); ?>: </label>
    <div class="col-md-9">
        <h6 id="other_information_1"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('other_information_2'); ?>: </label>
    <div class="col-md-9">
        <h6 id="other_information_2"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('other_information_3'); ?>: </label>
    <div class="col-md-9">
        <h6 id="other_information_3"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('other_information_4'); ?>: </label>
    <div class="col-md-9">
        <h6 id="other_information_4"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('photo_permission_slips'); ?>: </label>
    <div class="col-md-9">
        <h6 id="permission"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('date'); ?>: </label>
    <div class="col-md-9">
        <h6 id="date"></h6>
    </div>
</div>
<hr>
<div class=" " style="text-align: center; background-color: #E5E5E5; padding: 10px;">
    <strong><?php echo lang("emergency_contact"); ?></strong>

</div>
<hr>
<div class="form-group row">
    <label class="col-md-1"><?php echo lang('emergency_name_1'); ?>: </label>
    <div class="col-md-3">
        <h6 id="emergency_name_1"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_phone_1'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_phone_1"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_relationship_1'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_relationship_1"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-1"><?php echo lang('emergency_name_2'); ?>: </label>
    <div class="col-md-3">
        <h6 id="emergency_name_2"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_phone_2'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_phone_2"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_relationship_2'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_relationship_2"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-1"><?php echo lang('emergency_name_3'); ?>: </label>
    <div class="col-md-3">
        <h6 id="emergency_name_3"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_phone_3'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_phone_3"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_relationship_3'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_relationship_3"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('custody_info_specify'); ?>: </label>
    <div class="col-md-9">
        <h6 id="custody_info_specify"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('custody_info_other_info'); ?>: </label>
    <div class="col-md-9">
        <h6 id="custody_info_other_info"></h6>
    </div>
</div>
<hr>

<?php bs3_card_f(); ?>



<script>
    $(document).ready(function () {
        get_form_details();
    });

    function get_form_details() {
        var URL = "<?php echo base_url("enrollment_requests/get_registration_form_info/" . $url_seg); ?>";
        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {

                var family_last_name = data.data.family_last_name;
                var student_1 = data.data.student_1 + ', ' + data.data.student_age_1 + ', ' + data.data.student_grade_1;

                if (data.data.student_2 === "") {
                    var student_2 = "";
                } else {
                    var student_2 = data.data.student_2 + ', ' + data.data.student_age_2 + ', ' + data.data.student_grade_2;
                }
                if (data.data.student_3 === "") {
                    var student_3 = "";
                } else {
                    var student_3 = data.data.student_3 + ', ' + data.data.student_age_3 + ', ' + data.data.student_grade_3;
                }
                if (data.data.student_4 === "") {
                    var student_4 = "";
                } else {
                    var student_4 = data.data.student_4 + ', ' + data.data.student_age_4 + ', ' + data.data.student_grade_4;
                }
                var email = data.data.email;
                var home_address = data.data.home_address;
                var home_phone = data.data.home_phone;
                var mother_name = data.data.mother_name;
                var mother_phone = data.data.mother_phone;
                var father_name = data.data.father_name;
                var father_phone = data.data.father_phone;
                var attended = data.data.attended;
                var their_grade_level = data.data.their_grade_level;
                var other_information_1 = data.data.other_information_1;
                var other_information_2 = data.data.other_information_2;
                var other_information_3 = data.data.other_information_3;
                var other_information_4 = data.data.other_information_4;
                var agree = data.data.agree;
                var contract = data.data.contract;
                var permission = data.data.permission;
                var date = data.data.date;
                var type = data.data.type;

                // ============= Emergency Contact  =============
                var emergency_name_1   = data.data.emergency_name_1;
                var emergency_name_2   = data.data.emergency_name_2;
                var emergency_name_3   = data.data.emergency_name_3;
                var emergency_phone_1  = data.data.emergency_phone_1;
                var emergency_phone_2  = data.data.emergency_phone_2;
                var emergency_phone_3  = data.data.emergency_phone_3;
                var emergency_relationship_1 = data.data.emergency_relationship_1;
                var emergency_relationship_2 = data.data.emergency_relationship_2;
                var emergency_relationship_3 = data.data.emergency_relationship_3;
                var custody_info_specify        = data.data.custody_info_specify;
                var custody_info_other_info  = data.data.custody_info_other_info;

                if (contract === 1) {
                    contract = "<?php echo lang('yes') ?>";
                } else {
                    contract = "<?php echo lang('no') ?>";
                }
                if (permission === 1) {
                    permission = "<?php echo lang('yes') ?>";
                } else {
                    permission = "<?php echo lang('no') ?>";
                }

                if (custody_info_specify === 1) {
                    custody_info_specify = "<?php echo lang('yes') ?>";
                } else {
                    custody_info_specify = "<?php echo lang('no') ?>";
                }

                $("#family_last_name").text(family_last_name);
                $("#student_1").text(student_1);
                $("#student_2").text(student_2);
                $("#student_3").text(student_3);
                $("#student_4").text(student_4);
                $("#email").text(email);
                $("#home_address").text(home_address);
                $("#home_phone").text(home_phone);
                $("#mother_name").text(mother_name);
                $("#mother_phone").text(mother_phone);
                $("#father_name").text(father_name);
                $("#father_phone").text(father_phone);
                $("#attended").text(attended);
                $("#their_grade_level").text(their_grade_level);
                $("#other_information_1").text(other_information_1);
                $("#other_information_2").text(other_information_2);
                $("#other_information_3").text(other_information_3);
                $("#other_information_4").text(other_information_4);
                $("#agree").text(agree);
                $("#contract").text(contract);
                $("#permission").text(permission);
                $("#date").text(date);
                $("#type").text(type);

                $("#emergency_name_1").text(emergency_name_1);
                $("#emergency_name_2").text(emergency_name_2);
                $("#emergency_name_3").text(emergency_name_3);
                $("#emergency_phone_1").text(emergency_phone_1);
                $("#emergency_phone_2").text(emergency_phone_2);
                $("#emergency_phone_3").text(emergency_phone_3);
                $("#emergency_relationship_1").text(emergency_relationship_1);
                $("#emergency_relationship_2").text(emergency_relationship_2);
                $("#emergency_relationship_3").text(emergency_relationship_3);
                $("#custody_info_specify").text(custody_info_specify);
                $("#custody_info_other_info").text(custody_info_other_info);

            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

</script>
