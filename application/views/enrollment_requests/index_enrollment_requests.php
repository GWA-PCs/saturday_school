<div class="row">
    <?php $i = $color_index = 2 ?>
    <?php bs3_card_wid_colored(base_url('enrollment_requests/index_registration_form'), "mdi mdi-account-card-details ", lang("index_registration_form"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('enrollment_requests/index_arabic_reading_club'), "mdi mdi-book-variant ", lang("index_arabic_reading_club"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('enrollment_requests/index_youth_group_registration_form'), "mdi mdi-account-multiple ", lang("index_youth_group_registration_form"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('enrollment_requests/index_summer_camp_form'), "wi wi-solar-eclipse ", lang("index_summer_camp_form"), false, $color_wid[$color_index++], 100, $text_align,'padding: 16px;!important'); ?>

</div>