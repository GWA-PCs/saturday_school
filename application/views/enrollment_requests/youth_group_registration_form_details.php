<?php bs3_card($page_title); ?>
<?php
$CI = get_instance();
$url_seg = $CI->uri->segment(3);
?>

<div class="form-group row">
    <label class="col-md-3"><?php echo lang('family_last_name'); ?>: </label>
    <div class="col-md-9">
        <h6 class="family_last_name_y"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('student_name'); ?>: </label>
    <div class="col-md-9">
        <h6 class="student_1"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('age'); ?>: </label>
    <div class="col-md-9">
        <h6 class="student_age_1"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('email'); ?>: </label>
    <div class="col-md-9">
        <h6 class="email"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('home_address'); ?>: </label>
    <div class="col-md-9">
        <h6 class="home_address"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('home_phone'); ?>: </label>
    <div class="col-md-9">
        <h6 class="home_phone"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('mother_name'); ?>: </label>
    <div class="col-md-9">
        <h6 class="mother_name"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('father_name'); ?>: </label>
    <div class="col-md-9">
        <h6 class="father_name"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('mother_phone'); ?>: </label>
    <div class="col-md-9">
        <h6 class="mother_phone"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('father_phone'); ?>: </label>
    <div class="col-md-9">
        <h6 class="father_phone"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('photo_permission_slips'); ?>: </label>
    <div class="col-md-9">
        <h6 class="permission"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('date'); ?>: </label>
    <div class="col-md-9">
        <h6 class="date"></h6>
    </div>
</div>
<hr>
<div class=" " style="text-align: center; background-color: #E5E5E5; padding: 10px;">
    <strong><?php echo lang("emergency_contact"); ?></strong>

</div>
<hr>
<div class="form-group row">
    <label class="col-md-1"><?php echo lang('emergency_name_1'); ?>: </label>
    <div class="col-md-3">
        <h6 id="emergency_name_1"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_phone_1'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_phone_1"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_relationship_1'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_relationship_1"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-1"><?php echo lang('emergency_name_2'); ?>: </label>
    <div class="col-md-3">
        <h6 id="emergency_name_2"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_phone_2'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_phone_2"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_relationship_2'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_relationship_2"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-1"><?php echo lang('emergency_name_3'); ?>: </label>
    <div class="col-md-3">
        <h6 id="emergency_name_3"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_phone_3'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_phone_3"></h6>
    </div>
    <label class="col-md-2"><?php echo lang('emergency_relationship_3'); ?>: </label>
    <div class="col-md-2">
        <h6 id="emergency_relationship_3"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('custody_info_specify'); ?>: </label>
    <div class="col-md-9">
        <h6 id="custody_info_specify"></h6>
    </div>
</div>
<hr>
<div class="form-group row">
    <label class="col-md-3"><?php echo lang('custody_info_other_info'); ?>: </label>
    <div class="col-md-9">
        <h6 id="custody_info_other_info"></h6>
    </div>
</div>
<hr>
<?php bs3_card_f(); ?>



<script>
    $(document).ready(function () {
        get_form_details();
    });

    function get_form_details() {
        var URL = "<?php echo base_url("enrollment_requests/get_youth_group_registration_form_info/" . $url_seg); ?>";
        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var family_last_name_y = data.data.family_last_name;
                var student_1_y = data.data.student_1;
                var student_age_1_y = data.data.student_age_1;
                var email_y = data.data.email;
                var home_address_y = data.data.home_address;
                var home_phone_y = data.data.home_phone;
                var mother_name_y = data.data.mother_name;
                var mother_phone_y = data.data.mother_phone;
                var father_name_y = data.data.father_name;
                var father_phone_y = data.data.father_phone;
                var permission_y = data.data.permission;
                var date_y = data.data.date;
                  // ============= Emergency Contact  =============
                var emergency_name_1   = data.data.emergency_name_1;
                var emergency_name_2   = data.data.emergency_name_2;
                var emergency_name_3   = data.data.emergency_name_3;
                var emergency_phone_1  = data.data.emergency_phone_1;
                var emergency_phone_2  = data.data.emergency_phone_2;
                var emergency_phone_3  = data.data.emergency_phone_3;
                var emergency_relationship_1 = data.data.emergency_relationship_1;
                var emergency_relationship_2 = data.data.emergency_relationship_2;
                var emergency_relationship_3 = data.data.emergency_relationship_3;
                var custody_info_specify        = data.data.custody_info_specify;
                var custody_info_other_info  = data.data.custody_info_other_info;

                if (permission_y === "1") {
                    permission_y = "<?php echo lang('yes') ?>";
                } else {
                    permission_y = "<?php echo lang('no') ?>";
                }
//            
                $(".family_last_name_y").text(family_last_name_y);
                $(".student_1").text(student_1_y);
                $(".student_age_1").text(student_age_1_y);
                $(".email").text(email_y);
                $(".home_address").text(home_address_y);
                $(".home_phone").text(home_phone_y);
                $(".mother_name").text(mother_name_y);
                $(".mother_phone").text(mother_phone_y);
                $(".father_name").text(father_name_y);
                $(".father_phone").text(father_phone_y);
                $(".permission").text(permission_y);
                $(".date").text(date_y);
                $("#emergency_name_1").text(emergency_name_1);
                $("#emergency_name_2").text(emergency_name_2);
                $("#emergency_name_3").text(emergency_name_3);
                $("#emergency_phone_1").text(emergency_phone_1);
                $("#emergency_phone_2").text(emergency_phone_2);
                $("#emergency_phone_3").text(emergency_phone_3);
                $("#emergency_relationship_1").text(emergency_relationship_1);
                $("#emergency_relationship_2").text(emergency_relationship_2);
                $("#emergency_relationship_3").text(emergency_relationship_3);
                $("#custody_info_specify").text(custody_info_specify);
                $("#custody_info_other_info").text(custody_info_other_info);

            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

</script>
