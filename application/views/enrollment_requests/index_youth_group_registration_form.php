<?php bs3_card($page_title); ?>
<style>
    a:hover {
        color: #0056b3!important;
    }
    a {
        color: #20aee3!important;
    }
/*     .delete_item a {
        color: #FF5C6C!important;
    }*/
    
</style>
<div class="table-responsive m-t-40">
    <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>  <?php echo lang('family_name'); ?> </th>
                <th>  <?php echo lang('student_name'); ?> </th>
                <th>  <?php echo lang('age'); ?> </th>
                <th>  <?php echo lang('details'); ?> </th>
                <?php if ($_current_year == $_archive_year) { ?>
                    <th>  <?php echo lang('approve_registration'); ?> </th>
                    <th>  <?php echo lang('delete'); ?> </th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; ?>
            <?php if (isset($enrollment_requests) && $enrollment_requests) { ?>
                <?php foreach ($enrollment_requests as $item) { ?>
                    <tr data-id="<?php echo $item->id ?>"  id="item-<?php echo $item->id ?>">
                        <td><?php echo $item->family_last_name ?></td>
                        <td><?php echo $item->student_1 ?></td>
                        <td><?php echo $item->student_age_1 ?></td>

                        <td><a href="<?php echo base_url('enrollment_requests/' . $item->form_name . '_details/' . $item->id) ?>"><span><i class="fa-2x mdi mdi-alert-circle "></i></span></a></td>

                        <?php if ($_current_year == $_archive_year) { ?>
                            <?php $i ++; ?>
                            <!--edit td-->
                            <td class="non_printable">
                                <!--strat edit modal-->
                                <?php bs3_modal_header_crud_custom("update-modal$i", $title = 'approve_registration'); ?>
                                <div class="modal-body ">
                                    <?php bs3_hidden('update', true) ?>
                                    <?php bs3_hidden('id', $item->id) ?>
                                    <?php
                                    bs3_dropdown('grade_id_1', $students_grades_options, FALSE, '', FALSE);
                                    ?>

                                    <?php
                                    $options = array('000' => lang('select_room_name'));
                                    $options_extra = array();
                                    foreach ($students_rooms_options as $item) {
                                        $options[$item->id] = $item->room_name;
                                        $options_extra[$item->grade_id]['key'] = 'grade_id1';
                                        $options_extra[$item->grade_id]['value'] = $item->grade_id;
                                    }
                                    ?>
                                    <?php bs3_dropdown('room_id_1', $options, FALSE, '', $options_extra); ?>
                                </div>
                                <div class="modal-footer">
                                    <button type = "button" onclick="save_btn(<?php echo $item->id ?>)"  class=" btn btn-info btn-rounded hvr-icon-spin hvr-shadow"><?php echo lang('save'); ?></button>
                                    <button type="button" class="btn btn-default btn-rounded hvr-icon-spin hvr-shadow" data-dismiss="modal"><?php echo lang('close'); ?></button>
                                </div>
                                <!--end edit modal-->
                            </td>
                            <td> <a href='javascript:void(0)' onclick="delete_req(<?php echo $item->id ?>, '<?php echo $item->form_name ?>')" <span data-toggle='tooltip' title='delete' aria-describedby='tooltip80070' ><i class=" fa fa-2x fa-trash "></i> </span></a></td>
                        <?php } ?>
                        <!--end edit td-->

                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr class="odd">
                    <td valign="top" colspan="6" class="dataTables_empty">
                        <?php echo lang('no_matching_request_found'); ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>

<!--        <tbody id="students_requests">
</tbody> -->
    </table>
</div>

<?php bs3_card_f(); ?>


<script type="text/javascript">
    $(function () {
        get_students_requests();
    });
    function get_students_requests() {
        var URL = "<?php echo base_url('enrollment_requests/get_youth_group_registration_form/') ?>";
        $.getJSON(URL, function (data) {
            reload_table(data);
        });
    }

    function reload_table(data) {
        var output = "";
        if (data.data != null && data.data != "") {
            $.each(data.data, function (i, item) {
                output += "<tr data-id='" + item.id + "'" + "id='item-" + item.id + "'" + " >";
                output += "<td>";
                output += item.family_last_name;
                output += "</td>";
                output += "<td>";
                output += item.student_1 + ' ' + item.student_age_1 + ' ' + item.student_grade_1;
                output += "</td>";
                output += "<td>";
                output += item.student_2 + ' ' + item.student_age_2 + ' ' + item.student_grade_2;
                output += "</td>";
                output += "<td>";
                output += item.student_3 + ' ' + item.student_age_3 + ' ' + item.student_grade_3;
                output += "</td>";
                output += "<td>";
                output += item.student_4 + ' ' + item.student_age_4 + ' ' + item.student_grade_4;
                output += "</td>";
                output += "<td>";
                output += "<button value='" + item.id + "' type='button' class='btn btn-block btn-lg btn-info btn-rounded' id='del' data-toggle='modal' data-target='#delete-modal'>";
                output += "<?php echo lang('approve_registration'); ?>";
                output += "</button>";
                output += "</td>";
                output += "</tr>";
            });
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="6" class="dataTables_empty">';
            output += "<?php echo lang('no_matching_records_found'); ?>"
            output += '</td>';
            output += '</tr>';
        }

        $("#students_requests").html(output);
    }


</script>

<script>
 
   function save_btn(id) {
//        var id = $(".inputId").val();
        var grade_id_1 = $(".inputGrade_id_1").val();
        var room_id_1 = $(".inputRoom_id_1").val();
        var student_1 = $(".inputStudent_1").val();
        $.ajax({
            url: "<?php echo base_url("enrollment_requests/add_youth_group_registration_form") ?>",
            type: 'post',
            dataType: 'json',
            data: {id, grade_id_1, room_id_1, student_1},
            success: function (data) {
                if (data.status == "200") {
                    $('.update_c').hide();
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                        closeOnConfirm: false
                    });
                    $(".confirm").click(function () {
//                    $('.modal').modal('toggle');
                        $('.modal').modal('hide')
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                    // message error in controller data.message
                }
            }
        });
    }

    $(document).ready(function () {
        // ==================== for filter the rooms which student in it ===========

        $(".inputGrade_id_1").change(function () {
            var deptid = $(this).val();
            $.ajax({
                url: "<?php echo base_url("enrollment_requests/get_rooms_for_filter/youth_group_registration_form") ?>",
                type: 'post',
                data: {depart: deptid},
                dataType: 'json',
                success: function (response) {
                    var len = response.length;
                    $(".inputRoom_id_1").empty();
                    for (var i = 0; i < len; i++) {
                        var grade_id = response[i]['grade_id'];
                        var id = response[i]['room_id'];
                        var name = response[i]['room_name'];
                        if (grade_id === deptid)
                            $(".inputRoom_id_1").append("<option value='" + id + "' grade_id1='" + id + "'>" + name + "</option>");
                    }
                }
            });
        });
    });</script>
<style>
    a:hover {
        color: #0056b3!important;
    }
    a {
        color: #20aee3!important;
    }
</style>


<script>

    function delete_req(id, form_name) {
        swal({
            title: "<?php echo lang('delete_confirmation_text') ?>",
            text: "",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "<?php echo lang('close') ?>",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo lang('delete') ?>",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: "<?php echo base_url("enrollment_requests/delete_enrollment_request") ?>/" + id + "/" + form_name,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                    window.location.href = "<?php echo base_url("enrollment_requests/index_youth_group_registration_form") ?>";
                },
            });
        });
    }
</script>
