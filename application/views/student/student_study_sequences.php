<?php $student = (isset($student_info)) ? $student_info->student_name . ' ' . $student_info->family_name : ""; ?>
<?php bs3_card($page_title, $student); ?>

<?php if (isset($student_study_sequence) && $student_study_sequence) { ?>

    <div class="table-responsive m-t-40">
        <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>  <?php echo lang("grade_name"); ?> </th>
                    <th>  <?php echo lang("room_name"); ?> </th>
                    <th>  <?php echo lang("status"); ?> </th>
                    <th>  <?php echo lang("teacher_child"); ?> </th>
                    <th>  <?php echo lang("non_active_registration_date"); ?> </th>
                    <th>  <?php echo lang("photo_permission_slips"); ?> </th>
                    <th>  <?php echo lang("islamic_book_return"); ?> </th>
                    <th>  <?php echo lang("status_islamic_book_return"); ?> </th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($student_study_sequence as $item) { ?>
                    <tr>
                        <td><?php echo $item->grade_name; ?></td>
                        <td><?php echo $item->room_name; ?></td>

                        <td>
                            <?php
                            if ($item->is_active == '2')
                                echo lang("is_active");
                            elseif ($item->is_active == '3')
                                echo lang("not_active");
                            else
                                echo lang("transferred_to_another_grade");
                            ?>
                        </td>

                        <td>
                            <?php
                            if ($item->teacher_child == '2')
                                echo lang("yes");
                            else
                                echo lang("no");
                            ?>
                        </td>
                        
                        <td>
                            <?php
                            if ($item->non_active_registration_date != 'null')
                                echo $item->non_active_registration_date;
                            else
                                echo ' ';
                            ?>
                        </td>

                        <td>
                            <?php
//                            if ($item->stage_id == '2') {
                                if ($item->photo_permission_slips == '2')
                                    echo lang("yes");
                                else
                                    echo lang("no");
//                            }else {
//                                echo "-";
//                            }
                            ?>
                        </td>

                        <td>
                            <?php
//                            if ($item->stage_id == '2') {
                                if ($item->islamic_book_return == '2')
                                    echo lang("yes");
                                else
                                    echo lang("no");
//                            }else {
//                                echo "-";
//                            }
                            ?>
                        </td>

                        <td><?php
//                            if ($item->stage_id == '2') {
                                echo lang($item->status_islamic_book_return);
//                            } else {
//                                echo "-";
//                            }
                            ?>
                        </td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } else { ?>
    <div class="table-responsive m-t-40">
        <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>  <?php echo lang("grade_name"); ?> </th>
                    <th>  <?php echo lang("room_name"); ?> </th>
                    <th>  <?php echo lang("status"); ?> </th>
                    <th>  <?php echo lang("non_active_registration_date"); ?> </th>
                    <th>  <?php echo lang("photo_permission_slips"); ?> </th>
                    <th>  <?php echo lang("islamic_book_return"); ?> </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="6"><?php echo lang('no_matching_records_found') ?></td>
                </tr>
            </tbody>
        </table>
    </div>

<?php } ?>

<?php bs3_card_f(); ?>

