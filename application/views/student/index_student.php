<!--<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item fix-lang">
                <a  href="<?php echo base_url('home/home_page') ?>" ><?php echo lang('home_page') ?></a>
            </li>
          
            <li class="breadcrumb-item active fix-lang"><?php echo $page_title ?></li>
        </ol>
    </div>
</div>-->

<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?> 
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_student") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_input('student_name') ?>
    <a class="non_printable" href="<?php echo base_url('parents/index_parent'); ?>"><p style=" color: red;"><strong> <?php echo lang('note'); ?>: </strong> <?php echo lang('if_you_dont_find_parent_please_add_it_by_click_on_this_note'); ?></p></a>    
    <?php bs3_dropdown('parent_id', $family_name) ?>
    <?php // bs3_file_or_image('personal_image') ?>
    <?php // bs3_input('grade')  ?>
    <?php bs3_number('age') ?>
    <?php bs3_dropdown('gender', $gender) ?>
    <?php bs3_input('student_notes') ?>
    <?php bs3_input('non_encrept_pass', $non_encrept_pass) ?>

    <?php bs3_modal_footer('create'); ?>
<?php } ?>

<?php
//$letters = array(
//    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
//    "W", "X", "Y",
//    "Z",
//);
?>
<!--<div class="block" >
    <div class="block-title">
        <div class="block-options text-center">
<?php // foreach ($letters as $value) { ?>
                <button type="button" onclick="search_info(<?php echo $value ?>)" class="tn btn-sm btn-alt label-light-info btn-circle letter" value="<?php echo $value ?>" name="<?php echo $value ?>"><?php echo $value ?></button>
<?php // } ?>
  </div>
    </div>
</div>-->

<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>
<tbody>
</tbody>
<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>


<script type="text/javascript">
    $(function () {
//        search_info(data);
    });
    function search_info(value) {
        var value = value;
        var data = $(".letter").val();

        var url = "<?php echo base_url('student/index_students/'); ?>" + data;
//
        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {
                reload_table(data);
            },
            error: function () {
//                alert("Error");
            }
        });
    }
</script>

<style>
    .block .block-title{
        margin-top: 35px;
    }
    .letter{
        border:none!important;
    }
</style>


<script type="text/javascript">

    function edit_object(id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        save_method = 'update';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        //$('.help-block').empty(); // clear error string

        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
            var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
            if (hidden_in == "update") {
                $('[name=' + pair[0] + ']').parent().parent().hide();
            } else {
                $('[name=' + pair[0] + ']').parent().parent().show();
            }
        }

        $(show_modal_id).modal('show'); // show bootstrap modal
        //Ajax Load data from ajax
        $form = $(event.target);

        var URL = "<?php echo base_url($get_object_link_personal_image) ?>/" + id;

        $.ajax({
            url: URL,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $("form#form_crud :input").each(function (index, item) {
                    var input_name = $(this).attr('name'); // This is the jquery object of the input, do what you will
                    var input_type = $(this).attr('type');
                    if (input_type === 'file') {
                        $(".fileinput").removeClass("fileinput-new");
                        $(".fileinput").addClass("fileinput-exists");
                        $(".fileinput-preview").text(data.personal_image);
                    }
                    if (input_type != 'checkbox' && input_type != 'file') {
                        $(this).val(data[input_name]);
                    } else if (input_type == 'checkbox') {
                        if (data[input_name] == 2) {
                            $(this).prop("checked", true);
                        } else {
                            $(this).prop("checked", false);
                        }
                    }

                });

                var update_object_title = "<?php echo $update_object_title; ?>";
                $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error get data from ajax');
            }
        });
    }

</script>