<?php $student = (isset($student_info)) ? $student_info->student_name . ' ' . $student_info->family_name : ""; ?>
<?php bs3_card($page_title, $student); ?>

<?php if (isset($student_attendaces) && $student_attendaces) { ?>
    <?php foreach ($student_attendaces as $item) { ?>
        <div class="table-responsive m-t-40">
            <h5 class="text-center"><?php echo $item[0]['grade'] . ": " . $item[0]['room']; ?></h5>
            <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>  <?php echo lang('date'); ?> </th>
                        <th>  <?php echo lang('status'); ?> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($item as $value) { ?>
                        <tr>
                            <td><?php echo $value['data']; ?></td>
                            <td><?php echo $value['type']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="table-responsive m-t-40">
        <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>  <?php echo lang('date'); ?> </th>
                    <th>  <?php echo lang('status'); ?> </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2"><?php echo lang('no_matching_records_found') ?></td>
                </tr>
            </tbody>
        </table>
    </div>
  
<?php } ?>

<?php bs3_card_f(); ?>

