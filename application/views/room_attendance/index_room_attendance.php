<?php $room = (isset($room_name)) ? lang('room_id') . ': ' . $room_name->room_name . '<br><br>' . $room_name->grade_name : ""; ?>
<?php bs3_card($page_title, $room); ?>
<br>
<?php echo form_open_multipart('', 'class="form-horizontal" role="form" id="create_form"'); ?>
<?php echo bs3_date("attendance_date", $day_date); ?>
<div class="table-responsive m-t-40">
    <table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">
        <thead>
                <tr>
                    <th>  <?php echo lang('student_name'); ?> </th>
                    <th> <?php echo lang('attendance'); ?> </th>
                    <th>  <?php echo lang('absence'); ?></th>
                    <th>  <?php echo lang('delay'); ?></th>
                </tr>
            </thead>
            <thead>
                <tr>
                    <th>  <?php echo lang('select_all'); ?> </th>
                    <th> <input name="attendance_check" type="radio" class="all_attendance" />  </th>
                    <th>  <input name="attendance_check" type="radio" class="all_absence" /> </th>
                    <th> <input name="attendance_check" type="radio" class="all_delay" /> </th>
                </tr>


            </thead>
        <tbody id="attendances">
        </tbody> 
    </table>
</div>
<?php if ($_current_year == $_archive_year) { ?>
    <button type="button" style="margin-top: 26px;" onclick="save()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>

<?php } ?>
<br>
<?php bs3_card_f(); ?>

<style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
        right: 54px!important;
    }
    [type="radio"]:not(:checked), [type="radio"]:checked {
        position: absolute;
        left: inherit !important;
        opacity: 1 ;
    }
</style>  
<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>


<script type="text/javascript">

    $(function () {
        get_attendance_data();
    });
    function get_attendance_data() {

        var room_id = <?php echo $room_id ?>;
        var URL = "<?php echo base_url('room_attendance/get_all_students/') ?>" + room_id;
        $.getJSON(URL, function (data) {
            reload_table(data);
        })

    }

    function reload_table(data) {
        var output = "";
        var current_year =<?php echo $_current_year ?>;
        var archive_year =<?php echo $_archive_year ?>;

        if (data.data != null && data.data != "") {
            $.each(data.data, function (i, item) {
//                var stu_url = "<?php echo base_url('profile/page_student_profile/') ?>" + item.student_id;
                output += "<tr>";
                output += "<td> <a href= '";
                output += "<?php echo base_url('profile/page_student_profile/') ?>" + item.student_id;
                output += " ' >";
                output += item.name;
                output += "</a></td>";
//                output += '<td> <a href= '+stu_url;
//                 output += '>';
//                output += "<td>";
//                output += item.name;
//                output += "</td>";
//                output += "</a></td>";
                output += "<td>";
                if (current_year === archive_year) {
                    output += '<input type="radio" class="attend" name="attendance' + item.id + '" value="attend" ';
                } else {
                    output += '<input disabled="true"  class="attend" type="radio" name="attendance' + item.id + '" value="attend" ';
                }
                if (item.attend == 1) {
                    output += "checked";
                }
                output += ">"
                output += "</td>";
                output += "<td>";
                if (current_year === archive_year) {
                    output += '<input type="radio" class="absence" name="attendance' + item.id + '" value="absence" ';
                } else {
                    output += '<input  disabled="true"  class="absence" type="radio" name="attendance' + item.id + '" value="absence" ';
                }
                if (item.absence == 1) {
                    output += "checked";
                }
                output += ">"
                output += "</td>";
                output += "<td>";
                if (current_year === archive_year) {
                    output += '<input type="radio" class="delay" name="attendance' + item.id + '" value="delay" ';
                } else {
                    output += '<input disabled="true" class="delay" type="radio" name="attendance' + item.id + '" value="delay" ';
                }
                if (item.delay == 1) {
                    output += "checked";
                }
                output += ">"
                output += "</td>";
                output += "</tr>";
            });
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="4" class="dataTables_empty">';
            output += "<?php echo lang('no_matching_records_found'); ?>"
            output += '</td>';
            output += '</tr>';
            $(".btnSave").hide();
        }

        $("#attendances").html(output);
    }

    $("#inputAttendance_date").change(function () {
        var date = $(this).val();
        var room_id = <?php echo $room_id ?>;
        var URL = "<?php echo base_url('room_attendance/get_all_students/') ?>" + room_id;
        $.ajax({
            url: URL,
            type: "post",
            data: {date: date},
            dataType: "json",
            success: function (data) {
                reload_table(data);
            },
            error: function () {
//                alert("errors");
            }
        });
    });
    function save() {
        var data = $("#create_form").serialize();
        var room_id = <?php echo $room_id ?>;
        var URL = "<?php echo base_url('room_attendance/update_attendace_values/') ?>" + room_id;
        $.ajax({
            url: URL,
            type: "post",
            data: data,
            dataType: "json",
            success: function (data) {
                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                }
            },
            error: function () {
//                alert('error');
            }
        });
    }



</script>

<style>
    [type="radio"]:not(:checked), [type="radio"]:checked {
        position: unset !important; 
    }
</style>
<script>
    //for check all
    $(document).ready(function () {

        $('.all_attendance').change(function () {
            if ($('.all_attendance').is(':checked')) {
                $(".attend").prop('checked', true);
                $(".all_attendance").prop('checked', false);
            } else {
                $(".attend").prop('checked', false);
            }
        });
        $('.attend').change(function () {
            var all = false;
            $(".attend").each(function (i) {
                if ($(this).is(':checked')) {
                    all = true;
                } else {
                    all = false;
                    return all;
                }
            });
            if (all) {
                $(".all_attendance").prop('checked', true);
            } else {
                $(".all_attendance").prop('checked', false);
            }
        });


        $('.all_absence').change(function () {
            if ($('.all_absence').is(':checked')) {
                $(".absence").prop('checked', true);
                $(".all_absence").prop('checked', false);
            } else {
                $(".absence").prop('checked', false);
            }
        });
        $('.absence').change(function () {
            var all = false;
            $(".absence").each(function (i) {
                if ($(this).is(':checked')) {
                    all = true;
                } else {
                    all = false;
                    return all;
                }
            });
            if (all) {
                $(".all_absence").prop('checked', true);
            } else {
                $(".all_absence").prop('checked', false);
            }
        });

        $('.all_delay').change(function () {
            if ($('.all_delay').is(':checked')) {
                $(".delay").prop('checked', true);
                $(".all_delay").prop('checked', false);
            } else {
                $(".delay").prop('checked', false);
            }
        });
        $('.delay').change(function () {
            var all = false;
            $(".delay").each(function (i) {
                if ($(this).is(':checked')) {
                    all = true;
                } else {
                    all = false;
                    return all;
                }
            });
            if (all) {
                $(".all_delay").prop('checked', true);
            } else {
                $(".all_delay").prop('checked', false);
            }
        });

    });
</script>

<style>
    [type="radio"]:not(:checked).all_attendance, [type="radio"]:checked.all_attendance {
        position: inherit!important;
    }
    [type="radio"]:not(:checked).all_absence, [type="radio"]:checked.all_absence {
        position: inherit!important;
    }
    [type="radio"]:not(:checked).all_delay, [type="radio"]:checked.all_delay {
        position: inherit!important;
    }
    [type="radio"]:not(:checked).attend, [type="radio"]:checked.attend {
        position: inherit!important;
    }
    [type="radio"]:not(:checked).absence, [type="radio"]:checked.absence {
        position: inherit!important;
    }
    [type="radio"]:not(:checked).delay, [type="radio"]:checked.delay {
        position: inherit!important;
    }
</style>  
