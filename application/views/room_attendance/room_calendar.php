<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/assets/dist_calender/css/pignose.calendar.min.css"/>
<style>
    .event-box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
        margin-bottom: 10px;
    }

    .pignose-calendar-unit a {
        color: #003a7a !important;
    }
    .pignose-calendar .pignose-calendar-unit.pignose-calendar-unit-active a {
        background-color: #4970c0 !important;
    }
    .pignose-calendar .pignose-calendar-header .pignose-calendar-week.pignose-calendar-week-sat, .pignose-calendar .pignose-calendar-header .pignose-calendar-week.pignose-calendar-week-sun{
        color: #4970c0 !important;
    }
    .pignose-calendar-top-date{
        color: #fff !important;
    }
    .pignose-calendar-header {
        color: #aaaaab !important;
    }


    .static-box {
        background-color: #4970c0 !important;
        height: 140px;
        color: #ffffff;
    }

    .content-header {
        color: #aaaaab !important;
    }

    .middle{
        text-align: center;
    }
    .middle-span{
        display: inline-block;
        vertical-align: middle;
        line-height: normal;
    }
    .image-box {
        width: 100%;
        height: 140px;
    }
    .button-style {
        display: inline-block;
        margin: 20px 0;
        border-radius: 4px;
        /*width: 344px;*/
    }
    @media (min-width: 576px) and (max-width: 767px) {

    }
    @media (min-width: 768px) and (max-width: 991px) {

    }
    @media (min-width: 1200px) {
        .button-style {
        <?php if($lang == "ar") {?>
            margin-right: -26px;
        <?php } else {?>
            margin-left: -26px;
        <?php } ?>
        }

        .pignose-calendar {
            max-width: none !important;
            <?php if($lang == "ar") {?>
                margin-right: -26px;
            <?php } else {?>
                margin-left: -26px;
            <?php } ?>
        }

        .events {

        <?php if($lang == "ar") {?>
            margin-left: -33px;
        <?php } else {?>
            margin-right: -33px;
        <?php } ?>


        }

    }



</style>
<?php bs3_card($page_title); ?>
<section class="bread-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3 style="color: #4970c0;
                    margin-bottom: 0px;">
                    <?php echo strtoupper(lang('events')); ?>
                </h3>
            </div>
            <div class="col-sm-6 hidden-xs <?php if ($lang == 'ar'): ?>text-left<?php else: ?>text-right<?php endif; ?>">
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url(''); ?>"><?php echo lang('home'); ?></a></li>
                    <li style="color: #4970c0!important; "><?php echo lang('events'); ?></li>
                </ol><!--breadcrumb-->
            </div>
        </div>
    </div>
</section><!--.bread-wrap-->
<div class="container" style="margin-top: 50px; margin-bottom: 25px;">
    <div class="col-md-12">
        <div class="col-md-5 calendar" style="margin-bottom: 15px"></div>
        <div class="col-md-7">
            <div class="events" style="background-color: #eeeeee;">
                <!--            --><?php //if ($data && !empty($data)) { ?>
                <!--                --><?php //foreach ($data as $item) { ?>
                <!--                    --><?php //if ($item->external_url != "") { ?>
                <!--                        <a href="--><?php //echo http_check($item->external_url); ?><!--">-->
                <!--                        --><?php //} else if ($item->url != "") { ?>
                <!--                            <a href="--><?php //echo base_url('index.php/site/article') . '/' . $item->url ?><!--">-->
                <!--                            --><?php //} else { ?>
                <!--                                <a href="#">-->
                <!--                                --><?php //} ?>
                <!--                                <div class ="event-box col-md-12" style="    background-color: #eeeeee;">-->
                <!--                                    <div class="static-box middle col-md-3">-->
                <!--                                        <span class="middle-span">-->
                <!--                                            --><?php //echo $item->date ?>
                <!--                                        </span>-->
                <!--                                    </div>-->
                <!--                                    <div class="col-md-5">-->
                <!--                                        <h4>-->
                <!--                                            --><?php //echo $item->{"content_$lang"} ?>
                <!--                                        </h4>-->
                <!--                                        <p style="color: #8b8b9a; ">-->
                <!--                                            --><?php //echo $item->{"description_$lang"} ?>
                <!--                                        </p>-->
                <!--                                    </div>-->
                <!--                                    <div class="col-md-4">-->
                <!--                                        <img-->
                <!--                                        --><?php //if ($item->img != "") { ?>
                <!--                                                class="image-box"-->
                <!--                                                src="--><?php //echo base_url('assets/uploads/events/') . '/' . $item->img ?><!--"-->
                <!--                                            --><?php //} ?>
                <!--                                            >-->
                <!--                                    </div>-->
                <!--                                </div>-->
                <!--                            </a>-->
                <!--                        --><?php //} ?>
                <!--                    --><?php //} else { ?>
                
                <!--                    --><?php //} ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-5">
            <!--<a href="<?php echo base_url('index.php/site/all_events')?>" class="btn btn-primary btn-block button-style col-md-offset-2" style="margin-bottom: 10px;"><?php echo lang('all_events');?></a>-->
        </div>
        <div class="col-md-7 text-center">
            <div id="paginationholder"></div>
        </div>
    </div>
</div>
<?php bs3_card_f(); ?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/assets/dist_calender/js/pignose.calendar.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/assets/dist_calender/js/date.format.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/assets/dist_calender/js/jquery.twbsPagination.min.js"></script>

<script type="text/javascript">
    //<![CDATA[
    $(function () {
        $('#wrapper .version strong').text('v' + $.fn.pignoseCalendar.version);
        var divHieght = $('.static-box').height();
        $('.static-box').css('line-height', divHieght + "px");

        var page = 1;
        var current_page = 1;
        var current_view_month;
        var total_page = 0;
        var is_ajax_fire = 0;
        var pagination;
        manageData();

        /* manage data list */
        function manageData() {

            var url = '<?php echo base_url('room_attendance/get_events')?>'

            $.getJSON(url, function(data) {
                total_page = Math.ceil(data.data.total/3);
                current_page = page;
                $('#paginationholder').html('');
                $('#paginationholder').html('<ul id="pagination" class="pagination-sm"></ul>');
                $('#pagination').twbsPagination({
                    totalPages: total_page,
                    visiblePages: current_page,
                    onPageClick: function (event, pageL) {
                        page = pageL;
                        if(is_ajax_fire != 0){
                            getPageData(data.data.month);
                        }
                    }
                });

                manageBox(data);
                is_ajax_fire = 1;
            }).fail(function(jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + textStatus); });

        }

        function getPageData(month) {
            var url = '<?php echo base_url('index.php/site/get_events/')?>';
            $.ajax({
                dataType: 'json',
                url: url,
                data: {page:page,dataMonth:month}
            }).done(function(data){
                manageBox(data);
            });
        }

        function toDate(dateStr) {
            var newDate = new Date(dateStr);
            return newDate;
        }

        function manageBox(data) {
            var resutls = data.data.result;
            var output = "";
            var lang = "<?php echo $lang; ?>";
            if (data.status == "200") {
                console.log("hohohoh");
                for (var item in resutls) {
                    if (resutls[item]["external_url"] != "http://") {
                        console.log(resutls[item]["external_url"]);
                        output += '<a href="';
                        output += resutls[item]["external_url"];
                        output += '">'
                    } else if (resutls[item]["url"] != "" && resutls[item]["url"] != null) {
                        var url = "<?php echo base_url('index.php/site/article/') ?>";
                        output += '<a href="<?php echo base_url('index.php/site/article/') ?>';
                        output += '/' + resutls[item]["url"];
                        output += '">';

                    }
                    else {
                        output += '<a href="#">';
                    }
                    output += '<div class ="event-box col-md-12">';
                    output += '<div class="static-box middle col-md-3">';
                    output += '<span class="middle-span">';
                    var dateObj = toDate(resutls[item]["date"]);
                    output += dateObj.format("dddd mmmm yyyy");
                    output += '</span>';
                    output += '</div>';
                    output += '<div class="col-md-5">';
                    output += '<h4>';
                    output += resutls[item]["content_" + lang];
                    output += '</h4>';
                    output += '<p>';
                    output += resutls[item]["description_" + lang];
                    output += '</p>';
                    output += '</div>';
                    output += '<div class="col-md-4">';
                    output += '<img';

                    if (resutls[item]["img"] != "") {
                        output += ' class="image-box" src="';
                        output += "<?php echo base_url('assets/uploads/events/') ?>";
                        output += '/' + resutls[item]["img"] + '"';
                    }
                    output += '>';
                    output += '</div>';
                    output += '</div>';
                    output += '</a>';
                }
            } else {
                output += '<div class ="event-box">';
                output += '<h4>';
                output += '<?php echo lang('no_events_found'); ?>';
                output += '</h4>';
                output += '<br>';
            }
            $(".events").html(output);
            $(".events").fadeIn(2000);
            var divHieght = $('.static-box').height();
            $('.static-box').css('line-height', divHieght + "px");
        }

        function onSelectHandler(date, context) {
            /**
             * @date is an array which be included dates(clicked date at first index)
             * @context is an object which stored calendar interal data.
             * @context.calendar is a root element reference.
             * @context.calendar is a calendar element reference.
             * @context.storage.activeDates is all toggled data, If you use toggle type calendar.
             * @context.storage.events is all events associated to this date
             */
                // $(".events").css("display", "none");
            var $element = context.element;
            var $calendar = context.calendar;
            var $box = $element.siblings('.box').show();
            var text = '';
            if (date[0] !== null) {
                text += date[0].format('YYYY-MM-DD');
            }

            if (date[0] !== null && date[1] !== null) {
                text += ' ~ ';
            }
            else if (date[0] === null && date[1] == null) {
                text += 'nothing';
            }

            if (date[1] !== null) {
                text += date[1].format('YYYY-MM-DD');
            }

            //var keyValue = {"date_test":text};
            //var calenderUrl = "<?php //echo base_url('index.php/site/get_events/'); ?>//";
            //var lang = "<?php //echo $lang; ?>//";
            //var sendDate = $.ajax({
            //    url: calenderUrl,
            //    type: "POST",
            //    data: {date:text},
            //    dataType: "json",
            //    success: function (data) {
            //
            //    var resutls = data.data;
            //            var output = "";
            //            if (data.status == "200") {
            //    for (var item in resutls) {
            //    console.log(resutls[item]["content_en"]);
            //            if (resutls[item]["external_url"] != "") {
            //    output += '<a href="';
            //            output += resutls[item]["external_url"];
            //            output += '">'
            //    } else if (resutls[item]["url"] != "") {
            //    output += '<a href="<?php //echo base_url('index.php/site/article/') ?>//';
            //            output += '/' + resutls[item]["url"];
            //            output += '">';
            //    }
            //    else {
            //    output += '<a href="#">';
            //    }
            //    output += '<div class ="event-box col-md-12">';
            //            output += '<div class="static-box middle col-md-3">';
            //            output += '<span class="middle-span">';
            //            var dateObj = toDate(resutls[item]["date"]);
            //            output += dateObj.format("dddd mmmm yyyy");
            //            output += '</span>';
            //            output += '</div>';
            //            output += '<div class="col-md-5">';
            //            output += '<h4>';
            //            output += resutls[item]["content_" + lang];
            //            output += '</h4>';
            //            output += '<p>';
            //            output += resutls[item]["description_" + lang];
            //            output += '</p>';
            //            output += '</div>';
            //            output += '<div class="col-md-4">';
            //            output += '<img';
            //            if (resutls[item]["img"] != "") {
            //    output += 'class="image-box" src="';
            //            output += "<?php //echo base_url('assets/uploads/events/') ?>//";
            //            output += '/' + resutls[item]["img"] + '"';
            //    }
            //    output += '>';
            //            output += '</div>';
            //            output += '</div>';
            //            output += '</a>';
            //    }
            //    } else {
            //    output += '<div class ="event-box">';
            //            output += '<h4>';
            //            output += '<?php //echo lang('no_events_found'); ?>//';
            //            output += '</h4>';
            //            output += '<br>';
            //    }
            //    $(".events").html(output);
            //            $(".events").fadeIn(2000);
            //            var divHieght = $('.static-box').height();
            //            $('.static-box').css('line-height', divHieght + "px");
            //    },
            //});
            //sendDate.error(function() {
            //alert("somethig went error.");
            //});
            //$("#test").text(text);
        }

        function onSelectPrevNext(data, context) {
            page = 1;
            current_page = 1;
            total_page = 0;
            is_ajax_fire = 0;
            var month = data.month;
            console.log(data.month);

            var url = '<?php echo base_url('index.php/site/get_events/')?>';
            $.ajax({
                dataType: 'json',
                url: url,
                data: {dataMonth:month}
            }).done(function(data){
                if(data.data.total > 0) {
                    total_page = Math.ceil(data.data.total/3);
                    current_page = page;
                    $('#paginationholder').html('');
                    $('#paginationholder').html('<ul id="pagination" class="pagination-sm"></ul>');
                    $('#pagination').twbsPagination({
                        totalPages: total_page,
                        visiblePages: current_page,
                        onPageClick: function (event, pageL) {
                            page = pageL;
                            if(is_ajax_fire != 0){
                                getPageData(data.data.month);
                            }
                        }
                    });
                }else {
                    $('#paginationholder').html('');
                }

                manageBox(data);
                is_ajax_fire = 1;
            });

        }

        // Default Calendar
        $('.calendar').pignoseCalendar({
            select: onSelectHandler,
            prev : onSelectPrevNext,
            next:  onSelectPrevNext,
        });

    });

    //]]>
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/prism.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/components/prism-javascript.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/components/prism-typescript.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/components/prism-json.min.js"></script>
<script type="text/javascript" src="https://twemoji.maxcdn.com/2/twemoji.min.js?2.5"></script>

