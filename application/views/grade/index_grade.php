<!--<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item fix-lang">
                <a  href="<?php echo base_url('home/home_page') ?>" ><?php echo lang('home_page') ?></a>
            </li>

            <li class="breadcrumb-item active fix-lang"><?php echo $page_title ?></li>
        </ol>
    </div>
</div>-->

<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_grade") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_dropdown('stage_id', $stages_options); ?>
    <?php bs3_input('grade_name', '', false, false, 'enter_from_top_class_to_lower') ?>

    <?php // bs3_dropdown('next_grade_name', $next_grade_name_options); ?>

    <div class="form-group row" >
        <label for="inputNext_grade_name" class="col-sm-4 control-label "><?php echo lang('next_grade_name'); ?>:</label>
        <div class="col-sm-8">
            <select id="next_grade_name" name="next_grade_name" class="form-control">
                <option value="">Select next_grade_name</option>
            </select>
        </div>
    </div>

    <?php bs3_dropdown('report_card_name', $report_card_name_options); ?>

    <?php $name = 'grade_type' ?>
    <div class="form-group row" id="form<?php echo ucfirst($name); ?>">
        <?php if (lang($name)): ?>
            <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 control-label"><?php echo lang($name); ?>:</label>
        <?php else: ?>
            <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 control-label "></label>
        <?php endif; ?>
        <div class="col-sm-8">
            <input type="radio" name="<?php echo $name; ?>" value="youth_group" id="input<?php echo ucfirst($name); ?>" class="input<?php echo ucfirst($name); ?>"/>
            <?php echo lang('grade_youth_group'); ?>
            <br>
            <input type="radio" name="<?php echo $name; ?>" value="book_club" id="input<?php echo ucfirst($name); ?>" class="input<?php echo ucfirst($name); ?>" />
            <?php echo lang('grade_book_club'); ?>
            <br>
            <input type="radio" name="<?php echo $name; ?>" value="saturday_school"  id="input<?php echo ucfirst($name); ?>" class="input<?php echo ucfirst($name); ?>" />
            <?php echo lang('grade_saturday_school'); ?>

        </div>
    </div>
    <?php bs3_modal_footer('create'); ?>

<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>


<script>

    updateSelect();
    function updateSelect() {
        $.ajax({
            url: " <?php echo base_url("grade/get_next_grade_info") ?>",
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                $('#next_grade_name').html("");
                $.each(data['data'], function (key, value) {
                    var option = $('<option />');
                    option.attr('value', key).text(value);
                    $('#next_grade_name').append(option);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    }
<?php if (isset($add_object_link_grade) || isset($update_object_link_grade)) { ?>
        function save() {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable
            var url;
            if (save_method == 'add') {
                url = "<?php echo site_url($add_object_link_grade) ?>";
            } else {
                url = "<?php echo site_url($update_object_link_grade) ?>";
            }
            add_update_grade(url);
        }
<?php } ?>
    function add_update_grade(url) {
        // ajax adding data to database
        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
//            console.log($('[name=' + pair[0] + ']').attr('name'));
//            console.log($('[name=' + pair[0] + ']').attr('type'));
        }

        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                updateSelect();
                if (data.status) //if success close modal and reload ajax table
                {
                    get_response(data);
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                var save = '<?php echo lang('save') ?>';
                $('.btnSave').text(save); //change button text
                $('.btnSave').attr('disabled', false); //set button enable
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error adding / update data');
                var save = '<?php echo lang('save') ?>';
                $('.btnSave').text(save); //change button text
                $('.btnSave').attr('disabled', false); //set button enable
            }
        });
    }

</script>

