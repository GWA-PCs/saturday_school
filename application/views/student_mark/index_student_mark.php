<?php $room_info = (isset($room)) ? lang('room_id') . ': ' . $room->room_name . ' - ' . $room->grade_name . '<br>' : ""; ?>
<?php $subject_info = (isset($subject)) ? lang('subject_id') . ': ' . $subject->subject_name . '<br>' : ""; ?>
<?php $semester_info = lang('semester') . ': ' . lang($semester) ?>
<?php $sub_title = $room_info . '<br>' . $subject_info . '<br>' . $semester_info; ?>
<?php bs3_card($page_title, $sub_title); ?>

<?php echo form_open("", "id='form_crud2'" . " role='form'   enctype= 'multipart/form-data'"); ?>


<?php bs3_table($thead, 'object_table') ?>
<div><strong style="color:red; font-size: 16px; ">  <b>Note:</b> You should press <b>Save</b> button for every page  </strong></div>
<br>
    <?php // bs3_table_without_pagination($thead, 'mytable_without_pag') ?>
<?php bs3_table_f() ?>
<?php if ($_current_year == $_archive_year) { ?>
    <button type="button" onclick="edit_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>
<?php } ?>
<?php bs3_card_f(); ?>

<script>
    function edit_info() {
        var data = $("#form_crud2").serialize();

        var url = "<?php echo base_url('student_mark/add_student_mark/' . $semester . '/' . $room_id . '/' . $subject_id); ?>";

        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {

                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });

    }
</script>
