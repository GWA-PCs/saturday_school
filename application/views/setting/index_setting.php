<div class="row">
    <?php $i = $color_index = 6; ?>
    <?php bs3_card_wid_colored(base_url('setting/determine_current_year'), "mdi mdi-marker-check", lang("determine_current_year"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('setting/archive_year'), "mdi mdi-folder-multiple", lang("archive_year"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('setting/determine_school_contact_email'), "mdi mdi-email-variant", lang("determine_school_contact_email"), false, $color_wid[$color_index++], 100, $text_align); ?>
    <?php bs3_card_wid_colored(base_url('setting/determine_certificate_date'), "mdi mdi-calendar-text", lang("determine_certificate_date"), false, $color_wid[$color_index++], 100, $text_align); ?>

</div>