<?php bs3_card($page_title, FALSE, FALSE); ?>


<?php if ($_current_year == $_archive_year) { ?>
    <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
    <?php if (isset($grades) && $grades) { ?>
        <?php foreach ($grades as $value) { ?>
            <?php if (isset($certificates_dates_array) && $certificates_dates_array && isset($certificates_dates_array[$value->id])) { ?>
                <?php bs3_date_for_certificae_setting('date_of_expired', $value->id, $value->grade_name, $certificates_dates_array[$value->id]); ?>

            <?php } else { ?>
                <?php bs3_date_for_certificae_setting('date_of_expired', $value->id, $value->grade_name); ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    <button type="button" onclick="edit_info()" class="btn btn-info text-left btnSave btn-rounded hvr-icon-spin hvr-shadow" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>
<?php } else { ?>
    <?php if (isset($grades) && $grades) { ?>
        <?php foreach ($grades as $value) { ?>
                 <?php if (isset($certificates_dates_array) && $certificates_dates_array && isset($certificates_dates_array[$value->id])) { ?>
                <?php bs3_date_for_certificae_setting('date_of_expired', $value->id, $value->grade_name, $certificates_dates_array[$value->id],false, 'disabled=true'); ?>

            <?php } else { ?>
                <?php bs3_date_for_certificae_setting('date_of_expired', $value->id, $value->grade_name,'',false, 'disabled=true'); ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>
<?php } ?>
<?php bs3_card_f(); ?>
<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<script>
    function edit_info() {
        var data = $("#form_crud1").serialize();
        var url = "<?php echo base_url('setting/edit_certificate_date'); ?>";

        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {

                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });

    }
</script>
