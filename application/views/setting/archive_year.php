<?php
$page_title = $page_title . ': ' . $_current_year . '-' . ($_current_year + 1);
?>
<?php //  $sub_title ='" '.$_archive_year . '-' . ($_archive_year + 1).' "'; ?>
<?php // bs3_card($page_title,$sub_title,FALSE);  ?>
<?php bs3_card($page_title, '', FALSE); ?>
<br>
<?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php echo bs3_dropdown('year', $years, $_archive_year) ?>
<button type="button" onclick="edit_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
<?php echo form_close(); ?>
<?php bs3_card_f(); ?>
<style>
    @media (min-width: 576px){
        .col-sm-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-sm-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<script>
    function edit_info() {
        var data = $("#form_crud1").serialize();
        var url = "<?php echo base_url('setting/edit_archive_year'); ?>";

        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {
                console.log(data.data);
                if (data.status == "200") {

                    var nextYear = parseInt(data.data) + 1;

                    $(".academic_year").text('<?php echo lang('academic_year')?>'+': '+ data.data + '-' + nextYear );
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });

    }
</script>
