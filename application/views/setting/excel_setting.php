<?php bs3_card($page_title, FALSE, FALSE); ?>
<?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>

<?php bs3_input('header1', $header1->setting_value); ?>
<?php bs3_input('header2', $header2->setting_value); ?>
<?php bs3_input('header3', $header3->setting_value); ?>
<?php bs3_file_or_image('header_logo', "header_logo/" . $logo->setting_value); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <button type="button" onclick="edit_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>
<?php } ?>
<?php bs3_card_f(); ?>

<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<script>
    function edit_info() {
        var formData = new FormData($('#form_crud1')[0]);
        var url = "<?php echo base_url('setting/update_excel_setting'); ?>";
        var inputFileName = $(".fileinput-preview").text();
        if (inputFileName != "") {
            formData.append('fileName', inputFileName);
        }
        $.ajax({
            url: url,
            dataType: "json",
            data: formData,
            type: "post",
            contentType: false,
            processData: false,
            success: function (data) {

                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                } else if (data.status == "201") {
                    // valedation error data.data
                    messages_error("<?php echo lang('error'); ?>", data.data);
                }
            },
            error: function () {
                alert("Error");
            }
        });

    }
</script>
