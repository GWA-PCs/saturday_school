<body class="fix-header card-no-border fix-sidebar">  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-xlg-4 col-md-4 col-xs-4">
                <?php $this->load->view('profile/tab_student_card'); ?>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-9 col-xlg-8 col-md-8 col-xs-8">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs customtab2" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab"  href="#info" role="tab"><?php echo lang('info') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab"  href="#parent_tab" role="tab"><?php echo lang('parent') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attendances_tab" role="tab"><?php echo lang('attendances') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#note_tab" role="tab"><?php echo lang('student_notes') ?> </a> </li> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#allergy_tab" role="tab"><?php echo lang('index_student_allergy') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#study_sequence_tab" role="tab"><?php echo lang('study_sequence') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#report_card_tab" role="tab"><?php echo lang('report_card') ?> </a> </li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">

                                    <div class="box">


                                        <?php if ($user->type == "admin" || $user->type == "student"): ?>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5><?php echo lang("username") . ': '; ?></h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 id="UserName"></h5>
                                                </div>
                                            </div>


                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5><?php echo lang("passowrd") . ': '; ?></h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 id="password"></h5>
                                                </div>
                                            </div>
                                        <?php endif; ?>


                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><?php echo lang("age") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="age"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><?php echo lang("gender") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="gender"></h5>
                                            </div>
                                        </div>
                                        <?php if (isset($user_login) && $user_login && ($user_login->type != "teacher" )) { ?> 

                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5><?php echo lang("balance_student") . ": "; ?></h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 id="balance_student"></h5>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><?php echo lang("current_classes") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="current_classes"></h5>
                                            </div>
                                        </div>






                                        <!--<br>-->
                                        <!--                                        <div class="row">
                                                                                    <div class="col-md-4">
                                                                                        <h5><?php echo lang("custom") . ': '; ?></h5>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <h5 id="custom"></h5>
                                                                                    </div>
                                                                                </div>-->
                                        <!--                                                                                <div class="row">
                                                                                                                            <div class="col-md-6">
                                                                                                                                <h5><?php echo lang("status") . ': '; ?></h5>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <h5 id="status"></h5>
                                                                                                                            </div>
                                                                                                                        </div>-->
                                        <!--<br>-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="parent_tab" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">

                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <h5 class=""><?php echo lang("father_name"); ?> </h5>
                                                <?php if ($user->type != "student") { ?>
                                                    <a  href="<?php echo base_url("profile/page_parent_profile/") . $students->user_parent_id; ?>" >
                                                        <h5 style="color:#20aee3" id="father_name"></h5>
                                                    </a>
                                                <?php } else { ?>
                                                    <h5 id="father_name"></h5>
                                                <?php } ?>
                                            </div>

                                            <div class="col-md-5">
                                                <h5 class=""><?php echo lang("father_phone"); ?> </h5>
                                                <h5 id="father_phone"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <h5 class=""><?php echo lang("mother_name"); ?> </h5>
                                                <?php if ($user->type != "student") { ?>
                                                    <a href="<?php echo base_url("profile/page_parent_profile/") . $students->user_parent_id; ?>" >
                                                        <h5 style="color:#20aee3" id="mother_name"></h5>
                                                    </a>
                                                <?php } else { ?>
                                                    <h5 id="mother_name"></h5>
                                                <?php } ?>
                                            </div>

                                            <div class="col-md-5">
                                                <h5 class=""><?php echo lang("mother_phone"); ?> </h5>
                                                <h5 id="mother_phone"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <h5 class=""><?php echo lang("home_phone"); ?> </h5>
                                                <h5 id="home_phone"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5 class=""><?php echo lang("email"); ?> </h5>
                                                <h5 id="email"></h5>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--second tab-->
                        <div class="tab-pane" id="attendances_tab" role="tabpanel">
                            <div class="card-body">
                                <div id="attendances"></div>
                            </div>
                        </div>

                        <!--note tab-->
                        <div class="tab-pane" id="note_tab" role="tabpanel">
                            <div class="card-body">
                                <div id="notes-div"></div>
                            </div>
                        </div>

                        <!--note tab-->
                        <div class="tab-pane" id="allergy_tab" role="tabpanel">
                            <div class="card-body">
                                <div id="allergies-div"></div>
                            </div>
                        </div>

                        <div class="tab-pane" id="study_sequence_tab" role="tabpanel">
                            <div class="card-body">
                                <div id="student_study_sequences"></div>
                            </div>
                        </div>

                        <div class="tab-pane" id="report_card_tab" role="tabpanel">
                            <div class="card-body">
                                <div id="report_card_div"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
        </div> 
    </div> 
</body>

<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
</style>


<script>
    $(document).ready(function () {

        getInfo();
        get_student_balance();
        getAttendances_info();
        get_record_info();
        getNotes();
        getAllergies();
        getStudent_study_sequences_info();
        getStudentcard();
        get_user();
    });





    function get_user()
    {
        var user_id = "<?php echo $students->user_id; ?>";
        if (user_id)
        {
            var URL = "<?php echo base_url("student/get_user/") ?>" + user_id;

            $.ajax({
                url: URL,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                    var password = data.data.non_encrept_pass;
                    var userName = data.data.username;
                    $("#UserName").text(userName);
                    $("#password").text(password);

                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert("Error");
                }
            });
        }
    }






    function get_student_balance() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("student_payment/total_student_payment_and_balance/") ?>" + student_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var Balance_student = data.data.balance_student;
                $("#balance_student").text(Balance_student);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function get_record_info() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_students_records_info/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            var classes = "";
            //            var age = data.data[0].age;
            var status = "";
            $.each(data.data, function (i, item) {
                if (item.is_active === '2') {
                    classes += item.grade_name + " - " + item.room_name;
                    classes += '<br>';
                    classes += '<br>';
                }
                if (item.is_active === '2') {
                    status += item.grade_name + ":" + "<?php echo lang("is_active"); ?>" + " ";
                    status += '<br>';
                    status += '<br>';
                } else {
                    status += item.grade_name + ":" + "<?php echo lang("not_active"); ?>" + " ";
                    status += '<br>';
                    status += '<br>';
                }
            });

            $("#current_classes").html(classes);

            $("#status").html(status);
        });

        var URL2 = "<?php echo base_url("profile/profile_student_data/") ?>" + student_id;
        $.ajax({
            url: URL2,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var age = data.data.age;
                $("#age").text(age);

                var gender = data.data.gender;
                $("#gender").text(gender);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function getInfo() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/profile_student_data/") ?>" + student_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.student_name + " " + data.data.father_name + " " + data.data.family_name;

                if (data.data.father_name === null) {
                    var fatherName = "---" + " " + data.data.family_name;
                } else {
                    var fatherName = data.data.father_name + " " + data.data.family_name;
                }
                var fatherPhone = data.data.father_phone;
                var motherPhone = data.data.mother_phone;
                var motherName = data.data.mother_name;
                var homePhone = data.data.home_phone;
                var Email = data.data.email;


                $("#name").text(fullName);
                $("#father_name").text(fatherName);
                $("#mother_name").text(motherName);
                $("#mother_phone").text(motherPhone);
                $("#father_phone").text(fatherPhone);
                $("#home_phone").text(homePhone);
                $("#email").text(Email);


            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });

    }

    function getAttendances_info() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_student_attendances_info/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            if (data.data.length !== 0) {
                $.each(data.data, function (i, item) {
                    $("#attendances").append(showAttendanceTabel(item));
                });
            } else {
                var output = "";
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('no_attendance'); ?>"
                output += "</h4>";
                output += "</div>";
                $("#attendances").html(output);
            }
        });
    }

    function showAttendanceTabel(data) {
        var output = "";
        var student_id = <?php echo $student_id; ?>;
        if (data != null && data != "") {
            var gradeName = data[0].grade;
            var roomName = data[0].room;
            output += '<div class="table-responsive m-t-40">';
            output += '<h4 class="text-center">';
            output += gradeName;
            output += ": ";
            output += roomName;
            output += '</h4>';

            output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%">';
            output += '<thead>';
            output += '<tr>';
            output += '<th>  <?php echo lang("date"); ?> </th>';
            output += '<th>  <?php echo lang("status"); ?> </th>';
            output += '</tr>';
            output += '</thead>';
            output += '<tbody>';

            $.each(data, function (j, item) {
                output += '<tr>';
                output += '<td>';
                output += item.data;
                output += '</td>';
                output += '<td>';
                output += item.type;
                output += '</td>';
                output += '</tr>';
            });

            var url = "<?php echo base_url('room_attendance/index_student_attendance/') ?>" + student_id;
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="2" class="dataTables_empty">';
            output += '<a href="' + url + '" class="btn btn-default">';
            output += "<?php echo lang('read_more'); ?>";
            output += '</a>';
            output += '</td>';
            output += '</tr>';
            output += '</tbody>';

            output += '</div>';
        } else {

            var output = "";
            output += "<div class='box'>";
            output += '<h4 class="text-center">';
            output += "<?php echo lang('no_attendance'); ?>"
            output += "</h4>";
            output += "</div>";
            $("#attendances").html(output);
        }

        return output;
    }

    function getNotes() {

        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_notes/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            if (data.data) {
                $.each(data.data, function (i, item) {
                    $("#notes-div").append(drawNotesTable(i, item));
                });

                if (!$.each(data.data)) {
                } else {
                    var output = "";
                    output += "<div class='box'>";
                    output += '<h4 class="text-center">';
                    output += "<?php echo lang('no_notes'); ?>"
                    output += "</h4>";
                    output += "</div>";
                    $("#notes-div").html(output);
                }
            } else {
                var output = "";
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('no_notes'); ?>"
                output += "</h4>";
                output += "</div>";
                $("#notes-div").html(output);
            }
        });
    }

    function drawNotesTable(i, data) {
        var output = "";
        if (data != null && data != "") {
            var gradeName = data[0].grade_name;
            var roomName = data[0].room;
            output += '<div class="table-responsive m-t-40">';
            output += '<h4 class="text-center">';
            output += gradeName;
            output += ": ";
            output += roomName;
            output += '</h4>';

            output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
            output += '<thead>';
            output += '<tr>';
            output += '<th>  <?php echo lang("date"); ?> </th>';
            output += '<th>  <?php echo lang("note"); ?> </th>';
            output += '</tr>';
            output += '</thead>';
            output += '<tbody>';

            $.each(data, function (j, item) {
                output += '<tr>';
                output += '<td>';
                output += item.note_date;
                output += '</td>';
                output += '<td>';
                output += item.note_text;
                output += '</td>';
                output += '</tr>';
            });

            var url = "<?php echo base_url('student_note/index_student_note/') ?>" + data[0].student_record_id;
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="2" class="dataTables_empty">';
            output += '<a href="' + url + '" class="btn btn-default">';
            output += "<?php echo lang('read_more'); ?>";
            output += '</a>';
            output += '</td>';
            output += '</tr>';
            output += '</tbody>';

            output += '</div>';
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="2" class="dataTables_empty">';
            output += '<a href="' + url + '" class="btn btn-default">';
            output += "<?php echo lang('read_more'); ?>";
            output += '</a>';
            output += '</td>';
            output += '</tr>';
            output += '</tbody>';

            output += '</div>';
        }

        return output;

    }

    function getAllergies() {

        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_allergies/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            if (data.data) {
                $.each(data.data, function (i, item) {
                    $("#allergies-div").append(drawAllergiesTable(i, item));
                });

                if (!$.each(data.data)) {
                } else {
                    var output = "";
                    output += "<div class='box'>";
                    output += '<h4 class="text-center">';
                    output += "<?php echo lang('no_allergies'); ?>"
                    output += "</h4>";
                    output += "</div>";
                    $("#allergies-div").html(output);
                }
            } else {
                var output = "";
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('no_allergies'); ?>"
                output += "</h4>";
                output += "</div>";
                $("#allergies-div").html(output);
            }
        });
    }

    function drawAllergiesTable(i, data) {
        var output = "";
        if (data != null && data != "") {
            var gradeName = data[0].grade_name;
            var roomName = data[0].room;
            output += '<div class="table-responsive m-t-40">';
            output += '<h4 class="text-center">';
            output += gradeName;
            output += ": ";
            output += roomName;
            output += '</h4>';

            output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
            output += '<thead>';
            output += '<tr>';
            output += '<th>  <?php echo lang("allergy_name"); ?> </th>';
            output += '</tr>';
            output += '</thead>';
            output += '<tbody>';

            $.each(data, function (j, item) {
                output += '<tr>';
                output += '<td>';
                output += item.allergy_name;
                output += '</td>';
                output += '</tr>';
            });

            var url = "<?php echo base_url('student_allergy/index_student_allergy/') ?>" + data[0].student_record_id;
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="1" class="dataTables_empty">';
            output += '<a href="' + url + '" class="btn btn-default">';
            output += "<?php echo lang('read_more'); ?>";
            output += '</a>';
            output += '</td>';
            output += '</tr>';
            output += '</tbody>';

            output += '</div>';
        } else {
            output += '<tr class="odd">';
            output += '<td valign="top" colspan="2" class="dataTables_empty">';
            output += '<a href="' + url + '" class="btn btn-default">';
            output += "<?php echo lang('read_more'); ?>";
            output += '</a>';
            output += '</td>';
            output += '</tr>';
            output += '</tbody>';
            output += '</div>';
        }

        return output;

    }

    function getStudent_study_sequences_info() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_student_study_sequences/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            if (data.data) {
                var output = "";
                output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
                output += '<thead>';
                output += '<tr>';
                output += '<th>  <?php echo lang("grade_name"); ?> </th>';
                output += '<th>  <?php echo lang("room_name"); ?> </th>';
                output += '<th>  <?php echo lang("status"); ?> </th>';
                output += '<th>  <?php echo lang("teacher_child"); ?> </th>';
                output += '<th>  <?php echo lang("non_active_registration_date"); ?> </th>';
                //                output += '<th>  <?php echo lang("photo_permission_slips"); ?> </th>';
                //                output += '<th>  <?php echo lang("islamic_book_return"); ?> </th>';

                output += '</tr>';
                output += '</thead>';
                output += '<tbody>';

                $.each(data.data, function (j, item) {
                    output += '<tr>';
                    output += '<td>';
                    output += item.grade_name;
                    output += '</td>';
                    output += '<td>';
                    output += item.room_name;
                    output += '</td>';
                    output += '<td>';
                    if (item.is_active === '2') {
                        output += "<?php echo lang("is_active"); ?>";
                    } else if (item.is_active === '3') {
                        output += "<?php echo lang("not_active"); ?>";
                    } else {
                        output += "<?php echo lang("transferred_to_another_grade"); ?>";
                    }
                    output += '</td>';

                    output += '<td>';
                    if (item.teacher_child === '2') {
                        output += "<?php echo lang("yes"); ?>";
                    } else {
                        output += "<?php echo lang("no"); ?>";
                    }
                    output += '</td>';

                    output += '<td>';
                    if (item.non_active_registration_date !== null)
                        output += item.non_active_registration_date;
                    else
                        output += ' ';
                    output += '</td>';

                    output += '</tr>';
                });

                //                $.each(data.data, function (i, item) {
                //                    $("#student_study_sequences").append(showStudent_study_sequencesTabel(item));
                //                   
                //                });
                var url = "<?php echo base_url('student/student_study_sequences/') ?>" + student_id;
                output += '<tr class="odd">';
                output += '<td valign="top" colspan="5" class="dataTables_empty">';
                output += '<a href="' + url + '" class="btn btn-default">';
                output += "<?php echo lang('read_more'); ?>";
                output += '</a>';
                output += '</td>';
                output += '</tr>';
                output += '</tbody>';

                $("#student_study_sequences").html(output);


            } else {
                var output = "";
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('no_records'); ?>"
                output += "</h4>";
                output += "</div>";
                $("#student_study_sequences").html(output);
            }
        });
    }

    function showStudent_study_sequencesTabel(data) {
        var output = "";
        var student_id = <?php echo $student_id; ?>;
        if (data != null && data != "") {
            //            output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
            //            output += '<thead>';
            //            output += '<tr>';
            //            output += '<th>  <?php echo lang("grade_name"); ?> </th>';
            //            output += '<th>  <?php echo lang("room_name"); ?> </th>';
            //            output += '<th>  <?php echo lang("status"); ?> </th>';
            //            output += '</tr>';
            //            output += '</thead>';
            //            output += '<tbody>';

            output += '<tr>';
            output += '<td>';
            output += data.grade_name;
            output += '</td>';
            output += '<td>';
            output += data.room_name;
            output += '</td>';
            output += '<td>';
            if (data.is_active === '2') {
                output += "<?php echo lang("is_active"); ?>";
            } else if (data.is_active === '3') {
                output += "<?php echo lang("not_active"); ?>";
            } else {
                output += "<?php echo lang("transferred_to_another_grade"); ?>";
            }
            output += '</td>';
            output += '</tr>';
        }

        return output;
    }

    function getStudentcard() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_report_card/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            if (data) {
                $("#report_card_div").append(drawReportcardTable(data.data));
            } else {
                var output = "";
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('no_records'); ?>"
                output += "</h4>";
                output += "</div>";
                $("#report_card_div").html(output);
            }
        });
    }

    function drawReportcardTable(data) {
        var output = "";
        output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
        output += '<thead>';
        output += '<tr>';
        output += '<th>  <?php echo lang("grade_name"); ?> </th>';
        output += '<th>  <?php echo lang("room_name"); ?> </th>';
        output += '<th>  <?php echo lang("report_card"); ?> </th>';

        output += '</tr>';
        output += '</thead>';
        output += '<tbody>';

        $.each(data, function (j, item) {
            output += '<tr>';
            output += '<td>';
            output += item.grade_name;
            output += '</td>';
            output += '<td>';
            output += item.room_name;
            output += '</td>';

            output += '<td>';

            if (item.grade_type !== 'youth_group' && item.grade_id !== 'book_club') {
                output += "<a href=' ";
                output += "<?php echo base_url("report_card/") ?>" + item.report_card_name + "/1" + "/" + item.id;
                output += " '>";
                output += "<span ";
                output += ">Semester 1";
                output += "</span>";
                output += " </a> / ";

                output += "<a href=' ";
                output += "<?php echo base_url("report_card/") ?>" + item.report_card_name + "/2" + "/" + item.id;
                output += " '>";
                output += "<span ";
                output += ">Semester 2";
                output += "</span>";
                output += "</a>";
            } else {
                output += "<?php echo lang('no_report_card'); ?>"
            }


            output += '</td>';

            output += '</tr>';
        });

        return output;
    }

</script>


<script>
    $(document).ready(function () {
//        get_info();
        get_record_info_t();
    });

    function get_record_info_t() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_students_records_info/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            showCustomFields(data.data);
        });
    }

</script>

<style>
    .box{
        box-shadow: 0 0px 0px rgba(0,0,0,0.25)!important;
    }
</style>