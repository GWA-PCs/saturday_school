<div class="card">
    <div class="card-body" style="border-bottom:8px solid #20AEE3;">
        <h4 class="card-title m-t-10 text-center" id="family_name_card"></h4>
        <hr>
        <center id ="image" class="m-t-30" >
        </center>
    </div>
</div>
<!--<div class="card">
    <div class="card-body" style="border-bottom:8px solid #20AEE3;">
        <div class="row">
            <div class="col-md-12">
                <h5  class=""><?php echo lang("username") . ": "; ?></h5>
            </div>
            <?php if (isset($user_login) && $user_login && $user_login->type == "parent") { ?> 
                <div class="col-md-12">
                    <h5><?php echo $login_email ?></h5>
                </div>
            <?php } else { ?>
                <div class="col-md-12">
                    <h5  id="username1"></h5>
                </div>
                <div class="col-md-12">
                    <h5  class=""><?php echo lang("or") . ": "; ?></h5>
                </div>
                <div class="col-md-12">
                    <h5  id="username2"></h5>
                </div>
            <?php } ?>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <h5  class=""><?php echo lang("password") . ": "; ?></h5>
            </div>
            <div class="col-md-12">
                <h5  id="password"></h5>
            </div>

        </div>
    </div>
</div>-->


<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
</style>

<script>
    $(document).ready(function () {
//        get_info();
    });

    function get_info() {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/profile_parent_data/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var family_name_card = data.data.family_name;
                var username1 = data.data.email;
                var username2 = data.data.email2;
                var password = data.data.non_encrept_pass;

                $("#family_name_card").text(family_name_card);
                $("#username1").text(username1);
                if (username2 === '' || username2 === null) {
                    $("#username2").text("<?php echo lang('no_another_account_found'); ?>");
                } else {
                    $("#username2").text(username2);
                }

                $("#password").text(password);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>




<script>
    $(document).ready(function () {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/get_parent_image/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (img)
            {
                        console.log(img);

                $("#image").append(img.data);
            }
        });
    });


</script>