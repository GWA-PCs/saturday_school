<body class="fix-header card-no-border fix-sidebar">  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <?php $this->load->view('profile/tab_teacher_card_for_another_account'); ?>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab"  href="#info" role="tab"><?php echo lang('info') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab"  href="#contact_info" role="tab"><?php echo lang('contact_info') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#rooms" role="tab"><?php echo lang('index_teacher_rooms') ?> </a> </li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">
                                    <div class="box">
                                         <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("username") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5  id="username"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("password") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5  id="password"></h5>
                                            </div>

                                        </div>
                                         <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("teacher_name") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="teacher_name"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("assistant_teacher") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="assistant_teacher"></h5>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("birthdate") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="birthdate"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("address") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="address"></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="contact_info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">

                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <h5 class=""><?php echo lang("mobile"); ?>: </h5>
                                                <h5 id="mobile"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5 class=""><?php echo lang("email"); ?>: </h5>
                                                <!--<a href="#">-->
                                                <h5 id="email"></h5>
                                                <!--</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--note tab-->
                        <div class="tab-pane" id="rooms" role="tabpanel">
                            <div class="card-body">
                                <div id="rooms-div"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div> 
    </div> 
</body>

<style>
    .box {
        border: 1px solid #d8d8d8!important;
        padding: 10px!important;
        box-shadow: 0 0px 0px rgba(0,0,0,0.25)!important;
    }
</style>


<script>
    $(document).ready(function () {
        get_teacher_info();
        getRooms();
    });

    function get_teacher_info() {
        var teacher_id = <?php echo $teacher_id; ?>;
        var URL = "<?php echo base_url("profile/profile_teacher_data_for_another_account/") ?>" + teacher_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.first_name + " " + data.data.last_name;
                var fatherName = data.data.father_name;
                var motherName = data.data.mother_name;
                var birthdate = data.data.birthdate;
                var address = data.data.address;
                var phone = data.data.phone;
                var mobile = data.data.mobile;
                var email = data.data.email;
                var assistantTeacher = data.data.assistant_teacher;
                if (assistantTeacher === '1')
                    assistantTeacher = "<?php echo lang('no') ?>";
                else
                    assistantTeacher = "<?php echo lang('yes') ?>";

                $("#teacher_name").text(fullName);
                $("#father_name").text(fatherName);
                $("#mother_name").text(motherName);
                $("#birthdate").text(birthdate);
                $("#address").text(address);
                $("#phone").text(phone);
                $("#mobile").text(mobile);
                $("#email").text(email);
                $("#assistant_teacher").text(assistantTeacher);


            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function getRooms() {
        var teacher_id = <?php echo $teacher_id; ?>;
        var URL = "<?php echo base_url("profile/get_teacher_rooms/") ?>" + teacher_id;

        $.getJSON(URL, function (data) {
            var output = "";
            if (data.data) {
                output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
                output += '<thead>';
                output += '<tr>';
                output += '<th>  <?php echo lang("room_name"); ?> </th>';
                output += '<th>  <?php echo lang("grade_name"); ?> </th>';
                output += '<th>  <?php echo lang("index_teacher_subjects"); ?> </th>';
                output += '</tr>';
                output += '</thead>';
                output += '<tbody>';
                $.each(data.data, function (i, item) {
                    output += '<tr>';
                    output += '<td>';
                    output += item.room_name;
                    output += '</td>';
                    output += '<td>';
                    output += item.grade_name;
                    output += '</td>';


                    output += '<td>';
                    output += "<a href= '";
                    output += '<?php echo base_url("teacher/index_teacher_subjects/") ?>' + item.room_id + '/' + teacher_id;
                    output += " ' >";
                    output += '<span ';
                    output += 'data-toggle="tooltip" ';
                    output += 'title=<?php echo lang("index_teacher_subjects"); ?>';
                    output += '';
                    output += '>';
                    output += '<i class=" mdi  mdi-book-open-page-variant fa-2x "></i>';
                    output += '</span>';
                    output += '</a>';
                    output += '</td>';


                    output += '</tr>';
                });
                output += '</tbody>';
                output += '</table>';

            } else {
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('no_rooms'); ?>";
                output += "</h4>";
                output += "</div>";
            }
            $("#rooms-div").html(output);
        });
    }





</script>

<script>
    $(document).ready(function () {
        get_info();
    });

    function get_info() {
        var teacher_id = <?php echo $teacher_id; ?>;
        var URL = "<?php echo base_url("profile/profile_teacher_data_for_another_account/") ?>" + teacher_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.first_name + " " + data.data.last_name;
                var username = data.data.email;
                var password = data.data.non_encrept_pass;

                $("#name").text(fullName);
                $("#username").text(username);
                $("#password").text(password);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>

