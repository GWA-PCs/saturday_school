<body class="fix-header card-no-border fix-sidebar">  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <?php $this->load->view('profile/tab_teacher_card'); ?>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs customtab2" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab"  href="#info" role="tab"><?php echo lang('info') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab"  href="#contact_info" role="tab"><?php echo lang('contact_info') ?> </a> </li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">
                                    <div class="box" style="">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("username") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5  id="username"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("password") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5  id="password"></h5>
                                            </div>

                                        </div>
                                         <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("teacher_name") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="teacher_name"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("assistant_teacher") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="assistant_teacher"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("birthdate") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="birthdate"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5  class=""><?php echo lang("address") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5 id="address"></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="contact_info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">

                                    <div class="box" style="">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <h5 class=""><?php echo lang("mobile"); ?>: </h5>
                                                <h5 id="mobile"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5 class=""><?php echo lang("email"); ?>: </h5>
                                                <!--<a href="#">-->
                                                <h5 id="email"></h5>
                                                <!--</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
        </div> 
    </div> 
</body>



<style>
    .box {
        /*        border: 1px solid #d8d8d8;
                padding: 10px;*/
        box-shadow: 0 0px 0px rgba(0,0,0,0.25)!important;
    }
</style>


<script>
    $(document).ready(function () {
        get_teacher_info();
    });

    function get_teacher_info() {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/profile_teacher_data/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.first_name + " " + data.data.last_name;
                var fatherName = data.data.father_name;
                var motherName = data.data.mother_name;
                var birthdate = data.data.birthdate;
                var address = data.data.address;
                var phone = data.data.phone;
                var mobile = data.data.mobile;
                var email = data.data.email;
                var assistantTeacher = data.data.assistant_teacher;
                if (assistantTeacher === '1')
                    assistantTeacher = "<?php echo lang('no') ?>";
                else
                    assistantTeacher = "<?php echo lang('yes') ?>";

                $("#teacher_name").text(fullName);
                $("#father_name").text(fatherName);
                $("#mother_name").text(motherName);
                $("#birthdate").text(birthdate);
                $("#address").text(address);
                $("#phone").text(phone);
                $("#mobile").text(mobile);
                $("#email").text(email);
                $("#assistant_teacher").text(assistantTeacher);


            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>


<script>
    $(document).ready(function () {
        get_info();
    });

    function get_info() {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/profile_teacher_data/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.first_name + " " + data.data.last_name;
                var username = data.data.email;
                var password = data.data.non_encrept_pass;

                $("#name").text(fullName);
                $("#username").text(username);
                $("#password").text(password);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>

