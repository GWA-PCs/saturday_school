<div class="card">

    <div class="card-body"  style="border-bottom:8px solid #20AEE3;">

        <h4 class="card-title m-t-10 text-center" id="name"></h4>

        <!--<div>-->
        <hr>
        <!--</div>-->
        <center id ="image" class="m-t-20">
        </center>
    </div>
    <!--    <div class="card-body"  style="border-bottom:8px solid #20AEE3;">
            <div id="custom"></div>
        </div>-->
</div>
<!--<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <h5  class=""><?php echo lang("username") . ": "; ?></h5>
            </div>
            <div class="col-md-12">
                <h5  id="username"></h5>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <h5  class=""><?php echo lang("password") . ": "; ?></h5>
            </div>
            <div class="col-md-12">
                <h5  id="password"></h5>
            </div>

        </div>
    </div>
</div>-->

<script>
    $(document).ready(function () {
        get_info();
    });

    function get_info() {
        var teacher_id = <?php echo $teacher_id; ?>;
        var URL = "<?php echo base_url("profile/profile_teacher_data_for_another_account/") ?>" + teacher_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.first_name + " " + data.data.last_name;
                var username = data.data.email;
                var password = data.data.non_encrept_pass;

                $("#name").text(fullName);
                $("#username").text(username);
                $("#password").text(password);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>



<script>
    $(document).ready(function () {
        var teacher_id = <?php echo $teacher_id; ?>;
        var URL = "<?php echo base_url("profile/get_teacher_image/") ?>" + teacher_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (img)
            {

                $("#image").append(img.data);
            }
        });
    });


</script>