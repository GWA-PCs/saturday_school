<div class="card">

    <div class="card-body" style="border-bottom:8px solid #20AEE3;">

        <h4 class="card-title m-t-10 text-center" id="full_name"></h4>

        <!--<div>-->
        <hr>
        <!--</div>-->
        <center id ="image" class="m-t-20">
        </center>
    </div>
<!--    <div class="card-body"  style="border-bottom:8px solid #20AEE3;">
        <div id="custom"></div>
    </div>-->
</div>

<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
</style>

<script>
    $(document).ready(function () {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_personal_image/") ?>" + student_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (img)
            {
                $("#image").append(img.data);
            }
        });
    });


</script>

<script>
    $(document).ready(function () {
        get_info();
        get_record_info_t();
    });

    function get_info() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/profile_student_data/") ?>" + student_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {

                if (data.data.father_name === null) {
                    var fullName = data.data.student_name + " " + data.data.family_name;
                } else {
                    var fullName = data.data.student_name + " " + data.data.father_name + " " + data.data.family_name;
                }
                $("#full_name").text(fullName);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function get_record_info_t() {
        var student_id = <?php echo $student_id; ?>;
        var URL = "<?php echo base_url("profile/get_students_records_info/") ?>" + student_id;

        $.getJSON(URL, function (data) {
            showCustomFields(data.data);
        });
    }


    function showCustomFields(data) {
        var output = "";
        $.each(data, function (i, item) {

            output += "<div class='box' style=' margin-top: -36px;'>";
            output += "<h5 class='text-center font-weight-bold'>";
            output += item.grade_name;
            output += "</h5>";
            output += "<br>";
            if (item.stage_id == 2) {
                output += "<div class='row'>";
                output += "<div class='col-md-9'>";
                output += "<?php echo lang('islamic_book_return'); ?>";
                output += "</div>";
                output += "<div class='col-md-3'>";
                if (item.islamic_book_return == 2) {
                    output += "<?php echo lang('yes'); ?>";
                } else {
                    output += "<?php echo lang('no'); ?>";
                }
                output += "</div>";
                output += "</div>";
                output += "<br>";
            }
            output += "<div class='row'>";
            output += "<div class='col-md-9'>";
            output += "<?php echo lang('photo_permission_slips'); ?>";
            output += "</div>";
            output += "<div class='col-md-3'>";
            if (item.photo_permission_slips == 2) {
                output += "<?php echo lang('yes'); ?>";
            } else {
                output += "<?php echo lang('no'); ?>";
            }
            output += "</div>";
            output += "</div>";
            output += "</div>";
            output += "<br><br>";
        });

        $("#custom").html(output);

    }

</script>