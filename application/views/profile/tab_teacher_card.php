<div class="card">
    <div class="card-body"  style="border-bottom:8px solid #20AEE3;">
        <h4 class="card-title m-t-10 text-center" id="name"></h4>

        <hr>
        <center id ="image" class="m-t-30" >
        </center>
    </div>
</div>
<!--<div class="card">
    <div class="card-body" style="border-bottom:8px solid #20AEE3;">
        <div class="row">
            <div class="col-md-12">
                <h5  class=""><?php echo lang("username") . ": "; ?></h5>
            </div>
            <div class="col-md-12">
                <h5  id="username"></h5>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <h5  class=""><?php echo lang("password") . ": "; ?></h5>
            </div>
            <div class="col-md-12">
                <h5  id="password"></h5>
            </div>

        </div>
    </div>
</div>-->

<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 0px 0px rgba(0,0,0,0.25)!important;
    }
</style>

<script>
    $(document).ready(function () {
        get_info();
    });

    function get_info() {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/profile_teacher_data/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var fullName = data.data.first_name + " " + data.data.last_name;
                var username = data.data.email;
                var password = data.data.non_encrept_pass;

                $("#name").text(fullName);
                $("#username").text(username);
                $("#password").text(password);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>



<script>
    $(document).ready(function () {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/get_teacher_and_parent_image/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (img)
            {
                $("#image").append(img.data);
            }
        });
    });


</script>



<script>
    $(document).ready(function () {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/get_teacher_image/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (img)
            {
//                        console.log(img);

                $("#image").append(img.data);
            }
        });
    });


</script>