<body class="fix-header card-no-border fix-sidebar">  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <?php $this->load->view('profile/tab_parent_card'); ?>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs customtab2" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab"  href="#info" role="tab"><?php echo lang('info') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab"  href="#contact_info" role="tab"><?php echo lang('contact_info') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab"  href="#emergency_contact" role="tab"><?php echo lang('emergency_contact') ?> </a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab"  href="#children" role="tab"><?php echo lang('index_parent_children') ?> </a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">
                                    <div class="box">


                                        <?php if (isset($user_login) && $user_login && ($user_login->type == "parent" || $user_login->type == "admin")) { ?> 
                                            <div class="row">        
                                                <div class="col-md-4">       
                                                    <h5  class=""><?php echo lang("username") . ": "; ?></h5>
                                                </div>


                                                <div class="col-md-4">
                                                    <h5  id="username1"></h5>
                                                </div>
                                            </div>

                                            <div class="row or_div">
                                                <br>
                                                <div class="col-md-4">
                                                    <h5  class=""><?php echo lang("or") . ": "; ?></h5>
                                                </div>
                                                <br>
                                                <div class="col-md-4">
                                                    <h5  id="username2"></h5>
                                                </div>
                                            </div>


                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5  class=""><?php echo lang("password") . ": "; ?></h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5  id="password"></h5>
                                                </div>
                                            </div>
                                        <?php } ?>



                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("family_name") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="family_name"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5 class="" ><?php echo lang("father_name") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="father_name"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("mother_name") . ': '; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="mother_name"></h5>
                                            </div>
                                        </div>
                                        <?php if (isset($user_login) && $user_login && ($user_login->type != "teacher" )) { ?> 

                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5  class=""><?php echo lang("balance") . ': '; ?></h5>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5 id="account_balance"></h5>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5  class=""><?php echo lang("number_of_payments") . ': '; ?></h5>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5 id="number_of_payments"></h5>
                                                </div>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="contact_info" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">

                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("address") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="address"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("father_phone") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="father_phone"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("mother_phone") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="mother_phone"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("home_phone") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="home_phone"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("email") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="email"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("email2") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="email2"></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="emergency_contact" role="tabpanel">
                            <div class="card-body">
                                <div class="profiletimeline">
                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_name_1") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_name_1"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_phone_1") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_phone_1"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_relationship_1") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_relationship_1"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_name_2") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_name_2"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_phone_2") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_phone_2"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_relationship_2") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_relationship_2"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_name_3") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_name_3"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_phone_3") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_phone_3"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("emergency_relationship_3") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="emergency_relationship_3"></h5>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5  class=""><?php echo lang("custody_info_other_info") . ": "; ?></h5>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 id="custody_info_other_info"></h5>
                                            </div>
                                        </div>
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="children" role="tabpanel">
                            <div class="card-body">
                                <div id="children-div"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
        </div> 
    </div> 
</body>

<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
</style>


<script>
    $(document).ready(function () {
        get_parent_info();
        get_parent_balance();
        get_number_of_payments();
        getChildren();
    });

    function get_parent_info() {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/profile_parent_data/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var family_name = data.data.family_name;
                var fatherName = data.data.father_name;
                var motherName = data.data.mother_name;
//                var accountBalance = data.data.balance + '$';
                var Address = data.data.address;
                var fatherPhone = data.data.father_phone;
                var motherPhone = data.data.mother_phone;
                var homePhone = data.data.home_phone;
                var Email = data.data.email;
                var Email2 = data.data.email2;

                var emergency_name_1 = data.data.emergency_name_1;
                var emergency_name_2 = data.data.emergency_name_2;
                var emergency_name_3 = data.data.emergency_name_3;
                var emergency_phone_1 = data.data.emergency_phone_1;
                var emergency_phone_2 = data.data.emergency_phone_2;
                var emergency_phone_3 = data.data.emergency_phone_3;
                var emergency_relationship_1 = data.data.emergency_relationship_1;
                var emergency_relationship_2 = data.data.emergency_relationship_2;
                var emergency_relationship_3 = data.data.emergency_relationship_3;
                var custody_info_specify = data.data.custody_info_specify;
                var custody_info_other_info = data.data.custody_info_other_info;

                $("#family_name").text(family_name);
                $("#father_name").text(fatherName);
                $("#mother_name").text(motherName);
                $("#address").text(Address);
//                $("#account_balance").text(accountBalance);
                $("#father_phone").text(fatherPhone);
                $("#mother_phone").text(motherPhone);
                $("#home_phone").text(homePhone);
                $("#email").text(Email);
                $("#email2").text(Email2);

                $("#emergency_name_1").text(emergency_name_1);
                $("#emergency_name_2").text(emergency_name_2);
                $("#emergency_name_3").text(emergency_name_3);
                $("#emergency_phone_1").text(emergency_phone_1);
                $("#emergency_phone_2").text(emergency_phone_2);
                $("#emergency_phone_3").text(emergency_phone_3);
                $("#emergency_relationship_1").text(emergency_relationship_1);
                $("#emergency_relationship_2").text(emergency_relationship_2);
                $("#emergency_relationship_3").text(emergency_relationship_3);
                $("#custody_info_specify").text(custody_info_specify);
                $("#custody_info_other_info").text(custody_info_other_info);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function get_number_of_payments() {
        var parent_id = <?php echo $parent_id; ?>;
        var URL = "<?php echo base_url("profile/get_number_of_payments/") ?>" + parent_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var number_of_payments = data.data.number_of_payments;
                $("#number_of_payments").text(number_of_payments);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function get_parent_balance() {
        var parent_id = <?php echo $parent_id; ?>;
        var URL = "<?php echo base_url("profile/get_parent_balance/") ?>" + parent_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var accountBalance = '$' + data.data.balance;
                $("#account_balance").text(accountBalance);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }

    function getChildren() {


        var parent_id = <?php echo $parent_id; ?>;
        var URL = "<?php echo base_url("profile/get_children/") ?>" + parent_id;

        $.getJSON(URL, function (data) {
            if (data.data) {
//                $.each(data.data, function (i, item) {
                $("#children-div").append(drawChildrenTable(data.data));
//                });
            } else {
                var output = "";
                output += "<div class='box'>";
                output += '<h4 class="text-center">';
                output += "<?php echo lang('index_parent_children'); ?>"
                output += "</h4>";
                output += "</div>";
                $("#children-div").html(output);
            }
        });
    }

    function drawChildrenTable(data) {
        var parent_id = <?php echo $parent_id; ?>;
        var output = "";
        if (data != null && data != "") {
            output += '<table id="table-style" class="display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table" cellspacing="0" width="100%">';
            output += '<thead>';
            output += '<tr>';
            output += '<th>  <?php echo lang("student_name"); ?> </th>';
<?php if (isset($user_login) && $user_login && $user_login->type == "admin") { ?>
                output += '<th>  <?php echo lang("index_student_payment"); ?> </th>';
<?php } ?>
            output += '</tr>';
            output += '</thead>';
            output += '<tbody>';

            $.each(data, function (j, item) {
                output += '<tr>';
                output += '<td>';
                output += "<a href= '";
                output += '<?php echo base_url("profile/page_student_profile/") ?>' + item.student_id;
                output += " ' >";
                output += item.student_name;
                output += '</a></td>';
//                output += '<td>';
//                 $output = "";
<?php if (isset($user_login) && $user_login && $user_login->type == "admin") { ?>
                    output += '<td>';
                    output += "<a href= '";
                    output += '<?php echo base_url("student_payment/index_student_payment/") ?>' + item.student_id;
                    output += " ' >";
                    output += '<span ';
                    output += 'data-toggle="tooltip" ';
                    output += "title='<?php echo lang("index_student_payment"); ?>'";
                    output += 'aria-describedby="tooltip80070" ';
                    output += '>';
                    output += '<i class="mdi  mdi-human-child fa-2x "></i>';
                    output += '</span>';
                    output += '</a>';
                    output += '</td>';
<?php } ?>


//                output += '<td>';
//                output += item.note_text;
//                output += '</td>';
                output += '</tr>';
            });


            output += '</tbody>';

            output += '</div>';
        }

        return output;

    }
</script>


<style>
    .box{
        box-shadow: 0 0px 0px rgba(0,0,0,0.25)!important;
    }
</style>


<script>
    $(document).ready(function () {
        get_info();
    });

    function get_info() {
        var user_id = <?php echo $user_id; ?>;
        var URL = "<?php echo base_url("profile/profile_parent_data/") ?>" + user_id;

        $.ajax({
            url: URL,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var family_name_card = data.data.family_name;
                var username1 = data.data.email;
                var username2 = data.data.email2;
                var password = data.data.non_encrept_pass;

                $("#family_name_card").text(family_name_card);
                $("#username1").text(username1);
                if (username2 === '' || username2 === null) {
                    $(".or_div").hide();
                    $("#username2").text("<?php echo lang('no_another_account_found'); ?>");
                } else {
                    $("#username2").text(username2);
                }

                $("#password").text(password);
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert("Error");
            }
        });
    }
</script>


