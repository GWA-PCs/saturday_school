<?php bs3_card($page_title, FALSE, FALSE); ?>



 <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>

<?php bs3_input('family_name', isset($parents->family_name) ?$parents->family_name : FALSE ); ?>
<?php bs3_input('father_name',isset($parents->family_name) ?$parents->family_name : FALSE ); ?>
<?php bs3_input('mother_name', isset($parents->mother_name) ?$parents->mother_name : FALSE ); ?>
<?php bs3_input('address', isset($parents->address) ?$parents->address : FALSE ); ?>
<?php bs3_tel('father_phone',isset($parents->father_phone) ?$parents->father_phone : FALSE ); ?>
<?php bs3_tel('mother_phone', isset($parents->mother_phone) ?$parents->mother_phone : FALSE ); ?>
<?php bs3_tel('home_phone',isset($parents->home_phone) ?$parents->home_phone : FALSE ); ?>

<?php echo form_close(); ?>


<button type="button" onclick="edit_info()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>



<?php bs3_card_f(); ?>
<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>


<script>
    function edit_info() {
        var data = $("#form_crud1").serialize();
        var url = "<?php echo base_url('profile/edit_parent_profile/'.$parents->id); ?>";
        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {
                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                } else if (data.status == "201") {
                    // valedation error data.data
                    messages_error("<?php echo lang('error'); ?>", data.data);
                }
            },
            error: function () {
//                alert("Error");
            }
        });

    }
</script>
