<?php bs3_card($page_title); ?>

<?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_honored_order") ?>
<?php bs3_hidden('id') ?>
<?php bs3_input('order_title') ?>
<?php bs3_dropdown('stage_id', $stages_options, false, false, false, 'update') ?>
<?php bs3_date('honored_date'); ?>
<?php bs3_textarea('honored_reason'); ?>

<?php bs3_modal_footer('create'); ?>

<?php bs3_table($thead, 'object_table','','','',$non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>
