<?php bs3_card($page_title); ?>

<?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_group") ?>
<?php bs3_hidden('id') ?>
<?php bs3_input('group_name') ?>
<?php bs3_input('description') ?>
<?php bs3_modal_footer('create'); ?>

<?php bs3_table($thead,'object_table') ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>