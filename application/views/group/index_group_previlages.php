<?php bs3_card($page_title); ?>

<?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
    <div class="form-group  row">
        <?php foreach ($previlages as $perm) { ?>
                <label  for="input<?php echo ucfirst($perm['name']); ?>"  style="line-height: 3"   class="control-label  col-md-2"><?php echo $perm['label']; ?>: </label>
                <div class="col-md-2">
                    <input name="<?php echo $perm['name']?>" value="<?php echo $perm['id']?>" type="checkbox" <?php if (${$perm['name'] . "_old"})echo 'checked' ?>  required class="js-switch" data-color="#009efb" />
                </div>
        <?php }?>
    </div>
    <button type="button" onclick="add()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
<?php echo form_close(); ?>

<?php bs3_card_f(); ?>

<script>
    function add() {
        // ajax adding data to database
        var formData = new FormData($('#form_crud1')[0]);
        $.ajax({
            url: "<?php echo base_url("group/update_group_previlages/" . $group_id) ?>",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (dataR) {
                if (dataR.status == "200") //if success close modal and reload ajax table
                {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: dataR.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (dataR.status == "201") {
                    // valedation error data.data
                    messages_error("<?php echo lang('error'); ?>", dataR.data);

                } else if (dataR.status == "202") {

                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: dataR.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",

                    });
                } else if (dataR.status == "400") {
                    // message error in controller data.message
                } else if (dataR.status == "401") {
                    // message error in information data.message and status
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $('.btnSave').text('save'); //change button text
                $('.btnSave').attr('disabled', false); //set button enable
            }
        });
    }

</script>
