<!-- =================== Script for multiple select =================== -->
<script src="<?php echo base_url("assets/assets/select2.min.js") ?>" type="text/javascript"></script>
<link href="<?php echo base_url("assets/assets/select2.min.css") ?>" rel="stylesheet" type="text/css"/>

<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_meeting") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_dropdown('meeting_type', $meeting_with, FALSE, FALSE, FALSE, 'update') ?>

    <!-- ======================= Start Multiple select ======================== -->
    <div id="meeting_to">
        <div class="form-group row" >
            <label for="inputMeeting_to" class="col-sm-4 control-label"><?php echo lang('msg_to[]'); ?>:</label>
            <div class="col-sm-8">
                <select class="js-example-basic-multiple-limit1 form-control" id="inputMeeting_to" hidden-in="update" name="meeting_to[]" required="true" multiple="multiple">
                </select>
            </div>
        </div>
    </div>    
    <!-- ======================= End Multiple select ======================== -->


    <?php bs3_input('meeting_title') ?>
    <?php bs3_textarea('meeting_content') ?>
    <?php bs3_date('meeting_date') ?>


    <?php bs3_checkbox('upload_file_check'); ?>
    <div id="meeting_file">
        <?php bs3_file_or_image('meeting_file', '', false, '100', 'false', false) ?>    
    </div>
    <script>
        $(document).ready(function () {

            $('#meeting_file').hide();
            $('#checkboxupload_file_check').change(function () {
                if (this.checked) {
                    $('#meeting_file').fadeIn();
                    var element = "";
                    element += '<span class="btn btn-default btn-file">';
                    element += '  <span class="fileinput-new">';
                    element += '<?php echo lang("choose_file") ?>';
                    element += '  </span>';
                    element += '  <span class="fileinput-exists">';
                    element += '<?php echo lang("change") ?>';
                    element += '  </span>';
                    element += '<input required="true"  type="file" accept=".pdf,.docx,.doc,.xls,.xlsx,.txt,.csv,.ppt,.pptx,.zip,.rar,.psd,.txt,.pps,image/*" id="file" name="meeting_file">';
                    element += '  </span>';
                    element += '<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">';
                    element += '<?php echo lang("remove") ?>';
                    element += '</a>';
                    $("#div_for_file").html(element);
                } else {
                    $("#meeting_file").remove();
                }

            });
        });
    </script>


    <div id="send_by_email">
        <?php bs3_checkbox('send_by_email'); ?>
    </div>
    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>


<script type="text/javascript">

    function edit_object(id) {
        var show_modal_id = "<?php echo "#$modal_name" ?>";
        save_method = 'update';
        $('#form_crud')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        //$('.help-block').empty(); // clear error string

        var formData = new FormData($('#form_crud')[0]);
        for (var pair of formData.entries()) {
            var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
            if (hidden_in == "update") {
                $('[name=' + pair[0] + ']').parent().parent().hide();
            } else {
                $('[name=' + pair[0] + ']').parent().parent().show();
            }
        }

        $(show_modal_id).modal('show'); // show bootstrap modal
        //Ajax Load data from ajax
        $form = $(event.target);

        var URL = "<?php echo base_url($get_object_link_meeting) ?>/" + id;

        $.ajax({
            url: URL,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $("form#form_crud :input").each(function (index, item) {
                    var input_name = $(this).attr('name'); // This is the jquery object of the input, do what you will
                    var input_type = $(this).attr('type');
                    if (input_type === 'file') {
                        $(".fileinput").removeClass("fileinput-new");
                        $(".fileinput").addClass("fileinput-exists");
                        $(".fileinput-preview").text(data.meeting_file);
                    }
                    if (input_type != 'checkbox' && input_type != 'file') {
                        $(this).val(data[input_name]);
                    } else if (input_type == 'checkbox') {
                        if (data[input_name] == 2) {
                            $(this).prop("checked", true);
                        } else {
                            $(this).prop("checked", false);
                        }
                    }
                });

                var update_object_title = "<?php echo $update_object_title; ?>";
                $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error get data from ajax');
            }
        });
    }

</script>



<script>
    $(document).ready(function () {
        $('#meeting_to').hide();
        // ==================== For filter the rooms which student in it ===========
        $("#inputMeeting_type").change(function () {
            $("#meeting_to").fadeIn();
            var meeting_type = $(this).val();
            $.ajax({
                url: "<?php echo base_url("meeting/get_meeting_to_users") ?>",
                type: 'post',
                data: {meeting_type: meeting_type},
                dataType: 'json',
                success: function (response) {
                    var len = response.length;
                    $("#inputMeeting_to").empty();
                    if (meeting_type == "teachers")
                    {
                        for (var i = 0; i < len; i++) {
                            var user_id = response[i]['user_id'];
                            var first_name = response[i]['first_name'];
                            var last_name = response[i]['last_name'];
                            $("#inputMeeting_to").append("<option  id='" + user_id + "' class='my_form'  value='" + user_id + "'>" + first_name + ' ' + last_name + "</option>");
                        }
                    } else if (meeting_type == "class_teachers")
                    {

                        for (var i = 0; i < len; i++) {
                            var room_id = response[i]['room_id'];
                            var room_name = response[i]['room_name'];
                            var grade_name = response[i]['grade_name'];
                            $("#inputMeeting_to").append("<option  id='" + room_id + "' class='my_form'  value='" + room_id + "'>" + 'Teachers of' + ' ' + grade_name + "</option>");
                        }
                    }



                }});
        });
    });
</script>



<script type="text/javascript">
    $(".js-example-basic-multiple-limit").select2({
        maximumSelectionLength: 100,
    });
    $(".js-example-basic-multiple-limit1").select2({
        maximumSelectionLength: 100,
    });

</script>

<style>
    .select2-container--default .select2-results>.select2-results__options{
        margin-top: -20px !important;
    }
    .select2-selection__rendered {
        resize: auto!important;
        width: 400px;
        min-height: 100px;
        padding: 5px;
        overflow-y: scroll!important;
        box-sizing: border-box;
    }

    .select2-container--open{
        z-index:9999999         
    }
    .select2-container {
        width: 100% !important;
    }
    .select2-results__options li{
        list-style-type: none;
    }

    .select2-selection--multiple{
        margin-bottom: -2px!important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__rendered{
        border: 1px solid #dddddd;
        border-radius: .25rem;
        height: calc(2.25rem + 20px);
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        /* border: solid black 1px; */
        outline: 0;
        /*height: 45px;*/
        border-radius: 0px;
        border: 1px solid #e7e7e7;
        /*margin-top: 10px;*/
        width: 100%;
        display: block;
    }
    .select2-container--default .select2-selection--multiple {
        background-color: white;
        /*height: 45px  !important;*/
        border: 1px solid #fff !important;
        border-radius: 0px !important;
        cursor: text;
        /*margin-top: 10px;*/
        width: 100% !important;
    }
    .tg-search-form .select2-container--default.select2-container--focus .select2-selection--multiple {
        /* border: solid black 1px; */
        outline: 0;
        /*height: 38px!important;*/
        border-radius: 0px;
        border: 1px solid #e7e7e7;
        /*margin-top: 0;*/
        width: 84%!important;
        display: block;
    }
    .tg-search-form .select2-container--default .select2-selection--multiple {
        background-color: white;
        /*height: 38px!important;*/
        border: 1px solid #fff !important;
        border-radius: 0px !important;
        cursor: text;
        /*margin-top: 0px;*/
        width: 84%!important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        color: #657176!important;
    }


    /*    .form {
            border: 1px solid black;
            text-align: center;
            outline: none;
            min-width:4px;
        }*/

</style>
