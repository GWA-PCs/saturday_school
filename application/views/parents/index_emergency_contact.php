<?php bs3_card($page_title, '', false); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
<?php } ?>
<div class="form-group">
    <label><strong><?php echo lang("allowed_to_pick_up_students"); ?></strong></label>
</div>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_input('emergency_name_1', $emergency_contact->emergency_name_1) ?>
    <?php bs3_tel('emergency_phone_1', $emergency_contact->emergency_phone_1) ?>
    <?php bs3_input('emergency_relationship_1', $emergency_contact->emergency_relationship_1) ?>
    <?php bs3_input('emergency_name_2', $emergency_contact->emergency_name_2) ?>
    <?php bs3_tel('emergency_phone_2', $emergency_contact->emergency_phone_2) ?>
    <?php bs3_input('emergency_relationship_2', $emergency_contact->emergency_relationship_2) ?>
    <?php bs3_input('emergency_name_3', $emergency_contact->emergency_name_3) ?>
    <?php bs3_tel('emergency_phone_3', $emergency_contact->emergency_phone_3) ?>
    <?php bs3_input('emergency_relationship_3', $emergency_contact->emergency_relationship_3) ?>
    <div class="form-group">
        <label  for="custody_info"><strong><?php echo lang('custody_info'); ?>: </strong> </label>
    </div>
    <?php bs3_dropdown('custody_info_specify', $emergency_contact_options, $emergency_contact->custody_info_specify) ?> 

    <?php bs3_input('custody_info_other_info', $emergency_contact->custody_info_other_info) ?>

    <button type="button" onclick="add_to_db()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
    <?php echo form_close(); ?>
<?php } else { ?>
    <?php bs3_input('emergency_name_1', $emergency_contact->emergency_name_1, false, false, false, 'disabled="true"') ?>
    <?php bs3_tel('emergency_phone_1', $emergency_contact->emergency_phone_1, 'disabled="true"') ?>
    <?php bs3_input('emergency_relationship_1', $emergency_contact->emergency_relationship_1, false, false, false, 'disabled="true"') ?>
    <?php bs3_input('emergency_name_2', $emergency_contact->emergency_name_2, false, false, false, 'disabled="true"') ?>
    <?php bs3_tel('emergency_phone_2', $emergency_contact->emergency_phone_2, 'disabled="true"') ?>
    <?php bs3_input('emergency_relationship_2', $emergency_contact->emergency_relationship_2, false, false, false, 'disabled="true"') ?>
    <?php bs3_input('emergency_name_3', $emergency_contact->emergency_name_3, false, false, false, 'disabled="true"') ?>
    <?php bs3_tel('emergency_phone_3', $emergency_contact->emergency_phone_3, 'disabled="true"') ?>
    <?php bs3_input('emergency_relationship_3', $emergency_contact->emergency_relationship_3, false, false, false, 'disabled="true"') ?>
    <div class="form-group">
        <label  for="custody_info"><strong><?php echo lang('custody_info'); ?>: </strong> </label>
        <?php bs3_dropdown('custody_info_specify', $emergency_contact_options, $emergency_contact->custody_info_specify, 'disabled="true"') ?> 
    </div>
    <?php bs3_input('custody_info_other_info', $emergency_contact->custody_info_other_info, false, false, false, 'disabled="true"') ?>

<?php } ?>
<?php bs3_card_f(); ?>

<style>
    @media (min-width: 576px){
        .col-md-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-md-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

    @media (min-width: 576px){
        .col-sm-8 {
            flex: 0 0 60%!important;
            max-width: 60%!important;
        }
        .col-sm-4 {
            flex: 0 0 20%!important;
            max-width: 20%!important;
        }
    }

</style>

<script>
    function add_to_db() {
        // ajax adding data to database
        var formData = new FormData($('#form_crud1')[0]);
        $.ajax({
            url: "<?php echo base_url("parents/add_emergency_contact/") . $parent_id ?>",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (dataR) {
                if (dataR.status == "200") //if success close modal and reload ajax table
                {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: dataR.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (dataR.status == "201") {
                    // valedation error data.data
                    messages_error("<?php echo lang('error'); ?>", dataR.data);

                } else if (dataR.status == "202") {

                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: dataR.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",

                    });
                } else if (dataR.status == "400") {
                    // message error in controller data.message
                } else if (dataR.status == "401") {
                    // message error in information data.message and status
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
//                alert('Error adding / update data');
                $('.btnSave').text('save'); //change button text
                $('.btnSave').attr('disabled', false); //set button enable
            }
        });
    }

</script>