<!--<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item fix-lang">
<?php if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                            <a  href="<?php echo base_url('home/home_page_teacher') ?>" ><?php echo lang('home_page') ?></a>
<?php } elseif (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                            <a href="<?php echo base_url('home/home_page_parent') ?>"  ><?php echo lang('home_page') ?></a>
<?php } else { ?>
                            <a  href="<?php echo base_url('home/home_page') ?>" ><?php echo lang('home_page') ?></a>
<?php } ?>

            </li>
            <li class="breadcrumb-item active  fix-lang"><?php echo $page_title ?></li>
        </ol>
    </div>
</div>-->
<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_parent") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_input('family_name') ?>
    <?php bs3_input('father_name') ?>
    <?php bs3_input('mother_name') ?>
    <?php bs3_tel('father_phone') ?>
    <?php bs3_tel('mother_phone') ?>
    <?php bs3_tel('home_phone') ?>
    <?php bs3_textarea('address') ?>
    <?php // bs3_number_with_dollar('balance') ?>
    <?php bs3_email('email') ?>
    <?php bs3_email('email2') ?>
    <?php bs3_input('non_encrept_pass', $non_encrept_pass) ?>
    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>
<!--
<style>
    .color-bordered-table.info-bordered-table {
        color: currentColor!important;
    }

    .color-bordered-table.info-bordered-table thead th {
        background-color: currentColor!important;
    }
</style>-->