<?php bs3_card($page_title, '', false); ?>
<body class="fix-header card-no-border fix-sidebar" >  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class=" col-lg-9 col-xlg-9 col-md-9 center" >

                <div class="card-body ">
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher")) { ?>
                        <div class="dt-buttons">
                            <div class="non_printable">
                                <a class="dt-button buttons-print" href="javascript:window.print()"  style="color:white; font-size: 16px;"><i class="fa fa-print" media='print' ></i> Print </a>

                            </div>
                        </div>
                    <?php } ?>
                    <div class="box_minaret_sat">
                        <div style="text-align: center;" >
                            <img width="45%" src="<?php echo base_url('assets'); ?>/uploads/images/minaret_sat.png"
                                 alt="user" class="profile-pic" />
                        </div>
                    </div>
                    <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>

                    <div class="box"  style='padding-bottom: 0px!important; background-image: url("<?php echo base_url('assets'); ?>/uploads/images/background_card.jpg");'>

                        <div style="text-align: center" >
                            <h1 style="font-size:55px; color: black;"> <strong><b> PROGRESS REPORT </b> </strong></h1>
                            <h1 style="font-size:22px; color: gray;"> <b>  PRESCHOOL 1 </b> </h1>
                            <h1 style="font-size:20px; color: black;"> <b>  Name:</b>   
                                <?php if (isset($student_info) && $student_info) echo $student_info->student_name . ' ' . $student_info->family_name ?> 
                            </h1>
                            <h1 style="font-size:20px; color: black; "> <b>  Teacher:</b> 
                                  <?php if (isset($user_login) && $user_login && (($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>
                                    <input class="col-md-6" style=' border: 0;
                                           outline: 0;
                                           background: transparent;
                                           border-bottom: 1px solid black;'
                                           id="teacher" name="teacher" type="textarea" disabled="true"
                                           > 
                                       <?php } else { ?>
                                           <?php if ($_current_year == $_archive_year) { ?>
                                        <input class="col-md-6" style=' border: 0;
                                               outline: 0;
                                               background: transparent;
                                               border-bottom: 1px solid black;'
                                               id="teacher" name="teacher" type="textarea"
                                               > 
                                           <?php } else { ?>
                                        <input disabled="true" class="col-md-6" style=' border: 0;
                                               outline: 0;
                                               background: transparent;
                                               border-bottom: 1px solid black;'
                                               id="teacher" name="teacher" type="textarea"
                                               > 
                                           <?php } ?>
                                       <?php } ?>
                            </h1>
                        </div>
                        <br> <br>
                        <div class="row">
                            <!--<div class="col-md-6">-->
                            <table class="table " style="width:40%"  >
                                <tbody  style="font-size:18px; color: black;">
                                    <?php
                                    $print_parent = FALSE;
                                    ?>
                                    <?php if (isset($children_subjects_of_arabic) && $children_subjects_of_arabic) { ?>
                                        <?php foreach ($children_subjects_of_arabic as $key => $value) { ?> 
                                            <?php if ($only_arabic == FALSE) { ?>
                                                <?php if ($print_parent == FALSE) { ?>
                                                    <tr>
                                                        <td style="width:20%; text-align: left;" colspan="2"><strong><b><?php echo lang($value->parent_subject_name) ?></b></strong></td>
                                                    </tr>
                                                    <tr style="height:10px">  </tr>
                                                    <tr>
                                                        <td style="width:20%; text-align: left;" ><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                        <td style="background-color: #f7dc175c!important; width:30%; text-align: left;" >
                                                            <b> 
                                                                <?php
                                                                if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                    echo $student_marks[$value->id]->mark_key;
                                                                else
                                                                    echo '';
                                                                ?> 
                                                            </b>
                                                        </td>
                                                        <?php $print_parent = TRUE; ?>
                                                    </tr>
                                                    <tr style="height:10px">  </tr>

                                                <?php } else { ?>
                                                    <tr>
                                                        <td style="width:20%; text-align: left;" ><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                        <td style="background-color: #f7dc175c!important; width:30%; text-align: left;" >
                                                            <b> 
                                                                <?php
                                                                if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                    echo $student_marks[$value->id]->mark_key;
                                                                else
                                                                    echo '';
                                                                ?> 
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="height:10px">  </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td style="width:20%; text-align: left;" ><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                    <td style="background-color: #f7dc175c!important; width:30%; text-align: left;" >
                                                        <b> 
                                                            <?php
                                                            if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                echo $student_marks[$value->id]->mark_key;
                                                            else
                                                                echo '';
                                                            ?> 
                                                        </b>
                                                    </td>

                                                </tr>
                                                <tr style="height:10px">  </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if (isset($subjects) && $subjects) { ?>
                                        <?php foreach ($subjects as $key => $value) { ?>  
                                            <tr>
                                                <td style="width:20%; text-align: left;" ><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                <td style="background-color: #f7dc175c!important; width:30%; text-align: left;" >
                                                    <b> 
                                                        <?php
                                                        if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                            echo $student_marks[$value->id]->mark_key;
                                                        else
                                                            echo '';
                                                        ?> 
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr style="height:10px">  </tr>
                                        <?php } ?>
                                    <?php } ?>

                                </tbody>
                            </table>


                            <table class="table " style="width:2%"  >

                            </table>
                            <table class="table  " style="width:55%" >
                                <thead class="days_absent">
                                    <tr>
                                        <th  style="font-size:24px; color: black; text-align: center; "><strong><b>TEACHER COMMENTS </b></strong></th>

                                    </tr>
                                    <tr>
                                        <th style="font-size:18px; color: black;  height:192px;"  > 
                                               <?php if (isset($user_login) && $user_login && (($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>
                                                <textarea id="teacher_comments" name="teacher_comments"  onkeyup="resizeTextarea('teacher_comments')" style="height: 100%; width: 100%; resize: none!important; border: 0; background-color:#f7dc175c;" disabled="true" ></textarea>
                                            <?php } else { ?>
                                                <?php if ($_current_year == $_archive_year) { ?>
                                                    <textarea id="teacher_comments" name="teacher_comments"  onkeyup="resizeTextarea('teacher_comments')" style="height: 100%; width: 100%; resize: none!important; border: 0; background-color:#f7dc175c;" ></textarea>
                                                <?php } else { ?>
                                                    <textarea disabled="true" id="teacher_comments" name="teacher_comments"  onkeyup="resizeTextarea('teacher_comments')" style="height: 100%; width: 100%; resize: none!important; border: 0; background-color:#f7dc175c;" ></textarea>
                                                <?php } ?>
                                            <?php } ?>
                                        </th>
                                    </tr>
                                       <script>
                            function resizeTextarea(id) {
                                var a = document.getElementById(id);
                                a.style.height = a.scrollHeight + 'px';
                            }
                        </script>
                                </thead>
                            </table>
                            <!--</div>-->
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <table class="table "   >
                                    <tbody  style="font-size:28px; color: black;">
                                    <div style="text-align: center; ">
                                        <tr>
                                            <th  style="color: black;  "><strong><b>GRADING KEY</b></strong></th>
                                        </tr>
                                        <tr>
                                            <td  style="background-color: #f7dc175c!important;"><b>  O- Outstanding </b></td>
                                        </tr>

                                        <tr>
                                            <td  style="background-color: #f7dc175c!important;"><b> S- Satisfactory </b></td>
                                        </tr>
                                        <tr>
                                            <td  style="background-color: #f7dc175c!important;"><b>  N- Needs Improvement  </b></td>
                                        </tr>
                                    </div>
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-7">
                                <table class="table "   >
                                    <tbody  style="color: black;">
                                        <tr>
                                            <th colspan="2" style="font-size:28px; color: black; text-align: center; "><strong><b>SOCIAL DEVELOPMENT </b></strong></th>
                                        </tr>
                                        <tr>
                                            <td   style="font-size:18px; text-align: left;"  ><strong><b> Participation</b></strong></td>
                                            <td class="col-md-3"  style="font-size:18px; background-color: #f7dc175c!important;  text-align: left;">
                                                <b>
                                                    <?php if (isset($student_info) && $student_info) echo $student_info->participates_in_class ?> 
                                                </b>
                                            </td>
                                        </tr>
                                        <tr style="height:10px">  </tr>
                                        <tr>
                                            <td   style="font-size:18px; text-align: left;"  ><strong><b> Attitude with peers</b></strong></td>
                                            <td  style="font-size:18px; background-color: #f7dc175c!important;  text-align: left;">
                                                <b> 
                                                    <?php if (isset($student_info) && $student_info) echo $student_info->shows_cooperative_attitude_with_peers ?> 
                                                </b>
                                            </td>
                                        </tr>
                                        <tr style="height:10px">  </tr>
                                        <tr>
                                            <td   style="font-size:18px; text-align: left;"  ><strong><b> Attitude with teachers</b></strong></td>
                                            <td  style="font-size:18px; background-color: #f7dc175c!important;  text-align: left;">
                                                <b>  
                                                    <?php if (isset($student_info) && $student_info) echo $student_info->shows_cooperative_attitude_with_teachers ?> 
                                                </b>
                                            </td>
                                        </tr>
                                        <tr style="height:10px">  </tr>
                                        <tr>
                                            <td   style="font-size:18px; text-align: left;"  ><strong><b> Listening </b></strong></td>
                                            <td  style="font-size:18px; background-color: #f7dc175c!important; text-align: left;">
                                                <b>  
                                                    <?php if (isset($student_info) && $student_info) echo $student_info->listens_when_others_are_speaking ?> 
                                                </b>
                                            </td>
                                        </tr>
                                        <tr style="height:10px">  </tr>
                                        <tr>
                                            <td   style="font-size:18px; text-align: left;"  ><strong><b> Respects material </b></strong></td>
                                            <td  style="font-size:18px; background-color: #f7dc175c!important; text-align: left;">
                                                <b> 
                                                    <?php if (isset($student_info) && $student_info) echo $student_info->respects_equipment_and_material ?> 
                                                </b>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                                  <?php bs3_date_without_icon('date_of_expired','',false, 'disabled=true'); ?>                   

                        <br>
                        <br>
                        <br>
                        <br>
                        <!--                        <div class="row" style="padding-left: 4px!important; padding-right: 3px!important; ">
                                                    <img width="100%" src="<?php echo base_url('assets'); ?>/uploads/images/4.png" />
                                                </div>-->
                    </div>
                    <br>
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher") && $_current_year == $_archive_year) { ?>
                        <div class="non_printable col-md-12" >
                            <button style="float: right;" type="button" onclick="edit_report_card()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
                        </div>
                    <?php } ?>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!--</div>-->
            <!-- Column -->
        </div> 
    </div> 
</body>
<?php bs3_card_f(); ?>

<style>
    .box {
        border: 1px solid #d8d8d8;
        /*padding: 10px;*/
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
        /*         position: relative;
          margin-bottom: 1.5em;
          height: 400px;
          overflow: hidden;*/
        /* Background image */
        background-repeat: no-repeat !important;
        background-position: center !important;
        background-size: cover !important;
    }

    .box_minaret_sat {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
    .center {
        margin: auto;
        width: 100%;
        padding: 10px;
    }

    .blank_row
    {
        background-color: #FFFFFF;
    }
    .th_teacher_comments1
    {  
        color: white; 
        background-color:#055ea0; 
        width:30%;
        text-align: left!important;
    }
    .th_teacher_comments2
    { 
        color: white; 
        background-color: #b1b1b1; 
        width:40%;
        text-align: left!important;
    }
    .blank_row td, .blank_row th .blank_row tr{
        /*border-color:  #FFFFFF!important;*/
        background-color: #f7dc1700;
        border:0;
    }

    .table td, .table th{
        border:0px solid #ffffff!important;
    }

    .days_absent td, .days_absent th{
        border: 0px solid #ffffff!important;
    }

    @media print
    {

        /*        .card_background{
                    margin-bottom: -188px;
                    background-image:url("<?php echo base_url('assets'); ?>/uploads/images/background_card.jpg")!important;
                }*/

        * {-webkit-print-color-adjust:exact;}

    }
</style>


<script>
    $(document).ready(function () {
        var semester =<?php echo $this->uri->segment(3); ?>;
        var student_record_id =<?php echo $this->uri->segment(4); ?>;
        $.ajax({
            url: "<?php echo base_url("report_card/get_p1_report_card/") ?>" + semester + '/' + student_record_id,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var teacher = data.data.teacher;
                var teacher_comments = data.data.teacher_comments;
                var date_of_expired = data.data.date_of_expired;
                
                if(date_of_expired ==='0000-00-00'){
                    date_of_expired='';
                }

                $("#teacher").val(teacher);
                $("#teacher_comments").val(teacher_comments);
                $("#inputDate_of_expired").val(date_of_expired);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error');
            }
        });
    });

    function edit_report_card() {
        var data = $("#form_crud1").serialize();
        var semester =<?php echo $this->uri->segment(3); ?>;
        var student_record_id =<?php echo $this->uri->segment(4); ?>;

        var url = "<?php echo base_url("report_card/edit_p1_report_card/") ?>" + semester + '/' + student_record_id;
        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {
                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });
    }
</script>

<style>
    @media print
    {
        .non_printable {display:none;}
        .css-background-container input[type='checkbox']:checked::before{
            user-select:all !important;
        }
        body * {
            visibility: hidden;
        }
        .box * {
            visibility: visible;
        }
        .box_minaret_sat * {
            visibility: visible;
        }


        * {
            -webkit-print-color-adjust: exact;
            print-color-adjust: exact;
        }
    }
</style>





