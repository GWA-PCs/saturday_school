<?php bs3_card($page_title, '', false); ?>
<body class="fix-header card-no-border fix-sidebar">  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->

            <!-- Column -->
            <!-- Column -->
            <!--<div class="col-lg-8 col-xlg-9 col-md-7">-->
            <div class=" col-lg-12 col-xlg-12 col-md-12 center" >

                <div class="card-body ">
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher")) { ?>
                        <div class="dt-buttons">
                            <div class="non_printable">
                                <a class="dt-button buttons-print" href="javascript:window.print()"  style="color:white; font-size: 16px;"><i class="fa fa-print" media='print' ></i> Print </a>

                            </div>
                        </div>
                    <?php } ?>
                    <div class="box" style="padding: 50px!important;">
                        <div class="card-body">
                            <div style="text-align: center">
                                <img width="25%" src="<?php echo base_url('assets'); ?>/uploads/images/minaret_sat.png"
                                     alt="user" class="profile-pic" />
                                <br> <br> 
                                <h1 style="font-size:36px; color: black!important;"> <b> Progress Report </b> </h1>
                            </div>
                            <br> <br>

                            <table style="width:100%">
                                <tbody  style="font-size:20px; color: black!important;">
                                    <tr>
                                        <td  style="font-size:21px;"><b> GRADING KEY </b><td>

                                    </tr>
                                    <tr>
                                        <td> O- Outstanding</td>
                                        <td style="width: 80px;"><td>
                                        <td>NAME:   <b> <?php if (isset($student_info) && $student_info) echo $student_info->student_name . ' ' . $student_info->family_name ?>  </b> </td>
                                    </tr>
                                    <tr>
                                        <td>  S- Satisfactory</td>
                                        <td colspan="4"><td>
                                    </tr>
                                    <tr>
                                        <td> N- Needs Improvement</td>
                                        <td style="width: 80px;"><td>
                                        <td>LEVEL: <b>   <?php if (isset($student_info) && $student_info) echo $student_info->grade_name; ?>  </b>  </td>

                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <div class="row">
                                <table class="table table-bordered" style="width:100%">

                                    <thead  style="font-size:16px; " >
                                        <tr>
                                            <th style="width:20%; color: white!important; background-color:#055ea0!important;" colspan="1"><b> SUBJECT</b></th>
                                            <th style="width:20%; color: white!important; background-color:#055ea0!important;" ><b> GRADE</b></th>
                                            <th style="width:60%; color: white!important; background-color:#055ea0!important; " ><b> COMMENTS</b></th>
                                        </tr>

                                    </thead>
                                    <tbody  style="font-size:16px; color: white!important;">
                                        <?php
                                        $print_parent = FALSE;
                                        $arabic_comments = '';
                                        ?>

                                        <?php if (isset($children_subjects_of_arabic) && $children_subjects_of_arabic) { ?>
                                            <?php
                                            foreach ($children_subjects_of_arabic as $key => $value) {
                                                if (isset($student_marks) && $student_marks && isset($student_marks[$value->id])) {
                                                    $arabic_comments .= $student_marks[$value->id]->mark_note . '<br>';
                                                }
                                            }
                                            ?>
                                            <?php foreach ($children_subjects_of_arabic as $key => $value) { ?> 
                                                <?php if ($only_arabic == FALSE) { ?>
                                                    <?php if ($print_parent == FALSE) { ?>
                                                        <tr>
                                                            <td style="background-color: #b1b1b1!important;" colspan="2"><strong><b><?php echo lang($value->parent_subject_name) ?></b></strong></td>

                                                            <td style = "background-color: #d2d2d2!important; color: black!important; text-align: left;" rowspan = "<?php echo $count_children + 1 ?>">
                                                                <b>
                                                                    <?php
                                                                    echo $arabic_comments;
                                                                    ?> 
                                                                </b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                            <td style="background-color: #d2d2d2!important; color: black!important;" colspan="1">
                                                                <b> 
                                                                    <?php
                                                                    if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                        echo $student_marks[$value->id]->mark_key;
                                                                    else
                                                                        echo '';
                                                                    ?> 
                                                                </b>
                                                            </td>
                                                            <?php $print_parent = TRUE; ?>
                                                        </tr>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                            <td style="background-color: #d2d2d2!important; color: black!important;" colspan="1">
                                                                <b> 
                                                                    <?php
                                                                    if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                        echo $student_marks[$value->id]->mark_key;
                                                                    else
                                                                        echo '';
                                                                    ?> 
                                                                </b>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php }else { ?>
                                                    <tr>
                                                        <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                        <td style="background-color: #d2d2d2!important; color: black!important;" colspan="1">
                                                            <b> 
                                                                <?php
                                                                if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                    echo $student_marks[$value->id]->mark_key;
                                                                else
                                                                    echo '';
                                                                ?> 
                                                            </b>
                                                        </td>
                                                        <td style = "background-color: #d2d2d2!important; color: black!important; text-align: left;">
                                                            <b>
                                                                <?php
                                                                echo $arabic_comments;
                                                                ?> 
                                                            </b>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>


                                        <?php if (isset($subjects) && $subjects) { ?>
                                            <?php foreach ($subjects as $key => $value) { ?>  
                                                <tr>
                                                    <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                    <td style="background-color: #d2d2d2!important; color: black!important;" colspan="1">
                                                        <b> 
                                                            <?php
                                                            if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                echo $student_marks[$value->id]->mark_key;
                                                            else
                                                                echo '';
                                                            ?> 
                                                        </b>
                                                    </td>
                                                    <td style="background-color: #d2d2d2!important; color: black!important; text-align: left;" colspan="3">
                                                        <b> 
                                                            <?php
                                                            if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                echo $student_marks[$value->id]->mark_note;
                                                            else
                                                                echo '';
                                                            ?> 
                                                        </b>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>
                            <div class="row">
                                <div class="col-md-9">
                                    <table class="table">
                                        <thead  style="font-size:16px; color: white!important; " >
                                            <tr>
                                                <th class="th_teacher_comments1" style=" background-color:#055ea0!important; " > Quran Teacher</th>
                                                <th class="th_teacher_comments2" style=" background-color: #b1b1b1!important;" >
                                                    <?php if (isset($user_login) && $user_login && (($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>
                                                        <input  id="quran_teacher" name="quran_teacher" style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" disabled="true">
                                                    <?php } else { ?>
                                                        <?php if ($_current_year == $_archive_year) { ?>
                                                            <input  id="quran_teacher" name="quran_teacher" style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" >
                                                        <?php } else { ?>
                                                            <input  disabled="true" id="quran_teacher" name="quran_teacher" style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" >
                                                        <?php } ?>                                                   
                                                    <?php } ?>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th  class="th_teacher_comments1"  style=" background-color:#055ea0!important; "  > Arabic  Teacher</th>
                                                <th class="th_teacher_comments2" style=" background-color: #b1b1b1!important;">
                                                    <?php if (isset($user_login) && $user_login  &&(($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>
                                                        <input id="arabic_teacher" name="arabic_teacher"  style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" disabled="true">
                                                    <?php } else { ?>
                                                        <?php if ($_current_year == $_archive_year) { ?>
                                                            <input id="arabic_teacher" name="arabic_teacher"  style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" >
                                                        <?php } else { ?>
                                                            <input  disabled="true"  id="arabic_teacher" name="arabic_teacher"  style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" >
                                                        <?php } ?>
                                                    <?php } ?>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th  class="th_teacher_comments1" style=" background-color:#055ea0!important; "  > Islamic Studies Teacher</th>
                                                <th class="th_teacher_comments2" style=" background-color: #b1b1b1!important;" >
                                                    <?php if (isset($user_login) && $user_login && (($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>
                                                        <input id="islamic_studies_teacher" name="islamic_studies_teacher" style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea" disabled="true">
                                                    <?php } else { ?>
                                                        <?php if ($_current_year == $_archive_year) { ?>
                                                            <input id="islamic_studies_teacher" name="islamic_studies_teacher" style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea">
                                                        <?php } else { ?>
                                                            <input disabled="true" id="islamic_studies_teacher" name="islamic_studies_teacher" style="border: 0; background-color:#b1b1b1!important; height: 100%; width: 100%;" type="textarea">
                                                        <?php } ?>
                                                    <?php } ?>
                                                </th>
                                            </tr>
                                        </thead>

                                    </table>

                                </div>
                                <div class="col-md-3">
                                    <table class="table" >
                                        <thead class="days_absent">
                                            <tr>
                                                <th  style="font-size:16px; color: black!important;"><b>DAYS ABSENT </b></th>

                                            </tr>
                                            <tr>
                                                <td style="font-size:18px; color: black!important; background-color: #d2d2d2!important; height:131px;"  ><?php echo $absent_days ?> </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>

                        <?php bs3_date_without_icon('date_of_expired', '', false, 'disabled=true'); ?>
                    </div>
                    <br>
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher") && $_current_year == $_archive_year) { ?>
                        <div class="non_printable col-md-12" >
                            <button style="float: right;" type="button" onclick="edit_report_card()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
                        </div>
                    <?php } ?>
                    <?php echo form_close(); ?>
                </div>
            </div>

        </div>

        <!--</div>-->
        <!-- Column -->
    </div> 
</div> 
</body>
<?php bs3_card_f(); ?>

<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
    .center {
        margin: auto;
        width: 100%;
        padding: 10px;
    }

    .blank_row
    {
        background-color: #FFFFFF;
    }
    .th_teacher_comments1
    {  
        color: white;

        /*width:30%;*/
        text-align: left!important;
    }
    .th_teacher_comments2
    { 
        color: white!important;

        /*width:40%;*/
        text-align: left!important;
    }
    .blank_row td, .blank_row th .blank_row tr{
        border-color:  #FFFFFF!important;
        border:0;
    }

    .table td, .table th{
        border: 12px solid #ffffff!important;
    }

    .days_absent td, .days_absent th{
        border: 0px solid #ffffff!important;
    }
</style>

<script>
    $(document).ready(function () {
        var semester =<?php echo $this->uri->segment(3); ?>;
        var student_record_id =<?php echo $this->uri->segment(4); ?>;
        $.ajax({
            url: "<?php echo base_url("report_card/get_elem_report_card/") ?>" + semester + '/' + student_record_id,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var arabic_teacher = data.data.arabic_teacher;
                var quran_teacher = data.data.quran_teacher;
                var islamic_studies_teacher = data.data.islamic_studies_teacher;
                var date_of_expired = data.data.date_of_expired;

                if (date_of_expired === '0000-00-00') {
                    date_of_expired = '';
                }
                
                $("#arabic_teacher").val(arabic_teacher);
                $("#quran_teacher").val(quran_teacher);
                $("#islamic_studies_teacher").val(islamic_studies_teacher);
                $("#inputDate_of_expired").val(date_of_expired);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error');
            }
        });
    });

    function edit_report_card() {
        var data = $("#form_crud1").serialize();
        var semester =<?php echo $this->uri->segment(3); ?>;
        var student_record_id =<?php echo $this->uri->segment(4); ?>;

        var url = "<?php echo base_url("report_card/edit_elem_report_card/") ?>" + semester + '/' + student_record_id;
        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {
                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });
    }
</script>


<style>
    @media print
    {
        .non_printable {display:none;}
        .css-background-container input[type='checkbox']:checked::before{
            user-select:all !important;
        }
        body * {
            visibility: hidden;
        }
        .box * {
            visibility: visible;
        }
        .box_minaret_sat * {
            visibility: visible;
        }


        * {
            -webkit-print-color-adjust: exact;
            print-color-adjust: exact;
        }
    }
</style>
