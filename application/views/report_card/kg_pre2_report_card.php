<?php bs3_card($page_title, '', false); ?>
<body class="fix-header card-no-border fix-sidebar">  
    <div class="container-fluid"> 
        <!-- Row -->
        <div class="row">
            <!-- Column -->

            <!-- Column -->
            <!-- Column -->
            <!--<div class="col-lg-8 col-xlg-9 col-md-7">-->
            <div class=" col-lg-12 col-xlg-12 col-md-12 center" >

                <div class="card-body ">
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher")) { ?>
                        <div class="dt-buttons">
                            <div class="non_printable">
                                <a class="dt-button buttons-print" href="javascript:window.print()"  style="color:white; font-size: 16px;"><i class="fa fa-print" media='print' ></i> Print </a>

                            </div>
                        </div>
                    <?php } ?>
                    <div class="box" style="padding: 50px!important;">
                        <div class="card-body">
                            <div style="text-align: center">
                                <img width="25%" src="<?php echo base_url('assets'); ?>/uploads/images/minaret_sat.png"
                                     alt="user" class="profile-pic" />
                                <br> <br> 
                                <h1 style="font-size:36px; color: black;"> <b> Progress Report </b> </h1>
                                <h3 style="font-size:20px; color: black;"> <b>   Kindergarten, Preschool2 </b> </h3>

                            </div>
                            <br> <br>
                            <!--<div class="row" >-->
                            <!--<div class="col-md-12">-->
                            <?php echo form_open("", "id='form_crud1'" . " role='form'   enctype= 'multipart/form-data'"); ?>

                            <table width="100% ">
                                <tbody  style="font-size:18px; color: black;">
                                    <tr>
                                        <td width="3%" ></td>
                                        <td>NAME: <b> <?php if (isset($student_info) && $student_info) echo $student_info->student_name . ' ' . $student_info->family_name ?>  </b> <td>
                                        <td >TEACHER: 
                                            <?php if (isset($user_login) && $user_login && (($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>

                                                <input class="col-md-8" style=' border: 0;
                                                       outline: 0;
                                                       background: transparent;
                                                       border-bottom: 1px solid black;'
                                                       id="teacher" name="teacher" type="textarea" disabled="true"
                                                       >
                                                   <?php } else { ?>
                                                       <?php if ($_current_year == $_archive_year) { ?>
                                                    <input class="col-md-8" style=' border: 0;
                                                           outline: 0;
                                                           background: transparent;
                                                           border-bottom: 1px solid black;'
                                                           id="teacher" name="teacher" type="textarea" 
                                                           >
                                                       <?php } else { ?>
                                                    <input disabled="true" class="col-md-8" style=' border: 0;
                                                           outline: 0;
                                                           background: transparent;
                                                           border-bottom: 1px solid black;'
                                                           id="teacher" name="teacher" type="textarea" 
                                                           >
                                                       <?php } ?>
                                                   <?php } ?>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                            <!--</div>-->
                            <!--</div>-->
                            <br>
                            <br>
                            <br>
                            <div class="row" >
                                <div class="col-md-4">

                                    <table class="table table-bordered" >

                                        <thead  style="font-size:16px; color: white; " >
                                            <tr >
                                                <th style="background-color:#055ea0!important;"><b> SUBJECT</b></th>
                                                <th style="background-color:#055ea0!important;"><b> GRADE</b></th>

                                            </tr>

                                        </thead>
                                        <tbody  style="font-size:16px; color: white;">
                                            <?php
                                            $print_parent = FALSE;
                                            ?>
                                            <?php if (isset($children_subjects_of_arabic) && $children_subjects_of_arabic) { ?>
                                                <?php foreach ($children_subjects_of_arabic as $key => $value) { ?> 
                                                    <?php if ($only_arabic == FALSE) { ?>
                                                        <?php if ($print_parent == FALSE) { ?>
                                                            <tr>
                                                                <td style="background-color: #b1b1b1!important;" colspan="2"><strong><b><?php echo lang($value->parent_subject_name) ?></b></strong></td>

                                                            </tr>
                                                            <tr>
                                                                <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                                <td style="background-color: #eaeaea!important; color: black!important;" colspan="1">
                                                                    <b> 
                                                                        <?php
                                                                        if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                            echo $student_marks[$value->id]->mark_key;
                                                                        else
                                                                            echo '';
                                                                        ?> 
                                                                    </b>
                                                                </td>
                                                                <?php $print_parent = TRUE; ?>
                                                            </tr>

                                                        <?php } else { ?>
                                                            <tr>
                                                                <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                                <td style="background-color: #eaeaea!important; color: black!important;" colspan="1">
                                                                    <b> 
                                                                        <?php
                                                                        if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                            echo $student_marks[$value->id]->mark_key;
                                                                        else
                                                                            echo '';
                                                                        ?> 
                                                                    </b>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                            <td style="background-color: #eaeaea!important; color: black!important;" colspan="1">
                                                                <b> 
                                                                    <?php
                                                                    if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                        echo $student_marks[$value->id]->mark_key;
                                                                    else
                                                                        echo '';
                                                                    ?> 
                                                                </b>
                                                            </td>

                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if (isset($subjects) && $subjects) { ?>
                                                <?php foreach ($subjects as $key => $value) { ?>  
                                                    <tr>
                                                        <td style="background-color: #b1b1b1!important;" colspan="1"><strong><b><?php echo $value->subject_name ?></b></strong></td>
                                                        <td style="background-color: #eaeaea!important; color: black!important;" colspan="1">
                                                            <b> 
                                                                <?php
                                                                if (isset($student_marks) && $student_marks && isset($student_marks[$value->id]))
                                                                    echo $student_marks[$value->id]->mark_key;
                                                                else
                                                                    echo '';
                                                                ?> 
                                                            </b>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-8">
                                    <table class="table table-bordered">

                                        <thead  style="font-size:16px; color: white;" >
                                            <tr>
                                                <th style=" background-color:#055ea0!important;"><b> GENERAL SOCIAL DEVLOPMENT</b></th>
                                                <th style=" background-color:#055ea0!important;"><b> GRADE</b></th>
                                            </tr>

                                        </thead>
                                        <tbody  style="font-size:16px; color: white;">
                                            <tr>
                                                <td style="background-color: #b1b1b1!important;"><b> Participates in class</b></td>
                                                <td style="background-color: #eaeaea!important; color: black;">
                                                    <b>
                                                        <?php if (isset($student_info) && $student_info) echo $student_info->participates_in_class ?> 
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #b1b1b1!important;"><b> Shows cooperative attitude with peers</b></td>
                                                <td style="background-color: #eaeaea!important; color: black;">
                                                    <b> 
                                                        <?php if (isset($student_info) && $student_info) echo $student_info->shows_cooperative_attitude_with_peers ?> 
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #b1b1b1!important;"><b> Shows cooperative attitude with teachers</b></td>
                                                <td style="background-color: #eaeaea!important; color: black;">
                                                    <b>  
                                                        <?php if (isset($student_info) && $student_info) echo $student_info->shows_cooperative_attitude_with_teachers ?> 
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #b1b1b1!important;"><b> Listens when others are speaking</b></td>
                                                <td style="background-color: #eaeaea!important; color: black;">
                                                    <b>  
                                                        <?php if (isset($student_info) && $student_info) echo $student_info->listens_when_others_are_speaking ?> 
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #b1b1b1!important;"><b> Respects equipment and material</b></td>
                                                <td style="background-color: #eaeaea!important; color: black;">
                                                    <b>
                                                        <?php if (isset($student_info) && $student_info) echo $student_info->respects_equipment_and_material ?> 
                                                    </b>
                                                </td>
                                            </tr>


                                            <!-- Grading Key -->
                                            <tr class="grading_key">
                                                <td  style="font-size:18px;  color: black;text-align: left;"><b> GRADING KEY </b><td>
                                            </tr>
                                            <tr class="grading_key">
                                                <td style="font-size:16px; color: black;text-align: left;"> O- Outstanding</td>
                                            </tr>
                                            <tr class="grading_key">
                                                <td style="font-size:16px; color: black;text-align: left;">  S- Satisfactory</td>
                                            </tr>
                                            <tr class="grading_key">
                                                <td style="font-size:16px; color: black;text-align: left;"> N- Needs Improvement</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <table class="table  " >
                                        <thead class="days_absent">
                                            <tr>
                                                <th  style="font-size:16px; color: black; text-align: left;"><strong><b>DAYS ABSENT </b></strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="font-size:18px; color: black; background-color: #d2d2d2!important; height:131px;"  > <?php echo $absent_days ?>  </td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </div>
                                <div class="col-md-8">
                                    <table class="table  " >
                                        <thead class="days_absent">
                                            <tr>
                                                <th  style="font-size:16px; color: black; text-align: left; "><strong><b>TEACHER COMMENTS </b></strong></th>

                                            </tr>
                                            <tr>
                                                <th style="font-size:18px; color: black; background-color: #d2d2d2!important; height:131px;"  > 
                                                      <?php if (isset($user_login) && $user_login && (($user_login->type == "parent" ) || ($user_login->type == "student" ))) { ?>
                                                        <textarea id="teacher_comments" name="teacher_comments" onkeyup="resizeTextarea('teacher_comments')" style="resize: none!important; height: 100%; width: 100%;  border: 0; background-color:#d2d2d2!important;" disabled="true"></textarea>
                                                    <?php } else { ?>
                                                        <?php if ($_current_year == $_archive_year) { ?>
                                                            <textarea id="teacher_comments" name="teacher_comments" onkeyup="resizeTextarea('teacher_comments')" style="resize: none!important; height: 100%; width: 100%;  border: 0; background-color:#d2d2d2!important;" ></textarea>
                                                        <?php } else { ?>
                                                            <textarea disabled="true" id="teacher_comments" name="teacher_comments" onkeyup="resizeTextarea('teacher_comments')" style="resize: none!important; height: 100%; width: 100%;  border: 0; background-color:#d2d2d2!important;" ></textarea>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <script>
                            function resizeTextarea(id) {
                                var a = document.getElementById(id);
                                a.style.height = a.scrollHeight + 'px';
                            }
                        </script>
                        <?php bs3_date_without_icon('date_of_expired', '', false, 'disabled=true'); ?>
                    </div>

                    <br>
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher") && ($_current_year == $_archive_year)) { ?>
                        <div class="non_printable col-md-12" >
                            <button style="float: right;" type="button" onclick="edit_report_card()" class="btn btn-info text-left btn-rounded hvr-icon-spin hvr-shadow btnSave" value="true" name="create"><?php echo lang('save'); ?></button>
                        </div>
                    <?php } ?>
                    <?php echo form_close(); ?>


                </div>
            </div>
            <!--</div>-->
            <!-- Column -->
        </div> 
    </div> 
</body>
<?php bs3_card_f(); ?>

<style>
    .box {
        border: 1px solid #d8d8d8;
        padding: 10px;
        box-shadow: 0 3px 8px rgba(0,0,0,.25);
    }
    .center {
        margin: auto;
        width: 100%;
        padding: 10px;
    }

    .blank_row
    {
        background-color: #FFFFFF;
    }
    .th_teacher_comments1
    {  
        color: white; 
        background-color:#055ea0; 
        width:30%;
        text-align: left!important;
    }
    .th_teacher_comments2
    { 
        color: white; 
        background-color: #b1b1b1; 
        width:40%;
        text-align: left!important;
    }
    .blank_row td, .blank_row th .blank_row tr{
        border-color:  #FFFFFF!important;
        border:0;
    }

    .table td, .table th{
        border: 10px solid #ffffff!important;
    }

    .grading_key td{
        padding: 0px!important;
        /*border: 0px solid #ffffff!important;*/
    }

    .days_absent td, .days_absent th{
        border: 0px solid #ffffff!important;
    }
</style>


<script>
    $(document).ready(function () {
        var semester =<?php echo $this->uri->segment(3); ?>;
        var student_record_id =<?php echo $this->uri->segment(4); ?>;
        $.ajax({
            url: "<?php echo base_url("report_card/get_kg_pre2_report_card/") ?>" + semester + '/' + student_record_id,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                var teacher = data.data.teacher;
                var teacher_comments = data.data.teacher_comments;
                var date_of_expired = data.data.date_of_expired;

                if (date_of_expired === '0000-00-00') {
                    date_of_expired = '';
                }

                $("#teacher").val(teacher);
                $("#teacher_comments").val(teacher_comments);
                $("#inputDate_of_expired").val(date_of_expired);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error');
            }
        });
    });

    function edit_report_card() {
        var data = $("#form_crud1").serialize();
        var semester =<?php echo $this->uri->segment(3); ?>;
        var student_record_id =<?php echo $this->uri->segment(4); ?>;

        var url = "<?php echo base_url("report_card/edit_kg_pre2_report_card/") ?>" + semester + '/' + student_record_id;
        $.ajax({
            url: url,
            dataType: "json",
            data: data,
            type: "post",
            success: function (data) {
                if (data.status == "200") {
                    swal({
                        title: "<?php echo lang('success') ?>",
                        text: data.message,
                        type: "success",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "400") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.message,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close') ?>",
                    });
                } else if (data.status == "408") {
                    swal({
                        title: "<?php echo lang('error') ?>",
                        text: data.data,
                        type: "error",
                        confirmButtonText: "<?php echo lang('close'); ?>",
                    });
                }
            },
            error: function () {
//                alert("Error");
            }
        });
    }
</script>



<style>
    @media print
    {
        .non_printable {display:none;}
        .css-background-container input[type='checkbox']:checked::before{
            user-select:all !important;
        }
        body * {
            visibility: hidden;
        }
        .box * {
            visibility: visible;
        }
        .box_minaret_sat * {
            visibility: visible;
        }


        * {
            -webkit-print-color-adjust: exact;
            print-color-adjust: exact;
        }
    }
</style>
