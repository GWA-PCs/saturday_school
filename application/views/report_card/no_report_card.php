<?php bs3_card($page_title); ?> 
<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h2 class="text-uppercase">Report Card Not Found !</h2>
            <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" )) { ?>
                <h3 class="text-muted m-t-30 m-b-30">Please select report card name for this grade </h3>
                <a href="<?php echo base_url('grade/index_grade') ?>" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Go to grades page</a> </div>
            <?php } ?>
    </div>
</section>

<?php bs3_card_f(); ?>
