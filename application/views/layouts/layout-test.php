<!DOCTYPE html>
<html lang="<?php echo $lang ?>" dir="<?php echo $dir ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/assets/images/favicon.png">
    <title><?php echo $page_title; ?></title>
    <!-- Bootstrap Core CSS -->
    <?php if ($dir == "rtl") { ?>
        <link href="<?php echo base_url("assets/assets/node_modules/bootstrap/css11/bootstrap.min.css") ?>" rel="stylesheet">

    <?php } else { ?>

        <link href="<?php echo base_url("assets/assets/node_modules/bootstrap/css/bootstrap.min.css") ?>" rel="stylesheet">

    <?php } ?>
    <link href="<?php echo base_url("assets/assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/sweetalert/sweetalert.css") ?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/assets/hover.css") ?>" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="<?php echo base_url("assets/assets/node_modules/toast-master/css/jquery.toast.css") ?>" rel="stylesheet">


    <!-- page CSS -->
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/switchery/dist/switchery.min.css") ?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/multiselect/css/multi-select.css") ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?php echo base_url("assets/assets/node_modules/css-chart/css-chart.css") ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("assets/$dir/css/style.css") ?>" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?php echo base_url("assets/$dir/css/pages/dashboard1.css") ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url("assets/$dir/css/colors/default.css") ?>" id="theme" rel="stylesheet">
    <!--icon-->
    <link href="<?php echo base_url("assets/$dir/scss/icons/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/simple-line-icons/css/simple-line-icons.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/weather-icons/css/weather-icons.min.css"); ?>" rel="stylesheet">
    <!--    <link href="--><?php //echo base_url("assets/$dir/scss/icons/iconmind/iconmind.css");           ?><!--" id="theme" rel="stylesheet">-->
    <link href="<?php echo base_url("assets/$dir/scss/icons/themify-icons/themify-icons.css"); ?>" id="theme" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/flag-icon-css/flag-icon.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/material-design-iconic-font/css/materialdesignicons.min.css"); ?>" rel="stylesheet">
    <!--icon-->
    <link href="<?php echo base_url("assets/$dir/css/spinners.css"); ?>" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/timepicker/bootstrap-timepicker.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css") ?>" rel="stylesheet">
    <!-- Clock picker plugins css -->
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css") ?>"
          rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css") ?>" rel="stylesheet">

    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url('') ?>/assets/fonts/stylesheet.css" rel="stylesheet" type="text/css"/>
    <!-- Auto Search -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script src="<?php echo base_url("assets/assets/node_modules/jquery/jquery.min.js"); ?>"></script>
     <!-- Auto Search -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <!-- End Auto Search -->
    <script src="<?php echo base_url("assets/assets/node_modules/bootstrap/js/popper.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/assets/node_modules/bootstrap/js11/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/$dir/js/custom.js"); ?>"></script>

    <script src="<?php echo base_url("assets/assets/node_modules/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/assets/node_modules/sweetalert/sweetalert.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/assets/node_modules/sweetalert/jquery.sweet-alert.custom.js"); ?>"></script>
    <script src="<?php echo base_url("assets/arabic_lang.js"); ?>"></script>


    <script>

        var save_method; //for save method string
        var table;
        var base_url = '<?php echo base_url();?>';

        function reload_table() {
            table.ajax.reload(null, false); //reload datatable ajax
        }
        <?php if(isset($show_object_link)){ ?>
        $(document).ready(function () {

            //datatables
            table = $('.object_table').DataTable({
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'someID');
                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'excel', 'print'
                ],
                <?php if($lang == "ar") {?>
                "language": {
                    "sProcessing": "جاري التحميل...",
                    "sLengthMenu": "إظهار _MENU_ عناصر",
                    "sZeroRecords": "لم يُعثر على أية سجلات",
                    "sInfo": "العناصر الظاهرة من _START_ إلى _END_ من أصل _TOTAL_ عنصر",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجلّ",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                },
                <?php } ?>
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering": false,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url($show_object_link)?>",
                    "type": "POST"
                },
                //Set column definition initialisation properties.


                "columnDefs": [
                    {
                        "targets": [-1], //last column
                        "orderable": false, //set not orderable
                    },
                    {
                        "targets": [-2], //2 last column (photo)
                        "orderable": false, //set not orderable
                    },
                ],

            });

        });
        <?php } ?>
        <?php if(isset($modal_name)){?>
        function add_object(modal_id) {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            var formData = new FormData($('#form_crud')[0]);
            for (var pair of formData.entries()) {
                var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
                if (hidden_in == "add") {
                    $('[name=' + pair[0] + ']').parent().parent().hide();
                } else {
                    $('[name=' + pair[0] + ']').parent().parent().show();
                }
            }
            save_method = 'add';
            $('#form_crud')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(show_modal_id).modal('show'); // show bootstrap modal
            var add_object_title = "<?php echo $add_object_title; ?>";
            $('.modal-title').text(add_object_title); // Set Title to Bootstrap modal title

            $('#photo-preview').hide(); // hide photo preview modal

            $('#label-photo').text('Upload Photo'); // label photo upload
        }
        <?php } ?>
        <?php if (isset($get_object_link)) { ?>
        function edit_object(id) {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            save_method = 'update';
            $('#form_crud')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string

            var formData = new FormData($('#form_crud')[0]);
            for (var pair of formData.entries()) {
                var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
                if (hidden_in == "update") {
                    $('[name=' + pair[0] + ']').parent().parent().hide();
                } else {
                    $('[name=' + pair[0] + ']').parent().parent().show();
                }
            }
            $(show_modal_id).modal('show'); // show bootstrap modal
            //Ajax Load data from ajax
            $.ajax({
                url: "<?php echo base_url($get_object_link)?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    for (var pair of formData.entries()) {
                        $('[name=' + pair[0] + ']').val(data[pair[0]]);
                    }
                    var update_object_title = "<?php echo $update_object_title; ?>";
                    $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data from ajax');
                }
            });
        }
        <?php } ?>
        function get_response(data) {
            var show_modal_id = "<?php echo "#$modal_name" ?>";

            if (data.status == "200") {
                // message success data.message
                swal({
                    title: "<?php echo lang('success') ?>",
                    text: data.message,
                    type: "success",
                    confirmButtonText: "<?php echo lang('close') ?>",

                });
                $(show_modal_id).modal('hide');
                reload_table();


            } else if (data.status == "201") {
                // valedation error data.data
                messages_error("<?php echo lang('error'); ?>", data.data);

            } else if (data.status == "202") {

                swal({
                    title: "<?php echo lang('error') ?>",
                    text: data.message,
                    type: "error",
                    confirmButtonText: "<?php echo lang('close') ?>",

                });
            } else if (data.status == "400") {
                // message error in controller data.message
            } else if (data.status == "401") {
                // message error in information data.message and status
            }
        }
        <?php if (isset($add_object_link) || isset($update_object_link)) { ?>
        function save() {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable
            var url;
            if (save_method == 'add') {
                url = "<?php echo site_url($add_object_link)?>";
            } else {
                url = "<?php echo site_url($update_object_link)?>";
            }
            // ajax adding data to database
            var formData = new FormData($('#form_crud')[0]);
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) //if success close modal and reload ajax table
                    {
                        get_response(data);
                    }
                    else {
                        for (var i = 0; i < data.inputerror.length; i++) {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
                    $('.btnSave').text('save'); //change button text
                    $('.btnSave').attr('disabled', false); //set button enable
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                    $('.btnSave').text('save'); //change button text
                    $('.btnSave').attr('disabled', false); //set button enable
                }
            });
        }
        <?php } ?>

        <?php if (isset($delete_object_link)) { ?>

        function delete_object(id) {
            swal({
                title: "<?php echo lang('delete_confirmation_text') ?>",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "<?php echo lang('close') ?>",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('delete') ?>",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: "<?php echo site_url($delete_object_link)?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        //if success reload ajax table
                        // $(show_modal_id).modal('hide');
                        swal("<?php echo lang('delete_success') ?>", "", "success");
                        reload_table();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });


            });
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            //if (confirm('Are you sure delete this data?')) {
            //    // ajax delete data to database
            //    $.ajax({
            //        url: "<?php //echo site_url($delete_object_link)?>///" + id,
            //        type: "POST",
            //        dataType: "JSON",
            //        success: function (data) {
            //            //if success reload ajax table
            //            $(show_modal_id).modal('hide');
            //            reload_table();
            //        },
            //        error: function (jqXHR, textStatus, errorThrown) {
            //            alert('Error deleting data');
            //        }
            //    });
            //
            //}
        }

        <?php } ?>



    </script>

    <script type="text/javascript">
        function logout () {

            var user_id = -1;
            var token = "";

            if (localStorage) {
                user_id = localStorage.getItem("user_id");
                token = localStorage.getItem("token");
            }
            var logoutUrl = '<?php echo base_url('auth/logout')?>';
            var loginUrl = '<?php echo base_url('auth/login')?>';

            $.ajax({
                url:logoutUrl,
                type: "POST",
                headers:{"User-ID":user_id,"Authorization":token},
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    if(data.status == 200) {
                        messages_success("<?php echo lang('success'); ?>", data.data);
                        redirectTo(loginUrl);
                    }else if (data.status == 400) {
                        messages_error("<?php echo lang('error'); ?>", data.message);
                    }
                }
            });
            console.log(user_id);
            console.log(token);
        }

        function redirectTo(page) {
            window.location.replace(page);
        }

    </script>


</head>
<style>
    p, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .h4, th, td, table.dataTable thead .sorting, table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc, table.dataTable thead .sorting_asc_disabled, table.dataTable thead .sorting_desc_disabled, button, body {
        font-family: 'Droid Arabic Kufi';
    }

</style>
<?php if ($function_name != 'login') { ?>
<body class="card-no-border mini-sidebar">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Institute</p>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->

    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- Logo icon --><b>
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <img src="https://wrappixel.com/demos/admin-templates/admin-wrap/assets/images/logo-icon.png"
                             alt="homepage" class="dark-logo"/>
                        <!-- Light Logo icon -->
                        <img src="https://wrappixel.com/demos/admin-templates/admin-wrap/assets/images/logo-light-icon.png"
                             alt="homepage" class="light-logo"/>
                    </b>
                    <!--End Logo icon -->
                    <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url('assets'); ?>/assets/images/logo-text.png" alt="homepage"
                              class="dark-logo"/>
                        <!-- Light Logo text -->
                         <img src="<?php echo base_url('assets'); ?>/assets/images/logo-light-text.png"
                              class="light-logo" alt="homepage"/></span> </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto">
                    <!-- This is  -->
                    <li class="nav-item"><a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark"
                                            href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                    <li class="nav-item hidden-sm-down"><span></span></li>
                </ul>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <ul class="navbar-nav my-lg-0">
                    <!-- ============================================================== -->
                    <!-- Search -->
                    <!-- ============================================================== -->
                    <li class="nav-item hidden-xs-down search-box"><a
                                class="nav-link hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i
                                    class="icon-Magnifi-Glass2"></i></a>
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i
                                        class="ti-close"></i></a></form>
                    </li>
                    <!-- ============================================================== -->
                    <!-- Comment -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"> <i class="icon-Bell"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                            <ul>
                                <li>
                                    <div class="drop-title">Notifications</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Luanch Admin</h5> <span
                                                        class="mail-desc">Just see the my new admin!</span> <span
                                                        class="time">9:30 AM</span></div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span>
                                                <span class="time">9:10 AM</span></div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span>
                                                <span class="time">9:08 AM</span></div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span
                                                        class="mail-desc">Just see the my admin!</span> <span
                                                        class="time">9:02 AM</span></div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all
                                            notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- End Comment -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Messages -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-Mail"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>
                        <div class="dropdown-menu mailbox dropdown-menu-right animated bounceInDown"
                             aria-labelledby="2">
                            <ul>
                                <li>
                                    <div class="drop-title">You have 4 new messages</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img
                                                        src="https://wrappixel.com/demos/admin-templates/admin-wrap/assets/images/users/1.jpg"
                                                        alt="user" class="img-circle"> <span
                                                        class="profile-status online pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span
                                                        class="mail-desc">Just see the my admin!</span> <span
                                                        class="time">9:30 AM</span></div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img
                                                        src="<?php echo base_url('assets'); ?>/assets/images/users/2.jpg"
                                                        alt="user" class="img-circle"> <span
                                                        class="profile-status busy pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Sonu Nigam</h5> <span
                                                        class="mail-desc">I've sung a song! See you at</span> <span
                                                        class="time">9:10 AM</span></div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img
                                                        src="<?php echo base_url('assets'); ?>/assets/images/users/3.jpg"
                                                        alt="user" class="img-circle"> <span
                                                        class="profile-status away pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span
                                                        class="time">9:08 AM</span></div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img
                                                        src="<?php echo base_url('assets'); ?>/assets/images/users/4.jpg"
                                                        alt="user" class="img-circle"> <span
                                                        class="profile-status offline pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span
                                                        class="mail-desc">Just see the my admin!</span> <span
                                                        class="time">9:02 AM</span></div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all
                                            e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- End Messages -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- mega menu -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown mega-dropdown"><a
                                class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"><i class="icon-Box-Close"></i></a>
                        <div class="dropdown-menu animated bounceInDown">
                            <ul class="mega-dropdown-menu row">
                                <li class="col-lg-3 col-xlg-2 m-b-30">
                                    <h4 class="m-b-20">CAROUSEL</h4>
                                    <!-- CAROUSEL -->
                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item active">
                                                <div class="container"><img class="d-block img-fluid"
                                                                            src="<?php echo base_url('assets'); ?>/assets/images/big/img1.jpg"
                                                                            alt="First slide"></div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="container"><img class="d-block img-fluid"
                                                                            src="<?php echo base_url('assets'); ?>/assets/images/big/img2.jpg"
                                                                            alt="Second slide"></div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="container"><img class="d-block img-fluid"
                                                                            src="<?php echo base_url('assets'); ?>/assets/images/big/img3.jpg"
                                                                            alt="Third slide"></div>
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                           data-slide="prev"> <span class="carousel-control-prev-icon"
                                                                    aria-hidden="true"></span> <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                           data-slide="next"> <span class="carousel-control-next-icon"
                                                                    aria-hidden="true"></span> <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <!-- End CAROUSEL -->
                                </li>
                                <li class="col-lg-3 m-b-30">
                                    <h4 class="m-b-20">ACCORDION</h4>
                                    <!-- Accordian -->
                                    <div id="accordion" class="nav-accordion" role="tablist"
                                         aria-multiselectable="true">
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingOne">
                                                <h5 class="mb-0">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne">
                                                        Collapsible Group Item #1
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" class="collapse show" role="tabpanel"
                                                 aria-labelledby="headingOne">
                                                <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapseTwo" aria-expanded="false"
                                                       aria-controls="collapseTwo">
                                                        Collapsible Group Item #2
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse" role="tabpanel"
                                                 aria-labelledby="headingTwo">
                                                <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high life accusamus terry richardson ad squid.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingThree">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapseThree" aria-expanded="false"
                                                       aria-controls="collapseThree">
                                                        Collapsible Group Item #3
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseThree" class="collapse" role="tabpanel"
                                                 aria-labelledby="headingThree">
                                                <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high life accusamus terry richardson ad squid.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-lg-3  m-b-30">
                                    <h4 class="m-b-20">CONTACT US</h4>
                                    <!-- Contact -->
                                    <form>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="exampleInputname1"
                                                   placeholder="Enter Name"></div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Enter email"></div>
                                        <div class="form-group">
                                            <textarea class="form-control" id="exampleTextarea" rows="3"
                                                      placeholder="Message"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </form>
                                </li>
                                <li class="col-lg-3 col-xlg-4 m-b-30">
                                    <h4 class="m-b-20">List style</h4>
                                    <!-- List style -->
                                    <ul class="list-style-none">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> You
                                                can give link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Give
                                                link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                Another Give link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Forth
                                                link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                Another fifth link</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- Profile -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><img
                                    src="https://wrappixel.com/demos/admin-templates/admin-wrap/assets/images/users/1.jpg"
                                    alt="user" class="profile-pic"/></a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img">
                                            <img src="<?php echo base_url('assets'); ?>/assets/images/users/1.jpg">
                                        </div>
                                        <div class="u-text">
                                            <h4><?php echo $user_login->username ?></h4>
                                            <p class="text-muted"><?php echo $user_login->email ?></p>
                                            <a href="pages-profile.html" class="btn btn-rounded btn-danger btn-sm">
                                                View Profile
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>

                                <li><a href="#"><i class="fa ti-settings"></i> Setting</a></li>
                                <li role="separator" class="divider"></li>
<!--                                <li><a href="--><?php //echo base_url('auth/logout') ?><!--"><i class="fa fa-power-off"></i> --><?php //echo lang('logout') ?><!--</a></li>-->
                                <li><a onclick="logout();"><i class="fa fa-power-off"></i> <?php echo lang('logout') ?></a></li>
                            </ul>
                        </div>
                    </li>

                    <!-- ============================================================== -->
                    <!-- End mega menu -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Language -->
                    <!-- ============================================================== -->

                    <?php if ($lang == 'ar') { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-sy"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated bounceInDown">
                                <a class="dropdown-item" href="<?php echo base_url('employee/courses/lang/en') ?>">
                                    <i class="flag-icon flag-icon-us"></i> english
                                </a>
                            </div>
                        </li>
                    <?php } else { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-us"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated bounceInDown">
                                <a class="dropdown-item" href="<?php echo base_url('employee/courses/lang/ar') ?>">
                                    <i class="flag-icon flag-icon-sy"></i> arabic
                                </a>
                            </div>
                        </li>
                    <?php } ?>

                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">

                    <li class="fix-lang" <?php echo $page == "home_page" ? 'class="active"' : '' ?>>
                        <a class=" waves-effect waves-dark" href="<?php echo base_url("home/home_page"); ?>">
                            <i class="fa fa-home"></i>
                            <span class="hide-menu"><?php echo lang("home_page"); ?></span>
                        </a>
                    </li>
                    <?php if (isset($sidebar)) { ?>
                        <?php foreach ($sidebar as $item) { ?>
                            <?php if (isset($item['dropdown'])) { ?>

                                <li class="fix-lang">
                                    <?php if ($item['page'] == 'index') { ?>
                                        <a class="has-arrow waves-effect waves-dark" href="index.html#"
                                           aria-expanded="false">
                                            <i class="<?php echo $item['icon'] ?>"></i>
                                            <span class="hide-menu">
                                                <?php echo lang($item['controller']); ?>
                                            </span>
                                        </a>
                                        <ul aria-expanded="false" class="collapse">
                                            <?php foreach ($item['dropdown'] as $key => $sub) { ?>
                                                <li class="<?php echo $page == $key ? ' active' : '' ?>">
                                                    <a href="<?php echo base_url("$sub"); ?>">
                                                        <?php echo lang($key) ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } else { ?>

                                        <a class="has-arrow waves-effect waves-dark" href="index.html#"
                                           aria-expanded="false">
                                            <i class="<?php echo $item['icon'] ?>"></i>
                                            <span class="hide-menu">
                                                <?php echo lang($item['page']); ?>
                                            </span>
                                        </a>
                                        <ul aria-expanded="false" class="collapse">
                                            <?php foreach ($item['dropdown'] as $key => $sub) { ?>
                                                <li class="<?php echo $page == $key ? ' active' : '' ?>">
                                                    <a href="<?php echo base_url("$sub"); ?>">
                                                        <?php echo lang($key) ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>

                                </li>
                            <?php } else { ?>
                                <?php if ($item['page'] == 'index') { ?>


                                    <li class="fix-lang" <?php echo $page == $item['page'] && $Page_controller == $item['controller'] ? 'class="active"' : '' ?>>
                                        <a class=" waves-effect waves-dark" href="<?php echo $item['link']; ?>">
                                            <i class=" <?php echo $item['icon']; ?>    "></i>
                                            <span class="hide-menu"><?php echo lang($item['controller']); ?></span>
                                        </a>
                                    </li>


                                <?php } else { ?>
                                    <li class="fix-lang" <?php echo $page == $item['page'] ? 'class="active"' : '' ?>>
                                        <a class=" waves-effect waves-dark" href="<?php echo $item['link']; ?>">
                                            <i class=" <?php echo $item['icon']; ?>    "></i>
                                            <span class="hide-menu"><?php echo lang($item['page']); ?></span>
                                        </a>
                                    </li>

                                <?php } ?>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>


                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->


            <div class="row page-titles">
                <div class="col-md-12 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item fix-lang"><a
                                    href="javascript:void(0)"><?php echo lang('home_page') ?></a></li>
                        <li class="breadcrumb-item active  fix-lang"><?php echo $page_title ?></li>
                    </ol>
                </div>

                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10">
                        <i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- Info box -->
            <!-- ============================================================== -->
            <?php echo $yield ?>
            <!-- ============================================================== -->
            <!-- End Info box -->

            <!-- .right-sidebar -->


        </div>

        <footer class="footer text-center">
            <a href="https://gwa-group.com/">POWER BY © GWA GROUP 2010 - <?php echo date("Y") ?></a>
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php } else { ?>
<body class="card-no-border">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Login</p>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar"
         style="background-image:url(<?php echo base_url(); ?>assets/assets/images/background/login-register.jpg)">
    <?php echo $yield; ?>
</section>
<?php } ?>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<style>
    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .25);
    }

    .fix-lang, .select2-selection {
    <?php if($lang == 'ar'){ ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php }else{ ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php } ?>
    }

    .select2-selection {
        width: 100%;
    }

    .select2-style {
    <?php if($lang == 'ar'){ ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php }else{ ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php } ?>
    }

    .select2-container .select2-selection--single, .select2-dropdown {
        border: 1px solid rgba(0, 0, 0, .15) !important;
    }

    .label-selec, .control-label {
        line-height: 3;
    <?php if($lang == 'ar'){ ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php }else{ ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php } ?>
    }

    .reverse-fix-lang {
    <?php if($lang == 'ar'){ ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php }else{ ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php } ?>
    }

    .table td, .table th {
        text-align: center;
    }

    .color-table.primary-table thead th {
        background-color: #6772e5 !important;
        color: #ffffff;
    }

    .color-table.info-table thead th {
        background-color: #20aee3 !important;
        color: #ffffff;
    }

    .color-bordered-table.info-bordered-table {
        border: 2px solid #20aee3 !important;
    }

    .color-bordered-table.info-bordered-table thead th {
        background-color: #20aee3 !important;
        color: #ffffff;
    }

    .hover-opcity {
        opacity: 0.8;
    }

    .cursor-pointer {
        cursor: pointer;
    }

    .css-bar-pink.css-bar-100 {
        background-image: linear-gradient(450deg, #fd5f95 50%, transparent 50%, transparent), linear-gradient(270deg, #fd5e94 50%, #fafafa 50%, #fafafa);
    }

    .table-striped tbody tr:nth-of-type(odd) {
        background: #eaf9ff;
    }

    /*card style*/
    .card-actions a {
        padding-left: 5px;
        padding-right: 5px;
    }

    .card-header {
        border-right: 3px solid #46bce8;
        background-color: #fff;
        border-bottom: 0;
        border-top-right-radius: 0 !important;
        border-top-left-radius: 0 !important;
    }

    /*round*/

    .round-inverse {
        background: #4c5667;
    }

    .round-purple {
        background: #7460ee;
    }

    .round-pink {
        background: #fd5e94;
    }

    .round-default {
        background: #248be6;
    }

    .sidebar-nav > ul > li > a.active {
        background: #edf0f5;
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 5px);
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 5px);
        padding: 0;
        padding-left: 5px;
        padding-right: 5px;
    }

    .clockpicker-popover {
        z-index: 99999 !important;
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 5px);
        padding: 0;
        padding-left: 5px;
        padding-right: 5px;
    }

    @media (min-width: 1200px) {
        /*.mini-sidebar .sidebar-nav, .mini-sidebar .scroll-sidebar{*/
        /*max-width: 1335px;*/
        /*}*/
        /*.container {*/
        /*max-width: 1280px;*/
        /*}*/
    }

    .clockpicker-popover .popover-title {
        padding-left: 75px;
        padding-right: 75px;
    }

    span.clockpicker-span-hours.text-primary {
        float: left;
        text-align: right;
    }

    span.clockpicker-span-minutes {
        float: right;
        text-align: left;
    }
</style>

<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url("assets/assets/node_modules/ps/perfect-scrollbar.jquery.min.js"); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo base_url("assets/$dir/js/waves.js"); ?>"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url("assets/$dir/js/sidebarmenu.js"); ?>"></script>
<!--stickey kit -->
<script src="<?php echo base_url("assets/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/sparkline/jquery.sparkline.min.js"); ?>"></script>

<!--Custom JavaScript -->
<script src="<?php echo base_url("assets/$dir/js/toastr.js"); ?>"></script>
<script src="<?php echo base_url("assets/custom/custom.js"); ?>"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->
<script src="<?php echo base_url("assets/assets/node_modules/raphael/raphael-min.js"); ?>"></script>
<!--<script src="--><?php //echo base_url("assets/assets/node_modules/morrisjs/morris.min.js"); ?><!--"></script>-->
<!--c3 JavaScript -->
<script src="<?php echo base_url("assets/assets/node_modules/d3/d3.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/c3-master/c3.min.js"); ?>"></script>
<!-- Popup message jquery -->
<script src="<?php echo base_url("assets/assets/node_modules/toast-master/js/jquery.toast.js"); ?>"></script>
<!-- Chart JS -->
<script src="<?php echo base_url("assets/$dir/js/dashboard1.js"); ?>"></script>

<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- This is data table -->
<script src="<?php echo base_url("assets/assets/node_modules/datatables/jquery.dataTables.min.js"); ?>"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url("assets/assets/node_modules/datatables/dataTables.buttons.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/datatables/buttons.flash.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/datatables/jszip.min.js"); ?>"></script>

<script src="<?php echo base_url("assets/assets/node_modules/datatables/buttons.html5.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/datatables/buttons.print.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/styleswitcher/jQuery.style.switcher.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-switch/bootstrap-switch.min.js"); ?>"></script>

<!-- This page plugins -->
<!-- ============================================================== -->
<script src="<?php echo base_url("assets/assets/node_modules/switchery/dist/switchery.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"); ?>"></script>

<!-- This page plugins -->
<!-- ============================================================== -->
<script src="<?php echo base_url("assets/assets/node_modules/gauge/gauge.min.js"); ?>"></script>
<!-- ============================================================== -->
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/timepicker/bootstrap-timepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"); ?>"></script>
<!-- =================== Clock picker plugins js=================== -->
<script src="<?php echo base_url("assets/assets/node_modules/moment/moment.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/timepicker/bootstrap-timepicker.min.js"); ?>"></script>

<script>
    //
    // $( ".sortable" ).sortable();
    var sort = $('.sortable');
    $('.sortable').sortable({
        handle: '.handle',
        update: function () {
            sort = $(this);
            var order = 'sort=true&' + $('.sortable').sortable('serialize');
            $.post($('.sortable').data('url'), order, function (theResponse) {
            });
        }
    });
    // MAterial Date picker
    $('#timepicker').bootstrapMaterialDatePicker({format: 'HH:mm', time: true, date: false});
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').on('change', function () {
        console.log(this.value);
    });
    $('#check-minutes').on('click', function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
</script>

<script>

    jQuery(function () {
        // Date Picker
        jQuery('.mydatepicker, #datepicker').datepicker({
            format: 'yyyy/mm/dd'
        });
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy/mm/dd',
        });
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function () {
            new Switchery($(this)[0], $(this).data());
        });
        // For select 2
        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }
        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();
        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });
        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        //$(".touch-spin-number").TouchSpin();
        $(".touch-spin-number").TouchSpin({
            min: 0,
            max: 10000000000,
        });


    });
</script>


<script>
    $(function () {
        $('#myTable').DataTable();
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });

    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
    });
    $('.my_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
    });
</script>
<script>
    $(function () {
        "use strict";

        <?php if ($this->session->flashdata('messages_success')): ?>
        $.toast({
            heading: "<?php echo lang('success') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_success')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 10000,
            stack: 6
        });
        <?php endif ?>
        <?php if ($this->session->flashdata('messages_info')): ?>
        $.toast({
            heading: "<?php echo lang('info') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_info')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'info',
            hideAfter: 10000,
            stack: 6
        });
        <?php endif ?>

        <?php if ($this->session->flashdata('messages_warning')): ?>
        $.toast({
            heading: "<?php echo lang('warning') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_warning')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'warning',
            hideAfter: 10000,
            stack: 6
        });
        <?php endif ?>
        <?php if ($this->session->flashdata('messages_error')): ?>
        $.toast({
            heading: "<?php echo lang('error') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_error')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'error',
            hideAfter: 10000,
        });
        <?php endif ?>



    });
</script>
<script async type="text/javascript">
    $(document).ready(function () {
        if (document.getElementById('printable') == null) {
            $('#prin_btn').css('display', 'none');
        }
        $('#prin_btn').click(function () {
            window.print();
            return false;
        });

        $('[data-target="#delete-modal"] ,[data-target="#details-modal"] , [data-target="#school-contact-info-modal"] ,[data-target="#connect-modal"],[data-target="#cancel_connect-modal"], [data-target="#add-modal"]').on('click', function (event) {
            event.preventDefault();
            var item_id = $(this).closest('tr').data('id');
            var item_title = $(this).closest('tr').data('title');
            var modal = $($(this).data('target'));

            modal.find('span[id="item_title"]').html(item_title);
            modal.find('input[name="item_id"]').val(item_id);
        });

    });
</script>
<script>


    function messages_success(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'success',
            hideAfter: 10000,
            stack: 1
        });
    }

    function messages_info(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'info',
            hideAfter: 10000,
            stack: 6
        });
    }

    function messages_warning(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'warning',
            hideAfter: 10000,
            stack: 6
        });
    }

    function messages_error(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'error',
            hideAfter: 10000,
        });
    }

    $(".form-ajax").submit(function (event) {
        // $('.submit_click').prop('disabled', 'disabled');
        var my_form = $(this);
        var datastring = my_form.serialize();
        var url_data = my_form.attr('url-data');
        var action_form = my_form.attr('action-form');
        var new_url = "<?php echo base_url('') ?>" + url_data;

        var row_id = $(my_form).find('input[name="item_id"]').val();
        var item = "#item-" + row_id;

        $("[data-id=row_id]").css("background", "yellow!important");
        $.ajax({
            type: "POST",
            url: new_url,
            data: datastring,
            dataType: 'json',

            success: function (data) {

                if (data.success) {
                    messages_success(data.success, data.message);
                    if (action_form == "delete") {
                        $('.my_table').find(item).remove();
                    }
                    if (action_form == "update") {
                        var this_row_table = $('.my_table').find(item);
                        var this_form = this_row_table.find('.form-ajax');
                        //المرور على جميع عناصر الفورم
                        $(this_form).each(function () {
                            var dataArray = $(this).serializeArray(), dataObj = {};
                            $(dataArray).each(function (i, field) {

                                //ايجاد السطر الحالي الذي يتم التعديل عليه والمرور على جميع عناصره
                                this_row_table.find('td').each(function (column, td) {
                                    //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                    if (field.name == $(td).attr("data-name")) {
                                        $(td).find('span').text(field.value);
                                    }
                                });
                            });
                            //جلب جميع دروبداون الفورم
                            var dataArraySelect = $(this).find("select").serializeArray(), dataObj = {};
                            $(dataArraySelect).each(function (i, field) {
                                //ايجاد السطر الحالي الذي يتم التعديل عليه والمرور على جميع عناصره
                                this_row_table.find('td').each(function (column, td) {
                                    //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                    if (field.name == $(td).attr("data-name")) {
                                        var text_select = $(my_form).find('select[name=' + field.name + '] option:selected').text();
                                        $(td).find('span').text(text_select);
                                    }
                                });
                            });


                            $(my_form).find('input[type=checkbox]').each(function () {


                                var this_checkbox = this;
                                if ($(this_checkbox).is(":checked")) {
                                    //ايجاد السطر الحالي الذي يتم التعديل عليه والمرور على جميع عناصره
                                    this_row_table.find('td').each(function (column, td) {

                                        //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                        if ($(this_checkbox).attr("name") == $(td).attr("data-name")) {
                                            alert($(this_checkbox).attr("name"));
                                            // console.log($(this_checkbox).attr("name"));
                                            var text_select = $(td).attr("check");
                                            alert(text_select);
                                            $(td).find('span').text(text_select);
                                        }
                                    });
                                } else {
                                    this_row_table.find('td').each(function (column, td) {

                                        //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                        if ($(this_checkbox).attr("name") == $(td).attr("data-name")) {
                                            alert($(this_checkbox).attr("name"));
                                            // console.log($(this_checkbox).attr("name"));
                                            var text_select = $(td).attr("uncheck");
                                            alert(text_select);
                                            $(td).find('span').text(text_select);
                                        }
                                    });
                                }
                            });


                        });

                    }
                }
                if (data.error) {
                    messages_error(data.error, data.message);
                }
                if (data.warning) {
                    messages_warning(data.warning, data.message);
                }
                if (data.info) {
                    messages_info(data.info, data.message);
                }

            }
        });
        event.preventDefault();
    });
</script>


</body>

</html>