<!DOCTYPE html>
<html lang="<?php echo $lang ?>" dir="<?php echo $dir ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Saturday school is a System helps parents and teachers to access to all information about students'
              . 'for example notes, marks, allergies, repord card... etc and provide an attendence service and payments system">
    <meta name="author" content="GWA Group Company">
    <meta name="keywords" content="students, marks , attendence, payments , report card , Gwa Group, saturday school, school attendence system,">


    <?php
    meta_tags(array('general' => true,
        'og' => true,
        'twitter' => true,
        'robot' => true), 'saturday school', "", "", "");
    ?>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/assets/images/stratus-logo.png">
    <title><?php echo $page_title; ?></title>
    <!-- Bootstrap Core CSS -->
    <?php if ($dir == "rtl") { ?>
        <link href="<?php echo base_url("assets/assets/node_modules/bootstrap/css11/bootstrap.min.css") ?>" rel="stylesheet">

    <?php } else { ?>

        <link href="<?php echo base_url("assets/assets/node_modules/bootstrap/css/bootstrap.min.css") ?>" rel="stylesheet">

    <?php } ?>
    <link href="<?php echo base_url("assets/assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/sweetalert/sweetalert.css") ?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/assets/hover.css") ?>" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="<?php echo base_url("assets/assets/node_modules/toast-master/css/jquery.toast.css") ?>" rel="stylesheet">


    <!-- page CSS -->
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/switchery/dist/switchery.min.css") ?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/multiselect/css/multi-select.css") ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?php echo base_url("assets/assets/node_modules/css-chart/css-chart.css") ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("assets/$dir/css/style.css") ?>" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?php echo base_url("assets/$dir/css/pages/dashboard1.css") ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url("assets/$dir/css/colors/default.css") ?>" id="theme" rel="stylesheet">
    <!--icon-->
    <link href="<?php echo base_url("assets/$dir/scss/icons/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/simple-line-icons/css/simple-line-icons.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/weather-icons/css/weather-icons.min.css"); ?>" rel="stylesheet">
    <!--    <link href="--><?php //echo base_url("assets/$dir/scss/icons/iconmind/iconmind.css");                                                                                                                                                                                                                                                                                        ?><!--" id="theme" rel="stylesheet">-->
    <link href="<?php echo base_url("assets/$dir/scss/icons/themify-icons/themify-icons.css"); ?>" id="theme" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/flag-icon-css/flag-icon.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/$dir/scss/icons/material-design-iconic-font/css/materialdesignicons.min.css"); ?>" rel="stylesheet">
    <!--icon-->
    <link href="<?php echo base_url("assets/$dir/css/spinners.css"); ?>" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/timepicker/bootstrap-timepicker.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css") ?>" rel="stylesheet">
    <!-- Clock picker plugins css -->
    <link href="<?php echo base_url("assets/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css") ?>"
          rel="stylesheet">
    <link href="<?php echo base_url("assets/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css") ?>" rel="stylesheet">

    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url('') ?>/assets/fonts/stylesheet.css" rel="stylesheet" type="text/css"/>
    <!-- Auto Search -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo base_url('assets/assets/node_modules/bootstrap/css/jasny-bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script src="<?php echo base_url("assets/assets/node_modules/jquery/jquery.min.js"); ?>"></script>
    <!-- Auto Search -->
    <!--<script src="<?php echo base_url("assets/assets/jquery-1.12.4.js"); ?>"></script>-->
    <script src="<?php echo base_url("assets/assets/jquery-ui.js"); ?>"></script>
    <!-- End Auto Search -->

    <script src="<?php echo base_url("assets/assets/node_modules/bootstrap/js/popper.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/assets/node_modules/bootstrap/js11/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo base_url('assets/assets/node_modules/bootstrap/js/jasny-bootstrap.js'); ?>" type="text/javascript"></script>

    <script src="<?php echo base_url("assets/$dir/js/custom.js"); ?>"></script>

    <script src="<?php echo base_url("assets/assets/node_modules/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/assets/node_modules/sweetalert/sweetalert.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/assets/node_modules/sweetalert/jquery.sweet-alert.custom.js"); ?>"></script>
    <script src="<?php echo base_url("assets/arabic_lang.js"); ?>"></script>


    <script>

        var oldExportAction = function (self, e, dt, button, config) {
            if (button[0].className.indexOf('buttons-excel') >= 0) {
                if ($.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)) {
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config);
                }
                else {
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                }
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
        };
        var newExportAction = function (e, dt, button, config) {
            var self = this;
            var oldStart = dt.settings()[0]._iDisplayStart;
            dt.one('preXhr', function (e, s, data) {
                // Just this once, load all data from the server...
                data.start = 0;
                data.length = 2147483647;
                dt.one('preDraw', function (e, settings) {
                    // Call the original action function
                    oldExportAction(self, e, dt, button, config);
                    dt.one('preXhr', function (e, s, data) {
                        // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                        // Set the property to what it was before exporting.
                        settings._iDisplayStart = oldStart;
                        data.start = oldStart;
                    });
                    // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                    setTimeout(dt.ajax.reload, 0);
                    // Prevent rendering of the full data to the DOM
                    return false;
                });
            });
            // Requery the server with the new one-time export settings
            dt.ajax.reload();
        };
        var save_method; //for save method string
        var table;
        var base_url = '<?php echo base_url(); ?>';
        function reload_table() {
            table.ajax.reload(null, false); //reload datatable ajax
        }
        <?php if (isset($show_object_link)) { ?>
        <?php if (isset($display_non_column) && $display_non_column) { ?>
        $(document).ready(function () {
            //datatables
            table = $('.object_table').DataTable({

                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '<i class="mdi mdi-file-excel "> </i> Excel',
                        extend: 'excel',
                        exportOptions: {
                            columns: '.printable'
                        }
                    },
                    {
                        text:'<i class="mdi mdi-printer "> </i> Print',
                        extend: 'print',
                        exportOptions: {
                            columns: '.printable'
                        }
                    },
                ],
                <?php if ($lang == "ar") { ?>
                "language": {
                    "sProcessing": "جاري التحميل...",
                    "sLengthMenu": "إظهار _MENU_ عناصر",
                    "sZeroRecords": "لم يُعثر على أية سجلات",
                    "sInfo": "العناصر الظاهرة من _START_ إلى _END_ من أصل _TOTAL_ عنصر",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجلّ",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                },
                <?php } ?>
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering": false,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url($show_object_link) ?>",
                    "type": "POST",
                },
                //Set column definition initialisation properties.
                "displayLength": 25,
                "columnDefs": [
                    {
                        "targets": [0],
                        //                                    "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [ - 1], //last column
                        "orderable": false, //set not orderable
                    },
                    {
                        "targets": [ - 2], //2 last column (photo)
                        "orderable": false, //set not orderable
                    },
                ],
                'createdRow': function (row, data, dataIndex) {
                    $(row).attr('id', 'someID');
                },
            });
            // $('.object_table thead>tr').each(function() {
            //     var t = $(this).html();
            //     console.log(t);
            //     $(this).find('td').each (function() {
            //         var d = $(this).html();
            //         console.log(d);
            //     });
            // });

        });
        <?php } elseif (isset($one_column) && $one_column) { ?>
        $(document).ready(function () {
            //datatables
            table = $('.object_table').DataTable({

                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '<i class="mdi mdi-file-excel "> </i> Excel',
                        extend: 'excel',
                        exportOptions: {
                            columns: '.printable'
                        }
                    },
                    {
                        text:'<i class="mdi mdi-printer "> </i> Print',
                        extend: 'print',
                        exportOptions: {
                            columns: '.printable'
                        }
                    },
                ],
                <?php if ($lang == "ar") { ?>
                "language": {
                    "sProcessing": "جاري التحميل...",
                    "sLengthMenu": "إظهار _MENU_ عناصر",
                    "sZeroRecords": "لم يُعثر على أية سجلات",
                    "sInfo": "العناصر الظاهرة من _START_ إلى _END_ من أصل _TOTAL_ عنصر",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجلّ",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                },
                <?php } ?>
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering": false,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url($show_object_link) ?>",
                    "type": "POST",
                },
                //Set column definition initialisation properties.
                "displayLength": 25,
                "columnDefs": [
                    {
                        "targets": [0],
                        //                                    "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [ - 1], //last column
                        "orderable": false, //set not orderable
                    },
                    {
                        "targets": [ - 2], //2 last column (photo)
                        "orderable": false, //set not orderable
                    },
                ],
                'createdRow': function (row, data, dataIndex) {
                    $(row).attr('id', 'someID');
                },
            });
            // $('.object_table thead>tr').each(function() {
            //     var t = $(this).html();
            //     console.log(t);
            //     $(this).find('td').each (function() {
            //         var d = $(this).html();
            //         console.log(d);
            //     });
            // });

        });
        <?php } else { ?>
        $(document).ready(function () {
            var printCounter = 0;
            //datatables
            table = $('.object_table').DataTable({
                "serverSide": true,
                //                            lengthMenu: [['25', - 1 ], ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '<i class="mdi mdi-file-excel "> </i> Excel',
                        extend: 'excel',
                        action: newExportAction,
                        exportOptions: {
                            columns: '.printable'
                        }
                    },
                    {
                        text:'<i class="mdi mdi-printer "> </i> Print',
                        extend: 'print',
                        messageTop: function () {
                            printCounter++;
                            if (printCounter === 1) {
                                return 'This is the first time you have printed this document.';
                            }
                            else {
                                return 'You have printed this document ' + printCounter + ' times';
                            }
                        },
                        messageBottom: null,
                        exportOptions: {
                            columns: '.printable'
                        }
                    },
                ],
                <?php if ($lang == "ar") { ?>
                "language": {
                    "sProcessing": "جاري التحميل...",
                    "sLengthMenu": "إظهار _MENU_ عناصر",
                    "sZeroRecords": "لم يُعثر على أية سجلات",
                    "sInfo": "العناصر الظاهرة من _START_ إلى _END_ من أصل _TOTAL_ عنصر",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجلّ",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                },
                <?php } ?>
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "ordering": false,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url($show_object_link) ?>",
                    "type": "POST",
                },
                //Set column definition initialisation properties.
                "displayLength": 25,
                "columnDefs": [
                    {
                        "targets": [0],
                        //                                    "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [ - 1], //last column
                        "orderable": false, //set not orderable
                    },
                    {
                        "targets": [ - 2], //2 last column (photo)
                        "orderable": false, //set not orderable
                    },
                ],
                'createdRow': function (row, data, dataIndex) {
                    $(row).attr('id', 'someID');
                },
            });
            // $('.object_table thead>tr').each(function() {
            //     var t = $(this).html();
            //     console.log(t);
            //     $(this).find('td').each (function() {
            //         var d = $(this).html();
            //         console.log(d);
            //     });
            // });

        });
        <?php } ?>

        <?php } ?>

        <?php if (isset($modal_name)) { ?>
        function add_object(modal_id) {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            $(".fileinput").addClass("fileinput-new");
            $(".fileinput").removeClass("fileinput-exists");
            var formData = new FormData($('#form_crud')[0]);
            for (var pair of formData.entries()) {
                // cause error in index_message page
                if (pair[0] === 'msg_to[]'){
                    pair[0] = "'msg_to[]'";
                }

                if (pair[0] === 'meeting_to[]'){
                    pair[0] = "'meeting_to[]'";
                }

                var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
                if (hidden_in == "add") {
                    $('[name=' + pair[0] + ']').parent().parent().hide();
                } else {
                    $('[name=' + pair[0] + ']').parent().parent().show();
                }
            }
            save_method = 'add';
            $('#form_crud')[0].reset(); // reset form on modals
            //                $("[name='msg_to[]'").empty();
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(show_modal_id).modal('show'); // show bootstrap modal
            var add_object_title = "<?php echo $add_object_title; ?>";
            $('.modal-title').text(add_object_title); // Set Title to Bootstrap modal title

            $('#photo-preview').hide(); // hide photo preview modal

            $('#label-photo').text('Upload Photo'); // label photo upload
        }
        <?php } ?>
        <?php if (isset($get_object_link)) { ?>
        function edit_object(id) {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            save_method = 'update';
            $('#form_crud')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            //$('.help-block').empty(); // clear error string

            var formData = new FormData($('#form_crud')[0]);
            for (var pair of formData.entries()) {
                var hidden_in = $('[name=' + pair[0] + ']').attr("hidden-in");
                if (hidden_in == "update") {
                    $('[name=' + pair[0] + ']').parent().parent().hide();
                } else {
                    $('[name=' + pair[0] + ']').parent().parent().show();
                }
            }

            $(show_modal_id).modal('show'); // show bootstrap modal
            //Ajax Load data from ajax
            $form = $(event.target);
            $.ajax({
                url: "<?php echo base_url($get_object_link) ?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {

                    var fileUrl = "<?php echo base_url('assets/uploads/meeting_file/'); ?>" + data.meeting_file;
                    $("form#form_crud :input").each(function(index, item){
                        var input_name = $(this).attr('name'); // This is the jquery object of the input, do what you will
                        var input_type = $(this).attr('type');
                        if (input_type === 'file'){

                        }
                        if (input_type == 'radio'){
                            if (data.grade_type === 'youth_group'){
                                $('input:radio[name=grade_type]:nth(0)').attr('checked', true);
                                $("input[name=grade_type][value=" + data[input_name] + "]").attr('checked', 'checked');
                            } else if (data.grade_type === 'book_club'){
                                $('input:radio[name=grade_type]:nth(1)').attr('checked', true);
                            } else{
                                $('input:radio[name=grade_type]:nth(2)').attr('checked', true);
                            }
                        }
                        else if (input_type != 'checkbox') {
                            $(this).val(data[input_name]);
                        } else if (input_type == 'checkbox') {
                            if (data[input_name] == 2) {
                                $(this).prop("checked", true);
                            } else if (data[input_name] == 'September' ||
                                data[input_name] == 'October' ||
                                data[input_name] == 'November' ||
                                data[input_name] == 'December' ||
                                data[input_name] == 'January' ||
                                data[input_name] == 'February' ||
                                data[input_name] == 'March' ||
                                data[input_name] == 'April' ||
                                data[input_name] == 'May'
                            ){
                                $(this).prop("checked", true);
                            } else{
                                $(this).prop("checked", false);
                            }
                        }

                    });
                    //                        this.$http.post('assets/uploads/meeting_file', data, function (data, status, request)
                    var update_object_title = "<?php echo $update_object_title; ?>";
                    $('.modal-title').text(update_object_title); // Set title to Bootstrap modal title
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                        alert('Error get data from ajax');
                }
            });
        }

        <?php } ?>

        function get_response(data) {
            var show_modal_id = "<?php if (isset($modal_name)) echo "#$modal_name" ?>";
            if (data.status == "200") {
                // message success data.message
                swal({
                    title: "<?php echo lang('success') ?>",
                    text: data.message,
                    type: "success",
                    confirmButtonText: "<?php echo lang('close') ?>",
                });
                $(show_modal_id).modal('hide');
                reload_table();
            } else if (data.status == "201") {
                // valedation error data.data
                messages_error("<?php echo lang('error'); ?>", data.data);
            } else if (data.status == "202") {

                swal({
                    title: "<?php echo lang('error_existing') ?>",
                    text: data.message,
                    type: "error",
                    confirmButtonText: "<?php echo lang('close') ?>",
                });
            } else if (data.status == "203") {
                // permission error
                var redirct_url = "<?php echo base_url() ?>/home/home_page";
                window.location.replace(redirct_url);
            } else if (data.status == "204") {
                //permission success
            } else if (data.status == "205") {
                // Date error
                swal({
                    title: "<?php echo lang('date_error') ?>",
                    text: data.message,
                    type: "error",
                    confirmButtonText: "<?php echo lang('close') ?>",
                });
            } else if (data.status == "206") {
                // Date error
                swal({
                    title: "<?php echo lang('Time_error') ?>",
                    text: data.message,
                    type: "error",
                    confirmButtonText: "<?php echo lang('close') ?>",
                });
            } else if (data.status == "400") {
                swal({
                    title: "<?php echo lang('error') ?>",
                    text: data.message,
                    type: "error",
                    confirmButtonText: "<?php echo lang('close') ?>",
                });
                // message error in controller data.message
            } else if (data.status == "401") {
                // message error in information data.message and status
            }
        }

        <?php if (isset($add_object_link) || isset($update_object_link) || isset($detail_object_link)) { ?>
        function save() {

            var show_modal_id = "<?php echo "#$modal_name" ?>";
            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable
            var url;
            //                console.log(save_method);
            if (save_method == 'add') {
                url = "<?php echo site_url($add_object_link) ?>";
            } else {
                //                    if (save_method === 'detail') {
                //                    url = "<?php // echo site_url($detail_object_link);                                                                                                                                                                                           ?>";
                //                    } else{
                url = "<?php echo site_url($update_object_link) ?>";
                //                    }
            }
            add_update(url);
        }
        <?php } ?>

        <?php if (isset($detail_object_link)) { ?>
        function save() {
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable
            var url;
            url = "<?php echo site_url($detail_object_link); ?>";
            detail(url);
        }
        <?php } ?>

        <?php if (isset($delete_object_link)) { ?>

        function delete_object(id) {
            swal({
                title: "<?php echo lang('delete_confirmation_text') ?>",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "<?php echo lang('close') ?>",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('delete') ?>",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: "<?php echo site_url($delete_object_link) ?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        //if success reload ajax table
                        // $(show_modal_id).modal('hide');

                        if (data.status == '200') {
                            swal("<?php echo lang('delete_success') ?>", "", "success");
                        } else if (data.status == '400') {
                            //error in delete
                        } else if (data.status == '407') {
                            swal({
                                title: "<?php echo lang('warning') ?>",
                                text: data.message,
                                type: "warning",
                                confirmButtonText: "<?php echo lang('close') ?>",
                            });
                        }



                        reload_table();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert('Error deleting data');
                    }
                });
            });
            var show_modal_id = "<?php echo "#$modal_name" ?>";
            //if (confirm('Are you sure delete this data?')) {
            //    // ajax delete data to database
            //    $.ajax({
            //        url: "<?php //echo site_url($delete_object_link)                                                                                                                                                                                                                                                                             ?>///" + id,
            //        type: "POST",
            //        dataType: "JSON",
            //        success: function (data) {
            //            //if success reload ajax table
            //            $(show_modal_id).modal('hide');
            //            reload_table();
            //        },
            //        error: function (jqXHR, textStatus, errorThrown) {
            //            alert('Error deleting data');
            //        }
            //    });
            //
            //}
        }

        <?php } ?>
        <?php if (isset($permissions_link)) { ?>
        url = "<?php echo site_url($permissions_link) ?>";
        $.ajax({
            url: url,
            type: "POST",
            //                data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    get_response(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error permission');
            }
        });
        <?php } ?>
        function add_update(url) {
            // ajax adding data to database
            var formData = new FormData($('#form_crud')[0]);
//            console.log(formData);
            // for (var pair of formData.entries()) {
            //     console.log($('[name=' + pair[0] + ']').attr('name'));
            //     console.log($('[name=' + pair[0] + ']').attr('type'));
            // }

            var inputFileName = $(".fileinput-preview").text();
            if (inputFileName != ""){
                formData.append('fileName', inputFileName);
            } else{
                var ua = navigator.userAgent.toLowerCase();

                if (ua.indexOf('safari') != -1) {
                    if (ua.indexOf('chrome') > -1) {
//                      alert("1") // Chrome
                    } else {
                        var fileVal = $('#file').val();
                        if (fileVal == '')
                        {
                            alert("Please upload a file in the file field to complete your request");
//                           document.getElementById("btnSaveid").disabled = true;
                        }
//                      alert("2") // Safari
                    }
                }
            }

            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
//                    console.log(data.data);
                    if (data.status) //if success close modal and reload ajax table
                    {
                        get_response(data);
                    }
                    else {
                        for (var i = 0; i < data.inputerror.length; i++) {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
                    var save = '<?php echo lang('save') ?>';
                    $('.btnSave').text(save); //change button text
                    $('.btnSave').attr('disabled', false); //set button enable
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    var fileVal = $('#file').val();
                    if (fileVal == '')
                    {
//            alert("empty input file");
//            document.getElementById("btnSaveid").disabled = true;
                        $('#btnSaveid').attr('disabled', true);
                        swal({
                            title: "<?php echo lang('should_upload_file') ?>",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "<?php echo lang('close') ?>",
                        });
                    }
//                    alert('Error adding / update data');
                    var save = '<?php echo lang('save') ?>';
                    $('.btnSave').text(save); //change button text
                    $('.btnSave').attr('disabled', false); //set button enable
                }
            });
        }
    </script>


    <script type="text/javascript">
        function logout () {
            var user_id = - 1;
            var token = "";
            if (localStorage) {
                user_id = localStorage.getItem("user_id");
                token = localStorage.getItem("token");
            }
            var logoutUrl = '<?php echo base_url('auth/logout') ?>';
            var loginUrl = '<?php echo base_url('auth/login') ?>';
            $.ajax({
                url:logoutUrl,
                type: "POST",
                headers:{"User-ID":user_id, "Authorization":token},
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    if (data.status == 200) {
                        messages_success("<?php echo lang('success'); ?>", data.data);
                        redirectTo(loginUrl);
                    } else if (data.status == 400) {
                        messages_error("<?php echo lang('error'); ?>", data.message);
                    }
                }
            });
        }

        function redirectTo(page) {
            window.location.replace(page);
        }

    </script>



</head>
<style>
    .scroll-sidebar {
        height: auto!important;
    }
    .datepicker-dropdown, .mydatepicker{
        z-index: 9999!important;
    }
    .confirm, .cancel{
        border-radius: 60px!important;
        padding: 7px 18px!important;
        transform: perspective(1px) translateZ(0)!important;
        box-shadow: 0 0 1px rgba(0, 0, 0, 0)!important;
        transition-property: box-shadow!important;
        /*box-shadow: 0 10px 10px -10px rgba(0, 0, 0, 0.5);*/
    }
    .cancel:hover{
        box-shadow: 0 10px 10px -10px rgba(0, 0, 0, 0.5)!important;
        opacity: 0.8!important;
    }
    .confirm:hover{
        box-shadow: 0 10px 10px -10px rgba(0, 0, 0, 0.5)!important;
        opacity: 0.8!important;
    }
    .buttons-excel{
        background: #24d2b5!important;
    }

    .nav-link{
        padding: 14px!important;
    }
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        background-color: #20AEE3!important;
        color:white!important;

    }
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link {
        /*background-color: #20AEE3!important;*/
        color:black!important;

    }
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance: textfield;
    }

    p, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .h4, th, td, table.dataTable thead .sorting, table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc, table.dataTable thead .sorting_asc_disabled, table.dataTable thead .sorting_desc_disabled, button, body ,a {
        /*font-family: Arial, sans-serif!important;*/
        font-family: Roboto Condensed !important;

    }

    /*        .dataTables_filter {
                display: none!important;
            }*/
    @media (max-width: 767px) {

    }
    @media (max-width: 767px){
        .web-logo{
            display: none;
        }
    }

    @media (min-width: 768px){
        .mini-sidebar .sidebar-nav #sidebarnav > li > a {
            padding: 12px 12px!important;
            /*font-size: 10px;*/
        }
    }
    @media (min-width: 842px) and (max-width: 1043px) {
        /*@media (min-width: 835px) {*/
        .mini-sidebar .sidebar-nav #sidebarnav > li > a {
            /*padding: 12px 12px!important;*/
            font-size: 11px;
        }
    }

    /*        @media(max-width: 1020px){
                  .mini-sidebar .sidebar-nav #sidebarnav > li > a {
                                  font-size: 10px;

                }
            }*/

    textarea {
        resize: vertical!important;
    }

    .fix-lang {
        margin-right: 0px!important;
    }

    @media print
    {
        .non_printable {display:none;}
        .css-background-container input[type='checkbox']:checked::before{
            user-select:all !important;
        }
        /*            body * {
                        visibility: hidden;
                    }
                    .box * {
                        visibility: visible;
                    }
                    .box_minaret_sat * {
                        visibility: visible;
                    }


                    * {
                        -webkit-print-color-adjust: exact;
                        print-color-adjust: exact;
                    }*/
    }

</style>

<?php if ($lang == 'en') { ?>
    <style>
        .input-group-addon {
            padding-left: 12px;
            padding-right: 12px;
            padding-top: 8px;
            padding-bottom: 8px;
            margin-bottom: 0;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.25;
            color: #495057;
            text-align: center;
            background-color: #e9ecef;
            border: 1px solid rgba(0,0,0,.15);
            border-radius: .15rem;
        }
    </style>

<?php } ?>
<?php if ($function_name != 'login') { ?>
<body class="card-no-border mini-sidebar">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Loading</p>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <style>
        @media (max-width: 884px){
            .topbar .top-navbar .navbar-expand-md .navbar-light {
                padding-left: 160px!important;
                padding-right: 160px!important;
            }
            .navbar top-navbar navbar-expand-md navbar-light{
                padding-left: 60px!important;
            }
        }
        @media (min-width: 768px){
            #specific-top{
                padding-left: 85px!important;
                padding-right: 82px!important;

            }
        }
        .topbar{
            padding-top: 10px;
            padding-bottom: 10px;
        }
    </style>
    <header class="topbar non_printable" >
        <nav class="navbar top-navbar navbar-expand-md navbar-light" id="specific-top" >
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <!--<div class="navbar-header">-->
            <!--<a class="navbar-brand" href="index.html">-->
            <!-- Logo icon -->
            <!--<b>-->
            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
            <!-- Dark Logo icon -->
            <!--                                    <img src="https://wrappixel.com/demos/admin-templates/admin-wrap/assets/images/logo-icon.png"
                                                 alt="homepage" class="dark-logo"/>
                                             Light Logo icon
                                            <img src="https://wrappixel.com/demos/admin-templates/admin-wrap/assets/images/logo-light-icon.png"
                                                 alt="homepage" class="light-logo"/>-->
            <!--</b>-->
            <!--End Logo icon -->
            <!-- Logo text -->
            <!--<span>-->
            <!-- dark Logo text -->
            <!--<h1 style="color: white; background: #1d7bc2">Stratus</h1>-->
            <?php if ($lang == 'ar') { ?>
                <img  class="web-logo" style="padding: 8px 0px;width: 230px;" src="<?php echo base_url('assets'); ?>/assets/images/1.png" alt="homepage" />
            <?php } else { ?>
                <img   class="web-logo" style="padding: 8px 0px;width: 230px;" src="<?php echo base_url('assets'); ?>/assets/images/1.png" alt="homepage" />
            <?php } ?>

            <!-- Light Logo text -->
            <!--                                    <img src="<?php echo base_url('assets'); ?>/assets/images/logo-light-text.png"
                                             class="light-logo" alt="homepage"/></span> </a>-->
            <!--</div>-->
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto">
                    <!-- This is  -->
                    <li class="nav-item"><a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark"
                                            href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                    <li class="nav-item hidden-sm-down"><span></span></li>
                </ul>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <ul class="navbar-nav my-lg-0"  >
                    <!-- ============================================================== -->
                    <!-- Search -->
                    <!-- ============================================================== -->
                    <?php if (isset($user_login) && $user_login && ($user_login->type == "admin" || $user_login->type == "teacher")) { ?>
                        <li class="nav-item hidden-xs-down search-box"  style="padding-top: 22px;     padding-right: 8px;">
                            <div class="row">
                                <style>
                                    .auto_complete_search::placeholder {
                                        color: #1d7bc2;
                                    }
                                </style>
                                <form class="app-s" action="<?php echo site_url('student/search_about'); ?>" method="get" style="display:none; padding-top: 10px;     padding-right: 8px;" >
                                    <input type="text" class="form-control auto_complete_search" name="inputSearch_value" placeholder="Search" id="inputSearch_value" style="border-radius: 50px; width: 300px; border: activeborder; color: #1d7bc2;">
                                    <input type="text" class="form-control " name="inputSearch_key" placeholder="Search" hidden="true" id="inputSearch_key">
                                </form>
                                <a
                                        class="nav-link hidden-sm-down  hvr-grow" href="javascript:void(0)">
                                    <i class="icon-Magnifi-Glass2" style="font-size: 26px; color: #fff9ed !important;"></i>
                                </a>
                            </div>
                        </li>
                        <!--                                    <li class="nav-item hidden-xs-down search-box">
                                                                            <a
                                                                                class="nav-link hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i
                                                                                    class="icon-Magnifi-Glass2" style="font-size: 26px;"></i>
                                                                            </a>
                                                                            <form class="app-search" action="<?php echo site_url('student/search_about'); ?>" method="get">
                                                                                <input type="text" class="form-control auto_complete_search" name="inputSearch_value" placeholder="Search & enter" id="inputSearch_value">
                                                                                <input type="text" class="form-control " name="inputSearch_key" placeholder="Search & enter" hidden="true" id="inputSearch_key">

                                                                                <a class="srh-btn "><i class="ti-close"></i></a>
                                                                            </form>
                                                                        </li>-->

                    <?php } ?>
                    <!-- ============================================================== -->
                    <!-- Comment -->
                    <!-- ============================================================== -->
                    <!--                                                                <li class="nav-item dropdown">
                                                                                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                                                                                           aria-haspopup="true" aria-expanded="false"> <i class="icon-Bell"></i>
                                                                                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                                                                                        </a>
                                                                                        <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <div class="drop-title">Notifications</div>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <div class="message-center">
                                                                                                         Message
                                                                                                        <a href="#">
                                                                                                            <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                                                                            <div class="mail-contnet">
                                                                                                                <h5>Luanch Admin</h5> <span
                                                                                                                    class="mail-desc">Just see the my new admin!</span> <span
                                                                                                                    class="time">9:30 AM</span></div>
                                                                                                        </a>
                                                                                                         Message
                                                                                                        <a href="#">
                                                                                                            <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                                                                                            <div class="mail-contnet">
                                                                                                                <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span>
                                                                                                                <span class="time">9:10 AM</span></div>
                                                                                                        </a>
                                                                                                         Message
                                                                                                        <a href="#">
                                                                                                            <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                                                                                            <div class="mail-contnet">
                                                                                                                <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span>
                                                                                                                <span class="time">9:08 AM</span></div>
                                                                                                        </a>
                                                                                                         Message
                                                                                                        <a href="#">
                                                                                                            <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                                                                            <div class="mail-contnet">
                                                                                                                <h5>Pavan kumar</h5> <span
                                                                                                                    class="mail-desc">Just see the my admin!</span> <span
                                                                                                                    class="time">9:02 AM</span></div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all
                                                                                                            notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </li>-->
                    <!-- ============================================================== -->
                    <!-- End Comment -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Messages -->
                    <!-- ============================================================== -->
                    <?php // if ($user_login->type == 'admin') {             ?>
                    <script>
                        $(document).ready(function () {
                            count_received_msg_for_user_id();
                        });
                        function count_received_msg_for_user_id() {
                            $.ajax({
                                url: "<?php echo base_url("message/count_received_msg_for_user_id/") . $user_login->id ?>",
                                type: "POST",
                                dataType: "JSON",
                                success: function (data) {
                                    if (data.data == 0){
                                        $('.notify').hide();
                                    } else{
                                        $('.notify').show();
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                }
                            });
                        }
                    </script>

                    <li class="nav-item dropdown" style="padding: 10px!important;">
                        <?php if ($user_login->type != "student") { ?>
                            <a class="nav-link hvr-grow" href="<?php echo base_url('message/index_send_msg'); ?>" id="2"> <i class="mdi mdi-comment-check fa-2x" style="font-size: 1.8em;" title="<?php echo lang('send_msg'); ?>"></i>
                            </a>
                        <?php } ?>
                        <a class="nav-link  hvr-grow"  href="<?php echo base_url('message/index_recieve_msg'); ?>" id="2"> <i class="mdi mdi-email fa-2x" style="font-size: 1.8em;" title="<?php echo lang('recieve_msg'); ?>"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>

                    </li>


                    <?php // }             ?>
                    <!-- ============================================================== -->
                    <!-- End Messages -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- mega menu -->
                    <!-- ============================================================== -->
                    <!--                                <li class="nav-item dropdown mega-dropdown"><a
                                                                                    class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                                                                                    aria-haspopup="true" aria-expanded="false"><i class="icon-Box-Close"></i></a>
                                                                                <div class="dropdown-menu animated bounceInDown">
                                                                                    <ul class="mega-dropdown-menu row">
                                                                                        <li class="col-lg-3 col-xlg-2 m-b-30">
                                                                                            <h4 class="m-b-20">CAROUSEL</h4>
                                                                                             CAROUSEL
                                                                                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                                                                <div class="carousel-inner" role="listbox">
                                                                                                    <div class="carousel-item active">
                                                                                                        <div class="container"><img class="d-block img-fluid"
                                                                                                                                    src="<?php echo base_url('assets'); ?>/assets/images/big/img1.jpg"
                                                                                                                                    alt="First slide"></div>
                                                                                                    </div>
                                                                                                    <div class="carousel-item">
                                                                                                        <div class="container"><img class="d-block img-fluid"
                                                                                                                                    src="<?php echo base_url('assets'); ?>/assets/images/big/img2.jpg"
                                                                                                                                    alt="Second slide"></div>
                                                                                                    </div>
                                                                                                    <div class="carousel-item">
                                                                                                        <div class="container"><img class="d-block img-fluid"
                                                                                                                                    src="<?php echo base_url('assets'); ?>/assets/images/big/img3.jpg"
                                                                                                                                    alt="Third slide"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                                                                                   data-slide="prev"> <span class="carousel-control-prev-icon"
                                                                                                                         aria-hidden="true"></span> <span class="sr-only">Previous</span>
                                                                                                </a>
                                                                                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                                                                                   data-slide="next"> <span class="carousel-control-next-icon"
                                                                                                                         aria-hidden="true"></span> <span class="sr-only">Next</span>
                                                                                                </a>
                                                                                            </div>
                                                                                             End CAROUSEL
                                                                                        </li>
                                                                                        <li class="col-lg-3 m-b-30">
                                                                                            <h4 class="m-b-20">ACCORDION</h4>
                                                                                             Accordian
                                                                                            <div id="accordion" class="nav-accordion" role="tablist"
                                                                                                 aria-multiselectable="true">
                                                                                                <div class="card">
                                                                                                    <div class="card-header" role="tab" id="headingOne">
                                                                                                        <h5 class="mb-0">
                                                                                                            <a data-toggle="collapse" data-parent="#accordion"
                                                                                                               href="#collapseOne" aria-expanded="true"
                                                                                                               aria-controls="collapseOne">
                                                                                                                Collapsible Group Item #1
                                                                                                            </a>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                    <div id="collapseOne" class="collapse show" role="tabpanel"
                                                                                                         aria-labelledby="headingOne">
                                                                                                        <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
                                                                                                            high.
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="card">
                                                                                                    <div class="card-header" role="tab" id="headingTwo">
                                                                                                        <h5 class="mb-0">
                                                                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                                                                               href="#collapseTwo" aria-expanded="false"
                                                                                                               aria-controls="collapseTwo">
                                                                                                                Collapsible Group Item #2
                                                                                                            </a>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                    <div id="collapseTwo" class="collapse" role="tabpanel"
                                                                                                         aria-labelledby="headingTwo">
                                                                                                        <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
                                                                                                            high life accusamus terry richardson ad squid.
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="card">
                                                                                                    <div class="card-header" role="tab" id="headingThree">
                                                                                                        <h5 class="mb-0">
                                                                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                                                                               href="#collapseThree" aria-expanded="false"
                                                                                                               aria-controls="collapseThree">
                                                                                                                Collapsible Group Item #3
                                                                                                            </a>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                    <div id="collapseThree" class="collapse" role="tabpanel"
                                                                                                         aria-labelledby="headingThree">
                                                                                                        <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
                                                                                                            high life accusamus terry richardson ad squid.
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="col-lg-3  m-b-30">
                                                                                            <h4 class="m-b-20">CONTACT US</h4>
                                                                                             Contact
                                                                                            <form>
                                                                                                <div class="form-group">
                                                                                                    <input type="text" class="form-control" id="exampleInputname1"
                                                                                                           placeholder="Enter Name"></div>
                                                                                                <div class="form-group">
                                                                                                    <input type="email" class="form-control" placeholder="Enter email"></div>
                                                                                                <div class="form-group">
                                                                                                    <textarea class="form-control" id="exampleTextarea" rows="3"
                                                                                                              placeholder="Message"></textarea>
                                                                                                </div>
                                                                                                <button type="submit" class="btn btn-info">Submit</button>
                                                                                            </form>
                                                                                        </li>
                                                                                        <li class="col-lg-3 col-xlg-4 m-b-30">
                                                                                            <h4 class="m-b-20">List style</h4>
                                                                                             List style
                                                                                            <ul class="list-style-none">
                                                                                                <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> You
                                                                                                        can give link</a></li>
                                                                                                <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Give
                                                                                                        link</a></li>
                                                                                                <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                                                                        Another Give link</a></li>
                                                                                                <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Forth
                                                                                                        link</a></li>
                                                                                                <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                                                                        Another fifth link</a></li>
                                                                                            </ul>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </li>-->
                    <!-- ============================================================== -->
                    <!-- Profile -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <?php if (isset($user_login) && $user_login && $user_login->type == "admin") { ?>
                                <img src="<?php echo base_url('assets'); ?>/uploads/images/admin.png"
                                     alt="user" class="profile-pic" />
                            <?php } else if (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                                <img src="<?php echo base_url('assets'); ?>/uploads/images/parents.png"
                                     alt="user" class="profile-pic" />
                            <?php } else if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                                <img src="<?php echo base_url('assets'); ?>/uploads/images/teacher.png"
                                     alt="user" class="profile-pic" />
                            <?php } else if (isset($user_login) && $user_login && $user_login->type == "student") { ?>
                                <img src="<?php echo base_url('assets'); ?>/uploads/images/student_2.png"
                                     alt="user" class="profile-pic" />
                            <?php } ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img">
                                            <?php if (isset($user_login) && $user_login && $user_login->type == "admin") { ?>
                                                <img src="<?php echo base_url('assets'); ?>/uploads/images/admin.png" />
                                            <?php } else if (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                                                <img src="<?php echo base_url('assets'); ?>/uploads/images/parents.png" />
                                            <?php } else if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                                                <img src="<?php echo base_url('assets'); ?>/uploads/images/teacher.png" />
                                            <?php } else if (isset($user_login) && $user_login && $user_login->type == "student") { ?>
                                                <img src="<?php echo base_url('assets'); ?>/uploads/images/student_2.png" />
                                            <?php } ?>
                                        </div>
                                        <?php if (isset($user_login) && $user_login) { ?>
                                            <div class="u-text m-t-5">

                                                <!--<p class="text-muted"><?php echo $user_login->email ?></p>-->
                                                <?php if (isset($user_login) && $user_login && $user_login->type != "admin") { ?>
                                                    <h4  ><?php echo $login_email ?></h4>
                                                    <a  href="<?php echo base_url('profile/page_profile'); ?>" class="m-t-5 btn btn-rounded btn-danger btn-sm">
                                                        <?php echo lang('view_profile') ?>
                                                    </a>
                                                <?php } else { ?>
                                                    <h4 style="margin-top: 14px;" ><?php echo $login_email ?></h4>

                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>

                                <!--<li><a href="#"><i class="fa ti-settings"></i> Setting</a></li>-->
                                <!--<li role="separator" class="divider"></li>-->
                                <?php if ($user_login->type == "parent" || $user_login->type == "teacher"): ?>
                                    <li><a style="cursor: pointer" href="<?php echo base_url('profile/update_profile') ?>"><i class="mdi mdi-account-edit"></i> <?php echo lang('update_profile') ?></a></li>

                                <?php endif; ?>
                                <li><a style="cursor: pointer" href="<?php echo base_url('auth/change_password') ?>"><i class="mdi mdi-lock-outline"></i> <?php echo lang('change_password') ?></a></li>
                                <li><a style="cursor: pointer" onclick="logout();"><i class="mdi mdi-logout" ></i> <?php echo lang('logout') ?></a></li>


                            </ul>
                        </div>
                    </li>

                    <!-- ============================================================== -->
                    <!-- End mega menu -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Language -->
                    <!-- ============================================================== -->
                    <?php if ($lang == 'ar') { ?>
                        <!--                                    <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                                                                               aria-haspopup="true" aria-expanded="false">
                                                                                <i class="flag-icon flag-icon-sy"></i>
                                                                                <i> AR </i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right animated bounceInDown">
                                    <?php if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                                                                                                                                                                                                                    <a class="dropdown-item" href="<?php echo base_url('home/home_page_teacher/lang/en') ?>">
                                    <?php } elseif (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                                                                                                                                                                                                                    <a class="dropdown-item" href="<?php echo base_url('home/home_page_parent/lang/en') ?>">
                                    <?php } else { ?>
                                                                                                                                                                                                                    <a class="dropdown-item" href="<?php echo base_url('home/home_page/lang/en') ?>">
                                    <?php } ?>
                                                                                            <i class="flag-icon flag-icon-us"></i> english
                                                                                            <i>EN</i>
                                                                                        </a>
                                                                                        </div>
                                                                                        </li>-->
                    <?php } else { ?>
                        <!--                                                    <li class="nav-item dropdown">
                                                                                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                                                                                               aria-haspopup="true" aria-expanded="false">
                                                                                                <i class="flag-icon flag-icon-us"></i>
                                                                                                <i>EN</i>
                                                                                            </a>
                                                                                            <div class="dropdown-menu dropdown-menu-right animated bounceInDown">
                                    <?php if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                                                                                                                                                                                                                    <a class="dropdown-item" href="<?php echo base_url('home/home_page_teacher/lang/ar') ?>">
                                    <?php } elseif (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                                                                                                                                                                                                                    <a class="dropdown-item" href="<?php echo base_url('home/home_page_parent/lang/ar') ?>">
                                    <?php } else { ?>
                                                                                                                                                                                                                    <a class="dropdown-item" href="<?php echo base_url('home/home_page/lang/ar') ?>">
                                    <?php } ?>
                                            <i class="flag-icon flag-icon-sy"></i> arabic
                                                                                                            <i> AR </i>
                                                                                                        </a>
                                                                                                        </div>
                                                                                                        </li>-->
                    <?php } ?>

                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->

    <style>
        .topbar .navbar-light .navbar-nav .nav-item > a.nav-link:hover, .topbar .navbar-light .navbar-nav .nav-item > a.nav-link:focus {
            color: #fafafa !important;
        }
        .topbar .navbar-light .navbar-nav .nav-item > a.nav-link:hover, .topbar .navbar-light .navbar-nav .nav-item > a.nav-link:hover {
            color: #fafafa !important;
        }

        .waves-dark > .hide-menu {
            text-transform: uppercase!important;
        }
        /*                    .fix-lang  > .collapse {
                                text-transform: uppercase!important;
                            }*/
    </style>

    <!--<hr>-->
    <aside class="left-sidebar non_printable">

        <!-- Sidebar scroll-->
        <div class="scroll-sidebar" >
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav" >

                    <li class="fix-lang" <?php echo $page == "home_page" ? 'class="active"' : '' ?>>
                        <?php
                        if (isset($user_login) && $user_login) {
                            $type = $user_login->type;
                        }
                        ?>
                        <a class=" waves-effect waves-dark" href="<?php echo (isset($user_login) && $user_login && $user_login->type != 'admin' ) ? base_url("home/home_page" . '_' . $user_login->type) : base_url("home/home_page"); ?>">
                            <!--<i class="fa fa-home"></i>-->
                            <span class="hide-menu"><?php echo lang("home_page"); ?></span>
                        </a>
                    </li>
                    <?php if (isset($sidebar)) { ?>
                        <?php foreach ($sidebar as $item) { ?>
                            <?php if (isset($item['dropdown'])) { ?>

                                <li class="fix-lang">
                                    <?php if ($item['page'] == 'index') { ?>
                                        <a class="has-arrow waves-effect waves-dark" href="index.html#"
                                           aria-expanded="false">
                                            <!--<i class="<?php echo $item['icon'] ?>"></i>-->
                                            <span class="hide-menu">
                                                            <?php echo lang($item['controller']); ?>
                                                        </span>
                                        </a>
                                        <ul aria-expanded="false" class="collapse">
                                            <?php foreach ($item['dropdown'] as $key => $sub) { ?>
                                                <li class="<?php echo $page == $key ? ' active' : '' ?>">
                                                    <a href="<?php echo base_url("$sub"); ?>">
                                                        <?php echo lang($key) ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } else { ?>

                                        <a class="has-arrow waves-effect waves-dark" href="index.html#"
                                           aria-expanded="false">
                                            <!--<i class="<?php echo $item['icon'] ?>"></i>-->
                                            <span class="hide-menu">
                                                            <?php echo lang($item['page']); ?>
                                                        </span>
                                            <!--<b class="caret"></b>-->
                                        </a>
                                        <ul aria-expanded="false" class="collapse">
                                            <?php foreach ($item['dropdown'] as $key => $sub) { ?>
                                                <li class="<?php echo $page == $key ? ' active' : '' ?>">
                                                    <a href="<?php echo base_url("$sub"); ?>">
                                                        <?php if ($item['page'] == 'teacher_room') { ?>
                                                            <?php echo $key; ?>
                                                        <?php } else { ?>
                                                            <?php echo lang($key); ?>
                                                        <?php } ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>

                                </li>
                            <?php } else { ?>
                                <?php if ($item['page'] == 'index') { ?>


                                    <li class="fix-lang" <?php echo $page == $item['page'] && $Page_controller == $item['controller'] ? 'class="active"' : '' ?>>
                                        <a class=" waves-effect waves-dark" href="<?php echo $item['link']; ?>">
                                            <!--<i class=" <?php echo $item['icon']; ?>    "></i>-->
                                            <span class="hide-menu"><?php echo lang($item['controller']); ?></span>
                                        </a>
                                    </li>


                                <?php } else { ?>
                                    <li class="fix-lang" <?php echo $page == $item['page'] ? 'class="active"' : '' ?>>
                                        <a class=" waves-effect waves-dark" href="<?php echo $item['link']; ?>">
                                            <!--<i class=" <?php echo $item['icon']; ?>    "></i>-->
                                            <span class="hide-menu"><?php echo lang($item['page']); ?></span>
                                        </a>
                                    </li>

                                <?php } ?>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>


                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container" >
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <div class="non_printable row page-titles">
                <div class="col-md-12 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item fix-lang">
                            <?php if (isset($user_login) && $user_login && $user_login->type == "teacher") { ?>
                                <a  href="<?php echo base_url('home/home_page_teacher') ?>" ><?php echo lang('home_page') ?></a>
                            <?php } elseif (isset($user_login) && $user_login && $user_login->type == "parent") { ?>
                                <a href="<?php echo base_url('home/home_page_parent') ?>"  ><?php echo lang('home_page') ?></a>
                            <?php } else { ?>
                                <a  href="<?php echo base_url('home/home_page') ?>" ><?php echo lang('home_page') ?></a>
                            <?php } ?>

                        </li>
                        <li class="breadcrumb-item active  fix-lang"><?php echo $page_title ?></li>
                    </ol>
                </div>

                <!--                                                                            <div class="">
                                                                                                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10">
                                                                                                    <i class="ti-settings text-white"></i></button>
                                                                                            </div>-->
            </div>


            <?php // if ($_current_year != $_archive_year) {             ?>
            <div class="card non_printable">
                <!--    <div class="card-header" style="    border-bottom: 2px solid #46bce8;background-color: #fff;">-->
                <div class="card-header" style="border: 3px solid #46bce84f;
                                 box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.001), 0 2px 20px 0 rgba(0, 0, 0, 0.10);">
                    <div class="academic_year col-md-12 align-self-center text-center" style="font-style: oblique; font-size:15px;" >
                        <?php
                        //                                                                        var_dump($_archive_year);
                        echo lang('academic_year') . ': ' . $_archive_year . ' - ' . ($_archive_year + 1);
                        ?>
                    </div>
                </div>
            </div>
            <?php // }             ?>
            <!-- Info box -->
            <!-- ============================================================== -->
            <?php echo $yield ?>
            <!-- ============================================================== -->
            <!-- End Info box -->

            <!-- .right-sidebar -->


        </div>

        <footer class="non_printable footer text-center">
            <a href="http://gwa-us.com/">POWER BY © GWA GROUP 2010 - <?php echo date("Y") ?></a>
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php } else { ?>
<body class="card-no-border">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Login</p>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar"
         style="background-image:url(<?php echo base_url(); ?>assets/assets/images/background/login-register.jpg)">
    <?php echo $yield; ?>
</section>
<?php } ?>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<style>

    @media (max-width: 1024px){
        .modal-dialog {
            max-width: 498px;
            margin: 1.75rem auto;
        }
    }
    @media (max-width: 767px){
        .mini-sidebar .sidebar-nav {
            min-height: 112px;

        }
    }
    @media (min-width: 768px){
        .caret {
            margin-right: 2px;
            margin-left: 0;
            display: inline-block;
            width: 0;
            height: 0;
            margin-left: 2px;
            vertical-align: middle;
            border-top: 4px dashed;
            border-top: 4px solid\9;
            border-right: 4px solid transparent;
            border-left: 4px solid transparent;
        }
    }

    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .25);
    }

    .fix-lang, .select2-selection {
    <?php if ($lang == 'ar') { ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php } else { ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php } ?>
    }

    .select2-container--default .select2-results>.select2-results__options{
        margin-top: -9px !important;
    }

    .select2-selection {
        width: 100%;
    }

    .select2-style {
    <?php if ($lang == 'ar') { ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php } else { ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php } ?>
    }

    .select2-container .select2-selection--single, .select2-dropdown {
        border: 1px solid rgba(0, 0, 0, .15) !important;
    }

    .label-selec, .control-label {
        line-height: 3;
    <?php if ($lang == 'ar') { ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php } else { ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php } ?>
    }

    .reverse-fix-lang {
    <?php if ($lang == 'ar') { ?> float: left !important;;
        direction: ltr !important;;
        text-align: left !important;;
    <?php } else { ?> float: right !important;
        direction: rtl !important;;
        text-align: right !important;;
    <?php } ?>
    }

    .table td, .table th {
        text-align: center;
    }

    .color-table.primary-table thead th {
        background-color: #6772e5 !important;
        color: #ffffff;
    }

    .color-table.info-table thead th {
        background-color: #20aee3 !important;
        color: #ffffff;
    }

    .color-bordered-table.info-bordered-table {
        border: 2px solid #20aee3 !important;
    }

    .color-bordered-table.info-bordered-table thead th {
        background-color: #20aee3 !important;
        color: #ffffff;
        border: 1px solid #dee2e6!important;
    }

    .hover-opcity {
        opacity: 0.8;
    }

    .cursor-pointer {
        cursor: pointer;
    }

    .css-bar-pink.css-bar-100 {
        background-image: linear-gradient(450deg, #fd5f95 50%, transparent 50%, transparent), linear-gradient(270deg, #fd5e94 50%, #fafafa 50%, #fafafa);
    }

    .table-striped tbody tr:nth-of-type(odd) {
        background: #eaf9ff;
    }

    /*card style*/
    .card-actions a {
        padding-left: 5px;
        padding-right: 5px;
    }

    .card-header {
        border-right: 3px solid #46bce8;
        background-color: #fff;
        border-bottom: 0;
        border-top-right-radius: 0 !important;
        border-top-left-radius: 0 !important;
    }

    /*round*/

    .round-inverse {
        background: #4c5667;
    }

    .round-purple {
        background: #7460ee;
    }

    .round-pink {
        background: #fd5e94;
    }

    .round-default {
        background: #248be6;
    }

    .sidebar-nav > ul > li > a.active {
        background: #ffffff00!important;
    }



    .mini-sidebar .sidebar-nav #sidebarnav > li:hover > a {
        background: #ffffff00!important;
    }

    .mini-sidebar .sidebar-nav #sidebarnav > li:focus > a {
        background:  #ffffff00!important;
    }

    .mini-sidebar .sidebar-nav #sidebarnav > li > ul {
        background: #ffffff!important;
    }


    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 5px);
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 5px);
        padding: 0;
        padding-left: 5px;
        padding-right: 5px;
    }

    .clockpicker-popover {
        z-index: 99999 !important;
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 5px);
        padding: 0;
        padding-left: 5px;
        padding-right: 5px;
    }

    @media (min-width: 1200px) {
        /*.mini-sidebar .sidebar-nav, .mini-sidebar .scroll-sidebar{*/
        /*max-width: 1335px;*/
        /*}*/
        /*.container {*/
        /*max-width: 1280px;*/
        /*}*/
    }

    .clockpicker-popover .popover-title {
        padding-left: 75px;
        padding-right: 75px;
    }

    span.clockpicker-span-hours.text-primary {
        float: left;
        text-align: right;
    }

    span.clockpicker-span-minutes {
        float: right;
        text-align: left;
    }

    html body .font-light{
        line-height: 26px!important;
    }


    td
    {
        vertical-align: middle !important;
    }
</style>

<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url("assets/assets/node_modules/ps/perfect-scrollbar.jquery.min.js"); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo base_url("assets/$dir/js/waves.js"); ?>"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url("assets/$dir/js/sidebarmenu.js"); ?>"></script>
<!--stickey kit -->
<script src="<?php echo base_url("assets/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/sparkline/jquery.sparkline.min.js"); ?>"></script>

<!--Custom JavaScript -->
<script src="<?php echo base_url("assets/$dir/js/toastr.js"); ?>"></script>
<script src="<?php echo base_url("assets/custom/custom.js"); ?>"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->
<script src="<?php echo base_url("assets/assets/node_modules/raphael/raphael-min.js"); ?>"></script>
<!--<script src="--><?php //echo base_url("assets/assets/node_modules/morrisjs/morris.min.js");                                                                                                                                                                                                                                                                              ?><!--"></script>-->
<!--c3 JavaScript -->
<script src="<?php echo base_url("assets/assets/node_modules/d3/d3.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/c3-master/c3.min.js"); ?>"></script>
<!-- Popup message jquery -->
<script src="<?php echo base_url("assets/assets/node_modules/toast-master/js/jquery.toast.js"); ?>"></script>
<!-- Chart JS -->
<script src="<?php echo base_url("assets/$dir/js/dashboard1.js"); ?>"></script>

<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- This is data table -->
<script src="<?php echo base_url("assets/assets/node_modules/datatables/jquery.dataTables.min.js"); ?>"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url("assets/assets/node_modules/datatables/dataTables.buttons.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/datatables/buttons.flash.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/datatables/jszip.min.js"); ?>"></script>

<script src="<?php echo base_url("assets/assets/node_modules/datatables/buttons.html5.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/datatables/buttons.print.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/styleswitcher/jQuery.style.switcher.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-switch/bootstrap-switch.min.js"); ?>"></script>

<!-- This page plugins -->
<!-- ============================================================== -->
<script src="<?php echo base_url("assets/assets/node_modules/switchery/dist/switchery.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"); ?>"></script>

<!-- This page plugins -->
<!-- ============================================================== -->
<script src="<?php echo base_url("assets/assets/node_modules/gauge/gauge.min.js"); ?>"></script>
<!-- ============================================================== -->
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/timepicker/bootstrap-timepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"); ?>"></script>
<!-- =================== Clock picker plugins js=================== -->
<script src="<?php echo base_url("assets/assets/node_modules/moment/moment.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/assets/node_modules/timepicker/bootstrap-timepicker.min.js"); ?>"></script>

<script>
    //
    // $( ".sortable" ).sortable();
    var sort = $('.sortable');
    $('.sortable').sortable({
        handle: '.handle',
        update: function () {
            sort = $(this);
            var order = 'sort=true&' + $('.sortable').sortable('serialize');
            $.post($('.sortable').data('url'), order, function (theResponse) {
            });
        }
    });
    // MAterial Date picker
    $('#timepicker').bootstrapMaterialDatePicker({format: 'HH:mm', time: true, date: false});
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').on('change', function () {
    });
    $('#check-minutes').on('click', function (e) {
// Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });</script>

<script>

    jQuery(function () {
        // Date Picker
        jQuery('.mydatepicker, #datepicker').datepicker({
            format: 'mm-dd-yyyy'
        });
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'mm-dd-yyyy',
        });
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function () {
            new Switchery($(this)[0], $(this).data());
        });
        // For select 2
        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }
        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: - 1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();
        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });
        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });
        //$(".touch-spin-number").TouchSpin();
        $(".touch-spin-number").TouchSpin({
            min: 0,
            max: 10000000000,
        });
    });</script>


<script>
    $(function () {
        $('#myTable').DataTable();
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            //                            'copy', 'excel', 'print'
        ]
    });
    $('.my_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'print'
        ]
    });
</script>
<script>
    $(function () {
        "use strict";
        <?php if ($this->session->flashdata('messages_success')): ?>
        $.toast({
            heading: "<?php echo lang('success') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_success')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 10000,
            stack: 6
        });
        <?php endif ?>
        <?php if ($this->session->flashdata('messages_info')): ?>
        $.toast({
            heading: "<?php echo lang('info') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_info')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'info',
            hideAfter: 10000,
            stack: 6
        });
        <?php endif ?>

        <?php if ($this->session->flashdata('messages_warning')): ?>
        $.toast({
            heading: "<?php echo lang('warning') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_warning')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'warning',
            hideAfter: 10000,
            stack: 6
        });
        <?php endif ?>
        <?php if ($this->session->flashdata('messages_error')): ?>
        $.toast({
            heading: "<?php echo lang('error') ?>",
            text: "<?php echo preg_replace("/\r|\n/", " ", $this->session->flashdata('messages_error')) ?>",
            position: 'top-left',
            loaderBg: '#ff6849',
            icon: 'error',
            hideAfter: 10000,
        });
        <?php endif ?>



    });</script>
<script async type="text/javascript">
    $(document).ready(function () {
        if (document.getElementById('printable') == null) {
            $('#prin_btn').css('display', 'none');
        }
        $('#prin_btn').click(function () {
            window.print();
            return false;
        });
        $('[data-target="#delete-modal"] ,[data-target="#details-modal"] , [data-target="#school-contact-info-modal"] ,[data-target="#connect-modal"],[data-target="#cancel_connect-modal"], [data-target="#add-modal"]').on('click', function (event) {
            event.preventDefault();
            var item_id = $(this).closest('tr').data('id');
            var item_title = $(this).closest('tr').data('title');
            var modal = $($(this).data('target'));
            modal.find('span[id="item_title"]').html(item_title);
            modal.find('input[name="item_id"]').val(item_id);
        });
    });</script>
<script>


    function messages_success(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'success',
            hideAfter: 10000,
            stack: 1
        });
    }

    function messages_info(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'info',
            hideAfter: 10000,
            stack: 6
        });
    }

    function messages_warning(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'warning',
            hideAfter: 10000,
            stack: 6
        });
    }

    function messages_error(heading, message) {
        return $.toast({
            heading: heading,
            text: message,
            position: 'top-left',
            loaderBg: '#6142ff',
            icon: 'error',
            hideAfter: 10000,
        });
    }

    $(".form-ajax").submit(function (event) {
        // $('.submit_click').prop('disabled', 'disabled');
        var my_form = $(this);
        var datastring = my_form.serialize();
        var url_data = my_form.attr('url-data');
        var action_form = my_form.attr('action-form');
        var new_url = "<?php echo base_url('') ?>" + url_data;
        var row_id = $(my_form).find('input[name="item_id"]').val();
        var item = "#item-" + row_id;
        $("[data-id=row_id]").css("background", "yellow!important");
        $.ajax({
            type: "POST",
            url: new_url,
            data: datastring,
            dataType: 'json',
            success: function (data) {

                if (data.success) {
                    messages_success(data.success, data.message);
                    if (action_form == "delete") {
                        $('.my_table').find(item).remove();
                    }

                    if (action_form == "update") {
                        var this_row_table = $('.my_table').find(item);
                        var this_form = this_row_table.find('.form-ajax');
                        //المرور على جميع عناصر الفورم
                        $(this_form).each(function () {
                            var dataArray = $(this).serializeArray(), dataObj = {};
                            $(dataArray).each(function (i, field) {

                                //ايجاد السطر الحالي الذي يتم التعديل عليه والمرور على جميع عناصره
                                this_row_table.find('td').each(function (column, td) {
                                    //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                    if (field.name == $(td).attr("data-name")) {
                                        $(td).find('span').text(field.value);
                                    }
                                });
                            });
                            //جلب جميع دروبداون الفورم
                            var dataArraySelect = $(this).find("select").serializeArray(), dataObj = {};
                            $(dataArraySelect).each(function (i, field) {
                                //ايجاد السطر الحالي الذي يتم التعديل عليه والمرور على جميع عناصره
                                this_row_table.find('td').each(function (column, td) {
                                    //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                    if (field.name == $(td).attr("data-name")) {
                                        var text_select = $(my_form).find('select[name=' + field.name + '] option:selected').text();
                                        $(td).find('span').text(text_select);
                                    }
                                });
                            });
                            $(my_form).find('input[type=checkbox]').each(function () {


                                var this_checkbox = this;
                                if ($(this_checkbox).is(":checked")) {
                                    //ايجاد السطر الحالي الذي يتم التعديل عليه والمرور على جميع عناصره
                                    this_row_table.find('td').each(function (column, td) {

                                        //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                        if ($(this_checkbox).attr("name") == $(td).attr("data-name")) {
//                    alert($(this_checkbox).attr("name"));
                                            // console.log($(this_checkbox).attr("name"));
                                            var text_select = $(td).attr("check");
//                    alert(text_select);
                                            $(td).find('span').text(text_select);
                                        }
                                    });
                                } else {
                                    this_row_table.find('td').each(function (column, td) {

                                        //في حال كان العمود الحالي للجدول موافق لحقل الفورم الحالي يتم تعديل قيمة عمود الجدول بقيمة حقل الفورم
                                        if ($(this_checkbox).attr("name") == $(td).attr("data-name")) {
//                    alert($(this_checkbox).attr("name"));
                                            // console.log($(this_checkbox).attr("name"));
                                            var text_select = $(td).attr("uncheck");
//                    alert(text_select);
                                            $(td).find('span').text(text_select);
                                        }
                                    });
                                }
                            });
                        });
                    }

                }
                if (data.error) {
                    messages_error(data.error, data.message);
                }
                if (data.warning) {
                    messages_warning(data.warning, data.message);
                }
                if (data.info) {
                    messages_info(data.info, data.message);
                }

            }
        });
        event.preventDefault();
    });
</script>



<script type="text/javascript">
    $(".search-box a, .search-box .app-s .srh-btn").on('click', function () {
        $(".app-s").toggle(250);
    });
    $(function () {
        //autocomplete
        $(".auto_complete_search").autocomplete({
            minLength: 1,
            source: "<?php echo base_url() ?>index.php/student/search_auto_complete",
            select: function (event, ui) {
                $("#inputSearch_value").val(ui.item.value);
                $("#inputSearch_key").val(ui.item.key);
                $(".app-s").submit();
                return false;
            }

        });
    });
</script>

<script>
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('wheel', function (e) {
            e.preventDefault();
        });
    });
    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('wheel');
    });
    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function (e) {
        if (e.which == 38 || e.which == 40)
            e.preventDefault();
    });
    $(".calender_icon").click(function(e){
        var parent = $(this).parent().parent();
        var input = parent.find("input");
        input.focus();
    });
    $("#loginForm").submit(function(e){
        e.preventDefault();
        log();
    });
    $(".calender_icon").click(function(e){
        var parent = $(this).parent().parent();
        var input = parent.find("input");
        input.focus();
    });
</script>

</body>
</html>