<?php bs3_card($page_title); ?>
<?php if ($_current_year == $_archive_year) { ?> 
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_teacher") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_checkbox('assistant_teacher') ?>
    <?php bs3_input('first_name') ?>
    <?php bs3_input('last_name') ?>
    <?php bs3_date('birthdate') ?>
    <?php bs3_tel('mobile') ?>
    <?php bs3_input('address') ?>
    <?php // bs3_input('educational_attainment')  ?>
    <?php bs3_email('email') ?>
    <?php // bs3_number('salary')  ?>
    <?php bs3_input('non_encrept_pass', $non_encrept_pass) ?>
    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable, '', 'width:100px!important;') ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>