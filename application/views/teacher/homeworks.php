<?php bs3_card($page_title); ?>
<?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_homework") ?>
<?php bs3_hidden('id') ?>
<?php bs3_input('title'); ?>
<?php bs3_dropdown('room_teacher_id', $teacher_subjects); ?>
<?php bs3_textarea('msg_content'); ?>

<?php bs3_checkbox('upload_file_check'); ?>
<div id="homeworks_attachment">
    <?php bs3_file_or_image('homeworks_attachment', '', false, '100', 'false', false) ?>    
</div>
<script>
    $(document).ready(function () {

        $('#homeworks_attachment').hide();
        $('#checkboxupload_file_check').change(function () {

            if (this.checked) {
                $('#homeworks_attachment').fadeIn();
                var element = "";
                element += '<span class="btn btn-default btn-file">';
                element += '  <span class="fileinput-new">';
                element += '<?php echo lang("choose_file") ?>';
                element += '  </span>';
                element += '  <span class="fileinput-exists">';
                element += '<?php echo lang("change") ?>';
                element += '  </span>';
                element += '<input required="true"  type="file" accept=".pdf,.docx,.doc,.xls,.xlsx,.txt,.csv,.ppt,.pptx,.zip,.rar,.psd,.txt,.pps,image/*" id="file" name="homeworks_attachment">';
                element += '  </span>';
                element += '<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">';
                element += '<?php echo lang("remove") ?>';
                element += '</a>';
                $("#div_for_file").html(element);
            } else {
                $("#homeworks_attachment").remove();
            }

        });
    });
</script>




<?php bs3_date('deadline') ?>
<?php bs3_modal_footer('create'); ?>


<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>

<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>