<?php bs3_card($page_title, '', false); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_teacher") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_input('teacher_id', false, true, 'update') ?>
    <?php bs3_hidden('teacher_key') ?>

    <?php
    bs3_dropdown('grade_id', $grades_options, FALSE, FALSE, FALSE, 'update');
    ?>

    <?php
    $options = array('000' => lang('select_room_name'));
    $options_extra = array();
    foreach ($rooms_options as $item) {
        $options[$item->id] = $item->room_name;
        $options_extra[$item->id]['key'] = 'grade_id1';
        $options_extra[$item->id]['value'] = $item->grade_id;
    }
    ?>
    <div id="room_id">
        <?php bs3_dropdown('room_id', $options, FALSE, FALSE, $options_extra); ?>
    </div>

    <?php
    $options = array('000' => lang('select_subject'));
    $options_extra = array();
    foreach ($subjects_options as $item) {
        $options[$item->id] = $item->subject_name;
        $options_extra[$item->id]['key'] = 'grade_id2';
        $options_extra[$item->id]['value'] = $item->grade_id;
    }
    ?>
    <div id="subject_id">
        <?php bs3_dropdown('subject_id', $options, FALSE, FALSE, $options_extra); ?>
    </div>

    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>
<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>

<style>
    ul#ui-id-1 {z-index:10000!important}
</style>



<script>
    $(document).ready(function () {
        var add = <?php echo $add; ?> ;
        if (add === '1') {
            $('#room_id').hide();
            $('#subject_id').hide();
        }
    });
</script>

<script>
    $(function () {
        $("#inputTeacher_id").autocomplete({
            minLength: 1,
            source: "<?php echo base_url() ?>room_teacher/search_teacher_auto",
            focus: function (event, ui) {
                $("#inputTeacher_id").val(ui.item.value);
                return false;
            },
            select: function (event, ui) {
                $("#inputTeacher_id").val(ui.item.value);
                $("#inputTeacher_key").val(ui.item.key);
                return false;
            }
        });
    });

    $("#inputGrade_id").change(function () {
        if ($(this).val() == '000') {
            $('#room_id').fadeOut();
            $('#subject_id').fadeOut();
        } else {
            $('#room_id').fadeIn();
            $('#subject_id').fadeIn();
        }

        if ($(this).data('options') == undefined) {
            $(this).data('options', $('#inputRoom_id option').clone());
            $(this).data('options1', $('#inputSubject_id option').clone());
        }
        var id = $(this).val();
        var options = $(this).data('options').filter('[grade_id1=' + id + ']');
        var options1 = $(this).data('options1').filter('[grade_id2=' + id + ']');
        $('#inputRoom_id').html(options);
        $('#inputSubject_id').html(options1);
    });
</script>
