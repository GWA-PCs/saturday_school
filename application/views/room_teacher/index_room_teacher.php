<?php $room = (isset($room_name)) ? lang('room_id') . ': ' . $room_name->room_name . '<br><br>' . $room_name->grade_name : ""; ?>
<?php bs3_card($page_title, $room); ?>
<?php if ($_current_year == $_archive_year) { ?>
    <?php bs3_modal_header_crud($modal_name, 'create', "fa fa-plus-circle  hvr-icon", "add_new_teacher") ?>
    <?php bs3_hidden('id') ?>
    <?php bs3_input('teacher_id', false, true, 'update') ?>
    <?php bs3_hidden('teacher_key') ?>
    <?php bs3_dropdown('subject_id', $subjects_room_options) ?>
    <?php bs3_modal_footer('create'); ?>
<?php } ?>
<?php bs3_table($thead, 'object_table', '', '', '', $non_printable) ?>
<?php bs3_table_f() ?>
<?php bs3_card_f(); ?>

<style>
    ul#ui-id-1 {z-index:10000!important}
</style>
<script>
    $(function () {
        $("#inputTeacher_id").autocomplete({
            minLength: 1,
            source: "<?php echo base_url() ?>room_teacher/search_teacher_auto",
            focus: function (event, ui) {
                $("#inputTeacher_id").val(ui.item.value);
                return false;
            },
            select: function (event, ui) {
                $("#inputTeacher_id").val(ui.item.value);
                $("#inputTeacher_key").val(ui.item.key);
                return false;
            }
        });
    });
</script>