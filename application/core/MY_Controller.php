<?php

/**
 * A base controller for CodeIgniter with view autoloading, layout support,
 * model loading, helper loading, asides/partials and per-controller 404
 *
 * @link http://github.com/jamierumbelow/codeigniter-base-controller
 * @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
 */
class MY_Controller extends CI_Controller {

    protected $view = '';
    protected $data = array();
    protected $layout;
    protected $asides = array();
    protected $models = array();
    protected $model_string = '%_model';
    protected $helpers = array();
    protected $_lang;
    protected $_create;
    protected $_update;
    protected $_delete;
    protected $_user_login;
    protected $_user_previlages;
    public $_current_year;
    public $_archive_year;
    public $_login_email;
    protected $_session;

    public function __construct() {
        parent::__construct();
        $this->output->set_header('X-FRAME-OPTIONS: DENY');
        $this->_load_models();
        $this->_load_helpers();
        $ci = &get_instance();
        $this->load->helper('arrays_helper');

        $this->layout = 'layouts/layout';
        $this->load->model('rooms_teachers_model');
        $this->load->model('rooms_model');
        $this->load->model('teachers_model');
        $this->_session = $this->session->userdata('sat_data');

        if ($this->ion_auth->logged_in()) {

            $this->data['user_login'] = $this->_user_login = $this->ion_auth->user()->row();

            $id = $this->ion_auth->user()->row()->id;

            $this->load->model('general_model', 'users_authentication_m');
            $this->users_authentication_m->set_table('users_authentication');

            $where = array('users_id' => $id);
            $this->db->select('users_id,login_email');
            $this->db->order_by('updated_at', 'desc');
            $login_email = $this->users_authentication_m->get_by($where);

            if (isset($login_email) && $login_email) {
                $this->_login_email = $login_email->login_email;
            }

            $this->data['login_email'] = $this->_login_email;

            $this->session->set_userdata('user_login', $this->ion_auth->user()->row());
            $this->_get_user_previlages();
            $this->data['previlages'] = $this->_user_previlages;

            $this->load->model('settings_model');

            $this->_session['_current_year'] = $this->settings_model->get_current_year();

            if (!isset($this->_session['_archive_year'])) {
                $this->_archive_year = $this->_session['_archive_year'] = $this->_session['_current_year'];
            } else {
                $this->_archive_year = $this->_session['_archive_year'];
            }
            
        
            $this->data['_archive_year'] = $this->_archive_year = $this->_session['_archive_year'];
            $this->data['_current_year'] = $this->_current_year = $this->_session['_current_year'];
            $this->save_session();
            
//  var_dump($this->_session);
            //if the is error in url - validation - user type teacher
            $method = $this->router->fetch_method();
            if ($this->_user_login->type == "teacher") {
                $teacher_array = get_all_teacher_indexes();
//                var_dump(in_array($method, $teacher_array));
//                die;
                if (!in_array($method, $teacher_array)) {
                    redirect('home/home_page_teacher');
                }
            } elseif ($this->_user_login->type == "parent") {
                $parent_array = get_all_parent_indexes();
                if (!in_array($method, $parent_array)) {
                    redirect('home/home_page_parent');
                }
            } elseif ($this->_user_login->type == "student") {
                $student_array = get_all_student_indexes();

                if (!in_array($method, $student_array)) {
                    redirect('home/home_page_student');
                }
            }
        }

        // language
        // if not found lang in session set english  lang
//        $this->session->set_userdata('lang', 'english');
        $this->data['lang'] = check_lang_session();
//        var_dump(check_lang_session());
        if ($this->data['lang'] == 'ar') {
            $this->data['dir'] = 'rtl';
            $this->data['text_align'] = 'text-right';
        } else {
            $this->data['dir'] = 'ltr';
            $this->data['text_align'] = 'text-left';
        }


        if ($this->_user_login) {
            $rooms_of_teachers = array();
            if ($this->_user_login->type == "teacher") {
                $rooms_of_teachers = $this->_get_rooms_of_teacher();
            }
            $this->data['sidebar'] = get_employee_links_array($this->router->fetch_directory(), $this->router->fetch_class(), $this->_user_login->type, $rooms_of_teachers);
        }

        $this->data['home_page_links'] = home_page_links();
        if ($this->_user_login) {
            if ($this->_user_login->type == "teacher") {
                $rooms_of_teachers = $this->_get_rooms_of_teacher();
                $this->data['home_page_links'] = teacher_home_page_links($rooms_of_teachers);
            } elseif ($this->_user_login->type == "parent") {
                $this->data['home_page_links'] = parent_home_page_links();
            } elseif ($this->_user_login->type == "student") {
                $this->data['home_page_links'] = student_home_page_links();
            }
        }

        $this->data['color'] = get_color_array();
        $this->data['color_wid'] = get_color_array_for_wid();

        //check if segment 3 == lang
        change_lang($this->uri->segment(3), $this->uri->segment(4));
        $this->_lang = $this->session->userdata('lang');
        if ($this->_lang == 'arabic')
            $this->_lang = 'ar';
        else {
            $this->_lang = 'en';
        }
        $this->config->set_item('language', $this->session->userdata('lang'));
        $this->lang->load('dashboard', $this->session->userdata('lang'));
        $this->data['page_title'] = lang($this->router->fetch_method());
        $this->data['function_name'] = $this->data['page'] = $this->router->fetch_method();

        $this->_create = $this->input->post('create');
        $this->_update = $this->input->post('update');
        $this->_delete = $this->input->post('delete');
         date_default_timezone_set('America/Los_Angeles');
    }

    public function _remap($method) {
        if (method_exists($this, $method)) {
            call_user_func_array(array($this, $method), array_slice($this->uri->rsegments, 2));
        } else {
            if (method_exists($this, '_404')) {
                call_user_func_array(array($this, '_404'), array($method));
            } else {
                show_404(strtolower(get_class($this)) . '/' . $method);
            }
        }

        $this->_load_view();
    }

    /**
     * Automatically load the view, allowing the developer to override if
     * he or she wishes, otherwise being conventional.
     */
    protected function _load_view() {
        // If $this->view == FALSE, we don't want to load anything
        if ($this->view !== FALSE) {
            // If $this->view isn't empty, load it. If it isn't, try and guess based on the controller and action name
            $view = (!empty($this->view)) ? $this->view : $this->router->directory . $this->router->class . '/' . $this->router->method;

            // Load the view into $yield
            $data['yield'] = $this->load->view($view, $this->data, TRUE);

            // Do we have any asides? Load them.
            if (!empty($this->asides)) {
                foreach ($this->asides as $name => $file) {
                    $data['yield_' . $name] = $this->load->view($file, $this->data, TRUE);
                }
            }

            // Load in our existing data with the asides and view
            $data = array_merge($this->data, $data);
            $layout = FALSE;

            // If we didn't specify the layout, try to guess it
            if (!isset($this->layout)) {
                if (file_exists(APPPATH . 'views/layouts/' . $this->router->class . '.php')) {
                    $layout = 'layouts/' . $this->router->class;
                } else {
                    $layout = 'layouts/application';
                }
            } // If we did, use it
            else if ($this->layout !== FALSE) {
                $layout = $this->layout;
            }

            // If $layout is FALSE, we're not interested in loading a layout, so output the view directly
            if ($layout == FALSE) {
                $this->output->set_output($data['yield']);
            } // Otherwise? Load away :)
            else {
                $this->load->view($layout, $data);
            }
        }
    }

    /* --------------------------------------------------------------
     * MODEL LOADING
     * ------------------------------------------------------------ */

    /**
     * Load models based on the $this->models array
     */
    private function _load_models() {
        foreach ($this->models as $model) {
            $this->load->model($this->_model_name($model), $model);
        }
    }

    /**
     * Returns the loadable model name based on
     * the model formatting string
     */
    protected function _model_name($model) {
        return str_replace('%', $model, $this->model_string);
    }

    /* --------------------------------------------------------------
     * HELPER LOADING
     * ------------------------------------------------------------ */

    /**
     * Load helpers based on the $this->helpers array
     */
    private function _load_helpers() {
        foreach ($this->helpers as $helper) {
            $this->load->helper($helper);
        }
    }

    function _set_validation($names_array = array()) {
        foreach ($names_array as $key => $value) {
            if (!empty($value)) {
                $this->form_validation->set_rules($key, lang($key), $value);
            }
        }
    }

    function _validation($names_array = array(), $link = "") {
        if (!empty($names_array)) {
            $this->_set_validation($names_array);
        }
    }

    function _get_user_previlages() {
        $this->load->model("groups_model");
        $this->load->model("previlages_model");
        $user_info = $this->_user_login;
        if ($user_info) {
            $user_id = $user_info->id;
            $user_groups = $this->groups_model->get_groups_by_user_id($user_id);
            $user_previlages = $this->previlages_model->get_previlages_by_groups_id($user_groups);
            $previlage_array = array();
            if ($user_previlages) {
                foreach ($user_previlages as $previlage) {
                    $previlage_array[$previlage->id] = $previlage->function_name;
                }
            }

            $this->_user_previlages = $previlage_array;
        }
    }

    function authentication_login() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/index');
        }
    }

    protected function save_session() {
        $this->session->set_userdata('sat_data', $this->_session);
    }

    function _get_rooms_of_teacher() {
        $teacher_rooms = array();
        $user_info = $this->_user_login;
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);

        $teacher_rooms = $this->rooms_teachers_model->get_teacher_room($teacher_id);
      
        return $teacher_rooms;
    }

}
