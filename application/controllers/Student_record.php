<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_record extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_records_model');
        $this->load->model('students_model');
//        $this->load->model('types_payments_model');
//        $this->load->model('payments_model');
        $this->load->model('rooms_model');
        $this->load->model('ion_auth_model');
        $this->load->model('grades_model');
        $this->load->model('general_model', 'users_model');
        $this->users_model->set_table('users');
        $this->load->model('students_allergies_model');
        $this->load->model('students_notes_model');
        $this->load->model('students_payments_model');
    }

    function index() {
        
    }

    /**
     * @api {get} /student_record/index_student_record index_student_record
     * @apiName index_student
     * @apiGroup student_record
     * @apiDescription home page of student record
     * @apiParam {Number} room_id id of room  
     */
    function index_student_record($room_id) {
        $this->data['room_name'] = $room_name = $this->rooms_model->get_room($room_id);
//        $this->data['types_payments'] = $this->types_payments_model->get_type_payment_array();

        $photo_permission_slips_array = photo_permission_slips_array();
        $status_islamic_book_return_array = status_islamic_book_return_array();
        $status_islamic_book_return_array['0'] = lang('select_book_status');

        $this->data['status_islamic_book_return_array'] = $status_islamic_book_return_array;
        $this->data['photo_permission_slips_options'] = $photo_permission_slips_array;

        $this->data['show_object_link'] = 'student_record/show_student_records/' . $room_id;
        $this->data['get_object_link'] = 'student_record/get_student_record';
//        $this->data['permissions_link'] = 'student_record/permission_student_record';
        $this->data['add_object_link'] = 'student_record/add_student_record/' . $room_id;
        $this->data['update_object_link'] = 'student_record/update_student_record/' . $room_id;
        $this->data['delete_object_link'] = 'student_record/delete_student_record/' . $room_id;
        $this->data['modal_name'] = 'crud_student_records';
        $this->data['add_object_title'] = lang("add_new_student_record");
        $this->data['update_object_title'] = lang("update_student_record");
        $this->data['delete_object_title'] = lang("delete_student_record");
        $this->data['thead'] = array('student_name', 'is_active',);
//            'teacher_child', 'photo_permission_slips'
//        if ($room_name->stage_id == '2') {
//            $this->data['thead'][] = 'islamic_book_return';
//            $this->data['thead'][] = 'status_islamic_book_return';
//        }

        $this->data['thead'][] = 'index_student_allergy';
        $this->data['thead'][] = 'index_student_note';
        $this->data['thead'][] = 'index_general_social_development';
        $this->data['thead'][] = 'index_report_card';

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['index_student_allergy'] = 'index_student_allergy';
        $this->data['non_printable']['index_student_note'] = 'index_student_note';
        $this->data['non_printable']['index_general_social_development'] = 'index_general_social_development';
        $this->data['non_printable']['index_report_card'] = 'index_report_card';

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /student_record/show_student_records show_student_records
     * @apiName show_student_records
     * @apiGroup student_record
     * @apiDescription get all student records
     * @apiParam {Number} room_id id of room  
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student records table
     * @apiError  FALSE return false if failure in get data
     */
    function show_student_records($room_id) {
        $student_records = $this->students_records_model->get_students_records($room_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        $links = array('report_card/index_report_card/1',
            'report_card/index_report_card/2',);
        foreach ($student_records as $student_record) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_student_profile/" . $student_record->student_id, $student_record->student_name . ' ' . $student_record->family_name, '', 'update');

            $row[] = tinyint_lang_one_and_two($student_record->is_active);
//            $row[] = tinyint_lang_one_and_two($student_record->teacher_child);
//            $row[] = tinyint_lang_one_and_two($student_record->photo_permission_slips);
//            if ($student_record->stage_id == '2') {
//                $row[] = tinyint_lang_one_and_two($student_record->islamic_book_return);
//                $row[] = lang($student_record->status_islamic_book_return);
//            }
            //add html for action

            $row[] = bs3_link_crud("student_allergy/index_student_allergy/" . $student_record->id, '<i class="mdi mdi-hospital fa-2x "></i>', lang('index_student_allergy'), 'update');
            $row[] = bs3_link_crud("student_note/index_student_note/" . $student_record->id, '<i class="mdi mdi-note-multiple fa-2x "></i>', lang('index_student_note'), 'update');
            $row[] = bs3_link_crud("student_record/index_general_social_development/" . $student_record->id, '<i class=" mdi mdi-account-card-details fa-2x "></i>', lang('index_general_social_development'), 'update');
            if ($student_record->grade_type != 'youth_group' && $student_record->grade_type != 'book_club') {
                $row[] = bs3_many_links($links, $student_record->id);
            } else {
                $row[] = lang('no_report_card');
            }
//            $row[] = bs3_link_crud("student_absence/index_student_absence/" . $student_record->id, '<i class=" mdi mdi-account-multiple-minus fa-2x "></i>', lang('index_student_absence'), 'update');
//            $row[] = bs3_link_crud("student_payment/index_student_payment/" . $student_record->id, '<i class=" fa fa-money  fa-2x "></i>', lang('index_student_payment'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($student_record->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($student_record->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_records_model->count_all_students_records($room_id),
            "recordsFiltered" => $this->students_records_model->count_filtered_crud_students_records($room_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_record/add_student_record add_student_record
     * @apiName add_student_record
     * @apiGroup student_record
     * @apiDescription add student record data to student records database table
     * @apiParam {Number} student_key id of student 
     * @apiParam {Boolean} photo_permission_slips value of photo permission slips
     * @apiParam {Boolean} islamic_book_return value of islamic book return
     * @apiParam {String} status_islamic_book_return  status of islamic book return (select from dropdown menu)
     * @apiParam {Boolean} is_active value of active field to know status of student record 
     * @apiParam {Boolean} teacher_child value of teacher child field to know if student is a teacher child
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_student_record($room_id) {
        // get grade of room 
        $grade = $this->rooms_model->get_room($room_id);

        $input_array = array(
            'student_id' => "required",
            'is_active' => "required",
        );
        $this->_validation($input_array);

        $student_id = $this->input->post('student_key');
        $photo_permission_slips = $this->input->post('photo_permission_slips');
        $islamic_book_return = $this->input->post('islamic_book_return');
        $status_islamic_book_return = $this->input->post('status_islamic_book_return');
        $is_active = $this->input->post('is_active');
        $teacher_child = $this->input->post('teacher_child');
//        $registration = $this->input->post('registration');
//        $books = $this->input->post('books');
//        $islamic_textbook = $this->input->post('islamic_textbook');
//        $islamic_workbook = $this->input->post('islamic_workbook');

        if ($room_id == '000' || $room_id == '0') {
            error_message('400', 'error_in_grade_or_room');
        }

        $islamic_book_return = 1;
        if ($this->input->post('islamic_book_return')) {
            $islamic_book_return = 2;
        }

        $photo_permission_slips = 1;
        if ($this->input->post('photo_permission_slips')) {
            $photo_permission_slips = 2;
        }

        // if is_active == 1     => the student is move to another grade
        // if is is_active == 2  => the student is active
        // if is is_active == 3  => the student is manually deactivate
        $is_active = 1;
        if ($this->input->post('is_active')) {
            $is_active = 2;
        }

        $teacher_child = 1;
        if ($this->input->post('teacher_child')) {
            $teacher_child = 2;
        }

        $status_islamic_book_return = 'no_status';
        if ($this->input->post('status_islamic_book_return')) {
            $status_islamic_book_return = $this->input->post('status_islamic_book_return');
        }

        if ($this->form_validation->run()) {
            $student_id = $this->input->post('student_key');

            $student_order = $this->students_records_model->get_student_order($student_id);

            if ($student_id != 0) {
                $where = array(
                    'student_id' => $student_id,
                    'room_id' => $room_id,
                    'is_active' => $is_active,
                    'year' => $this->_current_year,
                );
                if (exist_item("students_records_model", $where)) {
                    send_message('', "202", 'student_already_exist_in_room');
                }

                // update status of student 
//                $where = array(
//                    'student_id' => $student_id,
//                    'is_active' => 3,
//                );
//                $update_data = array(
//                    'is_active' => 1,
//                );
//                $exist_item = $this->students_records_model->get_by($where);
//                if ($exist_item) {
//                    $update_item = $this->students_records_model->update_by(array('id' => $exist_item->id), $update_data);
//                }


                if ($grade->grade_type != 'youth_group' && $grade->grade_type != 'book_club') {
                    $where = array(
                        'student_id' => $student_id,
                        'room_id <>' => $room_id,
                        'is_active' => $is_active,
                        'year' => $this->_current_year,
                    );
                    if (exist_item("students_records_model", $where)) {
                        send_message('', "202", 'student_already_exist_in_another_room');
                    }
                }

                $where = array(
                    'student_id' => $student_id,
                    'is_active' => $is_active,
                    'year' => $this->_current_year,
                );
                $count_of_active_students_records = count($this->students_records_model->get_many_by($where));

                if ($count_of_active_students_records == '2') {
                    send_message('', "202", 'can_not_register_student_in_more_than_two_classes');
                }


//create student_record
                $order = $student_order + 1;
                $data = array(
                    'student_id' => $student_id,
                    'room_id' => $room_id,
                    'photo_permission_slips' => $photo_permission_slips,
                    'islamic_book_return' => $islamic_book_return,
                    'status_islamic_book_return' => $status_islamic_book_return,
                    'is_active' => $is_active,
                    'student_order' => $order,
                    'year' => $this->_current_year,
                    'teacher_child' => $teacher_child,
                );

                $insert = $this->students_records_model->insert($data);
                if (isset($insert) && $insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            } else {
                error_message('400', 'insert_student_record_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_record/get_student_record get_student_record
     * @apiName get_student_record
     * @apiGroup student_record
     * @apiDescription get id of student record to get its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student record from student records table
     * @apiError  FALSE return false if failure in get data
     */
    function get_student_record($id) {
        $data = $this->students_records_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student_record/update_student_record update_student_record
     * @apiName update_student_record
     * @apiGroup student_record
     * @apiDescription update student record data in student records database table
     * @apiParam {Number} id id of student record
     * @apiParam {Number} student_key id of student 
     * @apiParam {Boolean} photo_permission_slips value of photo permission slips
     * @apiParam {Boolean} islamic_book_return value of islamic book return
     * @apiParam {String} status_islamic_book_return  status of islamic book return (select from dropdown menu)
     * @apiParam {Boolean} is_active value of active field to know status of student record 
     * @apiParam {Boolean} teacher_child value of teacher child field to know if student is a teacher child
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_student_record($room_id) {
        $input_array = array(
            'student_id' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $student_id = $this->input->post('student_key');
            $photo_permission_slips = $this->input->post('photo_permission_slips');
            $islamic_book_return = $this->input->post('islamic_book_return');
            $status_islamic_book_return = $this->input->post('status_islamic_book_return');
            $is_active = $this->input->post('is_active');
            $teacher_child = $this->input->post('teacher_child');

            $is_active = 3;
            if ($this->input->post('is_active')) {
                $is_active = 2;
            }

            $non_active_registration_date = NULL;
            if ($is_active == 3) {
                date_default_timezone_set('America/Los_Angeles');
                $non_active_registration_date = date('Y-m-d');
            }

            $islamic_book_return = 1;
            if ($this->input->post('islamic_book_return')) {
                $islamic_book_return = 2;
            }
//
//            $photo_permission_slips = '1';
//            if ($this->input->post('photo_permission_slips')) {
//                $photo_permission_slips = '2';
//            }

            $teacher_child = 1;
            if ($this->input->post('teacher_child')) {
                $teacher_child = 2;
            }

            $where = array(
                'student_id' => $student_id,
                'room_id' => $room_id,
                'is_active' => $is_active,
                'id <>' => $item_id,
            );
            if (exist_item("students_records_model", $where)) {
                send_message('', "202", 'student_already_exist_in_room');
            }

            $data = array(
                'photo_permission_slips' => $photo_permission_slips,
                'islamic_book_return' => $islamic_book_return,
                'status_islamic_book_return' => $status_islamic_book_return,
                'is_active' => $is_active,
                'teacher_child' => $teacher_child,
                'non_active_registration_date' => $non_active_registration_date,
            );

            $update_item = $this->students_records_model->update_by(array('id' => $item_id), $data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student_record/delete_student_record delete_student_record
     * @apiName delete_student_record
     * @apiGroup student_record
     * @apiDescription get id of student record to delete its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiParam {Number} room_id id of room
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_student_record($room_id, $id = '') {
        $where = array('room_id' => $room_id);
        $student_records = $this->students_records_model->get_many_by($where);
        //start validation
        validation_edit_delete_redirect($student_records, "id", $id);
        // end validation

        $student_record = $this->students_records_model->get_student_record_by_student_record_id($id);

        //update all order of their brothers and sisters 
        if (isset($student_record) && $student_record)
            $brothers_and_sisters = $this->students_records_model->get_all_brothers_and_sisters($id, $student_record->parent_id, $student_record->student_order);

        if (isset($brothers_and_sisters) && $brothers_and_sisters) {
            foreach ($brothers_and_sisters as $value) {
                $data = array(
                    'student_order' => $value->student_order - 1,
                );
                $update_item = $this->students_records_model->update_by(array('id' => $value->id), $data);
            }
        }
        $delete_item = $this->students_records_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /student_record/search_student_auto search_student_auto
     * @apiName search_student_auto
     * @apiGroup student_record
     * @apiDescription get information about name of student for auto complete search
     * @apiParam {String} term Name of student
     * @apiSuccess  arr  Array contain data of student from students table
     */
    function search_student_auto() {
        $query = $this->input->get('term');
        if (isset($query) && $query) {
            $return_arr = new stdClass();
            $arr = array();
            try {
                $result = $this->students_model->get_students_for_auto_complete($query);
                foreach ($result as $value) {
                    $return_arr = new stdClass();
                    $return_arr->key = $value->student_id;
                    $return_arr->value = $value->student_name . ' ' . $value->family_name;
                    $arr[] = $return_arr;
                }
            } catch (PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
            /* Toss back results as json encoded array. */
            echo json_encode($arr);
            exit();
            die();
        }
    }

    /**
     * @api {get} /student_record/index_general_social_development index_general_social_development
     * @apiName index_general_social_development
     * @apiGroup student_record
     * @apiDescription home page of general social development for specific student record
     * @apiParam {Number} student_record_id id of student record
     */
    function index_general_social_development($student_record_id) {
        $student_general_social_development = $this->students_records_model->get_general_social_development($student_record_id);
        $data = array();

        $general_social_development = get_general_social_development();
        foreach ($general_social_development as $key => $value) {
            $data[$key] = $student_general_social_development->$key;
        }
        if (empty($data)) {
            foreach ($general_social_development as $key => $value) {
                $data[$key] = "";
            }
        }
        $this->data['data'] = $data;
        $this->data['student_record_id'] = $student_record_id;
        $this->data['mark_key_options'] = $mark_key_options = get_marks_key();
    }

    /**
     * @api {post} /student_record/add_general_social_development add_general_social_development
     * @apiName add_general_social_development
     * @apiGroup student_record
     * @apiDescription add student general social development data to student records database table
     * @apiParam {String} general_social_development_name Name of  general social development
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_general_social_development($student_record_id) {
        $general_social_development = get_general_social_development();
        foreach ($general_social_development as $value) {
            $input_array = array($value => "required",);
        }
        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $update_item = FALSE;

            $data = array();
            foreach ($general_social_development as $key => $value) {
                $post_general_social_development = $this->input->post($key);
                $data[$key] = $post_general_social_development;
            }

            $where = array(
                'id' => $student_record_id,
            );
            $update_item = $this->students_records_model->update_by($where, $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_record/register_student_in_grade register_student_in_grade
     * @apiName register_student_in_grade
     * @apiGroup student_record
     * @apiDescription home page of register student in grade
     */
    function register_student_in_grade() {
        $grades = $this->grades_model->get_grades();
        $rooms = $this->rooms_model->get_all();

        $this->data['grades_options'] = $grades;
        $this->data['rooms_options'] = $rooms;

        $photo_permission_slips_array = photo_permission_slips_array();
        $status_islamic_book_return_array = status_islamic_book_return_array();
        $status_islamic_book_return_array['0'] = lang('select_book_status');

        $this->data['status_islamic_book_return_array'] = $status_islamic_book_return_array;
        $this->data['photo_permission_slips_options'] = $photo_permission_slips_array;

        $this->data['show_object_link'] = 'student_record/show_register_student_in_grade';
        $this->data['get_object_link'] = 'student_record/get_register_student_in_grade';
        $this->data['add_object_link'] = 'student_record/add_register_student_in_grade';
        $this->data['update_object_link'] = 'student_record/update_register_student_in_grade';
        $this->data['delete_object_link'] = 'student_record/delete_register_student_in_grade';
        $this->data['modal_name'] = 'crud_register_student_in_grade';
        $this->data['add_object_title'] = lang("register_student_in_grade");
        $this->data['update_object_title'] = lang("update_student_in_grade");
        $this->data['delete_object_title'] = lang("delete_student_in_grade");
        $this->data['thead'] = array('student_name', 'is_active', 'grade_name', 'room_name',
            'index_student_allergy', 'index_student_note', 'index_general_social_development', 'index_report_card',);
//   'teacher_child', 'photo_permission_slips', 'islamic_book_return', 'status_islamic_book_return', 
        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['index_student_allergy'] = 'index_student_allergy';
        $this->data['non_printable']['index_student_note'] = 'index_student_note';
        $this->data['non_printable']['index_general_social_development'] = 'index_general_social_development';
        $this->data['non_printable']['index_report_card'] = 'index_report_card';

        $this->data['non_printable']['update'] = 'update';
        $this->data['non_printable']['delete'] = 'delete';
    }

    /**
     * @api {get} /student_record/show_register_student_in_grade show_register_student_in_grade
     * @apiName show_register_student_in_grade
     * @apiGroup student_record
     * @apiDescription get all student records
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student records table
     * @apiError  FALSE return false if failure in get data
     */
    function show_register_student_in_grade() {
        $student_records = $this->students_records_model->get_all_students_records();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }

        $links = array('report_card/index_report_card/1',
            'report_card/index_report_card/2',);
        foreach ($student_records as $student_record) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_student_profile/" . $student_record->student_id, $student_record->student_name . ' ' . $student_record->family_name, '', 'update');
            $row[] = tinyint_lang_one_and_two($student_record->is_active);
            $row[] = $student_record->grade_name;
            $row[] = ucwords(strtolower($student_record->room_name));
//            $row[] = tinyint_lang_one_and_two($student_record->teacher_child);
//            $row[] = tinyint_lang_one_and_two($student_record->photo_permission_slips);
//            
//            if ($student_record->stage_id == '2') {
//                $row[] = tinyint_lang_one_and_two($student_record->islamic_book_return);
//                $row[] = lang($student_record->status_islamic_book_return);
//            } else {
//                $row[] = '-';
//                $row[] = '-';
//            }
            //add html for action

            $row[] = bs3_link_crud("student_allergy/index_student_allergy/" . $student_record->id, '<i class="mdi mdi-hospital fa-2x "></i>', lang('index_student_allergy'), 'update');
            $row[] = bs3_link_crud("student_note/index_student_note/" . $student_record->id, '<i class="mdi mdi-note-multiple fa-2x "></i>', lang('index_student_note'), 'update');
            $row[] = bs3_link_crud("student_record/index_general_social_development/" . $student_record->id, '<i class=" mdi mdi-account-card-details fa-2x "></i>', lang('index_general_social_development'), 'update');
//            $row[] = bs3_link_crud("report_card/index_report_card/" . $student_record->id, '<i class=" mdi mdi-clipboard-text fa-2x "></i>', lang('index_report_card'), 'update');

            if ($student_record->grade_type != 'youth_group' && $student_record->grade_type != 'book_club') {
                $row[] = bs3_many_links($links, $student_record->id);
            } else {
                $row[] = lang('no_report_card');
            }

//            $row[] = bs3_link_crud("student_absence/index_student_absence/" . $student_record->id, '<i class=" mdi mdi-account-multiple-minus fa-2x "></i>', lang('index_student_absence'), 'update');
//            $row[] = bs3_link_crud("student_payment/index_student_payment/" . $student_record->id, '<i class=" fa fa-money  fa-2x "></i>', lang('index_student_payment'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($student_record->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($student_record->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_records_model->count_all_students_records_without_room(),
            "recordsFiltered" => $this->students_records_model->count_filtered_crud_students_records_without_room(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_record/add_register_student_in_grade add_register_student_in_grade
     * @apiName add_register_student_in_grade
     * @apiGroup student_record
     * @apiDescription add student record data to student records database table
     * @apiParam {Number} student_key id of student 
     * @apiParam {Boolean} photo_permission_slips value of photo permission slips
     * @apiParam {Boolean} islamic_book_return value of islamic book return
     * @apiParam {String} status_islamic_book_return  status of islamic book return (select from dropdown menu)
     * @apiParam {Boolean} is_active value of active field to know status of student record 
     * @apiParam {Boolean} teacher_child value of teacher child field to know if student is a teacher child
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_register_student_in_grade() {
        $input_array = array(
            'student_id' => "required",
            'is_active' => "required",
        );
        $this->_validation($input_array);

        $student_id = $this->input->post('student_key');
        $photo_permission_slips = $this->input->post('photo_permission_slips');
        $islamic_book_return = $this->input->post('islamic_book_return');
        $status_islamic_book_return = $this->input->post('status_islamic_book_return');
        $is_active = $this->input->post('is_active');
        $room_id = $this->input->post('room_id');
        $grade_id = $this->input->post('grade_id');
        $teacher_child = $this->input->post('teacher_child');
//        $registration = $this->input->post('registration');
//        $books = $this->input->post('books');
//        $islamic_textbook = $this->input->post('islamic_textbook');
//        $islamic_workbook = $this->input->post('islamic_workbook');

  if ($room_id == '000' || $room_id == '0' || $grade_id == '0' || $grade_id == '000') {
            error_message('400', 'error_in_grade_or_room');
        }
        $grade_info = $this->grades_model->get_by('id', $grade_id);
        $grade_type = $grade_info->grade_type;

      
        $islamic_book_return = 1;
        if ($this->input->post('islamic_book_return')) {
            $islamic_book_return = 2;
        }

//        $photo_permission_slips = 1;
//        if ($this->input->post('photo_permission_slips')) {
//            $photo_permission_slips = 2;
//        }
        // if is_active == 1     => the student is move to another grade
        // if is is_active == 2  => the student is active
        // if is is_active == 3  => the student is manually deactivate
        $is_active = 1;
        if ($this->input->post('is_active')) {
            $is_active = 2;
        }

        $teacher_child = 1;
        if ($this->input->post('teacher_child')) {
            $teacher_child = 2;
        }

        if ($this->input->post('status_islamic_book_return') == '0') {
            $status_islamic_book_return = 'no_status';
        }

        if ($this->form_validation->run()) {
            $student_id = $this->input->post('student_key');

            $student_order = $this->students_records_model->get_student_order($student_id);
//            var_dump($student_order);
            if ($student_id != 0) {
                $where = array(
                    'student_id' => $student_id,
                    'room_id' => $room_id,
                    'is_active' => $is_active,
                    'year' => $this->_current_year,
                );
                if (exist_item("students_records_model", $where)) {
                    send_message('', "202", 'student_already_exist_in_room');
                }

                // update status of student 
//                $where = array(
//                    'student_id' => $student_id,
//                    'is_active' => 3,
//                );
//                $update_data = array(
//                    'is_active' => 1,
//                );
//                $exist_item = $this->students_records_model->get_by($where);
//                if ($exist_item) {
//                    $update_item = $this->students_records_model->update_by(array('id' => $exist_item->id), $update_data);
//                }


                if ($grade_type != 'youth_group' && $grade_type != 'book_club') {
                    $where = array(
                        'student_id' => $student_id,
                        'room_id <>' => $room_id,
                        'is_active' => $is_active,
                        'year' => $this->_current_year,
                    );
                    if (exist_item("students_records_model", $where)) {
                        send_message('', "202", 'student_already_exist_in_another_room');
                    }
                }

                $where = array(
                    'student_id' => $student_id,
                    'is_active' => $is_active,
                    'year' => $this->_current_year,
                );
                $count_of_active_students_records = count($this->students_records_model->get_many_by($where));

                if ($count_of_active_students_records == '2') {
                    send_message('', "202", 'can_not_register_student_in_more_than_two_classes');
                }


//create student_record
                $order = $student_order + 1;
                $data = array(
                    'student_id' => $student_id,
                    'room_id' => $room_id,
                    'photo_permission_slips' => $photo_permission_slips,
                    'islamic_book_return' => $islamic_book_return,
                    'status_islamic_book_return' => $status_islamic_book_return,
                    'is_active' => $is_active,
                    'student_order' => $order,
                    'year' => $this->_current_year,
                    'teacher_child' => $teacher_child,
                );
                //                var_dump($data);
                //                die;
                $insert = $this->students_records_model->insert($data);
                if (isset($insert) && $insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            } else {
                error_message('400', 'insert_student_record_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_record/get_register_student_in_grade get_register_student_in_grade
     * @apiName get_register_student_in_grade
     * @apiGroup student_record
     * @apiDescription get id of student record to get its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student record from student records table
     * @apiError  FALSE return false if failure in get data
     */
    function get_register_student_in_grade($id) {
        $data = $this->students_records_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student_record/update_register_student_in_grade update_register_student_in_grade
     * @apiName update_register_student_in_grade
     * @apiGroup student_record
     * @apiDescription update student record data in student records database table
     * @apiParam {Number} id id of student record
     * @apiParam {Number} student_key id of student 
     * @apiParam {Boolean} photo_permission_slips value of photo permission slips
     * @apiParam {Boolean} islamic_book_return value of islamic book return
     * @apiParam {String} status_islamic_book_return  status of islamic book return (select from dropdown menu)
     * @apiParam {Boolean} is_active value of active field to know status of student record 
     * @apiParam {Boolean} teacher_child value of teacher child field to know if student is a teacher child
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_register_student_in_grade() {
        $input_array = array(
            'student_id' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $student_id = $this->input->post('student_key');
            $photo_permission_slips = $this->input->post('photo_permission_slips');
            $islamic_book_return = $this->input->post('islamic_book_return');
            $status_islamic_book_return = $this->input->post('status_islamic_book_return');
            $is_active = $this->input->post('is_active');
            $room_id = $this->input->post('room_id');
            $teacher_child = $this->input->post('teacher_child');

            $is_active = 3;
            if ($this->input->post('is_active')) {
                $is_active = 2;
            }

            $non_active_registration_date = NULL;
            if ($is_active == 3) {
                date_default_timezone_set('America/Los_Angeles');
                $non_active_registration_date = date('Y-m-d');
            }

            $islamic_book_return = 1;
            if ($this->input->post('islamic_book_return')) {
                $islamic_book_return = 2;
            }

//            $photo_permission_slips = 1;
//            if ($this->input->post('photo_permission_slips')) {
//                $photo_permission_slips = 2;
//            }


            $teacher_child = 1;
            if ($this->input->post('teacher_child')) {
                $teacher_child = 2;
            }

            $where = array(
                'student_id' => $student_id,
                'room_id' => $room_id,
                'is_active' => $is_active,
                'id <>' => $item_id,
            );
            if (exist_item("students_records_model", $where)) {
                send_message('', "202", 'student_already_exist_in_room');
            }

            $data = array(
                'photo_permission_slips' => $photo_permission_slips,
                'islamic_book_return' => $islamic_book_return,
                'status_islamic_book_return' => $status_islamic_book_return,
                'is_active' => $is_active,
                'teacher_child' => $teacher_child,
                'non_active_registration_date' => $non_active_registration_date,
            );

            $update_item = $this->students_records_model->update_by(array('id' => $item_id), $data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student_record/delete_register_student_in_grade delete_register_student_in_grade
     * @apiName delete_register_student_in_grade
     * @apiGroup student_record
     * @apiDescription get id of student record to delete its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_register_student_in_grade($id = '') {
        $where = array();
        $student_records = $this->students_records_model->get_many_by($where);
        //start validation
        validation_edit_delete_redirect($student_records, "id", $id);
        // end validation
        // check if related 
        $students_notes = $this->students_notes_model->get_by('student_record_id', $id);
        $students_allergies = $this->students_allergies_model->get_by('student_record_id', $id);
        $students_payments = $this->students_payments_model->get_by('student_record_id', $id);

        if ($students_notes || $students_allergies || $students_payments) {
            error_message('407', 'have_son_error');
        } else {
            $student_record = $this->students_records_model->get_student_record_by_student_record_id($id);

            $delete_item = $this->students_records_model->delete_by('id', $id);

            //update all order of their brothers and sisters 
            if (isset($student_record) && $student_record)
                $brothers_and_sisters = $this->students_records_model->get_all_brothers_and_sisters($id, $student_record->parent_id, $student_record->student_order);

            if (isset($brothers_and_sisters) && $brothers_and_sisters) {
                foreach ($brothers_and_sisters as $value) {
                    $data = array(
                        'student_order' => $value->student_order - 1,
                    );
                    $update_item = $this->students_records_model->update_by(array('id' => $value->id), $data);
                }
            }
            if (isset($delete_item) && $delete_item) {
                send_message("", '200', 'delete_success');
            } else {
                error_message('400', 'delete_error');
            }
        }
    }

}
