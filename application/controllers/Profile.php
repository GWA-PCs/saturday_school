<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_model');
        $this->load->model('grades_model');
        $this->load->model('students_model');
        $this->load->model('students_notes_model');
        $this->load->model('students_allergies_model');
        $this->load->model('parents_model');
        $this->load->model('students_payments_model');
        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');
    }

    /**
     * @api {get} /profile/page_profile page_profile
     * @apiName page_profile
     * @apiGroup profile
     * @apiDescription home page of profile
     */
    public function page_profile() {
        $user = $this->ion_auth->user()->row();
        if ($user->type == 'teacher') {
            redirect("profile/page_teacher_profile/" . $user->id);
        } elseif ($user->type == 'parent') {
            redirect("profile/page_parent_profile/" . $user->id);
        } elseif ($user->type == 'student') {
            $student = $this->students_model->get_by([
                "user_id" => $user->id,
            ]);

            redirect("profile/page_student_profile/" . $student->id);
        }
    }

    /**
     * @api {get} /profile/page_student_profile page_student_profile
     * @apiName page_student_profile
     * @apiGroup profile
     * @apiDescription home page of student profile
     * @apiParam {Number} student_id id of student
     */
    public function page_student_profile($student_id) {

        if (empty($student_id) || !is_numeric($student_id)) {
            redirect("home/home_page");
        }

        $this->data['student_id'] = $student_id;
        $this->data['grades'] = $this->grades_model->get_grades_array();
        $student = $this->students_model->get_student_by_id($student_id);
        $this->data['students'] = $student;
        $user = $this->ion_auth->user()->row();
        $this->data['user'] = $user;
    }

    /**
     * @api {get} /profile/profile_student_data profile_student_data
     * @apiName profile_student_data
     * @apiGroup profile
     * @apiDescription Get information of student to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  student_info Array contain data of students info
     */
    public function profile_student_data($student_id) {
        $student_info = $this->students_model->get_student_info($student_id);
        send_message($student_info);
    }

    /**
     * @api {get} /profile/get_student_attendances_info get_student_attendances_info
     * @apiName get_student_attendances_info
     * @apiGroup profile
     * @apiDescription Get information of student attendances to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  results Array contain data of students attendances info
     */
    public function get_student_attendances_info($student_id) {
        $results = $this->students_model->get_attendances($student_id);

        $res_array = array();
        foreach ($results as $item) {
            $sliced_array = array_slice($item, 0, 4);
            $res_array[] = $sliced_array;
        }
        send_message($res_array);
    }

    /**
     * @api {get} /profile/get_students_records_info get_students_records_info
     * @apiName get_students_records_info
     * @apiGroup profile
     * @apiDescription Get information of student records to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  results Array contain data of students records info
     */
    public function get_students_records_info($student_id) {
        $results = $this->students_model->get_student_records_for_profile($student_id);
        send_message($results);
    }

    /**
     * @api {get} /profile/get_notes get_notes
     * @apiName get_notes
     * @apiGroup profile
     * @apiDescription Get notes of student to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  results Array contain data of notes
     */
    public function get_notes($student_id) {
        $results = $this->students_notes_model->get_all_notes($student_id);

          foreach ($results as $value) {
            if ($value->note_date != '0000-00-00' && $value->note_date != '' && $value->note_date != NULL)
                $value->note_date = date("m-d-Y", strtotime($value->note_date));
        }

        send_message($results);
    }

    /**
     * @api {get} /profile/get_allergies get_allergies
     * @apiName get_allergies
     * @apiGroup profile
     * @apiDescription Get allergies of student to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  results Array contain data of allergies
     */
    public function get_allergies($student_id) {
        $results = $this->students_allergies_model->get_all_allergies($student_id);
        send_message($results);
    }

    /**
     * @api {get} /profile/get_children get_children
     * @apiName get_children
     * @apiGroup profile
     * @apiDescription Get children of parent to display it in profile
     * @apiParam {Number} parent_id id of parent
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data
     * @apiSuccess  results Array contain data of parent children
     */
    public function get_children($parent_id) {
        $results = $this->parents_model->get_parent_children($parent_id);
        send_message($results);
    }

    /**
     * @api {get} /profile/page_teacher_profile page_teacher_profile
     * @apiName page_teacher_profile
     * @apiGroup profile
     * @apiDescription Get user id  of teacher
     * @apiParam {Number} user_id id of user
     */
    public function page_teacher_profile($user_id) {
        if (empty($user_id) || !is_numeric($user_id)) {
            redirect("home/home_page");
        }
        $this->data['user_id'] = $user_id;
    }

    /**
     * @api {get} /profile/profile_teacher_data profile_teacher_data
     * @apiName profile_teacher_data
     * @apiGroup profile
     * @apiDescription Get data of teacher to display it in profile
     * @apiParam {Number} user_id id of user
     * @apiSuccess  results Array contain data of teacher
     */
    public function profile_teacher_data($user_id) {
        $results = $this->teachers_model->get_teacher_by_user_id($user_id);
        send_message($results);
    }

    /**
     * @api {get} /profile/page_parent_profile page_parent_profile
     * @apiName page_parent_profile
     * @apiGroup profile
     * @apiDescription Get id of parent
     * @apiParam {Number} user_id id of user
     */
    public function page_parent_profile($user_id) {
        if (empty($user_id) || !is_numeric($user_id)) {
            redirect("home/home_page");
        }
        $this->data['user_id'] = $user_id;

        $parent_info = $this->parents_model->get_by('user_id', $user_id);
        $parent_id = $parent_info->id;
        $this->data['parent_id'] = $parent_id;
    }

    /**
     * @api {get} /profile/profile_parent_data profile_parent_data
     * @apiName profile_parent_data
     * @apiGroup profile
     * @apiDescription Get data of parent to display it in profile
     * @apiParam {Number} user_id id of user
     * @apiSuccess  results Array contain data of parent
     */
    public function profile_parent_data($user_id) {
        $results = $this->parents_model->get_parent_by_user_id($user_id);
        send_message($results);
    }

    /**
     * @api {get} /profile/get_student_study_sequences get_student_study_sequences
     * @apiName get_student_study_sequences
     * @apiGroup profile
     * @apiDescription Get student study sequences to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  results Array contain data of student
     */
    public function get_student_study_sequences($student_id) {
        $results = $this->students_model->get_student_records_with_limit($student_id);

        foreach ($results as $value) {
            if ($value->non_active_registration_date != '0000-00-00' && $value->non_active_registration_date != '' && $value->non_active_registration_date != NULL)
                $value->non_active_registration_date = date("m-d-Y", strtotime($value->non_active_registration_date));
        }

        send_message($results);
    }

    /**
     * @api {get} /profile/page_teacher_profile_for_another_account page_teacher_profile_for_another_account
     * @apiName page_teacher_profile_for_another_account
     * @apiGroup profile
     * @apiDescription Get teacher id
     * @apiParam {Number} teacher_id id of teacher
     */
    public function page_teacher_profile_for_another_account($teacher_id) {
        if (empty($teacher_id) || !is_numeric($teacher_id)) {
            redirect("home/home_page");
        }
        $this->data['teacher_id'] = $teacher_id;
    }

    /**
     * @api {get} /profile/profile_teacher_data_for_another_account profile_teacher_data_for_another_account
     * @apiName profile_teacher_data_for_another_account
     * @apiGroup profile
     * @apiDescription Get data of teacher to display it in profile
     * @apiParam {Number} teacher_id id of teacher
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data
     * @apiSuccess  results Array contain data of teacher
     */
    public function profile_teacher_data_for_another_account($teacher_id) {
        $results = $this->teachers_model->get_teacher_by_id($teacher_id);
        send_message($results);
    }

    /**
     * @api {get} /profile/get_teacher_rooms get_teacher_rooms
     * @apiName get_teacher_rooms
     * @apiGroup profile
     * @apiDescription Get rooms of teacher to display it in profile
     * @apiParam {Number} teacher_id id of teacher
     * @apiSuccess  teacher_rooms Array contain data of teacher rooms
     */
    public function get_teacher_rooms($teacher_id) {
        $teacher_rooms = $this->rooms_teachers_model->get_teacher_room($teacher_id);
        send_message($teacher_rooms);
    }

    /**
     * @api {get} /profile/get_personal_image get_personal_image
     * @apiName get_personal_image
     * @apiGroup profile
     * @apiDescription Get personal image of student to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  image String contain data of image
     */
    public function get_personal_image($student_id = "") {
        $image = "";
        $student_info = $this->students_model->get_by('id', $student_id);

        $image .= '<img src="';
        if (isset($student_info->personal_image) && $student_info->personal_image) {
            $image .= base_url("assets/uploads/personal_image/") . $student_info->personal_image;
        } else {
//            if ($student_info->gender == 'female')
            //                $image .= base_url("assets/uploads/images/student_1.png");
            //            else
            $image .= base_url("assets/uploads/images/student_2.png");
        }
        $image .= '" class="img-circle" width="200"';
        $image .= '  />';

        send_message($image);
    }

    /**
     * @api {get} /profile/get_report_card get_report_card
     * @apiName get_report_card
     * @apiGroup profile
     * @apiDescription Get student info to know report card for each grade of student record to display it in profile
     * @apiParam {Number} student_id id of student
     * @apiSuccess  student_info Array contain data of student info
     */
    public function get_report_card($student_id) {
        $student_info = $this->students_records_model->get_all_active_student_record($student_id);
        send_message($student_info);
    }

    /**
     * @api {get} /profile/get_parent_balance get_parent_balance
     * @apiName get_parent_balance
     * @apiGroup profile
     * @apiDescription Get balance of parents to display it in profile
     * @apiParam {Number} parent_id id of student
     * @apiSuccess  cost_data Array contain data of balance
     */
    public function get_parent_balance($parent_id) {
        $parent_children = $this->parents_model->get_parent_children($parent_id);

        $invoice = 0;
        $payment = 0;
        $balance = 0;

        if (isset($parent_children) && $parent_children) {
            $parent_children_array = array();
            foreach ($parent_children as $value) {
                $parent_children_array[$value->student_id] = $value->student_id;
            }
            $student_payments = $this->students_payments_model->get_children_payments_for_parent($parent_children_array);

            if (isset($student_payments) && $student_payments) {
                foreach ($student_payments as $value) {
                    $invoice += $value->invoice;
                    $payment += $value->payment;
                }
            }
        }
        $balance = $payment - $invoice;

        $cost_data = array(
            'balance' => round($balance, 2),
        );
        send_message($cost_data);
    }

    /**
     * @api {get} /profile/get_number_of_payments get_number_of_payments
     * @apiName get_number_of_payments
     * @apiGroup profile
     * @apiDescription Get number of payments to display it in profile
     * @apiParam {Number} parent_id id of student
     * @apiSuccess  cost_data Array contain data of number of payments
     */
    public function get_number_of_payments($parent_id) {
        $parent_children = $this->parents_model->get_parent_children($parent_id);

        $number_of_payments = 0;

        if (isset($parent_children) && $parent_children) {
            $parent_children_array = array();
            foreach ($parent_children as $value) {
                $parent_children_array[$value->student_id] = $value->student_id;
            }
            $student_payments = $this->students_payments_model->get_children_payments_for_parent($parent_children_array);

            if (isset($student_payments) && $student_payments) {
                foreach ($student_payments as $value) {
                    $number_of_payments += $value->payment;
                }
            }
        }

        $cost_data = array(
            'number_of_payments' => '$' . $number_of_payments,
        );
        send_message($cost_data);
    }

    public function get_parent_image($user_id = "") {
        $image = "";
        $user_info = $this->users_m->get_by('id', $user_id);
        if ($user_info->type == 'teacher') {
            $image .= '<img src="';
            $image .= base_url("assets/uploads/images/teacher.png");
            $image .= '" class="img-circle" width="200"';
            $image .= '  />';
        } elseif ($user_info->type == 'parent') {
            $image .= '<img src="';
            $image .= base_url("assets/uploads/images/parents.png");
            $image .= '" class="img-circle" width="200"';
            $image .= '  />';
        }

        send_message($image);
    }

    public function get_teacher_image($teacher_id = "") {
        $image = "";
//        $user_info = $this->teachers_m->get_by('id', $teacher_id);

        $image .= '<img src="';
        $image .= base_url("assets/uploads/images/teacher.png");
        $image .= '" class="img-circle" width="200"';
        $image .= '  />';

        send_message($image);
    }

    public function update_profile() {
        $user = $this->ion_auth->user()->row();

        if ($user->type == "parent") {
            redirect("profile/update_parent_profile/" . $user->id);
            exit;
        } elseif ($user->type == "teacher") {
            redirect("profile/update_teacher_profile/" . $user->id);
            exit;
        }
    }

    public function update_teacher_profile($teacher_id) {
        if ($teacher_id != $_SESSION['user_id']) {
            redirect("home/home_page_teacher");
            exit;
        }
        $user_info = $this->users_m->get_by(["id" => $teacher_id]);
        $user_info->birthdate = date("m-d-Y", strtotime($user_info->birthdate));
        $this->data['teacher'] = $user_info;
    }

    public function update_parent_profile($parent_id) {
        if ($parent_id != $_SESSION['user_id']) {
            redirect("home/home_page_parent");
            exit;
        }

        $parents = $this->parents_model->get_by(["user_id" => $parent_id]);
        $parents = $this->parents_model->get_parent_by_id($parents->id);

        $this->data['page_title'] = lang("update_profile");

        $this->data['parents'] = $parents;
    }

    public function edit_parent_profile($parent_id) {
        $input_array = array(
            'family_name' => "required",
            'father_name' => "required",
            'mother_name' => "required",
            'home_phone' => "telephone",
            'mother_phone' => "telephone",
            'father_phone' => "telephone",
            'address' => "",
        );

        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $parent = $this->parents_model->get_by(["id" => $parent_id]);

            $parent_data = [
                "home_phone" => $this->input->post("home_phone"),
                "father_phone" => $this->input->post("father_phone"),
                "mother_phone" => $this->input->post("mother_phone"),
                "family_name" => $this->input->post("family_name"),
            ];

            $this->parents_model->update_by(["id" => $parent_id], $parent_data);

            $user_data = [
                "father_name" => $this->input->post("father_name"),
                "mother_name" => $this->input->post("mother_name"),
                "address" => $this->input->post("address"),
            ];

            $update_item = $this->users_m->update_by(["id" => $parent->user_id], $user_data);
            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    public function edit_teacher_profile($teacher_id) {
        $input_array = array(
            'first_name' => "required",
            'last_name' => "required",
            'birthdate' => "required",
            'mobile' => "telephone",
            'address' => "",
        );

        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $birthdate = $this->input->post('birthdate');
            if ($birthdate)
                $birthdate = date_format(date_create_from_format('m-d-Y', $birthdate), 'Y-m-d');
            $mobile = $this->input->post('mobile');
            $address = $this->input->post('address');

            $user_data = [
                "first_name" => $first_name,
                "last_name" => $last_name,
                "address" => $address,
                "birthdate" => $birthdate,
                "mobile" => $mobile,
            ];

            $update_item = $this->users_m->update_by(["id" => $teacher_id], $user_data);
            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

}
