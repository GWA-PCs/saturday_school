<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Grade extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('grades_model');
        $this->load->model('stages_model');
    }

    function index() {
        
    }

    /**
     * @api {get} /grade/index_grade index_grade
     * @apiName index_grade
     * @apiGroup grade
     * @apiDescription home page of grades
     */
    function index_grade() {
        $grades = $this->grades_model->get_all();
        $next_grade_name_options = array();
        $next_grade_name_options['0'] = lang('no_grade');
        $stages = $this->stages_model->get_all();

        $stages_options = array();
        $stages_options['0'] = lang('select_stage_name');
        foreach ($stages as $value) {
            $stages_options[$value->id] = $value->stage_name;
        }

        $report_card_name_options = array();

        $report_card_name = get_report_card_name();

        foreach ($report_card_name as $key => $value) {
            $report_card_name_options[$key] = $value;
        }


        $this->data['stages_options'] = $stages_options;
        $this->data['next_grade_name_options'] = $next_grade_name_options;
        $this->data['report_card_name_options'] = $report_card_name_options;

        $this->data['show_object_link'] = 'grade/show_grades';
        $this->data['get_object_link'] = 'grade/get_grade';
        $this->data['add_object_link_grade'] = 'grade/add_grade';
        $this->data['update_object_link_grade'] = 'grade/update_grade';
        $this->data['delete_object_link'] = 'grade/delete_grade';
        $this->data['modal_name'] = 'crud_grades';
        $this->data['add_object_title'] = lang("add_new_grade");
        $this->data['update_object_title'] = lang("update_grade");
        $this->data['delete_object_title'] = lang("delete_grade");
        $this->data['thead'] = array('grade_name', 'next_grade_name', 'stage_name', 'index_room', 'index_subject');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }


        // put the title whose don't apear in print and excel
        $this->data['non_printable']['index_room'] = 'index_room';
        $this->data['non_printable']['index_subject'] = 'index_subject';
        $this->data['non_printable']['update'] = 'update';
        $this->data['non_printable']['delete'] = 'delete';
    }

    /**
     * @api {get} /grade/show_grades show_grades
     * @apiName show_grades
     * @apiGroup grade
     * @apiDescription get all grades
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in grades table
     * @apiError  FALSE return false if failure in get data
     */
    function show_grades() {
        $grades = $this->grades_model->get_grades();

        $next_grade_name_options = array();
        $next_grade_name_options['0'] = lang('no_grade');

        foreach ($grades as $value) {
            $next_grade_name_options[$value->id] = $value->grade_name;
        }
//        $this->data['next_grade_name_options'] = $next_grade_name_options;

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($grades as $grade) {
            $no++;
            $row = array();
            $row[] = $grade->grade_name;
            if ($grade->next_grade_name == 0 || $grade->next_grade_name == '' || $grade->next_grade_name == NULL) {
                $row[] = lang('no_grade');
            } else {
                $grade_name = $this->grades_model->get_by('id', $grade->next_grade_name);
                $row[] = $grade_name->grade_name;
            }
            $row[] = $grade->stage_name;

            //add html for action

            $row[] = bs3_link_crud("room/index_room/" . $grade->id, '<i class=" mdi mdi-arrange-send-backward fa-2x "></i>', lang('index_room'), 'update');
            $row[] = bs3_link_crud("subject/index_subject/" . $grade->id, '<i class=" mdi  mdi-book-open-page-variant fa-2x "></i>', lang('index_subject'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($grade->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
              $row[] = bs3_update_delete_crud($grade->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->grades_model->count_all_grades(),
            "recordsFiltered" => $this->grades_model->count_filtered_crud_grades(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /grade/add_grade add_grade
     * @apiName add_grade
     * @apiGroup grade
     * @apiDescription add grade data to grade database table
     * @apiParam {Number} stage_id id of stage (select from dropdown list)
     * @apiParam {String} grade_name Name of grade
     * @apiParam {Number} next_grade_name Id of next grade
     * @apiParam {String} report_card_name Name of report card 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  Grade_name_already_exist202 return grade name already exist error message

     */
    function add_grade() {
        $input_array = array(
            'grade_name' => "required",
            'next_grade_name' => "required",
            'stage_id' => "required",
            'report_card_name' => "required",
            'grade_type' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $stage_id = $this->input->post('stage_id');
            $grade_name = $this->input->post('grade_name');
            $next_grade_name = $this->input->post('next_grade_name');
            $report_card_name = $this->input->post('report_card_name');
            $grade_type = $this->input->post('grade_type');

            if (exist_item("grades_model", array('grade_name' => $grade_name, 'stage_id' => $stage_id))) {
                send_message("", '202', 'grade_name_already_exist');
            }

//
//            $where = array(
//                'grade_name' => $grade_name,
//                'next_grade_name' => $next_grade_name,
//                'stage_name' => $stage_name,
//            );
//            $get_next_grade_name = $this->grades_model->get_by($where);
//            if (isset($get_next_grade_name) && $get_next_grade_name) {
//                if ($grade_name == $get_next_grade_name->next_grade_name) {
//                    error_message('400', 'insert_error');
//                } else {
            $data = array(
                'stage_id' => $stage_id,
                'grade_name' => $grade_name,
                'next_grade_name' => $next_grade_name,
                'report_card_name' => $report_card_name,
                'grade_type' => $grade_type,
            );
            $insert = $this->grades_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
//                }
//            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /grade/get_grade get_grade
     * @apiName get_grade
     * @apiGroup grade
     * @apiDescription get id of grade to get its info from grades database table
     * @apiParam {Number} id id of grade
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of grade from grades table
     * @apiError  FALSE return false if failure in get data
     */
    function get_grade($id) {
        $data = $this->grades_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /grade/update_grade update_grade
     * @apiName update_grade
     * @apiGroup grade
     * @apiDescription update grade data in grades database table
     * @apiParam {Number} id id of grade
     * @apiParam {Number} stage_id id of stage (select from dropdown list)
     * @apiParam {String} grade_name Name of grade
     * @apiParam {Number} next_grade_name Id of next grade
     * @apiParam {String} report_card_name Name of report card 
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_grade() {
        $grades = $this->grades_model->get_all();

        $input_array = array(
            'grade_name' => "required",
            'next_grade_name' => "required",
            'stage_id' => "required",
            'report_card_name' => "required",
            'grade_type' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $grade_name = $this->input->post('grade_name');
            $next_grade_name = $this->input->post('next_grade_name');
            $stage_id = $this->input->post('stage_id');
            $report_card_name = $this->input->post('report_card_name');
            $grade_type = $this->input->post('grade_type');

            // start validation
            if (exist_item("grades_model", array('grade_name' => $grade_name, 'stage_id' => $stage_id, 'id <>' => $item_id))) {
                send_message('', "202", 'grade_name_already_exist');
            }

            validation_edit_delete_redirect($grades, "id", $item_id);
            // end validation

            $data = array(
                'grade_name' => $grade_name,
                'next_grade_name' => $next_grade_name,
                'stage_id' => $stage_id,
                'report_card_name' => $report_card_name,
                'grade_type' => $grade_type,
            );

            $update_item = $this->grades_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /grade/delete_grade delete_grade
     * @apiName delete_grade
     * @apiGroup grade
     * @apiDescription get id of grade to delete its info from grades database table
     * @apiParam {Number} id id of grade
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_grade($id) {
        $grades = $this->grades_model->get_all();
        //start validation
        validation_edit_delete_redirect($grades, "id", $id);
        // end validation
//        $related = $this->grades_model->get_grade($id);

        $related = $this->grades_model->get_any_student_grade($id);

        if (isset($related) && $related) {
            error_message('407', 'related_with_another_elem');
        } else {
            $delete_item = $this->grades_model->delete_by('id', $id);
            if (isset($delete_item) && $delete_item) {
                send_message("", '200', 'delete_success');
            } else {
                error_message('400', 'delete_error');
            }
        }
    }

    /**
     * @api {post} /grade/get_next_grade_info get_next_grade_info
     * @apiName delete_grade
     * @apiGroup grade
     * @apiDescription get information for all grades from grades database table
     * @apiSuccess  next_grade_name_options return array contains all grades names to display it in dropdown menu
     */
    function get_next_grade_info() {
        $grades = $this->grades_model->get_all();
        $next_grade_name_options = array();
        $next_grade_name_options['0'] = lang('no_grade');

        foreach ($grades as $value) {
            $next_grade_name_options[$value->id] = $value->grade_name;
        }
        send_message($next_grade_name_options);
    }

}
