<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_note extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_notes_model');
    }

    /**
     * @api {get} /student_note/index_student_note index_student_note
     * @apiName index_student
     * @apiGroup index_student_note
     * @apiDescription home page of student note
     * @apiParam {Number} student_record_id id of student record
     */
    function index_student_note($student_record_id) {
        $months = get_all_monthes();
        $this->data['months'] = $months;

        $this->data['show_object_link'] = 'student_note/show_student_notes/' . $student_record_id;
        $this->data['get_object_link'] = 'student_note/get_student_note';
        $this->data['add_object_link'] = 'student_note/add_student_note/' . $student_record_id;
        $this->data['update_object_link'] = 'student_note/update_student_note/' . $student_record_id;
        $this->data['delete_object_link'] = 'student_note/delete_student_note/' . $student_record_id;
        $this->data['modal_name'] = 'crud_student_notes';
        $this->data['add_object_title'] = lang("add_new_student_note");
        $this->data['update_object_title'] = lang("update_student_note");
        $this->data['delete_object_title'] = lang("delete_student_note");
        $this->data['thead'] = array('note_month', 'note_date', 'note_text',);

        if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type != "parent") {
            if ($this->_current_year == $this->_archive_year) {
                 $this->data['add_student_note'] = 1;
                $this->data['thead'][] = 'update';
                $this->data['thead'][] = 'delete';
            }

            $this->data['non_printable']['delete'] = 'delete';
            $this->data['non_printable']['update'] = 'update';
        } else {
            $this->data['non_printable']['hidden'] = 'hidden';
        }
    }

    /**
     * @api {get} /student_note/show_student_notes show_student_notes
     * @apiName show_student_notes
     * @apiGroup student_note
     * @apiDescription get all student notes
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student notes table
     * @apiError  FALSE return false if failure in get data
     */
    function show_student_notes($student_record_id) {
        $student_notes = $this->students_notes_model->get_student_notes($student_record_id);
        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($student_notes as $student_note) {
            $no++;
            $row = array();
            $row[] = lang($student_note->note_month);
            $row[] = date("m-d-Y", strtotime($student_note->note_date));
            $row[] = $student_note->note_text;

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($student_note->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($student_note->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_notes_model->count_all_students_notes($student_record_id),
            "recordsFiltered" => $this->students_notes_model->count_filtered_crud_students_notes($student_record_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_note/add_student_note add_student_note
     * @apiName add_student_note
     * @apiGroup student_note
     * @apiDescription add student note data to student notes database table
     * @apiParam {String} note_month Name of month
     * @apiParam {Date} note_date date of note
     * @apiParam {String} note_text content of note
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_student_note($student_record_id) {
        $input_array = array(
            'note_month' => "required",
            'note_date' => "required",
            'note_text' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $note_month = $this->input->post('note_month');
            $note_date = $this->input->post('note_date');
            if ($note_date)
                $note_date = date_format(date_create_from_format('m-d-Y', $note_date), 'Y-m-d');
            $note_text = $this->input->post('note_text');

            $data = array(
                'note_month' => $note_month,
                'note_date' => $note_date,
                'note_text' => $note_text,
                'student_record_id' => $student_record_id,
            );
            $insert = $this->students_notes_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_note/get_student_note get_student_note
     * @apiName get_student_note
     * @apiGroup student_note
     * @apiDescription get id of student note to get its info from student notes database table
     * @apiParam {Number} id id of student note
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student from students table
     * @apiError  FALSE return false if failure in get data
     */
    function get_student_note($id) {
        $data = $this->students_notes_model->get_by('id', $id);
        $data->note_date = date("m-d-Y", strtotime($data->note_date));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student_note/update_student_note update_student_note
     * @apiName update_student_note
     * @apiGroup student_note
     * @apiDescription update student note data in student notes database table
     * @apiParam {String} note_month Name of month
     * @apiParam {Date} note_date date of note
     * @apiParam {String} note_text content of note
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_student_note($student_record_id) {
        $student_notes = $this->students_notes_model->get_student_notes($student_record_id);

        $input_array = array(
//            'note_month' => "required",
            'note_date' => "required",
            'note_text' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $note_month = $this->input->post('note_month');
            $note_date = $this->input->post('note_date');
            if ($note_date)
                $note_date = date_format(date_create_from_format('m-d-Y', $note_date), 'Y-m-d');
            $note_text = $this->input->post('note_text');

            validation_edit_delete_redirect($student_notes, "id", $item_id);
            // end validation

            $data = array(
//                'note_month' => $note_month,
                'note_date' => $note_date,
                'note_text' => $note_text,
            );
            $update_item = $this->students_notes_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student_note/delete_student_note delete_student_note
     * @apiName delete_student_note
     * @apiGroup student_note
     * @apiDescription get id of student note to delete its info from student notes database table
     * @apiParam {Number} id id of student note
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_student_note($student_record_id, $id) {
        $student_notes = $this->students_notes_model->get_student_notes($student_record_id);
        //start validation
        validation_edit_delete_redirect($student_notes, "id", $id);
        // end validation
        $delete_item = $this->students_notes_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
