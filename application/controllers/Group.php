<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->authentication_login();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model('groups_model');
        $this->load->model('previlages_model');
        $this->load->model('general_model', 'groups_previlages_model');
        $this->groups_previlages_model->set_table('groups_previlages');
    }


    public function index_group()
    {
        $this->data['show_object_link'] = 'group/show_groups';
        $this->data['get_object_link'] = 'group/get_group';
        $this->data['add_object_link'] = 'group/add_group';
        $this->data['update_object_link'] = 'group/update_group';
        $this->data['delete_object_link'] = 'group/delete_group';
        $this->data['modal_name'] = 'crud_group';
        $this->data['add_object_title'] = lang("add_group");
        $this->data['update_object_title'] = lang("edit_group");
        $this->data['delete_object_title'] = lang("delete_group");
        $this->data['thead'] = array('group_name', 'description', 'previlages', 'update', 'delete');
    }

    public function show_groups()
    {
        $groups = $this->groups_model->get_groups();

        $data = array();
        $no = $_POST['start'];
        foreach ($groups as $item) {
            $no++;
            $row = array();
            $row[] = $item->group_name;
            $row[] = $item->description;
            //add html for action
            $row[] = bs3_link_crud('group/index_group_previlages/' . $item->id, '<i class="mdi mdi-folder-lock-open fa-2x"></i>');
            $row[] = bs3_update_delete_crud($item->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            $row[] = bs3_update_delete_crud($item->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->groups_model->count_all(),
            "recordsFiltered" => $this->groups_model->count_filtered_crud(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;

    }

    public function get_group($id)
    {
        $data = $this->groups_model->get_group_by_id($id);
        echo json_encode($data);
        die;
    }

    public function add_group()
    {
        $input_array = array(
            'group_name' => 'required',
            'description' => 'required',
        );
        $this->_validation($input_array);

        if ($this->form_validation->run() === TRUE) {
            $group_name = $this->input->post('group_name');

            //validation
            if (exist_item("groups_model", array('group_name' => $group_name))) {
                send_message('group_name', "202", 'already_exist');
            }
            //validation

            $description = $this->input->post('description');
            $new_group_id = $this->ion_auth->create_group($group_name, $description);
            if ($new_group_id) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    public function update_group()
    {
        $input_array = array(
            'group_name' => 'required',
            'description' => 'required',
        );
        $this->_validation($input_array);

        if ($this->form_validation->run() === TRUE) {
            $group_name = $this->input->post('group_name');
            $description = $this->input->post('description');
            $item_id = $this->input->post('id');

            //validation
            if (exist_item("groups_model", array('group_name' => $group_name, 'id <>' => $item_id))) {
                send_message('group_name', "202", 'already_exist');
            }
            //validation

            $updated_group_id = $this->ion_auth->update_group($item_id, $group_name, $description);
            if ($updated_group_id) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    public function delete_group($id)
    {
        $deleted = $this->ion_auth->delete_group($id);
        if ($deleted) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    public function index_group_previlages($group_id)
    {
        $previlages = $this->previlages_model->get_all();
        $previlage_array = array();
        foreach ($previlages as $item) {
            $previlage_array[$item->id]['name'] = $item->function_name;
            $previlage_array[$item->id]['label'] = $item->{"previlage_name_{$this->_lang}"};
            $previlage_array[$item->id]['id'] = $item->id;

        }
        $this->data['previlages'] = $previlage_array;

        $where = array(
            'group_id' => $group_id,
        );
        $exist_group_permissions = array();
        $query = $this->groups_previlages_model->get_many_by($where);
        if($query) {
            foreach ($query as $row) {
                $exist_group_permissions[] = $row->previlage_id;
            }
        }

        foreach ($previlage_array as $perm) {
            if(!empty($exist_group_permissions)) {
                $this->data[$perm['name'] . "_old"] = false;
                if (in_array($perm['id'], $exist_group_permissions)) {
                    $this->data[$perm['name'] . "_old"] = true;
                }
            }else {
                $this->data[$perm['name'] . "_old"] = false;
            }

        }
        $this->data['group_id'] = $group_id;
    }

    public function update_group_previlages($group_id)
    {
        $where = array(
            'group_id' => $group_id,
        );
        $exist_group_permissions = array();
        $query = $this->groups_previlages_model->get_many_by($where);
        foreach ($query as $row) {
            $exist_group_permissions[] = $row->previlage_id;
        }

        // permissions id in post array
        $new_checked_permissions = array();

        $previlages = $this->previlages_model->get_all();
        $perm_array = array();
        foreach ($previlages as $perm) {
            $perm_array[$perm->id] = $perm->function_name;
        }
        foreach ($perm_array as $key => $perm) {
            if ($this->input->post($perm)) {
                $new_checked_permissions[] = $this->input->post($perm);
            }
        }

        $permissions_for_delete = array();
        foreach ($exist_group_permissions as $perm) {
            if (!in_array($perm, $new_checked_permissions)) {
                $permissions_for_delete[] = $perm;
            }
        }
        $permissions_for_add = array();
        foreach ($new_checked_permissions as $perm) {
            if (!in_array($perm, $exist_group_permissions)) {
                $permissions_for_add[] = $perm;
            }
        }

        $message_data = array();
        if(empty($permissions_for_delete)) {
            $message_data['delete'] = "no item to delete";
        }
        if(empty($permissions_for_add)) {
            $message_data['insert'] = "no item to insert";
        }

        foreach ($permissions_for_delete as $perm) {
            $delete = $this->groups_previlages_model->delete_by('previlage_id', $perm);
            if($delete) {
                $message_data['delete'] = "deleted done";
            }
        }
        foreach ($permissions_for_add as $perm) {
            $data = array(
                'group_id' => $group_id,
                'previlage_id' => $perm,
            );
            $insert = $this->groups_previlages_model->insert($data);
            if($insert) {
                $message_data['insert'] = "insert done";
            }
        }

        send_message($message_data, '200' , 'update_success');
    }

}