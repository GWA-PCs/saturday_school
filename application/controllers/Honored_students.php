<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Honored_students extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('honored_students_model');
        $this->load->model('honored_orders_model');
        $this->load->model('teachers_model');
    }

    function index() {
        
    }

    /**
     * @api {get} /honored_students/index_honored_students index_honored_students
     * @apiName index_honored_students
     * @apiGroup honored_students
     * @apiDescription home page of honored students
     * @apiParam {Number} id id of honored order
     */
    function index_honored_students($honored_order_id) {
        // get id of logged in user 
        $user_info = $this->_user_login;
        // get teacher id 
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);
        // get honored orders information for specific honored order id 
        $honored_orders = $this->honored_orders_model->get_by('id', $honored_order_id);

        $teacher_rooms = array();

        if (isset($honored_orders) && $honored_orders) {
            // get teacher tooms with same stage of honored_orders
            $teacher_rooms = $this->teachers_model->get_teacher_room($teacher_id, $honored_orders->stage_id);
        }

        // initiliaze grade array whose in teacher rooms
        $teacher_grades_options = array('000' => lang('select_grade_name'));
        foreach ($teacher_rooms as $value) {
            if ($value->stage_id == $honored_orders->stage_id)
                $teacher_grades_options[$value->grade_id] = $value->grade_name;
        }
        $this->data['teacher_rooms'] = $teacher_rooms;
        $this->data['teacher_grades_options'] = $teacher_grades_options;

        $students_teacher = array();
        if (isset($honored_orders) && $honored_orders) {
            $students_teacher = $this->teachers_model->get_students_teacher($teacher_id, $honored_orders->stage_id);
        }
        $this->data['students_teacher'] = $students_teacher;

        $this->data['show_object_link'] = 'honored_students/show_honored_students/' . $honored_order_id;
        $this->data['get_object_link'] = 'honored_students/get_honored_students';
        $this->data['add_object_link'] = 'honored_students/add_honored_students/' . $honored_order_id;
        $this->data['update_object_link'] = 'honored_students/update_honored_students/' . $honored_order_id;
        $this->data['delete_object_link'] = 'honored_students/delete_honored_students/' . $honored_order_id;
        $this->data['modal_name'] = 'crud_honored_students';
        $this->data['add_object_title'] = lang("add_new_honored_students");
        $this->data['update_object_title'] = lang("update_honored_students");
        $this->data['delete_object_title'] = lang("delete_honored_students");
        $this->data['honored_order_id'] = $honored_order_id;
//        $this->data['thead'] = array('grade_id', 'room_id', 'student_record_id', 'honored_date', 'honored_reason', 'update', 'delete');
        $this->data['thead'] = array('student_record_id', 'grade_id', 'room_id', 'delete');

        $this->data['non_printable']['delete'] = 'delete';
    }

    /**
     * @api {get} /honored_students/show_honored_students show_honored_students
     * @apiName show_honored_students
     * @apiGroup honored_students
     * @apiDescription get all honored students in honored order
     * @apiParam {Number} id id of honored order
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in honored students table
     * @apiError  FALSE return false if failure in get data
     */
    function show_honored_students($honored_order_id) {
        $honored_students = $this->honored_students_model->get_honored_students($honored_order_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($honored_students as $honored_request) {
            $no++;
            $row = array();

//            $row[] = $honored_request->student_name . ' ' . $honored_request->family_name;
            $row[] = bs3_link_crud("profile/page_student_profile/" . $honored_request->student_id, $honored_request->student_name . ' ' . $honored_request->family_name, '', 'update');
            $row[] = $honored_request->grade_name;
            $row[] = $honored_request->room_name;
//            $row[] = $honored_request->honored_date;
//            $row[] = $honored_request->honored_reason;
            //add html for action
//            $row[] = bs3_update_delete_crud($honored_request->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            $row[] = bs3_update_delete_crud($honored_request->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->honored_students_model->count_all_honored_students($honored_order_id),
            "recordsFiltered" => $this->honored_students_model->count_filtered_crud_honored_students($honored_order_id),
            "data" => $data,
        );

        //output to json format

        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /honored_students/add_honored_students add_honored_students
     * @apiName add_honored_students
     * @apiGroup honored_students
     * @apiDescription add honored students to honored students database table in specific honored order
     * @apiParam {Number} id id of honored order
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data


     */
    function add_honored_students($honored_order_id) {
        $user_info = $this->_user_login;
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);

        $input_array = array(
            'student_record_id' => "required",
//            'honored_date' => "required",
//            'honored_reason' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $student_record_id = $this->input->post('student_record_id');
            $honored_date = $this->input->post('honored_date');
            $honored_reason = $this->input->post('honored_reason');

            $data = array(
                'honored_order_id' => $honored_order_id,
                'student_record_id' => $student_record_id,
//                'honored_date' => $honored_date,
//                'honored_reason' => $honored_reason,
            );
            $insert = $this->honored_students_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /honored_students/get_honored_students get_honored_students
     * @apiName get_honored_students
     * @apiGroup honored_students
     * @apiDescription get id of honored student to get its info from honored students database table
     * @apiParam {Number} id id of honored student
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of honored student from honored students table
     * @apiError  FALSE return false if failure in get data
     */
    function get_honored_students($id) {
        $data = $this->honored_students_model->get_honored_request($id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /honored_students/update_honored_students update_honored_students
     * @apiName update_honored_students
     * @apiGroup honored_students
     * @apiDescription update honored student data in honored students database table
     * @apiParam {Number} id id of honored student
     * @apiParam {Date} honored_date Date of honored order
     * @apiParam {String} honored_reason Reason of honored order
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_honored_students($honored_order_id) {
        $honored_students = $this->honored_students_model->get_honored_students($honored_order_id);

        json_encode($honored_students);
        $input_array = array(
            'honored_date' => "required",
            'honored_reason' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $honored_date = $this->input->post('honored_date');
            if ($honored_date)
                $honored_date = date_format(date_create_from_format('m-d-Y', $honored_date), 'Y-m-d');
            $honored_reason = $this->input->post('honored_reason');
            // start validation

            validation_edit_delete_redirect($honored_students, "id", $item_id);
            // end validation

            $data = array(
                'honored_date' => $honored_date,
                'honored_reason' => $honored_reason,
            );

            $update_item = $this->honored_students_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /honored_students/delete_honored_students delete_honored_students
     * @apiName delete_honored_students
     * @apiGroup honored_students
     * @apiDescription get id of honored student to delete its info from honored students database table
     * @apiParam {Number} id id of honored student
     * @apiParam {Number} id id of honored order
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_honored_students($honored_order_id, $id) {
        $honored_students = $this->honored_students_model->get_honored_students($honored_order_id);
        //start validation
        validation_edit_delete_redirect($honored_students, "id", $id);
        // end validation

        $delete_item = $this->honored_students_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /honored_students/get_honored_students_for_filter get_honored_students_for_filter
     * @apiName get_honored_students_for_filter
     * @apiGroup honored_students
     * @apiDescription get id of honored student to get its info from honored students database table
     * @apiParam {Number} id id of honored order
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of honored student from honored students table
     * @apiError  FALSE return false if failure in get data
     */
    function get_honored_students_for_filter($honored_order_id) {
        $honored_students = $this->honored_students_model->get_honored_students($honored_order_id);

        $users_arr = array();
        foreach ($honored_students as $honored_request) {
            $row = array();
            $row['grade_id'] = $honored_request->grade_id;
            $row['room_id'] = $honored_request->room_id;
            $row['room_name'] = $honored_request->room_name;
            $row['student_name'] = $honored_request->student_name . ' ' . $honored_request->family_name;

            $users_arr[] = array("grade_id" => $row['grade_id'], "room_id" => $row['room_id'], "room_name" => $row['room_name'], "student_name" => $row['student_name'],);
        }
        //output to json format

        echo json_encode($users_arr);
        die;
    }

    /**
     * @api {get} /honored_students/index_honored_students_for_admin index_honored_students_for_admin
     * @apiName index_honored_students_for_admin
     * @apiGroup honored_students
     * @apiDescription home page of honored students in admin account 
     * @apiParam {Number} honored_order_id id of honored order
     * @apiParam {Number} teacher_id id of teacher
     */
    function index_honored_students_for_admin($teacher_id, $honored_order_id) {
//        $honored_orders = $this->honored_orders_model->get_by('id', $honored_order_id);
//        $teacher_rooms = $this->teachers_model->get_teacher_room($teacher_id, $honored_orders->stage_id);
//        $teacher_grades_options = array('000' => lang('select_grade_name'));
//        foreach ($teacher_rooms as $value) {
//            if ($value->stage_name == $honored_orders->stage_name)
//                $teacher_grades_options[$value->grade_id] = $value->grade_name;
//        }
//        $this->data['teacher_rooms'] = $teacher_rooms;
//        $this->data['teacher_grades_options'] = $teacher_grades_options;
//
//        $students_teacher = $this->teachers_model->get_students_teacher($teacher_id, $honored_orders->stage_name);
//        $this->data['students_teacher'] = $students_teacher;

        $this->data['show_object_link'] = 'honored_students/show_honored_students/' . $honored_order_id;
        $this->data['get_object_link'] = 'honored_students/get_honored_students';
        $this->data['add_object_link'] = 'honored_students/add_honored_students/' . $honored_order_id;
        $this->data['update_object_link'] = 'honored_students/update_honored_students/' . $honored_order_id;
        $this->data['delete_object_link'] = 'honored_students/delete_honored_students/' . $honored_order_id;
        $this->data['modal_name'] = 'crud_honored_students';
        $this->data['add_object_title'] = lang("add_new_honored_students");
        $this->data['update_object_title'] = lang("update_honored_students");
        $this->data['delete_object_title'] = lang("delete_honored_students");
        $this->data['honored_order_id'] = $honored_order_id;
//        $this->data['thead'] = array('grade_id', 'room_id', 'student_record_id', 'honored_date', 'honored_reason');
        $this->data['thead'] = array('grade_id', 'room_id', 'student_record_id');
    }

    /**
     * @api {get} /honored_students/show_honored_students_for_admin show_honored_students_for_admin
     * @apiName show_honored_students_for_admin
     * @apiGroup honored_students
     * @apiDescription get all honored students in honored order for admin account 
     * @apiParam {Number} honored_order_id id of honored order
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in honored students table
     * @apiError  FALSE return false if failure in get data
     */
    function show_honored_students_for_admin($honored_order_id) {
        $honored_students = $this->honored_students_model->get_honored_students($honored_order_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($honored_students as $honored_request) {
            $no++;
            $row = array();
            $row[] = $honored_request->grade_name;
            $row[] = $honored_request->room_name;
            $row[] = $honored_request->student_name . ' ' . $honored_request->family_name;
//            $row[] = $honored_request->honored_date;
//            $row[] = $honored_request->honored_reason;
            //add html for action
//            $row[] = bs3_update_delete_crud($honored_request->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
//            $row[] = bs3_update_delete_crud($honored_request->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->honored_students_model->count_all(),
            "recordsFiltered" => $this->honored_students_model->count_filtered_crud(),
            "data" => $data,
        );

        //output to json format

        echo json_encode($output);
        die;
    }

}
