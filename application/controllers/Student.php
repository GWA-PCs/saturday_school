<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Student extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_model');
        $this->load->model('rooms_teachers_model');
        $this->load->model('students_records_model');
        $this->load->model('teachers_model');
        $this->load->model('ion_auth_model');
        $this->load->model('parents_model');
        $this->load->model('homeworks_model');
        $this->load->model('subjects_model');

        $this->load->model('general_model', 'users_model');
        $this->users_model->set_table('users');

        $this->student = $this->students_model->get_by([
            "user_id" => $_SESSION['user_id']
        ]);

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
    }

    public function index() {
        
    }

    /**
     * @api {get} /student/index_student index_student
     * @apiName index_student
     * @apiGroup student
     * @apiDescription home page of students
     */
    public function index_student() {
        $parent_id = $this->parents_model->get_parents();
        $family_options = array();
        foreach ($parent_id as $value) {
            $family_options[$value->id] = $value->family_name . ': ' . $value->father_name . ' _ ' . $value->mother_name;
        }
        $gender_options = array(
            'male' => lang('male'),
            'female' => lang('female'),
        );
        $this->data['family_name'] = $family_options;
        $this->data['gender'] = $gender_options;
        $this->data['non_encrept_pass'] = random_password();

        $this->data['show_object_link'] = 'student/show_students';
        $this->data['get_object_link'] = 'student/get_student';
        $this->data['get_object_link_personal_image'] = 'student/get_student';
        $this->data['add_object_link'] = 'student/add_student';
        $this->data['update_object_link'] = 'student/update_student';
        $this->data['delete_object_link'] = 'student/delete_student';
        $this->data['modal_name'] = 'crud_students';
        $this->data['add_object_title'] = lang("add_student");
        $this->data['update_object_title'] = lang("update_student");
        $this->data['delete_object_title'] = lang("delete_student");
        $this->data['thead'] = array('student_name', 'parent_id', 'username', 'student_notes', 'age', 'gender');
        //'personal_image',

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /student/show_students show_students
     * @apiName show_students
     * @apiGroup student
     * @apiDescription get all students
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in students table
     * @apiError  FALSE return false if failure in get data
     */
    public function show_students() {
        $students = $this->students_model->get_students();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($students as $student) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_student_profile/" . $student->id, $student->student_name, '', 'update');
            $row[] = bs3_link_crud("profile/page_parent_profile/" . $student->user_id, $student->family_name, '', 'update');
            //            $row[] = $student->family_name;
            //            $row[] = bs3_file_download("assets/uploads/personal_image/" . $student->personal_image, $student->personal_image);

            $row[] = $student->email;
            $row[] = $student->student_notes;
            $row[] = $student->age;
            $row[] = lang($student->gender);
            //$row[] = $student->grade;
            //add html for action

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($student->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($student->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_model->count_all_students(),
            "recordsFiltered" => $this->students_model->count_filtered_crud_students(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student/add_student add_student
     * @apiName add_student
     * @apiGroup student
     * @apiDescription add student data to students database table
     * @apiParam {String} student_name Name of student
     * @apiParam {Number} parent_id id of parent (select from dropdown list)
     * @apiParam {String} student_notes Notes about student
     * @apiParam {Number} age Age of student
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    public function add_student() {
        $input_array = array(
            'student_name' => "required",
            'parent_id' => "required",
            'student_notes' => "required",
            'gender' => "required",
            //            'grade' => "required",
            'age' => "required|numeric",
        );
        $this->_validation($input_array);

        $student_name = $this->input->post('student_name');
        $parent_id = $this->input->post('parent_id');
        $student_notes = $this->input->post('student_notes');
        $password = $this->input->post("non_encrept_pass");
        //        $grade = $this->input->post('grade');
        $age = $this->input->post('age');
        $gender = $this->input->post('gender');
        //        $personal_image = file_or_image('personal_image', 'personal_image');

        if ($this->form_validation->run()) {
            $student_data = array(
                'student_name' => $student_name,
                'parent_id' => $parent_id,
            );
            $exist = $this->students_model->get_by($student_data);
            if ($exist) {
                error_message('400', 'this_student_is_already_exist');
            }
            $user_id = $this->ion_auth_model->register(time(), $password, time(), "student", ["non_encrept_pass" => $password]);


            //            if ($personal_image == '0') {
            //                error_message('400', 'insert_error_file_size_should_be_longer_than_zero');
            //            }
            //create student
            $data = array(
                'student_name' => $student_name,
                'parent_id' => $parent_id,
                'student_notes' => $student_notes,
                //                'personal_image' => $personal_image,
                //                'grade' => $grade,
                'age' => $age,
                'gender' => $gender,
                'user_id' => $user_id,
            );
            $insert = $this->students_model->insert($data);
            if (isset($insert) && $insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student/get_student get_student
     * @apiName get_student
     * @apiGroup student
     * @apiDescription get id of student to get its info from students database table
     * @apiParam {Number} id id of student
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student from students table
     * @apiError  FALSE return false if failure in get data
     */
    public function get_student($id) {
        $data = $this->students_model->get_student_by_id($id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student/update_student update_student
     * @apiName update_student
     * @apiGroup student
     * @apiDescription update student data in students database table
     * @apiParam {String} student_name Name of student
     * @apiParam {Number} parent_id id of parent (select from dropdown list)
     * @apiParam {String} student_notes Notes about student
     * @apiParam {Number} age Age of student
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    public function update_student() {
        $input_array = array(
            'student_name' => "required",
            'parent_id' => "required",
            'student_notes' => "required",
//            'grade' => "required",
            'age' => "required|numeric",
            'gender' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $student_name = $this->input->post('student_name');
            $parent_id = $this->input->post('parent_id');
            $student_notes = $this->input->post('student_notes');
//            $grade = $this->input->post('grade');
            $age = $this->input->post('age');
            $gender = $this->input->post('gender');

            $old_file_name = $this->input->post('fileName');
//            $personal_image = $old_file_name;
            //            if (isset($_FILES)) {
            //                if (!empty($_FILES['personal_image']['name']) && $_FILES['personal_image']['name'] != "") {
            //                    $personal_image = file_or_image('personal_image', 'personal_image');
            //                }
            //            }
            $student_data = array(
                'student_name' => $student_name,
                'parent_id' => $parent_id,
                'student_notes' => $student_notes,
//                'personal_image' => $personal_image,
                //                'grade' => $grade,
                'age' => $age,
                'gender' => $gender,
            );
            $update_item = $this->students_model->update_by(array('id' => $item_id), $student_data);


            $student = $this->students_model->get_by(["id" => $item_id]);

            $password = $this->input->post("non_encrept_pass");
            $user = $this->users_model->get_by(['id' => $student->user_id]);
            if ($user) {
                $change = $this->ion_auth->change_password($user->email, $user->non_encrept_pass, $password);
                $this->users_model->update_by(["id" => $student->user_id], ["non_encrept_pass" => $password]);
            }



            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student/delete_student delete_student
     * @apiName delete_student
     * @apiGroup student
     * @apiDescription get id of student to delete its info from students database table
     * @apiParam {Number} id id of student
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    public function delete_student($id = '') {
        $where = array();
        $students = $this->students_model->get_many_by($where);
//start validation
        validation_edit_delete_redirect($students, "id", $id);
// end validation
        $related = $this->students_records_model->get_by('student_id', $id);

        if (isset($related) && $related) {
            error_message('407', 'have_son_error');
        } else {
            $delete_item = $this->students_model->delete_by(array('id' => $id));
            if (isset($delete_item) && $delete_item) {
                send_message("", '200', 'delete_success');
            } else {
                error_message('400', 'delete_error');
            }
        }
    }

    /**
     * @api {get} /student/student_study_sequences student_study_sequences
     * @apiName student_study_sequences
     * @apiGroup student
     * @apiDescription get student information and his/her study sequences
     * @apiParam {Number} student_id id of student
     */
    public function student_study_sequences($student_id) {
        $where = array(
            'students.id' => $student_id,
        );
        $this->db->join("parents", "students.parent_id = parents.id");
        $student_info = $this->students_model->get_by($where);
        $this->data['student_info'] = $student_info;


        $student_study_sequence = $this->students_model->get_student_records($student_id);

        foreach ($student_study_sequence as $value) {
            if ($value->non_active_registration_date != '0000-00-00' && $value->non_active_registration_date != '' && $value->non_active_registration_date != NULL)
                $value->non_active_registration_date = date("m-d-Y", strtotime($value->non_active_registration_date));
        }
        $this->data['student_study_sequence'] = $student_study_sequence;
    }

    /**
     * @api {get} /student/index_students index_students
     * @apiName index_students
     * @apiGroup student
     * @apiDescription home page of students when search by letter
     * @apiParam {String} let Name of family name
     */
    public function index_students($let) {
        $students = $this->students_model->get_students_by_letter($let);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($students as $student) {
            $no++;
            $row = array();
//            $row[] = bs3_link_crud("profile/page_student_profile/" . $student->id, $student->student_name, '', 'update');
            //            $row[] = bs3_link_crud("profile/page_parent_profile/" . $student->user_id, $student->family_name, '', 'update');
            $row[] = $student->family_name;
            $row[] = $student->student_notes;
            $row[] = $student->age;
            $row[] = $student->grade;
            //add html for action
            //            if ($this->_current_year == $this->_archive_year) {
            //                $row[] = bs3_update_delete_crud($student->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            //                $row[] = bs3_update_delete_crud($student->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            //            }
            $data[] = $row;
        }
        $output = array(
//            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            //            "recordsTotal" => count($students),
            //            "recordsFiltered" => count($students),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /student/search_auto_complete search_auto_complete
     * @apiName search_auto_complete
     * @apiGroup student
     * @apiDescription get informations about name of teachers, parents, students and grade for bar search
     * @apiParam {String} term Name of teacher, parent, student or grade
     * @apiSuccess  result_arr  Array contain data of teacher, parent, student or grade
     */
    public function search_auto_complete() {
        $query = $this->input->get('term');

        $user_info = $this->_user_login;

        if (isset($query) && $query) {
            $teachers_arr = array('****************** Teachers ******************');
            $parents_arr = array('****************** Parents ********************');
            $students_arr = array('****************** Students ******************');
            $grades_arr = array('****************** Grades ********************');

            $result_arr = array();

            try {
                if ($user_info->type == 'admin') {
                    // ********************** search for teacher *******************

                    $this->db->select('teachers.id as id,first_name,last_name');
                    $this->db->like('LOWER(first_name)', strtolower($query));
                    $this->db->or_like('LOWER(last_name)', strtolower($query));
                    $this->db->or_like('first_name', $query);
                    $this->db->or_like('last_name', $query);
                    $this->db->join('teachers', 'teachers.user_id=users.id');
                    $this->db->limit(5);
                    $teachers = $this->users_m->get_all();

                    foreach ($teachers as $value) {
                        $return_teachers = new stdClass();
                        $return_teachers->key = $value->id;
                        $return_teachers->value = 'profile/page_teacher_profile_for_another_account/' . $value->id;
                        $return_teachers->label = $value->first_name . ' ' . $value->last_name;

                        $teachers_arr[] = $return_teachers;
                    }

                    // *********************** search for parents ******************
                    $this->db->select('users.id as id,father_name,family_name');
                    $this->db->like('LOWER(father_name)', strtolower($query));
                    $this->db->or_like('LOWER(family_name)', strtolower($query));
                    $this->db->or_like('father_name', $query);
                    $this->db->or_like('family_name', $query);
                    $this->db->join('parents', 'parents.user_id=users.id');
                    $this->db->limit(5);
                    $parents = $this->users_m->get_all();

                    foreach ($parents as $value) {
                        $return_parents = new stdClass();
                        $return_parents->key = $value->id;
                        $return_parents->value = 'profile/page_parent_profile/' . $value->id;
                        $return_parents->label = $value->father_name . ' ' . $value->family_name;
                        $parents_arr[] = $return_parents;
                    }
                    // *********************** search for students *****************

                    $this->db->select('students.id as id,student_name,family_name');
                    $this->db->like('LOWER(student_name)', strtolower($query));
                    $this->db->or_like('LOWER(family_name)', strtolower($query));
                    $this->db->or_like('student_name', $query);
                    $this->db->or_like('family_name', $query);
                    $this->db->join('parents', 'parents.id=students.parent_id');
                    $this->db->limit(5);
                    $students = $this->students_m->get_all();

                    foreach ($students as $value) {
                        $return_students = new stdClass();
                        $return_students->key = $value->id;
                        $return_students->value = 'profile/page_student_profile/' . $value->id;
                        $return_students->label = $value->student_name . ' ' . $value->family_name;
                        $students_arr[] = $return_students;
                    }

                    // *********************** search for grade ********************
                    $this->db->or_like('LOWER(grade_name)', strtolower($query));
                    $this->db->or_like('grade_name', $query);
                    $this->db->limit(5);
                    $grades = $this->grades_m->get_all();

                    foreach ($grades as $value) {
                        $return_grades = new stdClass();
                        $return_grades->key = $value->id;
                        $return_grades->value = 'room/index_room/' . $value->id;
                        $return_grades->label = $value->grade_name;
                        $grades_arr[] = $return_grades;
                    }

                    if (sizeof($teachers_arr) == 1) {
                        $teachers_arr[] = 'None found';
                    }
                    if (sizeof($parents_arr) == 1) {
                        $parents_arr[] = 'None found';
                    }
                    if (sizeof($students_arr) == 1) {
                        $students_arr[] = 'None found';
                    }
                    if (sizeof($grades_arr) == 1) {
                        $grades_arr[] = 'None found';
                    }
                    $result_arr = array_merge($teachers_arr, $parents_arr, $students_arr, $grades_arr);
                } elseif ($user_info->type == 'teacher') {
                    $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);

                    // *********************** search for students *****************
                    $students = $this->teachers_model->get_students_teacher_for_auto_complete($teacher_id, $query);

                    foreach ($students as $value) {
                        $return_students = new stdClass();
                        $return_students->key = $value->id;
                        $return_students->value = 'profile/page_student_profile/' . $value->id;
                        $return_students->label = $value->student_name . ' ' . $value->family_name;
                        $students_arr[] = $return_students;
                    }

//                    $grades = $this->teachers_model->get_teacher_room_for_auto_complete($teacher_id, $query);
                    //
                    //                    foreach ($grades as $value) {
                    //                        $return_grades = new stdClass();
                    //                        $return_grades->key = $value->id;
                    //                        $return_grades->value = 'room/index_room/' . $value->id;
                    //                        $return_grades->label = $value->grade_name;
                    //                        $grades_arr[] = $return_grades;
                    //                    }

                    if (sizeof($students_arr) == 1) {
                        $students_arr[] = 'None found';
                    }
//                    if (sizeof($grades_arr) == 1) {
                    //                        $grades_arr[] = 'None found';
                    //                    }
                    //                    $result_arr = array_merge($students_arr, $grades_arr);

                    $result_arr = $students_arr;
                }
            } catch (PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
            /* Toss back results as json encoded array. */
            echo json_encode($result_arr);
            exit();
            die();
        }
    }

    /**
     * @api {get} /student/search_about search_about
     * @apiName search_about
     * @apiGroup student
     * @apiDescription redirect to appropriate page when click on one of result appear in search bar
     * @apiParam {String} inputSearch_value Name of teacher, parent, student or grade
     */
    public function search_about() {
        $inputSearch_value = $this->input->get('inputSearch_value');

          if($inputSearch_value === '****************** Teachers ******************' ||
                            $inputSearch_value === '****************** Parents ********************' ||
                           $inputSearch_value=== '****************** Students ******************' ||
                           $inputSearch_value=== 'None found' ||
                           $inputSearch_value === '****************** Grades ********************'
                        ){
         redirect('home/home_page');
        }
        redirect($inputSearch_value);
    }

    public function homeworks() {
        $this->data['show_object_link'] = 'student/show_homeworks';
        $this->data['thead'] = array('title', 'content', 'deadline', 'subject', 'teacher', 'homeworks_attachment');


        if ($this->_current_year == $this->_archive_year) {
//            $this->data['thead'][] = 'update';
//            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['homeworks_attachment'] = 'homeworks_attachment';
//        $this->data['non_printable']['delete'] = 'delete';
//        $this->data['non_printable']['update'] = 'update';
    }

    public function show_homeworks() {
        $homeworks = $this->homeworks_model->get_homeworks_for_student($this->student->id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }

        foreach ($homeworks as $homework) {
            $no++;
            $row = array();
            $row[] = $homework->title;
            $row[] = $homework->text;
            $row[] = $homework->deadline;
            $row[] = $homework->subject_name;
            $row[] = $homework->first_name . " " . $homework->last_name;
            $row[] = bs3_link_crud("assets/uploads/homeworks_attachment/" . $homework->attach, $homework->attach);

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->homeworks_model->count_all_homeworks_from_student($this->student->id),
            "recordsFiltered" => $this->homeworks_model->count_filtered_crud_homeworks_from_student($this->student->id),
            "data" => $data,
        );



        //output to json format
        echo json_encode($output);
        die;
    }

    public function get_user($user_id) {
        $user = $this->users_model->get_by([
            "id" => $user_id
        ]);
        send_message($user);
    }

    public function create_account_for_student() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 6000000);

        $where = array('user_id' => NULL);
        $students = $this->students_model->get_many_by($where);

        foreach ($students as $value) {
            $password = random_password();
            $user_id = $this->ion_auth_model->register(time(), $password, time(), "student", ["non_encrept_pass" => $password]);

            $where = array(
                'id' => $value->id,
            );
            $data = array('user_id' => $user_id);
            $this->students_model->update_by($where, $data);

            sleep(1);
        }
        $students = $this->students_model->get_all();
    }

}
