<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_card extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();

        $this->load->model('report_cards_model');
        $this->load->model('students_records_model');
        $this->load->model('subjects_model');
        $this->load->model('students_marks_model');
        $this->load->model('rooms_attendances_model');
        $this->load->model('certificates_dates_model');
    }

    /**
     * @api {get} /report_card/index_report_card index_report_card
     * @apiName index_report_card
     * @apiGroup report_card
     * @apiDescription home page of report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     */
    function index_report_card($semester, $student_record_id) {
        $student_info = $this->students_records_model->get_report_name($student_record_id);

        if ($student_info->report_card_name == 'p1_report_card') {
            redirect('report_card/p1_report_card/' . $semester . '/' . $student_record_id);
        } elseif ($student_info->report_card_name == 'kg_pre2_report_card') {
            redirect('report_card/kg_pre2_report_card/' . $semester . '/' . $student_record_id);
        } elseif ($student_info->report_card_name == 'elem_report_card') {
            redirect('report_card/elem_report_card/' . $semester . '/' . $student_record_id);
        } else {
            redirect('report_card/no_report_card');
        }
    }

    /**
     * @api {get} /report_card/elem_report_card elem_report_card
     * @apiName elem_report_card
     * @apiGroup report_card
     * @apiDescription home page of elementary report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     */
    function elem_report_card($semester, $student_record_id) {
        $count = 0;
        $semester_name = 'first';
        if ($semester == '1')
            $semester_name = 'first';
        else
            $semester_name = 'second';
        // get student info for this student 
        $student_info = $this->students_records_model->get_student_record_info($student_record_id);

        $this->data['student_info'] = $student_info;

        $subjects = array();
        $children_subjects_of_arabic = array();
        if (isset($student_info) && $student_info) {
            $subjects = $this->subjects_model->get_subjects_except_arabic_and_its_children($student_info->grade_id);
            $children_subjects_of_arabic = $this->subjects_model->get_children_subjects_of_arabic($student_info->grade_id);
            $count_children_subjects_of_arabic = $this->subjects_model->count_get_children_subjects_of_arabic($student_info->grade_id);
            $count = $count_children_subjects_of_arabic->count;
        }

        $only_arabic = FALSE;
        $arabic = array('Arabic', 'arabic');
        $counter = 0;

        if (isset($count_children_subjects_of_arabic) && $count_children_subjects_of_arabic) {
            if (in_array($count_children_subjects_of_arabic->subject_name, $arabic)) {
                $counter++;
            }
        }

        if ($counter == 1)
            $only_arabic = TRUE;

        $subjects_array = array();
        if (isset($subjects) && $subjects) {
            foreach ($subjects as $value) {
                $subjects_array[$value->id] = $value->subject_name;
            }
        }
        $this->data['subjects_array'] = $subjects_array;
        $this->data['subjects'] = $subjects;
        $this->data['children_subjects_of_arabic'] = $children_subjects_of_arabic;
        $this->data['count_children'] = $count;
        $this->data['only_arabic'] = $only_arabic;

        // get marks of this student for specific semester 
        $student_marks = array();
        $absent_days = 0;
        if (isset($student_info) && $student_info) {
            $room_id = $student_info->room_id;
            $student_marks = $this->students_marks_model->get_all_student_marks($semester_name, $room_id, $student_record_id);


            $student_marks_array = array();
            if (isset($student_marks) && $student_marks) {
                foreach ($student_marks as $value) {
                    $student_marks_array[$value->subject_id] = $value;
                }
            }

            $this->data['student_marks'] = $student_marks_array;

            // number of absent days 
            $absent_days = $this->rooms_attendances_model->get_number_of_absent_dayes($student_record_id, $semester_name);
        }
        $this->data['absent_days'] = $absent_days;
    }

    /**
     * @api {post} /report_card/edit_elem_report_card edit_elem_report_card
     * @apiName edit_elem_report_card
     * @apiGroup report_card
     * @apiDescription edit information of elementary report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     * @apiParam {String} quran_teacher comment of quran teacher
     * @apiParam {String} arabic_teacher comment of arabic teacher
     * @apiParam {String} islamic_studies_teacher comment of islamic studies teacher
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function edit_elem_report_card($semester, $student_record_id) {
        $quran_teacher = $this->input->post('quran_teacher');
        $arabic_teacher = $this->input->post('arabic_teacher');
        $islamic_studies_teacher = $this->input->post('islamic_studies_teacher');
//        $date_of_expired = $this->input->post('date_of_expired');
//        $date_of_expired = date_format(date_create_from_format('m-d-Y', $date_of_expired), 'Y-m-d');

        $where = array(
            'student_record_id' => $student_record_id,
            'semester' => $semester,
        );
        $exist = $this->report_cards_model->get_by($where);

        if ($exist) {
            $data = array(
                'quran_teacher' => $quran_teacher,
                'arabic_teacher' => $arabic_teacher,
                'islamic_studies_teacher' => $islamic_studies_teacher,
//                'date_of_expired' => $date_of_expired,
            );
            $update_item = $this->report_cards_model->update_by($where, $data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            $data = array(
                'student_record_id' => $student_record_id,
                'semester' => $semester,
                'quran_teacher' => $quran_teacher,
                'arabic_teacher' => $arabic_teacher,
                'islamic_studies_teacher' => $islamic_studies_teacher,
//                'date_of_expired' => $date_of_expired,
            );
            $insert_item = $this->report_cards_model->insert($data);

            if (isset($insert_item) && $insert_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        }
    }

    /**
     * @api {get} /report_card/get_elem_report_card get_elem_report_card
     * @apiName get_elem_report_card
     * @apiGroup report_card
     * @apiDescription Get information of student report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in report card 
     */
    function get_elem_report_card($semester, $student_record_id) {
        $where = array(
            'student_record_id' => $student_record_id,
            'semester' => $semester,
        );
        $exist = $this->report_cards_model->get_by($where);

        $student_info = $this->students_records_model->get_student_record_info($student_record_id);

        // get date of expired for certificate 
        $date_of_expired = "";
        $where = array(
            'grade_id' => $student_info->grade_id,
            'year' => $this->_archive_year,
        );
        $certificates_date = $this->certificates_dates_model->get_by($where);
        if (isset($certificates_date) && $certificates_date) {
            $date_of_expired = date("m-d-Y", strtotime( $certificates_date->date_of_expired));
        }

        $arabic_teachers = '';
        $quran_teachers = '';
        $islamic_studies_teachers = '';

        if (!$exist) {
            // get name of arabic teacher 
            $arabic_teachers_info = $this->rooms_teachers_model->get_teacher_of_arabic_subject($student_info->room_id);

            $size_of_array_arabic_teachers = sizeof($arabic_teachers_info);
            $i = 1;
            if (isset($arabic_teachers_info) && $arabic_teachers_info) {
                foreach ($arabic_teachers_info as $value) {
                    if ($i < $size_of_array_arabic_teachers) {
                        $arabic_teachers .= $value->first_name . ' ' . $value->last_name . ' - ';
                    } else {
                        $arabic_teachers .= $value->first_name . ' ' . $value->last_name;
                    }
                    $i++;
                }
            }

            // get name of quran teacher 
            $quran_teachers_info = $this->rooms_teachers_model->get_teacher_of_quran_subject($student_info->room_id);

            $size_of_array_quran_teachers = sizeof($quran_teachers_info);
            $i = 1;
            if (isset($quran_teachers_info) && $quran_teachers_info) {
                foreach ($quran_teachers_info as $value) {
                    if ($i < $size_of_array_quran_teachers) {
                        $quran_teachers .= $value->first_name . ' ' . $value->last_name . ' - ';
                    } else {
                        $quran_teachers .= $value->first_name . ' ' . $value->last_name;
                    }
                    $i++;
                }
            }

            // get name of quran teacher 
            $islamic_studies_teachers_info = $this->rooms_teachers_model->get_teacher_of_islamic_studies_subject($student_info->room_id);

            $size_of_array_islamic_studies_teachers = sizeof($islamic_studies_teachers_info);
            $i = 1;

            if (isset($islamic_studies_teachers_info) && $islamic_studies_teachers_info) {
                foreach ($islamic_studies_teachers_info as $value) {
                    if ($i < $size_of_array_islamic_studies_teachers) {
                        $islamic_studies_teachers .= $value->first_name . ' ' . $value->last_name . ' - ';
                    } else {
                        $islamic_studies_teachers .= $value->first_name . ' ' . $value->last_name;
                    }
                    $i++;
                }
            }
            $exist['arabic_teacher'] = $arabic_teachers;
            $exist['quran_teacher'] = $quran_teachers;
            $exist['islamic_studies_teacher'] = $islamic_studies_teachers;

//            date_default_timezone_set('America/Los_Angeles');
//            $now_date = new DateTime('now');
//            $current_date = $now_date->getTimestamp();
//            $date_of_expired = date('m-d-Y', $current_date);
//            $date_of_expired = date('m-d-Y');

            $exist['date_of_expired'] = $date_of_expired;
        } else {
//            $exist->date_of_expired = date("m-d-Y", strtotime($exist->date_of_expired));
            $exist->date_of_expired = $date_of_expired;
        }
        send_message($exist);
    }

    /**
     * @api {get} /report_card/kg_pre2_report_card kg_pre2_report_card
     * @apiName kg_pre2_report_card
     * @apiGroup report_card
     * @apiDescription home page of kindergarten and preschool2 report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     */
    function kg_pre2_report_card($semester, $student_record_id) {
        $semester_name = 'first';
        if ($semester == '1')
            $semester_name = 'first';
        else
            $semester_name = 'second';
        // get student info for this student 
        $student_info = $this->students_records_model->get_student_record_info($student_record_id);

        $this->data['student_info'] = $student_info;

        $subjects = $this->subjects_model->get_subjects_except_arabic_and_its_children($student_info->grade_id);
        $children_subjects_of_arabic = $this->subjects_model->get_children_subjects_of_arabic($student_info->grade_id);
        $count_children_subjects_of_arabic = $this->subjects_model->count_get_children_subjects_of_arabic($student_info->grade_id);
        $count = $count_children_subjects_of_arabic->count;

        $only_arabic = FALSE;
        $arabic = array('Arabic', 'arabic');
        $counter = 0;
        if (in_array($count_children_subjects_of_arabic->subject_name, $arabic)) {
            $counter++;
        }

        if ($counter == 1)
            $only_arabic = TRUE;

        $subjects_array = array();
        foreach ($subjects as $value) {
            $subjects_array[$value->id] = $value->subject_name;
        }

        $this->data['subjects_array'] = $subjects_array;
        $this->data['subjects'] = $subjects;
        $this->data['children_subjects_of_arabic'] = $children_subjects_of_arabic;
        $this->data['count_children'] = $count;
        $this->data['only_arabic'] = $only_arabic;
//        $this->data['arabic_teachers'] = $arabic_teachers;
        // get marks of this student for specific semester 
        $room_id = $student_info->room_id;
        $student_marks = $this->students_marks_model->get_all_student_marks($semester_name, $room_id, $student_record_id);

        $student_marks_array = array();
        foreach ($student_marks as $value) {
            $student_marks_array[$value->subject_id] = $value;
        }

        $this->data['student_marks'] = $student_marks_array;

        // number of absent days 
        $absent_days = $this->rooms_attendances_model->get_number_of_absent_dayes($student_record_id, $semester_name);
        $this->data['absent_days'] = $absent_days;
    }

    /**
     * @api {post} /report_card/edit_kg_pre2_report_card edit_kg_pre2_report_card
     * @apiName edit_kg_pre2_report_card
     * @apiGroup report_card
     * @apiDescription edit information of kindergarten and preschool2 report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     * @apiSuccess InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     */
    function edit_kg_pre2_report_card($semester, $student_record_id) {
        $teacher = $this->input->post('teacher');
        $teacher_comments = $this->input->post('teacher_comments');
//        $date_of_expired = $this->input->post('date_of_expired');
//        $date_of_expired = date_format(date_create_from_format('m-d-Y', $date_of_expired), 'Y-m-d');

        $where = array(
            'student_record_id' => $student_record_id,
            'semester' => $semester,
        );
        $exist = $this->report_cards_model->get_by($where);

        if ($exist) {
            $data = array(
                'teacher' => $teacher,
                'teacher_comments' => $teacher_comments,
//                'date_of_expired' => $date_of_expired,
            );
            $update_item = $this->report_cards_model->update_by($where, $data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            $data = array(
                'student_record_id' => $student_record_id,
                'semester' => $semester,
                'teacher' => $teacher,
                'teacher_comments' => $teacher_comments,
//                'date_of_expired' => $date_of_expired,
            );
            $insert_item = $this->report_cards_model->insert($data);

            if (isset($insert_item) && $insert_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        }
    }

    /**
     * @api {get} /report_card/get_kg_pre2_report_card get_kg_pre2_report_card
     * @apiName get_kg_pre2_report_card
     * @apiGroup report_card
     * @apiDescription Get information of kindergarten and preschool2 student report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in report card 
     */
    function get_kg_pre2_report_card($semester, $student_record_id) {
        $where = array(
            'student_record_id' => $student_record_id,
            'semester' => $semester,
        );
        $exist = $this->report_cards_model->get_by($where);

        $student_info = $this->students_records_model->get_student_record_info($student_record_id);

        // get date of expired for certificate 
        $date_of_expired = "";
        $where = array(
            'grade_id' => $student_info->grade_id,
            'year' => $this->_archive_year,
        );
        $certificates_date = $this->certificates_dates_model->get_by($where);
        if (isset($certificates_date) && $certificates_date) {
            $date_of_expired = date("m-d-Y", strtotime( $certificates_date->date_of_expired));
        }

        $arabic_teachers = '';
        if (!$exist) {
//            $student_info = $this->students_records_model->get_student_record_info($student_record_id);
            // get name of arabic teacher 
            $arabic_teachers_info = $this->rooms_teachers_model->get_teacher_of_arabic_subject($student_info->room_id);

            $size_of_array = sizeof($arabic_teachers_info);
            $i = 1;
            if (isset($arabic_teachers_info) && $arabic_teachers_info) {
                foreach ($arabic_teachers_info as $value) {
                    if ($i < $size_of_array) {
                        $arabic_teachers .= $value->first_name . ' ' . $value->last_name . ' - ';
                    } else {
                        $arabic_teachers .= $value->first_name . ' ' . $value->last_name;
                    }
                    $i++;
                }
            }
            date_default_timezone_set('America/Los_Angeles');
//            $now_date = new DateTime('now');
//            $current_date = $now_date->getTimestamp();
//            $date_of_expired = date('m-d-Y', $current_date);
//            $date_of_expired = date('m-d-Y');
            $exist['date_of_expired'] = $date_of_expired;

            $exist['teacher'] = $arabic_teachers;
        } else {
//            $exist->date_of_expired = date("m-d-Y", strtotime($exist->date_of_expired));
            $exist->date_of_expired = $date_of_expired;
        }

        send_message($exist);
    }

    /**
     * @api {get} /report_card/p1_report_card p1_report_card
     * @apiName p1_report_card
     * @apiGroup report_card
     * @apiDescription home page of preschool1 report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     */
    function p1_report_card($semester, $student_record_id) {
        $semester_name = 'first';
        if ($semester == '1')
            $semester_name = 'first';
        else
            $semester_name = 'second';
        // get student info for this student 
        $student_info = $this->students_records_model->get_student_record_info($student_record_id);

        $this->data['student_info'] = $student_info;

        $subjects = $this->subjects_model->get_subjects_except_arabic_and_its_children($student_info->grade_id);
        $children_subjects_of_arabic = $this->subjects_model->get_children_subjects_of_arabic($student_info->grade_id);
        $count_children_subjects_of_arabic = $this->subjects_model->count_get_children_subjects_of_arabic($student_info->grade_id);
        $count = $count_children_subjects_of_arabic->count;

        $only_arabic = FALSE;
        $arabic = array('Arabic', 'arabic');
        $counter = 0;
        if (in_array($count_children_subjects_of_arabic->subject_name, $arabic)) {
            $counter++;
        }

        if ($counter == 1)
            $only_arabic = TRUE;

        $subjects_array = array();
        foreach ($subjects as $value) {
            $subjects_array[$value->id] = $value->subject_name;
        }
        $this->data['subjects_array'] = $subjects_array;
        $this->data['subjects'] = $subjects;
        $this->data['children_subjects_of_arabic'] = $children_subjects_of_arabic;
        $this->data['count_children'] = $count;
        $this->data['only_arabic'] = $only_arabic;

        // get marks of this student for specific semester 
        $room_id = $student_info->room_id;
        $student_marks = $this->students_marks_model->get_all_student_marks($semester_name, $room_id, $student_record_id);

        $student_marks_array = array();
        foreach ($student_marks as $value) {
            $student_marks_array[$value->subject_id] = $value;
        }

        $this->data['student_marks'] = $student_marks_array;

        // number of absent days 
        $absent_days = $this->rooms_attendances_model->get_number_of_absent_dayes($student_record_id, $semester_name);

        $this->data['absent_days'] = $absent_days;
    }

    /**
     * @api {post} /report_card/edit_p1_report_card edit_p1_report_card
     * @apiName edit_p1_report_card
     * @apiGroup report_card
     * @apiDescription edit information of preschool1 report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     * @apiParam {String} teacher_comments comment of teacher 
     * @apiParam {String} teacher Name of teacher 
     * @apiSuccess InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     */
    function edit_p1_report_card($semester, $student_record_id) {
        $teacher = $this->input->post('teacher');
        $teacher_comments = $this->input->post('teacher_comments');
//        $date_of_expired = $this->input->post('date_of_expired');
//        $date_of_expired = date_format(date_create_from_format('m-d-Y', $date_of_expired), 'Y-m-d');

        $where = array(
            'student_record_id' => $student_record_id,
            'semester' => $semester,
        );
        $exist = $this->report_cards_model->get_by($where);

        if ($exist) {
            $data = array(
                'teacher' => $teacher,
                'teacher_comments' => $teacher_comments,
//                'date_of_expired' => $date_of_expired,
            );
            $update_item = $this->report_cards_model->update_by($where, $data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            $data = array(
                'student_record_id' => $student_record_id,
                'semester' => $semester,
                'teacher' => $teacher,
                'teacher_comments' => $teacher_comments,
//                'date_of_expired' => $date_of_expired,
            );
            $insert_item = $this->report_cards_model->insert($data);

            if (isset($insert_item) && $insert_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        }
    }

    /**
     * @api {get} /report_card/get_p1_report_card get_p1_report_card
     * @apiName get_p1_report_card
     * @apiGroup report_card
     * @apiDescription Get information of preschool1 student report card
     * @apiParam {String} semester Name of semester 
     * @apiParam {Number} student_record_id id of student record 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in report card 
     */
    function get_p1_report_card($semester, $student_record_id) {
        $where = array(
            'student_record_id' => $student_record_id,
            'semester' => $semester,
        );
        $exist = $this->report_cards_model->get_by($where);

        $student_info = $this->students_records_model->get_student_record_info($student_record_id);

        // get date of expired for certificate 
        $date_of_expired = "";
        $where = array(
            'grade_id' => $student_info->grade_id,
            'year' => $this->_archive_year,
        );
        $certificates_date = $this->certificates_dates_model->get_by($where);

        if (isset($certificates_date) && $certificates_date) {
            $date_of_expired = date("m-d-Y", strtotime( $certificates_date->date_of_expired));
        }
        $arabic_teachers = '';
        if (!$exist) {
            // get name of arabic teacher 
            $arabic_teachers_info = $this->rooms_teachers_model->get_teacher_of_arabic_subject($student_info->room_id);

            $size_of_array = sizeof($arabic_teachers_info);
            $i = 1;

            if (isset($arabic_teachers_info) && $arabic_teachers_info) {
                foreach ($arabic_teachers_info as $value) {
                    if ($i < $size_of_array) {
                        $arabic_teachers .= $value->first_name . ' ' . $value->last_name . ' - ';
                    } else {
                        $arabic_teachers .= $value->first_name . ' ' . $value->last_name;
                    }
                    $i++;
                }
            }
            $exist['teacher'] = $arabic_teachers;

//            date_default_timezone_set('America/Los_Angeles');
//            $now_date = new DateTime('now');
//            $current_date = $now_date->getTimestamp();
//            $date_of_expired = date('m-d-Y', $current_date);
//            $date_of_expired = date('m-d-Y');
            $exist['date_of_expired'] = $date_of_expired;
        } else {
//            $exist->date_of_expired = date("m-d-Y", strtotime($exist->date_of_expired));
            $exist->date_of_expired = $date_of_expired;
        }
        send_message($exist);
    }

    /**
     * @api {get} /report_card/no_report_card no_report_card
     * @apiName no_report_card
     * @apiGroup report_card
     * @apiDescription Default page if no report card found for grade
     */
    function no_report_card() {
        
    }

}
