<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stage extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('stages_model');
        $this->load->model('grades_model');
    }

    function index() {
        
    }

    /**
     * @api {get} /stage/index_stage index_stage
     * @apiName index_stage
     * @apiGroup stage
     * @apiDescription home page of stages
     */
    function index_stage() {
        $this->data['show_object_link'] = 'stage/show_stages';
        $this->data['get_object_link'] = 'stage/get_stage';
        $this->data['add_object_link'] = 'stage/add_stage';
        $this->data['update_object_link'] = 'stage/update_stage';
        $this->data['delete_object_link'] = 'stage/delete_stage';
        $this->data['modal_name'] = 'crud_stages';
        $this->data['add_object_title'] = lang("add_new_stage");
        $this->data['update_object_title'] = lang("update_stage");
        $this->data['delete_object_title'] = lang("delete_stage");
        $this->data['thead'] = array('stage_name',);

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        } else {
            $this->data['thead'][] = 'hidden';
            $this->data['non_printable']['hidden'] = 'hidden';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /stage/show_stages show_stages
     * @apiName show_stages
     * @apiGroup stage
     * @apiDescription get all stages
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in stages table
     * @apiError  FALSE return false if failure in get data
     */
    function show_stages() {
        $stages = $this->stages_model->get_stages();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($stages as $stage) {
            $no++;
            $row = array();
            $row[] = $stage->stage_name;

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($stage->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($stage->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            } else {
                $row[] = ' ';
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => count($stages),
            "recordsFiltered" => count($stages),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /stage/add_stage add_stage
     * @apiName add_stage
     * @apiGroup stage
     * @apiDescription add stage data to stages database table
     * @apiParam {String} stage_name Name of stage
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  stage_name_already_exist202 return stage name already exist error message
     */
    function add_stage() {
        $input_array = array(
            'stage_name' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $stage_name = $this->input->post('stage_name');

            if (exist_item("stages_model", array('stage_name' => $stage_name, 'stage_name' => $stage_name))) {
                send_message("", '202', 'stage_name_already_exist');
            }

//
//            $where = array(
//                'stage_name' => $stage_name,
//                'next_stage_name' => $next_stage_name,
//                'stage_name' => $stage_name,
//            );
//            $get_next_stage_name = $this->stages_model->get_by($where);
//            if (isset($get_next_stage_name) && $get_next_stage_name) {
//                if ($stage_name == $get_next_stage_name->next_stage_name) {
//                    error_message('400', 'insert_error');
//                } else {
            $data = array(
                'stage_name' => $stage_name,
            );
            $insert = $this->stages_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
//                }
//            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /stage/get_stage get_stage
     * @apiName get_stage
     * @apiGroup stage
     * @apiDescription get id of stage to get its info from stages database table
     * @apiParam {Number} id id of grade
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of stage from stages table
     * @apiError  FALSE return false if failure in get data
     */
    function get_stage($id) {
        $data = $this->stages_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /stage/update_stage update_stage
     * @apiName update_stage
     * @apiGroup stage
     * @apiDescription update stage data in stages database table
     * @apiParam {Number} id id of stage
     * @apiParam {String} stage_name Name of stage
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_stage() {
        $stages = $this->stages_model->get_all();

        $input_array = array(
            'stage_name' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $stage_name = $this->input->post('stage_name');

            // start validation
            if (exist_item("stages_model", array('stage_name' => $stage_name, 'stage_name' => $stage_name, 'id <>' => $item_id))) {
                send_message('', "202", 'stage_name_already_exist');
            }

            validation_edit_delete_redirect($stages, "id", $item_id);
            // end validation

            $data = array(
                'stage_name' => $stage_name,
            );

            $update_item = $this->stages_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /stage/delete_stage delete_stage
     * @apiName delete_stage
     * @apiGroup stage
     * @apiDescription get id of stage to delete its info from stages database table
     * @apiParam {Number} id id of stage
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     * @apiError  Related_with_another_elem407 return JSON encoded string contain related with another element error message

     */
    function delete_stage($id) {
        $stages = $this->stages_model->get_all();
        //start validation
        validation_edit_delete_redirect($stages, "id", $id);
        // end validation

        $related = $this->grades_model->get_by(["stage_id" => $id]);
        if ($related) {
        error_message('407', 'related_with_another_elem');
        }
        $delete_item = $this->stages_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
