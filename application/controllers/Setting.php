<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('settings_model');
        $this->load->model('students_records_model');
        $this->load->model('certificates_dates_model');
    }

    /**
     * @api {get} /setting/index_setting index_setting
     * @apiName index_setting
     * @apiGroup setting
     * @apiDescription home page of setting to display main links in setting
     */
    function index_setting() {
        
    }

    /**
     * @api {get} /setting/determine_current_year determine_current_year
     * @apiName determine_current_year
     * @apiGroup setting
     * @apiDescription determine current year
     */
    function determine_current_year() {
        $old_year = $this->settings_model->get_current_year();
        $this->data['year'] = $old_year;
    }

    /**
     * @api {get} /setting/edit_year edit_year
     * @apiName edit_year
     * @apiGroup setting
     * @apiDescription edit current year
     * @apiParam {Number} year Number of year
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  year_error408 return JSON encoded string contain error message 
     */
    function edit_year() {
        $year = $this->input->post("year");
        $old_year = $this->settings_model->get_current_year();

        if ($old_year) {
            if ($year < $old_year) {
                send_message(lang("year_error") . $old_year, "408", "");
            }
        }

//        if ($old_year) {
//            if ($year > ($old_year + 1)) {
//                send_message(lang("year_error") . $old_year, "408", "");
//            }
//        }

        $update = $this->settings_model->update_year($year);

        $this->data['_current_year'] = $this->_current_year = $this->_session['_current_year'] = $year;
        $this->save_session();
        $this->data['_archive_year'] = $this->_archive_year = $this->_session['_archive_year'] = $year;
        $this->save_session();

        if ($this->_session['_current_year']) {
            send_message($year, "200", "update_success");
        } else {
            error_message("400", "update_error");
        }
    }

    /**
     * @api {get} /setting/archive_year archive_year
     * @apiName archive_year
     * @apiGroup setting
     * @apiDescription determine archive year
     */
    function archive_year() {
        for ($i = 2017; $i <= $this->_current_year; $i++)
            $years[$i] = $i . '-' . ($i + 1);

        /* Change the Current Year Of the System */
        $this->data['years'] = $years;
    }

    /**
     * @api {get} /setting/edit_archive_year edit_archive_year
     * @apiName edit_archive_year
     * @apiGroup setting
     * @apiDescription edit archive year
     * @apiParam {Number} year Number of year
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     */
    function edit_archive_year() {
        for ($i = 2017; $i <= $this->_current_year; $i++)
            $years[$i] = $i . '-' . ($i + 1);

        $year = $this->input->post('year');

        if ($year && array_key_exists($year, $years)) {
            $this->_session['_archive_year'] = $year;
            $this->save_session();
            if ($this->_session['_archive_year']) {
                send_message($year, "200", "update_success");
            } else {
                error_message("400", "update_error");
            }
        }
    }

    /**
     * @api {get} /setting/determine_school_contact_email determine_school_contact_email
     * @apiName determine_school_contact_email
     * @apiGroup setting
     * @apiDescription home page of school contact email
     */
    function determine_school_contact_email() {
        $school_contact_email = $this->settings_model->get_school_contact_email();
        $this->data['school_contact_email'] = $school_contact_email;
    }

    /**
     * @api {get} /setting/edit_school_contact_email edit_school_contact_email
     * @apiName edit_school_contact_email
     * @apiGroup setting
     * @apiDescription edit school contact email
     * @apiParam {String} school_contact_email Email of school contact
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function edit_school_contact_email() {
        $school_contact_email = $this->input->post("school_contact_email");

        $input_array = array(
            'school_contact_email' => 'required|valid_email',
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $update = $this->settings_model->update_school_contact_email($school_contact_email);

            if ($update) {
                send_message("", "200", "update_success");
            } else {
                error_message("400", "update_error");
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    public function excel_setting() {
        $this->data['header1'] = $this->settings_model->get_by(["setting_key" => "excel_header1"]);
        $this->data['header2'] = $this->settings_model->get_by(["setting_key" => "excel_header2"]);
        $this->data['header3'] = $this->settings_model->get_by(["setting_key" => "excel_header3"]);
        $this->data['logo'] = $this->settings_model->get_by(["setting_key" => "logo"]);
    }

    public function update_excel_setting() {

        $this->settings_model->update_by(["setting_key" => "excel_header1"], ["setting_value" => $this->input->post("header1")]);
        $this->settings_model->update_by(["setting_key" => "excel_header2"], ["setting_value" => $this->input->post("header2")]);
        $this->settings_model->update_by(["setting_key" => "excel_header3"], ["setting_value" => $this->input->post("header3")]);

        $header_logo = file_or_image('header_logo', 'header_logo');

        if ($header_logo == '0') {
            error_message('400', 'insert_error_file_size_should_be_longer_than_zero');
        }
        $this->settings_model->update_by(["setting_key" => "logo"], ["setting_value" => $header_logo]);

        send_message("");
    }

    function determine_certificate_date() {
        $certificates_dates = $this->certificates_dates_model->get_certificates_dates();

        $this->data['certificates_dates'] = $certificates_dates;

        $certificates_dates_array = array();
        if (isset($certificates_dates) && $certificates_dates) {
            foreach ($certificates_dates as $value) {
                if ($value->date_of_expired == '0000-00-00') {
                    $certificates_dates_array[$value->grade_id] = '0000-00-00';
                } else {
                    $certificates_dates_array[$value->grade_id] = date("m-d-Y", strtotime($value->date_of_expired));
                }
            }
        }

        $where = array('report_card_name <>' => 'no_report_card');
        $grades = $this->grades_model->get_many_by($where);
        $this->data['grades'] = $grades;
        $this->data['certificates_dates_array'] = $certificates_dates_array;

//        $grades_array=array();
//        $certificates_dates_array = array();
//        foreach ($grades as $value) {
//            if (isset($certificates_dates_array) && $certificates_dates_array && isset($certificates_dates_array[$value->id])) {
//                
//            }
//        }
    }

    /**
     * @api {get} /setting/edit_school_contact_email edit_school_contact_email
     * @apiName edit_school_contact_email
     * @apiGroup setting
     * @apiDescription edit school contact email
     * @apiParam {String} school_contact_email Email of school contact
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function edit_certificate_date() {
        $certificates_dates = $this->certificates_dates_model->get_certificates_dates();
        $certificates_dates_exist = array();
        if (isset($certificates_dates) && $certificates_dates) {
            foreach ($certificates_dates as $value) {
                $certificates_dates_exist[$value->grade_id] = $value->id;
            }
        }

        $where = array('report_card_name <>' => 'no_report_card');
        $grades = $this->grades_model->get_many_by($where);
        $this->data['grades'] = $grades;

        $certificates_dates_array = array(
            'year' => $this->_current_year,
        );
        $update = false;
        $insert = false;
        foreach ($grades as $value) {
            $certificates_dates_array['grade_id'] = $value->id;
            $certificates_dates_array['date_of_expired'] = $this->input->post("date_of_expired" . $value->id);

            if ($certificates_dates_array['date_of_expired'] != '')

//                $certificates_dates_array['date_of_expired'] = date_format(date_create_from_format('Y-m-d', $certificates_dates_array['date_of_expired']), 'm-d-Y');
                $certificates_dates_array['date_of_expired'] = date_format(date_create_from_format('m-d-Y', $certificates_dates_array['date_of_expired']), 'Y-m-d');

            if (isset($certificates_dates_exist) && $certificates_dates_exist && isset($certificates_dates_exist[$value->id])) {
                $where = array('id' => $certificates_dates_exist[$value->id]);
                $data = array('date_of_expired' => $certificates_dates_array['date_of_expired']);
                $update = $this->certificates_dates_model->update_by($where, $data);
            } else {
                $insert = $this->certificates_dates_model->insert($certificates_dates_array);
            }
        }
        if ($update || $insert) {
            send_message("", "200", "update_success");
        } else {
            error_message("400", "update_error");
        }
    }

}
