<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Room_attendance extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('rooms_attendances_model');
        $this->load->model('students_model');
        $this->load->model('rooms_model');
        $this->load->model('stages_model');
        $this->load->model('grades_model');
        date_default_timezone_set('America/Los_Angeles');
    }

    /**
     * @api {get} /room_attendance/index_room_attendance index_room_attendance
     * @apiName index_room_attendance
     * @apiGroup room_attendance
     * @apiDescription home page of room attendance for specific room
     * @apiParam {Number} room_id id of room
     */
    function index_room_attendance($room_id) {
        $this->data['room_name'] = $this->rooms_model->get_room($room_id);
        $this->data['room_id'] = $room_id;
        $this->data['semesters'] = array(
            'first_semester' => lang('first_semester'),
            'second_semester' => lang('second_semester'),
        );
//        $dt = new DateTime();
        $this->data['day_date'] = '0000-00-00';
    }

    /**
     * @api {get} /room_attendance/get_all_students get_all_students
     * @apiName get_all_students
     * @apiGroup room_attendance
     * @apiDescription get id of room to get students of this room from student records table
     * @apiParam {Number} id id of room
     * @apiParam {Date} date of attendance
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of students from rooms attendances table
     * @apiError  FALSE return false if failure in get data
     * @apiError ErrorMessage405 no student found
     */
    function get_all_students($room_id) {
//        $dt = new DateTime();
        $day_date = date('Y-m-d');
        $date = $day_date;
        if ($this->input->post("date")) {
            //TODO
            $date = $this->input->post("date");
            if ($date)
                $date = date_format(date_create_from_format('m-d-Y', $date), 'Y-m-d');
        }
        // get all recorded room attendence 
        $room_attendance_info = $this->rooms_attendances_model->get_room_attendance_by_date($room_id, $date);

        // get all recorded room attendence and students attendances
        $all_students_by_attendance_date = $this->rooms_attendances_model->get_all_students_by_attendance_date($room_id, $date);

        $all_students_by_room_id = $this->rooms_attendances_model->get_all_students_by_room_id($room_id);

        $attendance_array = array();
        if (isset($all_students_by_attendance_date) && $all_students_by_attendance_date) {
            $array_of_all_students_by_attendance_date = array();
            foreach ($all_students_by_attendance_date as $item) {
                $array_of_all_students_by_attendance_date[$item->student_id] = $item;
            }

            foreach ($all_students_by_room_id as $item) {
                $res = array();
                if (isset($array_of_all_students_by_attendance_date[$item->student_id])) {
                    $res["id"] = $array_of_all_students_by_attendance_date[$item->student_id]->id;
                    $res["student_id"] = $array_of_all_students_by_attendance_date[$item->student_id]->student_id;
                    $res["name"] = $array_of_all_students_by_attendance_date[$item->student_id]->student_name . " " . $array_of_all_students_by_attendance_date[$item->student_id]->father_name . " " . $array_of_all_students_by_attendance_date[$item->student_id]->family_name;
                    $res["attend"] = 1;
                    $res["absence"] = 0;
                    $res["delay"] = 0;
                    if (!isset($array_of_all_students_by_attendance_date[$item->student_id]->attendance_date)) {
                        $res["attend"] = 1;
                        $res["absence"] = 0;
                        $res["delay"] = 0;
                    } else if ($array_of_all_students_by_attendance_date[$item->student_id]->attendance_type == "absence") {
                        $res["attend"] = 0;
                        $res["absence"] = 1;
                        $res["delay"] = 0;
                    } else if ($array_of_all_students_by_attendance_date[$item->student_id]->attendance_type == "delay") {
                        $res["attend"] = 0;
                        $res["absence"] = 0;
                        $res["delay"] = 1;
                    }
                } else {

                    $registration_date = new DateTime($item->created_at);

                    $registration_date = $registration_date->format('Y-m-d');

//                    var_dump($registration_date);
                    if ($registration_date > $date) {
                        $res["attend"] = 0;
                        $res["absence"] = 0;
                        $res["delay"] = 0;
                    } else {
                        $res["attend"] = 1;
                        $res["absence"] = 0;
                        $res["delay"] = 0;
                    }
                    $res["id"] = $item->id;
                    $res["student_id"] = $item->student_id;
//                        $res["student_id"] = $item->student_id;
                    $res["name"] = $item->student_name . " " . $item->father_name . " " . $item->family_name;
                }
                $attendance_array[] = $res;
            }
        } else {
            $students_info = $this->rooms_attendances_model->get_all_students_by_room_id($room_id);

            if ($students_info) {
                if ($room_attendance_info || (isset($day_date) && $date == $day_date)) {
                    foreach ($students_info as $item) {
                        $res = array();
                        $res["id"] = $item->id;
                        $res["student_id"] = $item->student_id;
//                        $res["student_id"] = $item->student_id;
                        $res["name"] = $item->student_name . " " . $item->father_name . " " . $item->family_name;
                        $res["attend"] = 1;
                        $res["absence"] = 0;
                        $res["delay"] = 0;
                        $attendance_array[] = $res;
                    }
                } else {
                    foreach ($students_info as $item) {
                        $res = array();
                        $res["id"] = $item->id;
                        $res["student_id"] = $item->student_id;
//                        $res["student_id"] = $item->student_id;
                        $res["name"] = $item->student_name . " " . $item->father_name . " " . $item->family_name;
                        $res["attend"] = 0;
                        $res["absence"] = 0;
                        $res["delay"] = 0;
                        $attendance_array[] = $res;
                    }
                }
            } else {
                error_message('405', 'no_students');
            }
        }
        if ($attendance_array) {
            send_message($attendance_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    /**
     * @api {get} /room_attendance/update_attendace_values update_attendace_values
     * @apiName update_attendace_values
     * @apiGroup room_attendance
     * @apiDescription get id of room to get students of this room from student records table and update values 
     * @apiParam {Number} room_id id of room
     * @apiParam {Date} date of attendance
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of students from rooms attendances table
     * @apiError  FALSE return false if failure in get data
     * @apiError Error_future_date405 return error future date if attendance_date is greater than current date
     */
    function update_attendace_values($room_id) {
        $attendance_date = $this->input->post("attendance_date");

        if ($attendance_date == NULL) {
            error_message("400", "some_field_is_empty");
        }

        if ($attendance_date)
            $attendance_date = date_format(date_create_from_format('m-d-Y', $attendance_date), 'Y-m-d');

        if ($attendance_date == NULL) {
            error_message("400", "some_field_is_empty");
        }
        $day_date = date('Y-m-d');

        // get all recorded room attendence 
        $room_attendance = $this->rooms_attendances_model->get_room_attendance_by_date($room_id, $attendance_date);

        // get all recorded room attendence and students attendances
        $students_info = $this->rooms_attendances_model->get_all_students_by_room_id($room_id);

        $attendance_type_check = array();

        foreach ($students_info as $student) {
            $attendance_type_check[$student->id] = $this->input->post("attendance" . $student->id);
        }

        if (empty($attendance_type_check)) {
            error_message("400", "some_field_is_empty");
        }

        foreach ($students_info as $student) {
            if ($attendance_type_check[$student->id] == NULL) {
                error_message("400", "some_field_is_empty");
                break;
            }
        }

        //trying to save record for future date
        if ($attendance_date > $day_date) {
            error_message("401", "error_future_date");
        }

        //if this condition is true then its not the first save and it is an update status
        //if this condition is false then it will be the first save for this record and it is an insert status
        if ($room_attendance) {
            $old_students_attendaces_info = $this->rooms_attendances_model->get_all_students_by_attendance_date($room_id, $attendance_date);

            $old_students_attendaces_array = array();
            foreach ($old_students_attendaces_info as $student) {
                $old_students_attendaces_array[$student->id] = $student;
            }

            $check = true;
            foreach ($students_info as $student) {
                $attendance_type = $this->input->post("attendance" . $student->id);

                // new type is attend and old one is either absence or delay
                if ($attendance_type == "attend" && isset($old_students_attendaces_array[$student->id])) {
                    $delete_item = $this->rooms_attendances_model->delete_student_attendance($student->id);
                    if (!$delete_item) {
                        $check = false;
                    }
                    // new type is either absence or delay and old one is either absence or delay
                } else if ($attendance_type != "attend" && isset($old_students_attendaces_array[$student->id]) && $attendance_type != $old_students_attendaces_array[$student->id]->attendance_type) {
                    $update_data = array(
                        'attendance_type' => $attendance_type,
                    );
                    $update_item = $this->rooms_attendances_model->update_student_attendance($student->id, $update_data);
                    if (!$update_item) {
                        $check = false;
                    }
                    //new type is either absence or delay and old one is attend
                } else if (!isset($old_students_attendaces_array[$student->id])) {
                    $month = date("m", strtotime($attendance_date));
                    $semester = "first";
                    if ($month == "01" || $month == "02" || $month == "03" || $month == "04" || $month == "05") {
                        $semester = "second";
                    }

                    $inserted = false;
                    if ($attendance_type != NULL) {
                        $insert_data = array(
                            "semester" => $semester,
                            "attendance_date" => $attendance_date,
                            "attendance_type" => $attendance_type,
                            "student_record_id" => $student->id
                        );

                        $inserted = $this->rooms_attendances_model->insert_student_attendance($insert_data);
                    }
                    if (!$inserted) {
                        $check = false;
                    }
                }
            }
            if ($check) {
                send_message("", "200", "update_success");
            } else {
                error_message("400", "update_error");
            }
        } else {
//            if ($attendance_type != NULL) {
            $data = array(
                'attendance_date' => $attendance_date,
                "room_id" => $room_id
            );
            $inserted = $this->rooms_attendances_model->insert_attendance($data);
            if ($inserted) {
                $data_for_insert = array();

                foreach ($students_info as $student) {
                    $month = date("m", strtotime($attendance_date));
                    $semester = "first";
                    if ($month == "01" || $month == "02" || $month == "03" || $month == "04" || $month == "05") {
                        $semester = "second";
                    }
                    $attendance_type = $this->input->post("attendance" . $student->id);

                    if ($attendance_type != "attend" && $attendance_type != null) {
                        $data_for_insert[] = array("semester" => $semester, "attendance_date" => $attendance_date, "attendance_type" => $attendance_type, "student_record_id" => $student->id);
                    }
                }
                if (!empty($data_for_insert)) {
                    $inserted_to_students_attendances = $this->rooms_attendances_model->insert_list_students_attendaces($data_for_insert);
                }
                send_message("", "200", "insert_success");
            }
//            }
            else {
                error_message("400", "insert_error");
            }
        }
    }

    /**
     * @api {get} /room_attendance/index_student_attendance index_student_attendance
     * @apiName index_student_attendance
     * @apiGroup room_attendance
     * @apiDescription home page of student attendance for specific room
     * @apiParam {Number} room_id id of room
     */
    function index_student_attendance($student_id) {
        if (empty($student_id) || !is_numeric($student_id)) {
            redirect("home/home_page");
        }

        $where = array(
            'students.id' => $student_id,
        );
        $this->db->join("parents", "students.parent_id = parents.id");
        $student_info = $this->students_model->get_by($where);
        $this->data['student_info'] = $student_info;

        $results = $this->students_model->get_attendances($student_id);
        $this->data['student_attendaces'] = $results;
    }

    /**
     * @api {get} /room_attendance/take_attendance_from_home take_attendance_from_home
     * @apiName take_attendance_from_home
     * @apiGroup room_attendance
     * @apiDescription get grades and rooms of this grades 
     */
    function take_attendance_from_home() {
        $stages_options = array('000' => lang('select_stage_name'));
//        $grades_options = array('000' => lang('select_grade_name'));
        $rooms_options = array();
        $grades_options = array();

        $stages = $this->stages_model->get_all();
//        $grades = $this->grades_model->get_grades();
//        $rooms = $this->rooms_model->get_all();

        foreach ($stages as $value) {
            $stages_options[$value->id] = $value->stage_name;
        }

//        foreach ($grades as $value) {
//            $grades_options[$value->id] = $value->grade_name;
//        }
//
//        foreach ($rooms as $value) {
//            $rooms_options[$value->id] = $value->room_name;
//        }
        $this->data['stages_options'] = $stages_options;
        $this->data['grades_options'] = $grades_options;
        $this->data['rooms_options'] = $rooms_options;

//        $this->data['room_name'] = $this->rooms_model->get_room($room_id);
//        $this->data['room_id'] = $room_id;
//        $this->data['semesters'] = array(
//            'first_semester' => lang('first_semester'),
//            'second_semester' => lang('second_semester'),
//        );
//        $dt = new DateTime();
        $this->data['day_date'] = '0000-00-00';
    }

    function get_grades_in_stage() {
        $stage_id = $this->input->post('stage_id');
        $where = array('stage_id' => $stage_id);
        $grades = $this->grades_model->get_many_by($where);

        send_message($grades);
    }

    function get_rooms_in_grade() {
        $grade_id = $this->input->post('grade_id');
        $where = array('grade_id' => $grade_id);
        $rooms = $this->rooms_model->get_many_by($where);
        send_message($rooms);
    }

    /**
     * @api {get} /room_attendance/get_stage_of_grade get_stage_of_grade
     * @apiName get_stage_of_grade
     * @apiGroup room_attendance
     * @apiDescription get id of grade to get its info from grades database table
     * @apiParam {Number} grade_id id of grade
     * @apiSuccess  grade_info Array contain grades information for specific grade
     */
    function get_stage_of_grade($grade_id) {
        $grade_info = $this->grades_m->get_by('id', $grade_id);
        send_message($grade_info);
    }

//
//    function room_calendar() {
////        $this->data['method'] = 'calendar_lang';
//    }
//
//    public function get_events() {
//        $data = '';
//
//        if ($data) {
//            foreach ($data as $item) {
//                $item->external_url = http_check($item->external_url);
//            }
//            $result_data = array("result" => $data, "total" => $rows_number, "month" => $month);
//            send_message($result_data, "200", "success");
//        } else {
//            send_message("", "400", "no_events_found");
//        }
//    }
}
