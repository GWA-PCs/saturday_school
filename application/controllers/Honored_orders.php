<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Honored_orders extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('honored_orders_model');
        $this->load->model('teachers_model');
        $this->load->model('halls_model');
        $this->load->model('stages_model');
        date_default_timezone_set('America/Los_Angeles');
    }

    function index() {
        
    }

    /**
     * @api {get} /honored_orders/index_honored_orders index_honored_orders
     * @apiName index_honored_orders
     * @apiGroup honored_orders
     * @apiDescription home page of honored_orders
     */
    function index_honored_orders() {
        //get id of user
        $user_info = $this->_user_login;

        //get teacher id 
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);

        //get rooms of teacher 
        $teacher_rooms = $this->teachers_model->get_teacher_room($teacher_id);

        //get stages of grade whose teacher teached in it
        $teacher_stages = array();
        foreach ($teacher_rooms as $value) {
            $teacher_stages[$value->stage_id] = $value->stage_id;
        }
        $stages = $this->stages_model->get_stages();

        // fill stages array with stages id   
        $stages_options = array();
        $stages_options[0] = lang('select_stage_name');
        foreach ($stages as $value) {
            if (isset($teacher_stages[(int) $value->id])) {
                if ($teacher_stages[$value->id] == intval($value->id)) {
                    $stages_options[$value->id] = $value->stage_name;
                }
            }
        }

        $this->data['stages_options'] = $stages_options;
        $this->data['show_object_link'] = 'honored_orders/show_honored_orders/' . $teacher_id;
        $this->data['get_object_link'] = 'honored_orders/get_honored_order';
        $this->data['add_object_link'] = 'honored_orders/add_honored_order/' . $teacher_id;
        $this->data['update_object_link'] = 'honored_orders/update_honored_order';
        $this->data['delete_object_link'] = 'honored_orders/delete_honored_order';
        $this->data['modal_name'] = 'crud_honored_orders';
        $this->data['add_object_title'] = lang("add_new_honored_order");
        $this->data['update_object_title'] = lang("update_honored_order");
        $this->data['delete_object_title'] = lang("delete_honored_order");
        $this->data['thead'] = array('order_title', 'stage_id', 'honored_date', 'honored_reason', 'order_status', 'index_honored_students', 'update', 'delete');
//     'honored_order_created_date',
        // put the title whose don't apear in print and excel
        $this->data['non_printable']['index_honored_students'] = 'index_honored_students';
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /honored_orders/show_honored_orders show_honored_orders
     * @apiName show_honored_orders
     * @apiGroup honored_orders
     * @apiDescription get all honored orders
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in honored_orders table
     * @apiError  FALSE return false if failure in get data
     */
    function show_honored_orders($teacher_id) {
        $honored_orders = $this->honored_orders_model->get_honored_orders($teacher_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }

        foreach ($honored_orders as $honored_order) {
            $no++;

            $row = array();
            $row[] = $honored_order->order_title;

            $stage_id = $this->stages_model->get_stage($honored_order->stage_id);
            $row[] = $stage_id->stage_name;
            $row[] = date("m-d-Y", strtotime($honored_order->honored_date));


//            $row[] = $honored_order->created_at;
            $row[] = $honored_order->honored_reason;
//            $row[] = $honored_order->created_at;
            $row[] = lang($honored_order->order_status);
            //add html for action
            $row[] = bs3_link_crud("honored_students/index_honored_students/" . $honored_order->id, '<i class=" mdi mdi-account-star-variant fa-2x"></i>', lang('index_honored_students'), 'update');
            $row[] = bs3_update_delete_crud($honored_order->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            $row[] = bs3_update_delete_crud($honored_order->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->honored_orders_model->count_all_honored_orders($teacher_id),
            "recordsFiltered" => $this->honored_orders_model->count_filtered_crud_honored_orders($teacher_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /honored_orders/add_honored_order add_honored_order
     * @apiName add_honored_order
     * @apiGroup honored_orders
     * @apiDescription add honored order to honored orders database table
     * @apiParam {Number} teacher_id id of teacher who add honored order
     * @apiParam {String} order_title Title of honored order
     * @apiParam {Number} stage_id id of stage (select from dropdown list)
     * @apiParam {Date} honored_date Date of honored order
     * @apiParam {String} honored_reason Reason of honored order
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  Grade_name_already_exist202 return grade name already exist error message

     */
    function add_honored_order($teacher_id) {
        $input_array = array(
            'order_title' => "required",
            'stage_id' => "required",
            'honored_date' => "required",
            'honored_reason' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $order_title = $this->input->post('order_title');
            $stage_id = $this->input->post('stage_id');
            $honored_date = $this->input->post('honored_date');
            if ($honored_date)
                $honored_date = date_format(date_create_from_format('m-d-Y', $honored_date), 'Y-m-d');
            $honored_reason = $this->input->post('honored_reason');


//            if (exist_item("honored_orders_model", array('order_title' => $order_title))) {
//                send_message("", '202', 'order_title_already_exist');
//            }

            if ($stage_id != '000') {
                // get status of hall
                $data = array(
                    'order_title' => $order_title,
                    'stage_id' => $stage_id,
                    'teacher_id' => $teacher_id,
                    'honored_date' => $honored_date,
                    'honored_reason' => $honored_reason,
                );
                $hall_status = $this->halls_model->get_hall_status();

                // if hall_status is equal to reserved
                if ($hall_status == 'reserved') {
                    // get other honored order is exist at same date
                    $other_honored_orders = $this->honored_orders_model->get_other_honored_orders($teacher_id, $honored_date);

                    // check if other honored order is exist at same date
                    if (isset($other_honored_orders) && $other_honored_orders) {
                        $i = 0;
                        $reserved_done = FALSE;
                        foreach ($other_honored_orders as $value) {
                            $i++;
                            // if their is another honored orders you can reserved hall id stage id is equal to current stage
                            if ($stage_id == $value->stage_id) {
                                $data['order_status'] = 'reservation_done';
                                $insert = $this->honored_orders_model->insert($data);
                                if ($insert) {
                                    $reserved_done = TRUE;
                                    send_message("", '200', 'insert_success');
                                } else {
                                    error_message('400', 'insert_error');
                                }
                                break;
                            }
                            // if hall not reserved this main the status of honored orders is pending
                            if (($i == sizeof($other_honored_orders)) && $reserved_done == FALSE) {
                                $data['order_status'] = 'pending';
                                $insert = $this->honored_orders_model->insert($data);
                                if ($insert) {
                                    $reserved_done = TRUE;
                                    send_message("", '200', 'insert_success');
                                } else {
                                    error_message('400', 'insert_error');
                                }
                            }
                        }
                    }  // if no other honored order is exist at same date the honored order is done and hall is reserved
                    else {
                        $data['order_status'] = 'reservation_done';
                        $insert = $this->honored_orders_model->insert($data);
                        $hall_status_update = array('hall_status' => 'reserved');
                        $update_item = $this->halls_model->update_by(array('id' => '1'), $hall_status_update);
                        if ($insert && $update_item) {
                            send_message("", '200', 'insert_success');
                        } else {
                            error_message('400', 'insert_error');
                        }
                    }
                } // if hall status is not equal to reserved this main the honored order is done and hall is reserved
                else {
                    $data['order_status'] = 'reservation_done';
                    $insert = $this->honored_orders_model->insert($data);
                    $hall_status_update = array('hall_status' => 'reserved');
                    $update_item = $this->halls_model->update_by(array('id' => '1'), $hall_status_update);
                    if ($insert && $update_item) {
                        send_message("", '200', 'insert_success');
                    } else {
                        error_message('400', 'insert_error');
                    }
                }
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /honored_orders/get_honored_order get_honored_order
     * @apiName get_honored_order
     * @apiGroup honored_orders
     * @apiDescription get id of honored order to get its info from honored_orders database table
     * @apiParam {Number} id id of honored order
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of honored order from honored orders table
     * @apiError  FALSE return false if failure in get data
     */
    function get_honored_order($id) {
        $data = $this->honored_orders_model->get_by('id', $id);
        $data->honored_date = date("m-d-Y", strtotime($data->honored_date));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /honored_orders/update_honored_order update_honored_order
     * @apiName update_honored_order
     * @apiGroup honored_orders
     * @apiDescription update honored order data in honored_orders database table
     * @apiParam {Number} teacher_id id of teacher who add honored order
     * @apiParam {String} order_title Title of honored order
     * @apiParam {Number} stage_id id of stage (select from dropdown list)
     * @apiParam {Date} honored_date Date of honored order
     * @apiParam {String} honored_reason Reason of honored order
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_honored_order() {
        $honored_orders = $this->honored_orders_model->get_all();

        $input_array = array(
            'order_title' => "required",
            'stage_id' => "required",
//            'honored_date' => "required",
            'honored_reason' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $order_title = $this->input->post('order_title');
            $stage_id = $this->input->post('stage_id');
//            $honored_date = $this->input->post('honored_date');
            $honored_reason = $this->input->post('honored_reason');

            // start validation
//            if (exist_item("honored_orders_model", array('order_title' => $order_title, 'id <>' => $item_id))) {
//                send_message('', "202", 'order_title_already_exist');
//            }

            validation_edit_delete_redirect($honored_orders, "id", $item_id);
            // end validation

            $data = array(
                'order_title' => $order_title,
                'stage_id' => $stage_id,
//                'honored_date' => $honored_date,
                'honored_reason' => $honored_reason,
            );

            $update_item = $this->honored_orders_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /honored_orders/delete_honored_order delete_honored_order
     * @apiName delete_honored_order
     * @apiGroup honored_orders
     * @apiDescription get id of honored order to delete its info from honored_orders database table
     * @apiParam {Number} id id of honored order
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_honored_order($id) {
        $honored_orders = $this->honored_orders_model->get_all();
        //start validation
        validation_edit_delete_redirect($honored_orders, "id", $id);
        // end validation
        $delete_item = $this->honored_orders_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /honored_orders/index_honored_orders_for_admin index_honored_orders_for_admin
     * @apiName index_honored_orders_for_admin
     * @apiGroup honored_orders
     * @apiDescription home page of honored orders in admin account 
     */
    function index_honored_orders_for_admin() {
        $stages = get_stages();
        $stages_options = array();
        $stages_options['0'] = lang('select_stage_name');

        foreach ($stages as $key => $value) {
            $stages_options[$key] = $value;
        }

        $this->data['stages_options'] = $stages_options;
        $this->data['show_object_link'] = 'honored_orders/show_honored_orders_for_admin';
        $this->data['get_object_link'] = 'honored_orders/get_honored_order_for_admin';
        $this->data['add_object_link'] = 'honored_orders/add_honored_orders_for_admin';
        $this->data['update_object_link'] = 'honored_orders/update_honored_order_for_admin';
        $this->data['delete_object_link'] = 'honored_orders/delete_honored_order_for_admin';
        $this->data['modal_name'] = 'crud_honored_orders_for_admin';
        $this->data['add_object_title'] = lang("add_new_honored_order");
        $this->data['update_object_title'] = lang("update_honored_order");
        $this->data['delete_object_title'] = lang("delete_honored_order");
        $this->data['thead'] = array('teacher_id', 'order_title', 'stage_id', 'honored_order_created_date', 'honored_date', 'order_status', 'honored_done', 'honored_students');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        // put the title whose don't apear in print and excel
        $this->data['non_printable']['honored_students'] = 'honored_students';
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /honored_orders/show_honored_orders_for_admin show_honored_orders_for_admin
     * @apiName show_honored_orders_for_admin
     * @apiGroup honored_orders
     * @apiDescription get all honored orders in admin account
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in honored_orders table
     * @apiError  FALSE return false if failure in get data
     */
    function show_honored_orders_for_admin() {
        $honored_orders = $this->honored_orders_model->get_honored_orders_for_admin();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($honored_orders as $honored_order) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_teacher_profile_for_another_account/" . $honored_order->teacher_id, $honored_order->first_name . ' ' . $honored_order->last_name, '', 'update');
//            $row[] = $honored_order->first_name . ' ' . $honored_order->father_name . ' ' . $honored_order->last_name;
            $row[] = $honored_order->order_title;
            $stage_id = $this->stages_model->get_stage($honored_order->stage_id);
            $row[] = $stage_id->stage_name;
            $row[] = date("m-d-Y H:i:s", strtotime($honored_order->created_at));
            $row[] = date("m-d-Y", strtotime($honored_order->honored_date));
            $row[] = lang($honored_order->order_status);
            $row[] = tinyint_lang_one_and_two($honored_order->honored_done);
            //add html for action
            $row[] = bs3_link_crud("honored_students/index_honored_students_for_admin/" . $honored_order->teacher_id . '/' . $honored_order->id, '<i class=" mdi mdi-account-star-variant fa-2x"></i>', lang('index_honored_students_for_admin'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($honored_order->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($honored_order->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->honored_orders_model->count_all_honored_orders_for_admin(),
            "recordsFiltered" => $this->honored_orders_model->count_filtered_crud_honored_orders_for_admin(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /honored_orders/get_honored_order_for_admin get_honored_order_for_admin
     * @apiName get_honored_order_for_admin
     * @apiGroup honored_orders
     * @apiDescription get id of honored order to get its info from honored_orders database table
     * @apiParam {Number} id id of honored order
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of honored order from honored orders table
     * @apiError  FALSE return false if failure in get data
     */
    function get_honored_order_for_admin($id) {
        $data = $this->honored_orders_model->get_by('id', $id);
        $data->honored_date = date("m-d-Y", strtotime($data->honored_date));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /honored_orders/update_honored_order_for_admin update_honored_order_for_admin
     * @apiName update_honored_order_for_admin
     * @apiGroup honored_orders
     * @apiDescription update honored order in admin account data in honored_orders database table
     * @apiParam {Number} id id of honored order
     * @apiParam {Boolean} honored_done status of honored
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     */
    function update_honored_order_for_admin() {
        $honored_orders = $this->honored_orders_model->get_all();

//        $input_array = array(
//            'honored_done' => "required",
//             'stage_name' => "required",
//        );
//        $this->_validation($input_array);
//        $honored_done = $this->input->post('honored_done');
//        if ($this->form_validation->run()) {
        $item_id = $this->input->post('id');
        $honored_done = $this->input->post('honored_done');
//        $honored_date = $this->input->post('honored_date');
//            $stage_name = $this->input->post('stage_name');
        // start validation
//            if (exist_item("honored_orders_model", array('order_title' => $order_title, 'id <>' => $item_id))) {
//                send_message('', "202", 'order_title_already_exist');
//            }

        validation_edit_delete_redirect($honored_orders, "id", $item_id);
        // end validation

        if ($honored_done != '' || $honored_done != 0 || $honored_done != NULL)
            $honored_done = 2;

        $data = array(
            'honored_done' => $honored_done,
//            'honored_date' => $honored_date,
            'order_status' => 'honored_done'
//                'stage_name' => $stage_name,
        );

        $update_item = $this->honored_orders_model->update_by(array('id' => $item_id), $data);
        // check if any teacher reserved room 

        if ($update_item) {
            // check if any reserve honored_order is exist
            $exist = $this->honored_orders_model->get_any_reserve_honored_order();
            // get pending order 
            $pending_honored_orders = $this->honored_orders_model->get_pending_honored_orders();
            //get first pending order 
            $first_pending_honored_order = $this->honored_orders_model->get_first_pending_honored_order();

            if (!isset($exist) && !$exist) {
                $data = array(
                    'order_status' => 'reservation_done',
//                    'honored_date' => $honored_date
                );
                if (isset($first_pending_honored_order) && $first_pending_honored_order) {
                    $update_order = $this->honored_orders_model->update_by(array('id' => $first_pending_honored_order->id), $data);
                    $update_id_order = $this->honored_orders_model->get_by('id', $first_pending_honored_order->id);

                    foreach ($pending_honored_orders as $value) {
                        if ($value->stage_id == $update_id_order->stage_id) {
                            $update_order = $this->honored_orders_model->update_by(array('id' => $first_pending_honored_order->id), $data);
                        }
                    }
                }
            }
            send_message("", '200', 'update_success');
        } else {
            error_message('400', 'update_error');
        }
//        } else {
//            send_message(validation_errors(), '201', 'validation_error');
//        }
    }

    /**
     * @api {post} /honored_orders/delete_honored_order_for_admin delete_honored_order_for_admin
     * @apiName delete_honored_order_for_admin
     * @apiGroup honored_orders
     * @apiDescription get id of honored order to delete its info from honored orders database table
     * @apiParam {Number} id id of honored order
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_honored_order_for_admin($id) {
        $honored_orders = $this->honored_orders_model->get_all();
        //start validation
        validation_edit_delete_redirect($honored_orders, "id", $id);
        // end validation
        $delete_item = $this->honored_orders_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
