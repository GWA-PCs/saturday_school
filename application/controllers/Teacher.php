<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('teachers_model');
        $this->load->model('ion_auth_model');

        $this->load->model('general_model', 'users_model');
        $this->users_model->set_table('users');

        $this->load->model('rooms_teachers_model');
        $this->load->model('rooms_model');
        $this->load->model('subjects_model');
        $this->load->model('meetings_model');
        $this->load->model('teachers_notes_model');
        $this->load->model('homeworks_model');

        $this->load->model('users_meetings_model');
    }

    function index() {
        
    }

    /**
     * @api {get} /teacher/index_teacher index_teacher
     * @apiName index_teacher
     * @apiGroup teacher
     * @apiDescription home page of teacher 
     */
    function index_teacher() {
        $this->data['non_encrept_pass'] = random_password();
        $this->data['show_object_link'] = 'teacher/show_teachers';
        $this->data['get_object_link'] = 'teacher/get_teacher';
        $this->data['add_object_link'] = 'teacher/add_teacher';
        $this->data['update_object_link'] = 'teacher/update_teacher';
        $this->data['delete_object_link'] = 'teacher/delete_teacher';
        $this->data['modal_name'] = 'crud_teacher';
        $this->data['add_object_title'] = lang("add_teacher");
        $this->data['update_object_title'] = lang("edit_teacher");
        $this->data['delete_object_title'] = lang("delete_teacher");
        $this->data['thead'] = array('first_name', 'last_name', 'mobile', 'assistant_teacher', 'index_teacher_note');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['index_teacher_note'] = 'index_teacher_note';
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /teacher/show_teachers show_teachers
     * @apiName show_teachers
     * @apiGroup teacher
     * @apiDescription get all teachers
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function show_teachers() {
        $teachers = $this->teachers_model->get_teachers();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($teachers as $teacher) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_teacher_profile_for_another_account/" . $teacher->id, $teacher->first_name, '', 'update');
            $row[] = bs3_link_crud("profile/page_teacher_profile_for_another_account/" . $teacher->id, $teacher->last_name, '', 'update');
            $row[] = $teacher->mobile;
            $row[] = tinyint_lang_one_and_two($teacher->assistant_teacher);
            $row[] = bs3_link_crud("teacher_note/index_teacher_note/" . $teacher->id, '<i class="mdi mdi-note-multiple fa-2x "></i>', lang('index_teacher_note'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($teacher->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($teacher->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->teachers_model->count_all_teachers(),
            "recordsFiltered" => $this->teachers_model->count_filtered_crud_teachers(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /teacher/get_teacher get_teacher
     * @apiName get_teacher
     * @apiGroup teacher
     * @apiDescription get id of teacher to get its info from teachers database table
     * @apiParam {Number} id id of teacher
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of parent from teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function get_teacher($id) {
        $data = $this->teachers_model->get_teacher_by_id($id);
        $data->birthdate = date("m-d-Y", strtotime($data->birthdate));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /teacher/add_teacher add_teacher
     * @apiName add_teacher
     * @apiGroup teacher
     * @apiDescription add teacher data to users and teachers database table
     * @apiParam {String} first_name first name
     * @apiParam {String} last_name last_ name
     * @apiParam {Date} birthdate DAte of birth
     * @apiParam {Number} mobile mobile
     * @apiParam {String} email email of teacher
     * @apiParam {String} address address of teacher
     * @apiParam {String} non_encrept_pass Password for email 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_teacher() {
        $tables = $this->config->item('tables', 'ion_auth');
        $input_array = array(
            'first_name' => "required",
            'last_name' => "required",
            'birthdate' => "required",
            'mobile' => "telephone",
            'email' => 'required|valid_email|is_unique[' . $tables['users'] . '.email]',
            'address' => "",
            'salary' => "numeric|greater_than_equal_to[0]",
            'non_encrept_pass' => 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']',
        );

        $this->_validation($input_array);

        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $birthdate = $this->input->post('birthdate');
        if ($birthdate)
            $birthdate = date_format(date_create_from_format('m-d-Y', $birthdate), 'Y-m-d');
        $mobile = $this->input->post('mobile');
        $address = $this->input->post('address');
//        $educational_attainment = $this->input->post('educational_attainment');
        $email = $this->input->post('email');
//        $salary = $this->input->post('salary');
//        $password = "password";
        $password = $this->input->post('non_encrept_pass');

        $assistant_teacher = 1;
        if ($this->input->post('assistant_teacher')) {
            $assistant_teacher = 2;
        }


        $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'birthdate' => $birthdate,
            'mobile' => $mobile,
            'address' => $address,
            'non_encrept_pass' => $password,
//            'educational_attainment' => $educational_attainment,
        );

        if ($this->form_validation->run()) {
            $type = 'teacher';

            $user_id = $this->ion_auth_model->register($email, $password, $email, $type, $user_data);
            if (!isset($user_id)) {
                error_message('400', 'insert_error');
            } else {
                //create employee
                $data = array(
                    'user_id' => $user_id,
//                    'salary' => $salary,
                    'assistant_teacher' => $assistant_teacher,
                );
                $insert = $this->teachers_model->insert($data);
                if ($insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /teacher/update_teacher update_teacher
     * @apiName update_teacher
     * @apiGroup teacher
     * @apiDescription update teacher data in teachers database table
     * @apiParam {String} first_name first name
     * @apiParam {String} last_name last_ name
     * @apiParam {Date} birthdate DAte of birth
     * @apiParam {Number} mobile mobile
     * @apiParam {String} email email of teacher
     * @apiParam {String} address address of teacher
     * @apiParam {String} non_encrept_pass Password for email 
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  email_already_exist400 return JSON encoded string contain this email is already exist error message
     */
    function update_teacher() {
//        $national_number = $this->input->post('national_number');
        $input_array = array(
//            'national_number' => "required|check_national_number[$national_number]",
            'first_name' => "required",
            'last_name' => "required",
            'birthdate' => "required",
            'mobile' => "telephone",
            'address' => "",
            'email' => 'required|valid_email',
            'non_encrept_pass' => 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']',
//            'educational_attainment' => "",
//            'salary' => "numeric",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $birthdate = $this->input->post('birthdate');
            if ($birthdate)
                $birthdate = date_format(date_create_from_format('m-d-Y', $birthdate), 'Y-m-d');
            $mobile = $this->input->post('mobile');
            $address = $this->input->post('address');
//            $educational_attainment = $this->input->post('educational_attainment');
            $email = $this->input->post('email');
            $non_encrept_pass = $this->input->post('non_encrept_pass');
            $password = $this->input->post('non_encrept_pass');
            $password = $this->ion_auth_model->hash_password($password, false);

            $assistant_teacher = 1;
            if ($this->input->post('assistant_teacher')) {
                $assistant_teacher = 2;
            }

//            if (exist_item("users_model", array('username' => $username, 'id <>' => $item_id))) {
//                send_message('', "202", 'username_already_exist');
//            }
//
            $teacher = $this->teachers_model->get_by('id', $item_id);
            if (exist_item("users_model", array('email' => $email, 'id <>' => $teacher->user_id))) {
                send_message('', "202", 'email_already_exist');
            }

            $user_data = array(
//                'national_number' => $national_number,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'birthdate' => $birthdate,
                'mobile' => $mobile,
                'address' => $address,
//                'educational_attainment' => $educational_attainment,
                'email' => $email,
                'username' => $email,
                'password' => $password,
                'non_encrept_pass' => $non_encrept_pass,
            );
            $teacher_data = array(
//                'salary' => $salary,
                'assistant_teacher' => $assistant_teacher,
            );


            $update_item = $this->teachers_model->update_teacher_by_id($item_id, $user_data, $teacher_data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /teacher/delete_teacher delete_teacher
     * @apiName delete_teacher
     * @apiGroup teacher
     * @apiDescription get id of teacher to delete its info from teachers database table
     * @apiParam {Number} id id of teacher
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_teacher($id) {
        $teachers = $this->teachers_model->get_all();
        //start validation
        if (!validation_edit_delete_redirect($teachers, "id", $id)) {
            error_message("401", 'error_in_information');
        }

        $related = $this->teachers_notes_model->get_by('teacher_id', $id);

        if (isset($related) && $related) {
            error_message('407', 'have_son_error');
        } else {
            $delete_item = $this->teachers_model->delete_teacher($id);
            if (isset($delete_item) && $delete_item) {
                send_message("", '200', 'delete_success');
            } else {
                error_message('400', 'delete_error');
            }
        }
    }

    /**
     * @api {get} /teacher/index_teacher_rooms index_teacher_rooms
     * @apiName index_teacher_rooms
     * @apiGroup teacher
     * @apiDescription home page of teacher rooms 
     */
    function index_teacher_rooms() {
        $this->data['show_object_link'] = 'teacher/show_teacher_rooms';
        $this->data['get_object_link'] = 'teacher/get_teacher_rooms';
        $this->data['add_object_link'] = 'teacher/add_teacher_rooms';
        $this->data['update_object_link'] = 'teacher/update_teacher_rooms';
        $this->data['delete_object_link'] = 'teacher/delete_teacher_rooms';
        $this->data['modal_name'] = 'crud_teacher_rooms';
        $this->data['add_object_title'] = lang("add_teacher_rooms");
        $this->data['update_object_title'] = lang("edit_teacher_rooms");
        $this->data['delete_object_title'] = lang("delete_teacher_rooms");
//        $this->data['thead'] = array('grade_name', 'room_name', 'subject_name', 'index_student_record','index_mark');
        $this->data['thead'] = array('room_name', 'grade_name', 'index_teacher_subjects', 'index_student_record', 'index_room_attendance', 'homeworks');

        $this->data['non_printable']['index_teacher_subjects'] = 'index_teacher_subjects';
        $this->data['non_printable']['index_student_record'] = 'index_student_record';
        $this->data['non_printable']['index_room_attendance'] = 'index_room_attendance';
    }

    /**
     * @api {get} /teacher/show_teacher_rooms show_teacher_rooms
     * @apiName show_teacher_rooms
     * @apiGroup teacher
     * @apiDescription get all rooms teacher =
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in teacher rooms table
     * @apiError  FALSE return false if failure in get data
     */
    function show_teacher_rooms() {
        $user_info = $this->_user_login;
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);

        $teacher_rooms = $this->rooms_teachers_model->get_teacher_room($teacher_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($teacher_rooms as $teacher_room) {
            $no++;
            $row = array();
            $row[] = $teacher_room->room_name;
            $row[] = $teacher_room->grade_name;
//            if ($teacher_room->parent_subject_id == 1)
//                $row[] = $teacher_room->subject_name;
//            else
//                $row[] = lang($teacher_room->parent_subject_name) . ' - ' . $teacher_room->subject_name;
            $row[] = bs3_link_crud("teacher/index_teacher_subjects/" . $teacher_room->id, '<i class=" mdi  mdi-book-open-page-variant fa-2x "></i>', lang('index_teacher_subjects'), 'update');
            $row[] = bs3_link_crud("teacher/index_teacher_student_record/" . $teacher_room->id, '<i class=" mdi  mdi-account-multiple-plus fa-2x "></i>', lang('index_student_record'), 'update');
            $row[] = bs3_link_crud("room_attendance/index_room_attendance/" . $teacher_room->id, '<i class="mdi mdi-account-multiple-minus fa-2x "></i>', lang('index_room_attendance'), 'update');
            $row[] = bs3_link_crud("teacher/homeworks/" . $teacher_room->id, '<i class="mdi mdi-book-multiple-variant fa-2x "></i>', lang('homeworks'), 'update');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->rooms_teachers_model->count_all_teacher_room($teacher_id),
            "recordsFiltered" => $this->rooms_teachers_model->count_filtered_crud_teacher_room($teacher_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /teacher/index_teacher_student_record index_teacher_student_record
     * @apiName index_teacher_student_record
     * @apiGroup teacher
     * @apiDescription home page of students records in teacher study plane
     * @apiParam {Number} room_id id of room
     */
    function index_teacher_student_record($room_id) {
        $this->data['room_name'] = $this->rooms_model->get_room($room_id);
        $this->data['show_object_link'] = 'teacher/show_teacher_student_record/' . $room_id;
        $this->data['get_object_link'] = 'teacher/get_student_record/' . $room_id;
//        $this->data['permissions_link'] = 'student_record/permission_student_record';
        $this->data['add_object_link'] = 'teacher/add_student_record/' . $room_id;
        $this->data['update_object_link'] = 'teacher/update_student_record/' . $room_id;
        $this->data['delete_object_link'] = 'teacher/delete_student_record/' . $room_id;
        $this->data['modal_name'] = 'crud_student_records';
        $this->data['add_object_title'] = lang("add_new_student_record");
        $this->data['update_object_title'] = lang("update_student_record");
        $this->data['delete_object_title'] = lang("delete_student_record");
        $this->data['thead'] = array('student_name', 'index_student_note', 'index_student_attendance', 'index_general_social_development');

        $this->data['non_printable']['index_student_note'] = 'index_student_note';
        $this->data['non_printable']['index_student_attendance'] = 'index_student_attendance';
    }

    /**
     * @api {get} /teacher/show_teacher_student_record show_teacher_student_record
     * @apiName show_rooms_teachers
     * @apiGroup teacher
     * @apiDescription get all students records in teacher study plane
     * @apiParam {Number} room_id id of room
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in rooms teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function show_teacher_student_record($room_id) {
        $student_records = $this->students_records_model->get_students_records($room_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($student_records as $student_record) {
            $no++;
            $row = array();
//            $row[] = $student_record->student_name;
//            $row[] = $student_record->family_name;
            $row[] = bs3_link_crud("profile/page_student_profile/" . $student_record->student_id, $student_record->student_name . ' ' . $student_record->family_name, '', 'update');
            $row[] = bs3_link_crud("student_note/index_student_note/" . $student_record->id, '<i class="mdi mdi-note-multiple fa-2x "></i>', lang('index_student_note'), 'update');
            $row[] = bs3_link_crud("room_attendance/index_student_attendance/" . $student_record->student_id, '<i class="mdi mdi-account-minus fa-2x "></i>', lang('index_student_attendance'), 'update');
            $row[] = bs3_link_crud("student_record/index_general_social_development/" . $student_record->id, '<i class=" mdi mdi-account-card-details fa-2x "></i>', lang('index_general_social_development'), 'update');

            //add html for action

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_records_model->count_all_students_records($room_id),
            "recordsFiltered" => $this->students_records_model->count_filtered_crud_students_records($room_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /teacher/index_teacher_subjects index_teacher_subjects
     * @apiName index_teacher_subjects
     * @apiGroup teacher
     * @apiDescription home page of teacher subjects for specific room and teacher
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} teacher_id id of teacher
     */
    function index_teacher_subjects($room_id, $teacher_id = '') {
        $this->data['one_column'] = true;

        $this->data['room_name'] = $this->rooms_model->get_room($room_id);
        if ($teacher_id == '') {
            $this->data['show_object_link'] = 'teacher/show_teacher_subjects/' . $room_id;
        } else {
            $this->data['show_object_link'] = 'teacher/show_teacher_subjects/' . $room_id . '/' . $teacher_id;
        }
        $this->data['get_object_link'] = 'teacher/get_teacher_subjects/' . $room_id;
        $this->data['add_object_link'] = 'teacher/add_teacher_subjects/' . $room_id;
        $this->data['update_object_link'] = 'teacher/update_teacher_subjects/' . $room_id;
        $this->data['delete_object_link'] = 'teacher/delete_teacher_subjects/' . $room_id;
        $this->data['modal_name'] = 'crud_teacher_subjects';
        $this->data['add_object_title'] = lang("add_teacher_subjects");
        $this->data['update_object_title'] = lang("update_teacher_subjects");
        $this->data['delete_object_title'] = lang("delete_teacher_subjects");
        $this->data['thead'] = array('subject_name', 'student_mark_s1', 'student_mark_s2');

//        $this->data['non_printable']['hidden'] = 'hidden';
        $this->data['non_printable']['student_mark_s1'] = 'student_mark_s1';
        $this->data['non_printable']['student_mark_s2'] = 'student_mark_s2';
    }

    /**
     * @api {get} /teacher/show_teacher_subjects show_teacher_subjects
     * @apiName show_teacher_subjects
     * @apiGroup teacher
     * @apiDescription get all subjects in teacher study plane
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} teacher_id id of teacher
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in rooms teacher subjects table
     * @apiError  FALSE return false if failure in get data
     */
    function show_teacher_subjects($room_id, $teacher_id = '') {
        if ($teacher_id == '') {
            $user_info = $this->_user_login;
            $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);
            $teacher_subjects = $this->rooms_teachers_model->get_teacher_subjects($teacher_id, $room_id);
        } else {
            $teacher_subjects = $this->rooms_teachers_model->get_teacher_subjects($teacher_id, $room_id);
        }

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($teacher_subjects as $teacher_subject) {
            $no++;
            $row = array();
//            $row[] = '';
            $row[] = $teacher_subject->subject_name;
            $row[] = bs3_link_crud("student_mark/index_student_mark/" . 'first' . '/' . $room_id . '/' . $teacher_subject->subject_id, '<i class="mdi mdi-note-multiple fa-2x "></i>', lang('student_mark_s1'), 'update');
            $row[] = bs3_link_crud("student_mark/index_student_mark/" . 'second' . '/' . $room_id . '/' . $teacher_subject->subject_id, '<i class="mdi mdi-note-multiple fa-2x "></i>', lang('student_mark_s2'), 'update');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->rooms_teachers_model->count_all_teacher_subjects($teacher_id, $room_id),
            "recordsFiltered" => $this->rooms_teachers_model->count_filtered_crud_teacher_subjects($teacher_id, $room_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /teacher/index_teacher_meetings index_teacher_meetings
     * @apiName index_teacher_meetings
     * @apiGroup teacher
     * @apiDescription home page of meetings should be show in teacher account 
     */
    function index_teacher_meetings() {
        $this->data['show_object_link'] = 'teacher/show_teacher_meetings';
//        $this->data['get_object_link_meeting'] = 'meeting/get_meeting';
//        $this->data['add_object_link'] = 'meeting/add_meeting';
//        $this->data['update_object_link'] = 'meeting/update_meeting';
//        $this->data['delete_object_link'] = 'meeting/delete_meeting';
//        $this->data['modal_name'] = 'crud_meetings';
//        $this->data['add_object_title'] = lang("add_new_meeting");
//        $this->data['update_object_title'] = lang("update_meeting");
//        $this->data['delete_object_title'] = lang("delete_meeting");
        $this->data['thead'] = array('meeting_title', 'meeting_date', 'meeting_file');
    }

    /**
     * @api {get} /teacher/show_teacher_meetings show_teacher_meetings
     * @apiName show_teacher_meetings
     * @apiGroup teacher
     * @apiDescription get all meetings should be show in teacher account 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in meetings table
     * @apiError  FALSE return false if failure in get data
     */
    function show_teacher_meetings() {
        $meetings = $this->meetings_model->get_user_meeting($this->_user_login->id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($meetings as $meeting) {
            $no++;
            $row = array();
            $row[] = $meeting->meeting_title;
            $row[] = date("m-d-Y", strtotime($meeting->meeting_date));

            //add html for action
            $row[] = bs3_file_download("assets/uploads/meeting_file/" . $meeting->meeting_file, $meeting->meeting_file);

            $data[] = $row;
        }

        // var_dump($this->_user_login->id);
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->users_meetings_model->count_all_teacher_meeting($this->_user_login->id),
            "recordsFiltered" => $this->users_meetings_model->count_filtered_crud_teacher_meeting($this->_user_login->id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /teacher/get_teacher_subjects get_teacher_subjects
     * @apiName get_teacher_subjects
     * @apiGroup teacher
     * @apiDescription get id of room and teacher to get its subjects from subjects database table
     * @apiParam {Number} room_id id of room 
     * @apiParam {Number} teacher_id id of teacher
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of subject from subjects table
     * @apiError  FALSE return false if failure in get data
     */
    function get_teacher_subjects($room_id, $teacher_id) {
        $teacher_subjects = $this->subjects_model->get_teacher_subjects($teacher_id, $room_id);
        send_message($teacher_subjects);
    }

    /**
     * @api {get} /teacher/index_assign_teacher_to_grade index_assign_teacher_to_grade
     * @apiName index_assign_teacher_to_grade
     * @apiGroup teacher
     * @apiDescription home page of assign teacher to grade 
     */
    function index_assign_teacher_to_grade() {
        $grades = $this->grades_model->get_grades();

        $grades_options = array('000' => lang('select_grade_name'));
        foreach ($grades as $value) {
            $grades_options[$value->id] = $value->grade_name;
        }
        $rooms = $this->rooms_model->get_all();
        $subjects = $this->subjects_model->get_all();

        $this->data['add'] = TRUE;
        $this->data['grades_options'] = $grades_options;
        $this->data['rooms_options'] = $rooms;
        $this->data['subjects_options'] = $subjects;

        $this->data['show_object_link'] = 'teacher/show_assign_teacher_to_grade';
        $this->data['get_object_link'] = 'teacher/get_assign_teacher_to_grade';
        $this->data['add_object_link'] = 'teacher/add_assign_teacher_to_grade';
        $this->data['update_object_link'] = 'teacher/update_assign_teacher_to_grade';
        $this->data['delete_object_link'] = 'teacher/delete_assign_teacher_to_grade';
        $this->data['modal_name'] = 'crud_assign_teacher_to_grade';
        $this->data['add_object_title'] = lang("assign_new_teacher_to_grade");
        $this->data['update_object_title'] = lang("update_assign_teacher_to_grade");
        $this->data['delete_object_title'] = lang("delete_assign_teacher_to_grade");
        $this->data['thead'] = array('teacher_name', 'grade_name', 'room_name', 'subject_name');

        if ($this->_current_year == $this->_archive_year) {
//            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['delete'] = 'delete';
//        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /teacher/show_assign_teacher_to_grade show_assign_teacher_to_grade
     * @apiName show_assign_teacher_to_grade
     * @apiGroup teacher
     * @apiDescription home page of assign teacher to grade 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of teacher from rooms teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function show_assign_teacher_to_grade() {
        $room_teachers = $this->rooms_teachers_model->get_all_rooms_teachers_for_all_room();
        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }

        foreach ($room_teachers as $teacher) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_teacher_profile_for_another_account/" . $teacher->teacher_id, $teacher->first_name . ' ' . $teacher->last_name, '', 'update');
            $row[] = $teacher->grade_name;
            $row[] = $teacher->room_name;
            if ($teacher->parent_subject_id == 1)
                $row[] = $teacher->subject_name;
            else
                $row[] = lang($teacher->parent_subject_name) . ' - ' . $teacher->subject_name;
            //add html for action
            if ($this->_current_year == $this->_archive_year) {
//                $row[] = bs3_update_delete_crud($teacher->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($teacher->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->rooms_teachers_model->count_all_rooms_teachers_for_all_room(),
            "recordsFiltered" => $this->rooms_teachers_model->count_filtered_crud_rooms_teachers_for_all_room(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /teacher/add_assign_teacher_to_grade add_assign_teacher_to_grade
     * @apiName add_assign_teacher_to_grade
     * @apiGroup teacher
     * @apiDescription add teacher data to rooms teachers database table
     * @apiParam {Number} teacher_id id of teacher
     * @apiParam {Number} teacher_key id of teacher
     * @apiParam {Number} grade_id id of grade
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} subject_id id of subject
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  Room_name_already_exist202 return room name already exist error message

     */
    function add_assign_teacher_to_grade() {
        $input_array = array(
            'teacher_id' => "required",
            'grade_id' => "required",
            'room_id' => "required",
            'subject_id' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $teacher_id = $this->input->post('teacher_key');
            $subject_id = $this->input->post('subject_id');
            $grade_id = $this->input->post('grade_id');
            $room_id = $this->input->post('room_id');

            if ($teacher_id != 0) {
                $data = array(
                    'room_id' => $room_id,
                    'teacher_id' => $teacher_id,
                    'subject_id' => $subject_id,
                    'year' => $this->_current_year,
                );
                if (exist_item("rooms_teachers_model", $data)) {
                    send_message("", '202', 'room_name_already_exist');
                }

                $insert = $this->rooms_teachers_model->insert($data);
                if ($insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            } else {
                error_message('400', 'insert_room_teacher_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /teacher/get_assign_teacher_to_grade get_assign_teacher_to_grade
     * @apiName get_assign_teacher_to_grade
     * @apiGroup teacher
     * @apiDescription get id of room teacher to get its info from rooms teachers database table
     * @apiParam {Number} id id of room teacher
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of room teacher from rooms teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function get_assign_teacher_to_grade($id = '') {
        $data = $this->rooms_teachers_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /teacher/update_assign_teacher_to_grade update_assign_teacher_to_grade
     * @apiName update_assign_teacher_to_grade
     * @apiGroup teacher
     * @apiDescription update room teacher data in rooms teachers database table
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} subject_id id of subject
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_assign_teacher_to_grade() {
        $rooms_teachers = $this->rooms_teachers_model->get_all_rooms_teachers_for_all_room();

        $input_array = array(
            'subject_id' => "required",
            'room_id' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $subject_id = $this->input->post('subject_id');
            $room_id = $this->input->post('room_id');

            $data = array(
                'room_id' => $room_id,
                'subject_id' => $subject_id,
                'id <>' => $item_id
            );

            validation_edit_delete_redirect($rooms_teachers, "id", $item_id);
            // end validation

            $data = array(
                'room_id' => $room_id,
                'subject_id' => $subject_id,
            );
            $update_item = $this->rooms_teachers_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /teacher/delete_assign_teacher_to_grade delete_assign_teacher_to_grade
     * @apiName delete_assign_teacher_to_grade
     * @apiGroup teacher
     * @apiDescription get id of room teacher to delete its info from rooms teachers database table
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} id id of room teacher 
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_assign_teacher_to_grade($id = '') {
        $rooms_teachers = $this->rooms_teachers_model->get_all_rooms_teachers_for_all_room();
        //start validation
        validation_edit_delete_redirect($rooms_teachers, "id", $id);
        // end validation
        $delete_item = $this->rooms_teachers_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    public function homeworks($room_id) {
        // get all subject for this room and this teacher

        $user_info = $this->_user_login;
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);
        $teacher_subjects = $this->rooms_teachers_model->get_teacher_subjects($teacher_id, $room_id);

        $teacher_subjects_array = array();
        $teacher_subjects_array[] = lang('select_subject_name');
        foreach ($teacher_subjects as $value) {
            $teacher_subjects_array[$value->id] = $value->subject_name;
        }
        $this->data['teacher_subjects'] = $teacher_subjects_array;

        $this->data['show_object_link'] = 'teacher/show_homeworks/' . $room_id;

        $this->data['add_object_link'] = "teacher/add_homework";
        $this->data['update_object_link'] = "teacher/update_homework";
        $this->data['delete_object_link'] = 'teacher/delete_homework';

        $this->data['modal_name'] = 'add_homework';
        $this->data['page_title'] = lang("homeworks");
        $this->data['add_object_title'] = lang("add_homework");
        $this->data['update_object_title'] = lang("edit_homework");
        $this->data['delete_object_title'] = lang("delete_homework");
        $this->data['room_id'] = $room_id;
        $this->data['thead'] = ["subject_name", "title", "deadline", "homeworks_attachment"];


        if ($this->_current_year == $this->_archive_year) {
//            $this->data['thead'][] = 'update';
//            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['homeworks_attachment'] = 'homeworks_attachment';
//        $this->data['non_printable']['delete'] = 'delete';
//        $this->data['non_printable']['update'] = 'update';
    }

    public function show_homeworks($room_id) {
        $user_info = $this->_user_login;
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);
        $homeworks = $this->homeworks_model->get_homeworks($room_id, $teacher_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($homeworks as $homework) {
            $no++;
            $row = array();
            $row[] = $homework->subject_name;
            $row[] = $homework->title;
            $row[] = date("m-d-Y", strtotime($homework->deadline));
            $row[] = bs3_file_download("assets/uploads/homeworks_attachment/" . $homework->attach, $homework->attach);

            $data[] = $row;
        }
        $output = array(
            "data" => $data,
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->homeworks_model->count_all_homeworks($room_id, $teacher_id),
            "recordsFiltered" => $this->homeworks_model->count_filtered_crud_homeworks($room_id, $teacher_id),
        );
        //output to json format
        echo json_encode($output);
        die;
    }

    public function add_homework() {
        $input_array = array(
            'title' => "required",
            'msg_content' => "required",
            'room_teacher_id' => "required",
            'deadline' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $deadline = $this->input->post("deadline");
            if ($deadline)
                $deadline = date_format(date_create_from_format('m-d-Y', $deadline), 'Y-m-d');

            $title = $this->input->post("title");
            $room_teacher_id = $this->input->post("room_teacher_id");
            $text = $this->input->post("msg_content");
            $attach = file_or_image('homeworks_attachment', 'homeworks_attachment');

            if ($attach == '0') {
                error_message('400', 'insert_error_file_size_should_be_longer_than_zero');
            }

            $data = array(
                "title" => $title,
                "text" => $text,
                "room_teacher_id" => $room_teacher_id,
                "attach" => $attach,
                "deadline" => $deadline,
            );
            $insert = $this->homeworks_model->insert($data);

            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

}
