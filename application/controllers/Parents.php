<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('parents_model');
        $this->load->model('students_model');
        $this->load->model('ion_auth_model');

        $this->load->model('general_model', 'users_model');
        $this->users_model->set_table('users');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    }

    function index() {
        
    }

    /**
     * @api {get} /parents/index_parent index_parent
     * @apiName index_parent
     * @apiGroup parents
     * @apiDescription home page of parents in admin account
     */
    function index_parent() {
        $this->data['non_encrept_pass'] = random_password();
        $this->data['show_object_link'] = 'parents/show_parents';
        $this->data['get_object_link'] = 'parents/get_parent';
        $this->data['add_object_link'] = 'parents/add_parent';
        $this->data['update_object_link'] = 'parents/update_parent';
        $this->data['delete_object_link'] = 'parents/delete_parent';
        $this->data['modal_name'] = 'crud_parent';
        $this->data['add_object_title'] = lang("add_new_parent");
        $this->data['update_object_title'] = lang("update_parent");
        $this->data['delete_object_title'] = lang("delete_parent");
        $this->data['thead'] = array('family_name', 'father_name', 'mother_name', 'email', 'index_emergency_contact', 'index_parent_children');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['index_emergency_contact'] = 'index_emergency_contact';
        $this->data['non_printable']['index_parent_children'] = 'index_parent_children';
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /parents/show_parents show_parents
     * @apiName show_parents
     * @apiGroup parents
     * @apiDescription get all parents
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in parents table
     * @apiError  FALSE return false if failure in get data
     */
    function show_parents() {
        $parents = $this->parents_model->get_parents();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($parents as $parent) {
            $no++;
            $row = array();
            $row[] = bs3_link_crud("profile/page_parent_profile/" . $parent->user_id, $parent->family_name, '', 'update');

            $row[] = $parent->father_name;
            $row[] = $parent->mother_name;
            $row[] = $parent->email;
//add html for action

            $row[] = bs3_link_crud("parents/index_emergency_contact/" . $parent->id, '<i class="mdi mdi-phone-in-talk fa-2x "></i>', lang('index_emergency_contact'), 'update');

            $row[] = bs3_link_crud("parents/index_parent_children/" . $parent->id, '<i class="mdi  mdi-human-child fa-2x "></i>', lang('index_parent_children'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($parent->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($parent->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->parents_model->count_all_parents(),
            "recordsFiltered" => $this->parents_model->count_filtered_crud_parents(),
            "data" => $data,
        );

//output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /parents/get_parent get_parent
     * @apiName get_parent
     * @apiGroup parents
     * @apiDescription get id of parent to get its info from parents database table
     * @apiParam {Number} id id of parent
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of parent from parents table
     * @apiError  FALSE return false if failure in get data
     */
    function get_parent($id) {
        $data = $this->parents_model->get_parent_by_id($id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /parents/add_parent add_parent
     * @apiName add_parent
     * @apiGroup parents
     * @apiDescription add parent data to users and parents database table
     * @apiParam {String} family_name Name of family
     * @apiParam {String} father_name Name of father
     * @apiParam {String} mother_name Name of mother
     * @apiParam {Number} father_phone Phone of father
     * @apiParam {Number} mother_phone Phone of mother
     * @apiParam {Number} home_phone Phone of Home
     * @apiParam {String} email First email of parent
     * @apiParam {String} email Second email of parent
     * @apiParam {String} address address of parent
     * @apiParam {String} non_encrept_pass Password for email 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  this_parent_is_already_exist400 return JSON encoded string contain this parent is already exist error message
     */
    function add_parent() {
        $tables = $this->config->item('tables', 'ion_auth');

        $input_array = array(
            'family_name' => "required",
            'father_name' => "required",
            'mother_name' => "required",
            'father_phone' => "telephone",
            'mother_phone' => "telephone",
            'home_phone' => "telephone",
//            'balance' => "numeric|greater_than_equal_to[0]",
            'email' => 'required|valid_email|is_unique[' . $tables['users'] . '.email]|is_unique[' . $tables['users'] . '.email2]',
            'email2' => 'valid_email|is_unique[' . $tables['users'] . '.email]|is_unique[' . $tables['users'] . '.email2]',
            'address' => "",
            'non_encrept_pass' => 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']',
        );

        $this->_validation($input_array);

        $family_name = $this->input->post('family_name');
        $father_name = $this->input->post('father_name');
        $mother_name = $this->input->post('mother_name');
        $father_phone = $this->input->post('father_phone');
        $mother_phone = $this->input->post('mother_phone');
        $home_phone = $this->input->post('home_phone');
//        $balance = $this->input->post('balance');
        $address = $this->input->post('address');
        $email = $this->input->post('email');
        $email2 = $this->input->post('email2');
        $password = $this->input->post('non_encrept_pass');

        $user_data = array(
            'father_name' => $father_name,
            'mother_name' => $mother_name,
            'address' => $address,
            'non_encrept_pass' => $password,
            'email2' => $email2
        );

        if ($this->form_validation->run()) {
            $type = 'parent';
            $parent_data = array(
                'family_name' => $family_name,
                'father_name' => $father_name,
                'mother_name' => $mother_name,
            );
            $this->db->join('users', 'users.id=parents.user_id');
            $exist = $this->parents_model->get_by($parent_data);

            if ($exist) {
                error_message('400', 'this_parent_is_already_exist');
            }
            $user_id = $this->ion_auth_model->register($email, $password, $email, $type, $user_data);

            if (!isset($user_id)) {
                error_message('400', 'insert_error');
            } else {
                $data = array(
                    'user_id' => $user_id,
                    'family_name' => $family_name,
                    'father_phone' => $father_phone,
                    'mother_phone' => $mother_phone,
                    'home_phone' => $home_phone,
//                    'balance' => $balance,
                );
                $insert = $this->parents_model->insert($data);
                if ($insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /parents/update_parent update_parent
     * @apiName update_parent
     * @apiGroup parents
     * @apiDescription update parent data in parents database table
     * @apiParam {String} family_name Name of family
     * @apiParam {String} father_name Name of father
     * @apiParam {String} mother_name Name of mother
     * @apiParam {Number} father_phone Phone of father
     * @apiParam {Number} mother_phone Phone of mother
     * @apiParam {Number} home_phone Phone of Home
     * @apiParam {String} email First email of parent
     * @apiParam {String} email Second email of parent
     * @apiParam {String} address address of parent
     * @apiParam {String} non_encrept_pass Password for email 
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  email_already_exist400 return JSON encoded string contain this email is already exist error message
     */
    function update_parent() {
        $input_array = array(
            'family_name' => "required",
            'father_name' => "required",
            'mother_name' => "required",
            'father_phone' => "telephone",
            'mother_phone' => "telephone",
            'home_phone' => "telephone",
//            'balance' => "numeric",
            'email' => 'required|valid_email',
            'email2' => 'valid_email',
            'non_encrept_pass' => 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']',
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $family_name = $this->input->post('family_name');
            $father_name = $this->input->post('father_name');
            $mother_name = $this->input->post('mother_name');
            $father_phone = $this->input->post('father_phone');
            $mother_phone = $this->input->post('mother_phone');
            $home_phone = $this->input->post('home_phone');
//            $balance = $this->input->post('balance');
            $address = $this->input->post('address');
            $email = $this->input->post('email');
            $email2 = $this->input->post('email2');
            $non_encrept_pass = $this->input->post('non_encrept_pass');
            $password = $this->input->post('non_encrept_pass');
            $password = $this->ion_auth_model->hash_password($password, false);

            $parent_user_id = $this->parents_model->get_user_id_by_parent_id($item_id);

            if (exist_item("users_model", array('email' => $email, 'id <>' => $parent_user_id))) {
                send_message('', "202", 'email_already_exist');
            }

            if (exist_item("users_model", array('email2' => $email, 'id <>' => $parent_user_id))) {
                send_message('', "202", 'email_already_exist');
            }

            if (isset($email2) && $email2 && exist_item("users_model", array('email' => $email2, 'id <>' => $parent_user_id))) {
                send_message('', "202", 'email_already_exist');
            }

            if (isset($email2) && $email2 && exist_item("users_model", array('email2' => $email2, 'id <>' => $parent_user_id))) {
                send_message('', "202", 'email_already_exist');
            }

            $user_data = array(
                'father_name' => $father_name,
                'mother_name' => $mother_name,
                'address' => $address,
                'email' => $email,
                'email2' => $email2,
                'username' => $email,
                'password' => $password,
                'non_encrept_pass' => $non_encrept_pass,
            );

            $parent_data = array(
                'family_name' => $family_name,
                'father_phone' => $father_phone,
                'mother_phone' => $mother_phone,
                'home_phone' => $home_phone,
//                'balance' => $balance,
            );

            $update_item = $this->parents_model->update_parent_by_id($item_id, $user_data, $parent_data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /parent/delete_parent delete_parent
     * @apiName delete_parent
     * @apiGroup parent
     * @apiDescription get id of parent to delete its info from parents database table
     * @apiParam {Number} id id of parent
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     * @apiError  Have_son_error407 return JSON encoded string contain have son error message
     */
    function delete_parent($id) {
        $parents = $this->parents_model->get_all();
//start validation
        if (!validation_edit_delete_redirect($parents, "id", $id)) {
            error_message("401", 'error_in_information');
        }
// end validation
// check if i can delete parent 
        $related = $this->parents_model->get_student_parent($id);

        if (isset($related) && $related) {
            error_message('407', 'have_son_error');
        } else {
            $delete_item = $this->parents_model->delete_parent($id);
            if (isset($delete_item) && $delete_item) {
                send_message("", '200', 'delete_success');
            } else {
                error_message('400', 'delete_error');
            }
        }
    }

    /**
     * @api {get} /parents/index_parent_children index_parent_children
     * @apiName index_parent_children
     * @apiGroup parents
     * @apiDescription home page of parent children
     * @apiParam {Number} parent_id id of parent
     */
    function index_parent_children($parent_id = '') {
        if ($parent_id == '') {
            $user = $this->ion_auth->user()->row();
            $user_id = $user->id;
            $results = $this->parents_model->get_parent_by_user_id($user_id);
            $parent_id = $results->id;
        }
        $this->data['show_object_link'] = 'parents/show_parent_children/' . $parent_id;
        $this->data['get_object_link'] = 'parents/get_parent_children/' . $parent_id;
        $this->data['add_object_link'] = 'parents/add_parent_children/' . $parent_id;
        $this->data['update_object_link'] = 'parents/update_parent_children';
        $this->data['delete_object_link'] = 'parents/delete_parent_children';
        $this->data['modal_name'] = 'crud_parent_children';
        $this->data['add_object_title'] = lang("add_new_parent_children");
        $this->data['update_object_title'] = lang("update_parent_children");
        $this->data['delete_object_title'] = lang("delete_parent_children");
        $this->data['thead'] = array('student_name', 'index_student_payment',);

        $this->data['non_printable']['index_student_payment'] = 'index_student_payment';
    }

    /**
     * @api {get} /parents/show_parent_children show_parent_children
     * @apiName show_parent_children
     * @apiGroup parents
     * @apiDescription get all children parents
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in parent children table
     * @apiError  FALSE return false if failure in get data
     */
    function show_parent_children($parent_id, $only_attendence = '') {
        $parents = $this->students_model->get_parent_children($parent_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($parents as $parent) {
            $no++;
            $row = array();
//add html for action
            $row[] = bs3_link_crud("profile/page_student_profile/" . $parent->student_id, $parent->student_name . ' ' . $parent->family_name, '', 'update');
            if ($only_attendence == '') {
                $row[] = bs3_link_crud("student_payment/index_student_payment/" . $parent->student_id, '<i class="fa fa-money fa-2x "></i>', lang('index_student_payment'), 'update');
            }
            $row[] = bs3_link_crud("room_attendance/index_student_attendance/" . $parent->student_id, '<i class="mdi mdi-account-minus fa-2x "></i>', lang('index_student_attendance'), 'update');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_model->count_all_parent_children($parent_id),
            "recordsFiltered" => $this->students_model->count_filtered_crud_parent_children($parent_id),
            "data" => $data,
        );

//output to json format
        echo json_encode($output);
        die;
    }

    function index_emergency_contact($parent_id) {
        $emergency_contact = $this->parents_model->get_parent_by_id($parent_id);

        $emergency_contact_options = array(
            'no' => lang('no'),
            'yes' => lang('yes'),
        );
        $this->data['emergency_contact_options'] = $emergency_contact_options;
        $this->data['emergency_contact'] = $emergency_contact;
        $this->data['parent_id'] = $parent_id;
    }

    function add_emergency_contact($parent_id) {
        $input_array = array(
            'emergency_phone_1' => "telephone",
            'emergency_phone_2' => "telephone",
            'emergency_phone_3' => "telephone",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $update_item = FALSE;

            $emergency_name_1 = $this->input->post('emergency_name_1');
            $emergency_name_2 = $this->input->post('emergency_name_2');
            $emergency_name_3 = $this->input->post('emergency_name_3');

            $emergency_phone_1 = $this->input->post('emergency_phone_1');
            $emergency_phone_2 = $this->input->post('emergency_phone_2');
            $emergency_phone_3 = $this->input->post('emergency_phone_3');

            $emergency_relationship_1 = $this->input->post('emergency_relationship_1');
            $emergency_relationship_2 = $this->input->post('emergency_relationship_2');
            $emergency_relationship_3 = $this->input->post('emergency_relationship_3');

            $custody_info_specify = $this->input->post('custody_info_specify');
            $custody_info_other_info = $this->input->post('custody_info_other_info');

            $data = array(
                'emergency_name_1' => $emergency_name_1,
                'emergency_name_2' => $emergency_name_2,
                'emergency_name_3' => $emergency_name_3,
                'emergency_phone_1' => $emergency_phone_1,
                'emergency_phone_2' => $emergency_phone_2,
                'emergency_phone_3' => $emergency_phone_3,
                'emergency_relationship_1' => $emergency_relationship_1,
                'emergency_relationship_2' => $emergency_relationship_2,
                'emergency_relationship_3' => $emergency_relationship_3,
                'custody_info_specify' => $custody_info_specify,
                'custody_info_other_info' => $custody_info_other_info,
            );

            $where = array(
                'id' => $parent_id,
            );
            $update_item = $this->parents_model->update_by($where, $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    function index_my_children() {
        // get user id 
        $user_id = $this->_user_login->id;

        // get parent id of user id 
        $parent_info = $this->parents_model->get_by('user_id', $user_id);
        $parent_id = $parent_info->id;

        // get children of logged in parent
        $parents = $this->parents_model->get_parent_children($parent_info->id);
        $only_attendence = '1';
        $this->data['show_object_link'] = 'parents/show_parent_children/' . $parent_id . '/' . $only_attendence;
        $this->data['get_object_link'] = 'parents/get_parent_children/' . $parent_id;
        $this->data['add_object_link'] = 'parents/add_parent_children/' . $parent_id;
        $this->data['update_object_link'] = 'parents/update_parent_children';
        $this->data['delete_object_link'] = 'parents/delete_parent_children';
        $this->data['modal_name'] = 'crud_parent_children';
        $this->data['add_object_title'] = lang("add_new_parent_children");
        $this->data['update_object_title'] = lang("update_parent_children");
        $this->data['delete_object_title'] = lang("delete_parent_children");
//        $this->data['thead'] = array('student_name', 'index_student_payment', 'index_student_attendance');
        $this->data['thead'] = array('student_name', 'index_student_attendance');

//        $this->data['non_printable']['index_student_payment'] = 'index_student_payment';
        $this->data['non_printable']['index_student_attendance'] = 'index_student_attendance';
    }

    function index_payment_children($parent_id = '') {
        if ($parent_id == '') {
            $user = $this->ion_auth->user()->row();
            $user_id = $user->id;
            $results = $this->parents_model->get_parent_by_user_id($user_id);
            $parent_id = $results->id;
        }
        $this->data['show_object_link'] = 'parents/show_parent_children/' . $parent_id;
        $this->data['get_object_link'] = 'parents/get_parent_children/' . $parent_id;
        $this->data['add_object_link'] = 'parents/add_parent_children/' . $parent_id;
        $this->data['update_object_link'] = 'parents/update_parent_children';
        $this->data['delete_object_link'] = 'parents/delete_parent_children';
        $this->data['modal_name'] = 'crud_parent_children';
        $this->data['add_object_title'] = lang("add_new_parent_children");
        $this->data['update_object_title'] = lang("update_parent_children");
        $this->data['delete_object_title'] = lang("delete_parent_children");
        $this->data['thead'] = array('student_name', 'index_student_payment',);

        $this->data['non_printable']['index_student_payment'] = 'index_student_payment';
    }

}
