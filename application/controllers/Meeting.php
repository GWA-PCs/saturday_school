<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Meeting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('meetings_model');
        $this->load->model('teachers_model');
        $this->load->model('rooms_teachers_model');
        $this->load->model('general_model', 'users_m');
        $this->load->model('log_messages_model');
        $this->users_m->set_table('users');

        $this->load->model("users_meetings_model");
    }

    public function index() {
        
    }

    /**
     * @api {get} /meeting/index_meeting index_meeting
     * @apiName index_meeting
     * @apiGroup meeting
     * @apiDescription home page of meeting in admin account
     */
    public function index_meeting() {

        $meeting_with = array(
            '000' => lang('select_meeting_type'),
            'teachers' => lang('teachers'),
            'class_teachers' => lang('class_teachers'),
        );

        $this->data['meeting_with'] = $meeting_with;

        $this->data['show_object_link'] = 'meeting/show_meetings';
        $this->data['get_object_link_meeting'] = 'meeting/get_meeting';
        $this->data['add_object_link'] = 'meeting/add_meeting';
        $this->data['update_object_link'] = 'meeting/update_meeting';
        $this->data['delete_object_link'] = 'meeting/delete_meeting';
        $this->data['modal_name'] = 'crud_meetings';
        $this->data['add_object_title'] = lang("add_new_meeting");
        $this->data['update_object_title'] = lang("update_meeting");
        $this->data['delete_object_title'] = lang("delete_meeting");
        $this->data['thead'] = array('meeting_title', 'meeting_date', 'send_by_email', 'meeting_file');

        if ($this->_current_year == $this->_archive_year) {
//            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['meeting_file'] = 'meeting_file';
        $this->data['non_printable']['delete'] = 'delete';
//        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /meeting/show_meetings show_meetings
     * @apiName show_meetings
     * @apiGroup meeting
     * @apiDescription get all meetings
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in meetings table
     * @apiError  FALSE return false if failure in get data
     */
    public function show_meetings() {
        $meetings = $this->meetings_model->get_meetings();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($meetings as $meeting) {
            $no++;
            $row = array();
            $row[] = $meeting->meeting_title;
            $row[] = date("m-d-Y", strtotime($meeting->meeting_date));
            $row[] = tinyint_lang_one_and_two($meeting->send_by_email);

            //add html for action
            $row[] = bs3_file_download("assets/uploads/meeting_file/" . $meeting->meeting_file, $meeting->meeting_file);

            if ($this->_current_year == $this->_archive_year) {
//                $row[] = bs3_update_delete_crud($meeting->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($meeting->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->meetings_model->count_all_meetings(),
            "recordsFiltered" => $this->meetings_model->count_filtered_crud_meetings(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /meeting/add_meeting add_meeting
     * @apiName add_meeting
     * @apiGroup meeting
     * @apiDescription add meeting data to meetings database table
     * @apiParam {String} meeting_title Name of meeting
     * @apiParam {Date} meeting_date Date of meeting
     * @apiParam {String} meeting_file Name of file meeting
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  insert_error_file_size_should_be_longer_than_zero400 return JSON encoded string contain insert error file size should be longer than zero error message
     */
    public function add_meeting() {
        $sender_email = $this->settings_model->get_school_contact_email();

        $input_array = array(
            'meeting_title' => "required",
            'meeting_date' => "required",
            'meeting_type' => "required",
            'meeting_to[]' => "required",
            'meeting_content' => "required",
        );

        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $meeting_title = $this->input->post('meeting_title');
            $meeting_date = $this->input->post('meeting_date');
            if ($meeting_date)
                $meeting_date = date_format(date_create_from_format('m-d-Y', $meeting_date), 'Y-m-d');
            $meeting_file = file_or_image('meeting_file', 'meeting_file');
            $meeting_type = $this->input->post("meeting_type");
            $meeting_to = $this->input->post("meeting_to");
            $meeting_content = $this->input->post('meeting_content');
            $send_by_email = $this->input->post('send_by_email');

            if ($this->input->post('send_by_email')) {
                $send_by_email = "2";
            } else {
                $send_by_email = "1";
            }
            if ($meeting_file == '0') {
                error_message('400', 'insert_error_file_size_should_be_longer_than_zero');
            }
            $data = array(
                'meeting_title' => $meeting_title,
                'meeting_date' => $meeting_date,
                'meeting_file' => $meeting_file,
                'meeting_content' => $meeting_content,
                'send_by_email' => $send_by_email,
            );
            $insert = $this->meetings_model->insert($data);

            if ($meeting_type == "class_teachers") { // return teachers from rooms
                $meeting_to = $this->meetings_model->get_meeting_to_from_rooms($meeting_to);
            }
            foreach ($meeting_to as $value) {
                $where = array(
                    "id" => $value,
                );
                $user = $this->users_m->get_by($where);
                if ($send_by_email == '2') {
                    $email_infos = array(
                        'meeting_title' => $meeting_title,
                        'meeting_date' => $meeting_date,
                        'meeting_file' => $meeting_file,
                        'meeting_content' => $meeting_content,
                    );
                    $this->send_email($sender_email, $user->email, $email_infos);
                } else {
                    if (isset($user) && $user) {
                        $input_array = array("user_id" => $user->id,
                            "meet_id" => $insert,);
                        $this->users_meetings_model->insert($input_array);
                    }
                }
            }
            send_message("", '200', 'insert_success');
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    public function send_email($sender_email, $reciever_email, $sub_data) {
        // sending email ..
        $contact_us_email_from = $sender_email;
        $contact_us_email_to = $reciever_email;

        if (isset($contact_us_email_from) && isset($contact_us_email_to) && $contact_us_email_from && $contact_us_email_to) {
            $this->load->library('email');
            //SMTP & mail configuration
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.minaretsaturday.net',
                'smtp_port' => 465,
                'smtp_user' => 'support@minaretsaturday.net',
                'smtp_pass' => 'Paho%gDhz4EG',
                'mailtype' => 'html',
                'mailpath' => '/usr/sbin/sendmail',
                'charset' => 'utf-8'
            );
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            $this->email->from($contact_us_email_from);
            $this->email->to($contact_us_email_to);
            $this->email->subject($sub_data['meeting_title']);
            if (isset($sub_data['meeting_file']) && $sub_data['meeting_file']) {
                $site = base_url();
                $this->email->attach($site . '/assets/uploads/meeting_file/' . $sub_data['meeting_file']);
            }
            $this->email->message($sub_data['meeting_content']);

            $send = $this->email->send();
            $this->email->clear(TRUE);
            $status = 1;
            $data = array(
                'sender_email' => $contact_us_email_from,
                'reciever_email' => $contact_us_email_to,
                'status' => $status,
            );
            if ($send) {
                $status = 2;
                $data['status'] = $status;
                $this->log_messages_model->insert($data);
                return TRUE;
            } else {
                $status = 1;
                $data['status'] = $status;
                $this->log_messages_model->insert($data);
                return FALSE;
            }
        }
    }

    /**
     * @api {get} /meeting/get_meeting get_meeting
     * @apiName get_meeting
     * @apiGroup meeting
     * @apiDescription get id of meeting to get its info from meetings database table
     * @apiParam {Number} id id of meeting
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of meeting from meetings table
     * @apiError  FALSE return false if failure in get data
     */
    public function get_meeting($id) {
        $data = $this->meetings_model->get_by('id', $id);
        $data->meeting_date = date("m-d-Y", strtotime($data->meeting_date));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /meeting/update_meeting update_meeting
     * @apiName update_meeting
     * @apiGroup meeting
     * @apiDescription update meeting data in meetings database table
     * @apiParam {String} meeting_title Name of meeting
     * @apiParam {Date} meeting_date Date of meeting
     * @apiParam {String} meeting_file Name of file meeting
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    public function update_meeting() {
        $meetings = $this->meetings_model->get_all();

        $input_array = array(
            'meeting_title' => "required",
            'meeting_date' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {

            $item_id = $this->input->post('id');
            $meeting_title = $this->input->post('meeting_title');
            $meeting_date = $this->input->post('meeting_date');
            if ($meeting_date)
                $meeting_date = date_format(date_create_from_format('m-d-Y', $meeting_date), 'Y-m-d');

            $old_file_name = $this->input->post('fileName');
            $meeting_file = $old_file_name;
            if (isset($_FILES)) {
                if (!empty($_FILES['meeting_file']['name']) && $_FILES['meeting_file']['name'] != "") {
                    $meeting_file = file_or_image('meeting_file', 'meeting_file');
                }
            }

            validation_edit_delete_redirect($meetings, "id", $item_id);
            // end validation

            $data = array(
                'meeting_title' => $meeting_title,
                'meeting_date' => $meeting_date,
                'meeting_file' => $meeting_file,
            );

            $update_item = $this->meetings_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message($_FILES, '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /meeting/delete_meeting delete_meeting
     * @apiName delete_meeting
     * @apiGroup meeting
     * @apiDescription get id of meeting to delete its info from meetings database table
     * @apiParam {Number} id id of meeting
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    public function delete_meeting($id) {
        $meetings = $this->meetings_model->get_all();
        //start validation
        validation_edit_delete_redirect($meetings, "id", $id);
        // end validation

        $delete_item = $this->meetings_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    public function get_meeting_to_users() {
        $meeting_type = $_POST['meeting_type'];
        $res = [];
        if ($meeting_type == "teachers") {
            $res = $this->teachers_model->get_teachers();
        } elseif ($meeting_type == "class_teachers") {
            $res = $this->rooms_teachers_model->get_all_rooms_teachers_for_all_room_grouped_by_room_id();
        }
        die(json_encode($res));
    }

}
