<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Enrollment_requests extends MY_Controller {

    private $apps = array(
        'Z5aNppRO6VNdt9OGvlL1Bpim2' => array(
            'app_name' => 'Saturday school',
            'app_secret' => 'dDdjDiC8suGNcCCEEr3k7txgk'
        ),
    );
    public $main_site_registration_form = 'http://minaretsaturday.net/index.php/site/get_all_registration_form';
    public $main_site_arabic_reading_club = 'http://minaretsaturday.net/index.php/site/get_all_arabic_reading_club';
    public $main_site_summer_camp_form = 'http://minaretsaturday.net/index.php/site/get_all_summer_camp_form';
    public $main_site_youth_group_registration_form = 'http://minaretsaturday.net/index.php/site/get_all_youth_group_registration_form';

//    public $main_site_registration_form = '';
//    public $main_site_arabic_reading_club = '';
//    public $main_site_summer_camp_form = '';
//    public $main_site_youth_group_registration_form = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('enrollment_requests_model');
        $this->load->model('grades_model');
        $this->load->model('rooms_model');
        $this->load->model('parents_model');
        $this->load->model('students_model');
        $this->load->model('ion_auth_model');
        $this->load->model('general_model', 'users_model');
        $this->users_model->set_table('users');
        date_default_timezone_set('America/Los_Angeles');

//        $this->authentication_login();
    }

    /**
     * @api {get} /enrollment_requests/index_enrollment_requests index_enrollment_requests
     * @apiName index_enrollment_requests
     * @apiGroup enrollment_requests
     * @apiDescription links of enrollment requests should display in enrollment requests home page 
     */
    function index_enrollment_requests() {
        
    }

    /**
     * @api {post} /enrollment_requests/insert_enrollment_requests insert_enrollment_requests
     * @apiName insert_enrollment_requests
     * @apiGroup enrollment_requests
     * @apiDescription insert enrollment requests data to enrollment requests database table
     * @apiParam {String} app_key Key to compare with it when get information from website
     * @apiParam {String} app_secret Key to compare with it when get information from website
     * @apiParam {String} form_name Name of form get information from website
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function insert_enrollment_requests() {
        $app_key = $this->input->get('app_key');
        $app_secret = $this->input->get('app_secret');
        $form_name = $this->input->get('form_name');

        if ($form_name == 'registration_form' || $form_name == 'summer_camp_form') {
            $student_2 = $this->input->get('student_2');
            $student_3 = $this->input->get('student_3');
            $student_4 = $this->input->get('student_4');
            $student_age_2 = $this->input->get('student_age_2');
            $student_age_3 = $this->input->get('student_age_3');
            $student_age_4 = $this->input->get('student_age_4');
            $student_grade_1 = $this->input->get('student_grade_1');
            $student_grade_2 = $this->input->get('student_grade_2');
            $student_grade_3 = $this->input->get('student_grade_3');
            $student_grade_4 = $this->input->get('student_grade_4');
            $attended = $this->input->get('attended');
            $their_grade_level = $this->input->get('their_grade_level');
            $other_information_1 = $this->input->get('other_information_1');
            $other_information_2 = $this->input->get('other_information_2');
            $other_information_3 = $this->input->get('other_information_3');
            $other_information_4 = $this->input->get('other_information_4');
            $agree = $this->input->get('agree');
            $contract = $this->input->get('contract');

            if ($form_name != 'summer_camp_form') {
                $book_fees = $this->input->get('book_fees');
                $type = $this->input->get('type');
            }
        } elseif ($form_name == 'arabic_reading_club' || $form_name == 'arabic_reading_club_new') {
            $student_2 = $this->input->get('student_2');
            $student_3 = $this->input->get('student_3');
            $student_4 = $this->input->get('student_4');
            $student_age_2 = $this->input->get('student_age_2');
            $student_age_3 = $this->input->get('student_age_3');
            $student_age_4 = $this->input->get('student_age_4');
            $student_grade_1 = $this->input->get('student_grade_1');
            $student_grade_2 = $this->input->get('student_grade_2');
            $student_grade_3 = $this->input->get('student_grade_3');
            $student_grade_4 = $this->input->get('student_grade_4');
        }

        $family_last_name = $this->input->get('family_last_name');
        $student_1 = $this->input->get('student_1');
        $student_age_1 = $this->input->get('student_age_1');
        $email = $this->input->get('email');
        $home_address = $this->input->get('home_address');
        $home_phone = $this->input->get('home_phone');
        $mother_name = $this->input->get('mother_name');
        $father_name = $this->input->get('father_name');
        $mother_phone = $this->input->get('mother_phone');
        $father_phone = $this->input->get('father_phone');
        $date = $this->input->get('date');
        $permission = $this->input->get('permission');

        $emergency_name_1 = $this->input->get('emergency_name_1');
        $emergency_name_2 = $this->input->get('emergency_name_2');
        $emergency_name_3 = $this->input->get('emergency_name_3');

        $emergency_phone_1 = $this->input->get('emergency_phone_1');
        $emergency_phone_2 = $this->input->get('emergency_phone_2');
        $emergency_phone_3 = $this->input->get('emergency_phone_3');

        $emergency_relationship_1 = $this->input->get('emergency_relationship_1');
        $emergency_relationship_2 = $this->input->get('emergency_relationship_2');
        $emergency_relationship_3 = $this->input->get('emergency_relationship_3');

        $custody_info_specify = $this->input->get('custody_info_specify');
        $custody_info_other_info = $this->input->get('custody_info_other_info');

        $response = array(
            'error' => true,
            'data' => 'Invalid Application Key OR Application Secret'
        );

        if (isset($this->apps[$app_key]) && $this->apps[$app_key]['app_secret'] == $app_secret) {
            if ($form_name == 'registration_form' || $form_name == 'summer_camp_form') {
                $data = array(
                    'family_last_name' => $family_last_name,
                    'student_1' => $student_1,
                    'student_age_1' => $student_age_1,
                    'student_grade_1' => $student_grade_1,
                    'student_2' => $student_2,
                    'student_age_2' => $student_age_2,
                    'student_grade_2' => $student_grade_2,
                    'student_3' => $student_3,
                    'student_age_3' => $student_age_3,
                    'student_grade_3' => $student_grade_3,
                    'student_4' => $student_4,
                    'student_age_4' => $student_age_4,
                    'student_grade_4' => $student_grade_4,
                    'email' => $email,
                    'home_address' => $home_address,
                    'home_phone' => $home_phone,
                    'mother_name' => $mother_name,
                    'father_name' => $father_name,
                    'mother_phone' => $mother_phone,
                    'father_phone' => $father_phone,
                    'attended' => $attended,
                    'their_grade_level' => $their_grade_level,
                    'other_information_1' => $other_information_1,
                    'other_information_2' => $other_information_2,
                    'other_information_3' => $other_information_3,
                    'other_information_4' => $other_information_4,
                    'agree' => $agree,
                    'contract' => $contract,
                    'permission' => $permission,
                    'form_name' => $form_name,
                    'emergency_name_1' => $emergency_name_1,
                    'emergency_name_2' => $emergency_name_2,
                    'emergency_name_3' => $emergency_name_3,
                    'emergency_phone_1' => $emergency_phone_1,
                    'emergency_phone_2' => $emergency_phone_2,
                    'emergency_phone_3' => $emergency_phone_3,
                    'emergency_relationship_1' => $emergency_relationship_1,
                    'emergency_relationship_2' => $emergency_relationship_2,
                    'emergency_relationship_3' => $emergency_relationship_3,
                    'custody_info_specify' => $custody_info_specify,
                    'custody_info_other_info' => $custody_info_other_info,
                );
                if ($form_name != 'summer_camp_form') {
                    $data['book_fees'] = $book_fees;
                    $data['date'] = $date;
                    $data['type'] = $type;
                }
            } elseif ($form_name == 'youth_group_registration_form') {
                $data = array(
                    'family_last_name' => $family_last_name,
                    'student_1' => $student_1,
                    'student_age_1' => $student_age_1,
                    'email' => $email,
                    'home_address' => $home_address,
                    'home_phone' => $home_phone,
                    'mother_name' => $mother_name,
                    'father_name' => $father_name,
                    'mother_phone' => $mother_phone,
                    'father_phone' => $father_phone,
                    'permission' => $permission,
                    'date' => $date,
                    'form_name' => $form_name,
                    'emergency_name_1' => $emergency_name_1,
                    'emergency_name_2' => $emergency_name_2,
                    'emergency_name_3' => $emergency_name_3,
                    'emergency_phone_1' => $emergency_phone_1,
                    'emergency_phone_2' => $emergency_phone_2,
                    'emergency_phone_3' => $emergency_phone_3,
                    'emergency_relationship_1' => $emergency_relationship_1,
                    'emergency_relationship_2' => $emergency_relationship_2,
                    'emergency_relationship_3' => $emergency_relationship_3,
                    'custody_info_specify' => $custody_info_specify,
                    'custody_info_other_info' => $custody_info_other_info,
                );
            } elseif ($form_name == 'arabic_reading_club' || $form_name == 'arabic_reading_club_new') {
                $data = array(
                    'family_last_name' => $family_last_name,
                    'student_1' => $student_1,
                    'student_age_1' => $student_age_1,
                    'student_grade_1' => $student_grade_1,
                    'student_2' => $student_2,
                    'student_age_2' => $student_age_2,
                    'student_grade_2' => $student_grade_2,
                    'student_3' => $student_3,
                    'student_age_3' => $student_age_3,
                    'student_grade_3' => $student_grade_3,
                    'student_4' => $student_4,
                    'student_age_4' => $student_age_4,
                    'student_grade_4' => $student_grade_4,
                    'email' => $email,
                    'home_address' => $home_address,
                    'home_phone' => $home_phone,
                    'mother_name' => $mother_name,
                    'father_name' => $father_name,
                    'mother_phone' => $mother_phone,
                    'father_phone' => $father_phone,
                    'form_name' => $form_name,
                    'emergency_name_1' => $emergency_name_1,
                    'emergency_name_2' => $emergency_name_2,
                    'emergency_name_3' => $emergency_name_3,
                    'emergency_phone_1' => $emergency_phone_1,
                    'emergency_phone_2' => $emergency_phone_2,
                    'emergency_phone_3' => $emergency_phone_3,
                    'emergency_relationship_1' => $emergency_relationship_1,
                    'emergency_relationship_2' => $emergency_relationship_2,
                    'emergency_relationship_3' => $emergency_relationship_3,
                    'custody_info_specify' => $custody_info_specify,
                    'custody_info_other_info' => $custody_info_other_info,
                );
            }
            $insert = $this->enrollment_requests_model->insert($data);

            if ($insert) {
                $response['error'] = false;
                $response['data'] = 'hellloooooo world';
            } else {
                $response['data'] = 'Invalid Insert to db';
            }
        } else {
            $response = array(
                'error' => true,
                'data' => 'Invalid Application Key OR Application Secret'
            );
        }

        die(json_encode($response));
    }

    /**
     * @api {get} /enrollment_requests/get_rooms_for_filter get_rooms_for_filter
     * @apiName get_rooms_for_filter
     * @apiGroup enrollment_requests
     * @apiDescription get information of form from  enrollment requests database table
     * @apiParam {Number} id id of grade
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of form from enrollment requests table
     * @apiError  FALSE return false if failure in get data
     */
    function get_rooms_for_filter($form_name = '') {
        if ($form_name == 'arabic_reading_club_new' || $form_name == 'arabic_reading_club') {
            $grade_type = 'book_club';
            $rooms = $this->rooms_model->get_rooms_for_grade_type($grade_type);
        } else {
            $rooms = $this->rooms_model->get_all();
        }


        $arr = array();
        foreach ($rooms as $value) {
            $row = array();
            $row['grade_id'] = $value->grade_id;
            $row['room_id'] = $value->id;
            $row['room_name'] = $value->room_name;
            $arr[] = array("grade_id" => $row['grade_id'], "room_id" => $row['room_id'], "room_name" => $row['room_name'],);
        }
//output to json format

        echo json_encode($arr);
        die;
    }

    /**
     * @api {post} /enrollment_requests/add_parent add_parent
     * @apiName add_parent
     * @apiGroup enrollment_requests
     * @apiDescription add parent data to users and parents database table
     * @apiParam {String} family_name Name of family
     * @apiParam {String} father_name Name of father
     * @apiParam {String} mother_name Name of mother
     * @apiParam {Number} father_phone Phone of father
     * @apiParam {Number} mother_phone Phone of mother
     * @apiParam {Number} home_phone Phone of Home
     * @apiParam {String} email First email of parent
     * @apiParam {String} email Second email of parent
     * @apiParam {String} address address of parent
     * @apiParam {String} non_encrept_pass Password for email 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  this_parent_is_already_exist400 return JSON encoded string contain this parent is already exist error message
     */
    function add_parent($parent_data, $user_data, $email, $password) {
        $type = 'parent';
        $parent_data1 = array(
            'family_name' => $parent_data['family_name'],
            'father_name' => $user_data['father_name'],
            'mother_name' => $user_data['mother_name'],
        );
        $this->db->select('*,parents.id as id');
        $this->db->join('users', 'users.id=parents.user_id');
        $exist = $this->parents_model->get_by($parent_data1);

        $user_data1 = array(
            'father_name <>' => $user_data['father_name'],
            'mother_name <>' => $user_data['mother_name'],
            'email' => $email,
        );
        $exist_email = $this->users_model->get_by($user_data1);

        if (isset($exist_email) && $exist_email) {
            return 'exist_email_with_another_name';
        }

        if (isset($exist) && $exist) {
            return $exist->id;
        } else {
            $user_id = $this->ion_auth_model->register($email, $password, $email, $type, $user_data);

            if (isset($user_id) && $user_id) {
                $data = array(
                    'user_id' => $user_id,
                    'family_name' => $parent_data['family_name'],
                    'father_phone' => $parent_data['father_phone'],
                    'mother_phone' => $parent_data['mother_phone'],
                    'home_phone' => $parent_data['home_phone'],
                    'emergency_name_1' => $parent_data['emergency_name_1'],
                    'emergency_name_2' => $parent_data['emergency_name_2'],
                    'emergency_name_3' => $parent_data['emergency_name_3'],
                    'emergency_phone_1' => $parent_data['emergency_phone_1'],
                    'emergency_phone_2' => $parent_data['emergency_phone_2'],
                    'emergency_phone_3' => $parent_data['emergency_phone_3'],
                    'emergency_relationship_1' => $parent_data['emergency_relationship_1'],
                    'emergency_relationship_2' => $parent_data['emergency_relationship_2'],
                    'emergency_relationship_3' => $parent_data['emergency_relationship_3'],
                    'custody_info_specify' => $parent_data['custody_info_specify'],
                    'custody_info_other_info' => $parent_data['custody_info_other_info'],
                );
                $insert = $this->parents_model->insert($data);
                if ($insert) {
                    return $insert;
                } else {
                    return false;
                }
            } else {
                if ($exist_email) {
                    $data = array(
                        'user_id' => $exist_email->id,
                        'family_name' => $parent_data['family_name'],
                        'father_phone' => $parent_data['father_phone'],
                        'mother_phone' => $parent_data['mother_phone'],
                        'home_phone' => $parent_data['home_phone'],
                        'emergency_name_1' => $parent_data['emergency_name_1'],
                        'emergency_name_2' => $parent_data['emergency_name_2'],
                        'emergency_name_3' => $parent_data['emergency_name_3'],
                        'emergency_phone_1' => $parent_data['emergency_phone_1'],
                        'emergency_phone_2' => $parent_data['emergency_phone_2'],
                        'emergency_phone_3' => $parent_data['emergency_phone_3'],
                        'emergency_relationship_1' => $parent_data['emergency_relationship_1'],
                        'emergency_relationship_2' => $parent_data['emergency_relationship_2'],
                        'emergency_relationship_3' => $parent_data['emergency_relationship_3'],
                        'custody_info_specify' => $parent_data['custody_info_specify'],
                        'custody_info_other_info' => $parent_data['custody_info_other_info'],
                    );
                    $insert = $this->parents_model->insert($data);
                    if ($insert) {
                        return $insert;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * @api {post} /enrollment_requests/add_student add_student
     * @apiName add_student
     * @apiGroup enrollment_requests
     * @apiDescription add student data to students database table
     * @apiParam {String} student_name Name of student
     * @apiParam {Number} parent_id id of parent (select from dropdown list)
     * @apiParam {String} student_notes Notes about student
     * @apiParam {Number} age Age of student
     * @apiSuccess  BooleanValue return true if inserted successfully 
     * @apiError  BooleanValue return false if inserted error 
     */
    function add_student($parent_id, $student_data) {
        $student_data['parent_id'] = $parent_id;

        $student_data1 = array(
            'student_name' => $student_data['student_name'],
            'parent_id' => $parent_id,
        );
        $exist = $this->students_model->get_by($student_data1);
        if (isset($exist) && $exist) {
            return $exist->id;
        } else {
            sleep(1);
            $password = random_password();
            $user_id = $this->ion_auth_model->register(time(), $password, time(), "student", ["non_encrept_pass" => $password]);
            $student_data['user_id'] = $user_id;
            $insert = $this->students_model->insert($student_data);
            if (isset($insert) && $insert) {
                return $insert;
            } else {
                return false;
            }
        }
    }

    /**
     * @api {post} /enrollment_requests/register_student_in_grade register_student_in_grade
     * @apiName register_student_in_grade
     * @apiGroup enrollment_requests
     * @apiDescription add student record data to student records database table
     * @apiParam {Number} student_id id of student 
     * @apiParam {Number} room_id id of room 
     * @apiParam {Number} grade_id id of grade 
     * @apiParam {Boolean} photo_permission_slips value of photo permission slips
     * @apiSuccess  BooleanValue return true if inserted successfully 
     * @apiError  BooleanValue return false if inserted error 
     */
    function register_student_in_grade($student_id, $room_id, $grade_id, $photo_permission_slips) {
//        if ($room_id != '000' && $grade_id != '000' && $room_id != '0' && $grade_id != '0') {
        if ($room_id != '000' && $room_id != '0') {

            $this->db->join('grades', 'grades.id=rooms.grade_id');
            $grade_info = $this->rooms_model->get_by('rooms.id', $room_id);

//            $grade_info = $this->grades_model->get_by('id', $grade_id);
            $grade_type = $grade_info->grade_type;

            $teacher_child = 1;
            $islamic_book_return = 1;
            $is_active = 2;
            $status_islamic_book_return = 'no_status';

            $student_order = $this->students_records_model->get_student_order($student_id);

            $where = array(
                'student_id' => $student_id,
                'room_id' => $room_id,
                'is_active' => $is_active,
            );
            if (exist_item("students_records_model", $where)) {
                return false;
            }

            if ($grade_type != 'youth_group' && $grade_type != 'book_club') {
                $where = array(
                    'student_id' => $student_id,
                    'room_id <>' => $room_id,
                    'is_active' => $is_active,
                );
                $this->db->join('students', 'students.id=students_records.student_id');
                $s = $this->students_records_model->get_by($where);
                if (exist_item("students_records_model", $where)) {
                    return false;
                }
            }

            $where = array(
                'student_id' => $student_id,
                'is_active' => $is_active,
            );
            $count_of_active_students_records = count($this->students_records_model->get_many_by($where));

            if ($count_of_active_students_records == '2') {
                return false;
            }

            $data = array(
                'student_id' => $student_id,
                'room_id' => $room_id,
                'photo_permission_slips' => $photo_permission_slips,
                'islamic_book_return' => $islamic_book_return,
                'status_islamic_book_return' => $status_islamic_book_return,
                'is_active' => $is_active,
                'student_order' => $student_order + 1,
                'year' => $this->_current_year,
                'teacher_child' => $teacher_child,
            );
            $insert = $this->students_records_model->insert($data);
            if (isset($insert) && $insert) {
                return $insert;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @api {post} /enrollment_requests/add_registration_form add_registration_form
     * @apiName add_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription add registration form data to enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function add_registration_form() {
//        $this->data['enrollment_requests'] = $enrollment_requests;

        $item_id = $this->input->post('id');
        $post_data = $_POST;

        $where = array('form_name' => 'registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);

        $data_inserted = $this->register_student_in_school_registration_form($enrollment_requests, $post_data);

        if ($data_inserted && $data_inserted != 'exist_email_with_another_name') {
            send_message("", '200', 'non_duplicate_information_is_added_success');
        } elseif ($data_inserted == 'exist_email_with_another_name') {
            error_message('400', 'exist_email_with_another_name');
        } else {
            error_message('400', 'insert_error');
        }
    }

    /**
     * @api {get} /enrollment_requests/registration_form_details registration_form_details
     * @apiName registration_form_details
     * @apiGroup enrollment_requests
     * @apiDescription home page of registration form details
     */
    function registration_form_details() {
        
    }

    /**
     * @api {post} /enrollment_requests/get_registration_form_info get_registration_form_info
     * @apiName get_registration_form_info
     * @apiGroup enrollment_requests
     * @apiDescription get registration form data from enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function get_registration_form_info($item_id) {
        $where = array('form_name' => 'registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);
        send_message($enrollment_requests);
    }

    /**
     * @api {get} /enrollment_requests/index_registration_form index_registration_form
     * @apiName index_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription home page of registration form 
     */
    function index_registration_form() {
        $siteaddress = $this->main_site_registration_form . "?app_key=Z5aNppRO6VNdt9OGvlL1Bpim2&app_secret=dDdjDiC8suGNcCCEEr3k7txgk";
        $data_before_decode = @file_get_contents($siteaddress);

        $data = json_decode($data_before_decode);
        if (isset($data) && $data) {
            $this->save_info_in_enrollment_requests($data, 'registration_form');
        }


        $grades = $this->grades_model->get_all();
        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($grades as $value) {
            $students_grades_options[$value->id] = $value->grade_name;
        }

        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;

        $where = array('form_name' => 'registration_form',
            'is_deleted' => '0',
        );
        $this->db->where($where);
        $this->db->order_by('created_at', 'desc');
        $enrollment_requests = $this->enrollment_requests_model->get_all();
        $this->data['enrollment_requests'] = $enrollment_requests;

//        if ($this->input->post('update')) {
//            $item_id = $this->input->post('id');
//            $post_data = $_POST;
//
//            $where = array('form_name' => 'registration_form',
//                'is_deleted' => '0'
//            );
//            $this->db->where($where);
//            $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);
//
//            $data_inserted = $this->register_student_in_school_registration_form($enrollment_requests, $post_data);
//
//            if ($data_inserted) {
//                send_message("", '200', lang('insert_success'));
//            }
//        }
    }

    /**
     * @api {post} /enrollment_requests/get_registration_form get_registration_form
     * @apiName get_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription get all registration forms data from enrollment requests database table
     * @apiSuccess  enrollment_requests Array contain all registration forms
     * @apiError  FALSE return false if failure in get data
     */
    function get_registration_form() {
        $where = array('form_name' => 'registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_all();

        if ($enrollment_requests) {
            send_message($enrollment_requests, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    /**
     * @api {get} /enrollment_requests/register_student_in_school_registration_form register_student_in_school_registration_form
     * @apiName register_student_in_school_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription register student in school
     * @apiSuccess  data_inserted Array contain students records id 
     */
    function register_student_in_school_registration_form($enrollment_requests, $post_data) {
        $room_id_1 = 0;
        if (!isset($post_data['room_id_1'])) {
            
        } else {
            $room_id_1 = $post_data['room_id_1'];
        }
        $room_id_2 = 0;
        if (!isset($post_data['room_id_2'])) {
            
        } else {
            $room_id_2 = $post_data['room_id_2'];
        }
        $room_id_3 = 0;
        if (!isset($post_data['room_id_3'])) {
            
        } else {
            $room_id_3 = $post_data['room_id_3'];
        }
        $room_id_4 = 0;
        if (!isset($post_data['room_id_4'])) {
            
        } else {
            $room_id_4 = $post_data['room_id_4'];
        }

        if (!isset($post_data['grade_id_1'])) {
            
        } else {
            $grade_id_1 = $post_data['grade_id_1'];
        }
        $grade_id_2 = 0;
        if (!isset($post_data['grade_id_2'])) {
            
        } else {
            $grade_id_2 = $post_data['grade_id_2'];
        }
        $grade_id_3 = 0;
        if (!isset($post_data['grade_id_3'])) {
            
        } else {
            $grade_id_3 = $post_data['grade_id_3'];
        }
        $grade_id_4 = 0;
        if (!isset($post_data['grade_id_4'])) {
            
        } else {
            $grade_id_4 = $post_data['grade_id_4'];
        }

        $student_record_id_1 = false;
        $student_record_id_2 = false;
        $student_record_id_3 = false;
        $student_record_id_4 = false;

        $email = $enrollment_requests->email;
        $password = random_password();
        $user_data = array(
            'father_name' => $enrollment_requests->father_name,
            'mother_name' => $enrollment_requests->mother_name,
            'address' => $enrollment_requests->home_address,
            'non_encrept_pass' => $password,
        );

        $parent_data = array(
            'family_name' => $enrollment_requests->family_last_name,
            'father_phone' => $enrollment_requests->father_phone,
            'mother_phone' => $enrollment_requests->mother_phone,
            'home_phone' => $enrollment_requests->home_phone,
            'emergency_name_1' => $enrollment_requests->emergency_name_1,
            'emergency_name_2' => $enrollment_requests->emergency_name_2,
            'emergency_name_3' => $enrollment_requests->emergency_name_3,
            'emergency_phone_1' => $enrollment_requests->emergency_phone_1,
            'emergency_phone_2' => $enrollment_requests->emergency_phone_2,
            'emergency_phone_3' => $enrollment_requests->emergency_phone_3,
            'emergency_relationship_1' => $enrollment_requests->emergency_relationship_1,
            'emergency_relationship_2' => $enrollment_requests->emergency_relationship_2,
            'emergency_relationship_3' => $enrollment_requests->emergency_relationship_3,
            'custody_info_specify' => $enrollment_requests->custody_info_specify,
            'custody_info_other_info' => $enrollment_requests->custody_info_other_info,
        );

        $parent_id = $this->add_parent($parent_data, $user_data, $email, $password);

        if ($parent_id == 'exist_email_with_another_name') {
            return 'exist_email_with_another_name';
        }

        if (isset($parent_id) && $parent_id && $parent_id != 'exist_email_with_another_name') {

            $student_1_name = $enrollment_requests->student_1;
            $student_2_name = $enrollment_requests->student_2;
            $student_3_name = $enrollment_requests->student_3;
            $student_4_name = $enrollment_requests->student_4;

            $student_age_1 = $enrollment_requests->student_age_1;
            $student_age_2 = $enrollment_requests->student_age_2;
            $student_age_3 = $enrollment_requests->student_age_3;
            $student_age_4 = $enrollment_requests->student_age_4;

            $student_grade_1 = $enrollment_requests->student_grade_1;
            $student_grade_2 = $enrollment_requests->student_grade_2;
            $student_grade_3 = $enrollment_requests->student_grade_3;
            $student_grade_4 = $enrollment_requests->student_grade_4;

            $other_information_1 = $enrollment_requests->other_information_1;
            $other_information_2 = $enrollment_requests->other_information_2;
            $other_information_3 = $enrollment_requests->other_information_3;
            $other_information_4 = $enrollment_requests->other_information_4;

            $photo_permission_slips = $enrollment_requests->permission;

            $student_data = array(
                'student_name' => $student_1_name,
                'age' => $student_age_1,
                'grade' => $student_grade_1,
                'student_notes' => $other_information_1,
            );

            $student_id_1 = $this->add_student($parent_id, $student_data);

            if (isset($student_id_1) && $student_id_1 && isset($grade_id_1) && $grade_id_1 && isset($room_id_1) && $room_id_1 != 0) {
                $student_record_id_1 = $this->register_student_in_grade($student_id_1, $room_id_1, $grade_id_1, $photo_permission_slips);
            }
            if (isset($student_2_name) && $student_2_name) {
                $student_data2 = array(
                    'student_name' => $student_2_name,
                    'age' => $student_age_2,
                    'grade' => $student_grade_2,
                    'student_notes' => $other_information_2,
                );
                $student_id_2 = $this->add_student($parent_id, $student_data2);

                if (isset($student_id_2) && $student_id_2 && isset($grade_id_2) && $grade_id_2 && isset($room_id_2) && $room_id_2 != 0) {
                    $student_record_id_2 = $this->register_student_in_grade($student_id_2, $room_id_2, $grade_id_2, $photo_permission_slips);
                }
            }
            if (isset($student_3_name) && $student_3_name) {
                $student_data3 = array(
                    'student_name' => $student_3_name,
                    'age' => $student_age_3,
                    'grade' => $student_grade_3,
                    'student_notes' => $other_information_3,
                );
                $student_id_3 = $this->add_student($parent_id, $student_data3);
                if (isset($student_id_3) && $student_id_3 && isset($grade_id_3) && $grade_id_3 && isset($room_id_3) && $room_id_3 != 0) {
                    $student_record_id_3 = $this->register_student_in_grade($student_id_3, $room_id_3, $grade_id_3, $photo_permission_slips);
                }
            }
            if (isset($student_4_name) && $student_4_name) {
                $student_data4 = array(
                    'student_name' => $student_4_name,
                    'age' => $student_age_4,
                    'grade' => $student_grade_4,
                    'student_notes' => $other_information_4,
                );
                $student_id_4 = $this->add_student($parent_id, $student_data4);

                if (isset($student_id_4) && $student_id_4 && isset($grade_id_4) && $grade_id_4 && isset($room_id_4) && $room_id_4 != 0) {
                    $student_record_id_4 = $this->register_student_in_grade($student_id_4, $room_id_4, $grade_id_4, $photo_permission_slips);
                }
            }
            $data_inserted = array(
                'student_record_id_1' => $student_record_id_1,
                'student_record_id_2' => $student_record_id_2,
                'student_record_id_3' => $student_record_id_3,
                'student_record_id_4' => $student_record_id_4,
            );
            return $data_inserted;
        } else {
            return false;
        }
    }

    /**
     * @api {post} /enrollment_requests/add_youth_group_registration_form add_youth_group_registration_form
     * @apiName add_youth_group_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription add youth group registration form data to enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function add_youth_group_registration_form() {
//        $this->data['enrollment_requests'] = $enrollment_requests;

        $item_id = $this->input->post('id');
        $post_data = $_POST;

        $where = array('form_name' => 'youth_group_registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);

        $data_inserted = $this->register_student_in_school_youth_group($enrollment_requests, $post_data);

        if ($data_inserted && $data_inserted != 'exist_email_with_another_name') {
            send_message("", '200', 'non_duplicate_information_is_added_success');
        } elseif ($data_inserted == 'exist_email_with_another_name') {
            error_message('400', 'exist_email_with_another_name');
        } else {
            error_message('400', 'insert_error');
        }
    }

    /**
     * @api {get} /enrollment_requests/youth_group_registration_form_details youth_group_registration_form_details
     * @apiName youth_group_registration_form_details
     * @apiGroup enrollment_requests
     * @apiDescription home page of youth group registration form details
     */
    function youth_group_registration_form_details() {
        
    }

    /**
     * @api {post} /enrollment_requests/get_youth_group_registration_form_info get_youth_group_registration_form_info
     * @apiName get_youth_group_registration_form_info
     * @apiGroup enrollment_requests
     * @apiDescription get youth group registration form data from enrollment requests database table
     * @apiParam {Number} id id of enrollment request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function get_youth_group_registration_form_info($item_id) {
        $where = array('form_name' => 'youth_group_registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);
        send_message($enrollment_requests);
    }

    /**
     * @api {get} /enrollment_requests/index_youth_group_registration_form index_youth_group_registration_form
     * @apiName index_youth_group_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription home page of youth group registration form 
     */
    function index_youth_group_registration_form() {
        $siteaddress = $this->main_site_youth_group_registration_form . "?app_key=Z5aNppRO6VNdt9OGvlL1Bpim2&app_secret=dDdjDiC8suGNcCCEEr3k7txgk";
        $data_before_decode = @file_get_contents($siteaddress);
        $data = json_decode($data_before_decode);
        if (isset($data) && $data) {
            $this->save_info_in_enrollment_requests($data, 'youth_group_registration_form');
        }

        $where = array('grade_type' => 'youth_group');
        $this->db->where($where);
        $grades = $this->grades_model->get_all();
        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($grades as $value) {
            $students_grades_options[$value->id] = $value->grade_name;
        }

        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;

        $where = array('form_name' => 'youth_group_registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $this->db->order_by('created_at', 'desc');
        $enrollment_requests = $this->enrollment_requests_model->get_all();
        $this->data['enrollment_requests'] = $enrollment_requests;

//        if ($this->input->post('update')) {
//            $item_id = $this->input->post('id');
//            $post_data = $_POST;
//
//            $where = array('form_name' => 'youth_group_registration_form',
//                'is_deleted' => '0'
//            );
//            $this->db->where($where);
//            $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);
//
//            $data_inserted = $this->register_student_in_school_youth_group($enrollment_requests, $post_data);
//
//            if ($data_inserted) {
//                send_message("", '200', lang('insert_success'));
//            }
//        }
    }

    /**
     * @api {post} /enrollment_requests/get_youth_group_registration_form get_youth_group_registration_form
     * @apiName get_youth_group_registration_form
     * @apiGroup enrollment_requests
     * @apiDescription get all youth group registration forms data from enrollment requests database table
     * @apiSuccess  enrollment_requests Array contain all youth group registration forms
     * @apiError  FALSE return false if failure in get data
     */
    function get_youth_group_registration_form() {
        $where = array('form_name' => 'youth_group_registration_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_all();

        if ($enrollment_requests) {
            send_message($enrollment_requests, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    /**
     * @api {get} /enrollment_requests/register_student_in_school_youth_group register_student_in_school_youth_group
     * @apiName register_student_in_school_youth_group
     * @apiGroup enrollment_requests
     * @apiDescription register student in school
     * @apiSuccess  data_inserted Array contain students records id 
     */
    function register_student_in_school_youth_group($enrollment_requests, $post_data) {
        $room_id_1 = 0;
        if (!isset($post_data['room_id_1'])) {
            
        } else {
            $room_id_1 = $post_data['room_id_1'];
        }
        $grade_id_1 = 0;
        if (!isset($post_data['grade_id_1'])) {
            
        } else {
            $grade_id_1 = $post_data['grade_id_1'];
        }

        $student_record_id_1 = false;

        $email = $enrollment_requests->email;
        $password = random_password();
        $user_data = array(
            'father_name' => $enrollment_requests->father_name,
            'mother_name' => $enrollment_requests->mother_name,
            'address' => $enrollment_requests->home_address,
            'non_encrept_pass' => $password,
        );

        $parent_data = array(
            'family_name' => $enrollment_requests->family_last_name,
            'father_phone' => $enrollment_requests->father_phone,
            'mother_phone' => $enrollment_requests->mother_phone,
            'home_phone' => $enrollment_requests->home_phone,
            'emergency_name_1' => $enrollment_requests->emergency_name_1,
            'emergency_name_2' => $enrollment_requests->emergency_name_2,
            'emergency_name_3' => $enrollment_requests->emergency_name_3,
            'emergency_phone_1' => $enrollment_requests->emergency_phone_1,
            'emergency_phone_2' => $enrollment_requests->emergency_phone_2,
            'emergency_phone_3' => $enrollment_requests->emergency_phone_3,
            'emergency_relationship_1' => $enrollment_requests->emergency_relationship_1,
            'emergency_relationship_2' => $enrollment_requests->emergency_relationship_2,
            'emergency_relationship_3' => $enrollment_requests->emergency_relationship_3,
            'custody_info_specify' => $enrollment_requests->custody_info_specify,
            'custody_info_other_info' => $enrollment_requests->custody_info_other_info,
        );

        $parent_id = $this->add_parent($parent_data, $user_data, $email, $password);

        if ($parent_id == 'exist_email_with_another_name') {
            return 'exist_email_with_another_name';
        }

        if (isset($parent_id) && $parent_id && $parent_id != 'exist_email_with_another_name') {
            $student_1_name = $enrollment_requests->student_1;
            $student_age_1 = $enrollment_requests->student_age_1;
            $photo_permission_slips = $enrollment_requests->permission;

            $student_data = array(
                'student_name' => $student_1_name,
                'age' => $student_age_1,
            );
            $student_id_1 = $this->add_student($parent_id, $student_data);

            if (isset($student_id_1) && $student_id_1 && isset($grade_id_1) && $grade_id_1 && isset($room_id_1) && $room_id_1 && $room_id_1 != 0) {
                $student_record_id_1 = $this->register_student_in_grade($student_id_1, $room_id_1, $grade_id_1, $photo_permission_slips);
            }

            $data_inserted = array(
                'student_record_id_1' => $student_record_id_1,
            );

            return $data_inserted;
        }
    }

    /**
     * @api {get} /enrollment_requests/index_arabic_reading_club index_arabic_reading_club
     * @apiName index_arabic_reading_club
     * @apiGroup enrollment_requests
     * @apiDescription home page of arabic reading club form 
     */
    function index_arabic_reading_club() {
        $siteaddress = $this->main_site_arabic_reading_club . "?app_key=Z5aNppRO6VNdt9OGvlL1Bpim2&app_secret=dDdjDiC8suGNcCCEEr3k7txgk";
        $data_before_decode = @file_get_contents($siteaddress);
        $data = json_decode($data_before_decode);
        if (isset($data) && $data) {
            $this->save_info_in_enrollment_requests($data, 'arabic_reading_club');
        }

        $where = array('grade_type' => 'book_club');
        $this->db->where($where);
        $grades = $this->grades_model->get_all();
        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($grades as $value) {
            $students_grades_options[$value->id] = $value->grade_name;
        }

        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;

        $where_1 = array('arabic_reading_club', 'arabic_reading_club_new'
        );

        $this->db->where_in('form_name', $where_1);
        $this->db->where('is_deleted', '0');
        $this->db->order_by('created_at', 'desc');
        $enrollment_requests = $this->enrollment_requests_model->get_all();
        $this->data['enrollment_requests'] = $enrollment_requests;

        if ($this->input->post('update')) {
            $item_id = $this->input->post('id');
            $post_data = $_POST;

            $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);

            $data_inserted = $this->register_student_in_school_arabic_reading_club($enrollment_requests, $post_data);

            if ($data_inserted) {
                send_message("", '200', lang('insert_success'));
            }
        }
    }

    /**
     * @api {post} /enrollment_requests/get_arabic_reading_club get_arabic_reading_club
     * @apiName get_arabic_reading_club
     * @apiGroup enrollment_requests
     * @apiDescription get all arabic reading club forms data from enrollment requests database table
     * @apiSuccess  enrollment_requests Array contain all arabic reading club forms
     * @apiError  FALSE return false if failure in get data
     */
    function get_arabic_reading_club() {
        $where = array('form_name' => 'arabic_reading_club',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $this->db->or_where('form_name', 'arabic_reading_club_new');
        $enrollment_requests = $this->enrollment_requests_model->get_all();

        if ($enrollment_requests) {
            send_message($enrollment_requests, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    /**
     * @api {post} /enrollment_requests/add_arabic_reading_club add_arabic_reading_club
     * @apiName add_arabic_reading_club
     * @apiGroup enrollment_requests
     * @apiDescription add arabic reading club form data to enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function add_arabic_reading_club() {
//        $this->data['enrollment_requests'] = $enrollment_requests;

        $item_id = $this->input->post('id');
        $post_data = $_POST;

        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);

        $data_inserted = $this->register_student_in_school_arabic_reading_club($enrollment_requests, $post_data);

        if ($data_inserted && $data_inserted != 'exist_email_with_another_name') {
            send_message("", '200', 'non_duplicate_information_is_added_success');
        } elseif ($data_inserted == 'exist_email_with_another_name') {
            error_message('400', 'exist_email_with_another_name');
        } else {
            error_message('400', 'insert_error');
        }
    }

    /**
     * @api {get} /enrollment_requests/arabic_reading_club_details arabic_reading_club_details
     * @apiName arabic_reading_club_details
     * @apiGroup enrollment_requests
     * @apiDescription home page of arabic reading club form details
     */
    function arabic_reading_club_details() {
        
    }

    /**
     * @api {post} /enrollment_requests/get_arabic_reading_club_info get_arabic_reading_club_info
     * @apiName get_arabic_reading_club_info
     * @apiGroup enrollment_requests
     * @apiDescription get arabic reading club form data from enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function get_arabic_reading_club_info($item_id) {
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);
        send_message($enrollment_requests);
    }

    /**
     * @api {get} /enrollment_requests/register_student_in_school_arabic_reading_club register_student_in_school_arabic_reading_club
     * @apiName register_student_in_school_arabic_reading_club
     * @apiGroup enrollment_requests
     * @apiDescription register student in school
     * @apiSuccess  data_inserted Array contain students records id 
     */
    function register_student_in_school_arabic_reading_club($enrollment_requests, $post_data) {
        $room_id_1 = 0;
        if (!isset($post_data['room_id_1'])) {
            
        } else {
            $room_id_1 = $post_data['room_id_1'];
        }
        $room_id_2 = 0;
        if (!isset($post_data['room_id_2'])) {
            
        } else {
            $room_id_2 = $post_data['room_id_2'];
        }
        $room_id_3 = 0;
        if (!isset($post_data['room_id_3'])) {
            
        } else {
            $room_id_3 = $post_data['room_id_3'];
        }
        $room_id_4 = 0;
        if (!isset($post_data['room_id_4'])) {
            
        } else {
            $room_id_4 = $post_data['room_id_4'];
        }

        if (!isset($post_data['grade_id_1'])) {
            
        } else {
            $grade_id_1 = $post_data['grade_id_1'];
        }
        $grade_id_2 = 0;
        if (!isset($post_data['grade_id_2'])) {
            
        } else {
            $grade_id_2 = $post_data['grade_id_2'];
        }
        $grade_id_3 = 0;
        if (!isset($post_data['grade_id_3'])) {
            
        } else {
            $grade_id_3 = $post_data['grade_id_3'];
        }
        $grade_id_4 = 0;
        if (!isset($post_data['grade_id_4'])) {
            
        } else {
            $grade_id_4 = $post_data['grade_id_4'];
        }

        $student_record_id_1 = false;
        $student_record_id_2 = false;
        $student_record_id_3 = false;
        $student_record_id_4 = false;


        $email = $enrollment_requests->email;
        $password = random_password();
        $user_data = array(
            'father_name' => $enrollment_requests->father_name,
            'mother_name' => $enrollment_requests->mother_name,
            'address' => $enrollment_requests->home_address,
            'non_encrept_pass' => $password,
        );

        $parent_data = array(
            'family_name' => $enrollment_requests->family_last_name,
            'father_phone' => $enrollment_requests->father_phone,
            'mother_phone' => $enrollment_requests->mother_phone,
            'home_phone' => $enrollment_requests->home_phone,
            'emergency_name_1' => $enrollment_requests->emergency_name_1,
            'emergency_name_2' => $enrollment_requests->emergency_name_2,
            'emergency_name_3' => $enrollment_requests->emergency_name_3,
            'emergency_phone_1' => $enrollment_requests->emergency_phone_1,
            'emergency_phone_2' => $enrollment_requests->emergency_phone_2,
            'emergency_phone_3' => $enrollment_requests->emergency_phone_3,
            'emergency_relationship_1' => $enrollment_requests->emergency_relationship_1,
            'emergency_relationship_2' => $enrollment_requests->emergency_relationship_2,
            'emergency_relationship_3' => $enrollment_requests->emergency_relationship_3,
            'custody_info_specify' => $enrollment_requests->custody_info_specify,
            'custody_info_other_info' => $enrollment_requests->custody_info_other_info,
        );

        $parent_id = $this->add_parent($parent_data, $user_data, $email, $password);

        if ($parent_id == 'exist_email_with_another_name') {
            return 'exist_email_with_another_name';
        }
        if (isset($parent_id) && $parent_id && $parent_id != 'exist_email_with_another_name') {
            $student_1_name = $enrollment_requests->student_1;
            $student_2_name = $enrollment_requests->student_2;
            $student_3_name = $enrollment_requests->student_3;
            $student_4_name = $enrollment_requests->student_4;

            $student_age_1 = $enrollment_requests->student_age_1;
            $student_age_2 = $enrollment_requests->student_age_2;
            $student_age_3 = $enrollment_requests->student_age_3;
            $student_age_4 = $enrollment_requests->student_age_4;

            $student_grade_1 = $enrollment_requests->student_grade_1;
            $student_grade_2 = $enrollment_requests->student_grade_2;
            $student_grade_3 = $enrollment_requests->student_grade_3;
            $student_grade_4 = $enrollment_requests->student_grade_4;
            $photo_permission_slips = $enrollment_requests->permission;

            $student_data = array(
                'student_name' => $student_1_name,
                'age' => $student_age_1,
                'grade' => $student_grade_1,
            );
            $student_id_1 = $this->add_student($parent_id, $student_data);
            if (isset($student_id_1) && $student_id_1 && isset($grade_id_1) && $grade_id_1 && isset($room_id_1) && $room_id_1 && $room_id_1 != 0) {
                $student_record_id_1 = $this->register_student_in_grade($student_id_1, $room_id_1, $grade_id_1, $photo_permission_slips);
            }
            if (isset($student_2_name) && $student_2_name) {
                $student_data2 = array(
                    'student_name' => $student_2_name,
                    'age' => $student_age_2,
                    'grade' => $student_grade_2,
                );
                $student_id_2 = $this->add_student($parent_id, $student_data2);

                if (isset($student_id_2) && $student_id_2 && isset($grade_id_2) && $grade_id_2 && isset($room_id_2) && $room_id_2 && $room_id_2 != 0) {
                    $student_record_id_2 = $this->register_student_in_grade($student_id_2, $room_id_2, $grade_id_2, $photo_permission_slips);
                }
            }
            if (isset($student_3_name) && $student_3_name) {
                $student_data3 = array(
                    'student_name' => $student_3_name,
                    'age' => $student_age_3,
                    'grade' => $student_grade_3,
                );
                $student_id_3 = $this->add_student($parent_id, $student_data3);
                if (isset($student_id_3) && $student_id_3 && isset($grade_id_3) && $grade_id_3 && isset($room_id_3) && $room_id_3 && $room_id_3 != 0) {
                    $student_record_id_3 = $this->register_student_in_grade($student_id_3, $room_id_3, $grade_id_3, $photo_permission_slips);
                }
            }
            if (isset($student_4_name) && $student_4_name) {
                $student_data4 = array(
                    'student_name' => $student_4_name,
                    'age' => $student_age_4,
                    'grade' => $student_grade_4,
                );
                $student_id_4 = $this->add_student($parent_id, $student_data4);

                if (isset($student_id_4) && $student_id_4 && isset($grade_id_4) && $grade_id_4 && isset($room_id_4) && $room_id_4 && $room_id_4 != 0) {
                    $student_record_id_4 = $this->register_student_in_grade($student_id_4, $room_id_4, $grade_id_4, $photo_permission_slips);
                }
            }
            $data_inserted = array(
                'student_record_id_1' => $student_record_id_1,
                'student_record_id_2' => $student_record_id_2,
                'student_record_id_3' => $student_record_id_3,
                'student_record_id_4' => $student_record_id_4,
            );
            return $data_inserted;
        }
    }

    /**
     * @api {post} /enrollment_requests/add_summer_camp_form add_summer_camp_form
     * @apiName add_summer_camp_form
     * @apiGroup enrollment_requests
     * @apiDescription add  summer camp form data to enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function add_summer_camp_form() {
        $item_id = $this->input->post('id');
        $post_data = $_POST;

        $where = array('form_name' => 'summer_camp_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);

        $data_inserted = $this->register_student_in_school_summer_camp_form($enrollment_requests, $post_data);

        if ($data_inserted && $data_inserted != 'exist_email_with_another_name') {
            send_message("", '200', 'non_duplicate_information_is_added_success');
        } elseif ($data_inserted == 'exist_email_with_another_name') {
            error_message('400', 'exist_email_with_another_name');
        } else {
            error_message('400', 'insert_error');
        }
    }

    /**
     * @api {get} /enrollment_requests/summer_camp_form_details summer_camp_form_details
     * @apiName summer_camp_form_details
     * @apiGroup enrollment_requests
     * @apiDescription home page of summer camp form details
     */
    function summer_camp_form_details() {
        
    }

    /**
     * @api {post} /enrollment_requests/get_summer_camp_form_info get_summer_camp_form_info
     * @apiName get_summer_camp_form_info
     * @apiGroup enrollment_requests
     * @apiDescription get summer camp form data from enrollment requests database table
     * @apiParam {Number} id id of enrollment_request 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     */
    function get_summer_camp_form_info($item_id) {
        $where = array('form_name' => 'summer_camp_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);
        send_message($enrollment_requests);
    }

    /**
     * @api {get} /enrollment_requests/index_summer_camp_form index_summer_camp_form
     * @apiName index_summer_camp_form
     * @apiGroup enrollment_requests
     * @apiDescription home page of summer camp form 
     */
    function index_summer_camp_form() {
        $siteaddress = $this->main_site_summer_camp_form . "?app_key=Z5aNppRO6VNdt9OGvlL1Bpim2&app_secret=dDdjDiC8suGNcCCEEr3k7txgk";
        $data_before_decode = @file_get_contents($siteaddress);
        $data = json_decode($data_before_decode);
        if (isset($data) && $data) {
            $this->save_info_in_enrollment_requests($data, 'summer_camp_form');
        }

        $grades = $this->grades_model->get_all();
        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($grades as $value) {
            $students_grades_options[$value->id] = $value->grade_name;
        }

        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;

        $where = array('form_name' => 'summer_camp_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $this->db->order_by('created_at', 'desc');
        $enrollment_requests = $this->enrollment_requests_model->get_all();
        $this->data['enrollment_requests'] = $enrollment_requests;

        if ($this->input->post('update')) {
            $item_id = $this->input->post('id');
            $post_data = $_POST;

            $where = array('form_name' => 'summer_camp_form',
                'is_deleted' => '0'
            );
            $this->db->where($where);
            $enrollment_requests = $this->enrollment_requests_model->get_by('id', $item_id);

            $data_inserted = $this->register_student_in_school_summer_camp_form($enrollment_requests, $post_data);

            if ($data_inserted) {
                send_message("", '200', lang('insert_success'));
            }
        }
    }

    /**
     * @api {post} /enrollment_requests/get_summer_camp_form get_summer_camp_form
     * @apiName get_summer_camp_form
     * @apiGroup enrollment_requests
     * @apiDescription get all summer camp forms data from enrollment requests database table
     * @apiSuccess  enrollment_requests Array contain all summer camp forms
     * @apiError  FALSE return false if failure in get data
     */
    function get_summer_camp_form() {
        $where = array('form_name' => 'summer_camp_form',
            'is_deleted' => '0'
        );
        $this->db->where($where);
        $enrollment_requests = $this->enrollment_requests_model->get_all();

        if ($enrollment_requests) {
            send_message($enrollment_requests, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    /**
     * @api {get} /enrollment_requests/register_student_in_school_summer_camp_form register_student_in_school_summer_camp_form
     * @apiName register_student_in_school_summer_camp_form
     * @apiGroup enrollment_requests
     * @apiDescription register student in school
     * @apiSuccess  data_inserted Array contain students records id 
     */
    function register_student_in_school_summer_camp_form($enrollment_requests, $post_data) {
        $room_id_1 = 0;
        if (!isset($post_data['room_id_1'])) {
            
        } else {
            $room_id_1 = $post_data['room_id_1'];
        }
        $room_id_2 = 0;
        if (!isset($post_data['room_id_2'])) {
            
        } else {
            $room_id_2 = $post_data['room_id_2'];
        }
        $room_id_3 = 0;
        if (!isset($post_data['room_id_3'])) {
            
        } else {
            $room_id_3 = $post_data['room_id_3'];
        }
        $room_id_4 = 0;
        if (!isset($post_data['room_id_4'])) {
            
        } else {
            $room_id_4 = $post_data['room_id_4'];
        }

        if (!isset($post_data['grade_id_1'])) {
            
        } else {
            $grade_id_1 = $post_data['grade_id_1'];
        }
        $grade_id_2 = 0;
        if (!isset($post_data['grade_id_2'])) {
            
        } else {
            $grade_id_2 = $post_data['grade_id_2'];
        }
        $grade_id_3 = 0;
        if (!isset($post_data['grade_id_3'])) {
            
        } else {
            $grade_id_3 = $post_data['grade_id_3'];
        }
        $grade_id_4 = 0;
        if (!isset($post_data['grade_id_4'])) {
            
        } else {
            $grade_id_4 = $post_data['grade_id_4'];
        }

        $student_record_id_1 = false;
        $student_record_id_2 = false;
        $student_record_id_3 = false;
        $student_record_id_4 = false;

        $email = $enrollment_requests->email;
        $password = random_password();
        $user_data = array(
            'father_name' => $enrollment_requests->father_name,
            'mother_name' => $enrollment_requests->mother_name,
            'address' => $enrollment_requests->home_address,
            'non_encrept_pass' => $password,
        );

        $parent_data = array(
            'family_name' => $enrollment_requests->family_last_name,
            'father_phone' => $enrollment_requests->father_phone,
            'mother_phone' => $enrollment_requests->mother_phone,
            'home_phone' => $enrollment_requests->home_phone,
            'emergency_name_1' => $enrollment_requests->emergency_name_1,
            'emergency_name_2' => $enrollment_requests->emergency_name_2,
            'emergency_name_3' => $enrollment_requests->emergency_name_3,
            'emergency_phone_1' => $enrollment_requests->emergency_phone_1,
            'emergency_phone_2' => $enrollment_requests->emergency_phone_2,
            'emergency_phone_3' => $enrollment_requests->emergency_phone_3,
            'emergency_relationship_1' => $enrollment_requests->emergency_relationship_1,
            'emergency_relationship_2' => $enrollment_requests->emergency_relationship_2,
            'emergency_relationship_3' => $enrollment_requests->emergency_relationship_3,
            'custody_info_specify' => $enrollment_requests->custody_info_specify,
            'custody_info_other_info' => $enrollment_requests->custody_info_other_info,
        );

        $parent_id = $this->add_parent($parent_data, $user_data, $email, $password);

        if ($parent_id == 'exist_email_with_another_name') {
            return 'exist_email_with_another_name';
        }

        if (isset($parent_id) && $parent_id && $parent_id != 'exist_email_with_another_name') {
            $student_1_name = $enrollment_requests->student_1;
            $student_2_name = $enrollment_requests->student_2;
            $student_3_name = $enrollment_requests->student_3;
            $student_4_name = $enrollment_requests->student_4;

            $student_age_1 = $enrollment_requests->student_age_1;
            $student_age_2 = $enrollment_requests->student_age_2;
            $student_age_3 = $enrollment_requests->student_age_3;
            $student_age_4 = $enrollment_requests->student_age_4;

            $student_grade_1 = $enrollment_requests->student_grade_1;
            $student_grade_2 = $enrollment_requests->student_grade_2;
            $student_grade_3 = $enrollment_requests->student_grade_3;
            $student_grade_4 = $enrollment_requests->student_grade_4;

            $other_information_1 = $enrollment_requests->other_information_1;
            $other_information_2 = $enrollment_requests->other_information_2;
            $other_information_3 = $enrollment_requests->other_information_3;
            $other_information_4 = $enrollment_requests->other_information_4;

            $photo_permission_slips = $enrollment_requests->permission;

            $student_data = array(
                'student_name' => $student_1_name,
                'age' => $student_age_1,
                'grade' => $student_grade_1,
                'student_notes' => $other_information_1,
            );
            $student_id_1 = $this->add_student($parent_id, $student_data);

            if (isset($student_id_1) && $student_id_1 && isset($grade_id_1) && $grade_id_1 && isset($room_id_1) && $room_id_1 && $room_id_1 != 0) {
                $student_record_id_1 = $this->register_student_in_grade($student_id_1, $room_id_1, $grade_id_1, $photo_permission_slips);
            }
            if (isset($student_2_name) && $student_2_name) {
                $student_data2 = array(
                    'student_name' => $student_2_name,
                    'age' => $student_age_2,
                    'grade' => $student_grade_2,
                    'student_notes' => $other_information_2,
                );
                $student_id_2 = $this->add_student($parent_id, $student_data2);

                if (isset($student_id_2) && $student_id_2 && isset($grade_id_2) && $grade_id_2 && isset($room_id_2) && $room_id_2 && $room_id_2 != 0) {
                    $student_record_id_2 = $this->register_student_in_grade($student_id_2, $room_id_2, $grade_id_2, $photo_permission_slips);
                }
            }
            if (isset($student_3_name) && $student_3_name) {
                $student_data3 = array(
                    'student_name' => $student_3_name,
                    'age' => $student_age_3,
                    'grade' => $student_grade_3,
                    'student_notes' => $other_information_3,
                );
                $student_id_3 = $this->add_student($parent_id, $student_data3);
                if (isset($student_id_3) && $student_id_3 && isset($grade_id_3) && $grade_id_3 && isset($room_id_3) && $room_id_3 && $room_id_3 != 0) {
                    $student_record_id_3 = $this->register_student_in_grade($student_id_3, $room_id_3, $grade_id_3, $photo_permission_slips);
                }
            }
            if (isset($student_4_name) && $student_4_name) {
                $student_data4 = array(
                    'student_name' => $student_4_name,
                    'age' => $student_age_4,
                    'student_notes' => $other_information_4,
                );
                $student_id_4 = $this->add_student($parent_id, $student_data4);

                if (isset($student_id_4) && $student_id_4 && isset($grade_id_4) && $grade_id_4 && isset($room_id_4) && $room_id_4 && $room_id_4 != 0) {
                    $student_record_id_4 = $this->register_student_in_grade($student_id_4, $room_id_4, $grade_id_4, $photo_permission_slips);
                }
            }
            $data_inserted = array(
                'student_record_id_1' => $student_record_id_1,
                'student_record_id_2' => $student_record_id_2,
                'student_record_id_3' => $student_record_id_3,
                'student_record_id_4' => $student_record_id_4,
            );
            return $data_inserted;
        }
    }

    /**
     * @api {post} /enrollment_request/delete_enrollment_request delete_enrollment_request
     * @apiName delete_enrollment_request
     * @apiGroup enrollment_request
     * @apiDescription get id of enrollment request to delete its info from enrollment requests database table
     * @apiParam {Number} id id of enrollment request
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_enrollment_request($id, $form_name) {
        $data = array('is_deleted' => '1');

        $delete_item = $this->enrollment_requests_model->update_by(array('id' => $id), $data);
//        $delete_item = $this->enrollment_requests_model->delete_by('id', $id);

        if (isset($delete_item) && $delete_item) {
//            redirect("enrollment_requests/index_registration_form");
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    function save_info_in_enrollment_requests($data, $form_name) {
        $data_form = array();
        if (isset($data->data) && $data->data != 'Invalid Insert to db') {
            if ($form_name == 'registration_form') {
                foreach ($data->data as $key => $value) {
                    $data_form[] = array(
                        'family_last_name' => $value->family_last_name,
                        'student_1' => $value->student_1,
                        'student_age_1' => $value->student_age_1,
                        'student_grade_1' => $value->student_grade_1,
                        'student_2' => $value->student_2,
                        'student_age_2' => $value->student_age_2,
                        'student_grade_2' => $value->student_grade_2,
                        'student_3' => $value->student_3,
                        'student_age_3' => $value->student_age_3,
                        'student_grade_3' => $value->student_grade_3,
                        'student_4' => $value->student_4,
                        'student_age_4' => $value->student_age_4,
                        'student_grade_4' => $value->student_grade_4,
                        'email' => $value->email,
                        'home_address' => $value->home_address,
                        'home_phone' => $value->home_phone,
                        'mother_name' => $value->mother_name,
                        'father_name' => $value->father_name,
                        'mother_phone' => $value->mother_phone,
                        'father_phone' => $value->father_phone,
                        'attended' => $value->attended,
                        'their_grade_level' => $value->their_grade_level,
                        'other_information_1' => $value->other_information_1,
                        'other_information_2' => $value->other_information_2,
                        'other_information_3' => $value->other_information_3,
                        'other_information_4' => $value->other_information_4,
                        'agree' => $value->agree,
                        'contract' => $value->contract,
                        'permission' => $value->permission,
                        'form_name' => $form_name,
                        'emergency_name_1' => $value->emergency_name_1,
                        'emergency_name_2' => $value->emergency_name_2,
                        'emergency_name_3' => $value->emergency_name_3,
                        'emergency_phone_1' => $value->emergency_phone_1,
                        'emergency_phone_2' => $value->emergency_phone_2,
                        'emergency_phone_3' => $value->emergency_phone_3,
                        'emergency_relationship_1' => $value->emergency_relationship_1,
                        'emergency_relationship_2' => $value->emergency_relationship_2,
                        'emergency_relationship_3' => $value->emergency_relationship_3,
                        'custody_info_specify' => $value->custody_info_specify,
                        'custody_info_other_info' => $value->custody_info_other_info,
                        'created_at' => date('Y-m-d h:i:s', time()),
                        'updated_at' => date('Y-m-d h:i:s', time()),
                    );
                }
                $insert = $this->db->insert_batch('enrollment_requests', $data_form);
            } elseif ($form_name == 'arabic_reading_club') {
                foreach ($data->data as $key => $value) {
                    $data_form[] = array(
                        'family_last_name' => $value->family_last_name,
                        'student_1' => $value->student_1,
                        'student_age_1' => $value->student_age_1,
                        'student_grade_1' => $value->student_grade_1,
                        'student_2' => $value->student_2,
                        'student_age_2' => $value->student_age_2,
                        'student_grade_2' => $value->student_grade_2,
                        'student_3' => $value->student_3,
                        'student_age_3' => $value->student_age_3,
                        'student_grade_3' => $value->student_grade_3,
                        'student_4' => $value->student_4,
                        'student_age_4' => $value->student_age_4,
                        'student_grade_4' => $value->student_grade_4,
                        'email' => $value->email,
                        'mother_name' => isset($value->mother_name) ? $value->mother_name : '',
                        'father_name' => isset($value->father_name) ? $value->father_name : '',
                        'mother_phone' => $value->mother_phone,
                        'father_phone' => $value->father_phone,
                        'form_name' => $form_name,
                        'emergency_name_1' => $value->emergency_name_1,
                        'emergency_name_2' => $value->emergency_name_2,
                        'emergency_name_3' => $value->emergency_name_3,
                        'emergency_phone_1' => $value->emergency_phone_1,
                        'emergency_phone_2' => $value->emergency_phone_2,
                        'emergency_phone_3' => $value->emergency_phone_3,
                        'emergency_relationship_1' => $value->emergency_relationship_1,
                        'emergency_relationship_2' => $value->emergency_relationship_2,
                        'emergency_relationship_3' => $value->emergency_relationship_3,
                        'custody_info_specify' => $value->custody_info_specify,
                        'custody_info_other_info' => $value->custody_info_other_info,
                        'created_at' => date('Y-m-d h:i:s', time()),
                        'updated_at' => date('Y-m-d h:i:s', time()),
                    );
                }
                $insert = $this->db->insert_batch('enrollment_requests', $data_form);
            } elseif ($form_name == 'summer_camp_form') {
                foreach ($data->data as $key => $value) {
                    $data_form[] = array(
                        'family_last_name' => $value->family_last_name,
                        'student_1' => $value->student_1,
                        'student_age_1' => $value->student_age_1,
                        'student_grade_1' => $value->student_grade_1,
                        'student_2' => $value->student_2,
                        'student_age_2' => $value->student_age_2,
                        'student_grade_2' => $value->student_grade_2,
                        'student_3' => $value->student_3,
                        'student_age_3' => $value->student_age_3,
                        'student_grade_3' => $value->student_grade_3,
                        'student_4' => $value->student_4,
                        'student_age_4' => $value->student_age_4,
                        'student_grade_4' => $value->student_grade_4,
                        'email' => $value->email,
                        'home_address' => ($value->home_address) ? $value->home_address : '',
                        'home_phone' => $value->home_phone,
                        'mother_name' => $value->mother_name,
                        'father_name' => $value->father_name,
                        'mother_phone' => $value->mother_phone,
                        'father_phone' => $value->father_phone,
                        'attended' => $value->attended,
                        'their_grade_level' => $value->their_grade_level,
                        'other_information_1' => $value->other_information_1,
                        'other_information_2' => $value->other_information_2,
                        'other_information_3' => $value->other_information_3,
                        'other_information_4' => $value->other_information_4,
                        'agree' => $value->agree,
                        'contract' => $value->contract,
                        'permission' => $value->permission,
                        'form_name' => $form_name,
                        'emergency_name_1' => $value->emergency_name_1,
                        'emergency_name_2' => $value->emergency_name_2,
                        'emergency_name_3' => $value->emergency_name_3,
                        'emergency_phone_1' => $value->emergency_phone_1,
                        'emergency_phone_2' => $value->emergency_phone_2,
                        'emergency_phone_3' => $value->emergency_phone_3,
                        'emergency_relationship_1' => $value->emergency_relationship_1,
                        'emergency_relationship_2' => $value->emergency_relationship_2,
                        'emergency_relationship_3' => $value->emergency_relationship_3,
                        'custody_info_specify' => $value->custody_info_specify,
                        'custody_info_other_info' => $value->custody_info_other_info,
//                        'book_fees' => $value->book_fees,
                        'type' => $value->type,
//                        'date' => $value->date,
                        'created_at' => date('Y-m-d h:i:s', time()),
                        'updated_at' => date('Y-m-d h:i:s', time()),
                    );
                }
                $insert = $this->db->insert_batch('enrollment_requests', $data_form);
            } elseif ($form_name == 'youth_group_registration_form') {

                foreach ($data->data as $key => $value) {
                    $data_form[] = array(
//                        'family_last_name' => $value->family_last_name,
                        'student_1' => $value->student_name,
                        'student_age_1' => $value->student_age,
                        'email' => $value->email,
                        'home_address' => $value->home_address,
                        'home_phone' => $value->home_phone,
                        'mother_name' => $value->mother_name,
                        'father_name' => $value->father_name,
                        'mother_phone' => $value->mother_phone,
                        'father_phone' => $value->father_phone,
                        'permission' => $value->permission,
                        'date' => $value->date,
                        'form_name' => $form_name,
                        'emergency_name_1' => $value->emergency_name_1,
                        'emergency_name_2' => $value->emergency_name_2,
                        'emergency_name_3' => $value->emergency_name_3,
                        'emergency_phone_1' => $value->emergency_phone_1,
                        'emergency_phone_2' => $value->emergency_phone_2,
                        'emergency_phone_3' => $value->emergency_phone_3,
                        'emergency_relationship_1' => $value->emergency_relationship_1,
                        'emergency_relationship_2' => $value->emergency_relationship_2,
                        'emergency_relationship_3' => $value->emergency_relationship_3,
                        'custody_info_specify' => $value->custody_info_specify,
                        'custody_info_other_info' => $value->custody_info_other_info,
                        'created_at' => date('Y-m-d h:i:s', time()),
                        'updated_at' => date('Y-m-d h:i:s', time()),
                    );
                }
                $insert = $this->db->insert_batch('enrollment_requests', $data_form);
            }
        }
    }

}
