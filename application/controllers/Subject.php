<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('subjects_model');
        $this->load->model('grades_model');
        $this->load->model('subjects_model');
        $this->load->model('parent_subjects_model');
        $this->load->model('rooms_teachers_model');
    }

    /**
     * @api {get} /subject/index_subject index_subject
     * @apiName index_subject
     * @apiGroup subject
     * @apiDescription home page of subjects for specific grade
     * @apiParam {Number} id id of grade
     */
    function index_subject($grade_id) {
        $grades = $this->grades_model->get_by('id', $grade_id);
        $this->data['grade_name'] = $grades->grade_name;

        $parent_subjects = $this->parent_subjects_model->get_parent_subjects();

        $parent_subject_options = array();
        foreach ($parent_subjects as $value) {
            $parent_subject_options[$value->id] = lang($value->parent_subject_name);
        }
        $this->data['parent_subject_options'] = $parent_subject_options;

        $this->data['show_object_link'] = 'subject/show_subjects/' . $grade_id;
        $this->data['get_object_link'] = 'subject/get_subject';
        $this->data['add_object_link'] = 'subject/add_subject/' . $grade_id;
        $this->data['update_object_link'] = 'subject/update_subject/' . $grade_id;
        $this->data['delete_object_link'] = 'subject/delete_subject/' . $grade_id;
        $this->data['modal_name'] = 'crud_subject';
        $this->data['add_object_title'] = lang("add_new_subject");
        $this->data['update_object_title'] = lang("edit_subject");
        $this->data['delete_object_title'] = lang("delete_subject");
        $this->data['thead'] = array('subject_name', 'parent_subject_id');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /subject/show_subjects show_subjects
     * @apiName show_subjects
     * @apiGroup subject
     * @apiDescription get all subjects for specific grade
     * @apiParam {Number} id id of grade
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in subjects table
     * @apiError  FALSE return false if failure in get data
     */
    function show_subjects($grade_id) {
        $subjects = $this->subjects_model->get_subjects($grade_id);
        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($subjects as $subject) {
            $no++;
            $row = array();
            $row[] = $subject->subject_name;
            $row[] = lang($subject->parent_subject_name);
//            $row[] = $subject->min_mark;
//            $row[] = $subject->max_mark;

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($subject->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($subject->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->subjects_model->count_all_subjects($grade_id),
            "recordsFiltered" => $this->subjects_model->count_filtered_crud_subjects($grade_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /subject/add_subject add_subject
     * @apiName add_subject
     * @apiGroup subject
     * @apiDescription add subject data to subjects database table
     * @apiParam {Number} grade_id id of grade
     * @apiParam {String} subject_name Name of subject
     * @apiParam {Number} parent_subject_id id of parent subject
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  subject_name_already_exist202 return subject name already exist error message
     */
    function add_subject($grade_id) {
        $input_array = array(
            'subject_name' => "required",
            'parent_subject_id' => "required",
//            'min_mark' => "required|numeric",
//            'max_mark' => "required|numeric",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $subject_name = $this->input->post('subject_name');
            $parent_subject_id = $this->input->post('parent_subject_id');
//            $min_mark = $this->input->post('min_mark');
//            $max_mark = $this->input->post('max_mark');
//            if ($min_mark > $max_mark) {
//                send_message(lang('min_mark_should_be_less_than_max_mark'), '201', 'min_mark_should_be_less_than_max_mark');
//            }

            $check_data = array(
                'grade_id' => $grade_id,
                'subject_name' => $subject_name,
            );

            if (exist_item("subjects_model", $check_data)) {
                send_message("", '202', 'subject_name_already_exist');
            }

            $data = array(
                'grade_id' => $grade_id,
                'subject_name' => $subject_name,
                'parent_subject_id' => $parent_subject_id,
//                'min_mark' => $min_mark,
//                'max_mark' => $max_mark,
            );

            $insert = $this->subjects_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /subject/get_subject get_subject
     * @apiName get_subject
     * @apiGroup subject
     * @apiDescription get id of subject to get its info from subjects database table
     * @apiParam {Number} id id of subject
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of subject from subjects table
     * @apiError  FALSE return false if failure in get data
     */
    function get_subject($id) {
        $data = $this->subjects_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /subject/update_subject update_subject
     * @apiName update_subject
     * @apiGroup subject
     * @apiDescription update subject data in subjects database table
     * @apiParam {Number} grade_id id of grade
     * @apiParam {String} subject_name Name of subject
     * @apiParam {Number} parent_subject_id id of parent subject
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  subject_name_already_exist202 return subject name already exist error message
     */
    function update_subject($grade_id) {
        $subjects = $this->subjects_model->get_subjects($grade_id);

        $input_array = array(
            'subject_name' => "required",
            'parent_subject_id' => "required",
//            'min_mark' => "required|numeric",
//            'max_mark' => "required|numeric",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $subject_name = $this->input->post('subject_name');
            $parent_subject_id = $this->input->post('parent_subject_id');
//            $min_mark = $this->input->post('min_mark');
//            $max_mark = $this->input->post('max_mark');
            // start validation
//            if ($min_mark > $max_mark) {
//                send_message(lang('min_mark_should_be_less_than_max_mark'), '201', 'min_mark_should_be_less_than_max_mark');
//            }
//
//            if (exist_item("subjects_model", array('subject_name' => $subject_name, 'grade_id' => $grade_id, 'id <>' => $item_id))) {
//                send_message('', "202", 'subject_name_already_exist');
//            }
            validation_edit_delete_redirect($subjects, "id", $item_id);
            // end validation

            $data = array(
                'grade_id' => $grade_id,
                'subject_name' => $subject_name,
                'parent_subject_id' => $parent_subject_id,
//                'min_mark' => $min_mark,
//                'max_mark' => $max_mark,
            );
            $update_item = $this->subjects_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /subject/delete_subject delete_subject
     * @apiName delete_subject
     * @apiGroup subject
     * @apiDescription get id of subject to delete its info from subjects database table
     * @apiParam {Number} grade_id id of grade
     * @apiParam {Number} id id of subject
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_subject($grade_id, $id) {
        $subjects = $this->subjects_model->get_subjects($grade_id);
        //start validation
        validation_edit_delete_redirect($subjects, "id", $id);
        // end validation

        $related = $this->rooms_teachers_model->get_if_teacher_subject($id);

        if (isset($related) && $related) {
            error_message('407', 'have_son_error');
        } else {
            $delete_item = $this->subjects_model->delete_by('id', $id);
            if (isset($delete_item) && $delete_item) {
                send_message("", '200', 'delete_success');
            } else {
                error_message('400', 'delete_error');
            }
        }
    }

}
