<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transferring_students extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_records_model');
        $this->load->model('students_model');
        $this->load->model('types_payments_model');
        $this->load->model('payments_model');
        $this->load->model('rooms_model');
        $this->load->model('ion_auth_model');
        $this->load->model('grades_model');
        $this->load->model('rooms_model');
        $this->load->model('general_model', 'users_model');
        $this->users_model->set_table('users');
    }

    function index() {
        
    }

    /**
     * @api {get} /transferring_students/index_transferring_students index_transferring_students
     * @apiName index_transferring_students
     * @apiGroup transferring_students
     * @apiDescription home page of student records
     * @apiParam {Number} room_id id of room  
     */
    function index_transferring_students($room_id) {
        $this->data['room_name'] = $this->rooms_model->get_room($room_id);
        $transferring_students = $this->students_records_model->get_students_records($room_id);
        $this->data['room_id'] = $room_id;
        $this->data['rooms'] = $rooms = $this->rooms_model->get_rooms();

        $rooms_options = array();
        if (isset($rooms) && $rooms) {
            foreach ($rooms as $value) {
                if ($value->id != $room_id)
                    $rooms_options[$value->id] = $value->grade_name . ' - ' . $value->room_name;
            }
            $this->data['rooms_options'] = $rooms_options;
        }

        $this->data['students'] = $transferring_students;
//         $this->data['display_non_column'] = $display_non_column=true;
        $this->data['show_object_link'] = 'transferring_students/show_transferring_students/' . $room_id;
        $this->data['get_object_link'] = 'transferring_students/get_transferring_students';
        $this->data['add_object_link'] = 'transferring_students/add_transferring_students/' . $room_id;
        $this->data['update_object_link'] = 'transferring_students/update_transferring_students/' . $room_id;
        $this->data['delete_object_link'] = 'transferring_students/delete_transferring_students/' . $room_id;
        $this->data['modal_name'] = 'crud_transferring_students';
        $this->data['add_object_title'] = lang("add_new_transferring_students");
        $this->data['update_object_title'] = lang("update_transferring_students");
        $this->data['delete_object_title'] = lang("delete_transferring_students");
        $this->data['thead'] = array('bs3', 'hidden', 'student_name', 'father_name', 'mother_name');
    }

    /**
     * @api {get} /transferring_students/show_transferring_students show_transferring_students
     * @apiName show_transferring_students
     * @apiGroup transferring_students
     * @apiDescription get all student records
     * @apiParam {Number} room_id id of room  
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student records table
     * @apiError  FALSE return false if failure in get data
     */
    function show_transferring_students($room_id) {
        $transferring_students = $this->students_records_model->get_students_records($room_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($transferring_students as $transferring_students) {
            $no++;
            $row = array();

            $row[] = bs3_checkbox_for_table($transferring_students->id, 'i-check');
            $row[] = $transferring_students->id;
//            $row[] = $transferring_students->student_name . ' ' . $transferring_students->family_name;
            $row[] = bs3_link_crud("profile/page_student_profile/" . $transferring_students->student_id, $transferring_students->student_name . ' ' . $transferring_students->family_name, '', 'update');

            $row[] = $transferring_students->father_name;
            $row[] = $transferring_students->mother_name;

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_records_model->count_all_students_records($room_id),
            "recordsFiltered" => $this->students_records_model->count_filtered_crud_students_records($room_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /transferring_students/add_transferring_students add_transferring_students
     * @apiName add_transferring_students
     * @apiGroup transferring_students
     * @apiDescription transferring students from room in room in same grade or from room to room in another grade
     * @apiParam {Number} student_key id of student 
     * @apiParam {Boolean} is_active value of active field to know status of student record 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_transferring_students($room_id) {
        $input_array = array(
            'student_id' => "required",
            'is_active' => "required",
        );
        $this->_validation($input_array);

        $student_id = $this->input->post('student_key');

        $is_active = $this->input->post('is_active');
        $is_active = 1;
        if ($this->input->post('is_active')) {
            $is_active = 2;
        }


        if ($this->form_validation->run()) {
            $student_id = $this->input->post('student_key');

            if ($student_id != 0) {
                $where = array(
                    'student_id' => $student_id,
                    'room_id' => $room_id,
                    'is_active' => $is_active,
                );
                if (exist_item("students_records_model", $where)) {
                    send_message('', "202", 'student_already_exist_in_room');
                }

                $where = array(
                    'student_id' => $student_id,
                    'room_id <>' => $room_id,
                    'is_active' => $is_active,
                );
                if (exist_item("students_records_model", $where)) {
                    send_message('', "202", 'student_already_exist_in_another_room');
                }

//create transferring_students
                $data = array(
                    'student_id' => $student_id,
                    'room_id' => $room_id,
                    'is_active' => $is_active,
                );
                $insert = $this->students_records_model->insert($data);
                if (isset($insert) && $insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /transferring_students/get_transferring_students get_transferring_students
     * @apiName get_transferring_students
     * @apiGroup transferring_students
     * @apiDescription get id of student record to get its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student record from student records table
     * @apiError  FALSE return false if failure in get data
     */
    function get_transferring_students($id) {
        $data = $this->students_records_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /transferring_students/update_transferring_students update_transferring_students
     * @apiName update_transferring_students
     * @apiGroup transferring_students
     * @apiDescription update student record data in student records database table
     * @apiParam {Number} id id of student record
     * @apiParam {Number} student_key id of student 
     * @apiParam {Boolean} is_active value of active field to know status of student record 
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  student_already_exist_in_room202 return JSON encoded string contain error message if student is already exist in  another room
     */
    function update_transferring_students($room_id) {
        $input_array = array(
            'student_id' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $student_id = $this->input->post('student_key');
            $is_active = $this->input->post('is_active');

            $is_active = 1;
            if ($this->input->post('is_active')) {
                $is_active = 2;
            }

            $where = array(
                'student_id' => $student_id,
                'room_id' => $room_id,
                'is_active' => $is_active,
                'id <>' => $item_id,
            );
            if (exist_item("students_records_model", $where)) {
                send_message('', "202", 'student_already_exist_in_room');
            }

            $data = array(
                'is_active' => $is_active,
            );

            $update_item = $this->students_records_model->update_by(array('id' => $item_id), $data);

            if (isset($update_item) && $update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /transferring_students/delete_transferring_students delete_transferring_students
     * @apiName delete_transferring_students
     * @apiGroup transferring_students 
     * @apiDescription get id of student record to delete its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiParam {Number} room_id id of room
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_transferring_students($room_id, $id = '') {
        $where = array('room_id' => $room_id);
        $transferring_studentss = $this->students_records_model->get_many_by($where);
//start validation
        validation_edit_delete_redirect($transferring_studentss, "id", $id);
// end validation

        $delete_item = $this->students_records_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /transferring_students/movements movements
     * @apiName movements
     * @apiGroup transferring_students
     * @apiDescription get id of student record to get its info from student records database table
     * @apiParam {Number} id id of student record
     * @apiParam {Number} room id of room
     * @apiSuccess  move_success return JSON encoded string contain data of student record from student records table
     * @apiError  move_error return false if failure in get data
     */
    public function movements($current_room) {
        $data = json_decode(stripslashes($_POST['data']));
        $room_id = $this->input->post('room');

        $grade_of_current_room = $this->rooms_model->get_by('id', $current_room);
        $grade_of_move_room = $this->rooms_model->get_by('id', $room_id);

        if ((isset($grade_of_current_room) && $grade_of_current_room) && (isset($grade_of_move_room) && $grade_of_move_room)) {
            if ($grade_of_current_room->grade_id == $grade_of_move_room->grade_id) {
                $edit_status = false;
                foreach ($data as $value) {
                    $where = array(
                        'id' => $value,
                    );
                    $data = array(
                        'room_id' => $room_id,
                    );
                    $edit_status = $this->students_records_model->update_by($where, $data);
                }
                if (isset($edit_status) && $edit_status) {
                    send_message('', '200', 'move_success');
                } else {
                    error_message('400', 'move_error');
                }
            } else {
                $edit_status = false;
                foreach ($data as $value) {
                    if ($value == '')
                        continue;
                    $get_student_id = $this->students_records_model->get_by('id', $value);

                    $where = array(
                        'id' => $value,
                    );
                    $data = array(
                        'is_active' => 1,
                    );
                    $edit_status = $this->students_records_model->update_by($where, $data);

                    if ($edit_status && $edit_status) {
                        $data_insert = array(
                            'student_id' => $get_student_id->student_id,
                            'room_id' => $room_id,
                            'photo_permission_slips' => $get_student_id->photo_permission_slips,
                            'islamic_book_return' => $get_student_id->islamic_book_return,
                            'status_islamic_book_return' => $get_student_id->status_islamic_book_return,
                            'student_order' => $get_student_id->student_order,
                            'year' => $this->_current_year,
                            'is_active' => 2,
                        );
                        $insert_status = $this->students_records_model->insert($data_insert);
                    }
                }
                if (isset($edit_status) && $edit_status) {
                    send_message('', '200', 'move_success');
                } else {
                    error_message('400', 'move_error');
                }
            }
        }
    }

    /**
     * @api {get} /transferring_students/get_all_data get_all_data
     * @apiName movements
     * @apiGroup get_all_data
     * @apiDescription get all students records in specific room
     * @apiParam {Number} room id of room
     * @apiSuccess  transferring_students Array contain students records in specific room
     */
    function get_all_data($room_id) {
        $transferring_students = $this->students_records_model->get_students_records($room_id);
        send_message($transferring_students, "200", "success");
    }

}
