<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('grades_model');
        $this->load->model('students_records_model');
        $this->load->model('student_payment_months_model');
        $this->load->model('students_payments_model');
        $this->load->model('rooms_attendances_model');
        $this->load->model('general_model', 'students_attendances_model');
        $this->students_attendances_model->set_table('students_attendances');
    }

    /**
     * @api {get} /report/index_report index_report
     * @apiName index_report
     * @apiGroup report
     * @apiDescription home page of report 
     */
    function index_report() {
        
    }

    // ==================================================================================
    // ******************** start student_non_active_manually report ********************
    // ==================================================================================
    /**
     * @api {get} /report/student_non_active_manually student_non_active_manually
     * @apiName student_non_active_manually
     * @apiGroup report
     * @apiDescription home page of report student non active manually 
     */
    function student_non_active_manually() {
        $grades = $this->grades_model->get_grades();

        $grades_options = array();
        $grades_options['0'] = lang('select_grade');

        foreach ($grades as $value) {
            $grades_options[$value->id] = $value->grade_name;
        }
        $this->data['grades'] = $grades_options;
    }

    /**
     * @api {post} /report/search_student_non_active_manually search_student_non_active_manually
     * @apiName search_student_non_active_manually
     * @apiGroup report
     * @apiDescription Get information of student dependence on selected grade 
     * @apiParam {Number} grade id of grade
     * @apiSuccess  non_active_students_records_array Array contain data of students info
     */
    function search_student_non_active_manually() {
        $grade_id = $this->input->post('grade');

        $students = $this->students_records_model->get_non_active_manually_students_records($grade_id);

        $non_active_students_records_array = array();
        if (isset($students) && $students) {
            foreach ($students as $item) {
                $res = array();
                $res["id"] = $item->id;
                $res["student_name"] = $item->student_name . " " . $item->family_name;
                $res["father_name"] = $item->father_name;
                $res["mother_name"] = $item->mother_name;
                $res["room_name"] = $item->room_name;
                $non_active_students_records_array[] = $res;
            }
        } else {
            error_message('405', 'no_students');
        }

        if ($non_active_students_records_array) {
            send_message($non_active_students_records_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    // ==================================================================================
    // *****************  start students who did not give permission report *************
    // ==================================================================================
    /**
     * @api {get} /report/students_who_did_not_give_permission students_who_did_not_give_permission
     * @apiName students_who_did_not_give_permission
     * @apiGroup report
     * @apiDescription home page of report students who did not give photo permission
     */
    function students_who_did_not_give_permission() {
        $grades = $this->grades_model->get_grades();

        $grades_options = array();
        $grades_options['0'] = lang('select_grade');

        foreach ($grades as $value) {
//            if ($value->stage_id == '2')
            $grades_options[$value->id] = $value->grade_name;
        }
        $this->data['grades'] = $grades_options;
    }

    /**
     * @api {post} /report/search_students_who_did_not_give_permission search_students_who_did_not_give_permission
     * @apiName search_students_who_did_not_give_permission
     * @apiGroup report
     * @apiDescription Get information of student dependence on selected grade 
     * @apiParam {Number} grade id of grade
     * @apiSuccess  not_give_permission_students_records_array Array contain data of students info
     */
    function search_students_who_did_not_give_permission() {
        $grade_id = $this->input->post('grade');

        $students = $this->students_records_model->get_students_who_did_not_give_permission($grade_id);

        $not_give_permission_students_records_array = array();
        if (isset($students) && $students) {
            foreach ($students as $item) {
                $res = array();
                $res["id"] = $item->id;
                $res["student_name"] = $item->student_name . " " . $item->family_name;
                $res["father_name"] = $item->father_name;
                $res["mother_name"] = $item->mother_name;
                $res["room_name"] = $item->room_name;
                $not_give_permission_students_records_array[] = $res;
            }
        } else {
            error_message('405', 'no_students');
        }

        if ($not_give_permission_students_records_array) {
            send_message($not_give_permission_students_records_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    // ==================================================================================
    // ***************  start students who did not return the books report **************
    // ==================================================================================
    /**
     * @api {get} /report/students_who_did_not_return_the_books students_who_did_not_return_the_books
     * @apiName students_who_did_not_return_the_books
     * @apiGroup report
     * @apiDescription home page of report students who did not return the books
     */
    function students_who_did_not_return_the_books() {
        $grades = $this->grades_model->get_grades();

        $grades_options = array();
        $grades_options['0'] = lang('select_grade');

        foreach ($grades as $value) {
//            if ($value->stage_id == '2')
            $grades_options[$value->id] = $value->grade_name;
        }
        $this->data['grades'] = $grades_options;
    }

    /**
     * @api {post} /report/search_students_who_did_not_return_the_books search_students_who_did_not_return_the_books
     * @apiName search_students_who_did_not_return_the_books
     * @apiGroup report
     * @apiDescription Get information of student dependence on selected grade 
     * @apiParam {Number} grade id of grade
     * @apiSuccess  not_return_the_books_students_records_array Array contain data of students info
     * @apiError ErrorMessage405 no student found
     */
    function search_students_who_did_not_return_the_books() {
        $grade_id = $this->input->post('grade');

        $students = $this->students_records_model->get_students_who_did_not_return_the_books($grade_id);

        $not_return_the_books_students_records_array = array();
        if (isset($students) && $students) {
            foreach ($students as $item) {
                $res = array();
                $res["id"] = $item->id;
                $res["student_name"] = $item->student_name . " " . $item->family_name;
                $res["father_name"] = $item->father_name;
                $res["mother_name"] = $item->mother_name;
                $res["room_name"] = $item->room_name;
                $not_return_the_books_students_records_array[] = $res;
            }
        } else {
            error_message('405', 'no_students');
        }

        if ($not_return_the_books_students_records_array) {
            send_message($not_return_the_books_students_records_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    // ==================================================================================
    // ***************************  start number of students report *********************
    // ==================================================================================
    /**
     * @api {get} /report/number_of_students number_of_students
     * @apiName number_of_students
     * @apiGroup report
     * @apiDescription home page of report number of students 
     */
    function number_of_students() {
        $grades = $this->grades_model->get_grades();

        $grades_options = array();
        $grades_options['0'] = lang('select_grade');
        $grades_options['all'] = lang('all_grades');

        foreach ($grades as $value) {
            $grades_options[$value->id] = $value->grade_name;
        }
        $this->data['grades'] = $grades_options;
    }

    /**
     * @api {post} /report/search_number_of_students search_number_of_students
     * @apiName search_number_of_students
     * @apiGroup report
     * @apiDescription Get number of student dependence on selected grade 
     * @apiParam {Number} grade id of grade
     * @apiSuccess  number_of_students_array Array contain number of students 
     * @apiError ErrorMessage405 no student found
     */
    function search_number_of_students($grade_id) {
//        $grade_id = $this->input->post('grade');

        $number_of_students_array = array();
        if ($grade_id != 'all') {
            $number_of_students = $this->students_records_model->get_number_of_students($grade_id);
            if (isset($number_of_students) && $number_of_students) {
                $res = array();
                $res["grade_name"] = $number_of_students['grade_name'];
                $res["students_number_s1"] = $number_of_students['students_number_s1'];
                $res["students_number_s2"] = $number_of_students['students_number_s2'];
                $res["students_number_end_year"] = $number_of_students['students_number_end_year'];
                $number_of_students_array[] = $res;
            } else {
                error_message('405', 'no_students');
            }
        } else {
            $number_of_students = $this->students_records_model->get_number_of_students_in_all_grades($grade_id);

            foreach ($number_of_students as $value) {
                $res = array();
                $res["grade_name"] = $value['grade_name'];
                $res["students_number_s1"] = $value['students_number_s1'];
                $res["students_number_s2"] = $value['students_number_s2'];
                $res["students_number_end_year"] = $value['students_number_end_year'];
                $number_of_students_array[] = $res;
            }
        }

        if ($number_of_students_array) {
            send_message($number_of_students_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    // ==================================================================================
    // **************************  start student payment report *************************
    // ==================================================================================
    /**
     * @api {get} /report/students_payments students_payments
     * @apiName students_payments
     * @apiGroup report
     * @apiDescription home page of students payments report 
     */
    function students_payments() {
        $this->data['monthes_options'] = $monthes = get_monthes();
    }

    /**
     * @api {post} /report/search_students_payments search_students_payments
     * @apiName search_students_payments
     * @apiGroup report
     * @apiDescription Get information of student dependence on selected monthes 
     * @apiParam {String} monthe_name name of month
     * @apiSuccess  students_not_paid_array Array contain data of students info
     * @apiError ErrorMessage405 no student found
     */
    function search_students_payments() {
        $monthes = get_monthes();
        $month_name = array();
        foreach ($monthes as $key => $value) {
            if ($this->input->post($key) != null)
                $month_name[$key] = $this->input->post($key);
        }
        if (isset($month_name) && $month_name) {
            $students_not_paid = $this->student_payment_months_model->get_students_not_paid($month_name);
        }

        $students_not_paid_array = array();

        if (isset($students_not_paid) && $students_not_paid) {
            foreach ($students_not_paid as $value) {
                $res = array();
                $res["student_name"] = $value->student_name . ' ' . $value->family_name;
                $res["father_name"] = $value->father_name;
                $res["grade_name"] = $value->grade_name;
                $res["room_name"] = $value->room_name;

                $students_not_paid_array[] = $res;
            }
        } else {
            error_message('405', 'no_students');
        }

        if ($students_not_paid_array) {
            send_message($students_not_paid_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    // ==================================================================================
    // ***************************  start number of students balances report *********************
    // ==================================================================================
    /**
     * @api {get} /report/students_balances students_balances
     * @apiName students_balances
     * @apiGroup report
     * @apiDescription home page of students balances report 
     */
    function students_balances() {
        $balance_options = array(
            '0' => lang('select_balance_option'),
            'balance_less_than_zero' => lang('balance_less_than_zero'),
            'balance_greater_than_zero' => lang('balance_greater_than_zero'),
        );

        $this->data['balance_options'] = $balance_options;
    }

    /**
     * @api {post} /report/search_students_balances search_students_balances
     * @apiName search_students_balances
     * @apiGroup report
     * @apiDescription Get information of student dependence on selected balance 
     * @apiParam {String} balance_option balance option less or greater than zero 
     * @apiSuccess  students_not_paid_array Array contain data of students info
     * @apiError ErrorMessage405 no student found
     */
    function search_students_balances($balance_option) {
        $students = $this->students_records_model->get_all_students_in_year();

        $students_array = array();
        foreach ($students as $value) {
            $students_array[$value->student_id] = $value->student_id;
        }
        $student_payments = $this->students_payments_model->get_students_payments_for_all_students_in_year($students_array);

        if (isset($student_payments) && $student_payments) {
            if ($balance_option == 'balance_less_than_zero') {
                $students_balance_less_than_zero = array();

                foreach ($student_payments as $value) {
                    $balance = $value->payment_sum - $value->invoice_sum;
                    if ($balance < 0) {
                        $value->student_balance = '$' . $balance;
                        $students_balance_less_than_zero[$value->student_id] = $value;
                    }
                }
                if ($students_balance_less_than_zero) {
                    send_message($students_balance_less_than_zero, '200', 'success');
                } else {
                    error_message('400', 'error');
                }
            } elseif ($balance_option == 'balance_greater_than_zero') {
                $students_balance_greater_than_zero = array();

                foreach ($student_payments as $value) {
                    $balance = $value->payment_sum - $value->invoice_sum;
                    if ($balance > 0) {
                        $value->student_balance = '$' . $balance;
                        $students_balance_greater_than_zero[$value->student_id] = $value;
                    }
                }
                if ($students_balance_greater_than_zero) {
                    send_message($students_balance_greater_than_zero, '200', 'success');
                } else {
                    error_message('400', 'error');
                }
            } else {
                error_message('400', 'error');
            }
        } else {
            error_message('400', 'error');
        }
    }

    // ==================================================================================
    // ***************************  start students attendances report *********************
    // ==================================================================================
    /**
     * @api {get} /report/students_attendances students_attendances
     * @apiName students_attendances
     * @apiGroup report
     * @apiDescription home page of report attendances of students 
     */
    function students_attendances() {
        $grades = $this->grades_model->get_grades();

        $grades_options = array();
        $grades_options['0'] = lang('select_grade');
        $grades_options['all'] = lang('all_grades');

        foreach ($grades as $value) {
            $grades_options[$value->id] = $value->grade_name;
        }
        $this->data['grades'] = $grades_options;

        $attendances_type = get_attendances_type();

        $attendances_type_options = array();
        $attendances_type_options['0'] = lang('select_attendance_type');

        foreach ($attendances_type as $key => $value) {
            $attendances_type_options[$key] = lang($key);
        }

        $this->data['attendances_type'] = $attendances_type_options;
    }

    /**
     * @api {post} /report/search_students_attendances search_students_attendances
     * @apiName search_students_attendances
     * @apiGroup report
     * @apiDescription Get attendances of student dependence on selected items 
     * @apiParam {Number} grade id of grade
     * @apiSuccess  students_attendances_array Array contain number of students 
     * @apiError ErrorMessage405 no student found
     */
    function search_students_attendances() {
        $grade_id = $this->input->post('grade_id');
        $attendance_date = $this->input->post('attendance_date');
        $attendance_type = $this->input->post('attendance_type');

        if ($attendance_date)
            $attendance_date = date_format(date_create_from_format('m-d-Y', $attendance_date), 'Y-m-d');

        $attendances_students_array = array();
        if ($grade_id != 'all') {
            $students_records = $this->students_records_model->get_attendances_for_specific_attendance_type_date_and_grade($grade_id, $attendance_type, $attendance_date);

            foreach ($students_records as $value) {
                $attendances_students_array[] = $value;
            }
        } else {
            $students_records = $this->students_records_model->get_attendances_for_specific_attendance_type_date_and_grade($grade_id, $attendance_type, $attendance_date);

            foreach ($students_records as $value) {
                $attendances_students_array[] = $value;
            }
        }

        if ($attendances_students_array) {
            send_message($attendances_students_array, '200', 'success');
        } else {
            error_message('400', 'error');
        }
    }

    function delete_all_empty_in_students_attendence() {
        $where1 = array('attendance_type' => '');
        $where2 = array('attendance_type' => null);
        $this->db->where($where1);
        $this->db->or_where($where2);
        $s = $this->students_attendances_model->get_all();
//        var_dump($s);
        foreach ($s as $value) {
            $this->students_attendances_model->delete_by(array('id'=> $value->id));
        }
    }

}
