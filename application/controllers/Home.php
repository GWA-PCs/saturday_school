<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('parents_model');
    }

    function home_page() {
        
    }

    /**
     * @api {get} /home/home_page_teacher home_page_teacher
     * @apiName home_page_teacher
     * @apiGroup home
     * @apiDescription links of teacher pages should display in home page 
     */
    function home_page_teacher() {
        $teacher_rooms = array();
        $teacher_rooms = $this->_get_rooms_of_teacher();
        $this->data['teacher_rooms'] = $teacher_rooms;
    }

    function home_page_student() {
        
    }

    /**
     * @api {get} /home/home_page_parent home_page_parent
     * @apiName home_page_parent
     * @apiGroup home
     * @apiParam {Number} user_login_id user_id of logged in parent
     * @apiDescription home page of parent account to show children of logged in parent
     */
    function home_page_parent() {
        
    }

    /**
     * @api {get} /home/show_parent_children show_parent_children
     * @apiName show_parent_children
     * @apiGroup home
     * @apiDescription get all children of logged in parent
     * @apiParam {Number} parent_id Id of parent
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in home page of parent account
     * @apiError  FALSE return false if failure in get data
     */
    function show_parent_children($parent_id) {
        $parents = $this->parents_model->get_parent_children($parent_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($parents as $parent) {
            $no++;
            $row = array();
            //add html for action
            $row[] = bs3_link_crud("profile/page_student_profile/" . $parent->student_id, $parent->student_name . ' ' . $parent->family_name, '', 'update');
            $row[] = bs3_link_crud("student_payment/index_student_payment/" . $parent->student_id, '<i class="fa fa-money fa-2x "></i>', lang('index_student_payment'), 'update');
            $row[] = bs3_link_crud("room_attendance/index_student_attendance/" . $parent->student_id, '<i class="mdi mdi-account-minus fa-2x "></i>', lang('index_student_attendance'), 'update');
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => count($parents),
            "recordsFiltered" => count($parents),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    function template() {
        $this->data['absence_type_option'] = $absence_type_option = array(
            '0' => 'aaaaaaaaaaaaaaaaaaaaaa',
            '1' => 'aaaaaaaaaaaaaasfsdfsdfaaaaaaaa',
        );
        if ($this->_update) {

            $this->session->set_flashdata('messages_error', 'hhhhhhhdddddddddhhhhhhhhhhhhhhhh');
            redirect('/employee/home_employee/index');
        }
    }

    function teacher_room($room_id) {
        $user_info = $this->_user_login;
        $teacher_id = $this->teachers_model->get_teacher_id_by_user_id($user_info->id);
        $rooms_info = $this->rooms_teachers_model->get_room_teacher_info_by_room_id_and_teacher_id($room_id, $teacher_id);

        $this->data['rooms_info'] = $rooms_info;
    }

}
