<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_allergy extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_allergies_model');
    }

    /**
     * @api {get} /student_allergy/index_student_allergy index_student_allergy
     * @apiName index_student
     * @apiGroup index_student_allergy
     * @apiDescription home page of student allergy
     * @apiParam {Number} student_record_id id of student record
     */
    function index_student_allergy($student_record_id) {
        $months = get_monthes();
        $this->data['months'] = $months;

        $this->data['show_object_link'] = 'student_allergy/show_student_allergies/' . $student_record_id;
        $this->data['get_object_link'] = 'student_allergy/get_student_allergy';
        $this->data['add_object_link'] = 'student_allergy/add_student_allergy/' . $student_record_id;
        $this->data['update_object_link'] = 'student_allergy/update_student_allergy/' . $student_record_id;
        $this->data['delete_object_link'] = 'student_allergy/delete_student_allergy/' . $student_record_id;
        $this->data['modal_name'] = 'crud_student_allergies';
        $this->data['add_object_title'] = lang("add_new_student_allergy");
        $this->data['update_object_title'] = lang("update_student_allergy");
        $this->data['delete_object_title'] = lang("delete_student_allergy");
        $this->data['thead'] = array('allergy_name',);

        
        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        } else {
            $this->data['thead'][] = 'hidden';
            $this->data['non_printable']['hidden'] = 'hidden';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /student_allergy/show_student_allergies show_student_allergies
     * @apiName show_student_allergies
     * @apiGroup student_allergy
     * @apiDescription get all student allergies
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student allergies table
     * @apiError  FALSE return false if failure in get data
     */
    function show_student_allergies($student_record_id) {
        $student_allergies = $this->students_allergies_model->get_student_allergies($student_record_id);
        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($student_allergies as $student_allergy) {
            $no++;
            $row = array();

            $row[] = $student_allergy->allergy_name;

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($student_allergy->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($student_allergy->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            } else {
                $row[] = ' ';
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_allergies_model->count_all_students_allergies($student_record_id),
            "recordsFiltered" => $this->students_allergies_model->count_filtered_crud_students_allergies($student_record_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_allergy/add_student_allergy add_student_allergy
     * @apiName add_student_allergy
     * @apiGroup student_allergy
     * @apiDescription add student allergy data to student allergies database table
     * @apiParam {String} allergy_name Name of allergy
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_student_allergy($student_record_id) {
        $input_array = array(
            'allergy_name' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $allergy_name = $this->input->post('allergy_name');

            $data = array(
                'allergy_name' => $allergy_name,
                'student_record_id' => $student_record_id,
            );
            $insert = $this->students_allergies_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_allergy/get_student_allergy get_student_allergy
     * @apiName get_student_allergy
     * @apiGroup student_allergy
     * @apiDescription get id of student allergy to get its info from student allergies database table
     * @apiParam {Number} id id of student allergy
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student from students table
     * @apiError  FALSE return false if failure in get data
     */
    function get_student_allergy($id) {
        $data = $this->students_allergies_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student_allergy/update_student_allergy update_student_allergy
     * @apiName update_student_allergy
     * @apiGroup student_allergy
     * @apiDescription update student allergy data in student allergies database table
     * @apiParam {String} allergy_name Name of allergy
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_student_allergy($student_record_id) {
        $student_allergies = $this->students_allergies_model->get_student_allergies($student_record_id);

        $input_array = array(
            'allergy_name' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $allergy_name = $this->input->post('allergy_name');

            validation_edit_delete_redirect($student_allergies, "id", $item_id);
            // end validation

            $data = array(
                'allergy_name' => $allergy_name,
            );
            $update_item = $this->students_allergies_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student_allergy/delete_student_allergy delete_student_allergy
     * @apiName delete_student_allergy
     * @apiGroup student_allergy
     * @apiDescription get id of student allergy to delete its info from student allergies database table
     * @apiParam {Number} id id of student allergy
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_student_allergy($student_record_id, $id) {
        $student_allergies = $this->students_allergies_model->get_student_allergies($student_record_id);
        //start validation
        validation_edit_delete_redirect($student_allergies, "id", $id);
        // end validation
        $delete_item = $this->students_allergies_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
