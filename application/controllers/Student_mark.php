<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_mark extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_marks_model');
        $this->load->model('rooms_model');
        $this->load->model('subjects_model');
    }

    /**
     * @api {get} /student_mark/index_student_mark index_student_mark
     * @apiName student_mark
     * @apiGroup index_student_mark
     * @apiDescription home page of student mark
     * @apiParam {String} semester Name of semester
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} student_record_id id of student record
     */
    function index_student_mark($semester, $room_id, $subject_id) {
        $this->data['show_object_link'] = 'student_mark/show_student_marks/' . $semester . '/' . $room_id . '/' . $subject_id;
        $this->data['get_object_link'] = 'student_mark/get_student_mark';
        $this->data['add_object_link'] = 'student_mark/add_student_mark/' . $semester . '/' . $room_id . '/' . $subject_id;
        $this->data['update_object_link'] = 'student_mark/update_student_mark/' . $semester . '/' . $room_id . '/' . $subject_id;
        $this->data['delete_object_link'] = 'student_mark/delete_student_mark/' . $semester . '/' . $room_id . '/' . $subject_id;
        $this->data['modal_name'] = 'crud_student_marks';
        $this->data['add_object_title'] = lang("add_new_student_mark");
        $this->data['update_object_title'] = lang("update_student_mark");
        $this->data['delete_object_title'] = lang("delete_student_mark");
        $this->data['thead'] = array('student_name', 'mark_key', 'mark_note');

        $this->data['room'] = $this->rooms_model->get_room($room_id);
        $this->data['subject'] = $subject = $this->subjects_model->get_subject($subject_id);

        $this->data['semester'] = $semester;

        $this->data['room_id'] = $room_id;
        $this->data['subject_id'] = $subject_id;
    }

    /**
     * @api {get} /student_mark/show_student_marks show_student_marks
     * @apiName show_student_marks
     * @apiGroup student_mark
     * @apiDescription get all student marks
     * @apiParam {String} semester Name of semester
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} student_record_id id of student record
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student marks table
     * @apiError  FALSE return false if failure in get data
     */
    function show_student_marks($semester, $room_id, $subject_id) {
        $student_marks = $this->students_marks_model->get_students_marks($semester, $room_id, $subject_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        $mark_key_options = get_marks_key();
        foreach ($student_marks as $student_mark) {
            $no++;
            $row = array();
//            $row[] = $student_mark->student_name . ' ' . $student_mark->family_name;

            $row[] = bs3_link_crud("profile/page_student_profile/" . $student_mark->student_id, $student_mark->student_name . ' ' . $student_mark->family_name, '', 'update');

            if (isset($student_mark->mark_key) && $student_mark->mark_key) {
                $row[] = bs3_dropdown_for_table('stu_mark_' . $student_mark->id, $mark_key_options, $student_mark->mark_key);
            } else {
                $row[] = bs3_dropdown_for_table('stu_mark_' . $student_mark->id, $mark_key_options, 'o');
            }
            if (isset($student_mark->mark_note) && $student_mark->mark_note) {
                $row[] = bs3_textarea_for_table('stu_note_' . $student_mark->id, $student_mark->mark_note);
            } else {
                $row[] = bs3_textarea_for_table('stu_note_' . $student_mark->id, '');
            }
            //add html for action

            $data[] = $row;
        }
        $where = array(
            'semester' => $semester,
            'room_id' => $room_id,
            'subject_id' => $subject_id,
        );
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_records_model->count_all_students_records($room_id),
            "recordsFiltered" => $this->students_records_model->count_filtered_crud_students_records($room_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_mark/add_student_mark add_student_mark
     * @apiName add_student_mark
     * @apiGroup student_mark
     * @apiDescription add student mark data to student marks database table
     * @apiParam {String} semester Name of semester
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} student_record_id id of student record
     * @apiParam {Number} stu_mark mark for student
     * @apiParam {String} stu_note note for student mark 
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_student_mark($semester, $room_id, $subject_id) {
        $students = $this->students_marks_model->get_students_marks($semester, $room_id, $subject_id);

        $update = 0;
        $insert = 0;
        $item_update = false;
        $item_insert = false;
        foreach ($students as $student) {
            $student_mark = $this->input->post('stu_mark_' . $student->id);
            $student_note = $this->input->post('stu_note_' . $student->id);

            if (isset($student->mark_key)) {
                $info = array(
                    'student_record_id' => $student->id,
                    'subject_id' => $subject_id,
                    'mark_key' => $student_mark,
                    'mark_note' => $student_note,
                    'semester' => $semester,
                );
                $where = array(
                    'id' => $student->students_marks_id,
                );
                $item_update = $this->students_marks_m->update_by($where, $info);
                if (isset($item_update) && $item_update) {
                    $update++;
                }
            } else {
                $info = array(
                    'student_record_id' => $student->id,
                    'subject_id' => $subject_id,
                    'mark_key' => $student_mark,
                    'mark_note' => $student_note,
                    'semester' => $semester,
                );
                $item_insert = $this->students_marks_m->insert($info);
                if (isset($item_insert) && $item_insert) {
                    $insert++;
                }
            }
        }

        if (isset($item_update) || isset($item_insert)) {
            send_message("", '200', 'update_success');
        } else {
            error_message('400', 'update_error');
        }
    }

}
