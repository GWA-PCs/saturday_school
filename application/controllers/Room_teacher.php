<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Room_teacher extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('rooms_model');
        $this->load->model('rooms_teachers_model');
        $this->load->model('teachers_model');
        $this->load->model('subjects_model');
    }

    /**
     * @api {get} /room_teacher/index_room_teacher index_room_teacher
     * @apiName index_room_teacher
     * @apiGroup room_teacher
     * @apiDescription home page of room teacher for specific room
     * @apiParam {Number} room_id id of room
     */
    function index_room_teacher($room_id = '') {
        $subjects_room = $this->subjects_model->get_subjects_room($room_id);
        $subjects_room_options = array();
        foreach ($subjects_room as $value) {
            if ($value->parent_subject_id == 1)
                $subjects_room_options[$value->id] = $value->subject_name;
            else
                $subjects_room_options[$value->id] = lang($value->parent_subject_name) . ' - ' . $value->subject_name;
        }

        $this->data['subjects_room_options'] = $subjects_room_options;
        $this->data['room_name'] = $this->rooms_model->get_room($room_id);
        $this->data['show_object_link'] = 'room_teacher/show_rooms_teachers/' . $room_id;
        $this->data['permissions_link'] = 'room_teacher/permission_room_teacher' . '/' . $room_id;
        $this->data['get_object_link'] = 'room_teacher/get_room_teacher';
        $this->data['add_object_link'] = 'room_teacher/add_room_teacher/' . $room_id;
        $this->data['update_object_link'] = 'room_teacher/update_room_teacher/' . $room_id;
        $this->data['delete_object_link'] = 'room_teacher/delete_room_teacher/' . $room_id;
        $this->data['modal_name'] = 'crud_room_teacher';
        $this->data['add_object_title'] = lang("add_new_teacher");
        $this->data['update_object_title'] = lang("edit_room_teacher");
        $this->data['delete_object_title'] = lang("delete_room_teacher");
        $this->data['thead'] = array('teacher_name', 'subject_name');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /room_teacher/show_rooms_teachers show_rooms_teachers
     * @apiName show_rooms_teachers
     * @apiGroup room_teacher
     * @apiDescription get all teacher in specific room
     * @apiParam {Number} room_id id of room
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in rooms teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function show_rooms_teachers($room_id = '') {
        $room_teachers = $this->rooms_teachers_model->get_rooms_teachers($room_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($room_teachers as $teacher) {
            $no++;
            $row = array();
//            $row[] = $teacher->first_name . ' ' . $teacher->father_name . ' ' . $teacher->last_name;
            $row[] = bs3_link_crud("profile/page_teacher_profile_for_another_account/" . $teacher->teacher_id, $teacher->first_name . ' ' . $teacher->last_name, '', 'update');
            if ($teacher->parent_subject_id == 1)
                $row[] = ucwords(strtolower($teacher->subject_name));
            else
                $row[] = ucwords(strtolower($teacher->parent_subject_name)) . ' - ' . ucwords(strtolower($teacher->subject_name));
            //add html for action
            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($teacher->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($teacher->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->rooms_teachers_model->count_all_rooms_teachers($room_id),
            "recordsFiltered" => $this->rooms_teachers_model->count_filtered_crud_rooms_teachers($room_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    function permission_room_teacher($room_id = '') {
        if (!isset($room_id) || !$room_id) {
            send_message("", '203', 'permission_error');
        }
        send_message("", '204', 'permission_success');
    }

    /**
     * @api {post} /room_teacher/add_room_teacher add_room_teacher
     * @apiName add_room_teacher
     * @apiGroup room_teacher
     * @apiDescription add grade data to grade database table
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} teacher_key id of teacher
     * @apiParam {Number} subject_id id of subject
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  Room_name_already_exist202 return room name already exist error message

     */
    function add_room_teacher($room_id = '') {
        $input_array = array(
            'teacher_id' => "required",
            'subject_id' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $teacher_id = $this->input->post('teacher_key');
            $subject_id = $this->input->post('subject_id');

            if ($teacher_id != 0) {
                $data = array(
                    'room_id' => $room_id,
                    'teacher_id' => $teacher_id,
                    'subject_id' => $subject_id,
                    'year' => $this->_current_year,
                );
                if (exist_item("rooms_teachers_model", $data)) {
                    send_message("", '202', 'room_name_already_exist');
                }

                $insert = $this->rooms_teachers_model->insert($data);
                if ($insert) {
                    send_message("", '200', 'insert_success');
                } else {
                    error_message('400', 'insert_error');
                }
            } else {
                error_message('400', 'insert_room_teacher_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /room_teacher/get_room_teacher get_room_teacher
     * @apiName get_room_teacher
     * @apiGroup room_teacher
     * @apiDescription get id of room teacher to get its info from rooms teachers database table
     * @apiParam {Number} id id of room teacher
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of room teacher from rooms teachers table
     * @apiError  FALSE return false if failure in get data
     */
    function get_room_teacher($id = '') {
        $data = $this->rooms_teachers_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /room_teacher/update_room_teacher update_room_teacher
     * @apiName update_room_teacher
     * @apiGroup room_teacher
     * @apiDescription update room teacher data in rooms teachers database table
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} subject_id id of subject
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_room_teacher($room_id = '') {
        $rooms_teachers = $this->rooms_teachers_model->get_all_room_teachers($room_id);
        $input_array = array(
            'subject_id' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $subject_id = $this->input->post('subject_id');

            $data = array(
                'room_id' => $room_id,
                'subject_id' => $subject_id,
                'id <>' => $item_id
            );

            validation_edit_delete_redirect($rooms_teachers, "id", $item_id);
            // end validation

            $data = array(
                'room_id' => $room_id,
                'subject_id' => $subject_id,
            );
            $update_item = $this->rooms_teachers_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /room_teacher/delete_room_teacher delete_room_teacher
     * @apiName delete_room_teacher
     * @apiGroup room_teacher
     * @apiDescription get id of room teacher to delete its info from rooms teachers database table
     * @apiParam {Number} room_id id of room
     * @apiParam {Number} id id of room teacher 
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_room_teacher($room_id = '', $id = '') {
        $rooms_teachers = $this->rooms_teachers_model->get_all_room_teachers($room_id);
        //start validation
        validation_edit_delete_redirect($rooms_teachers, "id", $id);
        // end validation
        $delete_item = $this->rooms_teachers_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /room_teacher/search_teacher_auto search_teacher_auto
     * @apiName search_teacher_auto
     * @apiGroup room_teacher
     * @apiDescription get information about name of teacher for auto complete search
     * @apiParam {String} term Name of teacher
     * @apiSuccess  arr  Array contain data of teacher from teachers table
     */
    function search_teacher_auto() {
        $query = $this->input->get('term');
        if (isset($query) && $query) {
            $return_arr = new stdClass();
            $arr = array();
            try {
                $result = $this->teachers_model->get_teachers_for_auto_complete($query);
                foreach ($result as $value) {
                    $return_arr = new stdClass();
                    $return_arr->key = $value->teacher_id;
                    if ($value->assistant_teacher != 2) {
                        $return_arr->value = $value->first_name . ' ' . $value->last_name . ' / ' . lang('teacher');
                    } else {
                        $return_arr->value = $value->first_name . ' ' . $value->last_name . ' / ' . lang('assistant_teacher');
                    }
                    $arr[] = $return_arr;
                }
            } catch (PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
            /* Toss back results as json encoded array. */
            echo json_encode($arr);
            exit();
            die();
        }
    }

}
