<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Room extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('rooms_model');
        $this->load->model('grades_model');
    }

    /**
     * @api {get} /room/index_room index_room
     * @apiName index_room
     * @apiGroup room
     * @apiDescription home page of rooms for specific grade
     * @apiParam {Number} id id of grade
     */
    function index_room($grade_id) {
        $grades = $this->grades_model->get_by('id', $grade_id);
        $this->data['grade_name'] = $grades->grade_name;
        $this->data['show_object_link'] = 'room/show_rooms/' . $grade_id;
        $this->data['get_object_link'] = 'room/get_room';
        $this->data['add_object_link'] = 'room/add_room/' . $grade_id;
        $this->data['update_object_link'] = 'room/update_room/' . $grade_id;
        $this->data['delete_object_link'] = 'room/delete_room/' . $grade_id;
        $this->data['modal_name'] = 'crud_room';
        $this->data['add_object_title'] = lang("add_new_room");
        $this->data['update_object_title'] = lang("edit_room");
        $this->data['delete_object_title'] = lang("delete_room");

        $this->data['thead'] = array('room_name', 'index_room_teachers', 'index_student_record', 'index_transferring_students', 'index_room_attendance');
        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['index_room_teachers'] = 'index_room_teachers';
        $this->data['non_printable']['index_student_record'] = 'index_student_record';
        $this->data['non_printable']['index_transferring_students'] = 'index_transferring_students';
        $this->data['non_printable']['index_room_attendance'] = 'index_room_attendance';

        $this->data['non_printable']['update'] = 'update';
        $this->data['non_printable']['delete'] = 'delete';
    }

    /**
     * @api {get} /room/show_rooms show_rooms
     * @apiName show_rooms
     * @apiGroup room
     * @apiDescription get all rooms for specific grade
     * @apiParam {Number} id id of grade
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in rooms table
     * @apiError  FALSE return false if failure in get data
     */
    function show_rooms($grade_id) {
        $rooms = $this->rooms_model->get_rooms($grade_id);
        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($rooms as $room) {
            $no++;
            $row = array();
            $row[] = $room->room_name;
            //add html for action

            $row[] = bs3_link_crud("room_teacher/index_room_teacher/" . $room->id, '<i class=" mdi  mdi-account-multiple-plus fa-2x "></i>', lang('index_room_teachers'), 'update');
            $row[] = bs3_link_crud("student_record/index_student_record/" . $room->id, '<i class=" mdi  mdi-account-multiple-plus fa-2x "></i>', lang('index_student_record'), 'update');
            $row[] = bs3_link_crud("transferring_students/index_transferring_students/" . $room->id, '<i class="   mdi mdi-account-switch fa-2x "></i>', lang('index_transferring_students'), 'update');
            $row[] = bs3_link_crud("room_attendance/index_room_attendance/" . $room->id, '<i class="mdi mdi-account-multiple-minus fa-2x "></i>', lang('index_room_attendance'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($room->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($room->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->rooms_model->count_all_rooms($grade_id),
            "recordsFiltered" => $this->rooms_model->count_filtered_crud_rooms($grade_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /room/add_room add_room
     * @apiName add_room
     * @apiGroup room
     * @apiDescription add room data to rooms database table
     * @apiParam {Number} grade_id id of grade
     * @apiParam {String} room_name Name of room
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  room_name_already_exist202 return room name already exist error message

     */
    function add_room($grade_id) {
        $input_array = array(
            'room_name' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $room_name = $this->input->post('room_name');

            $data = array(
                'grade_id' => $grade_id,
                'room_name' => $room_name,
            );

            if (exist_item("rooms_model", $data)) {
                send_message("", '202', 'room_name_already_exist');
            }

            $insert = $this->rooms_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /room/get_room get_room
     * @apiName get_room
     * @apiGroup room
     * @apiDescription get id of room to get its info from rooms database table
     * @apiParam {Number} id id of room
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of room from rooms table
     * @apiError  FALSE return false if failure in get data
     */
    function get_room($id) {
        $data = $this->rooms_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /room/update_room update_room
     * @apiName update_room
     * @apiGroup room
     * @apiDescription update room data in rooms database table
     * @apiParam {Number} id id of grade
     * @apiParam {String} room_name Name of room
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  room_name_already_exist202 return room name already exist error message
     */
    function update_room($grade_id) {
        $rooms = $this->rooms_model->get_rooms($grade_id);

        $input_array = array(
            'room_name' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $room_name = $this->input->post('room_name');
            // start validation
            if (exist_item("rooms_model", array('room_name' => $room_name, 'grade_id' => $grade_id, 'id <>' => $item_id))) {
                send_message('', "202", 'room_name_already_exist');
            }
            validation_edit_delete_redirect($rooms, "id", $item_id);
            // end validation

            $data = array(
                'grade_id' => $grade_id,
                'room_name' => $room_name,
            );
            $update_item = $this->rooms_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /room/delete_room delete_room
     * @apiName delete_room
     * @apiGroup room
     * @apiDescription get id of room to delete its info from rooms database table
     * @apiParam {Number} id id of grade
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     * @apiError  Related_with_another_elem407 return JSON encoded string contain related with another element error message
     */
    function delete_room($grade_id, $id) {
        $rooms = $this->rooms_model->get_rooms($grade_id);

        //start validation
        validation_edit_delete_redirect($rooms, "id", $id);
        // end validation

        $room_teachers = $this->rooms_model->get_any_teachers($id);
        $room_students_records = $this->rooms_model->get_any_students_records($id);

//        var_dump($room_teachers);
//        var_dump($room_students_records);
//        die;
        
        if ((isset($room_teachers) && $room_teachers) || (isset($room_students_records) && $room_students_records)) {
            error_message('407', 'related_with_another_elem');
        }

        $delete_item = $this->rooms_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
