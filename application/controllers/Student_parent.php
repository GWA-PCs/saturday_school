<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_parent extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_parents_model');
        $this->load->model('students_model');
        $this->load->model('parents_model');
    }

    /**
     * @api {get} /student_parent/index_student_parent index_student_parent
     * @apiName index_student
     * @apiGroup student_parent
     * @apiDescription home page of student parent
     * @apiParam {Number} student_id id of student
     */
    function index_student_parent($student_id) {
        $parents = $this->parents_model->get_parents();
        $parents_options = array();
        foreach ($parents as $item) {
            $parents_options[$item->id] = $item->first_name . ' ' . $item->father_name . ' ' . $item->last_name;
        }
        $this->data['parents'] = $parents_options;
        $this->data['show_object_link'] = 'student_parent/show_student_parents/' . $student_id;
        $this->data['get_object_link'] = 'student_parent/get_student_parent';
        $this->data['add_object_link'] = 'student_parent/add_student_parent/' . $student_id;
        $this->data['update_object_link'] = 'student_parent/update_student_parent/' . $student_id;
        $this->data['delete_object_link'] = 'student_parent/delete_student_parent/' . $student_id;
        $this->data['modal_name'] = 'crud_student_parents';
        $this->data['add_object_title'] = lang("add_new_student_parent");
        $this->data['update_object_title'] = lang("update_student_parent");
        $this->data['delete_object_title'] = lang("delete_student_parent");
        $this->data['thead'] = array('parent_id', 'update', 'delete');

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /student_parent/show_student_parents show_student_parents
     * @apiName show_student_parents
     * @apiGroup student_parent
     * @apiDescription get all student parents
     * @apiParam {Number} student_id id of student
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student parents table
     * @apiError  FALSE return false if failure in get data
     */
    function show_student_parents($student_id) {
        $student_parents = $this->students_parents_model->get_students_parents($student_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($student_parents as $student_parent) {
            $no++;
            $row = array();
            $row[] = $student_parent->first_name . ' ' . $student_parent->father_name . ' ' . $student_parent->last_name;
            //add html for action
            $row[] = bs3_update_delete_crud($student_parent->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            $row[] = bs3_update_delete_crud($student_parent->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => count($student_parents),
            "recordsFiltered" => count($student_parents),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_parent/add_student_parent add_student_parent
     * @apiName add_student_parent
     * @apiGroup student_parent
     * @apiDescription add student parent data to student parents database table
     * @apiParam {Number} parent_id Name of parent
     * @apiParam {Number} student_id id of student
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  student_parent_already_exist400 return JSON encoded string contain this student parent is already exist error message
     */
    function add_student_parent($student_id) {
        $student_parents = $this->students_parents_model->get_students_parents($student_id);

        $input_array = array(
            'parent_id' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $parent_id = $this->input->post('parent_id');

            if (exist_item("students_parents_model", array('parent_id' => $parent_id, 'student_id' => $student_id))) {
                send_message('', "202", 'student_parent_already_exist');
            }
//            if (isset($parent_exist) && $parent_exist) {
            $data = array(
                'student_id' => $student_id,
                'parent_id' => $parent_id,
            );
            $insert = $this->students_parents_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
//            } else {
//                send_message(lang("parent_not_exist"), '201', 'parent_not_exist');
//            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_parent/get_student_parent get_student_parent
     * @apiName get_student_parent
     * @apiGroup student_parent
     * @apiDescription get id of student parent to get its info from student parents database table
     * @apiParam {Number} id id of student parent
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of parent from student parents table
     * @apiError  FALSE return false if failure in get data
     */
    function get_student_parent($id) {
        $data = $this->students_parents_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student_parent/update_student_parent update_student_parent
     * @apiName update_student_parent
     * @apiGroup student_parent
     * @apiDescription update student parent data in student parents database table
     * @apiParam {Number} parent_id Name of parent
     * @apiParam {Number} student_id id of student
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  student_parent_already_exist400 return JSON encoded string contain this student parent is already exist error message

     */
    function update_student_parent($student_id) {
        $student_parents = $this->students_parents_model->get_students_parents($student_id);

        $input_array = array(
            'parent_id' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $parent_id = $this->input->post('parent_id');

            if (exist_item("students_parents_model", array('parent_id' => $parent_id, 'student_id' => $student_id, 'id <>' => $item_id))) {
                send_message('', "202", 'student_parent_already_exist');
            }

            validation_edit_delete_redirect($student_parents, "id", $item_id);
            // end validation

            $data = array(
                'parent_id' => $parent_id,
            );
            $update_item = $this->students_parents_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student_parent/delete_student_parent delete_student_parent
     * @apiName delete_student_parent
     * @apiGroup student_parent
     * @apiDescription get id of student parent to delete its info from student parents database table
     * @apiParam {Number} id id of student parent
     * @apiParam {Number} student_id id of student
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_student_parent($student_id, $id) {
        $student_parents = $this->students_parents_model->get_students_parents($student_id);
        //start validation
        validation_edit_delete_redirect($student_parents, "id", $id);
        // end validation
        $delete_item = $this->students_parents_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /student_parent/search_parent_auto search_parent_auto
     * @apiName search_parent_auto
     * @apiGroup student_parent
     * @apiDescription get information about name of parent for auto complete search
     * @apiParam {String} term Name of parent
     * @apiSuccess  arr  Array contain data of parent from parents table
     */
    function search_parent_auto() {
        $query = $this->input->get('term');
        if (isset($query) && $query) {
            $return_arr = new stdClass();
            $arr = array();
            try {
                $result = $this->parents_model->get_parents_for_auto_complete($query);
                foreach ($result as $value) {
                    $return_arr = new stdClass();
                    $return_arr->key = $value->parent_id;
                    $return_arr->value = $value->first_name . ' ' . $value->father_name . ' ' . $value->last_name;
                    $arr[] = $return_arr;
                }
            } catch (PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
            /* Toss back results as json encoded array. */
//            json_encode($arr);
            echo json_encode($arr);
            exit();
            die();
        }
    }

}
