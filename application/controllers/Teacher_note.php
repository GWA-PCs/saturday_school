<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_note extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('teachers_notes_model');
    }

    function index_teacher_note($teacher_id) {
        $this->data['show_object_link'] = 'teacher_note/show_teacher_notes/' . $teacher_id;
        $this->data['get_object_link'] = 'teacher_note/get_teacher_note';
        $this->data['add_object_link'] = 'teacher_note/add_teacher_note/' . $teacher_id;
        $this->data['update_object_link'] = 'teacher_note/update_teacher_note/' . $teacher_id;
        $this->data['delete_object_link'] = 'teacher_note/delete_teacher_note/' . $teacher_id;
        $this->data['modal_name'] = 'crud_teacher_notes';
        $this->data['add_object_title'] = lang("add_new_teacher_note");
        $this->data['update_object_title'] = lang("update_teacher_note");
        $this->data['delete_object_title'] = lang("delete_teacher_note");
        $this->data['thead'] = array('note_date', 'note_text',);

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    function show_teacher_notes($teacher_id) {
        $teacher_notes = $this->teachers_notes_model->get_teacher_notes($teacher_id);
        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($teacher_notes as $teacher_note) {
            $no++;
            $row = array();
           
            $row[] = date("m-d-Y", strtotime($teacher_note->note_date));
            $row[] = $teacher_note->note_text;

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($teacher_note->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($teacher_note->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => count($teacher_notes),
            "recordsFiltered" => count($teacher_notes),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    function add_teacher_note($teacher_id) {
        $input_array = array(
            'note_date' => "required",
            'note_text' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $note_date = $this->input->post('note_date');
            if ($note_date)
                $note_date = date_format(date_create_from_format('m-d-Y', $note_date), 'Y-m-d');
            $note_text = $this->input->post('note_text');

            $data = array(
                'note_date' => $note_date,
                'note_text' => $note_text,
                'teacher_id' => $teacher_id,
            );
            $insert = $this->teachers_notes_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    function get_teacher_note($id) {
        $data = $this->teachers_notes_model->get_by('id', $id);
        $data->note_date = date("m-d-Y", strtotime($data->note_date));
        echo json_encode($data);
        die;
    }

    function update_teacher_note($teacher_id) {
        $teacher_notes = $this->teachers_notes_model->get_teacher_notes($teacher_id);

        $input_array = array(
            'note_date' => "required",
            'note_text' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $note_date = $this->input->post('note_date');
            if ($note_date)
                $note_date = date_format(date_create_from_format('m-d-Y', $note_date), 'Y-m-d');
            $note_text = $this->input->post('note_text');

            validation_edit_delete_redirect($teacher_notes, "id", $item_id);
            // end validation

            $data = array(
//                'note_month' => $note_month,
                'note_date' => $note_date,
                'note_text' => $note_text,
            );
            $update_item = $this->teachers_notes_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    function delete_teacher_note($teacher_id, $id) {
        $teacher_notes = $this->teachers_notes_model->get_teacher_notes($teacher_id);
        //start validation
        validation_edit_delete_redirect($teacher_notes, "id", $id);
        // end validation
        $delete_item = $this->teachers_notes_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
