<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('messages_model');
        $this->load->model('teachers_model');
        $this->load->model('parents_model');
        $this->load->model('students_model');
        $this->load->model('grades_model');
        $this->load->model('settings_model');
        $this->load->model('log_messages_model');
        $this->load->model('general_model', 'users_m');
        $this->users_m->set_table('users');
    }

    function index() {
        
    }

    function count_received_msg_for_user_id() {
        $user_id = $this->data['user_login']->id;
        $msg_count = $this->messages_model->count_received_msg_for_user_id($user_id);

        send_message($msg_count);
    }

    /**
     * @api {get} /message/index_send_msg index_send_msg
     * @apiName index_send_msg
     * @apiGroup message
     * @apiDescription home page of send messages
     */
    function index_send_msg() {
        // initialize  msg to options dropdown depending on user login
        if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "admin") {
            $msg_to_options = array(
                '000' => lang('select_msg_to'),
                'teachers' => lang('teachers'),
                'parents' => lang('parents'),
                'class_parents' => lang('class_parents'),
            );
        } elseif (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "parent") {
            $msg_to_options = array(
                '000' => lang('select_msg_to'),
                'admin' => lang('admin'),
                'teachers' => lang('teachers'),
            );
        } elseif (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "teacher") {
            $msg_to_options = array(
                '000' => lang('select_msg_to'),
                'parents' => lang('parents'),
                'class_parents' => lang('class_parents'),
                'students' => lang('students'),
                'class_students' => lang('class_students'),
            );
        }

        $msg_to = array();
        $users_options = array();
        $this->data['msg_to_options'] = $msg_to_options;
        $this->data['msg_to'] = $msg_to;
        $this->data['users_options'] = $users_options;
        $this->data['show_object_link'] = 'message/show_send_msg';
        $this->data['get_object_link_message'] = 'message/get_send_msg';
//        $this->data['detail_object_link'] = 'message/get_message';
        $this->data['add_object_link'] = 'message/add_send_msg';
        $this->data['update_object_link'] = 'message/update_send_msg';
        $this->data['delete_object_link'] = 'message/delete_send_msg';
        $this->data['modal_name'] = 'crud_messages';
        $this->data['add_object_title'] = lang("compose_message");
//        $this->data['detail_object_title'] = lang("details");
        $this->data['update_object_title'] = lang("update_message");
        $this->data['delete_object_title'] = lang("delete_message");
        $this->data['thead'] = array('msg_to[]', 'msg_subject', 'msg_date', 'msg_file', 'details');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'delete';
        }
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['details'] = 'details';
    }

    /**
     * @api {get} /message/show_send_msg show_send_msg
     * @apiName show_send_msg
     * @apiGroup message
     * @apiDescription get all send messages
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in messages table
     * @apiError  FALSE return false if failure in get data
     */
    function show_send_msg() {
        $messages = array();
        $messages = $this->messages_model->get_sender_messages_by_user($this->data['user_login']->id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($messages as $message) {
            $no++;
            $row = array();
            $row[] = $message->first_name . ' ' . $message->last_name . '' . $message->for_parent;
            $row[] = $message->msg_subject;
            $row[] = date("m-d-Y", strtotime($message->msg_date));
            $row[] = bs3_file_download("assets/uploads/msg_file/" . $message->msg_file, $message->msg_file);
//            $row[] = $message->msg_content;
            //add html for action
            $row[] = bs3_link_crud("message/details/send_msg/" . $message->id, '<i class="fa-2x mdi mdi-alert-circle "></i>', lang('details'), 'update');
//            $row[] = bs3_details_crud($message->id, '<i class="fa-2x mdi mdi-alert-circle "></i>', lang('edit'), 'detail');
//            $row[] = bs3_update_delete_crud($message->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($message->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->messages_model->count_all_sender_messages_by_user($this->data['user_login']->id),
            "recordsFiltered" => $this->messages_model->count_filtered_crud_sender_messages_by_user($this->data['user_login']->id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /message/add_send_msg add_send_msg
     * @apiName add_send_msg
     * @apiGroup message
     * @apiDescription add message data to messages database table as a sent message
     * @apiParam {String} msg_to List of type of user you can send a message to him/her
     * @apiParam {String} msg_subject Subject of message
     * @apiParam {String} msg_content Content of message
     * @apiParam {String} msg_file File of message
     * @apiParam {String} msg_to_type Name of user can send message to him/her
     * @apiParam {Boolean} send_by_email Checkbox if want to sent message to email  
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_send_msg() {
        $user = $this->ion_auth->user()->row();
//        $sender_email = $user->email;
        // get sender email from settings table
        $sender_email = $this->settings_model->get_school_contact_email();

        $msg_to = array();
        $insert = '';

        // initialize input array depending on type of logged in user 
        if ($this->data['user_login']->type != "parent") {
            $input_array = array(
                "msg_to[]" => "required",
                'msg_subject' => "required",
                'msg_content' => "required",
            );
        } else {
            $input_array = array(
                "msg_to_type" => "required",
                'msg_subject' => "required",
                'msg_content' => "required",
            );
        }
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $msg_to = $this->input->post("msg_to[]");

            $msg_subject = $this->input->post('msg_subject');
            $msg_content = $this->input->post('msg_content');
            $msg_file = file_or_image('msg_file', 'msg_file');
            $msg_to_type = $this->input->post('msg_to_type');
            $send_by_email = $this->input->post('send_by_email');

            date_default_timezone_set('America/Los_Angeles');
            $now_date = new DateTime('now');
            $current_date = $now_date->getTimestamp();

            $msg_date = date('Y-m-d H:i:s', $current_date);

            $data = array();
            $user_id_of_class_parent = array();
            $array_of_users_id_of_class_parent = array();
            $array_of_email_of_class_parent = array();
            $array_of_email2_of_class_parent = array();

            if ($msg_to_type == 'class_parents') {
                $send = FALSE;
                foreach ($msg_to as $key => $value) {
                    // get parents of selected classes for student record active only 
                    $class_parents_for_specific_class = $this->parents_model->get_class_parents_for_specific_class($value);
                    if (isset($class_parents_for_specific_class) && $class_parents_for_specific_class)
                        $user_id_of_class_parent[$value] = $class_parents_for_specific_class;
                }

                // store first and second email and the name of parent in arrays
                foreach ($msg_to as $key => $value) {
                    if (isset($user_id_of_class_parent[$value]) && $user_id_of_class_parent[$value]) {
                        foreach ($user_id_of_class_parent[$value] as $user_id_of_class_parent_elem) {
                            $array_of_users_id_of_class_parent[$user_id_of_class_parent_elem->user_id] = $user_id_of_class_parent_elem->father_name . ' - ' . $user_id_of_class_parent_elem->mother_name . ' / ' . $user_id_of_class_parent_elem->family_name;
                            $array_of_email_of_class_parent[$user_id_of_class_parent_elem->user_id] = $user_id_of_class_parent_elem->email;
                            $array_of_email2_of_class_parent[$user_id_of_class_parent_elem->user_id] = $user_id_of_class_parent_elem->email2;
                        }
                    }
                }

                // convert values of arrays to contain unique value 
                $array_unique_users_id_of_class_parent = array_unique($array_of_users_id_of_class_parent);
                $array_unique_email_of_class_parent = array_unique($array_of_email_of_class_parent);
                $array_unique_email2_of_class_parent = array_unique($array_of_email2_of_class_parent);

                // send msg to every user in array 
                foreach ($array_unique_users_id_of_class_parent as $key => $value) {
                    if ($send_by_email != null) {
                        $reciever_email = $array_unique_email_of_class_parent[$key];
                        $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $key,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            'for_parent' => $value,
                            'msg_date' => $msg_date,
                        );
                        $send = $this->send_email($sender_email, $reciever_email, $sub_data);
                        if ($send)
                            $insert = 'send_by_email';
                        if (!empty($array_unique_email2_of_class_parent)) {

                            $reciever_email2 = $array_unique_email2_of_class_parent[$key];
                            $sub_data = array(
                                'msg_from' => $user->id,
                                'msg_to' => $key,
                                'msg_subject' => $msg_subject,
                                'msg_content' => $msg_content,
                                'msg_file' => $msg_file,
                                'for_parent' => $value,
                                'msg_date' => $msg_date,
                            );
                            $send = $this->send_email($sender_email, $reciever_email2, $sub_data);
                            if ($send)
                                $insert = 'send_by_email';
                        }
                    } else {
                        $data[] = $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $key,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            'for_parent' => $value,
                            'msg_date' => $msg_date,
                        );
                    }
                }
            } elseif ($msg_to_type == 'parents') {
                $send = FALSE;

                foreach ($msg_to as $key => $value) {
                    // get parent information for every user in msg to array 
                    $parent_info = $this->parents_model->get_parent_by_user_id($value);

                    $for_parent = $parent_info->father_name . ' - ' . $parent_info->mother_name . ' / ' . $parent_info->family_name;

                    if ($send_by_email != null) {
                        $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $value,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            'for_parent' => $for_parent,
                            'msg_date' => $msg_date,
                        );
                        $reciever_email = $parent_info->email;
                        $reciever_email2 = $parent_info->email2;
                        $send = $this->send_email($sender_email, $reciever_email, $sub_data);

                        if (isset($reciever_email2) && $reciever_email2) {
                            $send = $this->send_email($sender_email, $reciever_email2, $sub_data);
                        }
                        if ($send)
                            $insert = 'send_by_email';
                    } else {
                        $data[] = $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $value,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            'for_parent' => $for_parent,
                            'msg_date' => $msg_date,
                        );
                    }
                }
            } elseif ($msg_to_type == 'admin') {
                if ($send_by_email != null) {
                    $where = array('type' => 'admin');
                    $user_info = $this->users_m->get_by($where);
//                    $reciever_email = $user_info->email;
                    // get email of school contact email
                    $reciever_email = $school_contact_email = $this->settings_model->get_school_contact_email();

                    // get email of logged in user 
                    $sender_email = $this->_login_email;

                    $sub_data = array(
                        'msg_from' => $user->id,
                        'msg_to' => $user_info->id,
                        'msg_subject' => $msg_subject,
                        'msg_content' => $msg_content,
                        'msg_file' => $msg_file,
                        'msg_date' => $msg_date,
                    );
                    $send = $this->send_email($sender_email, $reciever_email, $sub_data);

                    if ($send)
                        $insert = 'send_by_email';
                } else {
                    $data[] = $sub_data = array(
                        'msg_from' => $user->id,
                        'msg_to' => 1,
                        'msg_subject' => $msg_subject,
                        'msg_content' => $msg_content,
                        'msg_file' => $msg_file,
                        'msg_date' => $msg_date,
                    );
                }
            } elseif ($msg_to_type == 'class_students') {
                $send = FALSE;
                foreach ($msg_to as $key => $value) {
                    // get parents of selected classes for student record active only 
                    $class_student_for_specific_class = $this->students_model->get_class_students_for_specific_class($value);

                    if (isset($class_student_for_specific_class) && $class_student_for_specific_class)
                        $user_id_of_class_student[$value] = $class_student_for_specific_class;
                }

                $array_of_users_id_of_class_student = array();
                // store first and second email and the name of parent in arrays
                foreach ($msg_to as $key => $value) {
                    if (isset($user_id_of_class_student[$value]) && $user_id_of_class_student[$value]) {
                        foreach ($user_id_of_class_student[$value] as $user_id_of_class_student_elem) {
                            $array_of_users_id_of_class_student[$user_id_of_class_student_elem->user_id] = $user_id_of_class_student_elem->student_name . $user_id_of_class_student_elem->family_name;
                        }
                    }
                }


                // convert values of arrays to contain unique value 
                $array_unique_users_id_of_class_student = array_unique($array_of_users_id_of_class_student);

                // send msg to every user in array 
                foreach ($array_unique_users_id_of_class_student as $key => $value) {
                    $student_info = $this->students_model->get_student_by_user_id_use_in_msg($key);

                    if (isset($student_info) && $student_info) {
                        $for_student = $student_info->student_name . ' ' . $student_info->family_name;

                        $data[] = $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $key,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            // because we have this field yet .... so we don't need to add new for student
                            'for_parent' => $for_student,
                            'msg_date' => $msg_date,
                        );
                    }
                }
            } elseif ($msg_to_type == 'students') {
                $send = FALSE;

                foreach ($msg_to as $key => $value) {
                    // get parent information for every user in msg to array 
                    $student_info = $this->students_model->get_student_by_user_id_use_in_msg($value);
                    $for_student = $student_info->student_name . ' ' . $student_info->family_name;

                    $data[] = $sub_data = array(
                        'msg_from' => $user->id,
                        'msg_to' => $value,
                        'msg_subject' => $msg_subject,
                        'msg_content' => $msg_content,
                        'msg_file' => $msg_file,
                        // because we have this field yet .... so we don't need to add new for student
                        'for_parent' => $for_student,
                        'msg_date' => $msg_date,
                    );
                }
            } else {
                $teacher_email_array = array();
                foreach ($msg_to as $key => $value) {
                    $this->db->select('email');
                    $user_info = $this->users_m->get_by('id', $value);
                    $teacher_email_array[$value] = $user_info->email;
                }
                foreach ($msg_to as $key => $value) {
                    if ($send_by_email != null) {
                        $reciever_email = $teacher_email_array[$value];

                        $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $value,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            'msg_date' => $msg_date,
                        );
                        $send = $this->send_email($sender_email, $reciever_email, $sub_data);

                        if ($send)
                            $insert = 'send_by_email';
                        // break;
                    } else {
                        $data[] = $sub_data = array(
                            'msg_from' => $user->id,
                            'msg_to' => $value,
                            'msg_subject' => $msg_subject,
                            'msg_content' => $msg_content,
                            'msg_file' => $msg_file,
                            'msg_date' => $msg_date,
                        );
                    }
                }
            }


            if (isset($data) && $data)
                $insert = $this->db->insert_batch('messages', $data);

            if ($insert == 'send_by_email') {
                send_message("", '200', 'send_by_email_success');
            }
            if ($insert && $insert != 'send_by_email') {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /message/get_send_msg get_send_msg
     * @apiName get_send_msg
     * @apiGroup message
     * @apiDescription get id of message to get it info from messages database table
     * @apiParam {Number} id id of message
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of message from messages table
     * @apiError  FALSE return false if failure in get data
     */
    function get_send_msg($id) {
        $data = $this->messages_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    function update_send_msg() {
        $messages = $this->messages_model->get_all();

        $input_array = array(
            'msg_subject' => "required",
            'msg_content' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $msg_to = $this->input->post('msg_to');
            $msg_subject = $this->input->post('msg_subject');
            $msg_content = $this->input->post('msg_content');

            $old_file_name = $this->input->post('fileName');
            $msg_file = $old_file_name;
            if (isset($_FILES)) {
                if (!empty($_FILES['msg_file']['name']) && $_FILES['msg_file']['name'] != "") {
                    $msg_file = file_or_image('msg_file', 'msg_file');
                }
            }

            validation_edit_delete_redirect($messages, "id", $item_id);
            // end validation

            $data = array(
                'msg_subject' => $msg_subject,
                'msg_content' => $msg_content,
                'msg_file' => $msg_file,
            );

            $update_item = $this->messages_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message($_FILES, '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /message/delete_send_msg delete_send_msg
     * @apiName delete_send_msg
     * @apiGroup message
     * @apiDescription get id of message to delete its info from messages database table
     * @apiParam {Number} id id of message
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_send_msg($id) {
        $messages = $this->messages_model->get_all();
        //start validation
        validation_edit_delete_redirect($messages, "id", $id);
        // end validation

        $delete_item = $this->messages_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /message/get_msg_to_users get_msg_to_users
     * @apiName get_msg_to_users
     * @apiGroup message
     * @apiDescription get information of user type from database table of user type 
     * @apiParam {String} users_type type of user 
     * @apiSuccess  users_arr array contain information of user type
     */
    function get_msg_to_users($users_type) {
        $users = array();
        $grades = array();
        if ($users_type == 'teachers') {
            if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "parent") {
                $parent_id = $this->parents_model->get_parent_by_user_id($this->data['user_login']->id);
                $users = $this->teachers_model->get_teachers_for_parent_children($parent_id->id);
            } else {
                $users = $this->teachers_model->get_teachers();
            }
        } elseif ($users_type == 'parents') {
            if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "teacher") {
                $users = $this->teachers_model->get_parents_for_teacher_room_by_user_id($this->data['user_login']->id);
            } else {
                $users = $this->parents_model->get_parents_for_active_students();
            }
        } elseif ($users_type == 'class_parents') {
            if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "teacher") {
                $grades = $this->teachers_model->get_teacher_room_by_user_id($this->data['user_login']->id);
            } else {
                $grades = $this->grades_model->get_grades();
            }
            $users = $grades;
        } elseif ($users_type == 'students') {
            if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "teacher") {
                $users = $this->teachers_model->get_students_for_teacher_room_by_user_id($this->data['user_login']->id);
            } else {
                $users = $this->students_model->get_parents_for_active_students();
            }
        } elseif ($users_type == 'class_students') {
            if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type == "teacher") {
                $grades = $this->teachers_model->get_teacher_room_by_user_id($this->data['user_login']->id);
            } else {
                $grades = $this->grades_model->get_grades();
            }
            $users = $grades;
        }

//        var_dump($users);
//        die;
        $users_arr = array();
        foreach ($users as $value) {
            $row = array();

            if ($users_type == 'teachers') {
                $row['user_id'] = $value->user_id;
                $row['first_name'] = $value->first_name;
                $row['last_name'] = $value->last_name;
            } elseif ($users_type == 'parents') {
                $row['user_id'] = $value->user_id;
                $row['first_name'] = $value->father_name . ' - ' . $value->mother_name . ' / ';
                $row['last_name'] = $value->family_name;
            } elseif ($users_type == 'class_parents') {
                // ===============================================
                // ============ Very Importent note ==============
                // ===============================================
                // ===> This way is for unified the form only <===
                // ===============================================
                // user_id ===> grade_id same as id
                $row['user_id'] = $value->id;
                // first_name ===> string lang('parents_of')
                $row['first_name'] = lang('parents_of');
                // last_name ===> grade_name
                $row['last_name'] = $value->grade_name;
            } elseif ($users_type == 'students') {
                if ($value->user_id != NULL) {
                    $row['user_id'] = $value->user_id;
                    $row['first_name'] = $value->student_name;
                    $row['last_name'] = $value->family_name;
                } else {
                    $row['user_id'] = '';
                    $row['first_name'] = '';
                    $row['last_name'] = '';
                }
            } elseif ($users_type == 'class_students') {
                // ===============================================
                // ============ Very Importent note ==============
                // ===============================================
                // ===> This way is for unified the form only <===
                // ===============================================
                // user_id ===> grade_id same as id
                $row['user_id'] = $value->id;
                // first_name ===> string lang('parents_of')
                $row['first_name'] = lang('students_of');
                // last_name ===> grade_name
                $row['last_name'] = $value->grade_name;
            }
            $users_arr[] = array(
                "user_id" => $row['user_id'],
                "first_name" => $row['first_name'],
                "last_name" => $row['last_name'],
            );
        }
        //output to json format
        echo json_encode($users_arr);
        die;
    }

    /**
     * @api {get} /message/details details
     * @apiName details
     * @apiGroup message
     * @apiDescription get details of message  
     * @apiParam {Number} msg_id id of message
     * @apiSuccess  msg_file file of message
     */
    function details($msg_type, $msg_id) {
        $this->data['msg_id'] = $msg_id;
        $messages = $this->messages_model->get_message_by_msg_id($msg_id);
$this->data['user_id'] =$messages->user_id;
        $this->data['msg_file'] = $messages->msg_file;
    }

    /**
     * @api {get} /message/get_sent_msg_details get_sent_msg_details
     * @apiName get_sent_msg_details
     * @apiGroup message
     * @apiDescription get details of sent message  
     * @apiParam {Number} msg_id id of sent message
     * @apiSuccess  messages_object array contain information of receiver user 
     */
    function get_sent_msg_details($msg_id) {
        $messages = $this->messages_model->get_sent_message_by_msg_id($msg_id);
        $messages_object = $messages;
        if ($messages->type == 'teacher') {
            $messages_object->msg_to = $messages->first_name . ' ' . $messages->last_name;
        } elseif ($messages->type == 'parent') {
            $messages_object->msg_to = $messages->for_parent;
        } elseif ($messages->type == 'admin') {
            $messages_object->msg_to = $messages->first_name . ' ' . $messages->last_name;
        } elseif ($messages->type == 'student') {
            $messages_object->msg_to = $messages->for_parent;
        }
        send_message($messages_object);
    }

    /**
     * @api {get} /message/get_recieve_msg_details get_recieve_msg_details
     * @apiName get_recieve_msg_details
     * @apiGroup message
     * @apiDescription get details of recieve message  
     * @apiParam {Number} msg_id id of recieve message
     * @apiSuccess  messages_object array contain information of sender user 
     */
    function get_recieve_msg_details($msg_id) {
        $messages = $this->messages_model->get_recieve_message_by_msg_id($msg_id);
        $messages_object = $messages;
        if ($messages->type == 'teacher') {
            $messages_object->msg_from = $messages->first_name . ' ' . $messages->last_name;
        } elseif ($messages->type == 'parent' && $this->data['user_login']->type == "teacher") {
            $messages = $this->messages_model->get_recieve_message_by_msg_id_from_parent($msg_id);
            $messages_object = $messages;
            $messages_object->msg_from = $messages->father_name . ' - ' . $messages->mother_name . ' / ' . $messages->family_name;
        } elseif ($messages->type == 'admin' && $this->data['user_login']->type == "teacher") {
            $messages_object->msg_from = $messages->first_name . ' ' . $messages->last_name;
        }

        if ($messages->type == 'parent' && $this->data['user_login']->type == "admin") {
            $messages = $this->messages_model->get_recieve_message_by_msg_id_from_parent($msg_id);
            $messages_object = $messages;
            $messages_object->msg_from = $messages->father_name . ' - ' . $messages->mother_name . ' / ' . $messages->family_name;
        }


        send_message($messages_object);
    }

    /**
     * @api {get} /message/index_recieve_msg index_recieve_msg
     * @apiName index_recieve_msg
     * @apiGroup message
     * @apiDescription home page of recieve messages
     */
    function index_recieve_msg() {
        $this->data['show_object_link'] = 'message/show_recieve_msg';
        $this->data['get_object_link_message'] = 'message/get_recieve_msg';
        $this->data['add_object_link'] = 'message/add_recieve_msg';
        $this->data['update_object_link'] = 'message/update_recieve_msg';
        $this->data['delete_object_link'] = 'message/delete_recieve_msg';
        $this->data['modal_name'] = 'crud_messages';
        $this->data['add_object_title'] = lang("compose_message");
        $this->data['update_object_title'] = lang("update_message");
        $this->data['delete_object_title'] = lang("delete_message");
        $this->data['thead'] = array('msg_from', 'msg_subject', 'msg_file', 'details', 'seen');
//            'update', 'delete');

        $this->data['non_printable']['details'] = 'details';
    }

    /**
     * @api {get} /message/show_recieve_msg show_recieve_msg
     * @apiName show_recieve_msg
     * @apiGroup message
     * @apiDescription get all recieve messages
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in messages table
     * @apiError  FALSE return false if failure in get data
     */
    function show_recieve_msg() {
//        $messages = $this->messages_model->get_messages();
        if ($this->data['user_login']->type == "admin" || $this->data['user_login']->type == "teacher") {
            $messages = $this->messages_model->get_recieve_messages_by_user_from_parent($this->data['user_login']->id);
        } elseif ($this->data['user_login']->type == "student") {
            $messages = $this->messages_model->get_recieve_messages_by_user($this->data['user_login']->id);
        } else {
            $messages = $this->messages_model->get_recieve_messages_by_user($this->data['user_login']->id);
        }

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($messages as $message) {
            $no++;
            $row = array();

            if ($this->data['user_login']->type == "admin") {
                $row[] = $message->father_name . ' - ' . $message->mother_name . ' / ' . $message->family_name;
            } elseif ($this->data['user_login']->type == "teacher") {
                if (isset($message->family_name) && $message->father_name != '' && $message->mother_name != '') {
                    $row[] = $message->father_name . ' - ' . $message->mother_name . ' / ' . $message->family_name;
                } else {
                    $row[] = $message->first_name . ' ' . $message->last_name;
                }
            } else {
                $row[] = $message->first_name . ' ' . $message->last_name;
            }
            $row[] = $message->msg_subject;
            $row[] = bs3_file_download("assets/uploads/msg_file/" . $message->msg_file, $message->msg_file);
//            $row[] = $message->msg_content;
            //add html for action
            $row[] = bs3_link_crud("message/details/recieve_msg/" . $message->id, '<i class="fa-2x mdi mdi-alert-circle "></i>', lang('details'), 'update');

            if ($message->seen == 1)
                $row[] = lang('seen');
            else
                $row[] = lang('unseen');
//            $row[] = bs3_details_crud($message->id, '<i class="fa-2x mdi mdi-alert-circle "></i>', lang('edit'), 'detail');
//            $row[] = bs3_update_delete_crud($message->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
//            $row[] = bs3_update_delete_crud($message->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            $data[] = $row;
        }
        if ($this->data['user_login']->type == "admin" || $this->data['user_login']->type == "teacher") {
            $output = array(
                "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
                "recordsTotal" => $this->messages_model->count_all_recieve_messages_by_user($this->data['user_login']->id),
                "recordsFiltered" => $this->messages_model->count_filtered_crud_recieve_messages_by_user($this->data['user_login']->id),
                "data" => $data,
            );
        } else {
            $output = array(
                "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
                "recordsTotal" => $this->messages_model->count_all_recieve_messages_by_user_from_parent($this->data['user_login']->id),
                "recordsFiltered" => $this->messages_model->count_filtered_crud_recieve_messages_by_user_from_parent($this->data['user_login']->id),
                "data" => $data,
            );
        }



        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {get} /message/send_email send_email
     * @apiName send_email
     * @apiGroup message
     * @apiDescription send email from sender email to reciever email
     * @apiParam {String} sender_email Email address of sender 
     * @apiParam {String} reciever_email Email address of reciever 
     * @apiParam {Text} sub_data contant of message
     * @apiSuccess  BooleanValue return true
     * @apiError  BooleanValue return false 
     */
    function send_email($sender_email, $reciever_email, $sub_data) {
        // sending email .. 
        $contact_us_email_from = $sender_email;
        $contact_us_email_to = $reciever_email;

        if (isset($contact_us_email_from) && isset($contact_us_email_to) && $contact_us_email_from && $contact_us_email_to) {
            $this->load->library('email');
            //SMTP & mail configuration
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.minaretsaturday.net',
                'smtp_port' => 465,
                'smtp_user' => 'support@minaretsaturday.net',
                'smtp_pass' => 'Paho%gDhz4EG',
                'mailtype' => 'html',
                'mailpath' => '/usr/sbin/sendmail',
                'charset' => 'utf-8'
            );
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            $this->email->from($contact_us_email_from);
            $this->email->to($contact_us_email_to);
            $this->email->subject($sub_data['msg_subject']);
            if (isset($sub_data['msg_file']) && $sub_data['msg_file']) {
                $site = base_url();
                $this->email->attach($site . '/assets/uploads/msg_file/' . $sub_data['msg_file']);
            }
            $this->email->message($sub_data['msg_content']);

            $send = $this->email->send();
             $this->email->clear(TRUE);
            $status = 1;
            $data = array(
                'sender_email' => $contact_us_email_from,
                'reciever_email' => $contact_us_email_to,
                'status' => $status,
            );

            if ($send) {
                $status = 2;
                $data['status'] = $status;
                $this->log_messages_model->insert($data);
                return TRUE;
            } else {
                $status = 2;
                $data['status'] = $status;
                $this->log_messages_model->insert($data);
                return FALSE;
            }
        }
    }

    /**
     * @api {post} /message/update_status_details update_hall
     * @apiName update_status_details
     * @apiGroup message
     * @apiDescription update status of message from unseen to seen 
     * @apiParam {Number} msg_id id of message

     * @apiSuccess BooleanValue return true if update success 
     * @apiError  BooleanValue return false if update error 
     */
    function update_status_details($msg_id,$user_id) {
        $update_item = false;
        if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->id ==$user_id) {
            $data = array(
                'seen' => 1,
            );
            $update_item = $this->messages_model->update_by(array('id' => $msg_id), $data);
        }
        if ($update_item) {
            send_message($update_item);
        } else {
            send_message($update_item);
        }
    }

}
