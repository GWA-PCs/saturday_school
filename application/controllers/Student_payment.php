<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_payment extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('students_payments_model');
        $this->load->model('students_records_model');
        $this->load->model('student_payment_months_model');
        $this->load->model('payment_items_model');
        $this->load->model('students_model');
        $this->load->model('parents_model');
        $this->load->model('grades_model');
    }

    /**
     * @api {get} /student_payment/index_student_payment index_student_payment
     * @apiName index_student_payment
     * @apiGroup student_payment
     * @apiDescription home page of student payment
     * @apiParam {Number} student_id id of student
     */
    function index_student_payment($student_id) {
        $active_student_record = $this->students_records_model->get_all_active_student_record($student_id);

        $student_info = $this->students_model->get_student_by_id($student_id);
        $this->data['parent_id'] = $parent_id = $student_info->parent_id;

        $parent_info = $this->parents_model->get_by('id', $parent_id);
        $user_id = $parent_info->user_id;
        $this->data['user_id'] = $user_id;

        $this->data['student_name'] = $student_info->student_name . ' ' . $student_info->family_name;

        $monthes = get_monthes();

        $payment_types_options = array('000' => lang('select_payment_item_type'));
        $payment_types = get_payment_types();
        foreach ($payment_types as $key => $value) {
            $payment_types_options[$key] = $value;
        }

        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($active_student_record as $value) {
            $students_grades_options[$value->grade_id] = $value->grade_name;
        }

        $check_or_cash_options = array(
            'cash' => lang('cash'),
            'check' => lang('check')
        );
        $late_fee_type = array(
            '000' => lang('select_late_fee_type'),
            'late_payment_fee' => lang('late_payment_fee'),
            'late_pickup_fee' => lang('late_pickup_fee'),
        );


        $tuition_type = array(
            '000' => lang('select_tuition_type'),
            'full_year' => lang('full_year'),
            'bimonthly' => lang('bimonthly'),
            'monthly' => lang('monthly'),
        );

        $tuition = get_tuition_array();
        $saturday_school = array();
        $arabic_book_club = array();
        $youth_group = array();

        $books_payment_type = array(
            '000' => lang('select_books_type'),
            'pre_kinder_books_fee' => lang('pre_kinder_books_fee'),
            'elementary_books_fee' => lang('elementary_books_fee'),
            'islamic_textbook' => lang('islamic_textbook'),
            'islamic_workbook' => lang('islamic_workbook'),
        );

        // intialize saturday_school, arabic_book_club and youth_group arrays
        foreach ($tuition as $value) {
            if (strpos($value, 'saturday_school') !== false) {
                $saturday_school[$value] = lang($value);
            } elseif (strpos($value, 'arabic_book_club') !== false) {
                $arabic_book_club[$value] = lang($value);
            } elseif (strpos($value, 'youth_group') !== false) {
                $youth_group[$value] = lang($value);
            }
        }

        $this->data['books_payment_type'] = $books_payment_type;
        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;
        $this->data['monthes_options'] = $monthes;
        $this->data['check_or_cash_options'] = $check_or_cash_options;
        $this->data['late_fee_type'] = $late_fee_type;
        $this->data['tuition_type'] = $tuition_type;
        $this->data['active_student_record'] = $active_student_record;
        $this->data['payment_item_type'] = $payment_types_options;
        $this->data['student_id'] = $student_id;

        $this->data['specific_show_object_link'] = 'student_payment/show_student_payments/' . $student_id;
        $this->data['specific_get_object_link'] = 'student_payment/get_student_payment';
        $this->data['specific_add_object_link'] = 'student_payment/add_student_payment/' . $student_id;
        $this->data['specific_update_object_link'] = 'student_payment/update_student_payment/' . $student_id;
        $this->data['specific_delete_object_link'] = 'student_payment/delete_student_payment';
        $this->data['modal_name'] = 'crud_student_payments';
        $this->data['add_object_title'] = lang("add_new_student_payment");
        $this->data['update_object_title'] = lang("update_student_payment");
        $this->data['delete_object_title'] = lang("delete_student_payment");
        $this->data['thead'] = array('grade_id', 'room_id', 'is_active', 'payment_item_type', 'student_payment_date', 'invoice', 'payment', 'check_or_cash',
//            'student_payment_description', 'student_payment_date', 'student_payment_notes',
        );

        if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type != "parent") {
            if ($this->_current_year == $this->_archive_year) {
                $this->data['add_student_payment'] = 1;
                $this->data['thead'][] = 'update';
                $this->data['thead'][] = 'delete';
            }
        } else {
            $this->data['thead'][] = 'details';
            $this->data['non_printable']['details'] = 'details';
        }

        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /student_payment/show_student_payments show_student_payments
     * @apiName show_student_payments
     * @apiGroup student_payment
     * @apiDescription get all student payments
     * @apiParam {Number} student_id id of student
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in student payments table
     * @apiError  FALSE return false if failure in get data
     */
    function show_student_payments($student_id) {
        $student_payments = $this->students_payments_model->get_student_payments($student_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($student_payments as $std_pay) {
            $no++;
            $row = array();

            $row[] = $std_pay->grade_name;
            $row[] = $std_pay->room_name;
            $row[] = tinyint_lang_one_and_two($std_pay->is_active);
            $row[] = lang($std_pay->payment_item_type);
            $row[] = date("m-d-Y", strtotime($std_pay->student_payment_date));

//            if ($std_pay->teacher_child_discount != 0) {
//                $row[] = '$' . $std_pay->invoice - ($std_pay->invoice * $std_pay->teacher_child_discount / 100);
//            } elseif ($std_pay->tuition_type == 'full_year' && $std_pay->full_year_discount != 0) {
//                $row[] = '$' . $std_pay->invoice - ($std_pay->invoice * $std_pay->full_year_discount / 100);
//            } else {
//                $row[] = '$' . $std_pay->invoice;
//            }
//            if ($std_pay->teacher_child_discount != 0) {
//                $row[] = '$' . $std_pay->invoice * $std_pay->teacher_child_discount / 100;
//            } elseif ($std_pay->tuition_type == 'full_year' && $std_pay->full_year_discount != 0) {
//                $row[] = '$' . $std_pay->invoice - ($std_pay->invoice * $std_pay->full_year_discount / 100);
//            } else {
//                $row[] = '$' . $std_pay->invoice;
//            }
            $row[] = '$' . $std_pay->invoice;
            $row[] = '$' . $std_pay->payment;
            $row[] = lang($std_pay->check_or_cash);
            //add html for action
//            $row[] = bs3_update_delete_crud($std_pay->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            if (isset($this->data['user_login']) && $this->data['user_login'] && $this->data['user_login']->type != "parent") {
                $row[] = bs3_link_crud("student_payment/update_student_payment/" . $std_pay->student_id . '/' . $std_pay->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('update_student_payment'), 'update');
                $row[] = bs3_update_delete_crud($std_pay->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            } else {
                $row[] = bs3_link_crud("student_payment/details_of_student_payment/" . $std_pay->student_id . '/' . $std_pay->id, '<i class="fa-2x mdi mdi-alert-circle "></i>', lang('details'), 'update');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->students_payments_model->count_all_students_payments($student_id),
            "recordsFiltered" => $this->students_payments_model->count_filtered_crud_students_payments($student_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /student_payment/add_student_payment add_student_payment
     * @apiName add_student_payment
     * @apiGroup student_payment
     * @apiDescription add student payment data to student payments database table
     * @apiParam {Number} student_id id of student
     * @apiParam {Number} room_id id of room
     * @apiParam {String} payment_item_type type of payment item
     * @apiParam {String} tuition_type type of tuition payment
     * @apiParam {String} late_fee_type type of late fee payment
     * @apiParam {String} books_payment_type type of books payment
     * @apiParam {Number} full_year_discount discount value for full year student payment
     * @apiParam {Number} teacher_child_discount discount value of teacher child 
     * @apiParam {Number} invoice invoice value of student payment
     * @apiParam {Number} payment  payment value of student payment
     * @apiParam {String} check_or_cash type of student payment
     * @apiParam {String} student_payment_description description about student payment
     * @apiParam {Date} student_payment_date date of student payment
     * @apiParam {String} student_payment_notes notes about student payment
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_student_payment($student_id) {
        $count_number_of_checked_month = 0;
        $month_name = array();

        $input_array = array(
            'grade_id' => "required",
            'room_id' => "required",
            'payment_item_type' => "required",
            'invoice' => "required",
            'payment' => "required",
            'check_or_cash' => "required",
            'student_payment_description' => "required",
            'student_payment_date' => "required",
        );
        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $room_id = $this->input->post('room_id');
            $payment_item_type = $this->input->post('payment_item_type');
            $tuition_type = $this->input->post('tuition_type');
            $late_fee_type = $this->input->post('late_fee_type');
            $books_payment_type = $this->input->post('books_payment_type');
            $full_year_discount = $this->input->post('full_year_discount');
            $teacher_child_discount = $this->input->post('teacher_child_discount');
            $invoice = $this->input->post('invoice');
            $payment = $this->input->post('payment');
            $check_or_cash = $this->input->post('check_or_cash');
            $student_payment_description = $this->input->post('student_payment_description');
            $student_payment_date = $this->input->post('student_payment_date');
            if ($student_payment_date)
                $student_payment_date = date_format(date_create_from_format('m-d-Y', $student_payment_date), 'Y-m-d');
            $student_payment_notes = $this->input->post('student_payment_notes');


            $student_record = $this->students_records_model->get_student_record($student_id, $room_id);

            $monthes = get_monthes();

            foreach ($monthes as $key => $value) {
                $month_name[$key] = $this->input->post($key);
                if ($month_name[$key] != null) {
                    $count_number_of_checked_month++;
                }
            }

            // check if student is paid for full year 
            $where = array(
                'student_record_id' => $student_record->id,
                'payment_item_type' => 'tuition',
                'tuition_type' => 'full_year',
            );
            $student_is_paid_for_full_year = $this->students_payments_model->get_by($where);

            if (isset($student_is_paid_for_full_year) && $student_is_paid_for_full_year) {
                if ($payment_item_type == 'tuition') {
                    error_message('400', 'error_in_select_cause_paid_for_full_year');
                }
            }

            if ($tuition_type == 'monthly' && $count_number_of_checked_month != 1) {
                error_message('400', 'error_in_number_of_month');
            } elseif ($tuition_type == 'bimonthly' && $count_number_of_checked_month != 2) {
                error_message('400', 'error_in_number_of_month');
            }

            if ($payment_item_type == 'custom_payment') {
                $data = array(
                    'student_record_id' => $student_record->id,
                    'payment_item_type' => $payment_item_type,
                    'invoice' => round($invoice, 2),
                    'payment' => $payment,
                    'check_or_cash' => $check_or_cash,
                    'student_payment_description' => $student_payment_description,
                    'student_payment_date' => $student_payment_date,
                    'student_payment_notes' => $student_payment_notes,
                    'late_fee_type' => $late_fee_type,
                    'books_payment_type' => $books_payment_type,
                    'full_year_discount' => round($full_year_discount),
                    'teacher_child_discount' => round($teacher_child_discount),
                    'tuition_type' => $tuition_type,
                );
            } else {
                $data = array(
                    'student_record_id' => $student_record->id,
                    'payment_item_type' => $payment_item_type,
                    'invoice' => round($invoice, 2),
                    'payment' => $payment,
                    'check_or_cash' => $check_or_cash,
                    'student_payment_description' => $student_payment_description,
                    'student_payment_date' => $student_payment_date,
                    'student_payment_notes' => $student_payment_notes,
                    'late_fee_type' => $late_fee_type,
                    'books_payment_type' => $books_payment_type,
                    'full_year_discount' => round($full_year_discount),
                    'teacher_child_discount' => round($teacher_child_discount),
                    'tuition_type' => $tuition_type,
                );
            }

            $insert = $this->students_payments_model->insert($data);

            if ($insert) {
                foreach ($monthes as $key => $value) {
                    $month_name[$key] = $this->input->post($key);

                    if ($month_name[$key] != null) {
                        $data_monthes = array(
                            'student_payment_id' => $insert,
                            'month_name' => $key,
                        );
                        $insert_to_student_payment_months = $this->student_payment_months_model->insert($data_monthes);

//                        if (isset($insert_to_student_payment_months) && $insert_to_student_payment_months) {
//                            $student_payment_id = array('student_payment_months_id' => $insert_to_student_payment_months);
//                            $update_item = $this->students_payments_model->update_by(array('id' => $insert), $student_payment_id);
//                        }
                    }
                }
                send_message($student_id, '200', 'insert_success');
            } else {
                error_message('400', lang('insert_error'));
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /student_payment/get_student_payment get_student_payment
     * @apiName get_student_payment
     * @apiGroup student_payment
     * @apiDescription get id of student payment to get its info from student payments database table
     * @apiParam {Number} id id of student student payments
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of payment from student payments table
     * @apiError  FALSE return false if failure in get data
     */
    function get_student_payment($id) {
        $this->db->select('tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,invoice,check_or_cash,student_payment_description,student_payment_date,student_payment_notes');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $data = $this->students_payments_model->get_by('students_payments.id', $id);

        $data->student_payment_date = date("m-d-Y", strtotime($data->student_payment_date));

        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /student_payment/update_student_payment update_student_payment
     * @apiName update_student_payment
     * @apiGroup student_payment
     * @apiDescription update student payment data in student payments database table
     * @apiParam {Number} id id of student student payments
     * @apiParam {Number} student_id id of student
     * @apiParam {Number} room_id id of room
     * @apiParam {String} payment_item_type type of payment item
     * @apiParam {String} tuition_type type of tuition payment
     * @apiParam {String} late_fee_type type of late fee payment
     * @apiParam {String} books_payment_type type of books payment
     * @apiParam {Number} full_year_discount discount value for full year student payment
     * @apiParam {Number} teacher_child_discount discount value of teacher child 
     * @apiParam {Number} invoice invoice value of student payment
     * @apiParam {Number} payment  payment value of student payment
     * @apiParam {String} check_or_cash type of student payment
     * @apiParam {String} student_payment_description description about student payment
     * @apiParam {Date} student_payment_date date of student payment
     * @apiParam {String} student_payment_notes notes about student payment
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  student_parent_already_exist400 return JSON encoded string contain this student payment is already exist error message

     */
    function update_student_payment($student_id, $id) {
        $count_number_of_checked_month = 0;
        $month_name = array();

        $active_student_record = $this->students_records_model->get_all_active_student_record($student_id);

        $monthes = get_monthes();

        $payment_types_options = array('000' => lang('select_payment_item_type'));
        $payment_types = get_payment_types();
        foreach ($payment_types as $key => $value) {
            $payment_types_options[$key] = $value;
        }

        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($active_student_record as $value) {
            $students_grades_options[$value->grade_id] = $value->grade_name;
        }

        $check_or_cash_options = array(
            'cash' => lang('cash'),
            'check' => lang('check')
        );
        $late_fee_type = array(
            '000' => lang('select_late_fee_type'),
            'late_payment_fee' => lang('late_payment_fee'),
            'late_pickup_fee' => lang('late_pickup_fee'),
        );


        $tuition_type = array(
            '000' => lang('select_tuition_type'),
            'full_year' => lang('full_year'),
            'bimonthly' => lang('bimonthly'),
            'monthly' => lang('monthly'),
        );

        $tuition = get_tuition_array();
        $saturday_school = array();
        $arabic_book_club = array();
        $youth_group = array();
        $books_payment_type = array(
            '000' => lang('select_books_type'),
            'pre_kinder_books_fee' => lang('pre_kinder_books_fee'),
            'elementary_books_fee' => lang('elementary_books_fee'),
            'islamic_textbook' => lang('islamic_textbook'),
            'islamic_workbook' => lang('islamic_workbook'),
        );

        foreach ($tuition as $value) {
            if (strpos($value, 'saturday_school') !== false) {
                $saturday_school[$value] = lang($value);
            } elseif (strpos($value, 'arabic_book_club') !== false) {
                $arabic_book_club[$value] = lang($value);
            } elseif (strpos($value, 'youth_group') !== false) {
                $youth_group[$value] = lang($value);
            }
        }
        $student_payment_months_for_specific_student = $this->student_payment_months_model->get_student_payment_months_for_specific_student($id);
        $this->data['student_payment_months_for_specific_student'] = $student_payment_months_for_specific_student;

        $this->data['books_payment_type'] = $books_payment_type;
        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;
        $this->data['monthes_options'] = $monthes;
        $this->data['check_or_cash_options'] = $check_or_cash_options;
        $this->data['late_fee_type'] = $late_fee_type;
        $this->data['tuition_type'] = $tuition_type;
        $this->data['active_student_record'] = $active_student_record;
        $this->data['payment_item_type'] = $payment_types_options;
        $this->data['student_id'] = $student_id;

        $this->db->select('tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,late_fee_type,books_payment_type,full_year_discount,teacher_child,'
                . 'invoice,payment,check_or_cash,student_payment_description,student_payment_date,student_payment_notes,teacher_child_discount');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $data = $this->students_payments_model->get_by('students_payments.id', $id);

        $data->student_payment_date = date("m-d-Y", strtotime($data->student_payment_date));

//        if ($data->teacher_child == '2') {
//            $data->invoice = $data->invoice - ($data->invoice * $data->teacher_child_discount / 100);
//        }
//        if ($data->tuition_type == 'full_year' && $data->teacher_child == '1') {
//            $data->invoice = $data->invoice -  $data->full_year_discount;
//        }
        $data->invoice = $data->invoice;
        $this->data['students_payments'] = $data;
    }

    /**
     * @api {get} /student_payment/ajax_student_payment ajax_student_payment
     * @apiName ajax_student_payment
     * @apiGroup student_payment
     * @apiDescription get id of student payment to get its info from student payments database table
     * @apiParam {Number} id id of student payments
     * @apiParam {Number} student_id id of student
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError error_in_select_cause_paid_for_full_year400  return JSON encoded string contain error message if select other tuition payment type after select full year payment type
     * @apiError error_in_number_of_month400  return JSON encoded string contain error message if select more than month for monthly
     * tuition payment type, or more than two monthes for biomonthly tuition payment type
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function ajax_student_payment($student_id, $id) {
        $count_number_of_checked_month = 0;
        $month_name = array();

        $active_student_record = $this->students_records_model->get_all_active_student_record($student_id);

        $monthes = get_monthes();

        $payment_types_options = array('000' => lang('select_payment_item_type'));
        $payment_types = get_payment_types();
        foreach ($payment_types as $key => $value) {
            $payment_types_options[$key] = $value;
        }

        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($active_student_record as $value) {
            $students_grades_options[$value->grade_id] = $value->grade_name;
        }

        $check_or_cash_options = array(
            'cash' => lang('cash'),
            'check' => lang('check')
        );
        $late_fee_type_options = array(
            '000' => lang('select_late_fee_type'),
            'late_payment_fee' => lang('late_payment_fee'),
            'late_pickup_fee' => lang('late_pickup_fee'),
        );


        $tuition_type_options = array(
            '000' => lang('select_tuition_type'),
            'full_year' => lang('full_year'),
            'bimonthly' => lang('bimonthly'),
            'monthly' => lang('monthly'),
        );

        $tuition = get_tuition_array();
        $saturday_school = array();
        $arabic_book_club = array();
        $youth_group = array();
        $books_payment_type = array(
            '000' => lang('select_books_type'),
            'pre_kinder_books_fee' => lang('pre_kinder_books_fee'),
            'elementary_books_fee' => lang('elementary_books_fee'),
            'islamic_textbook' => lang('islamic_textbook'),
            'islamic_workbook' => lang('islamic_workbook'),
        );

        foreach ($tuition as $value) {
            if (strpos($value, 'saturday_school') !== false) {
                $saturday_school[$value] = lang($value);
            } elseif (strpos($value, 'arabic_book_club') !== false) {
                $arabic_book_club[$value] = lang($value);
            } elseif (strpos($value, 'youth_group') !== false) {
                $youth_group[$value] = lang($value);
            }
        }

        $student_payments = $this->students_payments_model->get_student_payments($student_id);
        $student_payment_months_for_specific_student = $this->student_payment_months_model->get_student_payment_months_for_specific_student($id);
        $this->data['student_payment_months_for_specific_student'] = $student_payment_months_for_specific_student;

        $this->data['books_payment_type'] = $books_payment_type;
        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;
        $this->data['monthes_options'] = $monthes;
        $this->data['check_or_cash_options'] = $check_or_cash_options;
        $this->data['late_fee_type'] = $late_fee_type_options;
        $this->data['tuition_type'] = $tuition_type_options;
        $this->data['active_student_record'] = $active_student_record;
        $this->data['payment_item_type'] = $payment_types_options;
        $this->data['student_id'] = $student_id;

        $this->db->select('tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,late_fee_type,books_payment_type,full_year_discount,'
                . 'invoice,payment,check_or_cash,student_payment_description,student_payment_date,student_payment_notes');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $students_payments_data = $this->students_payments_model->get_by('students_payments.id', $id);
        $students_payments_data->student_payment_date = date("m-d-Y", strtotime($students_payments_data->student_payment_date));
        $this->data['students_payments'] = $students_payments_data;

        $input_array = array(
//            'payment_item_type' => "required",
            'invoice' => "required",
            'payment' => "required",
            'check_or_cash' => "required",
            'student_payment_description' => "required",
            'student_payment_date' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
//            $item_id = $this->input->post('id');
            $item_id = $id;
            $payment_item_type = $this->input->post('payment_item_type');
//            $tuition_type = $this->input->post('tuition_type');
//            $late_fee_type = $this->input->post('late_fee_type');
            $books_payment_type = $this->input->post('books_payment_type');
            $full_year_discount = $this->input->post('full_year_discount');
            $teacher_child_discount = $this->input->post('teacher_child_discount');
            $invoice = $this->input->post('invoice');
            $payment = $this->input->post('payment');
            $check_or_cash = $this->input->post('check_or_cash');
            $student_payment_description = $this->input->post('student_payment_description');
            $student_payment_date = $this->input->post('student_payment_date');
            if ($student_payment_date)
                $student_payment_date = date_format(date_create_from_format('m-d-Y', $student_payment_date), 'Y-m-d');
            $student_payment_notes = $this->input->post('student_payment_notes');
            $room_id = $this->input->post('room_id');

            validation_edit_delete_redirect($student_payments, "id", $item_id);
            // end validation

            $data = array(
//                'payment_item_type' => $payment_item_type,
                'payment' => $payment,
                'invoice' => $invoice,
                'check_or_cash' => $check_or_cash,
                'student_payment_description' => $student_payment_description,
                'student_payment_date' => $student_payment_date,
                'student_payment_notes' => $student_payment_notes,
//                'late_fee_type' => $late_fee_type,
//                'books_payment_type' => $books_payment_type,
                'full_year_discount' => $full_year_discount,
                'teacher_child_discount' => $teacher_child_discount,
//                'tuition_type' => $tuition_type,
            );
            $update_item = $this->students_payments_model->update_by(array('id' => $item_id), $data);

//            $student_record = $this->students_records_model->get_student_record($student_id, $room_id);
//            
            $monthes = get_monthes();

            foreach ($monthes as $key => $value) {
                $month_name[$key] = $this->input->post($key);

                if ($month_name[$key] != null) {
                    $count_number_of_checked_month++;
                }
            }
            // check if student is paid for full year 
            $where = array(
                'id' => $id,
                'payment_item_type' => 'tuition',
                'tuition_type' => 'full_year',
            );
            $student_is_paid_for_full_year = $this->students_payments_model->get_by($where);

            if (isset($student_is_paid_for_full_year) && $student_is_paid_for_full_year) {
                if ($payment_item_type == 'tuition') {
                    error_message('400', 'error_in_select_cause_paid_for_full_year');
                }
            }


            if ($students_payments_data->tuition_type == 'monthly' && $count_number_of_checked_month != 1) {
                error_message('400', 'error_in_number_of_month');
            } elseif ($students_payments_data->tuition_type == 'bimonthly' && $count_number_of_checked_month != 2) {
                error_message('400', 'error_in_number_of_month');
            }

            if ($update_item) {
                // delete and insert 
                $where = array('student_payment_id' => $id,);
                $delete_student_payment_months = $this->student_payment_months_model->delete_by($where);

                foreach ($monthes as $key => $value) {
                    $month_name[$key] = $this->input->post($key);
                    if ($month_name[$key] != null) {
                        $data_monthes = array(
                            'student_payment_id' => $id,
                            'month_name' => $key,
                        );
                        $insert_to_student_payment_months = $this->student_payment_months_model->insert($data_monthes);
                    }
                }
            }

            if ($update_item) {
                send_message($student_id, '200', 'update_success');
            } else {
                error_message('400', lang('update_error'));
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /student_payment/delete_student_payment delete_student_payment
     * @apiName delete_student_payment
     * @apiGroup student_payment
     * @apiDescription get id of student payment to delete its info from student payments database table
     * @apiParam {Number} id id of student payment
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_student_payment($id) {
        $delete_item = $this->students_payments_model->delete_by('id', $id);

        $students_payments_months = $this->student_payment_months_model->get_by('student_payment_id', $id);

        if (isset($students_payments_months) && $students_payments_months) {
            $where = array(
                'student_payment_id' => $id,
            );
            $delete_student_payment_months = $this->student_payment_months_model->delete_by($where);
        }
        if (isset($delete_item) && $delete_item) {
            send_message('', '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /student_payment/get_rooms_for_filter get_rooms_for_filter
     * @apiName get_rooms_for_filter
     * @apiGroup student_payment
     * @apiDescription get id of student to get its rooms who is registered in it
     * @apiParam {Number} student_id id of student 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of student from students records table
     * @apiError  FALSE return false if failure in get data
     */
    function get_rooms_for_filter($student_id) {
        $active_student_record = $this->students_records_model->get_all_active_student_record($student_id);

        $users_arr = array();
        foreach ($active_student_record as $value) {
            $row = array();
            $row['grade_id'] = $value->grade_id;
            $row['room_id'] = $value->room_id;
            $row['room_name'] = $value->room_name;
            $users_arr[] = array("grade_id" => $row['grade_id'], "room_id" => $row['room_id'], "room_name" => $row['room_name'],);
        }
        //output to json format

        echo json_encode($users_arr);
        die;
    }

    /**
     * @api {get} /student_payment/get_book_invoice get_book_invoice
     * @apiName get_book_invoice
     * @apiGroup student_payment
     * @apiDescription get book invoice value 
     * @apiParam {Number} grade_id id of grade 
     * @apiParam {String} type type of payment
     * @apiSuccess  data array contain book invoice value 
     */
    function get_book_invoice($grade_id, $type) {
        $key = 'saturday_school_' . $type;
        $value = $this->payment_items_model->get_tuition_by_key($key);

        $data = array();
        if (isset($value) && $value) {
            $data["invoice"] = $value;
            send_message($data);
        } else {
            $data["invoice"] = 0;
            send_message($data);
        }
    }

    /**
     * @api {get} /student_payment/get_late_payment_fee_invoice get_late_payment_fee_invoice
     * @apiName get_late_payment_fee_invoice
     * @apiGroup student_payment
     * @apiDescription get late payment fee invoice  value 
     * @apiParam {Number} grade_id id of grade 
     * @apiParam {String} type type of payment
     * @apiSuccess  data array contain late payment fee invoice value 
     */
    function get_late_payment_fee_invoice($grade_id, $type) {
        $grade_info = $this->grades_model->get_by('id', $grade_id);
        $grade_type = $grade_info->grade_type;
        if ($grade_type != 'youth_group' && $grade_type != 'book_club') {
            $key = 'saturday_school_' . $type;
        } elseif ($grade_type == 'book_club') {
            $key = 'book_club' . $type;
        } elseif ($grade_type == 'youth_group') {
            $key = 'youth_group' . $type;
        }
        $value = $this->payment_items_model->get_tuition_by_key($key);

        $data = array();
        if (isset($value) && $value) {
            $data["invoice"] = $value;
            send_message($data);
        } else {
            $data["invoice"] = 0;
            send_message($data);
        }
    }

    /**
     * @api {get} /student_payment/get_tuition_invoice get_tuition_invoice
     * @apiName get_tuition_invoice
     * @apiGroup student_payment
     * @apiDescription get tuition invoice  value 
     * @apiParam {Number} grade_id id of grade 
     * @apiParam {Number} room_id id of room 
     * @apiParam {String} type type of payment
     * @apiParam {Number} student_id id of student 
     * @apiSuccess  data array contain tuition invoice value 
     */
    function get_tuition_invoice($grade_id, $room_id, $type, $student_id) {
        $data = array();
        $student_order = 1;
        $full_year_discount = 0;
        $grade_info = $this->grades_model->get_by('id', $grade_id);
        $grade_type = $grade_info->grade_type;
        //=================== Begin check if student is a teacher child ===================
        $where = array(
            'room_id' => $room_id,
            'student_id' => $student_id,
            'is_active' => 2,
        );
        $this->db->select('teacher_child,student_order');
        $student = $this->students_records_model->get_by($where);

        if (isset($student) && $student) {
            $student_order = $student->student_order;
            $data["teacher_child_check"] = $student->teacher_child;

            if ($student->teacher_child == 2) {
                // get value of tuition fee for any student 
                if ($grade_type != 'youth_group' && $grade_type != 'book_club') {
                    $teacher_child_key = 'saturday_school_teacher_child_discount';
                } elseif ($grade_type == 'book_club') {
                    $teacher_child_key = 'arabic_book_teacher_child_discount';
                } elseif ($grade_type == 'youth_group') {
                    $teacher_child_key = 'youth_group_teacher_child_discount';
                }
                $teacher_child_value = $this->payment_items_model->get_tuition_by_key($teacher_child_key);

                if (isset($teacher_child_value) && $teacher_child_value) {
                    $data["teacher_child_discount"] = $teacher_child_value;
                } else {
                    $data["teacher_child_discount"] = 0;
                }
            }
        }
        //============================================================================
        // get value of tuition fee for any student 
        if ($grade_type != 'youth_group' && $grade_type != 'book_club') {
            //=========================== get discount of full year payment ===================
            if ($type == 'full_year') {
                $full_year_discount = $this->payment_items_model->get_tuition_by_key('saturday_school_tuition_full_year_discount');
                if ($full_year_discount && $full_year_discount) {
                    $data["full_year_discount"] = $full_year_discount;
                } else {
                    $data["full_year_discount"] = 0;
                }
            }
            if ($student_order == 1) {
                $key = 'saturday_school_tuition_' . $type . '_first_child';
            } elseif ($student_order == 2) {
                $key = 'saturday_school_tuition_' . $type . '_second_child';
            } else {
                $key = 'saturday_school_tuition_' . $type . '_third_child';
            }
        } elseif ($grade_type == 'book_club') {
            $key = 'arabic_book_club_per_month';
        } elseif ($grade_type == 'youth_group') {
            if ($type == 'full_year') {
                $full_year_discount = $this->payment_items_model->get_tuition_by_key('youth_group_tuition_full_year_discount');
                if ($full_year_discount && $full_year_discount) {
                    $data["full_year_discount"] = $full_year_discount;
                } else {
                    $data["full_year_discount"] = 0;
                }
            }
            if ($student_order == 1) {
                $key = 'youth_group_tuition_' . $type . '_first_child';
            } elseif ($student_order == 2) {
                $key = 'youth_group_tuition_' . $type . '_second_child';
            } else {
                $key = 'youth_group_tuition_' . $type . '_third_child';
            }
        }
        $value = $this->payment_items_model->get_tuition_by_key($key);

        if (isset($value) && $value) {
            $data["invoice"] = $value;
            send_message($data);
        } else {
            $data["invoice"] = 0;
            send_message($data);
        }
    }

    /**
     * @api {get} /student_payment/get_registration_fee_invoice get_registration_fee_invoice
     * @apiName get_registration_fee_invoice
     * @apiGroup student_payment
     * @apiDescription get registration fee invoice value 
     * @apiParam {Number} grade_id id of grade 
     * @apiParam {String} type type of payment
     * @apiSuccess  data array contain registration fee invoice value 
     */
    function get_registration_fee_invoice($grade_id, $room_id, $type, $student_id) {
        $data = array();
        $grade_info = $this->grades_model->get_by('id', $grade_id);
        $grade_type = $grade_info->grade_type;

        // get value of registration fee for any student 
        if ($grade_type != 'youth_group' && $grade_type != 'book_club') {
            $key = 'saturday_school_' . $type;
        } elseif ($grade_type == 'book_club') {
            $key = 'arabic_book_club_per_month';
        } elseif ($grade_type == 'youth_group') {
            $key = 'youth_group_' . $type;
        }
        $value = $this->payment_items_model->get_tuition_by_key($key);

        if (isset($value) && $value) {
            $data["invoice"] = $value;
            send_message($data);
        } else {
            $data["invoice"] = 0;
            send_message($data);
        }
    }

    /**
     * @api {get} /student_payment/details_of_student_payment details_of_student_payment
     * @apiName details_of_student_payment
     * @apiGroup student_payment
     * @apiDescription get details of student payment
     * @apiParam {Number} student_id id of student 
     * @apiParam {Number} id id of student payment 
     * @apiSuccess  data array contain details of student payment
     */
    function details_of_student_payment($student_id, $id) {
        $count_number_of_checked_month = 0;
        $month_name = array();

        $active_student_record = $this->students_records_model->get_all_active_student_record($student_id);

        $monthes = get_monthes();

        $payment_types_options = array('000' => lang('select_payment_item_type'));
        $payment_types = get_payment_types();
        foreach ($payment_types as $key => $value) {
            $payment_types_options[$key] = $value;
        }

        $students_grades_options = array('000' => lang('select_grade_name'));
        $students_rooms_options = array();

        foreach ($active_student_record as $value) {
            $students_grades_options[$value->grade_id] = $value->grade_name;
        }

        $check_or_cash_options = array(
            'cash' => lang('cash'),
            'check' => lang('check')
        );
        $late_fee_type = array(
            '000' => lang('select_late_fee_type'),
            'late_payment_fee' => lang('late_payment_fee'),
            'late_pickup_fee' => lang('late_pickup_fee'),
        );


        $tuition_type = array(
            '000' => lang('select_tuition_type'),
            'full_year' => lang('full_year'),
            'bimonthly' => lang('bimonthly'),
            'monthly' => lang('monthly'),
        );

        $tuition = get_tuition_array();
        $saturday_school = array();
        $arabic_book_club = array();
        $youth_group = array();
        $books_payment_type = array(
            '000' => lang('select_books_type'),
            'pre_kinder_books_fee' => lang('pre_kinder_books_fee'),
            'elementary_books_fee' => lang('elementary_books_fee'),
            'islamic_textbook' => lang('islamic_textbook'),
            'islamic_workbook' => lang('islamic_workbook'),
        );

        foreach ($tuition as $value) {
            if (strpos($value, 'saturday_school') !== false) {
                $saturday_school[$value] = lang($value);
            } elseif (strpos($value, 'arabic_book_club') !== false) {
                $arabic_book_club[$value] = lang($value);
            } elseif (strpos($value, 'youth_group') !== false) {
                $youth_group[$value] = lang($value);
            }
        }

        //for check the monthes 
//        $student_payments = $this->students_payments_model->get_student_payments($student_id);
//        $student_payment_ids = array();
//        foreach ($student_payments as $value) {
//            if ($value->tuition_type == 'monthly' || $value->tuition_type == 'bimonthly')
//                $student_payment_ids[$value->id] = $value->id;
//        }
//        $student_payment_months_for_specific_student = $this->student_payment_months_model->get_student_payment_months_for_specific_student($student_payment_ids);
//        $this->data['student_payment_months_for_specific_student'] = $student_payment_months_for_specific_student;

        $student_payment_months_for_specific_student = $this->student_payment_months_model->get_student_payment_months_for_specific_student($id);
        $this->data['student_payment_months_for_specific_student'] = $student_payment_months_for_specific_student;

        $this->data['books_payment_type'] = $books_payment_type;
        $this->data['students_grades_options'] = $students_grades_options;
        $this->data['students_rooms_options'] = $students_rooms_options;
        $this->data['monthes_options'] = $monthes;
        $this->data['check_or_cash_options'] = $check_or_cash_options;
        $this->data['late_fee_type'] = $late_fee_type;
        $this->data['tuition_type'] = $tuition_type;
        $this->data['active_student_record'] = $active_student_record;
        $this->data['payment_item_type'] = $payment_types_options;
        $this->data['student_id'] = $student_id;

//        $student_payments = $this->students_payments_model->get_student_payments($student_id);

        $this->db->select('tuition_type,students_payments.id as id ,is_active,students_records.id as student_record_id,students_records.student_id,grade_name,grade_id,room_id,room_name'
                . ',payment_item_id,payment_item_type,late_fee_type,books_payment_type,full_year_discount,'
                . 'invoice,payment,check_or_cash,student_payment_description,student_payment_date,student_payment_notes');
        $this->db->join('students_records', 'students_records.id=students_payments.student_record_id');
        $this->db->join('rooms', 'rooms.id=students_records.room_id');
        $this->db->join('grades', 'grades.id=rooms.grade_id');
        $this->db->join('student_payment_months', 'student_payment_months.student_payment_id=students_payments.payment_item_id', 'left');
        $data = $this->students_payments_model->get_by('students_payments.id', $id);
        $data->student_payment_date = date("m-d-Y", strtotime($data->student_payment_date));
        $this->data['students_payments'] = $data;
    }

    /**
     * @api {get} /student_payment/total_student_payment_and_balance total_student_payment_and_balance
     * @apiName total_student_payment_and_balance
     * @apiGroup student_payment
     * @apiDescription get total student payment and balance value 
     * @apiParam {Number} student_id id of student 
     * @apiSuccess  data array contain total student payment and balance value 
     */
    function total_student_payment_and_balance($student_id) {
        $invoice = 0;
        $payment = 0;
        $balance_student = 0;
        $custom_payment = 0;
        $student_payments = $this->students_payments_model->get_student_payments($student_id);

        if (isset($student_payments) && $student_payments) {
            foreach ($student_payments as $value) {
//                if ($value->payment_item_type == 'custom_payment') {
////                    $custom_payment += $value->payment;
//                    $payment += $value->payment;
//                } else {
                $invoice += $value->invoice;
                $payment += $value->payment;
//                }
            }

            $balance_student = $payment - $invoice;
        }
        $cost_data = array(
            'sum_invoice' => '$' . round($invoice, 2),
            'sum_payment' => '$' . round($payment, 2),
            'balance_student' => '$' . round($balance_student, 2),
        );
        send_message($cost_data);
    }

}
