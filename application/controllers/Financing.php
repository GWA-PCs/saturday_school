<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Financing extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('pizza_and_snack_inputs_model');
        $this->load->model('pizza_and_snack_items_model');
        $this->load->model('petty_cashes_model');
        $this->load->model('payment_items_model');
    }

    /**
     *
     * @api {get}  /financing/index_financing index_financing
     * @apiName index_financing
     * @apiGroup financing
     * @apiDescription index_financing show all link should be viewed in financing home page
     *
     */
    function index_financing() {
        
    }

    /**
     * @api {get} /financing/index_pizza_and_snack_inputs index_pizza_and_snack_inputs
     * @apiName index_pizza_and_snack_inputs
     * @apiGroup financing
     * @apiDescription home page of pizza and snack inputs 
     */
    function index_pizza_and_snack_inputs() {
        $this->data['specific_show_object_link'] = 'financing/show_pizza_and_snack_inputs';
        $this->data['specific_get_object_link'] = 'financing/get_pizza_and_snack_input';
        $this->data['specific_add_object_link'] = 'financing/add_pizza_and_snack_input';
        $this->data['specific_update_object_link'] = 'financing/update_pizza_and_snack_input';
        $this->data['specific_delete_object_link'] = 'financing/delete_pizza_and_snack_input';
        $this->data['modal_name'] = 'crud_pizza_and_snack_input';
        $this->data['add_object_title'] = lang("add_new_input");
        $this->data['update_object_title'] = lang("edit_input");
        $this->data['delete_object_title'] = lang("delete_input");
        $this->data['thead'] = array('input_date', 'input_title', 'cost', 'index_pizza_and_snack_items',);
//'deposit', 'withdrawal'
//
        //appear delete amd update only when current year is equel to archive year
        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        // put the title whose don't apear in print and excel
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['index_pizza_and_snack_items'] = 'index_pizza_and_snack_items';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /financing/show_pizza_and_snack_inputs show_pizza_and_snack_inputs
     * @apiName show_pizza_and_snack_inputs
     * @apiGroup financing
     * @apiDescription get all pizza and snack inputs in current year 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in pizza and snack inputs table
     * @apiError  FALSE return false if failure in get data
     */
    function show_pizza_and_snack_inputs() {
        $pizza_and_snack_inputs = $this->pizza_and_snack_inputs_model->get_all_output_pizza_and_snack_inputs();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($pizza_and_snack_inputs as $pizza_and_snack_input) {
            $no++;
            $row = array();
            $row[] = date("m-d-Y", strtotime($pizza_and_snack_input->input_date));
            $row[] = $pizza_and_snack_input->input_title;
            $row[] = '$' . $pizza_and_snack_input->cost;
//            $row[] = '$' . $pizza_and_snack_input->deposit;
//            $row[] = '$' . $pizza_and_snack_input->withdrawal;
            //add html for action
            $row[] = bs3_link_crud("financing/index_pizza_and_snack_items/" . $pizza_and_snack_input->id, '<i class="mdi mdi-library-plus fa-2x "></i>', lang('index_pizza_and_snack_items'), 'update');

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($pizza_and_snack_input->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($pizza_and_snack_input->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->pizza_and_snack_inputs_model->count_all_output_pizza_and_snack_inputs(),
            "recordsFiltered" => $this->pizza_and_snack_inputs_model->count_filtered_crud_output_pizza_and_snack_inputs(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /financing/add_pizza_and_snack_input add_pizza_and_snack_input
     * @apiName add_pizza_and_snack_input
     * @apiGroup financing
     * @apiDescription add pizza and snack input data to pizza and snack input database table
     * @apiParam {Date} input_date Date of input data
     * @apiParam {Text} input_title Title of input data
     * @apiParam {Number} cost Cost of input  
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_pizza_and_snack_input() {
        $input_array = array(
            'input_date' => "required",
            'input_title' => "required",
            'cost' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $input_date = $this->input->post('input_date');
            if ($input_date)
                $input_date = date_format(date_create_from_format('m-d-Y', $input_date), 'Y-m-d');
            $input_title = $this->input->post('input_title');
            $cost = $this->input->post('cost');
//            $deposit = $this->input->post('deposit');
//            $withdrawal = $this->input->post('withdrawal');

            $data = array(
                'input_date' => $input_date,
                'input_title' => $input_title,
                'cost' => $cost,
//                'deposit' => $deposit,
//                'withdrawal' => $withdrawal,
                'year' => $this->_current_year,
            );

            $insert = $this->pizza_and_snack_inputs_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /financing/get_pizza_and_snack_input get_pizza_and_snack_input
     * @apiName get_pizza_and_snack_input
     * @apiGroup financing
     * @apiDescription get id of pizza and snack input data item to get its info from pizza and snack inputs database table
     * @apiParam {Number} id id of pizza and snack input
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of pizza and snack input from  pizza and snack input table
     * @apiError  FALSE return false if failure in get data
     */
    function get_pizza_and_snack_input($id) {
        $data = $this->pizza_and_snack_inputs_model->get_by('id', $id);
        $data->input_date = date("m-d-Y", strtotime($data->input_date));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /financing/update_pizza_and_snack_input update_pizza_and_snack_input
     * @apiName update_pizza_and_snack_input
     * @apiGroup financing
     * @apiDescription update pizza and snack input data in pizza and snack input database table
     * @apiParam {Number} id id of pizza and snack input
     * @apiParam {Date} input_date Date of input data
     * @apiParam {Text} input_title Title of input data
     * @apiParam {Number} cost Cost of input  
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_pizza_and_snack_input() {
        $pizza_and_snack_inputs = $this->pizza_and_snack_inputs_model->get_all();
        $input_array = array(
            'input_date' => "required",
            'input_title' => "required",
            'cost' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $input_date = $this->input->post('input_date');
            if ($input_date)
                $input_date = date_format(date_create_from_format('m-d-Y', $input_date), 'Y-m-d');
            $input_title = $this->input->post('input_title');
            $cost = $this->input->post('cost');
//            $deposit = $this->input->post('deposit');
//            $withdrawal = $this->input->post('withdrawal');

            validation_edit_delete_redirect($pizza_and_snack_inputs, "id", $item_id);

            // end validation 
            $data = array(
                'input_date' => $input_date,
                'input_title' => $input_title,
                'cost' => $cost,
//                'deposit' => $deposit,
//                'withdrawal' => $withdrawal,
            );

            $update_item = $this->pizza_and_snack_inputs_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /financing/delete_pizza_and_snack_input delete_pizza_and_snack_input
     * @apiName delete_pizza_and_snack_input
     * @apiGroup financing
     * @apiDescription get id of pizza and snack input data item to delete its info from pizza and snack inputs database table
     * @apiParam {Number} id id of pizza and snack input
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_pizza_and_snack_input($id) {
        $pizza_and_snack_inputs = $this->pizza_and_snack_inputs_model->get_all();
        //start validation
        validation_edit_delete_redirect($pizza_and_snack_inputs, "id", $id);
        // end validation
        // delete related inputs 

        $pizza_and_snack_items = $this->pizza_and_snack_items_model->get_many_by('pizza_and_snack_input_id', $id);

        $related_items = array();
        foreach ($pizza_and_snack_items as $value) {
            if ($value->pizza_and_snack_input_id == $id)
                $related_items[$value->id] = $value->id;
        }

        $delete_many_item = $this->pizza_and_snack_items_model->delete_many($related_items);

        $delete_item = $this->pizza_and_snack_inputs_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {get} /financing/index_pizza_and_snack_items index_pizza_and_snack_items
     * @apiName index_pizza_and_snack_items
     * @apiGroup financing
     * @apiDescription home page of pizza and snack items 
     * @apiParam {Number} pizza_and_snack_input_id id of pizza and snack input
     */
    function index_pizza_and_snack_items($pizza_and_snack_input_id) {
        $this->data['pizza_and_snack_input_id'] = $pizza_and_snack_input_id;
        $this->data['specific_show_object_link'] = 'financing/show_pizza_and_snack_items/' . $pizza_and_snack_input_id;
        $this->data['specific_get_object_link'] = 'financing/get_pizza_and_snack_item';
        $this->data['specific_add_object_link'] = 'financing/add_pizza_and_snack_item/' . $pizza_and_snack_input_id;
        $this->data['specific_update_object_link'] = 'financing/update_pizza_and_snack_item/' . $pizza_and_snack_input_id;
        $this->data['specific_delete_object_link'] = 'financing/delete_pizza_and_snack_item/' . $pizza_and_snack_input_id;
        $this->data['modal_name'] = 'crud_pizza_and_snack_item';
        $this->data['add_object_title'] = lang("add_new_item");
        $this->data['update_object_title'] = lang("edit_item");
        $this->data['delete_object_title'] = lang("delete_item");
        $this->data['thead'] = array('bills', 'quantity', 'total');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        // put the title whose don't apear in print and excel
        $this->data['non_printable']['update'] = 'update';
        $this->data['non_printable']['delete'] = 'delete';
    }

    /**
     * @api {get} /financing/show_pizza_and_snack_items show_pizza_and_snack_items
     * @apiName show_pizza_and_snack_items
     * @apiGroup financing
     * @apiDescription get all pizza and snack items in current year for specific pizza and snack input id
     * @apiParam {Number} pizza_and_snack_input_id id of pizza and snack input
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in pizza and snack items table
     * @apiError  FALSE return false if failure in get data
     */
    function show_pizza_and_snack_items($pizza_and_snack_input_id) {
        $pizza_and_snack_items = $this->pizza_and_snack_items_model->get_all_output_pizza_and_snack_items($pizza_and_snack_input_id);

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($pizza_and_snack_items as $pizza_and_snack_item) {
            $total = $pizza_and_snack_item->bills * $pizza_and_snack_item->quantity;
            $no++;
            $row = array();
            $row[] = '$' . $pizza_and_snack_item->bills;
            $row[] = $pizza_and_snack_item->quantity;
            $row[] = '$' . $total;

            if ($this->_current_year == $this->_archive_year) {
                $row[] = bs3_update_delete_crud($pizza_and_snack_item->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($pizza_and_snack_item->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->pizza_and_snack_items_model->count_all_output_pizza_and_snack_items($pizza_and_snack_input_id),
            "recordsFiltered" => $this->pizza_and_snack_items_model->count_filtered_crud_output_pizza_and_snack_items($pizza_and_snack_input_id),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /financing/add_pizza_and_snack_item add_pizza_and_snack_item
     * @apiName add_pizza_and_snack_item
     * @apiGroup financing
     * @apiDescription add pizza and snack item data to pizza and snack items database table
     * @apiParam {Number} bills bills of item  
     * @apiParam {Number} quantity quantity of item  
     * @apiParam {Number} pizza_and_snack_input_id id of pizza and snack input
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_pizza_and_snack_item($pizza_and_snack_input_id) {
        $input_array = array(
            'bills' => "required",
            'quantity' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $bills = $this->input->post('bills');
            $quantity = $this->input->post('quantity');

            $data = array(
                'bills' => $bills,
                'quantity' => $quantity,
                'total' => $bills * $quantity,
                'pizza_and_snack_input_id' => $pizza_and_snack_input_id,
            );

            $insert = $this->pizza_and_snack_items_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /financing/get_pizza_and_snack_item get_pizza_and_snack_item
     * @apiName get_pizza_and_snack_item
     * @apiGroup financing
     * @apiDescription get id of pizza and snack item to get its info from pizza and snack items database table
     * @apiParam {Number} id id of pizza and snack item
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of pizza and snack item from  pizza and snack items table
     * @apiError  FALSE return false if failure in get data
     */
    function get_pizza_and_snack_item($id) {
        $data = $this->pizza_and_snack_items_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /financing/update_pizza_and_snack_item update_pizza_and_snack_item
     * @apiName update_pizza_and_snack_item
     * @apiGroup financing
     * @apiDescription update pizza and snack item data in pizza and snack items database table
     * @apiParam {Number} bills bills of item  
     * @apiParam {Number} quantity quantity of item  
     * @apiParam {Number} pizza_and_snack_input_id id of pizza and snack input
     * @apiParam {Number} cost Cost of item  
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_pizza_and_snack_item($pizza_and_snack_input_id) {
        $pizza_and_snack_items = $this->pizza_and_snack_items_model->get_all_output_pizza_and_snack_items($pizza_and_snack_input_id);

        $input_array = array(
            'bills' => "required",
            'quantity' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $bills = $this->input->post('bills');
            $quantity = $this->input->post('quantity');

            validation_edit_delete_redirect($pizza_and_snack_items, "id", $item_id);

            // end validation 
            $data = array(
                'bills' => $bills,
                'quantity' => $quantity,
                'total' => $bills * $quantity,
            );

            $update_item = $this->pizza_and_snack_items_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /financing/delete_pizza_and_snack_item delete_pizza_and_snack_item
     * @apiName delete_pizza_and_snack_item
     * @apiGroup financing
     * @apiDescription get id of pizza and snack item  to delete its info from pizza and snack items database table
     * @apiParam {Number} pizza_and_snack_input_id id of pizza and snack input
     * @apiParam {Number} id id of pizza and snack item
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_pizza_and_snack_item($pizza_and_snack_input_id, $id) {
        $pizza_and_snack_items = $this->pizza_and_snack_items_model->get_all_output_pizza_and_snack_items($pizza_and_snack_input_id);

        //start validation
        validation_edit_delete_redirect($pizza_and_snack_items, "id", $id);
        // end validation
        $delete_item = $this->pizza_and_snack_items_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {post} /financing/how_much_made how_much_made
     * @apiName how_much_made
     * @apiGroup financing
     * @apiDescription calculate total,cost and  profit of pizza and snack items
     * @apiParam {Number} pizza_and_snack_input_id id of pizza and snack input
     * @apiSuccess  cost_data array contain total,cost and  profit of pizza and snack input
     */
    function how_much_made($pizza_and_snack_input_id) {
        $total_sum = $this->pizza_and_snack_items_model->get_sum_pizza_and_snack_items($pizza_and_snack_input_id);
        $pizza_and_snack_item = $this->pizza_and_snack_items_model->get_output_pizza_and_snack_items_by_id($pizza_and_snack_input_id);

        $profit = $total_sum->total_sum - $pizza_and_snack_item->cost;

        $cost_data = array(
            'total_sum' => '$' . round($total_sum->total_sum),
            'cost' => '$' . round($pizza_and_snack_item->cost, 2),
            'profit' => '$' . round($profit, 2),
//            'deposit' => '$' . round($pizza_and_snack_item->deposit, 2),
        );
        send_message($cost_data);
    }

    /**
     * @api {get} /financing/index_petty_cash index_petty_cash
     * @apiName index_petty_cash
     * @apiGroup financing
     * @apiDescription home page of petty cash
     */
    function index_petty_cash() {
        $this->data['petty_cash_type'] = $petty_cash_type = get_petty_cash_type_array();
        $this->data['specific_show_object_link'] = 'financing/show_petty_cash';
        $this->data['specific_get_object_link'] = 'financing/get_petty_cash';
        $this->data['specific_add_object_link'] = 'financing/add_petty_cash';
        $this->data['specific_update_object_link'] = 'financing/update_petty_cash';
        $this->data['specific_delete_object_link'] = 'financing/delete_petty_cash';
        $this->data['modal_name'] = 'crud_petty_cash';
        $this->data['add_object_title'] = lang("add_new_petty_cash");
        $this->data['update_object_title'] = lang("update_new_petty_cash");
        $this->data['delete_object_title'] = lang("delete_petty_cash");
        $this->data['thead'] = array('petty_cash_date', 'petty_cash_type', 'petty_cash_account', 'petty_cash_memo', 'petty_cash_payment', 'petty_cash_deposit');

        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        }

        $this->data['non_printable']['update'] = 'update';
        $this->data['non_printable']['delete'] = 'delete';
    }

    /**
     * @api {get} /financing/show_petty_cash show_petty_cash
     * @apiName show_petty_cash
     * @apiGroup financing
     * @apiDescription home page of petty cash 
     * @apiDescription get all petty cash in current year 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in petty cash table
     * @apiError  FALSE return false if failure in get data
     */
    function show_petty_cash() {
        $petty_cash = $this->petty_cashes_model->get_all_petty_cash();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($petty_cash as $petty_csh) {
            $no++;
            $row = array();
            $row[] = date("m-d-Y", strtotime($petty_csh->petty_cash_date));
            $row[] = lang($petty_csh->petty_cash_type);
            $row[] = $petty_csh->petty_cash_account;
            $row[] = $petty_csh->petty_cash_memo;
            $row[] = '$' . $petty_csh->petty_cash_payment;
            $row[] = '$' . $petty_csh->petty_cash_deposit;

            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($petty_csh->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($petty_csh->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->petty_cashes_model->count_all_petty_cash(),
            "recordsFiltered" => $this->petty_cashes_model->count_filtered_crud_petty_cash(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /financing/add_petty_cash add_petty_cash
     * @apiName add_petty_cash
     * @apiGroup financing
     * @apiDescription add petty cash data to petty cash database table
     * @apiParam {Date} petty_cash_date Date of petty cash
     * @apiParam {String} petty_cash_type Type of petty cash
     * @apiParam {String} petty_cash_account Account of petty cash
     * @apiParam {Text} petty_cash_memo Notes about petty cash
     * @apiParam {Number} petty_cash_payment Payment of petty cash
     * @apiParam {Number} petty_cash_deposit Deposit of petty cash
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_petty_cash() {
        $input_array = array(
            'petty_cash_date' => "required",
            'petty_cash_type' => "required",
            'petty_cash_account' => "required",
            'petty_cash_payment' => "numeric|greater_than_equal_to[0]",
            'petty_cash_deposit' => "numeric|greater_than_equal_to[0]",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $petty_cash_date = $this->input->post('petty_cash_date');
            if ($petty_cash_date)
                $petty_cash_date = date_format(date_create_from_format('m-d-Y', $petty_cash_date), 'Y-m-d');
            $petty_cash_type = $this->input->post('petty_cash_type');
            $petty_cash_account = $this->input->post('petty_cash_account');
            $petty_cash_memo = $this->input->post('petty_cash_memo');
            $petty_cash_payment = $this->input->post('petty_cash_payment');
            $petty_cash_deposit = $this->input->post('petty_cash_deposit');
//            $petty_cash_balance = $this->input->post('petty_cash_balance');

            $data = array(
                'petty_cash_date' => $petty_cash_date,
                'petty_cash_type' => $petty_cash_type,
                'petty_cash_account' => $petty_cash_account,
                'petty_cash_memo' => $petty_cash_memo,
                'petty_cash_payment' => $petty_cash_payment,
                'petty_cash_deposit' => $petty_cash_deposit,
                'year' => $this->_current_year,
//                'petty_cash_balance' => $petty_cash_balance,
            );

            $insert = $this->petty_cashes_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /financing/get_petty_cash get_petty_cash
     * @apiName get_petty_cash
     * @apiGroup financing
     * @apiDescription get id of petty cash data item to get its info from petty cash database table
     * @apiParam {Number} id id of petty cash 
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of pizza and snack input from  pizza and snack input table
     * @apiError  FALSE return false if failure in get data
     */
    function get_petty_cash($id) {
        $data = $this->petty_cashes_model->get_by('id', $id);
        $data->petty_cash_date = date("m-d-Y", strtotime($data->petty_cash_date));
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /financing/update_petty_cash update_petty_cash
     * @apiName update_petty_cash
     * @apiGroup financing
     * @apiDescription update petty cash data in petty cash database table
     * @apiParam {Date} petty_cash_date Date of petty cash
     * @apiParam {String} petty_cash_type Type of petty cash
     * @apiParam {String} petty_cash_account Account of petty cash
     * @apiParam {Text} petty_cash_memo Notes about petty cash
     * @apiParam {Number} petty_cash_payment Payment of petty cash
     * @apiParam {Number} petty_cash_deposit Deposit of petty cash
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function update_petty_cash() {
        $petty_cash = $this->petty_cashes_model->get_all_petty_cash();

        $input_array = array(
            'petty_cash_date' => "required",
            'petty_cash_type' => "required",
            'petty_cash_account' => "required",
            'petty_cash_payment' => "numeric|greater_than_equal_to[0]",
            'petty_cash_deposit' => "numeric|greater_than_equal_to[0]",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $petty_cash_date = $this->input->post('petty_cash_date');
            if ($petty_cash_date)
                $petty_cash_date = date_format(date_create_from_format('m-d-Y', $petty_cash_date), 'Y-m-d');
            $petty_cash_type = $this->input->post('petty_cash_type');
            $petty_cash_account = $this->input->post('petty_cash_account');
            $petty_cash_memo = $this->input->post('petty_cash_memo');
            $petty_cash_payment = $this->input->post('petty_cash_payment');
            $petty_cash_deposit = $this->input->post('petty_cash_deposit');
//            $petty_cash_balance = $this->input->post('petty_cash_balance');

            validation_edit_delete_redirect($petty_cash, "id", $item_id);

            // end validation 
            $data = array(
                'petty_cash_date' => $petty_cash_date,
                'petty_cash_type' => $petty_cash_type,
                'petty_cash_account' => $petty_cash_account,
                'petty_cash_memo' => $petty_cash_memo,
                'petty_cash_payment' => $petty_cash_payment,
                'petty_cash_deposit' => $petty_cash_deposit,
//                'petty_cash_balance' => $petty_cash_balance,
            );

            $update_item = $this->petty_cashes_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /financing/delete_petty_cash delete_petty_cash
     * @apiName delete_petty_cash
     * @apiGroup financing
     * @apiDescription get id of petty cash to delete its info from petty cash database table
     * @apiParam {Number} id id of petty cash
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_petty_cash($id) {
        $petty_cash = $this->petty_cashes_model->get_all();
        //start validation
        validation_edit_delete_redirect($petty_cash, "id", $id);
        // end validation
        $delete_item = $this->petty_cashes_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

    /**
     * @api {post} /financing/total_petty_cash total_petty_cash
     * @apiName total_petty_cash
     * @apiGroup financing
     * @apiDescription calculate payment,deposit and total balance of petty cash
     * @apiSuccess  cost_data array contain payment,deposit and total balance of petty cash 
     */
    function total_petty_cash() {
        $payment = 0;
        $deposit = 0;
        $result = 0;
        $total_sum = $this->petty_cashes_model->get_sum_petty_cash();

        if (isset($total_sum) && $total_sum) {
            $payment = $total_sum->payment;
            $deposit = $total_sum->deposit;
            $result = $total_sum->deposit - $total_sum->payment;
            if ($total_sum->payment == NULL || $total_sum->deposit == NULL) {
                $payment = 0;
                $deposit = 0;
                $result = 0;
            }
        }
        $cost_data = array(
            'payment' => '$' . round($payment, 2),
            'deposit' => '$' . round($deposit, 2),
            'balance' => '$' . round($result, 2),
        );
        send_message($cost_data);
    }

    /**
     * @api {get} /financing/index_tuition index_tuition
     * @apiName index_tuition
     * @apiGroup financing
     * @apiDescription home page of tuition 
     */
    function index_tuition() {
        $payment_items_tuition = $this->payment_items_model->get_tuition();
        $this->data['payment_items_tuition'] = $payment_items_tuition;
    }

    /**
     * @api {post} /financing/add_tuition add_tuition
     * @apiName add_tuition
     * @apiGroup financing
     * @apiDescription add tuition data to tuition database table
     * @apiParam {Number} Value of every tuition type
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     */
    function add_tuition() {
        $tuition = get_tuition_array();
        foreach ($tuition as $value) {
            $input_array = array($value => "numeric|greater_than_equal_to[0]",);
        }
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $update_item = FALSE;

            $data = array();
            foreach ($tuition as $key => $value) {
                $post_tuition = $this->input->post($key);
                $where = array('payment_key' => $key);
                $data = array('payment_value' => $post_tuition);
                $update_item = $this->payment_items_model->update_by($where, $data);
            }

            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /financing/how_much_made_overall_pizza_and_snack how_much_made_overall_pizza_and_snack
     * @apiName how_much_made_overall_pizza_and_snack
     * @apiGroup financing
     * @apiDescription calculate total,cost and profit for all pizza and snack items
     * @apiSuccess  cost_data array contain total,cost and profit for all pizza and snack input
     */
    function how_much_made_overall_pizza_and_snack() {
//        $sum_pizza_and_snack = $this->pizza_and_snack_items_model->get_sum_pizza_and_snack_all_items();
        $pizza_and_snack_item = $this->pizza_and_snack_items_model->get_output_pizza_and_snack_all_items();

        $pizza_and_snack_inputs = $this->pizza_and_snack_inputs_model->get_all_output_pizza_and_snack_inputs();



        $cost = 0;
//        $deposit = 0;
        $total_sum = 0;
        foreach ($pizza_and_snack_item as $value) {
            $total_sum += $value->total;
//            $deposit += $value->deposit;
        }

        foreach ($pizza_and_snack_inputs as $value) {
            $cost += $value->cost;
        }

//        foreach ($sum_pizza_and_snack as $value) {
//            $total_sum += $value->total_sum;
//             $cost += $value->cost_sum;
//        }
        $profit = $total_sum - $cost;

        $cost_data = array(
            'total_sum' => '$' . round($total_sum),
            'cost' => '$' . round($cost, 2),
            'profit' => '$' . round($profit, 2),
//            'deposit' => '$' . round($deposit, 2),
        );
        send_message($cost_data);
    }

}
