<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Previlage extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('previlages_model');
    }

    function index_previlage()
    {
        $this->data['types'] = $this->get_previlage_types_array();
        $this->data['show_object_link'] = 'previlage/show_previlages';
        $this->data['get_object_link'] = 'previlage/get_previlage';
        $this->data['add_object_link'] = 'previlage/add_previlage';
        $this->data['update_object_link'] = 'previlage/update_previlage';
        $this->data['delete_object_link'] = 'previlage/delete_previlage';
        $this->data['modal_name'] = 'crud_previlage';
        $this->data['add_object_title'] = lang("add_previlage");
        $this->data['update_object_title'] = lang("update_previlage");
        $this->data['delete_object_title'] = lang("delete_previlage");
        $this->data['thead'] = array('previlage_name_en', 'previlage_name_ar' , 'function_name', 'previlage_type', 'update', 'delete');
    }

    function get_previlage_types_array() {
        $previlage_types = array(
            'admins' => lang('admins'),
            'teachers' => lang('teachers'),
            'students' => lang('students')
        );
        return $previlage_types;
    }

    function show_previlages() {
        $previlages = $this->previlages_model->get_all_crud();

        $data = array();
        $no = $_POST['start'];
        foreach ($previlages as $item) {
            $no++;
            $row = array();
            $row[] = $item->previlage_name_en;
            $row[] = $item->previlage_name_ar;
            $row[] = $item->function_name;
            $row[] = lang($item->previlage_type);
            //add html for action
            $row[] = bs3_update_delete_crud($item->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
            $row[] = bs3_update_delete_crud($item->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->previlages_model->count_all(),
            "recordsFiltered" => $this->previlages_model->count_filtered_crud(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    function get_previlage($id) {
        $data = $this->previlages_model->get_previlage_by_id($id);
        echo json_encode($data);
        die;
    }

    function add_previlage() {
        $input_array = array(
            'function_name' => "required",
            'previlage_type' => '',
            'previlage_name_ar' => 'required',
            'previlage_name_en' => 'required',
        );

        $previlage_types = $this->get_previlage_types_array();

        $this->_validation($input_array);

        $function_name = $this->input->post('function_name');
        $previlage_type = $this->input->post('previlage_type');
        $previlage_name_ar = $this->input->post('previlage_name_ar');
        $previlage_name_en = $this->input->post('previlage_name_en');

        $data = array(
            'function_name' => $function_name,
            'previlage_type' => $previlage_type,
            'previlage_name_ar' => $previlage_name_ar,
            'previlage_name_en' => $previlage_name_en,
        );

        if ($this->form_validation->run()) {

            //validation
            if(exist_item("previlages_model", array('previlage_name_ar' => $previlage_name_ar))) {
                send_message('previlage_name_ar' ,"202", 'already_exist');
            }
            if(exist_item("previlages_model", array('previlage_name_en' => $previlage_name_en))) {
                send_message('previlage_name_en',"202", 'already_exist');
            }

            if(!isset($previlage_types[$previlage_type])) {
                error_message("401", 'error_in_information');
            }
            //validation

            $insert = $this->previlages_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }

        }else {
            send_message(validation_errors(), '201' , 'validation_error');
        }
    }

    function update_previlage() {
        $input_array = array(
            'function_name' => "required",
            'previlage_type' => '',
            'previlage_name_ar' => 'required',
            'previlage_name_en' => 'required',
        );

        $previlage_types = $this->get_previlage_types_array();

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $function_name = $this->input->post('function_name');
            $previlage_type = $this->input->post('previlage_type');
            $previlage_name_ar = $this->input->post('previlage_name_ar');
            $previlage_name_en = $this->input->post('previlage_name_en');

            //validation
            if(exist_item("previlages_model", array('previlage_name_ar' => $previlage_name_ar, 'id <>' => $item_id))) {
                send_message('previlage_name_ar' ,"202", 'already_exist');
            }
            if(exist_item("previlages_model", array('previlage_name_en' => $previlage_name_en, 'id <>' => $item_id))) {
                send_message('previlage_name_en',"202", 'already_exist');
            }

            if(!isset($previlage_types[$previlage_type])) {
                error_message("401", 'error_in_information');
            }
            //validation

            $data = array(
                'function_name' => $function_name,
                'previlage_type' => $previlage_type,
                'previlage_name_ar' => $previlage_name_ar,
                'previlage_name_en' => $previlage_name_en,
            );

            $insert = $this->previlages_model->update_by(array('id' => $item_id),$data);
            if ($insert) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        }else {
            send_message(validation_errors(), '201' , 'validation_error');
        }
    }

    function delete_previlage($id) {
        $previlages = $this->previlages_model->get_all();
        //validateion
        if(!validation_edit_delete_redirect($previlages, "id", $id)) {
            error_message("401", 'error_in_information');
        };
        //validateion
        $delete_item = $this->previlages_model->delete_by('id',$id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }
}
