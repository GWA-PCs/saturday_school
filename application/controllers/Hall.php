<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hall extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->authentication_login();
        $this->load->model('halls_model');
    }

    function index() {
        
    }

    /**
     * @api {get} /hall/index_hall index_hall
     * @apiName index_hall
     * @apiGroup hall
     * @apiDescription home page of halls
     */
    function index_hall() {
        $this->data['hall_status'] = get_hall_status();
        $this->data['show_object_link'] = 'hall/show_halls';
        $this->data['get_object_link'] = 'hall/get_hall';
        $this->data['add_object_link'] = 'hall/add_hall';
        $this->data['update_object_link'] = 'hall/update_hall';
        $this->data['delete_object_link'] = 'hall/delete_hall';
        $this->data['modal_name'] = 'crud_halls';
        $this->data['add_object_title'] = lang("add_new_hall");
        $this->data['update_object_title'] = lang("update_hall");
        $this->data['delete_object_title'] = lang("delete_hall");
//        $this->data['thead'] = array('hall_name', 'hall_status', 'update', 'delete');
        $this->data['thead'] = array('hall_name',);
        if ($this->_current_year == $this->_archive_year) {
            $this->data['thead'][] = 'update';
            $this->data['thead'][] = 'delete';
        } else {
            $this->data['thead'][] = 'hidden';
            $this->data['non_printable']['hidden'] = 'hidden';
        }

        // put the title whose don't apear in print and excel
        $this->data['non_printable']['delete'] = 'delete';
        $this->data['non_printable']['update'] = 'update';
    }

    /**
     * @api {get} /hall/show_halls show_halls
     * @apiName show_halls
     * @apiGroup hall
     * @apiDescription get all halls
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data should be show in halls table
     * @apiError  FALSE return false if failure in get data
     */
    function show_halls() {
        $halls = $this->halls_model->get_halls();

        $data = array();
        if (isset($_POST['start']) && $_POST['start']) {
            $no = $_POST['start'];
        } else {
            $no = 0;
        }
        foreach ($halls as $hall) {
            $no++;
            $row = array();
            $row[] = $hall->hall_name;
//            $row[] = lang($hall->hall_status);
//            
            if ($this->_current_year == $this->_archive_year) {
                //add html for action
                $row[] = bs3_update_delete_crud($hall->id, '<i class=" fa fa-2x fa-pencil "></i>', lang('edit'), 'update');
                $row[] = bs3_update_delete_crud($hall->id, '<i class=" fa fa-2x fa-trash "></i>', lang('delete'), 'delete');
            } else {
                $row[] = ' ';
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => (isset($_POST['draw']) && $_POST['draw']) ? $_POST['draw'] : 0,
            "recordsTotal" => $this->halls_model->count_all(),
            "recordsFiltered" => $this->halls_model->count_filtered_crud(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
        die;
    }

    /**
     * @api {post} /hall/add_hall add_hall
     * @apiName add_hall
     * @apiGroup hall
     * @apiDescription add hall data to halls database table
     * @apiParam {String} hall_name Name of hall
     * @apiSuccess  InsertSuccess200 return JSON encoded string contain success message
     * @apiError  InsertError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  Hall_name_already_exist202 return hall name already exist error message
     */
    function add_hall() {
        $input_array = array(
            'hall_name' => "required",
//            'hall_status' => "required",
        );
        $this->_validation($input_array);
        if ($this->form_validation->run()) {
            $hall_name = $this->input->post('hall_name');
//            $hall_status = $this->input->post('hall_status');

            if (exist_item("halls_model", array('hall_name' => $hall_name))) {
                send_message("", '202', 'hall_name_already_exist');
            }

            $data = array(
                'hall_name' => $hall_name,
//                'hall_status' => $hall_status,
            );
            $insert = $this->halls_model->insert($data);
            if ($insert) {
                send_message("", '200', 'insert_success');
            } else {
                error_message('400', 'insert_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {get} /Hall/get_hall get_hall
     * @apiName get_hall
     * @apiGroup hall
     * @apiDescription get id of hall to get its info from halls database table
     * @apiParam {Number} id id of hall
     * @apiSuccess  JSON_encoded_string return JSON encoded string contain data of hall from halls table
     * @apiError  FALSE return false if failure in get data
     */
    function get_hall($id) {
        $data = $this->halls_model->get_by('id', $id);
        echo json_encode($data);
        die;
    }

    /**
     * @api {post} /hall/update_hall update_hall
     * @apiName update_hall
     * @apiGroup hall
     * @apiDescription update hall data in halls database table
     * @apiParam {Number} id id of hall
     * @apiParam {String} hall_name Name of hall
     * @apiSuccess UpdateSuccess200 return JSON encoded string contain success message
     * @apiError  UpdateError400 return JSON encoded string contain error message
     * @apiError  Validation_error201 return validation error message if error is happen when user input data
     * @apiError  Hall_name_already_exist202 return hall name already exist error message
     */
    function update_hall() {
        $halls = $this->halls_model->get_all();

        $input_array = array(
            'hall_name' => "required",
//            'hall_status' => "required",
        );

        $this->_validation($input_array);

        if ($this->form_validation->run()) {
            $item_id = $this->input->post('id');
            $hall_name = $this->input->post('hall_name');
//            $hall_status = $this->input->post('hall_status');
            // start validation
            if (exist_item("halls_model", array('hall_name' => $hall_name, 'id <>' => $item_id))) {
                send_message('', "202", 'hall_name_already_exist');
            }

            validation_edit_delete_redirect($halls, "id", $item_id);
            // end validation

            $data = array(
                'hall_name' => $hall_name,
//                'hall_status' => $hall_status,
            );

            $update_item = $this->halls_model->update_by(array('id' => $item_id), $data);
            if ($update_item) {
                send_message("", '200', 'update_success');
            } else {
                error_message('400', 'update_error');
            }
        } else {
            send_message(validation_errors(), '201', 'validation_error');
        }
    }

    /**
     * @api {post} /hall/delete_hall delete_hall
     * @apiName delete_hall
     * @apiGroup hall
     * @apiDescription get id of hall to delete its info from halls database table
     * @apiParam {Number} id id of hall
     * @apiSuccess  DeleteSuccess200 return JSON encoded string contain delete success message
     * @apiError  DeleteError400 return JSON encoded string contain delete error message
     */
    function delete_hall($id) {
        $halls = $this->halls_model->get_all();
        //start validation
        validation_edit_delete_redirect($halls, "id", $id);
        // end validation
        $delete_item = $this->halls_model->delete_by('id', $id);
        if (isset($delete_item) && $delete_item) {
            send_message("", '200', 'delete_success');
        } else {
            error_message('400', 'delete_error');
        }
    }

}
