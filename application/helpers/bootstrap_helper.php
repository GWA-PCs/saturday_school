<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('bs3_modal_small_header')) {

    function bs3_modal_small_header($name = '', $title = 'update', $extra = 'fa fa-pencil', $no_icon = false, $ajax_url = true, $action = 'update', $temp_url = '', $form_id = "form_crud") {
        ?>
        <?php if ($action != 'delete') { ?>
            <?php if (!$no_icon) { ?>
                <a  href="#" title="" data-toggle="tooltip" title="<?php echo lang($title) ?>" aria-describedby="tooltip80070"  ><i  data-toggle="modal" data-target="#<?php echo $name; ?>" title="<?php echo lang($title) ?>" class=" <?php echo $extra; ?> "></i></a>
            <?php } else { ?>
                <button type="button" class="btn btn-success btn-rounded hvr-icon-spin hvr-shadow" data-toggle="modal" data-target="#<?php echo $name; ?>"><i class="<?php echo $extra ?>"></i> <?php echo '  ' . lang($no_icon) ?></button>
            <?php } ?>
        <?php } ?>

        <div class="modal fade bs-example-modal-lg"  id="<?php echo $name; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                        <h4 class="modal-title" id="myLargeModalLabel" style="color: white !important;"><?php echo lang($title) ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <?php
                        $action_form = "action-form='" . $action . "'";
                        $form_id = 'id="' . $form_id . '"';
                        if ($ajax_url) {
                            $class = "class='form-horizontal form-ajax'" . "url-data='" . $ajax_url . "'";
                        } else {
                            $class = "class='form-horizontal'";
                        }

                        echo form_open($temp_url, $form_id . " role='form'   enctype= 'multipart/form-data'" . $class . ' ' . $action_form);
                        ?>

                        <?php
                    }

                }


                if (!function_exists('bs3_card_wid')) {

                    function bs3_card_wid_colored($link = false, $icon, $title = false, $subtitle = false, $color, $data_lable = false, $text_align = 'text-right', $style = '') {
                        ?>

                        <div class="col-md-6 col-lg-4 col-xlg-4">
                            <a href="<?php echo $link ?>">
                                <div class="card hvr-float-shadow " >
                                    <div class="text-center " style="min-height: 100px; background-color: <?php echo $color ?> ">
                                        <i class="hvr-grow fa-5x <?php echo $icon ?> " style="color:#fafafa;  <?php echo $style ?>"></i>
                                    </div>
                                    <div class="text-center" style="min-height:50px; background-color:white">
                                        <!--<h1 class="font-light text-white">2.9s</h1>-->
                                        <?php if ($title) { ?>
                                            <h5 class="font-light " style="line-height: 40px!important"><?php echo $title ?></h5>
                                        <?php } ?>

                                        <?php if ($subtitle) { ?>
                                            <h6 class="text-muted"  style="line-height: 40px!important"><?php echo $subtitle ?></h6>
                                        <?php } ?>
                                    </div>
                                </div>
                            </a>

                        </div>
                        <?php
                    }

                }

                if (!function_exists('bs3_card_wid_home_page')) {

                    function bs3_card_wid_home_page($link = false, $icon, $title = false, $subtitle = false, $color, $data_lable = false, $text_align = 'text-right') {
                        ?>

                        <div class="col-md-4 col-lg-3 col-xlg-3">
                            <a href="<?php echo $link ?>">
                                <div class="card hvr-float-shadow " >
                                    <div class="text-center " style="min-height: 100px; background-color: <?php echo $color ?> ">
                                        <i class="hvr-grow fa-5x <?php echo $icon ?> " style="color:#fafafa;"></i>
                                    </div>
                                    <div class="text-center" style="min-height:50px; background-color:white">
                                        <!--<h1 class="font-light text-white">2.9s</h1>-->
                                        <?php if ($title) { ?>
                                            <h5 class="font-light " style="line-height: 40px!important"><?php echo $title ?></h5>
                                        <?php } ?>

                                        <?php if ($subtitle) { ?>
                                            <h6 class="text-muted"  style="line-height: 40px!important"><?php echo $subtitle ?></h6>
                                        <?php } ?>
                                    </div>
                                </div>
                            </a>

                        </div>
                        <?php
                    }

                }

                function bs3_input_for_table($string = "", $value = "") {
                    $output = "";
                    $output .= "<td> ";
                    $output .= "<input ";
                    $output .= 'name="';
                    $output .= $string . '" ';
                    $output .= 'value="';
                    $output .= $value . '" ';
                    $output .= 'type="input"';
                    $output .= ' class="form-control"';
                    $output .= " >";
                    $output .= " $value";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_textarea_for_table($string = "", $value = "") {
                    $output = "";
                    $output .= "<td> ";
                    $output .= "<textarea ";
                    $output .= 'name="';
                    $output .= $string . '" ';
                    $output .= 'value="';
                    $output .= $value . '" ';
                    $output .= 'type="input"';
                    $output .= ' class="form-control"';
                    $output .= " >";
                    $output .= " $value";
                    $output .= "</textarea>";
                    $output .= "</td>";
                    return $output;
                }

                function bs3_dropdown_for_table($name = "", $options = "", $selected = false) {
                    $output = "";
                    $output .= "<td> ";
                    $output .= "<select ";
                    $output .= 'name="';
                    $output .= $name . '" ';
                    $output .= ' class="form-control"';
                    $output .= " >";
                    foreach ($options as $key => $val) {
                        $sel = ($key == $selected) ? ' selected="selected"' : '';
                        $output .= '<option value="' . $key . '"' . $sel . '>' . $val . "</option>\n";
                    }
                    $output .= "</select>";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_field_display_none($value = "") {
                    $output = "";
                    $output .= "<td ";
                    $output .= 'style="display:none">';
                    $output .= $value;
                    $output .= "</td>";

                    return $output;
                }

                function bs3_checkbox_for_table($string = "", $class = "") {
                    $output = "";
                    $output .= "<td> ";
//  $output .= '<div class="form-group row"> ';
//  $output .= ' <div class="col-md-9"> ';
//  $output .= '  <div class="checkbox checkbox-success"> ';

                    $output .= "<input ";
                    $output .= 'name="';
                    $output .= $string . '" ';
                    $output .= 'class="';
                    $output .= $class . '" ';
                    $output .= 'value="1" ';
                    $output .= 'type="checkbox"';
                    $output .= " >";

//  $output .= "<label ";
//  $output .= 'for="checkbox';
//  $output .= $string . '" ';
//  $output .= " ></label>";
//  $output .= " </div>";
//  $output .= " </div>";
//  $output .= " </div>";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_link_crud($link = "", $string = '', $title = '', $type = 'update') {
                    $new_link = base_url($link);
                    $output = "";
                    $output .= "<td class='non_printable'>";
                    $output .= "<a ";
                    if ($link) {
                        $output .= "href='" . $new_link . "' ";
                    } else {
                        $output .= "href='javascript:void(0)' ";
                    }
                    $output .= " >";
                    $output .= "<span ";
                    $output .= "data-toggle='tooltip' ";
                    $output .= "title='$title' ";
                    $output .= "aria-describedby='tooltip80070' ";
                    $output .= ">";
                    $output .= $string;
                    $output .= "</span>";
                    $output .= "</a>";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_file_download($link = "", $string = '', $title = '', $type = 'update') {


                    $new_link = base_url($link);
                    $output = "";
                    $output .= "<td>";
                    $output .= "<a  ";
                    if ($link) {
                        $output .= "href='" . $new_link . "' download ";
                    } else {
                        $output .= "href='javascript:void(0)' ";
                    }
                    $output .= "target='_blank' >";
                    $output .= "<span ";
                    $output .= "data-toggle='tooltip' ";
                    $output .= "title='$title' ";
                    $output .= "aria-describedby='tooltip80070' ";
                    $output .= ">";
                    $output .= $string;
                    $output .= "</span>";
                    $output .= "</a>";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_update_delete_crud($object_id, $string = '', $title = '', $type = 'update') {

                    /* <span title="" data-toggle="tooltip" data-original-title="<?php echo lang($title) ?>" aria-describedby="tooltip80070" > */

                    $output = "";
                    $output .= "<td>";
                    $output .= "<a ";
                    $output .= "href='javascript:void(0)' ";
                    if ($type == 'update') {
                        $output .= "onclick=";
                        $output .= '"edit_object(';
                        $output .= "'" . $object_id . "'";
                        $output .= ')"';
//$output .= "onclick='edit_object(".$object_id.")'";
                    } else {
                        $output .= "style='color:#FF5C6C' onclick=";
                        $output .= '"delete_object(';
                        $output .= "'" . $object_id . "'";
                        $output .= ')" ';
                    }

                    $output .= " >";
                    $output .= "<span ";
                    $output .= "data-toggle='tooltip'  ";
                    $output .= "title='$title' ";
                    $output .= "aria-describedby='tooltip80070' ";
                    $output .= ">";
                    $output .= $string;
                    $output .= "</span>";
                    $output .= "</a>";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_details_crud($object_id, $string = '', $title = '', $type = 'detail') {

                    /* <span title="" data-toggle="tooltip" data-original-title="<?php echo lang($title) ?>" aria-describedby="tooltip80070" > */

                    $output = "";
                    $output .= "<td>";
                    $output .= "<a ";
                    $output .= "href='javascript:void(0)' ";

                    $output .= "onclick=";
                    $output .= '"detail_object(';
                    $output .= "'" . $object_id . "'";
                    $output .= ')"';
//$output .= "onclick='edit_object(".$object_id.")'";

                    $output .= " >";
                    $output .= "<span ";
                    $output .= "data-toggle='tooltip' ";
                    $output .= "title='$title' ";
                    $output .= "aria-describedby='tooltip80070' ";
                    $output .= ">";
                    $output .= $string;
                    $output .= "</span>";
                    $output .= "</a>";
                    $output .= "</td>";

                    return $output;
                }

                function bs3_update_crud($object_id, $icon = "fa fa-2x fa-pencil ") {
                    ?>
                    <td>
                        <a href="javascript:void(0)" onclick="delete_object(<?php echo '"' . $object_id . '"' ?>)">
                            <?php echo bs3_tip('<i class="' . $icon . '" ></i>', lang('edit')); ?>
                        </a>
                    </td>
                    <?php
                }

                if (!function_exists('bs3_date')) {

                    function bs3_date($name, $value = '', $is_required = false, $extra = false) {
                        ?>
                        <div class="example">

                            <div class="form-group row">
                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span> </label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" <?php if ($is_required) echo 'required' ?>  name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>"  value="<?php echo (empty($value) || $value == '0000-00-00') ? false : date_format(date_create_from_format('m-d-Y', $value), 'Y-m-d'); ?>" class="form-control mydatepicker"  <?php echo $extra ?>>
                                        <span class="input-group-addon calender_icon"><i class="mdi mdi-calendar-text "></i></span>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php
                    }

                }
                if (!function_exists('bs3_date_for_certificae_setting')) {

                    function bs3_date_for_certificae_setting($name, $input_id = '', $name_for_display = '', $value = '', $is_required = false, $extra = false) {
                        ?>
                        <div class="example">
                            <div class="form-group row">
                                <label for="input<?php echo ucfirst($name) . $input_id; ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name) . ' ' . lang('for') . ' ' . $name_for_display; ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span> </label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" <?php if ($is_required) echo 'required' ?>  name="<?php echo $name . $input_id; ?>" id="input<?php echo ucfirst($name) . $input_id; ?>"  value="<?php echo (empty($value) || $value == '0000-00-00') ? false : $value; ?>" class="form-control mydatepicker"  <?php echo $extra ?>>
                                        <span class="input-group-addon calender_icon"><i class="mdi mdi-calendar-text "></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }

                }

                if (!function_exists('bs3_birthday_in_profile')) {

                    function bs3_birthday_in_profile($name, $value = '', $is_required = false, $extra = false) {
                        ?>
                        <div class="example">

                            <div class="form-group row">
                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span> </label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" <?php if ($is_required) echo 'required' ?>  name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>"  value="<?php echo (empty($value) || $value == '0000-00-00') ? false : $value ?>" class="form-control mydatepicker"  <?php echo $extra ?>>
                                        <span class="input-group-addon calender_icon"><i class="mdi mdi-calendar-text "></i></span>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php
                    }

                }
                if (!function_exists('bs3_modal_header')) {

                    function bs3_modal_header($name = '', $title = 'update', $extra = 'fa fa-pencil', $no_icon = false, $ajax_url = true, $action = 'update', $temp_url = '', $form_id = "form_crud") {
                        ?>
                        <?php if ($action != 'delete') { ?>
                            <?php if (!$no_icon) { ?>
                                <a  href="#" title="" data-toggle="tooltip" title="<?php echo lang($title) ?>" aria-describedby="tooltip80070"  ><i  data-toggle="modal" data-target="#<?php echo $name; ?>" title="<?php echo lang($title) ?>" class=" <?php echo $extra; ?> "></i></a>
                            <?php } else { ?>
                                <button type="button" class="btn btn-success btn-rounded hvr-icon-spin hvr-shadow" data-toggle="modal" data-target="#<?php echo $name; ?>"><i class="<?php echo $extra ?>"></i> <?php echo '  ' . lang($no_icon) ?></button>
                            <?php } ?>
                        <?php } ?>

                        <div class="modal fade bs-example-modal-lg"  id="<?php echo $name; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                        <h4 class="modal-title" id="myLargeModalLabel" style="color: white !important;"><?php echo lang($title) ?></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $action_form = "action-form='" . $action . "'";
                                        $form_id = 'id="' . $form_id . '"';
                                        if ($ajax_url) {
                                            $class = "class='form-horizontal form-ajax'" . "url-data='" . $ajax_url . "'";
                                        } else {
                                            $class = "class='form-horizontal'";
                                        }

                                        echo form_open($temp_url, $form_id . " role='form'   enctype= 'multipart/form-data'" . $class . ' ' . $action_form);
                                        ?>

                                        <?php
                                    }

                                }

                                if (!function_exists('bs3_modal_header_lg_crud')) {

                                    function bs3_modal_header_lg_crud($name = '', $title = 'update', $icon = 'fa fa-pencil', $btn_string = false, $form_id = "form_crud", $add_btn = true) {
                                        ?>

                                        <?php if ($add_btn) { ?>
                                            <?php if (!$btn_string) { ?>
                                                <a  href="#" title="" data-toggle="tooltip" title="<?php echo lang($title) ?>" aria-describedby="tooltip80070"  ><i  data-toggle="modal" data-target="#<?php echo $name; ?>" title="<?php echo lang($title) ?>" class=" <?php echo $icon; ?> "></i></a>
                                            <?php } else { ?>
                                                <button id="id<?php echo $name; ?>" onclick="add_object('<?php echo $name; ?>')" type="button" class="btn btn-success btn-rounded hvr-icon-spin hvr-shadow"  data-toggle="modal"><i class="<?php echo $icon ?>"></i> <?php echo '  ' . lang($btn_string) ?></button>
                                            <?php } ?>

                                        <?php } ?>
                                        <div class="modal fade bs-example-modal-lg"  id="<?php echo $name; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                        <h4 class="modal-title" id="myLargeModalLabel" style="color: white !important;"><?php echo lang($title) ?></h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php
                                                        $form_id = 'id="' . $form_id . '"';
                                                        echo form_open("", $form_id . " role='form'   enctype= 'multipart/form-data'");
                                                        ?>

                                                        <?php
                                                    }

                                                }


                                                if (!function_exists('bs3_modal_header_crud')) {

                                                    function bs3_modal_header_crud($name = '', $title = 'update', $icon = 'fa fa-pencil', $btn_string = false, $form_id = "form_crud", $add_btn = true) {
                                                        ?>

                                                        <?php if ($add_btn) { ?>
                                                            <?php if (!$btn_string) { ?>
                                                                <a  href="#" title="" data-toggle="tooltip" title="<?php echo lang($title) ?>" aria-describedby="tooltip80070"  ><i  data-toggle="modal" data-target="#<?php echo $name; ?>" title="<?php echo lang($title) ?>" class=" <?php echo $icon; ?> "></i></a>
                                                            <?php } else { ?>
                                                                <button id="id<?php echo $name; ?>" onclick="add_object('<?php echo $name; ?>')" type="button" class="btn btn-rounded hvr-icon-spin hvr-shadow"  data-toggle="modal" style="color:white; background-color:#FF8C00;"><i class="<?php echo $icon ?>"></i> <?php echo '  ' . lang($btn_string) ?></button>
                                                            <?php } ?>

                                                        <?php } ?>
                                                        <div class="modal fade"  id="<?php echo $name; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                        <h4 class="modal-title" id="myLargeModalLabel" style="color: white !important;"><?php echo lang($title) ?></h4>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        $form_id = 'id="' . $form_id . '"';
                                                                        echo form_open("", $form_id . " role='form'   enctype= 'multipart/form-data'");
                                                                        ?>

                                                                        <?php
                                                                    }

                                                                }

                                                                if (!function_exists('bs3_modal_footer')) {

                                                                    function bs3_modal_footer($type = 'update', $i = '0') {
                                                                        ?>
                                                                        <div class="modal-footer" style="padding-bottom: 0px;">
                                                                            <?php
                                                                            switch ($type) {
                                                                                case 'no_submit': {
                                                                                        break;
                                                                                    }
                                                                                case 'create': {
                                                                                        ?>
                                                                                        <button type="button" onclick="save()" class="btn btn-info text-left btnSave btn-rounded hvr-icon-spin hvr-shadow" value="true" id="btnSaveid" name="create"><?php echo lang('save'); ?></button>
                                                                                        <?php
                                                                                        break;
                                                                                    }
                                                                                case 'update': {
                                                                                        ?>
                                                                                        <button type="submit" class="btn btn-info text-left  btnSave btn-rounded hvr-icon-spin hvr-shadow" value="true" name="update"><?php echo lang('save'); ?></button>
                                                                                        <?php
                                                                                        break;
                                                                                    }
                                                                                case 'delete': {
                                                                                        ?>
                                                                                        <button type="submit" class="btn btn-danger text-left btn-rounded hvr-icon-spin hvr-shadow" value="true" name="delete"><?php echo lang('delete'); ?></button>
                                                                                        <?php
                                                                                        break;
                                                                                    }
                                                                                case 'detail': {
                                                                                        ?>
                                                                                        <?php
                                                                                        break;
                                                                                    }
                                                                                case 'yes': {
                                                                                        ?>
                                                                                        <button type="submit" class="btn btn-primary text-left btn-rounded hvr-icon-spin hvr-shadow" value="true" name="yes"><?php echo lang('yes'); ?></button>
                                                                                        <?php
                                                                                        break;
                                                                                    }
                                                                                default : {
                                                                                        ?>
                                                                                        <button type="submit" class="btn btn-primary text-left btn-rounded hvr-icon-spin hvr-shadow" value="true" name="submit"><?php echo lang('save'); ?></button>
                                                                                        <?php
                                                                                        break;
                                                                                    }
                                                                            }
                                                                            ?>
                                                                            <button type="button" class="btn btn-default text-left btn-rounded hvr-icon-spin hvr-shadow" data-dismiss="modal"><?php echo lang('close') ?></button>

                                                                        </div>

                                                                    </div>
                                                                    <?php echo form_close() ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }

                                                }
                                                if (!function_exists('bs3_card')) {

                                                    function bs3_card($title_ = false, $subtitle = false, $reload = true) {
                                                        ?>
                                                        <div class="card" style="border-bottom: 8px solid #20AEE3;">
                                                            <!--    <div class="card-header" style="    border-bottom: 2px solid #46bce8;background-color: #fff;">-->
                                                            <div class="card-header non_printable" style="">
                                                                <?php
                                                                $CI = & get_instance();
                                                                $lang = $CI->session->userdata('lang');
                                                                ?>
                                                                <?php if ($lang == "english") { ?>
                                                                    <div class="card-actions" style="float: right">
                                                                    <?php } else { ?>
                                                                        <div class="card-actions" style="float: left">
                                                                        <?php } ?>
                                                                        <?php if ($reload) { ?>
                                                                            <a class="fix-lang btn-close"  onclick="reload_table()"><i class="mdi mdi-reload"></i></a>
                                                                        <?php } ?>
                                                                        <a class="fix-lang" data-action="collapse"><i class="ti-minus"></i></a>
                                                                        <a class="fix-lang btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                                                    </div>
                                                                    <?php if ($title_) { ?>
                                                                        <span class="n h5"><?php echo $title_; ?> ...</span>
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="card-body collapse show">
                                                                    <?php if ($subtitle) { ?>
                                                                        <h5 class="card-title text-center"><b><?php echo $subtitle; ?></b></h5>
                                                                    <?php } ?>
                                                                    <?php
                                                                }

                                                            }

                                                            if (!function_exists('bs3_card_f')) {

                                                                function bs3_card_f() {
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <?php
                                                        }

                                                    }
                                                    if (!function_exists('bs3_form_open')) {

                                                        function bs3_form_open() {
                                                            ?>

                                                            <?php
                                                        }

                                                    }

                                                    if (!function_exists('bs3_card_old')) {

                                                        function bs3_card_old($title_ = false, $subtitle = false) {
                                                            ?>

                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <?php if ($title_) { ?>
                                                                                <h4 class="card-title text-center"><?php echo $title_; ?></h4>
                                                                            <?php } ?>
                                                                            <?php if ($subtitle) { ?>
                                                                                <h6 class="card-subtitle  text-center"><?php echo $subtitle; ?></h6>
                                                                            <?php } ?>
                                                                            <?php
                                                                        }

                                                                    }

                                                                    if (!function_exists('bs3_card_f_old')) {

                                                                        function bs3_card_f_old() {
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <?php
                                                        }

                                                    }

                                                    if (!function_exists('bs3_input')) {

                                                        function bs3_input($name, $value = '', $is_required = false, $hidden_in = false, $note = false, $extra = false) {
                                                            ?>
                                                            <!--        --><?php //echo $_SESSION['lang']                                                                                        ?>
                                                            <div class="form-group row">
                                                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span></label>
                                                                <div class="col-md-8">
                                                                    <input type="text" <?php if ($is_required) echo 'required' ?> hidden-in="<?php echo $hidden_in ?>"  id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?>>
                                                                    <?php if ($note) { ?>
                                                                        <small class="form-control-feedback" style="color:red"> <?php echo lang($note) ?> </small>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }

                                                    }



                                                    if (!function_exists('bs3_number')) {

                                                        function bs3_number($name, $value = '', $extra = false, $is_required = false, $note = false) {
                                                            ?>
                                                            <div class="form-group row">
                                                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: </label>
                                                                <div class="col-md-8">
                                                                    <input  min="0" type="number" <?php if ($is_required) echo 'required' ?>  id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?>>
                                                                    <?php if ($note) { ?>
                                                                        <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }

                                                    }

                                                    if (!function_exists('bs3_tel')) {

                                                        function bs3_tel($name, $value = '', $extra = false, $is_required = false, $note = false) {
                                                            ?>
                                                            <div class="form-group row">
                                                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: </label>
                                                                <div class="col-md-8">
                                                                    <input type="tel"  <?php if ($is_required) echo 'required' ?>  id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?>>
                                                                    <?php if ($note) { ?>
                                                                        <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }

                                                    }

                                                    if (!function_exists('bs3_email')) {

                                                        function bs3_email($name, $value = '', $is_required = false, $hidden_in = false, $note = false, $extra = false) {
                                                            ?>
                                                            <div class="form-group row">
                                                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span></label>
                                                                <div class="col-md-8">
                                                                    <input type="email" <?php if ($is_required) echo 'required' ?>  hidden-in="<?php echo $hidden_in ?>"    id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?> placeholder="">
                                                                    <?php if ($note) { ?>
                                                                        <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }

                                                    }

                                                    if (!function_exists('bs3_number')) {

                                                        function bs3_number($name, $value = '', $is_required = false, $hidden_in = false, $note = false, $extra = false) {
                                                            ?>
                                                            <div class="form-group row">
                                                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span></label>
                                                                <div class="col-md-8">
                                                                    <input type="number" <?php if ($is_required) echo 'required' ?> hidden-in="<?php echo $hidden_in ?>"  id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?>>
                                                                    <?php if ($note) { ?>
                                                                        <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>

                                                            <?php
                                                        }

                                                    }

                                                    if (!function_exists('bs3_password')) {

                                                        function bs3_password($name, $value = '', $is_required = false, $hidden_in = false, $note = false, $extra = false) {
                                                            ?>
                                                            <div class="form-group row">
                                                                <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span></label>
                                                                <div class="col-md-8">
                                                                    <input type="password" min="0" <?php if ($is_required) echo 'required' ?>    hidden-in="<?php echo $hidden_in ?>"     id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?> placeholder="">
                                                                    <?php if ($note) { ?>
                                                                        <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }

                                                    }
                                                    if (!function_exists('bs3_table')) {

                                                        function bs3_table($thead_table = false, $class = "my_table", $title_ = false, $subtitle = false, $id = 'table-style', $non_printable = '', $style = '', $specific_th_style = '') {
                                                            if ($title_) {
                                                                ?>
                                                                <br>
                                                                <h4 class="card-title" style="margin-top: 15px"><?php echo $title_; ?></h4>
                                                            <?php } ?>
                                                            <?php if ($subtitle) { ?>
                                                                <h6 class="card-subtitle"><?php echo $subtitle; ?></h6>
                                                            <?php } ?>
                                                            <div class="table-responsive m-t-40" style="<?php echo $style ?>">

                                                                <table class="<?php echo $class; ?> display nowrap table  table  table-hover table-striped table-bordered color-bordered-table info-bordered-table " cellspacing="0" width="100%" id="<?php echo $id; ?>">
                                                                    <?php if ($thead_table) { ?>
                                                                        <thead>
                                                                            <tr >
                                                                                <?php if (sizeof($thead_table) > 1) { ?>
                                                                                    <?php foreach ($thead_table as $item) { ?>
                                                                                        <?php if ($non_printable != '' && in_array($item, $non_printable)) { ?>
                                                                                            <?php if ($item == 'assistant_teacher') { ?>
                                                                                                <th style="<?php echo $specific_th_style; ?>" class="non_printable"><?php echo lang($item); ?></th>
                                                                                            <?php } else { ?> 
                                                                                                <th style="" class="non_printable"><?php echo lang($item); ?></th>
                                                                                            <?php } ?>
                                                                                        <?php } else { ?>
                                                                                            <?php if ($item == 'assistant_teacher') { ?>
                                                                                                <th style="<?php echo $specific_th_style; ?>" class="printable"><?php echo lang($item); ?></th>
                                                                                            <?php } else { ?> 
                                                                                                <th style="" class="printable"><?php echo lang($item); ?></th>
                                                                                            <?php } ?>
                                                                                        <?php } ?>

                                                                                    <?php } ?>
                                                                                <?php } else {
                                                                                    ?>
                                                                                    <?php foreach ($thead_table as $item) { ?>
                                                                                        <?php if ($item == 'assistant_teacher') { ?>
                                                                                            <th style="<?php echo $specific_th_style; ?> ><?php echo lang($item); ?></th>
                                                                                        <?php } else { ?>
                                                                                                <th style="<?php echo $specific_th_style; ?> ><?php echo lang($item); ?></th>
                                                                                            <?php } ?>
                                                                                            <?php break; ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                            </tr>
                                                                        </thead>

                                                                    <?php } ?>

                                                                    <?php
                                                                }

                                                            }

                                                            if (!function_exists('bs3_table_without_pagination')) {

                                                                function bs3_table_without_pagination($thead_table = false, $class = "my_table", $title_ = false, $subtitle = false) {
                                                                    if ($title_) {
                                                                        ?>
                                                                        <br>
                                                                        <h4 class="card-title" style="margin-top: 15px"><?php echo $title_; ?></h4>
                                                                    <?php } ?>
                                                                    <?php if ($subtitle) { ?>
                                                                        <h6 class="card-subtitle"><?php echo $subtitle; ?></h6>
                                                                    <?php } ?>
                                                                    <div class="table-responsive m-t-40">

                                                                        <table class="<?php echo $class; ?> display nowrap  table  table-hover table-striped table-bordered " cellspacing="0" width="100%">
                                                                            <?php if ($thead_table) { ?>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <?php if (sizeof($thead_table) > 1) { ?>
                                                                                            <?php foreach ($thead_table as $item) { ?>
                                                                                                <th><?php echo lang($item); ?></th>
                                                                                            <?php } ?>
                                                                                        <?php } else {
                                                                                            ?>
                                                                                            <?php foreach ($thead_table as $item) { ?>
                                                                                                <th><?php echo lang($item); ?></th>
                                                                                                <?php break; ?>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                    </tr>
                                                                                </thead>

                                                                            <?php } ?>

                                                                            <?php
                                                                        }

                                                                    }

                                                                    if (!function_exists('bs3_table_f')) {

                                                                        function bs3_table_f() {
                                                                            ?>
                                                                        </table>
                                                                    </div>
                                                                    <?php
                                                                }

                                                            }
                                                            if (!function_exists('bs3_tip')) {

                                                                function bs3_tip($string, $title = '') {
                                                                    ?>
                                                                    <span title="" data-toggle="tooltip" title="<?php echo lang($title) ?>" aria-describedby="tooltip80070" >
                                                                        <?php echo $string ?>
                                                                    </span>
                                                                    <?php
                                                                }

                                                            }
                                                            if (!function_exists('bs3_td_delete')) {

                                                                function bs3_td_delete() {
                                                                    ?>
                                                                    <td title="<?php echo lang('delete'); ?>" class="non_printable text-danger hover-opcity cursor-pointer" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-2x fa-trash" title = '<?php echo lang('delete') ?>'></i></td>
                                                                    <?php
                                                                }

                                                            }

                                                            if (!function_exists('bs3_card_wid')) {

                                                                function bs3_card_wid($link = false, $icon, $title = false, $subtitle = false, $color = '', $data_lable = false, $text_align = 'text-right') {
                                                                    ?>

                                                                    <div class="col-lg-4 col-md-6">
                                                                        <a href="<?php echo $link ?>">
                                                                            <div class="card">
                                                                                <div class="card-body  hvr-icon-pulse hvr-float-shadow">
                                                                                    <div class="row p-t-10 p-b-10">
                                                                                        <!-- Column -->
                                                                                        <div class="col p-r-0">
                                                                                            <?php if ($title) { ?>
                                                                                                <h5 class="font-light"><?php echo $title ?></h5>
                                                                                            <?php } ?>

                                                                                            <?php if ($subtitle) { ?>
                                                                                                <h6 class="text-muted"><?php echo $subtitle ?></h6>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                        <!-- Column -->
                                                                                        <div class="col <?php echo $text_align ?> align-self-center">
                                                                                            <div data-label="<?php echo $data_lable . '%' ?>" class="css-bar m-b-0  <?php echo 'css-bar-' . $color ?>  <?php echo 'css-bar-' . $data_lable ?>"><i class="hvr-icon <?php echo $icon ?>"></i></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>

                                                                    </div>
                                                                    <?php
                                                                }

                                                            }

                                                            if (!function_exists('bs3_switch')) {

                                                                function bs3_switch($name, $value = '', $extra = false, $is_required = false, $note = false) {
                                                                    ?>
                                                                    <div class="form-group  row">
                                                                        <label  for="input<?php echo ucfirst($name); ?>"  style="line-height: 3"   class="control-label  col-md-4"><?php echo lang($name); ?>: </label>
                                                                        <div class="col-md-8">
                                                                            <input type="checkbox" <?php if ($value) echo 'checked' ?>  <?php if ($is_required) echo 'required' ?>  class="js-switch" data-color="#009efb" />
                                                                            <?php if ($note) { ?>
                                                                                <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>

                                                                    <?php
                                                                }

                                                            }
                                                            if (!function_exists('bs3_checkbox')) {

                                                                function bs3_checkbox($name, $value = '', $id = '', $extra = false, $is_required = false, $note = '', $hidden_in = false) {
                                                                    ?>

                                                                    <div class="form-group row">
                                                                        <?php if (empty($note)) { ?>
                                                                            <label class="control-label  col-md-4"  style="line-height: 3" ><?php echo lang($name); ?>:</label>
                                                                        <?php } else { ?>
                                                                            <label class="control-label  col-md-4"  style="line-height: 3" ><?php echo $note; ?>:</label>
                                                                        <?php } ?>
                                                                        <div class="col-md-8" style="padding-top: 14px!important;">
                                                                            <div class="checkbox checkbox-success">
                                                                                <input  name="<?php echo $name; ?>" id="checkbox<?php echo $name . $id; ?>" hidden-in="<?php echo $hidden_in ?>" value= "1" <?php
                                                                                if ($value == '1') {
                                                                                    echo 'checked';
                                                                                }
                                                                                ?>    type="checkbox" >
                                                                                        <?php // var_dump($value);         ?>                  
                                                                                <label for="checkbox<?php echo $name . $id; ?>"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <?php
                                                                }

                                                            }




                                                            if (!function_exists('bs3_dropdown')) {

                                                                function bs3_dropdown($name, $options, $selected = false, $extra = false, $options_extra = false, $hidden_in = false) {
                                                                    ?>
                                                                    <div class="form-group row" id="form<?php echo ucfirst($name); ?>">

                                                                        <?php if (lang($name)): ?>
                                                                            <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 control-label"><?php echo lang($name); ?>:</label>
                                                                        <?php else: ?>
                                                                            <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 control-label "></label>
                                                                        <?php endif; ?>
                                                                        <div class="col-sm-8">
                                                                            <select name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>"hidden-in="<?php echo $hidden_in ?>"  class="form-control input<?php echo ucfirst($name); ?>" <?php echo $extra ?>>
                                                                                <?php
                                                                                foreach ($options as $key => $val) {
                                                                                    $sel = ($key == $selected) ? ' selected="selected"' : '';
                                                                                    if ($options_extra && isset($options_extra[$key])) {
                                                                                        echo '<option value="' . $key . '"' . $sel . ' ' . $options_extra[$key]['key'] . '="' . $options_extra[$key]['value'] . '">' . $val . "</option>\n";
                                                                                    } else
                                                                                        echo '<option value="' . $key . '"' . $sel . '>' . $val . "</option>\n";
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }

                                                            }


                                                            if (!function_exists('bs3_dropdown_auto')) {

                                                                function bs3_dropdown_auto($name, $options, $selected = false, $extra = false, $options_extra = false) {
                                                                    ?>
                                                                    <div  class="form-group row " id="form<?php echo ucfirst($name); ?>">
                                                                        <div class="col-sm-3">
                                                                            <?php if (lang($name)): ?>
                                                                                <label for="input<?php echo ucfirst($name); ?>" class="control-label label-selec"><?php echo lang($name); ?>:</label>
                                                                            <?php else: ?>
                                                                                <label for="input<?php echo ucfirst($name); ?>" class="control-label  label-selec"></label>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <div class="col-sm-9 select2-style">
                                                                            <div  class="select2 form-control custom-select" style="width: 100%; height:36px;">


                                                                                <select name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>" class="form-control input<?php echo ucfirst($name); ?>" <?php echo $extra ?>>
                                                                                    <optgroup label="Alaskan/Hawaiian Time Zone">
                                                                                        <?php
                                                                                        foreach ($options as $key => $val) {
                                                                                            $sel = ($key == $selected) ? ' selected="selected"' : '';
                                                                                            if ($options_extra && isset($options_extra[$key])) {
                                                                                                echo '<option value="' . $key . '"' . $sel . ' ' . $options_extra[$key]['key'] . '="' . $options_extra[$key]['value'] . '">' . $val . "</option>\n";
                                                                                            } else
                                                                                                echo '<option value="' . $key . '"' . $sel . '>' . $val . "</option>\n";
                                                                                        }
                                                                                        ?>
                                                                                    </optgroup>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }

                                                                }


                                                                if (!function_exists('bs3_calendar_dates_note')) {

                                                                    function bs3_calendar_dates_note($calendar_dates, $msg = "", $extra_msg = array()) {
                                                                        ?>
                                                                        <div class="alert alert-warning non_printable">
                                                                            <?php if (!empty($msg)) { ?>
                                                                                <h5 style="color: red;"><?php echo $msg; ?></h5>
                                                                            <?php } ?>
                                                                            <h5><?php echo lang('start_year_date') . ": " . $calendar_dates['start']; ?></h5>
                                                                            <h5><?php echo lang('end_semester_1') . ": " . $calendar_dates['end_semester_1']; ?></h5>
                                                                            <h5><?php echo lang('start_semester_2') . ": " . $calendar_dates['start_semester_2']; ?></h5>
                                                                            <h5><?php echo lang('end_year_date') . ": " . $calendar_dates['end']; ?></h5>
                                                                            <?php foreach ($extra_msg as $key => $ms) { ?>
                                                                                <h5><?php echo lang($key) . ": " . $ms; ?></h5>
                                                                            <?php } ?>
                                                                        </div>

                                                                        <?php
                                                                    }

                                                                }


                                                                if (!function_exists('bs3_file_or_image')) {

                                                                    function bs3_file_or_image($name, $value = '', $extra = false, $max_siz = '100', $hidden_in = 'false', $upload = 'true') {
                                                                        ?>
                                                                        <?php
                                                                        $is_file = FALSE;
                                                                        $allowed_file = array("pdf", "docx", "doc", "xls", "xlsx", "csv");
                                                                        foreach ($allowed_file as $key) {
                                                                            if (preg_match("/.$key/i", $value)) {
                                                                                $is_file = TRUE;
                                                                                break;
                                                                            }
                                                                        }
                                                                        $allowed_types = array("jpg", "jpeg", "png", "gif", "pdf", "docx", "doc", "xls", "xlsx", "csv", "txt", "pptx");
                                                                        $is_val = FALSE;
                                                                        foreach ($allowed_types as $key) {
                                                                            if (preg_match("/.$key/i", $value)) {
                                                                                $is_val = TRUE;
                                                                                break;
                                                                            }
                                                                        }
                                                                        ?>
<!--                                                                        <div class="form-group row">
                                                                            <p style="color:red;" class="col-sm-12 col-md-12"><?php echo 'If you use' ?> <strong  style="font-weight: bold;"><?php echo 'Safari Browser' ?></strong> <?php echo 'you must be enter a file' ?> <p>   
                                                                        </div>-->
                                                                        <?php // if ($hidden_in == 'detail') {   ?>
                                                                        <div class="form-group row" id="form<?php echo ucfirst($name); ?>">
                                                                            <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 col-md-4 control-label"><?php echo lang($name); ?>:</label>
                                                                            <div class="col-sm-8 col-md-8">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <?php if (!$is_file) { ?>
                                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                                            <?php if (!$is_val) {
                                                                                                ?>
                                                                                                <img src="<?php echo base_url("assets/uploads/images/no_image.png"); ?>" >
                                                                                            <?php } else {
                                                                                                ?>
                                                                                                <img data-src = "<?php echo base_url("assets/uploads/$value"); ?>" src = "<?php echo base_url("assets/uploads/$value"); ?>" >
                                                                                            <?php }
                                                                                            ?>
                                                                                        </div>
                                                                                    <?php } ?>

                                                                                    <?php if ($is_file) { ?>
                                                                                        <?php
                                                                                        $ex = array();
                                                                                        $ex = explode('/', $value);
                                                                                        ?>

                                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; display: block !important"><a href="<?php echo base_url("assets/uploads/$value"); ?>"><?php echo end($ex) ?></a></div>

                                                                                    <?php } else { ?>
                                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>

                                                                                    <?php } ?>

                                                                                    <div id="div_for_file">
                                                                                        <?php if ($upload == 'true') { ?>
                                                                                            <span class="btn btn-default btn-file">
                                                                                                <span class="fileinput-new"><?php echo lang('choose_file') ?></span>
                                                                                                <span class="fileinput-exists"><?php echo lang('change') ?></span>
                                                                                                <input required="true" hidden-in="<?php echo $hidden_in ?>" type="file" accept=".pdf,.docx,.doc,.xls,.xlsx,.txt,.csv,.ppt,.pptx,.zip,.rar,.psd,.txt,.pps,image/*" id="file" name="<?php echo $name; ?>">
                                                                                            </span>
                                                                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo lang('remove') ?></a>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                            <?php
                                                                            $CI = & get_instance();
                                                                            $lang = $CI->session->userdata('lang');
                                                                            ?>
                                                                            <?php // if ($lang == "arabic") { ?>
                                                <!--<p style="margin-right: 216px;"><?php echo lang('en_name') ?></p>-->
                                                                            <?php // } else { ?> 
                                                <!--<p  style="margin-left: 216px;"><?php echo lang('en_name') ?></p>-->
                                                                            <?php // } ?> 

                                                                        </div>
                                                                        <?php // }  ?>
                                                                        <?php
                                                                    }

                                                                }






                                                                if (!function_exists('bs3_time')) {

                                                                    function bs3_time($name, $value = '', $extra = false) {
                                                                        ?>

                                                                        <div class="example">

                                                                            <div class="form-group row">
                                                                                <label for="input<?php echo ucfirst($name); ?>"  style="line-height: 3" class="control-label  col-md-3"><?php echo lang($name); ?>:</label>

                                                                                <div class="col-sm-9">
                                                                                    <div class="input-group clockpicker " data-placement="left" data-align="top" data-autoclose="true">
                                                                                        <input type="text"  name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>"  value="<?php echo (empty($value) || $value == '00:00') ? false : $value; ?>" class="form-control"  <?php echo $extra ?>>
                                                                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <?php
                                                                    }

                                                                }






                                                                if (!function_exists('bs3_file')) {

                                                                    function bs3_file($name, $old_file = false, $id = false) {
                                                                        ?>



                                                                        <div class="form-group">

                                                                            <label for="input" class="col-sm-4 control-label"><?php echo lang($name); ?>:</label>

                                                                            <div class="col-sm-8">

                                                                                <div class="checkbox col-sm-4">

                                                                                    <p>

                                                                                        <input type="checkbox" id="delete_file_<?php echo $id ?>" name="delete_file_<?php echo $name ?>" <?= $old_file ? '' : 'checked' ?> data-label="<?php echo lang('no_file') ?>">

                                                                                    </p>

                                                                                </div>

                                                                                <a class="col-sm-4 btn-download_<?php echo $id ?> btn btn-default" target="_blank" href="<?php echo base_url($old_file) ?>"  style="<?= $old_file ? '' : 'display: none' ?>">Download Old File</a>

                                                                                <div class="clearfix"></div>

                                                                                <p style="color:red;"><?php echo lang('english_file_name'); ?></p>

                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput" id="file_uploader_<?php echo $id ?>" style="<?= $old_file ? '' : 'display: none' ?>">

                                                                                    <div class="form-control" data-trigger="fileinput">

                                                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>

                                                                                        <span class="fileinput-filename"></span>

                                                                                    </div>

                                                                                    <span class="input-group-addon btn btn-default btn-file">

                                                                                        <span class="fileinput-new"><?php echo lang('select_file') ?></span>

                                                                                        <span class="fileinput-exists"><?php echo lang('change') ?></span>

                                                                                        <input type="file" name="<?php echo $name; ?>"></span>

                                                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo lang('delete') ?></a>

                                                                                </div>

                                                                                <script type="text/javascript" charset="utf-8">

                                                                                    $("#delete_file_<?php echo $id ?>").change(function () {

                                                                                        if (this.checked) {

                                                                                            // $('.btn-download_<?php echo $id ?>').hide();

                                                                                            $('#file_uploader_<?php echo $id ?>').stop().slideUp();

                                                                                        } else {

                                                                                            // $('.btn-download_<?php echo $id ?>').show();

                                                                                            $('#file_uploader_<?php echo $id ?>').stop().slideDown();

                                                                                        }

                                                                                    });

                                                                                </script>

                                                                            </div>

                                                                        </div>



                                                                        <?php
                                                                    }

                                                                }



                                                                if (!function_exists('bs3_image')) {

                                                                    function bs3_image($name, $old_img = false, $preview_img = false, $id = false) {
                                                                        ?>
                                                                        <div class="form-group">
                                                                            <label for="input" class="col-sm-4 control-label"><?php echo lang($name); ?>:</label>
                                                                            <div class="col-sm-8">
                                                                                <?php
                                                                                $img = $old_img;
                                                                                $image_url = $old_img ? base_url("assets/uploaded/$old_img") : base_url('assets/img/no_image.png');
                                                                                ?>
                                                                                <div class="checkbox">
                                                                                    <p>
                                                                                        <input type="checkbox" id="delete_image_<?php echo $id ?>" name="delete_image_<?php echo $name ?>" <?= $img ? '' : 'checked' ?> data-label="<?php echo lang('no_image') ?>">
                                                                                    </p>
                                                                                </div>
                                                                                <p style="color:red;"><?php echo lang('english_image_name'); ?></p>
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput" id="image_uploader_<?php echo $id ?>" style="<?= $img ? '' : 'display: none' ?>">
                                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                                        <img src="<?php echo $image_url ?>" alt="<?php echo ucfirst($name); ?>">
                                                                                    </div>
                                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                                                    <div>
                                                                                        <span class="btn btn-default btn-file">
                                                                                            <span class="fileinput-new"><?php echo lang('select_image') ?></span>
                                                                                            <span class="fileinput-exists"><?php echo lang('change') ?></span>
                                                                                            <input type="file" name="<?php echo $name; ?>"></span>
                                                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo lang('delete') ?></a>
                                                                                    </div>
                                                                                </div>



                                                                            </div>

                                                                        </div>



                                                                        <?php
                                                                    }

                                                                }



                                                                if (!function_exists('bs3_hidden')) {

                                                                    function bs3_hidden($name, $value = '', $extra = false) {
                                                                        ?>



                                                                        <input type="hidden" name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>" class="form-control input<?php echo ucfirst($name); ?>" value="<?php echo $value ?>" <?php echo $extra ?>>



                                                                        <?php
                                                                    }

                                                                }





                                                                if (!function_exists('bs3_modal_header_no_icon')) {

                                                                    function bs3_modal_header_no_icon($name = '', $title = 'update', $extra = 'fa fa-pencil') {
                                                                        ?>

                                                                        <!--edit modal-->
                                                                        <div class="modal fade" id="<?php echo $name; ?>">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                        <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                    </div>
                                                                                    <?php
                                                                                }

                                                                            }

                                                                            if (!function_exists('bs3_note')) {

                                                                                function bs3_note($string, $link = '') {
                                                                                    ?>
                                                                                    <?php if (!empty($link)) { ?>
                                                                                        <a class="non_printable" href="<?php echo $link ?>"><p style="padding-right: 18px; color: red;"><strong> <?php echo lang('note'); ?>: </strong> <?php echo $string ?></p></a>    
                                                                                    <?php } else { ?>
                                                                                        <p class="non_printable" style="padding-right: 18px!important;padding-left: 18px!important; color: red; padding-top: 0px!important;  padding-bottom: 0px!important ; "><strong>  <?php echo lang('note'); ?>: </strong> <?php echo $string ?></p>
                                                                                    <?php } ?>
                                                                                    <?php
                                                                                }

                                                                            }
                                                                            if (!function_exists('bs3_links')) {

                                                                                function bs3_links($link, $title, $icon, $color_array, $color) {
                                                                                    ?>
                                                                                    <div class="col-md-4">
                                                                                        <div class="popup-card">
                                                                                            <a href="<?php echo $link ?>">
                                                                                                <div class="pop-icon <?php echo $color_array[$color] ?>">
                                                                                                    <i class="fa fa-5x <?php echo $icon ?>"></i>
                                                                                                </div>

                                                                                                <p><?php echo $title ?></p>
                                                                                            </a>

                                                                                        </div>
                                                                                    </div>
                                                                                    <?php
                                                                                }

                                                                            }
                                                                            if (!function_exists('bs3_modal_no_metter')) {

                                                                                function bs3_modal_no_metter($name = '', $title = 'no_metter', $extra = 'fa fa-check-square-o') {
                                                                                    ?>
                                                                                    <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> "></i>
                                                                                    <!--edit modal-->
                                                                                    <div class="modal fade" id="<?php echo $name; ?>">
                                                                                        <div class="modal-dialog">
                                                                                            <div class="modal-content">
                                                                                                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                    <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                </div>

                                                                                                <?php
                                                                                            }

                                                                                        }

                                                                                        if (!function_exists('bs3_modal_pause_the_registration')) {

                                                                                            function bs3_modal_pause_the_registration($name = '', $title = 'pause_the_registration', $extra = 'fa fa-check-square-o') {
                                                                                                ?>

                                                                                                <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> "></i>
                                                                                                <!--edit modal-->
                                                                                                <div class="modal fade" id="<?php echo $name; ?>">
                                                                                                    <div class="modal-dialog">
                                                                                                        <div class="modal-content">
                                                                                                            <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                            <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                                <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                            </div>

                                                                                                            <?php
                                                                                                        }

                                                                                                    }
                                                                                                    if (!function_exists('bs3_modal_active_the_registration')) {

                                                                                                        function bs3_modal_active_the_registration($name = '', $title = 'active_the_registration', $extra = 'fa fa-check-square-o') {
                                                                                                            ?>

                                                                                                            <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> "></i>
                                                                                                            <!--edit modal-->
                                                                                                            <div class="modal fade" id="<?php echo $name; ?>">
                                                                                                                <div class="modal-dialog">
                                                                                                                    <div class="modal-content">
                                                                                                                        <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                                        <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                                            <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                                        </div>

                                                                                                                        <?php
                                                                                                                    }

                                                                                                                }



                                                                                                                if (!function_exists('bs3_modal_details')) {

                                                                                                                    function bs3_modal_details($name = '', $title = 'details', $extra = 'fa fa-search') {
                                                                                                                        ?>

                                                                                                                        <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> " style="color:#428bca;"></i>
                                                                                                                        <!--edit modal-->
                                                                                                                        <div class="modal fade" id="<?php echo $name; ?>">
                                                                                                                            <div class="modal-dialog">
                                                                                                                                <div class="modal-content">
                                                                                                                                    <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                                                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                                                        <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                                                    </div>

                                                                                                                                    <?php
                                                                                                                                }

                                                                                                                            }
                                                                                                                            if (!function_exists('bs3_modal_mark')) {

                                                                                                                                function bs3_modal_mark($name = '', $title = 'mark', $extra = 'fa fa-pencil') {
                                                                                                                                    ?>

                                                                                                                                    <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> " style="color:#428bca;"></i>
                                                                                                                                    <!--edit modal-->
                                                                                                                                    <div class="modal fade" id="<?php echo $name; ?>">
                                                                                                                                        <div class="modal-dialog">
                                                                                                                                            <div class="modal-content">
                                                                                                                                                <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                                                                <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                                                                    <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                                                                </div>

                                                                                                                                                <?php
                                                                                                                                            }

                                                                                                                                        }

                                                                                                                                        if (!function_exists('bs3_modal_transportation_sure')) {

                                                                                                                                            function bs3_modal_transportation_sure($name = '', $title = 'transportation_sure', $extra = 'ion-checkmark') {
                                                                                                                                                ?>

                                                                                                                                                <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> " style="font-size: x-large; cursor: pointer"></i>
                                                                                                                                                <!--edit modal-->
                                                                                                                                                <div class="modal fade" id="<?php echo $name; ?>">
                                                                                                                                                    <div class="modal-dialog">
                                                                                                                                                        <div class="modal-content">
                                                                                                                                                            <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                                                                            <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                                                                                <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                                                                            </div>

                                                                                                                                                            <?php
                                                                                                                                                        }

                                                                                                                                                    }

                                                                                                                                                    if (!function_exists('bs3_modal_sure_transportation_refuse')) {

                                                                                                                                                        function bs3_modal_sure_transportation_refuse($name = '', $title = 'sure_transportation_refuse', $extra = 'ion-close') {
                                                                                                                                                            ?>
                                                                                                                                                            <i  data-toggle="modal" data-target="#<?php echo $name; ?>" class=" <?php echo $extra; ?> " style="font-size: x-large; cursor: pointer"></i>
                                                                                                                                                            <!--edit modal-->
                                                                                                                                                            <div class="modal fade" id="<?php echo $name; ?>">
                                                                                                                                                                <div class="modal-dialog">
                                                                                                                                                                    <div class="modal-content">
                                                                                                                                                                        <?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
                                                                                                                                                                        <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                                                                                            <h4 class="modal-title" style="color: white !important;"><?php echo lang($title); ?></h4>
                                                                                                                                                                        </div>

                                                                                                                                                                        <?php
                                                                                                                                                                    }

                                                                                                                                                                }


                                                                                                                                                                if (!function_exists('bs3_textarea')) {

                                                                                                                                                                    function bs3_textarea($name, $value = '', $extra = false) {
                                                                                                                                                                        ?>
                                                                                                                                                                        <div class="form-group row">
                                                                                                                                                                            <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label col-md-4"><?php echo lang($name); ?>:</label>
                                                                                                                                                                            <div class="col-md-8">
                                                                                                                                                                                <textarea name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>" class="form-control" <?php echo $extra ?>><?php echo $value ?></textarea>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <?php
                                                                                                                                                                    }

                                                                                                                                                                }



                                                                                                                                                                if (!function_exists('bs3_submit')) {

                                                                                                                                                                    function bs3_submit($label = false, $extra = false) {

                                                                                                                                                                        if (!$label)
                                                                                                                                                                            $label = lang('save');
                                                                                                                                                                        ?>



                                                                                                                                                                        <div class="form-group">

                                                                                                                                                                            <div class="col-sm-8 col-sm-offset-2">

                                                                                                                                                                                <button type="submit" class="btn btn-primary submit_click" <?php echo $extra ?>><?php echo $label ?></button>

                                                                                                                                                                            </div>

                                                                                                                                                                        </div>



                                                                                                                                                                        <?php
                                                                                                                                                                    }

                                                                                                                                                                }

                                                                                                                                                                if (!function_exists('bs3_national_number')) {

                                                                                                                                                                    function bs3_national_number($name, $value = '', $extra = false) {
                                                                                                                                                                        ?>
                                                                                                                                                                        <div class="form-group" id="form<?php echo ucfirst($name); ?>">
                                                                                                                                                                            <label for="input<?php echo ucfirst($name); ?>" class="col-sm-2 control-label"><?php echo lang($name); ?>:</label>
                                                                                                                                                                            <div class="col-sm-10">
                                                                                                                                                                                <input type="text" pattern="[0-9,e]{11}" name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>" class="form-control" value="<?php echo $value ?>" <?php echo $extra ?>>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <?php
                                                                                                                                                                    }

                                                                                                                                                                }

                                                                                                                                                                if (!function_exists('bs3_number_with_dollar')) {

                                                                                                                                                                    function bs3_number_with_dollar($name, $value = '', $extra = false, $is_required = false, $note = false) {
                                                                                                                                                                        ?>
                                                                                                                                                                        <div class="form-group row">
                                                                                                                                                                            <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: </label>
                                                                                                                                                                            <div class="col-md-8">
                                                                                                                                                                                <div class="input-group">
                                                                                                                                                                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                                                                                                                                                                    <input min="0" type="number" <?php if ($is_required) echo 'required' ?>  id="input<?php echo ucfirst($name); ?>" name="<?php echo $name; ?>" class="form-control"  value="<?php echo htmlentities($value) ?>" <?php echo $extra ?>>

                                                                                                                                                                                    <?php if ($note) { ?>
                                                                                                                                                                                        <small class="form-control-feedback"> <?php echo lang($note) ?> </small>
                                                                                                                                                                                    <?php } ?>
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <?php
                                                                                                                                                                    }

                                                                                                                                                                }



                                                                                                                                                                if (!function_exists('bs3_radio')) {

                                                                                                                                                                    function bs3_radio($name, $extra = false, $hidden_in = false) {
                                                                                                                                                                        ?>
                                                                                                                                                                        <div class="form-group row" id="form<?php echo ucfirst($name); ?>">

                                                                                                                                                                            <?php if (lang($name)): ?>
                                                                                                                                                                                <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 control-label"><?php echo lang($name); ?>:</label>
                                                                                                                                                                            <?php else: ?>
                                                                                                                                                                                <label for="input<?php echo ucfirst($name); ?>" class="col-sm-4 control-label "></label>
                                                                                                                                                                            <?php endif; ?>
                                                                                                                                                                            <div class="col-sm-8">
                                                                                                                                                                                <?php echo lang('no'); ?>
                                                                                                                                                                                <input type="radio" name="<?php echo $name; ?>" value="2" checked="checked" id="input<?php echo ucfirst($name); ?>"hidden-in="<?php echo $hidden_in ?>"  class="input<?php echo ucfirst($name); ?>" <?php echo $extra ?>/>

                                                                                                                                                                                <?php echo lang('yes'); ?>
                                                                                                                                                                                <input type="radio" name="<?php echo $name; ?>" value="1" checked="checked"id="input<?php echo ucfirst($name); ?>"hidden-in="<?php echo $hidden_in ?>"  class="input<?php echo ucfirst($name); ?>" <?php echo $extra ?>/>


                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <?php
                                                                                                                                                                    }

                                                                                                                                                                }

                                                                                                                                                                function bs3_many_links($links, $student_record_id) {
                                                                                                                                                                    $output = "";
                                                                                                                                                                    $output .= "<td class='non_printable'>";
                                                                                                                                                                    foreach ($links as $key => $value) {
                                                                                                                                                                        $output .= "<a ";
                                                                                                                                                                        $output .= "href='" . base_url($value) . "/" . $student_record_id . "' ";
                                                                                                                                                                        $output .= " >";
                                                                                                                                                                        $output .= "<span ";
                                                                                                                                                                        $output .= "> Semester ";
                                                                                                                                                                        $output .= $key + 1;
                                                                                                                                                                        $output .= "</span>";
                                                                                                                                                                        $output .= "</a><br>";
                                                                                                                                                                    }
                                                                                                                                                                    $output .= "</td>";

                                                                                                                                                                    return $output;
                                                                                                                                                                }

                                                                                                                                                                if (!function_exists('bs3_modal_header_crud_custom')) {

                                                                                                                                                                    function bs3_modal_header_crud_custom($name = '', $title = 'update', $icon = 'fa fa-pencil fa-2x', $btn_string = false, $form_id = "form_crud", $add_btn = true) {
                                                                                                                                                                        ?>

                                                                                                                                                                        <?php if ($add_btn) { ?>
                                                                                                                                                                            <?php if (!$btn_string) { ?>
                                                                                                                                                                                <a  title="" data-toggle="tooltip" title="<?php echo lang($title) ?>" aria-describedby="tooltip80070"  ><i  data-toggle="modal" data-target="#<?php echo $name; ?>" title="<?php echo lang($title) ?>" class=" <?php echo $icon; ?> "></i></a>
                                                                                                                                                                            <?php } else { ?>
                                                                                                                                                                                <!--<button id="id<?php echo $name; ?>" onclick="add_object('<?php echo $name; ?>')" type="button" class="btn btn-success btn-rounded hvr-icon-spin hvr-shadow"  data-toggle="modal"><i class="<?php echo $icon ?>"></i> <?php echo '  ' . lang($btn_string) ?></button>-->
                                                                                                                                                                             <!--<button type="button" class="btn btn-info" onclick="save_fun()"><?php echo lang('save'); ?></button>-->
                                                                                                                                                                                <button id="id<?php echo $name; ?>" onclick="save_fun()" type="button" class="btn btn-success btn-rounded hvr-icon-spin hvr-shadow"  data-toggle="modal"><i class="<?php echo $icon ?>"></i> <?php echo '  ' . lang($btn_string) ?></button>

                                                                                                                                                                            <?php } ?>

                                                                                                                                                                        <?php } ?>
                                                                                                                                                                        <div class="modal fade"  id="<?php echo $name; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" >
                                                                                                                                                                            <div class="modal-dialog">
                                                                                                                                                                                <div class="modal-content">

                                                                                                                                                                                    <?php echo form_open('', 'class="form-horizontal" role="form"  enctype= "multipart/form-data" '); ?>
                                                                                                                                                                                    <div class="modal-header" style="background-color: #20aee3 !important; margin-bottom: 1px;">
                                                                                                                                                                                        <h4 class="modal-title" id="myLargeModalLabel" style="color: white !important;"><?php echo lang($title) ?></h4>
                                                                                                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                                                                                                                    </div>


                                                                                                                                                                                    <?php
                                                                                                                                                                                }

                                                                                                                                                                            }

                                                                                                                                                                            if (!function_exists('bs3_date_without_icon')) {

                                                                                                                                                                                function bs3_date_without_icon($name, $value = '', $is_required = false, $extra = false) {
                                                                                                                                                                                    ?>

                                                                                                                                                                                    <div class="form-group row ">
                                                                                                                                                                                        <label for="input<?php echo ucfirst($name); ?>" class="col-sm-1 control-label"><?php echo lang($name); ?></label>

                                                                                                                                                                                        <div class="col-sm-3 ">
                                                                                                                                                                                            <input type="text" <?php if ($is_required) echo 'required' ?>  name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>"  value="<?php echo (empty($value) || $value == '0000-00-00') ? false : $value; ?>" class="form-control"  <?php echo $extra ?>>

                                                                                                                                                                                        </div>

                                                                                                                                                                                    </div>
                                                                                                                                                                                    <style>
                                                                                                                                                                                        .form-control{
                                                                                                                                                                                            border: none!important;
                                                                                                                                                                                            border-bottom: none;
                                                                                                                                                                                            border-radius: unset!important;
                                                                                                                                                                                            background-color: #ffffff05!important;
                                                                                                                                                                                            text-align: center;
                                                                                                                                                                                            color:#000!important;

                                                                                                                                                                                        }
                                                                                                                                                                                        .control-label{
                                                                                                                                                                                            margin-left: 16px!important;
                                                                                                                                                                                            color:#000!important;
                                                                                                                                                                                        }
                                                                                                                                                                                        .form-control:disabled, .form-control[readonly] {
                                                                                                                                                                                            opacity: unset;
                                                                                                                                                                                        }

                                                                                                                                                                                        .col-sm-3{
                                                                                                                                                                                            flex: 0 0 20%!important;
                                                                                                                                                                                            max-width:  20%!important;
                                                                                                                                                                                        }
                                                                                                                                                                                        .col-sm-1{
                                                                                                                                                                                            flex: 0 0 10%!important;
                                                                                                                                                                                            max-width:  10%!important;
                                                                                                                                                                                        }
                                                                                                                                                                                    </style>
                                                                                                                                                                                    <?php
                                                                                                                                                                                }

                                                                                                                                                                            }

                                                                                                                                                                            if (!function_exists('bs3_date_for_details')) {

                                                                                                                                                                                function bs3_date_for_details($name, $value = '', $is_required = false, $extra = false) {
                                                                                                                                                                                    ?>
                                                                                                                                                                                    <div class="example">
                                                                                                                                                                                        <div class="form-group row">
                                                                                                                                                                                            <label for="input<?php echo ucfirst($name); ?>" style="line-height: 3" class="control-label  col-md-4"><?php echo lang($name); ?>: <span style="color: red"><?php if ($is_required) echo "*" ?></span> </label>
                                                                                                                                                                                            <div class="col-sm-8">
                                                                                                                                                                                                <div class="input-group">
                                                                                                                                                                                                    <input type="text" <?php if ($is_required) echo 'required' ?>  name="<?php echo $name; ?>" id="input<?php echo ucfirst($name); ?>"  value="<?php echo (empty($value) || $value == '0000-00-00') ? false : $value ?>" class="form-control mydatepicker"  <?php echo $extra ?>>
                                                                                                                                                                                                    <span class="input-group-addon calender_icon"><i class="mdi mdi-calendar-text "></i></span>
                                                                                                                                                                                                </div>
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>

                                                                                                                                                                                    <?php
                                                                                                                                                                                }

                                                                                                                                                                            }
