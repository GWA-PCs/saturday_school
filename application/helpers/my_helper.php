<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function color_index($i, $array_color) {
    if ($i >= sizeof($array_color)) {
        return 0;
    } else {
        return $i;
    }
}

if (!function_exists('check_lang_session')) {

    function check_lang_session() {
        $CI = & get_instance();
        if (!$CI->session->userdata('lang')) {
            $CI->session->set_userdata('lang', 'english');
            $lang = 'en';
        } else {
            if ($CI->session->userdata('lang') == 'arabic') {
                $lang = 'ar';
            } else {
                $lang = 'en';
            }
        }
        return $lang;
    }

}
if (!function_exists('change_lang')) {

    function change_lang($segment, $next_segment) {
        $CI = & get_instance();
        if ($segment == 'lang') {
            switch ($next_segment) {
                case 'en': {
                        $CI->session->set_userdata('lang', 'english');
                        $lang = 'en';
                        break;
                    }
                case 'ar': {
                        $CI->session->set_userdata('lang', 'arabic');
                        $lang = 'ar';
                        break;
                    }
            }
            $CI = & get_instance();
            $user_login = $CI->ion_auth->user()->row();

            if ($user_login->type == 'teacher') {
                redirect('home/home_page_teacher');
            } elseif ($user_login->type == 'parent') {
                redirect('home/home_page_parent');
            } else {
                redirect('home/home_page');
            }
        }
    }

}

if (!function_exists('exist_item')) {

    function exist_item($model, $data) {
        $CI = & get_instance();
        $result = $CI->$model->get_by($data);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('validation_edit_delete_redirect')) {

    function validation_edit_delete_redirect($array_view, $key, $value) {
        $CI = & get_instance();
        foreach ($array_view as $item) {
            if ($item->$key == $value) {
                return TRUE;
            }
        }
        return false;
    }

}

if (!function_exists('random_password')) {

    function random_password() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array();
        $alpha_length = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password);
    }

}

if (!function_exists('send_message')) {

    function send_message($data, $status = 200, $message = 'Good Request') {
        $response = array('status' => $status, 'message' => lang($message), 'data' => $data);
        exit(json_encode($response));
    }

}

if (!function_exists('send_message_without_die')) {

    function send_message_without_die($data, $status = 200, $message = 'Good Request') {
        $response = array('status' => $status, 'message' => lang($message), 'data' => $data);
        json_encode($response);
    }

}

if (!function_exists('error_message')) {

    function error_message($status = 204, $message = 'Bad Request') {
        $response = array('status' => $status, 'message' => lang($message));
        exit(json_encode($response));
    }

}

if (!function_exists('tinyint_lang')) {

    function tinyint_lang($str) {
        if ($str == 1)
            return lang('yes');
        else
            return lang('no');
    }

}


if (!function_exists('tinyint_lang_one_and_two')) {

    function tinyint_lang_one_and_two($str) {
        if ($str == 1)
            return lang('no');
        elseif ($str == 2)
            return lang('yes');
        else
            return lang('no');
    }

}

if (!function_exists('status_islamic_book_return_array')) {

    function status_islamic_book_return_array() {
        $status_islamic_book_return_array = array(
            'excellent' => lang('excellent'),
            'very_good' => lang('very_good'),
            'good' => lang('good'),
            'bad' => lang('bad'),
            'no_status' => lang('no_status'),
        );
        return $status_islamic_book_return_array;
    }

}

if (!function_exists('photo_permission_slips_array')) {

    function photo_permission_slips_array() {
        $photo_permission_slips_array = array(
            1 => lang('no'),
            2 => lang('yes'),
        );
        return $photo_permission_slips_array;
    }

}

if (!function_exists('file_or_image')) {

    function file_or_image($input_name = '', $upload_path_file = '', $max_size = '', $allowed_types = '') {
        $CI = & get_instance();

        if (!empty($_FILES[$input_name]['name'])) {
            if (!$upload_path_file) {
                $config['upload_path'] = 'assets/uploads/';
            } else {
                $config['upload_path'] = 'assets/uploads/' . $upload_path_file;
            }
            if (!$allowed_types) {
                $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|docx|doc|xls|xlsx|csv|txt|pptx';
            } else {
                $config['allowed_types'] = $allowed_types;
            }

//            if (!$max_size) {
//                $config['max_size'] = 100;
//            } else {
//                $config['max_size'] = $max_size;
//            }

            $config['file_name'] = $_FILES[$input_name]['name'];

//Load upload library and initialize configuration
            $CI->load->library('upload', $config);
            $CI->upload->initialize($config);

            if ($CI->upload->do_upload($input_name)) {
                $uploadData = $CI->upload->data();
                $file = $uploadData['file_name'];
                return $file;
            } else {
                return FALSE;
            }
        }
    }

}

