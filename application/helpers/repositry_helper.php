<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('erorr_quantity')) {

    function error_quantity($model , $data) { 
        $CI = & get_instance();
        $where = array(
            'repository_item_id' => $data['repository_item_id'],
            'input' => 1,
        );
        $CI->db->select('SUM(transaction_count) as transaction_count_input');
        $inputs = $CI->$model->get_by($where);

        $where = array(
            'repository_item_id' => $data['repository_item_id'],
            'input' => 0
        );
        $CI->db->select('SUM(transaction_count) as transaction_count_output');
        $outputs = $CI->$model->get_by($where);


        if (isset($inputs) && $inputs) {
            $count_input = $inputs->transaction_count_input;
            $count_output = 0;
            if (isset($outputs) && $outputs) {
                $count_output = $outputs->transaction_count_output;
            }
            if ($count_input < $count_output + $data['transaction_count']) {
                return True;
            } else {
                return FALSE;
            }
        } else {
            return True;
        }
    }

}
                