<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('date_coordinate')) {

    function date_coordinate($date_1, $date_2) {
        if ($date_1 <= $date_2) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
if (!function_exists('time_coordinate')) {

    function time_coordinate($time_1, $time_2) {
        if ($time_1 <= $time_2) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
 