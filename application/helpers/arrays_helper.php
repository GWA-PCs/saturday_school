<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function get_links_array() {
    $CI = &get_instance();
    if (!$CI->session->userdata('lang')) {
        $CI->session->set_userdata('lang', 'arabic');
        $lang = 'ar';
    } else {
        if ($CI->session->userdata('lang') == 'arabic') {
            $lang = 'ar';
        } else {
            $lang = 'en';
        }
    }
    return $lang;
}

function get_employee_links_array($directory, $controller, $type = '', $rooms_of_teachers = '') {

//:::::::::::::::::::::::::::dropdown manager:::::::::::::::::::::::::::
    $parent_manag = array(
        'index_parent' => 'parents/index_parent',
    );

    $teacher_manag = array(
        'index_teacher' => 'teacher/index_teacher',
        'index_assign_teacher_to_grade' => 'teacher/index_assign_teacher_to_grade',
    );

    $student_manag = array(
        'index_student' => 'student/index_student',
        'register_student_in_grade' => 'student_record/register_student_in_grade',
    );

    $grades_manag = array(
        'index_grade' => 'grade/index_grade',
    );

    $halls_manag = array(
        'index_hall' => 'hall/index_hall',
    );

    $honored_manag = array(
        'index_honored_orders_for_admin' => 'honored_orders/index_honored_orders_for_admin',
    );

    $meeting_manag = array(
        'index_meeting' => 'meeting/index_meeting',
    );

    $financing_manage = array(
        'index_tuition' => 'financing/index_tuition',
        'index_petty_cash' => 'financing/index_petty_cash',
        'index_pizza_and_snack_inputs' => 'financing/index_pizza_and_snack_inputs',
    );

    /////////////////////////////////manager////////////////////////////////
    $manager_main = array(
        // $key => $value
        'parent_manag' => 'mdi mdi-account-multiple-plus',
        'student_manag' => 'mdi mdi-human-child fa-2x',
        'teacher_manag' => 'mdi mdi-account-multiple-plus',
//        'index_stage' => 'mdi mdi-arrange-send-backward',
        'index_grade' => 'mdi mdi-home-modern',
        'index_honored_orders_for_admin' => 'mdi mdi-book-plus',
        'index_meeting' => 'mdi mdi-account-multiple',
        'financing_manage' => 'fa fa-money',
        'index_setting' => 'mdi mdi-settings',
        'index_report' => 'mdi mdi-chart-pie',
    );

    if ($type == 'teacher') {
        $teacher_room = array();

        $CI = &get_instance();
        $all_teacher_room = $CI->_get_rooms_of_teacher();
        if (isset($all_teacher_room) && $all_teacher_room) {
            foreach ($all_teacher_room as $r) {
                $teacher_room[$r->room_name] = 'home/teacher_room/' . $r->room_id;
            }
        }
        if (empty($all_teacher_room)) {
            $teacher_room['no classrooms found'] = '';
        }
        $manager_main = array(
            'teacher_room' => 'mdi mdi-arrange-bring-forward',
            'index_honored_orders' => 'mdi mdi-book-plus',
            'index_teacher_meetings' => 'mdi mdi-account-multiple',
        );

//        $manager_main = array(
//            'index_teacher_rooms' => 'mdi mdi-arrange-bring-forward',
//            'index_honored_orders' => 'mdi mdi-book-plus',
//            'index_teacher_meetings' => 'mdi mdi-account-multiple',
//        );

        $school_links = array();
        foreach ($manager_main as $key => $value) {
            if ($key == 'index_honored_orders') {
                $directory = 'honored_orders';
            } elseif ($key == 'index_teacher_rooms') {
                $directory = 'teacher';
            } elseif ($key == 'index_teacher_meetings') {
                $directory = 'teacher';
            }
            $school_links[$key . '_' . $controller]['directory'] = $directory;

            $school_links[$key . '_' . $controller]['page'] = $key;
            $school_links[$key . '_' . $controller]['icon'] = $value;

            $school_links[$key . '_' . $controller]['controller'] = $controller;
            $school_links[$key . '_' . $controller]['link'] = base_url() . "$directory/$key";

            if (isset(${"$key"}) && ($key == "teacher_room")) {
                $school_links[$key . '_' . $controller]['dropdown'] = ${"$key"};
            }
        }

        return $school_links;
    } elseif ($type == 'parent') {
        $directory = 'parents';
        $manager_main = array(
            'index_my_children' => 'mdi mdi-account-multiple',
            'index_payment_children' => 'fa fa-money',
        );

        $school_links = array();
        foreach ($manager_main as $key => $value) {
            $school_links[$key . '_' . $controller]['directory'] = $directory;
            $school_links[$key . '_' . $controller]['page'] = $key;
            $school_links[$key . '_' . $controller]['icon'] = $value;
            $school_links[$key . '_' . $controller]['controller'] = $controller;
            $school_links[$key . '_' . $controller]['link'] = base_url() . "$directory/$key";
        }

        return $school_links;
    } elseif ($type == "student") {
        $manager_main = array(
            'homeworks' => 'mdi mdi-book-multiple-variant',
            "page_profile" => 'mdi mdi-account'
        );

        $school_links = array();
        foreach ($manager_main as $key => $value) {
            if ($key == 'homeworks') {
                $directory = 'student';
            }
            if ($key == 'page_profile') {
                $directory = 'profile';
            }

            $school_links[$key . '_' . $controller]['directory'] = $directory;

            $school_links[$key . '_' . $controller]['page'] = $key;
            $school_links[$key . '_' . $controller]['icon'] = $value;

            $school_links[$key . '_' . $controller]['controller'] = $controller;
            $school_links[$key . '_' . $controller]['link'] = base_url() . "$directory/$key";
        }


        return $school_links;
    }

    $school_links = array();
    foreach ($manager_main as $key => $value) {
        $school_links[$key . '_' . $controller]['directory'] = $directory;
        $school_links[$key . '_' . $controller]['page'] = $key;
        $school_links[$key . '_' . $controller]['icon'] = $value;
        if ($key == "index_setting") {
            $school_links[$key . '_' . $controller]['controller'] = "setting";
            $school_links[$key . '_' . $controller]['link'] = base_url() . "setting/$key";
        } elseif ($key == "index_grade") {
            $school_links[$key . '_' . $controller]['controller'] = "grade";
            $school_links[$key . '_' . $controller]['link'] = base_url() . "grade/$key";
            /* } elseif ($key == "index_hall") {
              $school_links[$key . '_' . $controller]['controller'] = "hall";
              $school_links[$key . '_' . $controller]['link'] = base_url() . "hall/$key"; */
        } elseif ($key == "index_stage") {
            $school_links[$key . '_' . $controller]['controller'] = "stage";
            $school_links[$key . '_' . $controller]['link'] = base_url() . "stage/$key";
        } elseif ($key == "index_report") {
            $school_links[$key . '_' . $controller]['controller'] = "report";
            $school_links[$key . '_' . $controller]['link'] = base_url() . "report/$key";
        } elseif ($key == "index_meeting") {
            $school_links[$key . '_' . $controller]['controller'] = "meeting";
            $school_links[$key . '_' . $controller]['link'] = base_url() . "meeting/$key";
        } elseif ($key == "index_honored_orders_for_admin") {
            $school_links[$key . '_' . $controller]['controller'] = "honored_orders";
            $school_links[$key . '_' . $controller]['link'] = base_url() . "honored_orders/$key";
        } else {
            $school_links[$key . '_' . $controller]['controller'] = $controller;
            $school_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
        }
        // if have dropdown
        //        if (isset(${"$key"}) && $key != "index_setting" && $key != "index_financing" && $key != "index_report" && $key != "index_meeting") {
        //            $school_links[$key . '_' . $controller]['dropdown'] = ${"$key"};
        //        }

        if (isset(${"$key"}) && $key != "index_meeting") {
            $school_links[$key . '_' . $controller]['dropdown'] = ${"$key"};
        }
    }


    return $school_links;
}

function get_color_array() {
    return array(
        'inverse',
        'purple',
        'pink',
        'default',
        'danger',
        'warning',
        'info',
        'success',
        'primary',
        'megna',
    );
}

function get_color_array_for_wid() {
    return array(
        '#20AEE3',
        '#24D2B5',
        '#6772E5',
        '#FF5C6C',
        '#FF8C00',
        '#1D7BC2',
        '#20AEE3',
        '#24D2B5',
        '#6772E5',
        '#FF5C6C',
        '#FF8C00',
        '#1D7BC2',
    );
}

function home_page_links() {
    $home_links = array();

    // *************************** Take Attendence *****************************
    $directory = '';
    $controller = 'room_attendance';
    $link_icon_array = array(
        'take_attendance_from_home' => 'mdi mdi-pencil-box-outline',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }
    // ********************* Register student in grade *************************
    $directory = '';
    $controller = 'student_record';
    $link_icon_array = array(
        'register_student_in_grade' => 'mdi mdi-account-multiple-plus',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }
    /*     * *******************************stages      ******************************* */
    $directory = '';
    $controller = 'stage';
    $link_icon_array = array(
        'index_stage' => 'mdi mdi-arrange-send-backward',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // ******************************* Grade ***********************************
    $directory = '';
    $controller = 'grade';
    $link_icon_array = array(
        'index_grade' => 'mdi mdi-home-modern',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // *********************** Enrollment requests *****************************
    $directory = '';
    $controller = 'enrollment_requests';
    $link_icon_array = array(
        'index_enrollment_requests' => 'mdi mdi-library-books',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // *************************** Parent ********************************
    $directory = '';
    $controller = 'parents';
    $link_icon_array = array(
        'index_parent' => 'mdi mdi-account-multiple-plus',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // *************************** Student ********************************
    $directory = '';
    $controller = 'student';
    $link_icon_array = array(
        'index_student' => 'mdi mdi-account fa-2x',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // *************************** Teacher ********************************
    $directory = '';
    $controller = 'teacher';
    $link_icon_array = array(
        'index_teacher' => 'mdi mdi-account-multiple',
        'index_assign_teacher_to_grade' => 'mdi mdi-account-multiple-plus',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }
    // *************************** Honored orders ******************************
    $directory = '';
    $controller = 'honored_orders';
    $link_icon_array = array(
        'index_honored_orders_for_admin' => 'mdi mdi-book-plus',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // ******************************* Meetings ********************************
    $directory = '';
    $controller = 'meeting';
    $link_icon_array = array(
        'index_meeting' => 'mdi mdi-account-multiple',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    // ******************************* Financing *******************************
    $directory = '';
    $controller = 'financing';
    $link_icon_array = array(
        'index_financing' => 'mdi mdi-cash-usd',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }
    // ********************************* Setting *******************************
    $directory = '';
    $controller = 'setting';
    $link_icon_array = array(
        'index_setting' => 'mdi mdi-settings',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }
    // ******************************* Report **********************************
    $directory = '';
    $controller = 'report';
    $link_icon_array = array(
        'index_report' => 'mdi mdi-chart-pie',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    return $home_links;
}

function teacher_home_page_links($rooms_of_teachers = '') {
    $home_links = array();
    $directory = '';
    $controller = 'teacher';
    $link_icon_array = array(
        'index_teacher_rooms' => 'mdi mdi-arrange-bring-forward',
//        'index_send_students_to_honor' => 'mdi mdi-account-star-variant',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }
    $directory = '';
    $controller = 'honored_orders';
    $link_icon_array = array(
        'index_honored_orders' => 'mdi mdi-book-plus',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'teacher';
    $link_icon_array = array(
        'index_teacher_meetings' => 'mdi mdi-account-multiple',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }


    $directory = '';
    $controller = 'message';
    $link_icon_array = array(
        'index_send_msg' => 'mdi mdi-comment-check',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'message';
    $link_icon_array = array(
        'index_recieve_msg' => 'mdi mdi-email',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'profile';
    $link_icon_array = array(
        'page_profile' => 'mdi mdi-account-circle',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }


    return $home_links;
}

function parent_home_page_links() {
    // get user id 
//    $this->load->helper('load_controller');
//    load_controller('home', 'home_page_parent');


    $home_links = array();
    $directory = '';
    $controller = 'parents';
    $link_icon_array = array(
        'index_my_children' => 'mdi mdi-account-multiple',
//        'index_send_students_to_honor' => 'mdi mdi-account-star-variant',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'parents';
    $link_icon_array = array(
        'index_payment_children' => 'mdi mdi-square-inc-cash',
//        'index_send_students_to_honor' => 'mdi mdi-account-star-variant',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'message';
    $link_icon_array = array(
        'index_send_msg' => 'mdi mdi-comment-check',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'message';
    $link_icon_array = array(
        'index_recieve_msg' => 'mdi mdi-email',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    $directory = '';
    $controller = 'profile';
    $link_icon_array = array(
        'page_profile' => 'mdi mdi-account-circle',
    );
    foreach ($link_icon_array as $key => $value) {
        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }

    return $home_links;
}

function student_home_page_links() {
    $home_links = array();
    $directory = '';
    $controller = 'student';
    $link_icon_array = array(
        'homeworks' => 'mdi mdi-book-multiple-variant',
        "page_profile" => 'mdi mdi-account'
    );
    foreach ($link_icon_array as $key => $value) {
        if ($key == 'page_profile') {
            $controller = "profile";
        } elseif ($key == 'homeworks') {
            $controller = "student";
        }

        $home_links[$key . '_' . $controller]['directory'] = $directory;
        $home_links[$key . '_' . $controller]['controller'] = $controller;
        $home_links[$key . '_' . $controller]['page'] = $key;
        $home_links[$key . '_' . $controller]['icon'] = $value;
        $home_links[$key . '_' . $controller]['link'] = base_url() . "$directory$controller/$key";
    }


    return $home_links;
}

function get_week_days() {
    $days = array(
        'Sat' => lang('Sat'),
        'Sun' => lang('Sun'),
        'Mon' => lang('Mon'),
        'Tue' => lang('Tue'),
        'Wed' => lang('Wed'),
        'Thu' => lang('Thu'),
        'Fri' => lang('Fri'),
    );
    return $days;
}

function get_course_type() {
    $course_type = array(
        'in_building' => lang('in_building'),
        'online' => lang('online'),
    );
    return $course_type;
}

function get_course_domain() {
    $course_domain = array(
        'training' => lang('training'),
        'studying' => lang('studying'),
    );
    return $course_domain;
}

function general_payment_type() {
    $general_payment_type = array(
        'in' => lang('in'),
        'out' => lang('out'),
    );
    return $general_payment_type;
}

function get_absence_type() {
    $absence_type = array(
        'unjustified' => lang('unjustified'),
        'justified' => lang('justified'),
    );
    return $absence_type;
}

function get_hall_status() {
    $hall_status = array(
        'empty' => lang('empty'),
        'reserved' => lang('reserved'),
    );
    return $hall_status;
}

function get_general_social_development_lang() {
    $general_social_development_lang = array(
        'participates_in_class' => lang('participates_in_class'),
        'shows_cooperative_attitude_with_peers' => lang('shows_cooperative_attitude_with_peers'),
        'shows_cooperative_attitude_with_teachers' => lang('shows_cooperative_attitude_with_teachers'),
        'listens_when_others_are_speaking' => lang('listens_when_others_are_speaking'),
        'respects_equipment_and_material' => lang('respects_equipment_and_material'),
    );
    return $general_social_development_lang;
}

function get_general_social_development() {
    $general_social_development = array(
        'participates_in_class' => 'participates_in_class',
        'shows_cooperative_attitude_with_peers' => 'shows_cooperative_attitude_with_peers',
        'shows_cooperative_attitude_with_teachers' => 'shows_cooperative_attitude_with_teachers',
        'listens_when_others_are_speaking' => 'listens_when_others_are_speaking',
        'respects_equipment_and_material' => 'respects_equipment_and_material',
    );
    return $general_social_development;
}

function get_all_teacher_indexes() {
    $indexes = array(
        "edit_teacher_profile",
        'update_profile',
        "update_teacher_profile",
        'home_page_teacher',
        'logout',
        'change_password',
        'index_teacher_rooms',
        'show_teacher_rooms',
        'index_teacher_student_record',
        'show_teacher_student_record',
        'index_honored_students',
        'show_honored_students',
        'add_honored_students',
        'get_honored_students',
        'update_honored_students',
        'delete_honored_students',
        'index_honored_orders',
        'show_honored_orders',
        'add_honored_order',
        'get_honored_order',
        'update_honored_order',
        'delete_honored_order',
        'get_honored_students_for_filter',
        'index_teacher_subjects',
        'show_teacher_subjects',
        'index_student_note',
        'show_student_notes',
        'add_student_note',
        'get_student_note',
        'update_student_note',
        'delete_student_note',
        'index_student_mark',
        'show_student_marks',
        'add_student_mark',
        'index_teacher_meetings',
        'show_teacher_meetings',
        'page_student_profile',
        'profile_student_data',
        'get_student_attendances_info',
        'get_students_records_info',
        'get_notes',
        'index_room_attendance',
        'get_all_students',
        'update_attendace_values',
        'index_student_attendance',
        'page_teacher_profile',
        'page_profile',
        'profile_teacher_data',
        'tab_teacher_card',
        'index_send_msg',
        'show_send_msg',
        'add_send_msg',
        'get_send_msg',
        'update_send_msg',
        'delete_send_msg',
        'get_msg_to_users',
        'details',
        'get_sent_msg_details',
        'get_recieve_msg_details',
        'send_msg',
        'recieve_msg',
        'index_recieve_msg',
        'show_recieve_msg',
        'add_recieve_msg',
        'get_recieve_msg',
        'update_recieve_msg',
        'delete_recieve_msg',
        'get_class_parents_for_specific_class',
        'update_status_details',
        'count_received_msg_for_user_id',
        'get_student_study_sequences',
        'student_study_sequences',
        'page_parent_profile',
        'get_children',
        'profile_parent_data',
        'get_report_card',
        'index_report_card',
        'elem_report_card',
        'edit_elem_report_card',
        'get_elem_report_card',
        'kg_pre2_report_card',
        'edit_kg_pre2_report_card',
        'get_kg_pre2_report_card',
        'p1_report_card',
        'edit_p1_report_card',
        'get_p1_report_card',
        'no_report_card',
        'search_auto_complete',
        'search_about',
        'get_parent_balance',
        'total_student_payment_and_balance',
        'index_emergency_contact',
        'add_emergency_contact',
        'get_number_of_payments',
        'homeworks',
        "add_homework",
        'get_teacher_image',
        'get_parent_image',
        'get_personal_image',
        "show_homeworks",
        'index_general_social_development',
        'add_general_social_development',
        'teacher_room',
        'get_user',
        'get_allergies',
         'index_student_allergy',
        'show_student_allergies',
        'get_student_allergy',
        'add_student_allergy',
        'update_student_allergy',
        'delete_student_allergy',
        
    );
    return $indexes;
}

function get_all_parent_indexes() {
    $indexes = array(
        "edit_parent_profile",
        "update_parent_profile",
        'update_profile',
        'home_page_parent',
        'logout',
        'change_password',
        'index_student_note',
        'show_student_notes',
        'add_student_note',
        'get_student_note',
        'update_student_note',
        'delete_student_note',
        'index_student_mark',
        'show_student_marks',
        'add_student_mark',
        'page_student_profile',
        'profile_student_data',
        'get_student_attendances_info',
        'get_students_records_info',
        'get_notes',
        'index_room_attendance',
        'get_all_students',
        'update_attendace_values',
        'index_student_attendance',
        'show_parent_children',
        'page_parent_profile',
        'page_profile',
        'profile_parent_data',
        'tab_parent_card',
        'index_student_payment',
        'show_student_payments',
        'details_of_student_payment',
        'index_send_msg',
        'show_send_msg',
        'add_send_msg',
        'get_send_msg',
        'update_send_msg',
        'delete_send_msg',
        'get_msg_to_users',
        'details',
        'get_sent_msg_details',
        'get_recieve_msg_details',
        'send_msg',
        'recieve_msg',
        'index_recieve_msg',
        'show_recieve_msg',
        'add_recieve_msg',
        'get_recieve_msg',
        'update_recieve_msg',
        'delete_recieve_msg',
        'get_class_parents_for_specific_class',
        'update_status_details',
        'count_received_msg_for_user_id',
        'get_student_study_sequences',
        'student_study_sequences',
        'get_children',
        'get_report_card',
        'index_report_card',
        'elem_report_card',
        'edit_elem_report_card',
        'get_elem_report_card',
        'kg_pre2_report_card',
        'edit_kg_pre2_report_card',
        'get_kg_pre2_report_card',
        'p1_report_card',
        'edit_p1_report_card',
        'get_p1_report_card',
        'no_report_card',
        'get_parent_balance',
        'total_student_payment_and_balance',
        'index_emergency_contact',
        'add_emergency_contact',
        'get_number_of_payments',
        'get_teacher_image',
        'get_parent_image',
        'get_personal_image',
        'index_my_children',
        'index_payment_children',
        'get_user',
        'get_allergies',
         'index_student_allergy',
        'show_student_allergies',
        'get_student_allergy',
        'add_student_allergy',
        'update_student_allergy',
        'delete_student_allergy',
    );
    return $indexes;
}

function get_all_student_indexes() {
    $indexes = [
        "home_page_student",
        "logout",
        "change_password",
        "page_profile",
        "homeworks",
        "show_homeworks",
        "page_student_profile",
        "get_students_records_info",
        "get_report_card",
        "get_student_study_sequences",
        "get_allergies",
        "get_student_attendances_info",
        "total_student_payment_and_balance",
        "profile_student_data",
        "get_notes",
        "get_personal_image",
        "count_received_msg_for_user_id",
        "get_user",
        'show_recieve_msg',
        'index_send_msg',
        'index_recieve_msg',
        'details',
        'get_recieve_msg_details',
        'update_status_details',
        //=====================
        'index_student_note',
        'show_student_notes',
        'add_student_note',
        'get_student_note',
        'update_student_note',
        'delete_student_note',
        'index_student_mark',
        'show_student_marks',
        'add_student_mark',
        'page_student_profile',
        'profile_student_data',
        'get_student_attendances_info',
        'get_students_records_info',
        'get_notes',
        'index_room_attendance',
        'get_all_students',
        'update_attendace_values',
        'index_student_attendance',
        'show_parent_children',
        'page_parent_profile',
        'page_profile',
        'profile_parent_data',
        'tab_parent_card',
        'index_student_payment',
        'show_student_payments',
        'details_of_student_payment',
        'index_send_msg',
        'show_send_msg',
        'add_send_msg',
        'get_send_msg',
        'update_send_msg',
        'delete_send_msg',
        'get_msg_to_users',
        'details',
        'get_sent_msg_details',
        'get_recieve_msg_details',
        'send_msg',
        'recieve_msg',
        'index_recieve_msg',
        'show_recieve_msg',
        'add_recieve_msg',
        'get_recieve_msg',
        'update_recieve_msg',
        'delete_recieve_msg',
        'get_class_parents_for_specific_class',
        'update_status_details',
        'count_received_msg_for_user_id',
        'get_student_study_sequences',
        'student_study_sequences',
        'get_children',
        'get_report_card',
        'index_report_card',
        'elem_report_card',
        'edit_elem_report_card',
        'get_elem_report_card',
        'kg_pre2_report_card',
        'edit_kg_pre2_report_card',
        'get_kg_pre2_report_card',
        'p1_report_card',
        'edit_p1_report_card',
        'get_p1_report_card',
        'no_report_card',
        'get_parent_balance',
        'total_student_payment_and_balance',
        'index_emergency_contact',
        'add_emergency_contact',
        'get_number_of_payments',
        'get_teacher_image',
        'get_parent_image',
        'get_personal_image',
        'index_my_children',
        'index_payment_children',
    ];

    return $indexes;
}

function get_stages() {
    $stages = array(
        'preschool_1' => lang('preschool_1'),
        'preschool_2' => lang('preschool_2'),
        'kindergarten' => lang('kindergarten'),
        'beginner_class' => lang('beginner_class'),
        'first_grade' => lang('first_grade'),
        'second_grade' => lang('second_grade'),
        'third_grade' => lang('third_grade'),
        'fourth_grade' => lang('fourth_grade'),
        'fifth_grade' => lang('fifth_grade'),
        'sixth_grade' => lang('sixth_grade'),
    );
    return $stages;
}

function get_monthes() {
    $monthes = array(
        'January' => lang('January'),
        'February' => lang('February'),
        'March' => lang('March'),
        'April' => lang('April'),
        'May' => lang('May'),
        'September' => lang('September'),
        'October' => lang('October'),
        'November' => lang('November'),
        'December' => lang('December'),
    );
    return $monthes;
}

function get_all_monthes() {
    $monthes = array(
        'January' => lang('January'),
        'February' => lang('February'),
        'March' => lang('March'),
        'April' => lang('April'),
        'May' => lang('May'),
        'June' => lang('June'),
        'July' => lang('July'),
        'August' => lang('August'),
        'September' => lang('September'),
        'October' => lang('October'),
        'November' => lang('November'),
        'December' => lang('December'),
    );
    return $monthes;
}

function get_marks_key() {
    $marks_key = array(
        'O' => lang('outstanding'),
        'S' => lang('satisfactory'),
        'N' => lang('needs_improvement'),
    );

    return $marks_key;
}

function get_petty_cash_type_array() {
    $petty_cash_type = array(
        'csh' => lang('csh'),
        'dep' => lang('dep'),
    );

    return $petty_cash_type;
}

function get_months_of_first_semester() {
    $months_of_first_semester = array('8', '9', '10', '11', '12');
    return $months_of_first_semester;
}

function get_months_of_second_semester() {
    $months_of_second_semester = array('1', '2', '3', '4', '6');
    return $months_of_second_semester;
}

function get_tuition_array() {
    $tuition = array(
        'saturday_school_registration_fee' => 'saturday_school_registration_fee',
        'saturday_school_pre_kinder_books_fee' => 'saturday_school_pre_kinder_books_fee',
        'saturday_school_elementary_books_fee' => 'saturday_school_elementary_books_fee',
        'saturday_school_islamic_textbook' => 'saturday_school_islamic_textbook',
        'saturday_school_islamic_workbook' => 'saturday_school_islamic_workbook',
        'saturday_school_tuition_full_year_first_child' => 'saturday_school_tuition_full_year_first_child',
        'saturday_school_tuition_full_year_second_child' => 'saturday_school_tuition_full_year_second_child',
        'saturday_school_tuition_full_year_third_child' => 'saturday_school_tuition_full_year_third_child',
        'saturday_school_tuition_full_year_discount' => 'saturday_school_tuition_full_year_discount',
        'saturday_school_tuition_bimonthly_first_child' => 'saturday_school_tuition_bimonthly_first_child',
        'saturday_school_tuition_bimonthly_second_child' => 'saturday_school_tuition_bimonthly_second_child',
        'saturday_school_tuition_bimonthly_third_child' => 'saturday_school_tuition_bimonthly_third_child',
        'saturday_school_tuition_monthly_first_child' => 'saturday_school_tuition_monthly_first_child',
        'saturday_school_tuition_monthly_second_child' => 'saturday_school_tuition_monthly_second_child',
        'saturday_school_tuition_monthly_third_child' => 'saturday_school_tuition_monthly_third_child',
        'saturday_school_late_payment_fee' => 'saturday_school_late_payment_fee',
        'saturday_school_late_pickup_fee' => 'saturday_school_late_pickup_fee',
        'saturday_school_teacher_child_discount' => 'saturday_school_teacher_child_discount',
        'arabic_book_club_per_month' => 'arabic_book_club_per_month',
        'youth_group_registration_fee' => 'youth_group_registration_fee',
        'youth_group_tuition_full_year_first_child' => 'youth_group_tuition_full_year_first_child',
        'youth_group_tuition_full_year_second_child' => 'youth_group_tuition_full_year_second_child',
        'youth_group_tuition_full_year_third_child' => 'youth_group_tuition_full_year_third_child',
        'youth_group_tuition_full_year_discount' => 'youth_group_tuition_full_year_discount',
        'youth_group_tuition_bimonthly_first_child' => 'youth_group_tuition_bimonthly_first_child',
        'youth_group_tuition_bimonthly_second_child' => 'youth_group_tuition_bimonthly_second_child',
        'youth_group_tuition_bimonthly_third_child' => 'youth_group_tuition_bimonthly_third_child',
        'youth_group_tuition_monthly_first_child' => 'youth_group_tuition_monthly_first_child',
        'youth_group_tuition_monthly_second_child' => 'youth_group_tuition_monthly_second_child',
        'youth_group_tuition_monthly_third_child' => 'youth_group_tuition_monthly_third_child',
        'youth_group_late_payment_fee' => 'youth_group_late_payment_fee',
        'youth_group_late_pickup_fee' => 'youth_group_late_pickup_fee',
        'youth_group_teacher_child_discount' => 'youth_group_teacher_child_discount',
    );
    return $tuition;
}

function get_payment_types() {
    $payment_types = array(
        'registration_fee' => lang('registration_fee'),
        'books' => lang('books'),
        'tuition' => lang('tuition'),
        'late_fee' => lang('late_fee'),
        'custom_payment' => lang('custom_payment'),
    );
    return $payment_types;
}

function get_report_card_name() {
    $report_card_name = array(
        'no_report_card' => lang('no_report_card'),
        'p1_report_card' => lang('p1_report_card'),
        'kg_pre2_report_card' => lang('kg_pre2_report_card'),
        'elem_report_card' => lang('elem_report_card'),
    );
    return $report_card_name;
}

if (!function_exists('load_controller')) {

    function load_controller($controller, $method = 'index') {
        require_once(FCPATH . APPPATH . 'controllers/' . $controller . '.php');

        $controller = new $controller();

        return $controller->$method();
    }

}

function get_attendances_type() {
    $attendances_types = array(
        'attendance' => lang('attendance'),
        'absence' => lang('absence'),
        'delay' => lang('delay'),
    );
    return $attendances_types;
}
