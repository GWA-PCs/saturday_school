<?php  if ( ! defined("BASEPATH")) exit("No direct script access allowed");

$lang["arabic"]                                                                 = "arabic";

$lang["login"]                                                                  = "login";

$lang["update"]                                                                 = "update";

$lang["save"]                                                                   = "save";

$lang["close"]                                                                  = "close";

$lang["add_new_student"]                                                        = "add new student";

$lang["first_name"]                                                             = "first name";

$lang["last_name"]                                                              = "last name";

$lang["father_name"]                                                            = "father\"s name";

$lang["mother_name"]                                                            = "mother\"s name";

$lang["mobile"]                                                                 = "father\"s number";

$lang["phone"]                                                                  = "mother\"s number";

$lang["birthdate"]                                                              = "birthday";

$lang["email"]                                                                  = "email";

$lang["username"]                                                               = "username";

$lang["home_employee"]                                                          = "home page";

$lang["student_manag"]                                                          = "students";

$lang["delete"]                                                                 = "delete";

$lang["delete_confirmation_text"]                                               = "are you sure you want to delete this";

$lang["note"]                                                                   = "notice";

$lang["already_exist"]                                                          = "already exists";

$lang["error_in_information"]                                                   = "error in information input";

$lang["error_existing"]                                                                  = "error already exists";

$lang["error"]                                                                  = "error";

$lang["date_error"]                                                                  = "date error";

$lang["Time_error"]                                                                  = "time error";

$lang["warning"]                                                                = "warning";

$lang["success"]                                                                = "success";

$lang["info"]                                                                   = "notice";

$lang["have_son_error"]                                                         = "could not delete because has related components";

$lang["delete_success"]								= "deleted successfully";

$lang["home"]								        = "home";

$lang["home_page"]								= "home page";

$lang["students"]								= "students";

$lang["manage_students"]							= "manage students";

$lang["create"]								        = "create new";

$lang["remember_me"]								= "remember me";

$lang["logout"]								        = "logout";

$lang["update_success"]								= "updated successfully";

$lang["messages_error"]								= "error";

$lang["messages_success"]							= "success";

$lang["home"]								        = "home";

$lang["home_page"]                                                              = "home page";

$lang["students"]							        = "students";

$lang["teachers"]								    = "teacher";

$lang["address"]								    = "home address";

$lang["create"]								        = "create";

$lang["insert_success"]							        = "inserted successfully";

$lang["update_success"]							        = "updated successfully";

$lang["update_error"]							        = "update error";

$lang["delete_error"]							        = "delete error";

$lang["educational_attainment"]					                = "التحصيل العلمي";

$lang["add_new_previlage"]							= "إضافة صلاحية جديدة";

$lang["Previlage"]							        = "إضافة صلاحية جديدة";

$lang["employees"]							        = "employees";

$lang["add_new_teacher"]							                            = "add new teacher";

$lang["add_teacher"]							                                = "add new teacher";

$lang["edit_teacher"]							                                = "edit teacher info";

$lang["delete_teacher"]							                                = "delete teacher";

$lang["salary"]							                                        = "salary";

$lang["teacher_manag"]							                                = "teachers";

$lang["index_teacher"]							                                = "manage teachers";

$lang["index_teacher_absences"]							                        = "teacher absences";

$lang["add_student"]                                                            = "add new student";

$lang["special_course_name"]                                                    = "course name";

$lang["special_course_date"]                                                    = "course date";

$lang["index_student"]                                                          = "manage students";

$lang["index_employee"]                                                         = "manage teachers";

$lang["absences"]                                                               = "absences";

$lang["add_teacher_absence"]                                                    = "add absence";

$lang["delete_teacher_absence"]                                                 = "delete absence";

$lang["update_teacher_absence"]                                                 = "edit absence";

$lang["teacher_absence_date"]                                                   = "date of absence";

$lang["reason_of_absence"]                                                      = "reason of absence";

$lang["username_already_exist"]                                                 = "username already exists";

$lang["email_already_exist"]                                                    = "email already exists";

$lang["add_employee"]                                                           = "add new employee";

$lang["index_room"]                                                             = "halls";

$lang["institute_address"]                                                      = "address";

$lang["institute_phone"]                                                        = "الهاتف";

$lang["institute_mobile"]                                                       = "الموبايل";

$lang["field_name"]                                                             = "اسم الحقل";

$lang["field_value"]                                                            = "value";

$lang["update_student"]                                                         = "edit student info";

$lang["update_employee"]                                                        = "edit employee info";

$lang["room_id"]                                                        = "اسم الشعبة ";

$lang["room_name"]                                                        = "اسم الشعبة ";

$lang["add_new_room"]                                                        = "إضافة شعبة جديدة ";

$lang["Sat"]                                                                    = "saturday";

$lang["Sun"]                                                                    = "sunday";

$lang["Mon"]                                                                    = "monday";

$lang["Tue"]                                                                    = "tuesdat";

$lang["Wed"]                                                                    = "wednesday";

$lang["Thu"]                                                                    = "thursday";

$lang["Fri"]                                                                    = "friday";

$lang["edit_room"]                                                       = "تعديل معلومات الشعبة ";

$lang["delete_room"]                                                       = "حذف  معلومات الشعبة ";

$lang["student_id"]                                                             = "student id";

$lang["login_error"]                                                              = "error in username or password";

$lang["login_unsuccessful"]                                                     = "login information incorrect";

$lang["yes"]                                                                    = "yes";

$lang["no"]                                                                     = "no"; 

$lang["teacher_absences"]                                                       = "teacher absences"; 

$lang["student_absences"]                                                       = "student absences"; 

$lang["triple_name"]                                                            = "id";

$lang["student_not_exist"]                                                      = "student does not exist";

$lang["index_student_absence"]                                                  = "student absences"; 

$lang["add_new_student_absence"]                                                 = "add absence";

$lang["update_student_absence"]                                                   = "edit absence";

$lang["delete_student_absence"]                                                  = "delete absence";

$lang["index_teacher_absence"]                                                  = "teacher absence"; 

$lang["add_new_teacher_absence"]                                                 = "add absence";

$lang["update_teacher_absence"]                                                   = "edit absence";

$lang["delete_teacher_absence"]                                                  = "delete absence";

$lang["absence_date"]                                                            = "absence date";

$lang["index_student_payment"]                                                  = "student payments"; 

$lang["add_new_student_payment"]                                                = "add payment";

$lang["update_student_payment"]                                                 = "edit payment info";

$lang["delete_student_payment"]                                                 = "delete payment";

$lang["student_payment_name"]                                                   = "payment name";

$lang["student_payment_description"]                                            = "payment description";

$lang["student_payment_date"]                                                   = "payment date";

$lang["student_payment_value"]                                                  = "payment value";

$lang["page_student_profile"]                                                   =" student profiles ";

$lang["user_manage"]                                                            = "manage users";

$lang["insert_error"]							        = "insertion error";

$lang["index_parent"]                                                           = "manage parents";

$lang["add_new_parent"]                                                         = "add parent";

$lang["update_parent"]                                                           = "edit parent info";

$lang["delete_parent"]                                                           = "delete parent info";

$lang["index_grade"]                                                            = "manage classes";

$lang["grades_manag"]                                                           = "manage classes";

$lang["add_new_grade"]                                                              = "add class";

$lang["update_grade"]                                                             = "edit class info";

$lang["delete_grade"]                                                           = "delete class";

$lang["grade_name"]                                                              = "class name";

$lang["grade_name_already_exist"]                                               = "class name already exists";

$lang["age"]                                                                    = "age";

$lang["grade_id"]                                                               = "class";

$lang["index_student_parent"]                                                   = "parents";

$lang["add_new_student_parent"]                                                 = "add parent to this student";

$lang["parent_id"]                                                              = "parent id";

$lang["parent_not_exist"]                                                       = "parent does not exist";

$lang["student_parent_already_exist"]                                           = "parent already exists";

$lang["absence_type"]                                                            = "absence type";

$lang["unjustified"]                                                            ="unjustified";

$lang["justified"]                                                              ="justified";

$lang["total_payment"]                                                          ="total payment";

$lang["remain_cost"]                                                          ="remaining costs";

$lang["index_room"]                                                             ="الشعب";

$lang["hall_status"]                                                             ="hall status";

$lang["empty"]                                                                  ="empty";

$lang["reserved"]                                                               ="reserved";

$lang["add_new_hall"]                                                           ="add new hall";

$lang["hall_name"]                                                              ="hall name";

$lang["halls_manag"]                                                            ="halls";

$lang["index_hall"]                                                            ="manage halls";

$lang["update_hall"]                                                            ="edit hall info";

$lang["delete_hall"]                                                            ="delete hall";

$lang["teacher_room"]                                                           ="شعب المعلم";

$lang["index_student_record"]                                                   ="طلاب الشعبة";

$lang["family_name"]                                                            ="last name";

$lang["teacher"]                                                                ="teacher";

$lang["assistant_teacher"]                                                      ="teacher\"s assistant";

$lang["related_with_another_elem"]                                              ="could not delete because has related components";

$lang["father_phone"]                                                            ="father\"s number";

$lang["mother_phone"]                                                            ="mother\"s number";

$lang["home_phone"]                                                             ="home phone number";

$lang["student_name"]                                                           ="student name";

$lang["balance"]                                                                ="account balance";

$lang["student_notes"]                                                          ="notes";

$lang["grade"]                                                          ="grade";

$lang["parent_id"]                                                          ="last name";

$lang["select_family_name"]                                                     ="select last name";

$lang["add_new_student_record"]                                                     ="إضافة طالب للشعبة ";

$lang["update_student_record"]                                                     ="تعديل معلومات الطالب";

$lang["delete_student_record"]                                                     ="حذف الطالب";

$lang["index_room_teachers"]                                                       ="معلمي الشعبة";

$lang["teacher_name"]                                                           ="teacher name";

$lang["is_active"]                                                                 ="التسجيل مفعل";

$lang["student_already_exist_in_room"]                                          ="هذا الطالب موجود في الشعبة";

$lang["index_subject"]                                                          =" subjects";

$lang["add_new_subject"]                                                              = "add subject";

$lang["update_subject"]                                                             = "edit subject info";

$lang["delete_subject"]                                                           = "delete subject";

$lang["subject_name"]                                                              = "subject name";

$lang["subject_name_already_exist"]                                               = "subject already existts";

$lang["teacher_id"]                                                             = "teacher id";

$lang["participates_in_class"]                                                  = "participates in class";

$lang["shows_cooperative_attitude_with_peers"]                                  = "shows cooperative attitude with peers";

$lang["shows_cooperative_attitude_with_teachers"]                               = "shows cooperative attitude with teachers";

$lang["listens_when_others_are_speaking"]                                       = "listens when others are speaking";

$lang["respects_equipment_and_material"]                                        = "respects equipment and material";

$lang["index_general_social_development"]                                       = "general social development";

$lang["index_room_teacher"]                                                     = "معلمي الشعبة";

$lang["subjects_manag"]                                                         = "المواد";

$lang["parent_subject_id"]                                                    = "اسم المادة الأب";

$lang["arabic"]                                                                 = "arabic";

$lang["no_parent_subject"]                                                      = "لا يوجد مادة أب";

$lang["min_mark"]                                                               = "العلامة الدنيا";

$lang["max_mark"]                                                               = "العلامة العظمى";

$lang["edit_subject"]                                                           = "edit subject info";

$lang["delete_subject"]                                                         = "delete subject";

$lang["min_mark_should_be_less_than_max_mark"]                                  = "العلامة الدنيا يجب أن تكون أقل من العلامة العظمى";

$lang["index_teacher_rooms"]                                                    = "الشعب";

$lang["next_grade_name"]                                                        = "اسم الصف اللاحق";

$lang["enter_from_top_class_to_lower"]                                          = "يرجى إدخال الصفوف من الصف الأعلى إلى الصف الأدنى";

$lang["no_grade"]                                                               = "لا يوجد";

$lang["subject_id"]                                                             = "اسم المادة";

$lang["edit_room_teacher"]                                                      = "تعديل معلومات المعلم";

$lang["home_page_teacher"]                                                      = "الصفحة الرئيسية";

$lang["index_teacher_student_record"]                                           = "طلاب الشعبة";

$lang["index_mark"]                                                             = "العلامات";

$lang["index_honored_students"]                                                 = "add students to ceremony";

$lang["add_new_honored_students"]                                               = "add student to ceremony";

$lang["honored_date"]                                                           = "ceremony date";

$lang["honored_reason"]                                                         = "ceremony reason";

$lang["select_student_name"]                                                    = "select student ";

$lang["select_grade_name"]                                                      = "select class";

$lang["student_record_id"]                                                      = "student name";

$lang["select_room_name"]                                                       = "اختر اسم الشعبة";

$lang["preschool_1"]                                                            =  "preschool 1";

$lang["preschool_2"]                                                            =  "preschool 2";

$lang["kindergarten"]                                                           =  "kindergarten";

$lang["beginner_class"]                                                         =  "beginner\"s class";

$lang["first_grade"]                                                            =  "first grade";

$lang["second_grade"]                                                           =  "second grade";

$lang["third_grade"]                                                            =  "third grade";

$lang["fourth_grade"]                                                           =  "fourth grade";

$lang["fifth_grade"]                                                            =  "fifth grade";

$lang["sixth_grade"]                                                            =  "sixth grade";

$lang["stage_name"]                                                             =  "المرحلة";

$lang["select_stage_name"]                                                      =  "اختر اسم المرحلة";

$lang["add_new_honored_order"]                                                  =  "add student";

$lang["order_title"]                                                            =  "عنوان الطلب";

$lang["index_honored_orders"]                                                   =  "طلبات التكريم";

$lang["update_honored_students"]                                                =  "edit student info";

$lang["honored_status"]                                                         =  "student status";

$lang["update_honored_students"]                                                =  "تعديل معلومات الطلب ";

$lang["show_honored_orders_for_admin"]                                          =  "إدارة طلبات التكريم";

$lang["honored_order_created_date"]                                             =  "تاريخ الطلب";

$lang["pending"]                                                                =  "waitlisted";

$lang["reservation_done"]                                                       =  "hall reserved";

$lang["honored_done"]                                                           =  "تم التكريم";

$lang["update_honored_order"]                                                   =  "تعديل معلومات الطلب";

$lang["order_status"]                                                           =  "حالة الطلب";

$lang["index_honored_orders_for_admin"]                                          =  "طلبات التكريم";

$lang["honored_students"]                                                       =  "الطلاب"; 

$lang["honored_done"]                                                           =  "تم التكريم";

$lang["honored_manag"]                                                          =  "إدارة طلبات التكريم";

$lang["index_teacher_subjects"]                                                 =  "المواد";

$lang["password"]                                                               =  "password";

$lang["stages_and_grades_manage"]                                               =  "إدارة المراحل والصفوف";

$lang["index_stage"]                                                            =  "إدارة المراحل";

$lang["add_new_stage"]                                                          =  "إضافة مرحلة جديدة";

$lang["update_stage"]                                                           =  "تعديل معلومات المرحلة";

$lang["stage_id"]                                                               =  "اسم المرحلة";

$lang["photo_permission_slips"]                                                 =  "السماح بأخذ صورة";

$lang["islamic_book_return"]                                                    =  "تم إرجاع الكتب الإسلامية";

$lang["status_islamic_book_return"]                                             =  "حالة استلام الكتب الإسلامية";

$lang["excellent"]                                                              =  "ممتاز";

$lang["very_good"]                                                              =  "جيد جداً";

$lang["good"]                                                                   =  "جيد";

$lang["bad"]                                                                    =  "سيء";

$lang["select_book_status"]                                                     =  "اختر حالة الكتاب";

$lang["no_status"]                                                              =  "لا يوجد";

$lang["student_already_exist_in_another_room"]                                  =  "الطالب موجود في شعبة أخرى";

$lang["index_setting"]                                                          =  "الإعدادات";

$lang["determine_current_year"]                                                 =  "تحديد العام الدراسي";

$lang["year"]                                                                   =  "year";

$lang["year_error"]                                                             =  "year error";

$lang["stage_id"]                                                               =  "المرحلة";

$lang["index_honored_students_for_admin"]                                       =  "طلاب التكريم";

$lang["index_transferring_students"]                                             =  "transfer students";

$lang["january"]                                                                =  "january";

$lang["february"]                                                               =  "february";

$lang["march"]                                                                  =  "march";

$lang["april"]                                                                  =  "april";

$lang["may"]                                                                    =  "may";

$lang["september"]                                                              =  "september";

$lang["october"]                                                                =  "october";

$lang["november"]                                                               =  "november";

$lang["december"]                                                               =  "december";

$lang["index_student_note"]                                                     =  "notes";

$lang["no_stage"]                                                               =  "لا يوجد";

$lang["add_new_student_note"]                                                   =  "add note";

$lang["note_month"]                                                             =  "month";

$lang["note_date"]                                                              =  "date";

$lang["note_text"]                                                              =  "note";

$lang["update_student_note"]                                                    =  "edit note";

$lang["archive_year"]                                                           =  "archived years";

$lang["index_student_allergy"]                                                  =  "student allergy";

$lang["allergy_name"]                                                           =  "allergy info";

$lang["add_new_student_allergy"]                                                =  "add allergy";

$lang["update_student_allergy"]                                                 =  "edit allergy info";

$lang["meeting_title"]                                                          =  "meeting title";

$lang["meeting_date"]                                                           =  "meeting date";

$lang["meeting_file"]                                                           =  "meetings";

$lang["add_new_meeting"]                                                        =  "add meeting";

$lang["update_meeting"]                                                         =  "edit meeting";

$lang["index_meeting"]                                                          =  "meetings";

$lang["change"]                                                                 =  "change";

$lang["remove"]                                                                 =  "delete";

$lang["choose_file"]                                                            =  "upload file";

$lang["en_name"]                                                                =  "file name must be in english";

$lang["student_mark_s1"]                                                        =  "علامات الطلاب - ف1";

$lang["student_mark_s2"]                                                        =  "علامات الطلاب - ف2";

$lang["mark_key"]                                                               =  "تقييم الطالب";

$lang["mark_note"]                                                              =  "ملاحظات";

$lang["outstanding"]                                                            =  "outstanding";

$lang["satisfactory"]                                                           =  "satisfactory";

$lang["needs_improvement"]                                                      =  "needs improvement";

$lang["index_student_mark"]                                                     =  "علامات الطلاب";

$lang["semester"]                                                               =  "semester";

$lang["first"]                                                                  =  "first";

$lang["second"]                                                                 =  "second";

$lang["move_to_room"]                                                           =  "نقل إلى الشعبة";

$lang["move_success"]                                                           =  "تم نقل الطلاب بنجاح";

$lang["move_error"]                                                             =  "حدث خطأ في نقل الطلاب";

$lang["index_room_attendance"]                                                  =  "حضور الطلاب";

$lang["attendance"]                                                             =  "attendance";

$lang["absence"]                                                                =  "absent";

$lang["delay"]                                                                  =  "tardy";

$lang["first_semester"]                                                         =  "first semester";

$lang["second_semester"]                                                        =  "second semester";

$lang["attendance_date"]                                                        =  "date";

$lang["no_students"]                                                            =  "no students in class";

$lang["error_future_date"]                                                      =  "error future date";

$lang["index_teacher_meetings"]                                                 =  "meetings";

$lang["meeting_manage"]                                                         =  "manage meetings";

$lang["financing_manage"]                                                       =  "manage finances";

$lang["index_tuition"]                                                          =  "tuition";

$lang["index_petty_cash"]                                                       =  "petty cash";

$lang["index_pizza_and_snack_inputs"]                                           =  "food sales";

$lang["index_financing"]                                                        =  "finances";

$lang["input_date"]                                                             =  "date";

$lang["input_title"]                                                            =  "title";

$lang["cost"]                                                                   =  "cost";

$lang["withdrawal"]                                                             =  "withdrawl";

$lang["add_new_input"]                                                          =  "add new item";

$lang["edit_input"]                                                             =  "edit input";

$lang["index_pizza_and_snack_items"]                                            =  "تفاصيل الإدخال";

$lang["bills"]                                                                  =  "bills";

$lang["quantity"]                                                               =  "quantity";

$lang["add_new_item"]                                                           =  "add new item";

$lang["edit_item"]                                                              =  "edit item";

$lang["total"]                                                                  =  "total";

$lang["total_sum"]                                                              =  " total summ";

$lang["cost"]                                                                   =  "cost";

$lang["profit"]                                                                 =  "profit";

$lang["deposit"]                                                                =  "deposit";

$lang["parent"]                                                                 =  "parents";

$lang["attendances"]                                                            =  "attendance";

$lang["attend"]                                                                 =  "حضور";

$lang["date"]                                                                   =  "date";

$lang["info"]                                                                   =  "general information";

$lang["current_classes"]                                                        =  "current classes";

$lang["status"]                                                                 =  "status";

$lang["edit_classes"]                                                           =  "edit classes";

$lang["not_active"]                                                             =  "inactive";

$lang["add_note"]                                                               =  "add note";

$lang["read_more"]                                                              =  "read more";

$lang["no_notes"]                                                               =  "no notes";

$lang["no_records"]                                                             =  "no record";

$lang["index_student_attendance"]                                               =  " student attendance";

$lang["add_new_petty_cash"]                                                     =  "add new item";

$lang["update_new_petty_cash"]                                                  =  "edit item";

$lang["petty_cash_date"]                                                        =  "date";

$lang["petty_cash_type"]                                                        =  "item type";

$lang["petty_cash_account"]                                                     =  "account";

$lang["petty_cash_payment"]                                                     =  "payment";

$lang["petty_cash_deposit"]                                                     =  "deposit";

$lang["petty_cash_balance"]                                                     =  "balance";

$lang["petty_cash_memo"]                                                        =  "notes";

$lang["csh"]                                                                    =  "csh";

$lang["dep"]                                                                    =  "dep";

$lang["no_matching_records_found"]                                              =  "no matching records found";

$lang["can_not_register_student_in_more_than_two_classes"]                      =  "cannot register student in more than two classes";

$lang["index_report"]                                                           =  "reports";

$lang["student_non_active_manually"]                                            =  "inactive students";

$lang["search"]                                                                 =  "search";

$lang["select_grade"]                                                           =  "select grade";

$lang["students_who_did_not_give_permission"]                                   =  "students who did not give permission for a photo ";

$lang["students_who_did_not_return_the_books"]                                  =  "students who did not return books ";

$lang["number_of_students"]                                                     =  "number of students";

$lang["students_number_s1"]                                                     =  "number of students first semester";

$lang["students_number_s2"]                                                     =  "number of students second semester";

$lang["students_number_end_year"]                                               =  "number of students at the end of the year";

$lang["all_grades"]                                                             =  "all grades";

$lang["change_password"]                                                        =  "change password";

$lang["saturday_school_registration_fee" ]                                      = "registration fee";

$lang["saturday_school_books_fee" ]                                             = "books fee";

$lang["saturday_school_tuition_full_year_first_child" ]                                 = "full year tuition first child";

$lang["saturday_school_tuition_full_year_second_child" ]                                = "full year tuition second child";

$lang["saturday_school_tuition_full_year_third_child" ]                                 = "full year tuition third child";

$lang["saturday_school_full_year_discount" ]                                    = "full year discount";

$lang["saturday_school_tuition_bimonthly_first_child" ]                                 = "bimonthly tuition first child";

$lang["saturday_school_tuition_bimonthly_second_child" ]                                = "bimonthly tuition second child";

$lang["saturday_school_tuition_bimonthly_third_child" ]                                 = "bimonthly tuition third child";

$lang["saturday_school_tuition_monthly_first_child" ]                                   =  "monthly tuition first child";

$lang["saturday_school_tuition_monthly_second_child" ]                                  =  "monthly tuition second child";

$lang["saturday_school_tuition_monthly_third_child" ]                                   ="monthly tuition third child";

$lang["saturday_school_late_payment_fee" ]                                      = "late payment fee";

$lang["saturday_school_late_pickup_fee" ]                                       = "late pickup fee";

$lang["saturday_school_teacher_child_discount" ]                                = "teacher discount";

$lang["arabic_book_club_per_month" ]                                            = "arabic book club tuition";

$lang["youth_group_registration_fee" ]                                          = "registration fee";

$lang["youth_group_tuition_full_year_first_child" ]                                     = "full year tuition first child";

$lang["youth_group_tuition_full_year_second_child" ]                                    = "full year tuition second child";

$lang["youth_group_tuition_full_year_third_child" ]                                     = "full year tuition third child";

$lang["youth_group_tuition_full_year_discount" ]                                        ="full year discount";

$lang["youth_group_tuition_bimonthly_first_child" ]                                     = "bimonthly tuition first child";

$lang["youth_group_tuition_bimonthly_second_child" ]                                    =  "bimonthly tuition second child";

$lang["youth_group_tuition_bimonthly_third_child" ]                                     =  "bimonthly tuition third child";

$lang["youth_group_tuition_monthly_first_child" ]                                       = "monthly tuition first child";

$lang["youth_group_tuition_monthly_second_child" ]                                      =  "monthly tuition second child";

$lang["youth_group_tuition_monthly_third_child" ]                                       =   "monthly tuition third child";

$lang["youth_group_late_payment_fee" ]                                          =  "late payment fee";

$lang["youth_group_late_pickup_fee" ]                                           = "late pickup fee";

$lang["youth_group_teacher_child_discount" ]                                    =  "teacher discount";

$lang["saturday_school"]                                                        =  "saturday school";

$lang["arabic_book_club"]                                                       =  "arabic book club";

$lang["youth_group"]                                                            =  "youth group";

$lang["saturday_school_pre_kinder_books_fee"]                                   =  "pre kinnder books fee";

$lang["saturday_school_elementary_books_fee"]                                   =  "elementary books fee";

$lang["saturday_school_islamic_textbook"]                                       =  "islamic textbook";

$lang["saturday_school_islamic_workbook"]                                       = "islamic workbook";

$lang["index_parent_children"]                                                  = "children";

$lang["payment_item_id"]                                                        = "payment type";

$lang["month_id"]                                                               = "month";

$lang["invoice"]                                                                = "invoice";

$lang["check_or_cash"]                                                          = "check or cash";

$lang["student_payment_description"]                                            = "description";

$lang["student_payment_date"]                                                   = "date";

$lang["student_payment_notes"]                                                  = "notes";

$lang["check"]                                                                  = "check";

$lang["cash"]                                                                   = "cash";

$lang["September"]                                                              = "september";

$lang["October"]                                                                = "october";

$lang["November"]                                                               = "novemeber";

$lang["December"]                                                               = "december";

$lang["January"]                                                                = "january";

$lang["February"]                                                               = "february";

$lang["March"]                                                                  = "march";

$lang["April"]                                                                  = "april";

$lang["May"]                                                                    = "may";

$lang["September-October"]                                                      = "September-October";

$lang["October-November"]                                                       = "October-November";

$lang["November-December"]                                                      = "November-December";

$lang["December-January"]                                                       = "December-January";

$lang["January-February"]                                                       = "January-February";

$lang["February-March"]                                                         = "February-March";

$lang["March-April"]                                                            = "March-April";

$lang["April-May"]                                                              = "April-May";

$lang["registration_fee"]                                                       = "registration fee";

$lang["books"]                                                                  = "books";

$lang["tuition"]                                                                = "tuition";

$lang["payment_item_type"]                                                      = "payment type";

$lang["select_payment_item_type"]                                               = "select payment type";

$lang["select_books_payment_type"]                                              = "book payment type";





