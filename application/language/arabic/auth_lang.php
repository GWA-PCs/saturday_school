<?php  if ( ! defined("BASEPATH")) exit("No direct script access allowed");
/**
 * Name:  Auth Lang - English
 *
 * Author: Ben Edmunds
 * 		  ben.edmunds@gmail.com
 *         @benedmunds
 *
 * Author: Daniel Davis
 *         @ourmaninjapan
 *
 * Location: http://github.com/benedmunds/ion_auth/
 *
 * Created:  03.09.2013
 *
 * Description:  English language file for Ion Auth example views
 *
 */

// Errors
$lang["error_csrf"] = "This form post did not pass our security checks.";

// Login
$lang["login_heading"]                                          = "تسجيل الدخول";
$lang["login_subheading"]                                       = "الرجاء تسجيل الدخول باستخدام البريد الإلكتروني / اسم المستخدم وكلمة المرور أدناه.";
$lang["login_identity_label"]                                   = "البريد الإلكتروني / اسم المستخدم:" ;
$lang["login_password_label"]                                   = "كلمة السر:" ;
$lang["login_remember_label"]                                   = "تذكرني:" ;
$lang["login_submit_btn"]                                       = "تسجيل الدخول" ;
$lang["login_forgot_password"]                                  = "هل نسيت كلمة المرور؟";
$lang["index_heading"]                                          = "المستخدمون";
$lang["index_subheading"]                                       = "فيما يلي قائمة بالمستخدمين.";
$lang["index_fname_th"]                                         = "الاسم الأول" ;
$lang["index_lname_th"]                                         = "اسم العائلة" ;
$lang["index_email_th"]                                         = "البريد الإلكتروني";
$lang["index_groups_th"]                                        = "مجموعات";
$lang["index_status_th"]                                        = "الحالة" ;
$lang["index_action_th"]                                        = "العمل" ;
$lang["index_active_link"]                                      = "نشط";
$lang["index_inactive_link"]                                    = "غير نشط" ;
$lang["index_create_user_link"]                                 = "إنشاء مستخدم جديد" ;
$lang["index_create_group_link"]                                = "إنشاء مجموعة جديدة" ;
$lang["deactivate_heading"]                                     = "إلغاء تنشيط المستخدم" ;
$lang["deactivate_subheading"]                                  = "هل أنت متأكد من أنك تريد إلغاء تنشيط المستخدم ";
$lang["deactivate_confirm_y_label"]                             = "نعم:" ;
$lang["deactivate_confirm_n_label"]                             = "لا:" ;
$lang["deactivate_submit_btn"]                                  = "إرسال" ;
$lang["deactivate_validation_confirm_label"]                    = "تأكيد" ;
$lang["deactivate_validation_user_id_label"]                    = "هوية المستخدم";
$lang["create_user_heading"]                                    = "إنشاء مستخدم" ;
$lang["create_user_subheading"]                                 = "الرجاء إدخال معلومات المستخدمين أدناه.";
$lang["create_user_fname_label"]                                = "الاسم الأول:" ;
$lang["create_user_lname_label"]                                = "اسم العائلة:";
$lang["create_user_identity_label"]                             = "الهوية:" ;
$lang["create_user_company_label"]                              = "اسم الشركة:" ;
$lang["create_user_email_label"]                                = "البريد الإلكتروني:" ;
$lang["create_user_phone_label"]                                = "هاتف:" ;
$lang["create_user_password_label"]                             = "كلمة السر:" ;
$lang["create_user_password_confirm_label"]                     = "تأكيد كلمة المرور:" ;
$lang["create_user_submit_btn"]                                 = "إنشاء مستخدم" ;
$lang["create_user_validation_fname_label"]                     = "الاسم الأول" ;
$lang["create_user_validation_lname_label"]                     = "اسم العائلة" ;
$lang["create_user_validation_identity_label"]                  = "الهوية" ;
$lang["create_user_validation_email_label"]                     = "عنوان البريد الإلكتروني";
$lang["create_user_validation_phone_label"]                     = "هاتف";
$lang["create_user_validation_company_label"]                   = "اسم الشركة";
$lang["create_user_validation_password_label"]                  = "كلمة المرور" ;
$lang["create_user_validation_password_confirm_label"]          = "تأكيد كلمة المرور" ;
$lang["edit_user_heading"]                                      = "تحرير المستخدم" ;
$lang["edit_user_subheading"]                                   = "الرجاء إدخال معلومات المستخدمين أدناه.";
$lang["edit_user_fname_label"]                                  = "الاسم الأول:" ;
$lang["edit_user_lname_label"]                                  = "اسم العائلة:";
$lang["edit_user_company_label"]                                = "اسم الشركة:" ;
$lang["edit_user_email_label"]                                  = "البريد الإلكتروني:" ;
$lang["edit_user_phone_label"]                                  = "هاتف:" ;
$lang["edit_user_password_label"]                               = "كلمة المرور: (إذا تم تغيير كلمة المرور)";
$lang["edit_user_password_confirm_label"]                       = "تأكيد كلمة المرور: (إذا تم تغيير كلمة المرور)";
$lang["edit_user_groups_heading"]                               = "عضو المجموعات" ;
$lang["edit_user_submit_btn"]                                   = "حفظ المستخدم" ;
$lang["edit_user_validation_fname_label"]                       = "الاسم الأول" ;
$lang["edit_user_validation_lname_label"]                       = "اسم العائلة" ;
$lang["edit_user_validation_email_label"]                       = "عنوان البريد الإلكتروني";
$lang["edit_user_validation_phone_label"]                       = "هاتف";
$lang["edit_user_validation_company_label"]                     = "اسم الشركة";
$lang["edit_user_validation_groups_label"]                      = "مجموعات";
$lang["edit_user_validation_password_label"]                    = "كلمة المرور" ;
$lang["edit_user_validation_password_confirm_label"]            = "تأكيد كلمة المرور" ;
$lang["create_group_title"]                                     = "إنشاء مجموعة" ;
$lang["create_group_heading"]                                   = "إنشاء مجموعة" ;
$lang["create_group_subheading"]                                = "الرجاء إدخال معلومات المجموعة أدناه.";
$lang["create_group_name_label"]                                = "اسم المجموعة:" ;
$lang["create_group_desc_label"]                                = "الوصف:" ;
$lang["create_group_perm_label"]                                = "الصلاحيات:" ;
$lang["create_group_submit_btn"]                                = "إنشاء مجموعة" ;
$lang["create_group_validation_name_label"]                     = "اسم المجموعة" ;
$lang["create_group_validation_desc_label"]                     = "الوصف" ;
$lang['create_group_validation_perm_label']                     = 'الصلاحيات';

$lang["edit_group_title"]                                       = "تحرير المجموعة" ;
$lang["edit_group_saved"]                                       = "المجموعة المحفوظة" ;
$lang["edit_group_heading"]                                     = "تحرير المجموعة" ;
$lang["edit_group_subheading"]                                  = "الرجاء إدخال معلومات المجموعة أدناه.";
$lang["edit_group_name_label"]                                  = "اسم المجموعة:" ;
$lang["edit_group_desc_label"]                                  = "الوصف:" ;
$lang["edit_group_submit_btn"]                                  = "حفظ المجموعة" ;
$lang["edit_group_validation_name_label"]                       = "اسم المجموعة" ;
$lang["edit_group_validation_desc_label"]                       = "الوصف" ;
$lang["change_password_heading"]                                = "تغيير كلمة المرور" ;
$lang["change_password_old_password_label"]                     = "كلمة المرور القديمة:";
$lang["change_password_new_password_label"]                     = "كلمة مرور جديدة (طول %s محارف  على الأقل):";
$lang["change_password_new_password_confirm_label"]             = "قم بتأكيد كلمة المرور الجديدة:";
$lang["change_password_submit_btn"]                             = "تغيير" ;
$lang["change_password_validation_old_password_label"]          = "كلمة المرور القديمة";
$lang["change_password_validation_new_password_label"]          = "كلمة السر الجديدة";
$lang["change_password_validation_new_password_confirm_label"]  = "تأكيد كلمة المرور الجديدة" ;
$lang["forgot_password_heading"]                                = "نسيت كلمة المرور" ;
$lang["forgot_password_subheading"]                             = "يرجى إدخال٪ s حتى نتمكن من إرسال بريد إلكتروني إليك لإعادة تعيين كلمة المرور.";
$lang["forgot_password_email_label"]                            = "٪ s:" ;
$lang["forgot_password_submit_btn"]                             = "إرسال" ;
$lang["forgot_password_validation_email_label"]                 = "عنوان البريد الإلكتروني";
$lang["forgot_password_username_identity_label"]                = "اسم المستخدم" ;
$lang["forgot_password_email_identity_label"]                   = "البريد الإلكتروني";
$lang["forgot_password_email_not_found"]                        = "لا يوجد سجل لعنوان البريد الإلكتروني هذا.";
$lang["forgot_password_identity_not_found"]                     = "لا يوجد سجل لعنوان اسم المستخدم هذا.";
$lang["reset_password_heading"]                                 = "تغيير كلمة المرور" ;
$lang["reset_password_new_password_label"]                      = "كلمة مرور جديدة (طول حرف٪ s على الأقل):";
$lang["reset_password_new_password_confirm_label"]              = "قم بتأكيد كلمة المرور الجديدة:";
$lang["reset_password_submit_btn"]                              = "تغيير" ;
$lang["reset_password_validation_new_password_label"]           = "كلمة السر الجديدة";
$lang["reset_password_validation_new_password_confirm_label"]   = "تأكيد كلمة المرور الجديدة" ;
$lang["email_activate_heading"]                                 = "تنشيط الحساب لـ٪ s" ;
$lang["email_activate_subheading"]                              = "الرجاء النقر فوق هذا الارتباط إلى٪ s.";
$lang["email_activate_link"]                                    = "تفعيل حسابك";
$lang["email_forgot_password_heading"]                          = "إعادة تعيين كلمة المرور لـ٪ s" ;
$lang["email_forgot_password_subheading"]                       = "الرجاء النقر فوق هذا الارتباط إلى٪ s.";
$lang["email_forgot_password_link"]                             = "إعادة ضبط كلمة المرور الخاصة بك" ;
$lang['email_new_password_heading']                             = 'كلمة مرور جديدة لـ %s';
$lang['email_new_password_subheading']                          = 'تمت إعادة تعيين كلمة المرور الخاصة بك إلى: %s';

$lang['identify']                                               = 'اسم المستخدم';
$lang['password']                                               = 'كلمة المرور';

