<?php
$lang['form_validation_required']		=  "حقل %s مطلوب.";
$lang['form_validation_isset']			=  "يجب أن يحتوي %s قيمة.";
$lang['form_validation_valid_email']		= "يجب أن يحتوي %s عنوان بريد إلكتروني صحيح.";
$lang['form_validation_valid_emails']		 = "يجب أن يحتوي الحقل %s على عناوين بريد إلكتروني صحيحة.";
$lang['form_validation_valid_url']		 = "يجب أن يحتوي الحقل %s عنوان URL صحيح.";
$lang['form_validation_valid_ip']		 = "يجب أن يحتوي الحقل %s عنوان IP صحيح.";
$lang['form_validation_min_length']		 = "يجب أن يكون عدد أحرف كلمة المرور على الأقل 8.";
$lang['form_validation_max_length']		 = "يجب أن لا يزيد عدد أحرف %s عن %s.";
$lang['form_validation_exact_length']		= "يجب أن يتكون %s من %s أحرف بالضبط.";
$lang['form_validation_alpha']			= "يجب أن يحتوي %s أحرفاً فقط.";
$lang['form_validation_alpha_numeric']		= "يجب أن يحتوي %s أحرفاً إنجليزية وأرقاماً فقط.";
$lang['form_validation_alpha_numeric_spaces']	= "يجب أن يحتوي %s أحرفاو فراغاتً إنجليزية وأرقاماً فقط.";
$lang['form_validation_alpha_dash']		= "يجب أن يحتوي %s على أرقام وأحرف وعلامتي - و _ فقط.";
$lang['form_validation_numeric']		 = "يجب أن يحتوي %s على أعداد فقط.";
$lang['form_validation_is_numeric']		= "يجب أن يحتوي %s أعداد فقط.";
$lang['form_validation_integer']		 = "يجب أن يحتوي %s على عدد.";
$lang['form_validation_regex_match']		 = "الحقل %s ليس مكتوباً بالنسق الصحيح.";
$lang['form_validation_matches']		= "قيمة كلمة المرور لا تطابق قيمة التأكيد";
$lang['form_validation_differs']		= 'The {field} field must differ from the {param} field.';
$lang['form_validation_is_unique'] 		= "%s المدخل موجود مسبقاً";
$lang['form_validation_is_natural']		 = "يجب أن يحتوي %s أرقام موجبة فقط.";
$lang['form_validation_is_natural_no_zero']	= "يجب أن يحتوي %s رقماً أكبر من الصفر.";
$lang['form_validation_decimal']		= "يجب ان يحتوي %s على رقم عشري..";
$lang['form_validation_less_than']		 = "يجب ان يحتوي %1 على عدد اصغر من %s.";
$lang['form_validation_less_than_equal_to']	= 'The {field} field must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= "خطأ في الحقل %s يجب أن يحوي على قيمة أكبر أو تساوي  %d";
$lang['form_validation_greater_than_equal_to']	= 'The {field} field must contain a number greater than or equal to {param}.';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your field name {field}.';
$lang['form_validation_in_list']		= 'The {field} field must be one of: {param}.';
$lang['form_validation_biger_than']		= "خطأ في الحقل %s يجب أن يحوي على قيمة أكبر من %d";
$lang['form_validation_check_national_number']		= 'يجب أن يحتوي حقل الرقم الوطني على   11 رقم فقط';

/* End of file form_validation_lang.php */
/* Location: ./application/language/arabic/form_validation_lang.php */
