$('[data-target="#delete-modal"]').on('click', function (event) {
    event.preventDefault();
    var item_id = $(this).closest('tr').data('id');
    var item_title = $(this).closest('tr').data('title');
    var modal = $($(this).data('target'));

    modal.find('span[id="item_title"]').html(item_title);
    modal.find('input[name="item_id"]').val(item_id);
});

//
//$("#inputGrade_id").change(function () {
//
//    if ($(this).data('options') == undefined) {
//        $(this).data('options', $('#inputRoom_id option').clone());
//    }
//    var id = $(this).find(":selected").attr('grade_id1');
//    var options = $(this).data('options').filter('[grade_id2=' + id + ']');
//    $('#inputRoom_id').html(options);
//});


$("#inputRoom_id").change(function () {

    if ($(this).data('options') == undefined) {
        $(this).data('options', $('#inputStudent_record_id option').clone());
    }
    var id = $(this).val();
    var options = $(this).data('options').filter('[grade_id2=' + id + ']');
    $('#inputStudent_record_id').html(options);
});

